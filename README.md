# Oroboros Core

Oroboros is a robust cross-language framework focusing on providing a unified
and standards compliant system for quickly prototyping, developing, and deploying
applications, middleware, modules, extensions, services, and libraries of useful tools.

It provides api's for bash, javascript and php, all of which can function independently
or inter-dependently. The api and order of operations of each is distinctly similar,
allowing for a straightforward, no-nonsense approach to fullstack development.
Code written in any of the three will look very familiar to anyone who is familiar
with any of them, regardless of whether they have spent any time in that specific language.

This system provides robust tools for quickly laying out entire applications in a couple
of short cli one-liners. It's toolset is inherently unopinionated, standards compliant,
and compatible with most everything without issue. It does not assume control unless
explicitly told to do so, so it can operate as a library of resources, a wrapper for
an existing system, a set of complementary utilities, or as a fully independent
application stack.

## Requirements

There are very few hard requirements, but much that is supported.

- Bash 4.2+ _(if you are using the cli api. Ksh should also work fine. If you are not using the cli api, you don't need this)_
- PHP 7.4+ _(only if you are using the php api, bash and javascript work fine without this)_
- Node.js _(needed if you are building from source and using the full web stack. If you are not, you don't need it)_
- Apache 2.4+ _(if you are using the full web stack. NginX support will be added at a later date. If you are not using the integrated web stack you don't need this unless your other dependencies require it)_.

### Optional Requirements

- PhantomJS _(For headless pipeline driven frontend testing via Mocha, if desired)_
- MySQL _(for model development, materialization of models, database backup management, etc, if desired. Default web stack functionality does not require a database)_
- Bash _(Or Ksh. Used for synchronizing with a command line api, if desired)_
- Redis/Memcached/OpCache _(For Psr-6 caching, one or more of if desired)_

## Installation

Setup is straightforward. There is little to no configuration required.
Run the following to build the package:

- `cd path/to/repo`
- `chmod +x oroboros`
- `npm install`
- `grunt init`

That's it for basic installation.

If you have any issues, there is a builtin utility to analyze your server environment for any conflicts:

`./oroboros -c -v`, or alternately `./oroboros --core --verify` if you prefer the longhand version.

If you want to use the build and scaffold tools systemwide, you should also run this:

`ln -s /path/to/oroboros/oroboros /usr/local/bin/oroboros`


### Using as a standalone application

There is a build tool to create an application in any relevant source directory.

_(assuming you symlinked the binary into /usr/local/bin as mentioned above)_

- Make a directory in your webroot
- `cd /path/to/your/folder`
- `oroboros build app --namespace="yourvendorname\\yourpackagename", --name="yoursitename" --author="yourname" --path="$(pwd)"`

`--path` may also be arbitrarily specified from anywhere if you are not working directly in the source folder, provided your shell user has write permissions for the specified directory.

--------

__if you prefer an interactive shell to choose build options__

run this instead of the aforementioned command:

`oroboros build app -i`

This will walk you through all required and optional settings.

Your app will then be accessible via `localhost` by default, or whatever name you configured if you setup a `vhost.conf` file for it.

You may also run the default core implementation the same way by placing it in your webroot and assigning it a `vhost.conf` file of it's own.

--------

__If you created an app:__

You may then freely override any classes, configs, environment variables, etc by mirroring their structure,
or add your own also, which will be picked up automatically. Classloading is done using a variant of Psr-4 which creates
a priority ordering for declared vendor/package namespace roots, which allows your application to override any class
as needed without disrupting the underlying package or performing complicated registration.
All of the internals that explicitly require pre-defined classes include them verbosely,
so cases where override functionality should integrate into mechanics and places where it should not are not a further concern.

Likewise, configurations and environment variables may be similarly overridden. Only keys that need to be added/modified are required,
everything else will inherit the pre-existing defaults. This is consistent throughout all implementations of configuration or environment
variable definition.

### Using as middleware in an existing application

This package ships with its own PSR-4 implementation, so there is no need to register it as a composer package.
You may, but will work fine without any such dependency also.

todo _Document Middleware Psr Integration_

## Features

This package ships with a number of useful default features, which can in most cases be used interchangeably with standardized implementations.

### PHP Features

- Psr-1 compliant.
- Psr-2 compliant.
- Standalone Psr-4 implementation, which can be used alone or in conjunction with composer.
- Psr-6 implementation.
- Psr-7 implementation.
- Psr-11 implementation.
- Psr-17 implementation.
- Twig templating integration, which can be substituted for another template engine with minimal overhead.
- Provides clean implementations of Bootstrap and Material Design to build off of.
- Out of the box bootstrap 5 implementation with fully overrideable, extendable, and configurable templating via twig.
- A websocket layer.
- A database migration/backup/restore/unit-testing utility
- Integrated unit testing for php and javascript
- Extensible module/service/extension system for bash, php, as well as javascript.

Planned:

- Psr-3 _under development_
- ELK stack integration
- AWS integration
- Application clustering with auto-discoverability
- SSO

### Backend Features

- Database schemas are automatically mapped and cached, including relational constraints, indexes, valid column formats and lengths. This makes models automatically discoverable to related models with little direct consideration.
- Provides a robust command line api that calls like a standard bash command, interacts with your controllers identically to a web request, and outputs views correctly formatted for the console. Both route types (http and cli) are inaccessible to each other to prevent introducing security exploits.
- Scaffold models automatically by parsing an existing data layer schema.
- Scaffold a data layer schema from existing models. Useful for data layer migrations.
- Extension api to add additional libraries, views, models, components, etc to the package as needed.
- Everything is overrideable at the application scope.
- Psr specs used extensively. Package substitution is trivial.
- Very straightforward rest api. Easy to use and develop on.
- Routing and configuration paradigms are very flexible. They can be served from a config, database, redis, flat file, or whatever means is appropriate to implementation. Underlying platform enforces only the format, not how it arrives at said format.

### Frontend Features

- Automatically registers preload/prefetch for all remote assets included to maximize page speed.
- Templating supports nested dependencies, and automatically includes dependencies of registered assets when they are included.
- Interchangeable layouts.
- Interchangeable skins/color schemes, which are compatible with all layouts.
- Supports multiple frontend frameworks. Even in the same page if needed.

### CI Features

The following functionality is mapped to `grunt watch`, or may be manually called via other grunt commands.

- Unit testing is configured to be fully automated on file change for any detected changes to PHP or Javascript files throughout the entire project.
- Builds run automatically on file change for all javascript/scss/templates
- Cache management is automatic.
- Only relevant grunt tasks run based on the modifications detected, which prevents automation time from growing excessive.
- Enforces PSR standards for PHP
- Enforces clean linting of ES6 compliance for Javascript
- Backend Unit Testing via PHPUnit
- Frontend Unit Testing via ESLint and Mocha

### Design Features

- Integrated SASS support.
- Integrated Compass support.
- Layout (structure) and design (skin/style) inherently separated. Layout and palatte can be freely mixed and matched without collision.
- Simple, extensible, robust templating. Easy to understand, extend, or add to. No actual php code exists in templates whatsoever.

## Extension

todo _The extension api is currently under development, but is not yet implemented fully._

## Testing

Unit tests for php automatically run on file change whenever you are monitoring with `grunt watch`.

You may also run them manually with `grunt test`

