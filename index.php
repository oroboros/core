<?php
/**
 * Entry point for using this library as a standalone web stack.
 * Including this file will use the package as it stands as a standalone application.
 */
ini_set('display_errors', 1);
ini_set('memory_limit', '4G');
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once __DIR__ . DIRECTORY_SEPARATOR . 'setup.php';
\Oroboros\core\Oroboros::bootstrap();
exit;
