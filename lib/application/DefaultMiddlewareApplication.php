<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\application;

/**
 * Default Middleware Application
 * The default application implementation.
 * This application is used if none is provided.
 *
 * @author Brian Dayhoff
 */
final class DefaultMiddlewareApplication extends \Oroboros\core\abstracts\application\AbstractApplication implements \Oroboros\core\interfaces\CoreInterface
{

    /**
     * Returns base namespace of the application.
     * This is used for Psr-4 registration.
     * @return string
     */
    public function getBaseNamespace(): string
    {
        return \Oroboros\core\interfaces\environment\EnvironmentInterface::PACKAGE_NAMESPACE_ROOT;
    }

    /**
     * Returns the root directory of the application.
     * @return string
     */
    public function getApplicationRoot(): string
    {
        return OROBOROS_CORE_BASE_DIRECTORY;
    }

    /**
     * Returns the source directory of application templates.
     * This is used for template overrides.
     * Return null if not applicable.
     * @return string|null
     */
    public function getApplicationTemplateDirectory(): ?string
    {
        return OROBOROS_CORE_TEMPLATES;
    }

    /**
     * Returns the public folder of the application.
     * This is used for generating links to frontend content.
     * Return null if not applicable.
     * @return string|null
     */
    public function getApplicationPublicDirectory(): ?string
    {
        return OROBOROS_CORE_BASE_DIRECTORY . 'public' . DIRECTORY_SEPARATOR;
    }

    /**
     * Returns the source folder for frontend scripts and stylesheet source.
     * This is used for running build tasks for the application.
     * Return null if not applicable.
     * @return string|null
     */
    public function getApplicationEtcDirectory(): ?string
    {
        return OROBOROS_CORE_BASE_DIRECTORY . 'etc' . DIRECTORY_SEPARATOR;
    }

    /**
     * Returns the source directory of the application.
     * This is used for Psr-4 autoload registration.
     * @return string
     */
    public function getApplicationSourceDirectory(): string
    {
        return $this::getApplicationRoot() . 'lib' . DIRECTORY_SEPARATOR;
    }

    /**
     * Returns the unit test directory.
     * This is used for running automated unit tests.
     * Return null if not applicable.
     * @return string|null
     */
    public function getApplicationTestDirectory(): ?string
    {
        return OROBOROS_CORE_BASE_DIRECTORY . 'tests' . DIRECTORY_SEPARATOR;
    }
}
