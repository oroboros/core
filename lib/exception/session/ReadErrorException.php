<?php
/*
 * The MIT License
 *
 * @author Brian Dayhoff <Brian Dayhoff@me.com>
 * @copyright (c) 2021, Brian Dayhoff <Brian Dayhoff@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\exception\session;

/**
 * Session Read Error Exception
 *
 * Indicates that a session exists but cannot be read.
 * Used in the session handler.
 *
 * @author Brian Dayhoff <Brian Dayhoff@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 */
class ReadErrorException extends \Oroboros\core\exception\ErrorException implements \Oroboros\core\interfaces\CoreInterface
{
    
}
