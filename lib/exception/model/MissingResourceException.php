<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\exception\model;

/**
 * Missing Resource Exception
 * 
 * Exception raised by models when the
 * underlying resource they represent is not reachable.
 * 
 * This may mean a data model cannot connect to a database
 * because the database is not on or the schema represented
 * does not exist in the given database connection.
 *
 * For a file model, it means the file represented is missing.
 * For a service model, it means the endpoint api they are
 * attempting to connect to is not accepting connections
 * or is not responding.
 * 
 * For an object model, it means that the object they are
 * acting against is missing, unloadable,
 * or otherwise unavailable.
 * 
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
final class MissingResourceException extends \Oroboros\core\exception\ErrorException implements \Oroboros\core\interfaces\exception\model\ModelExceptionInterface, \Oroboros\core\interfaces\CoreInterface
{
    
}
