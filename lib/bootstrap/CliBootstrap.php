<?php
namespace Oroboros\core\bootstrap;

/**
 * Bootstraps the environment if called from the command line (with shell).
 *
 * @author Brian Dayhoff
 */
final class CliBootstrap extends \Oroboros\core\abstracts\bootstrap\AbstractBootstrap implements \Oroboros\core\interfaces\CoreInterface
{

    const BOOTSTRAP_SCOPE = 'cli';
    const BOOTSTRAP_CLI = true;
    const BOOTSTRAP_HEADLESS = false;
    
    /**
     * Indicates to use the CliLogger by default
     */
    const DEFAULT_LOGGER = 'log\\CliLogger';

    /**
     * Defines the router class as cli
     */
    const ROUTER_CLASS = 'router\\CliRouter';

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
    }

    public function handleUncaughtError(\Exception $error)
    {
        try {
            $controller = $this->getController()->getErrorController();
            $method = 'error';
            try {
                throw $error;
            } catch (\Oroboros\core\exception\cli\ExitException $e) {
                // Clean exit
                $code = 0;
            } catch (\Oroboros\core\exception\http\ForbiddenException $e) {
                // Controller could not resolve operation, 
                // but reported cleanly that it failed.
                $code = 1;
                $controller->$method($code, $error->getMessage());
            } catch (\Exception $e) {
                // All other errors default to fatal error code, usually 500 response
                $code = $controller::DEFAULT_FATAL_ERROR_CODE;
                $controller->$method($code, $error->getMessage());
            }
        } catch (\Exception $e) {
            parent::handleUncaughtError($error);
        }
    }
    
    protected function initializeUser(\Oroboros\core\interfaces\library\user\UserInterface $user = null): void
    {
        if (is_null($user)) {
            $user = $this->load('library', 'user\\CliUser', null, array_replace([
                'request' => $this->getRequest()
                    ], $this->getArguments()), $this->getFlags());
        }
        parent::initializeUser($user);
    }
}
