<?php
/*
 * The MIT License
 *
 * Copyright 2015 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\bootstrap;

/**
 * Bootstraps the environment if called from an HTTP request.
 *
 * @author Brian Dayhoff
 */
final class HttpBootstrap extends \Oroboros\core\abstracts\bootstrap\AbstractBootstrap implements \Oroboros\core\interfaces\CoreInterface
{

    const BOOTSTRAP_SCOPE = 'http';
    const BOOTSTRAP_CLI = false;
    const BOOTSTRAP_HEADLESS = false;

    /**
     * Defines the router class as http
     */
    const ROUTER_CLASS = 'router\\HttpRouter';

    /**
     * Uncomment one of the below for debugging
     */
//    const DEFAULT_LOGGER = 'log\\DebugLogger';

//    const DEFAULT_LOGGER = 'log\\DevLogger';


    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
    }

    public function handleUncaughtError(\Exception $error)
    {
        try {
            $controller = $this->getController()->getErrorController();
            $method = 'error';
            try {
                throw $error;
            } catch (\Oroboros\core\exception\http\NotAuthorizedException $e) {
                // Not authorized
                $code = 401;
            } catch (\Oroboros\core\exception\http\ForbiddenException $e) {
                // Access Forbidden
                $code = 403;
            } catch (\Oroboros\core\exception\http\NotFoundException $e) {
                // Page Not Found
                $code = 404;
            } catch (\Oroboros\core\exception\http\ServerErrorException $e) {
                // Internal Server Error
                $code = 500;
            } catch (\Exception $e) {
                // All other errors default to fatal error code, usually 500 response
                $code = $controller::DEFAULT_FATAL_ERROR_CODE;
            }
            $controller->$method($code, $error->getMessage());
        } catch (\Exception $e) {
            parent::handleUncaughtError($error);
        }
    }

    protected function initializeUser(\Oroboros\core\interfaces\library\user\UserInterface $user = null): void
    {
        if (is_null($user)) {
            $user = $this->load('library', 'user\\HttpUser', null, array_replace([
                'request' => $this->getRequest()
                    ], $this->getArguments()), $this->getFlags());
        }
        parent::initializeUser($user);
    }
}
