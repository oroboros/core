<?php
namespace Oroboros\core\bootstrap;

/**
 * Bootstraps the environment if called from the command line (headless).
 *
 * @author Brian Dayhoff
 */
final class CgiBootstrap extends \Oroboros\core\abstracts\bootstrap\AbstractBootstrap implements \Oroboros\core\interfaces\CoreInterface
{

    const BOOTSTRAP_SCOPE = 'cgi';
    const BOOTSTRAP_CLI = true;
    const BOOTSTRAP_HEADLESS = true;

    /**
     * Defines the router class as cli
     */
    const ROUTER_CLASS = 'router\\CliRouter';

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
    }
}
