<?php
namespace Oroboros\core\view\html;

/**
 * Provides rendering capabilities for html pages
 *
 * @author Brian Dayhoff
 */
final class PageView extends \Oroboros\core\abstracts\view\AbstractHtmlView implements \Oroboros\core\interfaces\CoreInterface
{
    // no-op. All functionality provided through abstraction.
}
