<?php
namespace Oroboros\core\view\xml;

/**
 * Provides rendering capabilities for xml responses
 *
 * @author Brian Dayhoff
 */
final class XmlView extends \Oroboros\core\abstracts\view\AbstractXmlView implements \Oroboros\core\interfaces\CoreInterface
{
    // no-op. All functionality provided through abstraction.
}
