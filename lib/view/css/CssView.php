<?php
namespace Oroboros\core\view\css;

/**
 * Provides rendering capabilities for css responses
 *
 * @author Brian Dayhoff
 */
final class CssView extends \Oroboros\core\abstracts\view\AbstractCssView implements \Oroboros\core\interfaces\CoreInterface
{
    // no-op. All functionality provided through abstraction.
}
