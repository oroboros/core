<?php
namespace Oroboros\core\view\javascript;

/**
 * Provides rendering capabilities for javascript responses
 *
 * @author Brian Dayhoff
 */
final class JavascriptView extends \Oroboros\core\abstracts\view\AbstractJavascriptView implements \Oroboros\core\interfaces\CoreInterface
{
    // no-op. All functionality provided through abstraction.
}
