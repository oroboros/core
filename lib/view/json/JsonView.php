<?php
namespace Oroboros\core\view\json;

/**
 * Provides rendering capabilities for json responses
 *
 * @author Brian Dayhoff
 */
final class JsonView extends \Oroboros\core\abstracts\view\AbstractJsonView implements \Oroboros\core\interfaces\CoreInterface
{
    // no-op. All functionality provided through abstraction.
}
