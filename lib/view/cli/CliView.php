<?php
namespace Oroboros\core\view\cli;

/**
 * Provides rendering capabilities for responses to the command line
 *
 * @author Brian Dayhoff
 */
final class CliView extends \Oroboros\core\abstracts\view\AbstractCliView implements \Oroboros\core\interfaces\CoreInterface
{
    // no-op. All functionality provided through abstraction.
}
