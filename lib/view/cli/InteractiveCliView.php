<?php
namespace Oroboros\core\view\cli;

/**
 * Provides rendering capabilities for interactive command line shells.
 *
 * @author Brian Dayhoff
 */
final class InteractiveCliView extends \Oroboros\core\abstracts\view\AbstractInteractiveCliView implements \Oroboros\core\interfaces\CoreInterface
{
    // no-op. All functionality provided through abstraction.
}
