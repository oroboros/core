<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\coreDemo\library\menu\login;

/**
 * Frontend Ui Menu Definer
 * Defines the frontend navigation menu data.
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
final class DemoUiMenuDefiner extends \Oroboros\core\abstracts\library\menu\AbstractMenuDefiner implements \Oroboros\core\interfaces\CoreInterface
{

    use \Oroboros\core\traits\pluggable\ModuleLibraryTrait;
    use \Oroboros\core\traits\definer\JsonDefinerTrait {
        \Oroboros\core\traits\definer\JsonDefinerTrait::declareDataSet as private traitDeclareDataSet;
    }

    /**
     * Path to the definer definition config, relative to the application root.
     */
    const DEFINER_FILE = 'etc/menus/login/ui-menu.json';

    /**
     * Preflights class constant declaration prior to calling parent constructor
     * @param string $command
     * @param array $arguments
     * @param array $flags
     */
    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        $this->validateDefinerFile();
        parent::__construct($command, $arguments, $flags);
    }
    
    /**
     * Updates the ui menu definitions based on debug status, user login status,
     * and module elements that need to append to the ui menu.
     * 
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    protected function declareDataSet(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $data = $this->traitDeclareDataSet();
        if ($this->hasArgument('user')) {
            $data = $this->appendUserSpecificUi($data);
        }
        if ($this->hasArgument('modules')) {
            $data = $this->appendModuleSpecificUi($data);
        }
        if ($this::app()->debugEnabled()) {
            $data = $this->appendDebugUi($data);
        }
        return $data;
    }
    
    /**
     * Adds debug ui elements.
     * 
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $data
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    protected function appendDebugUi(\Oroboros\core\interfaces\library\container\ContainerInterface $data): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        return $data;
    }
    
    /**
     * Adds any user login state based ui elements.
     * 
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $data
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    protected function appendUserSpecificUi(\Oroboros\core\interfaces\library\container\ContainerInterface $data): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        d('appending user elements', $data);
        return $data;
    }
    
    /**
     * Adds any module-specific ui control elements.
     * 
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $data
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    protected function appendModuleSpecificUi(\Oroboros\core\interfaces\library\container\ContainerInterface $data): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        return $data;
    }
    
    /**
     * Localize the data directory to the module.
     * 
     * @return string
     */
    protected function declareDataRootPath(): string
    {
        return $this->getModule()->path();
    }
}
