<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\coreDemo\controller\html;

/**
 * Demo Dashboard Controller
 * Displays demo content for dashboard pages
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
final class DemoDashboardController extends \Oroboros\core\abstracts\controller\AbstractDashboardPage implements \Oroboros\core\interfaces\CoreInterface
{

    use \Oroboros\core\traits\html\DefinerPageContentUtility;

    const MODULE_NAME = 'demo';
    /**
     * Scope the controller to the login dashboard
     */
    const CLASS_SCOPE = 'dashboard';

    /**
     * Use the default html dashboard error controller
     */
    const ERROR_CONTROLLER = 'html\\dashboard\\ErrorController';

    /**
     * Handles the demo dashboard index page
     * @return \Oroboros\core\interfaces\controller\ControllerInterface
     */
    public function index()
    {
        try {
            $this->setupPage('index');
            $this->queueDemoAssets();
            return $this->render('html', 'dashboard/index');
        } catch (\Exception $e) {
            return $this->error(500, $e->getMessage());
        }
    }

    public function demoDashboardPage($args)
    {
        switch ($args['page']) {
            case 'charts':
                return $this->demoDashboardCharts();
                break;
            case 'buttons':
                return $this->demoDashboardButtons();
                break;
            case 'cards':
                return $this->demoDashboardCards();
                break;
            case 'utilities-color':
                return $this->demoDashboardColors();
                break;
            case 'utilities-borders':
                return $this->demoDashboardBorders();
                break;
            case 'utilities-animations':
                return $this->demoDashboardAnimations();
                break;
            case 'utilities-other':
                return $this->demoDashboardOther();
                break;
            case 'blank-page':
                return $this->demoBlankPage();
                break;
            case 'tables':
                return $this->demoDashboardTables();
                break;
            case 'charts':
                return $this->demoDashboardCharts();
                break;
            case 'not-authorized':
                return $this->demoDashboard401();
                break;
            case 'forbidden':
                return $this->demoDashboard403();
                break;
            case 'not-found':
                return $this->demoDashboard404();
                break;
            case 'server-error':
                return $this->demoDashboard500();
                break;
            case 'not-implemented':
                return $this->demoDashboard501();
                break;
            default:
                $this->error(404);
                break;
        }
    }

    /**
     * Handles the demo blank page display
     * @return \Oroboros\core\interfaces\controller\ControllerInterface
     */
    public function demoBlankPage()
    {
        try {
            $this->setupPage('blank-page');
            $this->queueDemoAssets();
            return $this->render('html', 'dashboard/index');
        } catch (\Exception $e) {
            return $this->error(500, $e->getMessage());
        }
    }

    /**
     * Handles the demo tables page display
     * @return \Oroboros\core\interfaces\controller\ControllerInterface
     */
    public function demoDashboardTables()
    {
        try {
            $this->setupPage('tables');
            $this->queueDemoAssets();
            return $this->render('html', 'dashboard/index');
        } catch (\Exception $e) {
            return $this->error(500, $e->getMessage());
        }
    }

    /**
     * Handles the demo chart page display
     * @return \Oroboros\core\interfaces\controller\ControllerInterface
     */
    public function demoDashboardCharts()
    {
        try {
            $this->setupPage('charts');
            $this->queueDemoAssets();
            return $this->render('html', 'dashboard/index');
        } catch (\Exception $e) {
            return $this->error(500, $e->getMessage());
        }
    }

    /**
     * Handles the demo button component page display
     * @return \Oroboros\core\interfaces\controller\ControllerInterface
     */
    public function demoDashboardButtons()
    {
        try {
            $this->setupPage('components-buttons');
            $this->queueDemoAssets();
            return $this->render('html', 'dashboard/index');
        } catch (\Exception $e) {
            return $this->error(500, $e->getMessage());
        }
    }

    /**
     * Handles the demo card component page display
     * @return \Oroboros\core\interfaces\controller\ControllerInterface
     */
    public function demoDashboardCards()
    {
        try {
            $this->setupPage('components-cards');
            $this->queueDemoAssets();
            return $this->render('html', 'dashboard/index');
        } catch (\Exception $e) {
            return $this->error(500, $e->getMessage());
        }
    }

    /**
     * Handles the demo animation utilities page display
     * @return \Oroboros\core\interfaces\controller\ControllerInterface
     */
    public function demoDashboardAnimations()
    {
        try {
            $this->setupPage('utilities-animations');
            $this->queueDemoAssets();
            return $this->render('html', 'dashboard/index');
        } catch (\Exception $e) {
            return $this->error(500, $e->getMessage());
        }
    }

    /**
     * Handles the demo border utilities page display
     * @return \Oroboros\core\interfaces\controller\ControllerInterface
     */
    public function demoDashboardBorders()
    {
        try {
            $this->setupPage('utilities-borders');
            $this->queueDemoAssets();
            return $this->render('html', 'dashboard/index');
        } catch (\Exception $e) {
            return $this->error(500, $e->getMessage());
        }
    }

    /**
     * Handles the demo color utilities page display
     * @return \Oroboros\core\interfaces\controller\ControllerInterface
     */
    public function demoDashboardColors()
    {
        try {
            $this->setupPage('utilities-colors');
            $this->queueDemoAssets();
            return $this->render('html', 'dashboard/index');
        } catch (\Exception $e) {
            return $this->error(500, $e->getMessage());
        }
    }

    /**
     * Handles the demo miscellaneous utilities page display
     * @return \Oroboros\core\interfaces\controller\ControllerInterface
     */
    public function demoDashboardOther()
    {
        try {
            $this->setupPage('utilities-other');
            $this->queueDemoAssets();
            return $this->render('html', 'dashboard/index');
        } catch (\Exception $e) {
            return $this->error(500, $e->getMessage());
        }
    }

    /**
     * Handles the demo dashboard 404 error page
     * @return \Oroboros\core\interfaces\controller\ControllerInterface
     */
    public function demoDashboard401()
    {
        return $this->error(401);
    }

    /**
     * Handles the demo dashboard 404 error page
     * @return \Oroboros\core\interfaces\controller\ControllerInterface
     */
    public function demoDashboard403()
    {
        return $this->error(403);
    }

    /**
     * Handles the demo dashboard 404 error page
     * @return \Oroboros\core\interfaces\controller\ControllerInterface
     */
    public function demoDashboard404()
    {
        return $this->error(404);
    }

    /**
     * Handles the demo dashboard 500 server error page
     * @return \Oroboros\core\interfaces\controller\ControllerInterface
     */
    public function demoDashboard500()
    {
        return $this->error(500);
    }

    /**
     * Handles the demo dashboard 501 not implemented page
     * @return \Oroboros\core\interfaces\controller\ControllerInterface
     */
    public function demoDashboard501()
    {
        return $this->error(501);
    }
    
    /**
     * Fetches the definer designated to handle the current page content payload
     * 
     * @param string|null $section
     * @param string|null $subsection
     * @param array|null $arguments
     * @return \Oroboros\core\interfaces\library\content\ContentDefinerInterface
     */
    protected function getDefiner(string $section = null, string $subsection = null, array $arguments = null): \Oroboros\core\interfaces\library\content\ContentDefinerInterface
    {
        if (is_null($arguments)) {
            $arguments = [];
        }
        $section = $this->brutecase(str_replace('-', ' ', $section));
        $path = trim(sprintf('content\\%1$s\\%2$s', static::CLASS_SCOPE, sprintf('Demo%1$sContentDefiner', ucfirst($section))), '\\');
        $definer = $this->load('library', $path, $subsection, $this->getArguments(), $this->getFlags());
        return $definer;
    }
    
    /**
     * Fetches the definer designated to handle the specified menu
     * 
     * @param string $section
     * @param string $subsection
     * @param array $arguments
     * @return \Oroboros\core\interfaces\library\menu\MenuDefinerInterface
     */
    protected function getMenuDefiner(string $section = null, string $subsection = null, array $arguments = null): \Oroboros\core\interfaces\library\menu\MenuDefinerInterface
    {
        if (is_null($arguments)) {
            $arguments = [];
        }
        $section = $this->brutecase(str_replace('-', ' ', $section));
        $path = trim(sprintf('menu\\%1$s\\%2$s', static::CLASS_SCOPE, sprintf('Demo%1$sDefiner', ucfirst($section))), '\\');
        $definer = $this->load('library', $path, $subsection, $this->getArguments(), $this->getFlags());
        return $definer;
    }

    private function queueDemoAssets()
    {
        $this->addScript('footer', 'dashboard', 'dashboard');
        $this->addScript('footer', 'chart.js', 'chart.js');
        $this->addScript('footer', 'demo-dashboard-chart-area', 'demo-dashboard-chart-area');
        $this->addScript('footer', 'demo-dashboard-chart-pie', 'demo-dashboard-chart-pie');
        $this->addScript('footer', 'demo-dashboard-datatables', 'demo-dashboard-datatables');
        $this->addStylesheet('stylesheet', 'dashboard', 'dashboard');
        $this->addStylesheet('stylesheet', 'chart.js', 'chart.js');
    }
}
