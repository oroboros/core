<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\coreDemo\controller\html;

/**
 * Demo Index Controller
 * Displays demo content for frontend pages
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
final class DemoIndexController extends \Oroboros\core\abstracts\controller\AbstractPageController implements \Oroboros\core\interfaces\CoreInterface
{

    use \Oroboros\core\traits\html\DefinerPageContentUtility;

    const MODULE_NAME = 'demo';

    /**
     * Scope the controller to the frontend
     */
    const CLASS_SCOPE = 'frontend';

    /**
     * Use the standard html error controller
     */
    const ERROR_CONTROLLER = 'html\\ErrorController';

    /**
     * Handles the demo index page
     * @return \Oroboros\core\interfaces\controller\ControllerInterface
     */
    public function index()
    {
        try {
            $this->setupPage('index');
            return $this->render('html', 'demo/index');
        } catch (\Exception $e) {
            return $this->error(500, $e->getMessage());
        }
    }

    public function demoPage(\Oroboros\core\interfaces\library\container\ContainerInterface $args)
    {
        if (!$args->has('section')) {
            return $this->error(404);
        }
        switch ($args['section']) {
            case 'blank-page':
                return $this->demoBlankPage();
                break;
            case 'three-columns':
                return $this->demoThreeColumns();
                break;
            case 'login':
            case 'registration':
            case 'forgot-password':
                return $this->error(501);
                break;
            case 'not-authorized':
                return $this->demo401();
                break;
            case 'forbidden':
                return $this->demo403();
                break;
            case 'not-found':
                return $this->demo404();
                break;
            case 'server-error':
                return $this->demo500();
                break;
            case 'not-implemented':
                return $this->demo501();
                break;
            default:
                return $this->error(404);
                break;
        }
    }

    /**
     * Handles the three column layout page
     * @return \Oroboros\core\interfaces\controller\ControllerInterface
     */
    public function demoThreeColumns()
    {
        try {
            $this->setupPage('three-columns');
            return $this->render('html', 'demo/index');
        } catch (\Exception $e) {
            return $this->error(500, $e->getMessage());
        }
    }

    /**
     * Handles the demo 404 error page
     * @return \Oroboros\core\interfaces\controller\ControllerInterface
     */
    public function demo401()
    {
        return $this->error(401, 'This is an example 401 error.');
    }

    /**
     * Handles the demo 404 error page
     * @return \Oroboros\core\interfaces\controller\ControllerInterface
     */
    public function demo403()
    {
        return $this->error(403, 'This is an example 403 error.');
    }

    /**
     * Handles the demo 404 error page
     * @return \Oroboros\core\interfaces\controller\ControllerInterface
     */
    public function demo404()
    {
        return $this->error(404, 'This is an example 404 error.');
    }

    /**
     * Handles the demo 500 server error page
     * @return \Oroboros\core\interfaces\controller\ControllerInterface
     */
    public function demo500()
    {
        return $this->error(500, 'This is an example 500 error.');
    }

    /**
     * Handles the demo 501 not implemented page
     * @return \Oroboros\core\interfaces\controller\ControllerInterface
     */
    public function demo501()
    {
        return $this->error(501, 'This is an example 501 error.');
    }
}
