# Oroboros Extension Documentation Index

Extensions allow Oroboros to be quickly extended with additional logic and application functionality.