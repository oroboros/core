/* 
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

'use strict';
(function (core) {
    const page = 'documentation-page';
    const oroboros = core();
    const abstract = oroboros.abstract('page');
    const component_bindings = {
    };
    const lib = {
        init: function () {
            
        }
    };
    const Documentation = class Documentation extends abstract {

        static classKey()
        {
            return page;
        }

        constructor(id, element) {
            super(id, element);
        }

        onReady(subject) {
            let element = subject.element;
            for (let key in component_bindings)
            {
                try {
                    subject.loadComponent(component_bindings[key], key);
                } catch (e) {
                    console.warn(e.message);
                }
            }

        }
    }
    lib.init();
    oroboros.register('page', page, Documentation);

    let element = document.getElementsByTagName('body')[0];
    element.id = page;
    oroboros.create('page', page, element);
})(Oroboros);
