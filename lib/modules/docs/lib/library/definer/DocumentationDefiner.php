<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\coreDocs\library\definer;

/**
 * Description of DocumentationDefiner
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
final class DocumentationDefiner extends \Oroboros\core\abstracts\library\definer\AbstractDefiner implements \Oroboros\coreDocs\interfaces\library\definer\DocumentationDefinerInterface, \Oroboros\core\interfaces\CoreInterface
{

    use \Oroboros\core\traits\pluggable\ModuleLibraryTrait;

    private static $definer_paths_initialized = false;
    private static $definer_paths = [
        'app' => null,
        'core' => [
            'architecture' => 'docs/architecture/',
            'installation' => 'docs/installation/',
            'configuration' => 'docs/configuration/',
            'filesystem' => 'docs/filesystem/',
            'troubleshooting' => 'docs/troubleshooting/',
        ],
        'modules' => null,
        'extensions' => null,
        'services' => null,
        'api' => [
            'bash' => 'docs/usage/cli/',
            'javascript' => 'docs/usage/javascript/',
            'php' => 'docs/usage/php/',
            'scss' => 'docs/usage/sass/',
            'design' => 'docs/design/',
        ],
    ];

    protected function declareDataSet(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        if (!self::$definer_paths_initialized) {
            $this->initializeDefinerPaths();
        }
        $dataset = [
            'require' => [],
            'optional' => [],
            'provide' => [],
            'defaults' => self::$definer_paths,
        ];
        return $this->containerize($dataset);
    }

    private function fetchCategoryDetails(array $category): array
    {
        $results = [];
        foreach ($category as $subcategory => $value) {
            if (is_null($value)) {
                continue;
            }
            if (is_string($value) && is_dir($value) && is_readable($value)) {
                $results[$subcategory] = $this->extractDocumentation($value);
                continue;
            }
            $results[$subcategory] = $value;
        }
        return $results;
    }

    private function extractDocumentation(string $path): array
    {
        if (!is_dir($path) || !is_readable($path)) {
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. Provided path [%2$s] is '
                        . 'not a directory or is not readable.', get_class($this), $path)
            );
        }
        $results = [];
        foreach (scandir($path) as $key => $value) {
            if (in_array($value, ['.', '..'])) {
                continue;
            }
            $test_path = str_replace('//', '/', sprintf('%2$s%1$s%3$s%1$s', DIRECTORY_SEPARATOR, $path, $value));
            if (is_dir($test_path) && is_readable($test_path)) {
                $results[$value] = $this->extractDocumentation($test_path);
                continue;
            }
            if (is_file($value) && strpos($value, '.md') === false) {
                // Not a documentation file
                continue;
            }
            $results[$value] = $this->generateFileDefinition(rtrim(str_replace('//', '/', sprintf('%2$s%1$s%3$s%1$s', DIRECTORY_SEPARATOR, $path, $value)), DIRECTORY_SEPARATOR));
        }
        return $results;
    }

    private function generateFileDefinition(string $file): array
    {
        $link_base = 'documentation';
        $slug = str_replace('.md', null, basename($file));
        $stub = $file;
        $name = basename($file);
        $is_index = false;
        $is_core = false;
        $is_app = false;
        $module = null;
        $extension = null;
        $service = null;
        if ($name === 'index.md') {
            $slug = $name = basename(substr($file, 0, strrpos($file, DIRECTORY_SEPARATOR . $name)));
        }
        $name = ucwords(str_replace('-', ' ', str_replace('.md', null, $name)));
        if (strpos($file, OROBOROS_CORE_BASE_DIRECTORY) !== false) {
            $is_core = true;
            $stub = str_replace(OROBOROS_CORE_BASE_DIRECTORY, null, $stub);
        } elseif (strpos($stub, $this::app()->getApplicationRoot()) !== false) {
            $is_app = true;
            $stub = str_replace($this::app()->getApplicationRoot(), null, $subject);
        }
        if (strpos($stub, 'index.md') !== false) {
            $is_index = true;
            $stub = trim(substr($stub, 0, strrpos($stub, 'index.md')), DIRECTORY_SEPARATOR);
        }
        $stub = str_replace('.md', null, $stub);
        if (strpos($stub, 'docs/') === 0) {
            // Core or app doc
            $link_base = sprintf('%1$s/%2$s', $link_base, 'core');
            $stub = substr($stub, 5);
        }
        if (strpos($stub, 'lib/modules/') === 0) {
            // Module doc
            $module = substr($stub, 12);
            $module = trim(substr($module, 0, strpos($module, DIRECTORY_SEPARATOR)), DIRECTORY_SEPARATOR);
            $link_base = sprintf('%1$s/%2$s', $link_base, 'modules');
            $stub = trim(str_replace('docs', null, substr($stub, 12)), DIRECTORY_SEPARATOR);
            if ($stub === $module) {
                $name = ucwords(str_replace('-', ' ', $stub));
            }
        }
        if (strpos($stub, 'lib/extensions/') === 0) {
            // Extension doc
            $extension = substr($stub, 15);
            $extension = trim(substr($extension, 0, strpos($extension, DIRECTORY_SEPARATOR)), DIRECTORY_SEPARATOR);
            $link_base = sprintf('%1$s/%2$s', $link_base, 'extensions');
            $stub = trim(str_replace('docs', null, substr($stub, 15)), DIRECTORY_SEPARATOR);
            if ($stub === $extension) {
                $name = ucwords(str_replace('-', ' ', $stub));
            }
        }
        if (strpos($stub, 'lib/services/') === 0) {
            // Extension doc
            $service = substr($stub, 13);
            $service = trim(substr($service, 0, strpos($service, DIRECTORY_SEPARATOR)), DIRECTORY_SEPARATOR);
            $link_base = sprintf('%1$s/%2$s', $link_base, 'services');
            $stub = trim(str_replace('docs', null, substr($stub, 13)), DIRECTORY_SEPARATOR);
            if ($stub === $service) {
                $name = ucwords(str_replace('-', ' ', $stub));
            }
        }
        $details = [
            'index' => $is_index,
            'core' => $is_core,
            'app' => $is_app,
            'module' => $module,
            'extension' => $extension,
            'service' => $service,
            'name' => $name,
            'slug' => $slug,
            'file' => $file,
            'link' => str_replace('//', '/', sprintf('/%1$s/%2$s/', $link_base, $stub)),
        ];
        return $details;
    }

    private function initializeDefinerPaths(): void
    {
        if (self::$definer_paths_initialized) {
            return;
        }
        $this->updateAppDocs();
        $this->updateCoreDocs();
        $this->updateExtensionDocs();
        $this->updateModuleDocs();
        $this->updateServiceDocs();
        self::$definer_paths_initialized = true;
    }

    private function updateCoreDocs(): void
    {
        $core = [];
        foreach (self::$definer_paths['core'] as $topic => $path) {
            $path = sprintf('%1$s%2$s', OROBOROS_CORE_BASE_DIRECTORY, $path);
            $core[$topic] = $this->extractDocumentation($path);
        }
        $core['index.md'] = $this->generateFileDefinition(OROBOROS_CORE_BASE_DIRECTORY . 'docs' . DIRECTORY_SEPARATOR . 'index.md');
        $core['index.md']['slug'] = 'core';
        $core['index.md']['name'] = 'Core';
        $core['index.md']['link'] = '/documentation/core/';
        self::$definer_paths['core'] = $core;
        $api = [];
        foreach (self::$definer_paths['api'] as $topic => $path) {
            $path = sprintf('%1$s%2$s', OROBOROS_CORE_BASE_DIRECTORY, $path);
            $api[$topic] = $this->extractDocumentation($path);
        }
        $api['index.md'] = $this->generateFileDefinition(OROBOROS_CORE_BASE_DIRECTORY . 'docs/usage' . DIRECTORY_SEPARATOR . 'index.md');
        $api['index.md']['slug'] = 'api';
        $api['index.md']['name'] = 'Api';
        $api['index.md']['link'] = '/documentation/core/usage/';
        self::$definer_paths['api'] = $api;
    }

    private function updateAppDocs(): void
    {
        if (OROBOROS_CORE_BASE_DIRECTORY === $this::app()->getApplicationRoot()) {
            // Core is running, no app docs
            unset(self::$definer_paths['app']);
            return;
        }
        $path = $this::app()->getApplicationRoot() . 'docs' . DIRECTORY_SEPARATOR;
        if (is_dir($path) && is_readable($path)) {
            self::$definer_paths['app'] = $this->extractDocumentation($path);
        }
    }

    private function updateModuleDocs(): void
    {
        self::$definer_paths['modules'] = [];
        foreach ($this->getModules() as $key => $module) {
            $path = $module->path() . 'docs' . DIRECTORY_SEPARATOR;
            if (is_dir($path) && is_readable($path)) {
                self::$definer_paths['modules'][$key] = $this->extractDocumentation($path);
                if (array_key_exists('index.md', self::$definer_paths['modules'][$key])) {
                    self::$definer_paths['modules'][$key]['index.md']['slug'] = $key;
                    if ($key === 'docs') {
                        // Correction for the documentation
                        // module having the same name as the folder
                        self::$definer_paths['modules'][$key]['index.md']['link'] = sprintf('%1$s%2$s/', self::$definer_paths['modules'][$key]['index.md']['link'], $key);
                    }
                }
            }
        }
        self::$definer_paths['modules']['index.md'] = $this->generateModuleIndexPath();
    }

    private function updateExtensionDocs(): void
    {
        self::$definer_paths['extensions'] = [];
        foreach ($this->getExtensions() as $key => $extension) {
            $path = $extension->path() . 'docs' . DIRECTORY_SEPARATOR;
            if (is_dir($path) && is_readable($path)) {
                self::$definer_paths['extensions'][$key] = $this->extractDocumentation($path);
                if (array_key_exists('index.md', self::$definer_paths['extensions'][$key])) {
                    self::$definer_paths['extensions'][$key]['index.md']['slug'] = $key;
                }
            }
        }
        self::$definer_paths['extensions']['index.md'] = $this->generateExtensionIndexPath();
    }

    private function updateServiceDocs(): void
    {
        self::$definer_paths['services'] = [];
        foreach ($this->getServices() as $key => $service) {
            $path = $service->path() . 'docs' . DIRECTORY_SEPARATOR;
            if (is_dir($path) && is_readable($path)) {
                self::$definer_paths['services'][$key] = $this->extractDocumentation($path);
                if (array_key_exists('index.md', self::$definer_paths['services'][$key])) {
                    self::$definer_paths['services'][$key]['index.md']['slug'] = $key;
                }
            }
        }
        self::$definer_paths['services']['index.md'] = $this->generateServiceIndexPath();
    }

    /**
     * Generates a data-structure valid index key for the module documentation section.
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private function generateModuleIndexPath(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $file = $this->generateFileDefinition($this->getModule()->path() . 'etc' . DIRECTORY_SEPARATOR . 'module-index.md');
        $file['slug'] = 'modules';
        $file['name'] = 'Modules';
        $file['link'] = '/documentation/modules/';
        $file['module'] = null;
        return $this->containerize($file);
    }

    /**
     * Generates a data-structure valid index key for the extension documentation section.
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private function generateExtensionIndexPath(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $file = $this->generateFileDefinition($this->getModule()->path() . 'etc' . DIRECTORY_SEPARATOR . 'extension-index.md');
        $file['slug'] = 'extensions';
        $file['name'] = 'Extensions';
        $file['link'] = '/documentation/extensions/';
        $file['module'] = null;
        return $this->containerize($file);
    }

    /**
     * Generates a data-structure valid index key for the service documentation section.
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private function generateServiceIndexPath(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $file = $this->generateFileDefinition($this->getModule()->path() . 'etc' . DIRECTORY_SEPARATOR . 'service-index.md');
        $file['slug'] = 'services';
        $file['name'] = 'Services';
        $file['link'] = '/documentation/services/';
        $file['module'] = null;
        return $this->containerize($file);
    }
}
