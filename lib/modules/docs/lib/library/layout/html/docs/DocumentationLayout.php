<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\coreDocs\library\layout\html\docs;

/**
 * Documentation Layout
 * This is the default layout for system documentation.
 *
 * @author Brian Dayhoff
 */
final class DocumentationLayout extends \Oroboros\core\abstracts\library\layout\AbstractLayout implements \Oroboros\core\interfaces\CoreInterface
{
    use \Oroboros\core\traits\pluggable\ModuleLibraryTrait;

    const MODULE_NAME = 'docs';
    
    private $layout_sections = [
        'header' => [
            'component' => 'header',
            'attributes' => [
                'id' => 'documentation-header',
                'class' => 'd-block order-first sticky-top'
            ]
        ],
        'left-sidebar' => [
            'component' => 'documentation-sidebar',
            'attributes' => [
                'id' => 'sidebar-left',
                'class' => 'h-100'
            ]
        ],
        'main' => [
            'component' => 'content',
            'attributes' => [
                'id' => 'documentation-content',
                'class' => 'd-flex container-fluid d-block h-100'
            ]
        ],
        'footer' => [
            'component' => 'footer',
            'attributes' => [
                'id' => 'page-footer',
                'class' => 'd-flex order-last mx-0 px-0 w-100 mt-auto'
            ]
        ],
    ];

    protected function declareLayoutBase(): string
    {
        return 'html/layout/documentation/default';
    }

    protected function declareSections(): array
    {
        return $this->layout_sections;
    }
}
