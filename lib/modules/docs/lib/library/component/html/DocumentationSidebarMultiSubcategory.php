<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\coreDocs\library\component\html;

/**
 * Documentation Sidebar Multi Subcategory
 * Renders a documentation subcategory that has additional nested subcategories
 * in the documentation sidebar
 * 
 * @author Brian Dayhoff
 */
final class DocumentationSidebarMultiSubcategory extends DocumentationSidebarSubcategory implements \Oroboros\core\interfaces\CoreInterface
{
    use \Oroboros\core\traits\pluggable\ModuleLibraryTrait;
    use \Oroboros\coreDocs\traits\library\component\html\DocumentationSidebarComponentUtilitiesTrait;

    const COMPONENT_TEMPLATE = 'component/documentation/sidebar/topic-subcategory-multi';
    const COMPONENT_KEY = 'documentation-sidebar-subcategory-multi';
    

    private $context_keys = [
        'title',
        'href',
        'icon',
        'subcategories',
    ];
    private $context_component_filters = [
        'subcategories' => 'filterSubcategories'
    ];

    protected function declareValidContextKeys(): array
    {
        return array_merge(parent::declareValidContextKeys(), $this->context_keys);
    }

    protected function declareContextFilters(): array
    {
        return array_merge(parent::declareContextFilters(), $this->context_component_filters);
    }
}
