<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\coreDocs\traits\library\component\html;

/**
 * Documentation Sidebar Component Utilities Trait
 * Common methods for documentation sidebar components
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
trait DocumentationSidebarComponentUtilitiesTrait
{

    protected function filterTopics($topics)
    {
        $results = [];
        foreach ($topics as $key => $topic) {
            if (is_object($topic) && ($topic instanceof \Oroboros\core\interfaces\library\container\ContainerInterface)) {
                $results[$key] = $this->getTopicComponent($key, $topic);
                continue;
            }
            $this->addContext($key, $topic);
        }
        return $this->containerize($results);
    }

    protected function filterCategories(array $categories): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $results = [];
        foreach ($categories as $key => $category) {
            $id = sprintf('%1$s-category-%2$s', $this->getContext('id'), $key);
            if (!(is_object($category) && ($category instanceof \Oroboros\core\interfaces\library\container\ContainerInterface))) {
                continue;
            }
            $component = $this->getComponent('documentation-sidebar-category', $id, [], $this->getFlags());
            $component->addContext('id', $id);
            $component->addContext('tagname', 'li');
            $component->addContext('class', 'list-group-item');
            foreach ($category as $k => $context) {
                if ($key === 'id') {
                    continue;
                }
                if ($k === 'children') {
                    $component->addContext('subcategories', $context);
                    continue;
                }
                $component->addContext($k, $context);
            }
            $component->setParent($this);
            $results[$key] = $component;
        }
        return $this->containerize($results);
    }

    protected function filterSubcategories(array $subcategories): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $results = [];


        foreach ($subcategories as $key => $subcategory) {
            if (in_array($key, ['id', 'parent', 'context', 'category'])) {
                continue;
            }
            if ($subcategory->has('children')) {
                $component = $this->getDocumentationSidebarMultiSubcategoryComponent($key, $subcategory);
            } else {
                $component = $this->getDocumentationSidebarSubcategoryComponent($key, $subcategory);
            }
            $results[$key] = $component;
        }

//
//        if ($this instanceof \Oroboros\coreDocs\library\component\html\DocumentationSidebarMultiSubcategory) {
//
//
//            d('result subcategories...', get_class($this), $results);
//        }


        return $this->containerize($results);
    }

    private function getDocumentationSidebarSubcategoryComponent(string $key, \Oroboros\core\interfaces\library\container\ContainerInterface $details)
    {
        $id = sprintf('%1$s-subcategory-%2$s', $this->getContext('id'), $key);
        $component = $this->getComponent('documentation-sidebar-subcategory', $id, [], $this->getFlags());
        $component->addContext('id', $id);
        $component->addContext('tagname', 'li');
        $component->addContext('class', 'list-group-item');
        $component->setParent($this);
        foreach ($details as $k => $context) {
            $component->addContext($k, $context);
        }
        return $component;
    }

    private function getDocumentationSidebarMultiSubcategoryComponent(string $key, \Oroboros\core\interfaces\library\container\ContainerInterface $details)
    {
        $id = sprintf('%1$s-subcategory-%2$s', $this->getContext('id'), $key);
        $component = $this->getComponent('documentation-sidebar-subcategory-multi', $id, [], $this->getFlags());
        $component->addContext('id', $id);
        $component->addContext('tagname', 'li');
        $component->addContext('class', 'list-group-item');
        $component->setParent($this);
        foreach ($details as $k => $context) {
            if ($k === 'children') {
//                d('setting children...', get_class($this), get_class($component), $context->toArray());
                $component->addContext('subcategories', $context);
                continue;
            }
            $component->addContext($k, $context);
        }
//        d(get_class($this), $key, get_class($component), $component->render());
        return $component;
    }

    private function getTopicComponent(string $key, \Oroboros\core\interfaces\library\container\ContainerInterface $data): \Oroboros\coreDocs\library\component\html\DocumentationSidebarTopic
    {
        $component = $this->getComponent('documentation-sidebar-topic', $key, [], $this->getFlags());
        $component->addContext('id', sprintf('documentation-sidebar-menu-topic-%1$s', $key));
        $component->addContext('tagname', 'li');
        $component->addContext('class', 'list-group-item p-0');
        foreach ($data as $key => $value) {
            if ($key === 'id') {
                continue;
            }
            if ($key === 'children') {
                $component->addContext('categories', $value);
                continue;
            }
            $component->addContext($key, $value);
        }
        $component->setParent($this);
        return $component;
    }
}
