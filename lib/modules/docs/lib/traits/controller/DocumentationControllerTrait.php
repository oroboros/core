<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\coreDocs\traits\controller;

/**
 * Documentation Controller Trait
 * Provides common methods to documentation controllers
 *
 * @author Brian Dayhoff
 */
trait DocumentationControllerTrait
{

    use \Oroboros\core\traits\html\HtmlModuleControllerUtilityTrait {
        __construct as private traitConstruct;
    }

    /**
     * Contains all detected documentation
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private static $documentation = null;
    
    /**
     * The documentation definer object that provides link and file mappings
     * 
     * @var \Oroboros\coreDocs\interfaces\library\definer\DocumentationDefinerInterface
     */
    private static $definer = null;
    
    /**
     * Default argument set
     * @var array
     */
    private $defaults = [
        'category' => 'index',
        'subcategory' => 'index',
        'article' => 'index',
        'sub1' => 'index',
        'sub2' => 'index',
        'sub3' => 'index',
        'sub4' => 'index',
        'sub5' => 'index',
        'sub6' => 'index',
        'sub7' => 'index',
        'sub8' => 'index',
        'sub9' => 'index',
        'sub10' => 'index',
    ];

    /**
     * Standard constructor format
     * 
     * @param string $command
     * @param array $arguments
     * @param array $flags
     */
    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        $this->traitConstruct($command, $arguments, $flags);
        $this->initializeDocumentation();
    }

    /**
     * Landing method for all documentation requests.
     * 
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $args
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $flags
     * @return \Oroboros\core\interfaces\controller\ControllerInterface
     */
    public function index(\Oroboros\core\interfaces\library\container\ContainerInterface $args = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\ControllerInterface
    {
        if ($this::app()->getApplicationRoot() !== OROBOROS_CORE_BASE_DIRECTORY) {
            // serve the app docs
            $category = 'app';
        } else {
            // serve the core docs
            $category = 'core';
        }
        return $this->categoryIndex($this::containerize(['category' => $category]));
    }

    /**
     * Returns the article requested with formatted request args.
     * 
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $args
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $flags
     * @return \Oroboros\core\interfaces\controller\ControllerInterface
     */
    public function categoryIndex(\Oroboros\core\interfaces\library\container\ContainerInterface $args = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\ControllerInterface
    {
        $args = $this->formatRequestArgs($args);
        return $this->article($args, $flags);
    }

    /**
     * 
     * @param string $section
     * @param string $path
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    protected function getDocumentation(string $section, string $path): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        if (!self::$documentation->has($section)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Provided documentation section [%2$s] does not exist.', get_class($this), $section));
        }
        if (!self::$documentation[$section]->has($path)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Provided documentation index [%2$s] does not exist in section [%3$s].', get_class($this), $path, $section));
        }
        return self::$documentation[$section][$path];
    }

    /**
     * Looks up the article requested within the documentation data set
     * 
     * @param array $indexes
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    protected function lookupDocumentation(array $indexes): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $docs = self::$documentation;
        foreach ($indexes as $index) {
            if ($index === 'index' && $docs->has('index.md')) {
                $index = 'index.md';
            } elseif($index === 'index' && !$docs->has('index.md')) {
                break;
            } elseif($docs->has(sprintf('%1$s.md', $index))) {
                $index = sprintf('%1$s.md', $index);
            }
            if (!is_object($docs)) {
                throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Provided documentation index [%2$s] could not be found because the depth level was already reached for this topic.', get_class($this), $index));
            }
            if (!$docs->has($index) && $index === 'cli' && $docs->has('bash')) {
                $index = 'bash';
            }
            if (!$docs->has($index)) {
                throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Provided documentation index [%2$s] could not be found.', get_class($this), $index));
            }
            $docs = $docs[$index];
        }
        return $docs;
    }

    /**
     *  Generates the full dataset for a documentation menu
     * 
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    protected function generateDocumentationMenu(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $menu = [];
        $base = $this::HOSTNAME . '/';
        foreach (self::$documentation as $key => $value) {
            $menu[$key] = $this->generateDocumentationLink($base, $key, $value);
        }
        return $this->containerize($menu);
    }

    /**
     * Returns the compiled documentation definer object
     * 
     * @return \Oroboros\coreDocs\interfaces\library\definer\DocumentationDefinerInterface
     */
    protected function getDocumentationDefiner(): \Oroboros\coreDocs\interfaces\library\definer\DocumentationDefinerInterface
    {
        $this->initializeDocumentationDefiner();
        return self::$definer;
    }
    
    /**
     * Returns a container of the generated documentation content based on the given categories
     * 
     * @param array $categories
     * @return mixed
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    private function getDocumentationContent(array $categories)
    {
        try {
            $documentation = $this->lookupDocumentation($categories);
        } catch (\Oroboros\core\exception\InvalidArgumentException $e) {
            try {
                $categories['category'] = 'api';
                if ($categories['subcategory'] === 'usage') {
                    unset($categories['subcategory']);
                }
                $documentation = $this->lookupDocumentation($categories);
            } catch (\Oroboros\core\exception\InvalidArgumentException $e2) {
                throw $e;
            }
        }
        return $this->load('library', sprintf('parser\\%1$s\\MarkdownParser', static::CLASS_SCOPE), $documentation['file'])->fetch()['output'];
    }

    /**
     * Fetches the documentation menu component for the documentation menu sidebar
     * 
     * @return \Oroboros\core\interfaces\library\component\ComponentInterface
     */
    private function getDocumentationMenu(): \Oroboros\core\interfaces\library\component\ComponentInterface
    {
        $component = $this->getComponent('documentation-sidebar-menu', 'documentation-menu', [
            'id' => 'documentation-menu',
            'class' => 'sidebar-menu documentation-menu',
            'topics' => $this->generateDocumentationMenu()
        ]);
        return $component;
    }

    /**
     * Generates menu-component compatible data structure
     * from the documentation data set
     * 
     * @param string $uri_base
     * @param string $section
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $content
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private function generateDocumentationLink(string $uri_base, string $section, \Oroboros\core\interfaces\library\container\ContainerInterface $content): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $scoped = $content;
        if ($content->has('index.md') && $content['index.md']) {
            $scoped = $content['index.md'];
        } elseif ($content->has(sprintf('%1$s.md', $section))) {
            $scoped = $content[sprintf('%1$s.md', $section)];
        }
        $link = [
            'title' => $scoped['name'],
            'href' => sprintf('%1$s%2$s', $uri_base, ltrim($scoped['link'], '/')),
            'icon' => 'far fa-file'
        ];
        if ($scoped->has('icon')) {
            $link['icon'] = $scoped['icon'];
        }
        if ($scoped !== $content) {
            $children = [];
            foreach ($content as $key => $value) {
                if ($value === $scoped) {
                    continue;
                }
                if ($value->has('slug') && $value->has('link')) {
                    $children[$value['slug']] = $this->generateDocumentationLink($uri_base, $value['slug'], $value);
                    continue;
                }
                $children[$key] = $this->generateDocumentationLink($uri_base, $key, $value);
            }
            if (!empty($children)) {
                $link['children'] = $this->containerize($children);
            }
        }
        return $this->containerize($link);
    }

    /**
     * Formats the arguments passed to the controller to resolve against
     * the documentation set
     * 
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $args
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private function formatRequestArgs(\Oroboros\core\interfaces\library\container\ContainerInterface $args = null): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        if (is_null($args)) {
            $args = $this->containerize();
        }
        $output = array_replace($this->defaults, $args->toArray());
        $unset = false;
        foreach ($output as $key => $value) {
            if ($unset) {
                unset($output[$key]);
                continue;
            }
            if ($value === 'index') {
                $unset = true;
            }
            $output[$key] = $value;
        }
        return $this->containerize($output);
    }

    /**
     * initializes the internal documentation store if not already done
     * 
     * @return void
     */
    private function initializeDocumentation(): void
    {
        $this->initializeDocumentationDefiner();
        if (is_null(self::$documentation)) {
            self::$documentation = $this->getDocumentationDefiner()->fetch();
        }
    }

    /**
     * Initializes the definer object if not already done
     * 
     * @return void
     */
    private function initializeDocumentationDefiner(): void
    {
        if (!is_null(self::$definer)) {
            return;
        }
        self::$definer = $this->load('library', 'definer\\DocumentationDefiner')->compile();
    }
}
