<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\coreDocs\controller\html;

/**
 * Documentation Controller
 * Provides Html documentation rendering as pages
 *
 * @author Brian Dayhoff
 */
final class DocumentationController extends \Oroboros\core\abstracts\controller\AbstractPageController implements \Oroboros\core\interfaces\CoreInterface
{

    /**
     * For documentation data import
     */
    use \Oroboros\coreDocs\traits\controller\DocumentationControllerTrait;

/**
     * For generating page content from the documentation definer
     */
    use \Oroboros\core\traits\html\DefinerPageContentUtility {
        \Oroboros\core\traits\html\DefinerPageContentUtility::setupPage as private traitSetupPage;
    }

    /**
     * Use the standard html error controller
     */
    const ERROR_CONTROLLER = 'html\\ErrorController';

    /**
     * Use the standard documentation layout,
     * or a custom one if one is registered.
     */
    const LAYOUT_CLASS = 'docs\\DocumentationLayout';

    /**
     * Scope output to general html
     */
    const CLASS_SCOPE = 'html';

    /**
     * Identify the controller as belonging to the docs module
     */
    const MODULE_NAME = 'docs';

    /**
     * Serves the requested article if it is found, otherwise 404's the page
     * 
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $args
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $flags
     * @return \Oroboros\core\interfaces\controller\ControllerInterface
     */
    public function article(\Oroboros\core\interfaces\library\container\ContainerInterface $args, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null)
    {
        try {
            $this->addPageId('documentation-page');
            $this->setupPage('index', $args->toArray(), $flags);
            $this->addScript('footer', 'hilight.js', 'hilight.js');
            $this->addScript('footer', 'documentation-page', 'documentation-page');
            $this->addStylesheet('stylesheet', 'hilight.js', 'hilight.js');
            return $this->render('html', 'documentation/index');
        } catch (\InvalidArgumentException $e) {
            // Page not found
            return $this->error(404, $e->getMessage(), null, $flags);
        } catch (\Exception $e) {
            // Server error
            return $this->error(500, $e->getMessage(), null, $flags);
        }
    }

    /**
     * Sets up the documentation page
     * @param string $section
     * @param array $categories
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $flags
     */
    protected function setupPage(string $section, array $categories, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null)
    {
        $this->traitSetupPage('documentation-page');
        $content = $this->content;
        foreach ($this->getContentSections() as $section) {
            $this->getDocumentationPageDetails($section, $categories, $content);
        }
        $this->addStylesheet('stylesheet', 'documentation', 'documentation');
        $this->addStylesheet('stylesheet', 'github-markdown', 'github-markdown');
    }

    /**
     * Fetches the definer designated to handle the specified menu
     * 
     * @param string $section
     * @param string $subsection
     * @param array $arguments
     * @return \Oroboros\core\interfaces\library\menu\MenuDefinerInterface
     */
    protected function getMenuDefiner(string $section = null, string $subsection = null, array $arguments = null): \Oroboros\core\interfaces\library\menu\MenuDefinerInterface
    {
        if (is_null($arguments)) {
            $arguments = [];
        }
        $section = $this->brutecase(str_replace('-', ' ', $section));
        $path = trim(sprintf('menu\\frontend\\%1$s', sprintf('%1$sDefiner', ucfirst($section))), '\\');
        $definer = $this->load('library', $path, $subsection, $this->getArguments(), $this->getFlags());
        return $definer;
    }

    /**
     * Packages the documentation page data into the layout
     * 
     * @param string $section
     * @param array $categories
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $content
     * @return void
     */
    private function getDocumentationPageDetails(string $section, array $categories, \Oroboros\core\interfaces\library\container\ContainerInterface $content): void
    {
        if (!$content['body']['content']->has($section)) {
            return;
        }
        if ($section === 'left-sidebar') {
            $content['body']['content'][$section]['content'] = $this->containerize(['documentation-menu' => $this->getDocumentationMenu()]);
        }
        if ($section === 'main') {
            $content['body']['content'][$section]['content'] = [
                'component' => 'documentation-content',
                'content' => $this->getDocumentationContent($categories)
            ];
        }
        foreach ($content['body']['content'][$section] as $key => $value) {
            if (is_array($value)) {
                $value = $this->containerize($value);
            }
            if (!is_string($value) && $value->has('component')) {
                $value = $this->getComponent($value['component'], $key, $value->toArray());
            }
            $this->addContent($section, $key, $value);
        }
    }
}
