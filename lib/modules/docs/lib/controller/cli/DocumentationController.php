<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\coreDocs\controller\cli;

/**
 * Command Line Build Controller
 * Provides build utilities for the command line
 *
 * @author Brian Dayhoff
 */
final class DocumentationController extends \Oroboros\core\abstracts\controller\AbstractCliController implements \Oroboros\core\interfaces\CoreInterface
{

    /**
     * For documentation data import
     */
    use \Oroboros\coreDocs\traits\controller\DocumentationControllerTrait;

    /**
     * Use the standard cli error controller
     */
    const ERROR_CONTROLLER = 'cli\\ErrorController';

    /**
     * Identify the controller as belonging to the docs module
     */
    const MODULE_NAME = 'docs';


    public function article(\Oroboros\core\interfaces\library\container\ContainerInterface $args, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null)
    {
        if (is_null($flags)) {
            $flags = $this->containerize();
        }
        try {
            $view = $this->getView('cli', null, $flags->toArray());
            $content = $this->getDocumentationContent($this->formatRequestArgs($args)->toArray());
            $view->clear();
            $view->raw($content);
        } catch (\InvalidArgumentException $e) {
            $args = $args->toArray();
            $key = 'index';
            while ($key === 'index') {
                $key = array_pop($args);
            }
            $view->debug(sprintf('[Handled Exception Message]: %1$s', $e->getMessage()));
            $view->debug(sprintf('[Exception Thrown At]: line [%1$s] of file [%2$s] with code: [%3$s].', $e->getLine(), $e->getFile(), $e->getCode()));
            return $this->error(404, sprintf('Provided documentation index [%1$s] could not be found.', $key), null, $flags);
        }
        return $this;
    }
}
