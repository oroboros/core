# Oroboros Documentation Module

This module handles parsing and displaying documentation in the browser and on the command line.

This is a core module, distributed with every installation. It may be disabled in applications built on the platform if desired.