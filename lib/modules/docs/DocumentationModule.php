<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\coreDocs;

/**
 * Oroboros Documentation Module
 * Provides support for reading documentation.
 *
 * @author Brian Dayhoff
 */
final class DocumentationModule extends \Oroboros\core\abstracts\module\AbstractModule
{

    use \Oroboros\core\traits\pluggable\MenuSupplierModuleTrait;

    private static $valid_menus = [
        'html:menu' => 'filterPrimaryMenu',
//        'html:ui-menu' => 'filterUiMenu',
        'frontend:menu' => 'filterPrimaryMenu',
//        'frontend:ui-menu' => 'filterUiMenu',
        'login:menu' => 'filterPrimaryMenu',
//        'login:ui-menu' => 'filterUiMenu',
        'maintenance:menu' => 'filterPrimaryMenu',
//        'maintenance:ui-menu' => 'filterUiMenu',
    ];

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
    }

    private function initializeMenus(): void
    {
        if (is_null(self::$menu_details)) {
            $definer = new \Oroboros\coreDocs\library\definer\DocumentationDefiner();
            $definer->compile();
            $data = $definer->fetch();
            $menus = $this->containerize([
                'frontend' => [
                    'menu' => [
                        'id' => 'documentation-link',
                        'title' => 'Documentation',
                        'icon' => 'fas fa-book-open',
                        'href' => '/documentation/',
                        'children' => $this->filterDocumentationMenuChildren($data)
                    ],
                    'ui-menu' => [],
                ],
                'dashboard' => [],
                'ui-menu' => [],
            ]);
            self:: $menu_details = $menus;
        }
    }

    private function filterDocumentationMenuChildren($item)
    {
        $expected = \Oroboros\core\interfaces\library\container\ContainerInterface::class;
        if (!is_iterable($item)) {
            return $item;
        }
        if (is_array($item)) {
            $item = $this->containerize($item);
        }
        if (is_iterable($item)) {
            foreach ($item as $key => $value) {
                $item[$key] = $this->filterDocumentationMenuItem($key, $value);
            }
        }
        return $item;
    }

    private function filterDocumentationMenuItem(string $key, \Oroboros\core\interfaces\library\container\ContainerInterface $item = null)
    {
        if (is_null($item)) {
            return null;
        }
        $expected = \Oroboros\core\interfaces\library\container\ContainerInterface::class;
        $class = get_class($item);
        $results = [];
        $children = [];
        if ($item->has('index.md')) {
            $results['title'] = $item['index.md']['name'];
            $results['href'] = $item['index.md']['link'];
        }
        foreach ($item as $k => $value) {
            if ($k === 'index.md') {
                continue;
            }
            if (is_object($value) && ($value instanceof $expected)) {
                $id = $k;
                if ($value->has('slug')) {
                    $id = $value['slug'];
                }
                $children[$k] = $this->filterDocumentationMenuItem($id, $value);
                continue;
            }
            if ($k === 'name') {
                $results['title'] = $value;
            }
            if ($k === 'link') {
                $results['href'] = $value;
            }
        }
        if ($item->has('name') && !$item->has('title')) {
            $results['title'] = $item['name'];
            unset($item['name']);
        }
        if ($item->has('link') && !$item->has('href')) {
            $results['href'] = $item['link'];
        }
        if (!$item->has('icon')) {
            $results['icon'] = $this->getDocumentationMenuIcon($key);
        }
        if (!empty($children)) {
            $results['children'] = $class::init(null, $children);
        }
        return $class::init(null, $results);
    }

    private function getDocumentationMenuIcon(string $key)
    {
        switch ($key) {
            case 'core':
                return 'fas fa-server';
            case 'modules':
            case 'module':
                return 'fas fa-box';
            case 'extensions':
            case 'extension':
            case 'adapter':
                return 'fas fa-plug';
            case 'services':
            case 'service':
                return 'fas fa-network-wired';
            case 'api':
                return 'fas fa-laptop-code';
            case 'architecture':
                return 'fas fa-project-diagram';
            case 'installation':
                return 'fas fa-laptop-code';
            case 'configuration':
                return 'fas fa-tools';
            case 'filesystem':
                return 'fas fa-folder';
            case 'troubleshooting':
                return 'fas fa-medkit';
            case 'usage':
                return 'fas fa-info-circle';
            case 'php':
                return 'fab fa-php';
            case 'bash':
            case 'cli':
            case 'shell':
            case 'command-line':
                return 'fas fa-terminal';
            case 'web':
            case 'html':
            case 'http':
            case 'rest':
            case 'browser':
            case 'controller':
            case 'bootstrap':
            case 'model':
            case 'library':
            case 'trait':
            case 'interface':
                return 'far fa-file-code';
            case 'socket':
            case 'websocket':
                return 'fas fa-link';
            case 'javascript':
                return 'fab fa-js-square';
            case 'sass':
            case 'scss':
                return 'fab fa-sass';
            case 'design':
            case 'templating':
            case 'template':
            case 'layout':
                return 'far fa-object-group';
            case 'theme':
                return 'fas fa-palette';
            case 'environment':
                return 'fas fa-leaf';
            case 'beanstalk':
                return 'fas fa-retweet';
            case 'database':
            case 'mysql':
            case 'postgres':
            case 'mssql':
            case 'oracle':
                return 'fas fa-database';
            case 'redis':
                return 'far fa-list-alt';
            case 'supervisor':
                return 'fas fa-sitemap';
            case 'app':
            case 'view':
            case 'page':
                return 'fas fa-tablet-alt';
            case 'pluggables':
            case 'component':
                return 'fas fa-cube';
            default:
                return 'fas fa-file';
        }
    }
}
