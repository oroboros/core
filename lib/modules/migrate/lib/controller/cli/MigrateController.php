<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\coreMigrate\controller\cli;

/**
 * Command Line Migrate Controller
 * Provides migration utilities for the command line
 *
 * @author Brian Dayhoff
 */
final class MigrateController extends \Oroboros\core\abstracts\controller\AbstractCliController implements \Oroboros\core\interfaces\CoreInterface
{

    use \Oroboros\coreMigrate\traits\controller\MigrateControllerTrait;

    /**
     * Use the standard cli error controller
     */
    const ERROR_CONTROLLER = 'cli\\ErrorController';
    const MODULE_NAME = 'migrate';

    /**
     * Base initialization methods required for controller setup, if any.
     * @var array
     */
    private $setup = [];

    public function index()
    {
        $this->success_message('You have successfully loaded [the migration controller].');
        exit(0);
    }

    public function database()
    {
        $this->info_message('Running database migrations.');
        exit(0);
    }

    public function cache()
    {
        $this->info_message('Running cache migrations.');
        exit(0);
    }

    public function services()
    {
        $this->info_message('Running service migrations.');
        exit(0);
    }

    public function jobs()
    {
        $this->info_message('Running job migrations.');
        exit(0);
    }

    public function routes()
    {
        $this->info_message('Running routing migrations.');
        exit(0);
    }

    public function permissions()
    {
        $this->info_message('Running permissions migrations.');
        exit(0);
    }

    public function extensions()
    {
        $this->info_message('Running extension migrations.');
        exit(0);
    }

    public function modules()
    {
        $this->info_message('Running module migrations.');
        exit(0);
    }

    public function scripts()
    {
        $this->info_message('Running javascript migrations.');
        exit(0);
    }

    public function stylesheets()
    {
        $this->info_message('Running stylesheet migrations.');
        exit(0);
    }

    public function fonts()
    {
        $this->info_message('Running font migrations.');
        exit(0);
    }

    public function media()
    {
        $this->info_message('Running media migrations.');
        exit(0);
    }
}
