<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\coreSetup\library\layout\html\setup;

/**
 * Setup Layout
 * This is the default layout for system setup.
 *
 * @author Brian Dayhoff
 */
final class SetupLayout extends \Oroboros\core\abstracts\library\layout\AbstractLayout implements \Oroboros\core\interfaces\CoreInterface
{
    use \Oroboros\core\traits\pluggable\ModuleLibraryTrait;

    const MODULE_NAME = 'setup';
    
    private $layout_sections = [
        'header' => [
            'component' => 'html\\SetupHeader',
            'attributes' => [
                'id' => 'setup-header',
                'class' => 'd-block order-first sticky-top'
            ]
        ],
//        'right-sidebar' => [
//            'component' => 'html\\Sidebar',
//            'attributes' => [
//                'id' => 'sidebar-right',
//                'class' => 'h-100 mx-0 px-0'
//            ]
//        ],
//        'left-sidebar' => [
//            'component' => 'html\\Sidebar',
//            'attributes' => [
//                'id' => 'sidebar-left',
//                'class' => 'h-100 mx-0 px-0'
//            ]
//        ],
        'main' => [
            'component' => 'html\\SetupContent',
            'attributes' => [
                'id' => 'setup-content',
                'class' => 'd-flex mx-0 px-0'
            ]
        ],
        'footer' => [
            'component' => 'html\\Footer',
            'attributes' => [
                'id' => 'page-footer',
                'class' => 'd-flex order-last mx-0 px-0 w-100 fixed-bottom'
            ]
        ],
    ];
    
    /**
     * Layout sections only enabled when debug mode is active
     * @var array
     */
    private $layout_debug_sections = [
        'debug' => [
            'component' => self::DEBUG_COMPONENT,
            'attributes' => [
                'id' => self::DEBUG_COMPONENT,
                'class' => 'd-flex row d-block m-0 p-0'
            ],
        ],
        'error' => [
            'component' => self::ERROR_COMPONENT,
            'attributes' => [
                'id' => self::ERROR_COMPONENT,
                'class' => 'd-flex row d-block m-0 p-0'
            ],
        ]
    ];

    protected function declareLayoutBase(): string
    {
        return 'html/layout/setup';
    }

    protected function declareSections(): array
    {
        if ($this::app()->debugEnabled()) {
            return array_merge_recursive($this->layout_sections, $this->layout_debug_sections);
        }
        return $this->layout_sections;
    }
}
