<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\coreSetup\library\component\html;

/**
 * Global Setup Header
 * Represents a component wrapper for a Bootstrap responsive header
 * @author Brian Dayhoff
 */
final class SetupHeader extends \Oroboros\bootstrap\abstracts\library\component\html\AbstractBootstrapComponent implements \Oroboros\core\interfaces\CoreInterface
{
    use \Oroboros\core\traits\pluggable\ModuleLibraryTrait;

    const MODULE_NAME = 'setup';
    const COMPONENT_TEMPLATE = 'component/header/header';
    const COMPONENT_KEY = 'setup-header';

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
    }

    public function addContext(string $key, $value): \Oroboros\core\interfaces\library\component\ComponentInterface
    {
        return parent::addContext($key, $value);
    }
}
