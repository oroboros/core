<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\coreSetup\controller\html;

/**
 * Scaffold Controller
 * Provides setup utilities for the browser
 * to verify and install the system
 *
 * @author Brian Dayhoff
 */
final class SetupController extends \Oroboros\core\abstracts\controller\AbstractPageController implements \Oroboros\core\interfaces\CoreInterface
{

    use \Oroboros\coreSetup\traits\controller\SetupControllerTrait;

    /**
     * Use the standard html error controller
     */
    const ERROR_CONTROLLER = 'html\\SetupErrorController';
    const LAYOUT_CLASS = 'setup\\SetupLayout';
    const CLASS_SCOPE = 'html';
    const MODULE_NAME = 'setup';

    public function index()
    {
        try {
            $this->setupPage('index');
        } catch (\Exception $e) {
            return $this->error(500, $e->getMessage());
        }
        return $this->render('html', 'setup/index');
    }

    public function setup(\Oroboros\core\interfaces\library\container\ContainerInterface $args, \Oroboros\core\interfaces\library\container\ContainerInterface $flags)
    {
        try {
            $section = $args['section'];
            $this->setupPage($section);
            return $this->render('html', 'setup/' . $section);
        } catch (\InvalidArgumentException $e) {
//            ini_set('memory_limit', '1G'); d($this, $section, $e->getMessage()); exit;
            return $this->error(404, $e->getMessage());
        } catch (\Exception $e) {
            return $this->error(500, $e->getMessage());
        }
    }

    protected function setupPage(string $section)
    {
        $content = $this->getSectionPageContent($section);
        $this->addPageMeta($section);
        $this->addPageAssets($section);
        foreach ($this->getContentSections() as $section) {
            if ($content['body']->has($section)) {
                foreach ($content['body'][$section] as $key => $value) {
                    if (is_array($value)) {
                        $value = $this->containerize($value);
                    }
                    if ($value->has('component')) {
                        $value = $this->getComponent($value['component'], $key, $value->toArray());
                    }
                    $this->addContent($section, $key, $value);
                }
            }
        }
    }
}
