<?php
namespace Oroboros\coreSetup\controller\html;

/**
 * Default Http Error Controller
 * Provides error handling for html requests
 *
 * @author Brian Dayhoff
 */
final class SetupErrorController extends \Oroboros\core\abstracts\controller\AbstractErrorPageController implements \Oroboros\core\interfaces\CoreInterface
{

    const CLASS_SCOPE = 'html';
    const ERROR_CONTROLLER = __CLASS__;
    const MODULE_NAME = 'setup';

    public function error(int $code = null, string $message = null, \Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\ErrorControllerInterface
    {
        switch ($code) {
            case 401:
                break;
            case 403:
                break;
            case 404:
                break;
            case 500:
            default:
                $code = 500;
                break;
        }
        http_response_code($code);
        $layout_class = 'html\\error\\Error' . $code . 'Layout';
        $this->initializeLayout($layout_class);
        if ($this::app()->debugEnabled() && array_key_exists('debug', $arguments)) {
            ini_set('memory_limit', '1G');
            d($arguments, $layout_class, $this->getLayout());
        }
        exit;
    }
}
