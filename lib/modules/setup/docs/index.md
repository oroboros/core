# Oroboros Setup Module Documentation {#title}

- [Overview](#overview)

## Overview {#overview}

This module handles system installation and configuration for the core system and apps. It is used to configure connections to databases, caches, manage modules, extensions, services, and help ease configuration of your application to get it running quickly and manage application lifecycle easily.

This module provides both a [web api][1] and a [command line api][2]

[1]: /documentation/modules/setup/web/
[2]: /documentation/modules/setup/cli/