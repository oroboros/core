# Oroboros Setup Module Web Api Documentation {#title}

- [Overview](#overview)

## Overview {#overview}

This section outlines using the web-based setup utility.

If you are looking for command line setup instructions, you may find them [here][1].

[1]: /documentation/modules/setup/cli/