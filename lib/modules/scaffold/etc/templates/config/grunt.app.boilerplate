{
    "init": {
        "build": "<%= new Date().getTime() %>",
        "product": "<%= _.capitalize(pkg.name) %>",
        "copyright": "Copyright (c) <%= grunt.template.today('yyyy') %> <%= pkg.author.name %> <<%= pkg.author.email %>>.",
        "php": {
            "dist": {
                "options": {
                    "port": 8888
                }
            },
            "test": {
                "options": {
                    "keepalive": true,
                    "open": true
                }
            }
        },
        "composer": {
            "install": {
                "dev": {
                    "options": {
                        "usePhp": true,
                        "flags": [],
                        "composerLocation": "/usr/bin/composer"
                    }
                },
                "production": {
                    "options": {
                        "usePhp": true,
                        "flags": ["no-dev"],
                        "composerLocation": "/usr/bin/composer"
                    }
                }
            },
            "update": {
                "dev": {
                    "options": {
                        "usePhp": true,
                        "flags": [],
                        "composerLocation": "/usr/bin/composer"
                    }
                },
                "production": {
                    "options": {
                        "usePhp": true,
                        "flags": ["no-dev"],
                        "composerLocation": "/usr/bin/composer"
                    }
                }
            }
        },
        "phpunit": {
            "classes": {
                "dir": "tests/phpunit/"
            },
            "options": {
                "bin": "vendor/bin/phpunit",
                "configuration": "tests/phpunit/phpunit.xml"
            }
        },
        "eslint": {
            "browserFiles": {
                "src": ["public/js/**/*.js", "!public/js/vendors/**/*"],
                "options": {
                    "config": "tests/eslint/eslint.json",
                    "rulesdir": ["tests/eslint"]
                }
            }
        },
        "apidoc": {
            "core": {
                "src": "lib/",
                "dest": "public/html/api/"
            }
        },
        "mocha": {
            "test": {
                "options": {
                    "reporter": "XUnit"
                },
                "src": ["tests/mocha/**/*.html"],
                "dest": "tests/coverage/mocha/xunit.out"
            }
        },
        "mkdir": {
            "init": {
                "cache": {
                    "options": {
                        "mode": "0770",
                        "create": ["cache/sass"]
                    }
                },
                "twig": {
                    "options": {
                        "mode": "0770",
                        "create": ["cache/twig"]
                    }
                },
                "phpunit": {
                    "options": {
                        "mode": "0770",
                        "create": ["cache/phpunit"]
                    }
                }
            },
            "build": {},
            "deploy": {}
        },
        "clean": {
            "build": {
                "src": [
                    "etc/build/**",
                    "!etc/build/.gitignore"
                ]
            },
            "cache": {
                "src": [
                    ".sass_cache",
                    "cache/**/*",
                    "!cache/.htaccess",
                    "!cache/.gitignore"
                ]
            },
            "composer": {
                "src": [
                    "vendor/*",
                    "!vendor/.gitignore",
                    "!vendor/.htaccess"
                ]
            },
            "css": {
                "src": [
                    "public/css/*",
                    "!public/css/.gitignore"
                ]
            },
            "js": {
                "src": [
                    "public/js/**",
                    "!public/js/.gitignore"
                ]
            },
            "font": {
                "src": [
                    "public/font/**",
                    "!public/font/.gitignore",
                    "public/css/font/*"
                ],
                "folder": ["public/css/font/"]
            },
            "uploads": {
                "src": [
                    "public/uploads/*",
                    "!public/uploads/.gitignore",
                    "!public/uploads/.htaccess"
                ]
            },
            "media": {
                "src": [
                    "public/media/**",
                    "!public/media/.gitignore"
                ]
            },
            "html": {
                "src": [
                    "public/html/**",
                    "!public/html/.gitignore"
                ]
            },
            "postbuild": {
                "css": {
                    "src": [
                        "public/css/vendors/"
                    ],
                    "folder": ["public/css/vendors"]
                },
                "js": {
                    "src": [
                        "public/js/vendors/"
                    ],
                    "folder": ["public/js/vendors"]
                }
            },
            "test": {
                "src": [
                    "tests/coverage/phpunit/**",
                    "!tests/coverage/phpunit/.gitignore",
                    "tests/coverage/mocha/**",
                    "!tests/coverage/mocha/.gitignore",
                    "tests/coverage/mocha/**",
                    "!tests/coverage/mocha/.gitignore"
                ]
            }
        },
        "googlefonts": {
            "build": {
                "options": {
                    "fontPath": "../font/",
                    "cssFile": "etc/src/scss/fonts/_fonts.scss",
                    "formats": {
                        "eot": true,
                        "woff2": true,
                        "woff": true,
                        "svg": true
                    },
                    "fonts": [
                        {
                            "family": "Open Sans",
                            "subsets": [
                                "latin",
                                "cyrillic"
                            ],
                            "styles": [
                                300, 400, 700
                            ]
                        },
                        {
                            "family": "Droid Sans",
                            "styles": [
                                400, 700
                            ]
                        },
                        {
                            "family": "Lato",
                            "styles": [
                                300,
                                400
                            ]
                        }
                    ]
                }
            }
        },
        "cssmin": {
            "target": {
                "files": [{
                        "expand": true,
                        "cwd": "public/css",
                        "src": ["*.css", "!*.min.css"],
                        "dest": "public/css",
                        "ext": ".min.css"
                    }]
            }
        },
        "copy": {
            "build": {
                "files": [
                    {
                        "expand": true,
                        "cwd": "etc/src/media",
                        "src": "**",
                        "dest": "public/media/"
                    },
                    {
                        "expand": true,
                        "cwd": "etc/src/js",
                        "src": "**",
                        "dest": "public/js/"
                    },
                    {
                        "nonull": true,
                        "expand": true,
                        "timestamp": true,
                        "cwd": "node_modules/jquery/dist",
                        "src": ["jquery.min.js", "jquery.min.map"],
                        "dest": "public/js/vendors/jquery"
                    },
                    {
                        "nonull": true,
                        "expand": true,
                        "timestamp": true,
                        "cwd": "node_modules/jquery.easing",
                        "src": ["jquery.easing.compatibility.js", "jquery.easing.min.js"],
                        "dest": "public/js/vendors/jquery-easing"
                    },
                    {
                        "nonull": true,
                        "expand": true,
                        "timestamp": true,
                        "cwd": "node_modules/@popperjs/core/dist/umd",
                        "src": ["popper.min.js", "popper.min.js.map"],
                        "dest": "public/js/vendors/popper"
                    },
                    {
                        "nonull": true,
                        "expand": true,
                        "timestamp": true,
                        "cwd": "node_modules/bootstrap/dist/js",
                        "src": "**",
                        "dest": "public/js/vendors/bootstrap"
                    },
                    {
                        "nonull": true,
                        "expand": true,
                        "timestamp": true,
                        "cwd": "node_modules/mdb-ui-kit/js",
                        "src": "**",
                        "dest": "public/js/vendors/mdbootstrap"
                    },
                    {
                        "nonull": true,
                        "expand": true,
                        "timestamp": true,
                        "cwd": "node_modules/datatables/media/js",
                        "src": ["jquery.dataTables.min.js"],
                        "dest": "public/js/vendors/datatables"
                    },
                    {
                        "nonull": true,
                        "expand": true,
                        "timestamp": true,
                        "cwd": "node_modules/datatables/media/css",
                        "src": ["jquery.dataTables.min.css"],
                        "dest": "public/css/vendors/datatables"
                    },
                    {
                        "nonull": true,
                        "expand": true,
                        "timestamp": true,
                        "cwd": "node_modules/datatables.net-bs5/css",
                        "src": ["dataTables.bootstrap5.min.css"],
                        "dest": "public/css/vendors/datatables"
                    },
                    {
                        "nonull": true,
                        "expand": true,
                        "timestamp": true,
                        "cwd": "node_modules/datatables.net-bs5/js",
                        "src": ["dataTables.bootstrap5.min.js"],
                        "dest": "public/js/vendors/datatables"
                    },
                    {
                        "nonull": true,
                        "expand": true,
                        "timestamp": true,
                        "cwd": "node_modules/datatables.net-bs5/images",
                        "src": ["*.png"],
                        "dest": "public/css/vendors/images"
                    },
                    {
                        "nonull": true,
                        "expand": true,
                        "timestamp": true,
                        "cwd": "node_modules/@fortawesome/fontawesome-free/js",
                        "src": ["all.min.js"],
                        "dest": "public/js/vendors/fontawesome"
                    },
                    {
                        "nonull": true,
                        "expand": true,
                        "timestamp": true,
                        "cwd": "node_modules/@fortawesome/fontawesome-free/css",
                        "src": ["all.min.css"],
                        "dest": "public/css/vendors/fontawesome"
                    },
                    {
                        "nonull": true,
                        "expand": true,
                        "timestamp": true,
                        "cwd": "node_modules/@fortawesome/fontawesome-free/webfonts",
                        "src": "**",
                        "dest": "public/css/vendors/webfonts"
                    },
                    {
                        "nonull": true,
                        "expand": true,
                        "timestamp": true,
                        "cwd": "node_modules/github-markdown-css",
                        "src": "github-markdown.css",
                        "dest": "public/css/vendors/github-markdown-css"
                    },
                    {
                        "nonull": true,
                        "expand": true,
                        "timestamp": true,
                        "cwd": "node_modules/chart.js/dist",
                        "src": "Chart.min.js",
                        "dest": "public/js/vendors/chart.js"
                    },
                    {
                        "nonull": true,
                        "expand": true,
                        "timestamp": true,
                        "cwd": "node_modules/chart.js/dist",
                        "src": "Chart.min.css",
                        "dest": "public/css/vendors/chart.js"
                    }
                ],
                "font": []
            },
            "dependencies": {
                "files": [
                    {
                        "nonull": true,
                        "expand": true,
                        "timestamp": true,
                        "cwd": "node_modules/bootstrap/scss",
                        "src": "**",
                        "dest": "etc/src/scss/vendors/bootstrap"
                    },
                    {
                        "nonull": true,
                        "expand": true,
                        "timestamp": true,
                        "cwd": "node_modules/mdb-ui-kit/src/scss",
                        "src": "**",
                        "dest": "etc/src/scss/vendors/mdbootstrap"
                    },
                    {
                        "nonull": true,
                        "expand": true,
                        "timestamp": true,
                        "cwd": "node_modules/@fortawesome/fontawesome-free/scss",
                        "src": "**",
                        "dest": "etc/src/scss/vendors/fontawesome"
                    }
                ]
            },
            "test": {
                "files": [
                    {
                        "nonull": true,
                        "expand": true,
                        "timestamp": true,
                        "cwd": "etc/test",
                        "src": "**",
                        "dest": "public/html/test"
                    }
                ]
            }
        },
        "concat": {
            "build": {
                "options": {
                    "separator": ";"
                },
                "dist": {
                    "src": ["etc/src/js/test/**/*.js"],
                    "dest": "public/js/test"
                }
            }
        },
        "terser": {
            "options": {
                "ecma": 2016,
                "compress": true,
                "sourceMap": true,
                "format": {
                    "preamble": "/*! <%= _.capitalize(pkg.name) %> Copyright (c) <%= grunt.template.today('yyyy') %> <%= pkg.author.name %> <<%= pkg.author.email %>>. Released under the <%= pkg.license %> license. */"
                }
            },
            "build": {
                "files": {
                    "public/js/oroboros.min.js": "etc/src/js/oroboros.js",
                    "public/js/dashboard/dashboard.min.js": "etc/src/js/dashboard/dashboard.js",
                    "public/js/component/card.min.js": "etc/src/js/component/card.js",
                    "public/js/component/primary-navbar.min.js": "etc/src/js/component/primary-navbar.js",
                    "public/js/demo/dashboard/chart-area-demo.min.js": "etc/src/js/demo/dashboard/chart-area-demo.js",
                    "public/js/demo/dashboard/chart-bar-demo.min.js": "etc/src/js/demo/dashboard/chart-bar-demo.js",
                    "public/js/demo/dashboard/chart-pie-demo.min.js": "etc/src/js/demo/dashboard/chart-pie-demo.js",
                    "public/js/demo/dashboard/datatables-demo.min.js": "etc/src/js/demo/dashboard/datatables-demo.js"
                }
            },
            "test": {
                "files": {
                    "public/js/tests/mocha/test-example.min.js": "etc/src/js/test/example.js"
                }
            }
        },
        "sass": {
            "options": {
                "cacheLocation": "cache/sass",
                "trace": true,
                "sourceMap": true,
                "sourceMapContents": true,
                "sourceMapEmbed": true
            },
            "build": {
                "files": [{
                        "expand": true,
                        "cwd": "etc/src/scss/",
                        "src": ["**/*.scss", "!vendors"],
                        "dest": "public/css",
                        "ext": ".css"
                    }]
            }
        },
        "postcss": {
            "options": {
                "map": {
                    "inline": false,
                    "annotation": "public/css/"
                }
            },
            "dist": {
                "main": {
                    "src": "public/css/main.css"
                },
                "dashboard": {
                    "src": "public/css/dashboard.css"
                }
            }
        },
        "watch": {
            "css": {
                "files": "etc/src/scss/**/*.scss",
                "tasks": [
                    "sass:build",
                    "postcss",
                    "cssmin"
                ]
            },
            "js": {
                "files": "etc/src/js/**/*.js",
                "tasks": [
                    "terser:build",
                    "terser:test",
                    "concat",
                    "eslint",
                    "mocha"
                ]
            },
            "php": {
                "files": "lib/**/*.php",
                "tasks": [
                    "phpunit"
                ]
            },
            "twig": {
                "files": "tpl/twig/**/*.twig",
                "tasks": [
                    "clean:cache"
                ]
            }
        }
    },
    "tasks": {
        "init": [
            "composer:install:dev",
            "build"
        ],
        "test": [
            "copy:test",
            "build",
            "eslint",
            "mocha",
            "phpunit"
        ],
        "build": [
            "googlefonts:build",
            "copy",
            "sass:build",
            "postcss",
            "cssmin",
            "terser:build",
            "terser:test",
            "concat",
            "eslint",
            "mocha",
            "phpunit"
        ],
        "cleanup": [
            "clean:css",
            "clean:js",
            "clean:font",
            "clean:uploads",
            "clean:media",
            "clean:html",
            "clean:test",
            "clean:cache"
        ],
        "update": [],
        "package": [],
        "deploy": [],
        "default": [
            "init",
            "watch"
        ]
    }
}
