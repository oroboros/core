<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\coreScaffold\controller\cli;

/**
 * Command Line Script Scaffold Controller
 * Provides scaffolding utilities for the command line
 * to quickly prototype new javascript files
 *
 * @author Brian Dayhoff
 */
final class ScriptScaffoldController extends \Oroboros\core\abstracts\controller\AbstractCliController implements \Oroboros\core\interfaces\CoreInterface
{

    use \Oroboros\coreScaffold\traits\controller\ScaffoldControllerTrait;

    /**
     * Use the standard cli error controller
     */
    const ERROR_CONTROLLER = 'cli\\ErrorController';
    const MODULE_NAME = 'scaffold';

    private $scaffold_valid = [
        'script' => [
            'enabled' => true,
            'display' => 'A Javascript File',
            'method' => 'scriptScaffold',
        ],
    ];

    /**
     * Valid script scaffolding type options
     * @var array
     */
    private $opts = [
        'bootstrap' => 'An Oroboros.js Bootstrap Class',
        'component' => 'An Oroboros.js Component Class',
        'controller' => 'An Oroboros.js Controller Class',
        'extension' => 'An Oroboros.js Extension Class',
        'layout' => 'An Oroboros.js Layout Class',
        'model' => 'An Oroboros.js Model Class',
        'page' => 'An Oroboros.js Page Class',
        'theme' => 'An Oroboros.js Theme Class',
        'view' => 'An Oroboros.js View Class',
        'exit' => 'Terminate the program.'
    ];

    public function scaffold(\Oroboros\core\interfaces\library\container\ContainerInterface $args = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null)
    {
        if (!is_null($flags)) {
            $flags = $this->parseScriptFlags($flags);
        }
        if (is_null($args)) {
            $args = $this->containerize();
        }
        if (is_null($args)) {
            $args = $this->containerize();
        }
        try {
            $args = $this->resolveArguments($args, $flags);
            if ($args->has('interactive')) {
                $args = $this->interactiveShell($args);
            }
            $this->validateArguments($args);
            $this->executeOperation($args);
        } catch (\InvalidArgumentException $e) {
            // Cannot resolve scaffold attempt. Exit with error.
            throw new \Oroboros\core\exception\cli\UnhandledErrorException($e->getMessage());
        }
        return $this;
    }

    /**
     * Resolves given flags to standardized names from shorthand
     * and unsets conflicting flags when they are overridden by a higher priority one
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $flags
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private function parseScriptFlags(\Oroboros\core\interfaces\library\container\ContainerInterface $flags): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $result = [];
        if ($flags->has('interactive') || $flags->has('i')) {
            // Use the interactive shell for any unresolved arguments
            $result['interactive'] = true;
        }
        if ($flags->has('localized') || $flags->has('l')) {
            // Path is relative to the app root
            $result['localized'] = true;
        }
        if ($flags->has('quiet') || $flags->has('q')) {
            // Silence all non-error output
            $result['quiet'] = true;
        }
        if ($flags->has('debug') || $flags->has('d')) {
            // Do not print debug output if either the quiet or silent flag was passed
            if (!array_key_exists('quiet', $result)) {
                // Print debug output
                $result['debug'] = true;
            }
        }
        if ($flags->has('type')) {
            // Type supplied with a flag
            $result['type'] = $flags['type'];
        }
        if ($flags->has('name')) {
            // Name supplied with a flag
            $result['name'] = $flags['name'];
        }
        if ($flags->has('path')) {
            // Path supplied with a flag
            $result['path'] = $flags['path'];
            if (array_key_exists('localized', $result)) {
                // If localized, path is relative to the app root
                $result['path'] = $this::app()->getApplicationRoot() . trim($result['path'], DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;
            }
        } else {
            // Use the current working directory
            $result['path'] = getcwd() . DIRECTORY_SEPARATOR;
            // If localized is set, unset it. It is pointless without a path.
            if (array_key_exists('localized', $result)) {
                unset($result['localized']);
            }
        }
        if ($flags->has('filename')) {
            // Filename supplied with a flag
            $result['filename'] = $flags['filename'];
        }
        if ($flags->has('dryrun')) {
            // No file will be generated, only a report of what would be done will be performed
            $result['dryrun'] = $flags['dryrun'];
        }
        return $this->containerize($result);
    }

    /**
     * Resolves the arguments and flags to a full internal argument set
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $args
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $flags
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     * @throws \InvalidArgumentException if there are argument conflicts
     */
    private function resolveArguments(\Oroboros\core\interfaces\library\container\ContainerInterface $args, \Oroboros\core\interfaces\library\container\ContainerInterface $flags): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        if ($args->has('type') && $flags->has('type') && $flags['type'] !== $args['type']) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Ambiguous type declaration. Flag declaration [%1$s] collides with parameter declaration [%2$s]. Please use only one type definer.', $flags['type'], $args['type']));
        }
        if ($args->has('name') && $flags->has('name') && $flags['name'] !== $args['name']) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Ambiguous name declaration. Flag declaration [%1$s] collides with parameter declaration [%2$s]. Please use only one name definer.', $flags['name'], $args['name']));
        }
        foreach ($flags as $key => $value) {
            $args[$key] = $value;
        }
        if (!$args->has('filename') && $args->has('name')) {
            $args['filename'] = $this->canonicalize($args['name']) . '.js';
        }
        return $args;
    }

    /**
     * Throws an InvalidArgumentException if any flags are missing or not valid
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $args
     * @return void
     * @throws \InvalidArgumentException
     */
    private function validateArguments(\Oroboros\core\interfaces\library\container\ContainerInterface $args): void
    {
        if (!$args->has('path')) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('No path value supplied, cannot generate file.'));
        }
        if (!$args->has('name')) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('No name value supplied, cannot generate file.'));
        }
        if (!$args->has('filename')) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('No filename value supplied, cannot generate file.'));
        }
        $file = $args['path'] . $args['filename'];
        if (file_exists($file)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('File [%1$s] cannot be scaffolded because it already exists in [%2$s].', $args['filename'], $args['path']));
        }
    }

    private function executeOperation(\Oroboros\core\interfaces\library\container\ContainerInterface $args): void
    {
        $component_name = sprintf('javascript\\oroboros\\%1$s', ucfirst($args['type']));
        $component = $this->load('component', $component_name, $args['name'], [
            'builder' => $this->load('library', 'component\\javascript\\ComponentBuilder')
        ]);
        $source = $component->render();
        if (!$args->has('quiet')) {
            $this->info_message(sprintf('Generating javascript file [%1$s] of type [%2$s] in directory [%3$s].', $args['filename'], $args['type'], $args['path']));
        }
        if ($args->has('dryrun')) {
            if (!$args->has('quiet')) {
                $this->info_message(sprintf('No file generated because dryrun option is enabled.'));
            }
            return;
        }
        $writer = $this->load('library', 'file\\FileWriter');
        $writer->setOption('generate-directories', true);
        $writer->setDirectory($args['path']);
        $writer->setFile($args['filename']);
        $writer->write($source);
    }

    private function interactiveShell(\Oroboros\core\interfaces\library\container\ContainerInterface $args): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        if ($args->has('type') && $args->has('name') && $args->has('path')) {
            // Nothing to do
            return $args;
        }
        $this->clear_screen();
        $this->print_message('Script Scaffold Utility.');
        if (!$args->has('type')) {
            $selection = null;
            while (!array_key_exists($selection, $this->opts)) {
                $this->clear_screen();
                if (!is_null($selection)) {
                    $this->error_message(sprintf('[%1$s] is not a valid selection. Please select one of the options below, or [exit] to quit', $selection));
                }
                $this->print_message('What type of script would you like to create?');
                foreach ($this->opts as $key => $details) {
                    if ($key === 'exit') {
                        continue;
                    }
                    $this->print_message(sprintf('[%1$s] - %2$s', $key, $details));
                }
                $selection = strtolower($this->read_input());
            }
            if ($selection === 'exit') {
                $this->print_message('Program terminated.');
                throw new \Oroboros\core\exception\cli\ExitException();
            }
            $args['type'] = $selection;
        }
        if (!$args->has('name')) {
            $selection = null;
            $this->print_message('Please enter a name for your script (lower case canonicalized),');
            $this->print_message('or [exit] to quit.');
            $selection = trim($this->canonicalize(strtolower($this->read_input())), '-');
            if ($selection === 'exit') {
                $this->print_message('Program terminated.');
                throw new \Oroboros\core\exception\cli\ExitException();
            }
            $args['name'] = $selection;
        }
        if (!$args->has('path')) {
            $selection = null;
            $msg = 'Please enter a directory path for the output.';
            if ($args->has('localized')) {
                $msg .= sprintf(' The given path will be appended to [%1$s] since localization is enabled.', $this::app()->getApplicationRoot());
            }
            $this->print_message($msg);
            $this->print_message('Or [exit] to quit.');
            $selection = $this->read_input();
            if ($selection === 'exit') {
                $this->print_message('Program terminated.');
                throw new \Oroboros\core\exception\cli\ExitException();
            }
            if ($args->has('localized')) {
                $selection = $this::app()->getApplicationRoot() . trim($path, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;
            }
            $args['path'] = $selection;
        }
        if (!$args->has('filename')) {
            $args['filename'] = $this->canonicalize($args['name']) . '.js';
        }
        return $args;
    }
}
