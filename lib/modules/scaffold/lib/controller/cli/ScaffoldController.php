<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\coreScaffold\controller\cli;

/**
 * Command Line Scaffold Controller
 * Provides scaffolding utilities for the command line
 * to quickly prototype new extensions, modules, services, and apps
 *
 * @author Brian Dayhoff
 */
final class ScaffoldController extends \Oroboros\core\abstracts\controller\AbstractCliController implements \Oroboros\core\interfaces\CoreInterface
{

    use \Oroboros\coreScaffold\traits\controller\ScaffoldControllerTrait;

    /**
     * Use the standard cli error controller
     */
    const ERROR_CONTROLLER = 'cli\\ErrorController';
    const MODULE_NAME = 'scaffold';

    private $scaffold_valid = [
        'app' => [
            'enabled' => false,
            'display' => 'An Application',
            'method' => 'appScaffold',
        ],
        'module' => [
            'enabled' => false,
            'display' => 'A module',
            'method' => 'moduleScaffold',
        ],
        'extention' => [
            'enabled' => false,
            'display' => 'An Extention',
            'method' => 'extentionScaffold',
        ],
        'service' => [
            'enabled' => false,
            'display' => 'A Service',
            'method' => 'serviceScaffold',
        ],
        'component' => [
            'enabled' => false,
            'display' => 'A Component',
            'method' => 'componentScaffold',
        ],
        'library' => [
            'enabled' => false,
            'display' => 'A Library',
            'method' => 'libraryScaffold',
        ],
        'adapter' => [
            'enabled' => false,
            'display' => 'An Adapter',
            'method' => 'adapterScaffold',
        ],
        'controller' => [
            'enabled' => false,
            'display' => 'A Controller',
            'method' => 'controllerScaffold',
        ],
        'model' => [
            'enabled' => false,
            'display' => 'A Model',
            'method' => 'modelScaffold',
        ],
        'view' => [
            'enabled' => false,
            'display' => 'A View',
            'method' => 'viewScaffold',
        ],
        'interface' => [
            'enabled' => false,
            'display' => 'An Interface',
            'method' => 'interfaceScaffold',
        ],
        'trait' => [
            'enabled' => false,
            'display' => 'A Trait',
            'method' => 'traitScaffold',
        ],
    ];

    public function index()
    {
        try {
            $key = $this->getMethod();
            $method = $this->scaffold_valid[$key]['method'];
            return $this->$method();
        } catch (\Oroboros\core\exception\cli\ExitException $e) {
            // Clean exit, Let the bootstrap handle it
            throw $e;
        } catch (\Exception $e) {
            $this->error_message(sprintf('Unable to resolve the scaffold request due to an unhandled error: [%1$s].', $e->getMessage()));
            throw new \Oroboros\core\exception\cli\UnhandledErrorException('Could not complete the requested scaffolding operation.', 1, E_USER_ERROR);
        }
    }

    public function appScaffold(\Oroboros\core\interfaces\library\container\ContainerInterface $args = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null)
    {
        s($flags->toArray());
        exit;
        if (!is_null($args)) {
            s($args);
        }
        $this->print_message('Application Scaffold Utility.');
        return $this;
    }

    public function moduleScaffold(\Oroboros\core\interfaces\library\container\ContainerInterface $args = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null)
    {
        $this->print_message('Module Scaffold Utility.');
        return $this;
    }

    public function extentionScaffold(\Oroboros\core\interfaces\library\container\ContainerInterface $args = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null)
    {
        $this->print_message('Extention Scaffold Utility.');
        return $this;
    }

    public function serviceScaffold(\Oroboros\core\interfaces\library\container\ContainerInterface $args = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null)
    {
        $this->print_message('Service Scaffold Utility.');
        return $this;
    }

    public function componentScaffold(\Oroboros\core\interfaces\library\container\ContainerInterface $args = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null)
    {
        $this->print_message('Component Scaffold Utility.');
        return $this;
    }

    public function libraryScaffold(\Oroboros\core\interfaces\library\container\ContainerInterface $args = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null)
    {
        $this->print_message('Library Scaffold Utility.');
        return $this;
    }

    public function adapterScaffold(\Oroboros\core\interfaces\library\container\ContainerInterface $args = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null)
    {
        $this->print_message('Adapter Scaffold Utility.');
        return $this;
    }

    public function controllerScaffold(\Oroboros\core\interfaces\library\container\ContainerInterface $args = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null)
    {
        $this->print_message('Controller Scaffold Utility.');
        return $this;
    }

    public function modelScaffold(\Oroboros\core\interfaces\library\container\ContainerInterface $args = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null)
    {
        $this->print_message('Model Scaffold Utility.');
        return $this;
    }

    public function viewScaffold(\Oroboros\core\interfaces\library\container\ContainerInterface $args = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null)
    {
        $this->print_message('View Scaffold Utility.');
        return $this;
    }

    public function interfaceScaffold(\Oroboros\core\interfaces\library\container\ContainerInterface $args = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null)
    {
        $this->print_message('Interface Scaffold Utility.');
        return $this;
    }

    public function traitScaffold(\Oroboros\core\interfaces\library\container\ContainerInterface $args = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null)
    {
        $this->print_message('Trait Scaffold Utility.');
        return $this;
    }

    /**
     * Prompts the user for a build option.
     * @return string the key of the build option
     */
    private function getMethod(): string
    {
        $opts = ['exit'];
        $selection = null;
        while (!in_array($selection, $opts)) {
            $this->clear_screen();
            if (!is_null($selection)) {
                $this->error_message(sprintf('[%1$s] is not a valid selection. Please select one of the options below, or [exit] to quit', $selection));
            }
            $this->print_message('What would you like to build?');
            foreach ($this->scaffold_valid as $key => $details) {
                if (!$details['enabled']) {
                    continue;
                }
                $opts[] = $key;
                $this->print_message(sprintf('[%1$s] - %2$s', $key, $details['display']));
            }
            $selection = $this->read_input();
        }
        if ($selection === 'exit') {
            $this->print_message('Program terminated.');
            throw new \Oroboros\core\exception\cli\ExitException();
        }
        return $selection;
    }
}
