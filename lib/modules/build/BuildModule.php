<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\coreBuild;

/**
 * Oroboros Build Module
 * Provides support for running application builds.
 *
 * @author Brian Dayhoff
 */
final class BuildModule extends \Oroboros\core\abstracts\module\AbstractModule implements \Oroboros\core\interfaces\CoreInterface
{

    public function update(\SplSubject $subject): void
    {
//        d('module observer updated', $subject->getObserverEvent());
        parent::update($subject);
    }
//    
//    protected function handleAdapterObserverUpdate(\Oroboros\core\interfaces\adapter\AdapterInterface $adapter): void
//    {
//        
//    }
//
//    protected function handleApplicationObserverUpdate(\Oroboros\core\interfaces\adapter\AdapterInterface $application): void
//    {
//        
//    }
//
//    protected function handleBootstrapObserverUpdate(\Oroboros\core\interfaces\bootstrap\BootstrapInterface $bootstrap): void
//    {
//        
//    }
//
//    protected function handleControllerObserverUpdate(\Oroboros\core\interfaces\controller\ControllerInterface $controller): void
//    {
//      d('Fired controller event in build module', $controller->getObserverEvent(), $this->filterObserverClassEventName($controller), get_class($controller), $controller::CLASS_TYPE, $controller::CLASS_SCOPE);  
//    }
//
//    protected function handleExceptionObserverUpdate(\Oroboros\core\interfaces\exception\ExceptionInterface $exception): void
//    {
//        
//    }
//
//    protected function handleExtensionObserverUpdate(\Oroboros\core\interfaces\extension\ExtensionInterface $extension): void
//    {
//        
//    }
//
//    protected function handleLibraryObserverUpdate(\Oroboros\core\interfaces\library\LibraryInterface $library): void
//    {
//        
//    }
//
//    protected function handleModelObserverUpdate(\Oroboros\core\interfaces\model\ModelInterface $model): void
//    {
//        
//    }
//    
//    protected function handleModuleObserverUpdate(\Oroboros\core\interfaces\module\ModuleInterface $module): void
//    {
//        
//    }
//    
//    protected function handleServiceObserverUpdate(\Oroboros\core\interfaces\service\ServiceInterface $service): void
//    {
//        
//    }
//    
//    protected function handleViewObserverUpdate(\Oroboros\core\interfaces\view\ViewInterface $view): void
//    {
//        
//    }

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
//        d($this);
    }
}
