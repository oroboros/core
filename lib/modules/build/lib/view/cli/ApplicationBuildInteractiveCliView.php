<?php
namespace Oroboros\coreBuild\view\cli;

/**
 * Application Build Interactive Cli View
 * 
 * Provides an interactive shell for a command line user to declare app build arguments
 *
 * @author Brian Dayhoff
 */
final class ApplicationBuildInteractiveCliView extends \Oroboros\core\abstracts\view\AbstractInteractiveCliView implements \Oroboros\core\interfaces\CoreInterface
{

    const MODULE_NAME = 'build';

    /**
     * Validates a user returned argument.
     * Throws an `\Oroboros\core\exception\InvalidArgumentException`
     * if it does not pass validation.
     * 
     * Default behavior defers to the declared resolver class
     * defined by class constant `RESOLVER_CLASS`
     * 
     * If you need more robust error checking methods,
     * you should override this method for custom checking.
     * 
     * @note This method fires AFTER `filterOptionValue`,
     *       so it will still validate transformed arguments.
     *       Transformation filters should not operate with the assumption
     *       that the value is correct. If it is not, return a non-validating
     *       value.
     * 
     * @param mixed $value
     * @param mixed $valid
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException if the argument is not valid
     */
    protected function validateOptionValue(string $key, $value = null, $valid = null): void
    {
        parent::validateOptionValue($key, $value, $valid);
        switch ($key) {
            case 'email':
                $this->validateEmail($value);
                break;
            case 'namespace':
                $this->validateNamespace($value);
                break;
        }
    }

    private function validateEmail($email): void
    {
        // Email is not mandatory
        if (in_array($email, [null, false])) {
            return;
        }
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('[%1$s] is not a valid email address.', $email));
        }
    }
    
    private function validateNamespace(string $namespace): void
    {
        $regex = '/^(?!\\\)+[a-zA-Z]{1}[a-zA-Z0-9]{0,}\\[a-zA-Z]{1}[a-zA-Z0-9]{0,}+(?!\\\)/';
        if (!preg_match($regex, $namespace) === false) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('[%1$s] is not a valid base namespace. '
                . 'Format should be [%2$s] with no leading or trailing backslashes, '
                . 'no hyphens or underscores, and no numerical digits preceding '
                . 'either segment.', $namespace, 'vendor\\package'));
        }
    }
}
