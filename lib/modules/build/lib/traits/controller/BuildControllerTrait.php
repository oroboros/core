<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\coreBuild\traits\controller;

/**
 * Build Controller Trait
 * Provides common methods to build controllers
 *
 * @author Brian Dayhoff
 */
trait BuildControllerTrait
{

    /**
     * Represents the build map for build operations.
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $build_map;
    private static $mapfile = 'config/build-map.json';

    /**
     * Returns the build map
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    protected function getBuildMap(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $this->initializeBuildMap();
        return $this->build_map;
    }

    /**
     * Imports the build map if it does not already exist in this scope.
     * @return void
     */
    private function initializeBuildMap(): void
    {
        if (!is_null($this->build_map)) {
            return;
        }
        $path = $this->getModule()->path() . self::$mapfile;
        $data = $this->load('library', 'parser\\JsonParser', $path)->fetch();
        foreach ($data as $key => $value) {
            $data[$key] = $this->recurseMap($value);
        }
        $this->build_map = $data;
    }

    private function recurseMap($map)
    {
        if (!is_array($map)) {
            return $map;
        }
        foreach ($map as $key => $value) {
            $map[$key] = $this->recurseMap($value);
        }
        return $this->containerize($map);
    }
}
