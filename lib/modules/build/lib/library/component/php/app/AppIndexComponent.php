<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\coreBuild\library\component\php\app;

/**
 * App Index Component
 * Generates an index.php file for an application build.
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
final class AppIndexComponent extends \Oroboros\core\abstracts\library\component\php\AbstractFileComponent implements \Oroboros\core\interfaces\CoreInterface
{

    use \Oroboros\core\traits\pluggable\ModuleLibraryTrait;

    const MODULE_NAME = 'build';
    const COMPONENT_KEY = 'php-app-build-index';

    private static $required_keys = [
        'author' => 'string',
        'namespace' => 'string',
        'core-path' => 'string'
    ];
    private static $optional_keys = [
        'email' => 'string',
        'license' => 'string'
    ];
    private $satisfied_keys = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeSatisfiedKeys();
    }

    /**
     * Provides all valid context keys for this component.
     * 
     * @return array
     */
    protected function declareValidContextKeys(): array
    {
        return array_merge(parent::declareValidContextKeys(), [
            "author", // Declares the individual or organization project owner
            "email", // Declares the author or organization email for the project owner
            "license", // Declares the type of license for the project, if any
            "core-path", // Declares the path to oroboros core
        ]);
    }

    /**
     * Provides all context filters for this component.
     * 
     * @return array
     */
    protected function declareContextFilters(): array
    {
        return array_merge(parent::declareContextFilters(), []);
    }

    protected function initializeContext(): void
    {
        parent::initializeContext();
        // no assumption that there an author provided
        $this->addContext('author', false);
        // no assumption that there is an author email provided
        $this->addContext('email', false);
        // no assumption that there is a license provided
        $this->addContext('license', false);
        // by default, use the defined core-path if not otherwise supplied
        $this->addContext('core-path', OROBOROS_CORE_BASE_DIRECTORY);
    }

    private function initializeSatisfiedKeys(): void
    {
        if (is_null($this->satisfied_keys)) {
            $this->satisfied_keys = $this->containerize();
        }
    }
}
