<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\coreBuild\library\component\json\config;

/**
 * Composer Config Component
 * Used to dynamically generate a composer.json config file.
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
final class ComposerConfigComponent extends \Oroboros\core\abstracts\library\component\json\AbstractJsonComponent implements \Oroboros\core\interfaces\CoreInterface
{

    use \Oroboros\core\traits\pluggable\ModuleLibraryTrait;

    const MODULE_NAME = 'build';
    const COMPONENT_KEY = 'json-build-composer-config';

    /**
     * Indicates that json components *should not* inherently validate context keys.
     * This constant can be overridden and changed to `false` to disable this functionality.
     */
    const REQUIRE_VALID_CONTEXT = true;

    private $valid_keys = [
        'name',
        'namespace',
        'description',
        'version',
        'type',
        'readme',
        'authors',
        'config',
        'autoload',
        'bin',
        'require',
        'require-dev',
    ];
    private $filters = [
        'name' => 'filterName',
        'description' => 'filterDescription',
        'version' => 'filterVersion',
        'type' => 'filterType',
        'readme' => 'filterReadme',
        'authors' => 'filterAuthors',
        'config' => 'filterConfig',
        'autoload' => 'filterAutoload',
        'bin' => 'filterBin',
        'require' => 'filterRequire',
        'require-dev' => 'filterRequireDev',
    ];
    private $omitted_keys = [
        
    ];
    
    protected function declareOmittedContext(): array
    {
        return array_merge(parent::declareOmittedContext(), $this->omitted_keys);
    }

    /**
     * Override this method to declare keys that should be passed from arguments 
     * into the context for the template, if they are provided.
     * @return array
     */
    protected function declareValidContextKeys(): array
    {
        return array_merge(parent::declareValidContextKeys(), $this->valid_keys);
    }

    /**
     * Override this method to declare filters for given context keys.
     * This should return an associative array, where the key is the context key,
     * and the value is a function to pass the given context value into.
     * The given function must accept a single mixed argument,
     * and return the formatted value.
     * The function must also be either protected or public.
     * @return array
     */
    protected function declareContextFilters(): array
    {
        return array_merge(parent::declareContextFilters(), $this->filters);
    }

    protected function initializeContext(): void
    {
        parent::initializeContext();
//        // no assumption that a name exists
//        $this->addContext('name', false);
//        // no assumption that a description exists
//        $this->addContext('description', false);
//        // no assumption that a version number exists
//        $this->addContext('version', false);
//        // no assumption that a type exists
//        $this->addContext('type', false);
//        // no assumption that a readme exists
//        $this->addContext('readme', false);
//        // no assumption that authors exist
//        $this->addContext('authors', false);
//        // no assumption that config options exist
//        $this->addContext('config', false);
//        // no assumption that autoload options exist
//        $this->addContext('autoload', false);
//        // no assumption that binaries exist
//        $this->addContext('bin', false);
//        // no assumption package dependencies exist
//        $this->addContext('require', false);
//        // no assumption that dev dependenies exist
//        $this->addContext('require-dev', false);
    }

    protected function filterName($name)
    {
        if (!is_string($name))
        {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                sprintf('Error encountered in [%1$s]. Provided key [%2$s] must be a string.', get_class($this), 'name')
            );
        }
        return $name;
    }

    protected function filterDescription($description)
    {
        if (!is_string($description))
        {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                sprintf('Error encountered in [%1$s]. Provided key [%2$s] must be a string.', get_class($this), 'description')
            );
        }
        return $description;
    }

    protected function filterVersion($version)
    {
        if (!is_string($version))
        {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                sprintf('Error encountered in [%1$s]. Provided key [%2$s] must be a string.', get_class($this), 'version')
            );
        }
        return $version;
    }

    protected function filterType($type)
    {
        if (!is_string($type))
        {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                sprintf('Error encountered in [%1$s]. Provided key [%2$s] must be a string.', get_class($this), 'type')
            );
        }
        return $type;
    }

    protected function filterReadme($readme)
    {
        if (!is_string($readme))
        {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                sprintf('Error encountered in [%1$s]. Provided key [%2$s] must be a string.', get_class($this), 'readme')
            );
        }
        return $readme;
    }

    protected function filterAuthors($authors)
    {
        if ($authors === false) {
            return $authors;
        }
        return $authors;
    }

    protected function filterConfig($config)
    {
        if ($config === false) {
            return $config;
        }
        return $config;
    }

    protected function filterAutoload($autoload)
    {
        if ($autoload === false) {
            return $autoload;
        }
        return $autoload;
    }

    protected function filterBin($bin)
    {
        if ($bin === false) {
            return $bin;
        }
        return $bin;
    }

    protected function filterRequire($require)
    {
        if ($require === false) {
            return $require;
        }
        return $require;
    }

    protected function filterRequireDev($require_dev)
    {
        if ($require_dev === false) {
            return $require_dev;
        }
        return $require_dev;
    }
}
