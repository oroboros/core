<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\coreBuild\library\definer\php\application;

/**
 * Filesystem Definer
 * Manages definitions for the application filesystem for build operations
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
final class FilesystemDefiner extends \Oroboros\core\abstracts\library\definer\AbstractDefiner implements \Oroboros\core\interfaces\CoreInterface
{

    use \Oroboros\core\traits\pluggable\ModuleLibraryTrait;
    use \Oroboros\coreBuild\traits\library\definer\BuildDefinerTrait;

    const MODULE_NAME = 'build';

    /**
     * Path to the definer definition config, relative to the module root.
     */
    const DEFINER_FILE = 'etc/definers/php/application/filesystem.json';

    /**
     * Any additional definers found within the filetree definition
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $definers = null;

    /**
     * Components used to generate files in conjunction with definers
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $components = null;

    /**
     * The builder object required by components
     * @var \Oroboros\core\interfaces\pattern\builder\BuilderInterface
     */
    private $component_builder = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        $this->initializeDefinerContainer();
        $this->initializeComponentContainer();
        $this->initializeComponentBuilder();
        parent::__construct($command, $arguments, $flags);
    }

    /**
     * Returns a boolean determination as to whether the final data set
     * can compile with the current set of satisfied keys.
     * 
     * Also checks internally declared definers.
     * 
     * @return bool
     */
    public function canCompile(): bool
    {
        if (parent::canCompile() === false) {
            return false;
        }
        foreach ($this->getDefiners() as $key => $definer) {
            if (!$definer->canCompile()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Compiles the final dataset output. All required keys must be satisfied.
     * An ErrorException will be raised if there are missing required keys.
     * 
     * @return \Oroboros\core\interfaces\library\definer\DefinerInterface returns $this (method chainable)
     * @throws \Oroboros\core\exception\ErrorException if any required keys have not yet been passed definitions.
     */
    public function compile(): \Oroboros\core\interfaces\library\definer\DefinerInterface
    {
        if (!$this->canCompile()) {
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. Cannot compile dataset '
                        . 'because required keys [%2$s] do not have definitions.'
                        , get_class($this), implode(', ', array_keys($this->getUnsatisfiedRequiredKeys())))
            );
        }
        foreach ($this->getDefiners() as $key => $definer) {
            $definer->compile();
            $this->addComponentContext($key);
        }
        parent::compile();
        return $this;
    }

    /**
     * Updates local required keys as well as any internally declared definers
     * that use the same key in either required or optional scope.
     * 
     * @param string $key
     * @param type $value
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException if the designated key
     *         is not a required key, or if the type of the value is not valid
     *         for the required type.
     */
    public function setRequiredKey(string $key, $value): void
    {
        parent::setRequiredKey($key, $value);
        foreach ($this->getDefiners() as $k => $definer) {
            if ($definer->hasRequiredKey($key)) {
                $definer->setRequiredKey($key, $value);
            }
            if ($definer->hasOptionalKey($key)) {
                $definer->setOptionalKey($key, $value);
            }
        }
    }

    /**
     * Updates local required keys as well as any internally declared definers
     * that use the same key in optional scope.
     * 
     * @param string $key
     * @param type $value
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException if the designated key
     *         is not an optional key, or if the type of the value is not valid
     *         for the required type.
     */
    public function setOptionalKey(string $key, $value): void
    {
        parent::setOptionalKey($key, $value);
        foreach ($this->getDefiners() as $k => $definer) {
            if ($definer->hasOptionalKey($key)) {
                $definer->setOptionalKey($key, $value);
            }
        }
    }

    /**
     * Prepares the initial dataset for external operations.
     * Sets defaults if they do not exist, formats the dataset,
     * and fetches existing required keys from anywhere in
     * the dataset tree recursively.
     * 
     * @return void
     */
    protected function prepareDataSet(): void
    {
        parent::prepareDataSet();
        foreach ($this->getDefiners() as $key => $definer) {
            $this->updateRequiredDefinitions($definer);
            $this->updateOptionalDefinitions($definer);
        }
    }

    /**
     * Extracts required keys from definers set within the master dataset and
     * pulls their required keys into the front-facing set.
     * 
     * @param \Oroboros\core\interfaces\library\definer\DefinerInterface $definer
     * @return void
     */
    protected function updateRequiredDefinitions(\Oroboros\core\interfaces\library\definer\DefinerInterface $definer): void
    {
        $data = $this->getDataSet();
        $required = $definer->getRequired();
        foreach ($required as $key => $format) {
            if (!$data['require']->has($key)) {
                $data['require'][$key] = $format;
            }
        }
    }

    /**
     * Extracts optional keys from definers set within the master dataset and
     * pulls their optional keys into the front-facing set.
     * 
     * @param \Oroboros\core\interfaces\library\definer\DefinerInterface $definer
     * @return void
     */
    protected function updateOptionalDefinitions(\Oroboros\core\interfaces\library\definer\DefinerInterface $definer): void
    {
        $data = $this->getDataSet();
        $optional = $definer->getOptional();
        foreach ($optional as $key => $format) {
            if (!$data['optional']->has($key)) {
                $data['optional'][$key] = $format;
            }
        }
    }

    /**
     * Additionally instantiates any definer definitions found within the dataset.
     * @param mixed $data
     */
    protected function formatDataSet($data, string $keyname = null)
    {
        $data = parent::formatDataSet($data, $keyname);
        if (!(is_object($data) && $data instanceof \Oroboros\core\interfaces\library\container\ContainerInterface)) {
            return $data;
        }
        if (array_key_exists('definer', $data->toArray()) && is_string($data['definer'])) {
            $this->validateDefiner($data['definer']);
            $definer = $this->load('library', $data['definer'], $keyname, null, $this->getFlags());
            $this->definers[$keyname] = $definer;
            $data['definer'] = $definer;
        }
        if (array_key_exists('component', $data->toArray()) && is_string($data['component'])) {
            $this->validateComponent($data['component']);
            $component = $this->load('component', $data['component'], $keyname, [
                'builder' => $this->getComponentBuilder()
                ], $this->getFlags());
            $this->components[$keyname] = $component;
            $data['component'] = $component;
        }
        return $data;
    }

    /**
     * Returns the definer container
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    protected function getDefiners(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        return $this->definers;
    }

    /**
     * Returns the component container
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    protected function getComponents(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        return $this->components;
    }

    /**
     * Returns the component builder used to construct components
     * @return \Oroboros\core\interfaces\pattern\builder\BuilderInterface
     */
    protected function getComponentBuilder(): \Oroboros\core\interfaces\pattern\builder\BuilderInterface
    {
        return $this->component_builder;
    }

    /**
     * Injects internal definer context into the corresponding component.
     * 
     * @param string $keyname
     * @return void
     */
    private function addComponentContext(string $keyname): void
    {
        $definer = $this->getDefiners()[$keyname];
        $component = $this->getComponents()[$keyname];
        foreach ($definer->fetch() as $key => $context) {
            $component->addContext($key, $context);
        }
    }

    /**
     * Checks that a given definer declared in the dataset can resolve
     * to a valid definer object.
     * 
     * @param string $definer_name
     * @return void
     * @throws \Oroboros\core\exception\ErrorException
     */
    private function validateDefiner(string $definer_name): void
    {
        $expected = 'Oroboros\\core\\interfaces\\library\\definer\\DefinerInterface';
        if (!$this->getResolver()->resolve($definer_name, $expected)) {
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. Configuration [%2$s] '
                        . 'provides an invalid definer name [%3$s]. '
                        . 'Expected string classname resolving to an instance of [%4$s]',
                        get_class($this), static::DEFINER_FILE, $definer_name, $expected)
            );
        }
    }

    /**
     * Checks that a given component declared in the dataset can resolve
     * to a valid component object.
     * 
     * @param string $component_name
     * @return void
     * @throws \Oroboros\core\exception\ErrorException
     */
    private function validateComponent(string $component_name): void
    {
        $expected = 'Oroboros\\core\\interfaces\\library\\component\\ComponentInterface';
        if (!$this->getResolver()->resolve($component_name, $expected)) {
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. Configuration [%2$s] '
                        . 'provides an invalid component name [%3$s]. '
                        . 'Expected string classname resolving to an instance of [%4$s]',
                        get_class($this), static::DEFINER_FILE, $component_name, $expected)
            );
        }
    }

    /**
     * Initiailizes the container to add components generated by various definer
     * instances found within the filetree data.
     * @return void
     */
    private function initializeComponentBuilder(): void
    {
        if (is_null($this->component_builder)) {
            $this->component_builder = $this->load('library', 'component\\php\\ComponentBuilder');
        }
    }

    /**
     * Initiailizes the container to add components generated by various definer
     * instances found within the filetree data.
     * @return void
     */
    private function initializeComponentContainer(): void
    {
        if (is_null($this->components)) {
            $this->components = $this->containerize();
        }
    }

    /**
     * Initiailizes the container to add additional definer
     * instances found within the filetree data.
     * @return void
     */
    private function initializeDefinerContainer(): void
    {
        if (is_null($this->definers)) {
            $this->definers = $this->containerize();
        }
    }
}
