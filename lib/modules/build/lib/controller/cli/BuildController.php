<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\coreBuild\controller\cli;

/**
 * Command Line Build Controller
 * Provides build utilities for the command line
 *
 * @author Brian Dayhoff
 */
final class BuildController extends \Oroboros\core\abstracts\controller\AbstractCliController implements \Oroboros\core\interfaces\CoreInterface
{

    use \Oroboros\core\traits\cli\CliModuleControllerUtilityTrait;
    use \Oroboros\coreBuild\traits\controller\BuildControllerTrait;

    /**
     * Use the standard cli error controller
     */
    const ERROR_CONTROLLER = 'cli\\ErrorController';
    const MODULE_NAME = 'build';

    private static $valid_build_types = [
        'app' => 'buildApp',
        'module' => 'buildModule',
        'extension' => 'buildExtension',
        'service' => 'buildService',
        'component' => 'buildComponent',
    ];
    private $view = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this::addViewType('cli-interactive-build', 'cli\\ApplicationBuildInteractiveCliView');
    }

    public function index(\Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\CliControllerInterface
    {
        if (is_null($arguments)) {
            $arguments = $this->containerize([
                'type' => 'help'
            ]);
        }
        if (is_null($flags)) {
            $flags = $this->containerize();
        }
        if (array_search('help', $arguments->toArray(), true) !== false) {
            return $this->help($arguments, $flags);
        }
        if (array_key_exists($arguments['type'], self::$valid_build_types)) {
            $method = self::$valid_build_types[$arguments['type']];
            return $this->$method($arguments, $flags);
        }
        return $this->error(404, sprintf('Specified build type [%1$s] is not known.', $arguments['type']), $arguments, $flags);
    }

    /**
     * Builds an application using the command line interface
     * 
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $arguments
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $flags
     * @return \Oroboros\core\interfaces\controller\CliControllerInterface
     */
    protected function buildApp(\Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\CliControllerInterface
    {
        if (is_null($flags)) {
            $flags = $this->containerize();
        }
        $this->view = $this->getView('cli', null, $flags->toArray());
        $this->view->debug('Resolving passed arguments.');
        $resolved = $this->resolvePassedArguments($this->getBuildDefaults(), $flags->toArray());
        $this->view->debug(sprintf('Resolved arguments set as: [%1$s]', print_r($resolved, 1)));
        if (($flags->has('i') && $flags['i'] === true) || ($flags->has('interactive') && $flags['interactive'] === true)) {
            try {
                // Use the interactive shell to confirm arguments
                $resolved = $this->getInteractiveShellArguments('application', $resolved, $flags);
            } catch (\Oroboros\core\exception\cli\ExitException $e) {
                // Setup aborted
                $this->view->info('Setup aborted. No changes have been made.');
                return $this;
            } catch (\Oroboros\core\exception\ErrorException $e) {
                // Interactive mode error
                return $this->error(1, $e->getMessage(), null, $flags);
            }
        }
        $filesystem_definer = $this->load('library', 'definer\\php\\application\\FilesystemDefiner', null, $flags->toArray());
        try {
            $this->view->debug('Injecting supplied arguments into the filesystem definition.');
            $this->loadFilesystemDefinerVars($filesystem_definer, $resolved);
        } catch (\Oroboros\core\exception\ErrorException $e) {
            // Not enough keys provided
            return $this->error(1, $e->getMessage(), null, $flags);
        }
        $this->view->info('Supplied arguments satisfactory for application build.');
        $this->view->debug('Compiling application filesystem definition.');
        $filesystem_definition = $filesystem_definer->compile()->fetch();
        $app_data = $this->resolveFilesystem('application', $this->containerize($resolved), $filesystem_definition, $flags);
        // $app_data = all performed operations if needed for something.
        return $this;
    }

    /**
     * Builds a module using the command line interface
     * 
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $arguments
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $flags
     * @return \Oroboros\core\interfaces\controller\CliControllerInterface
     */
    protected function buildModule(\Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\CliControllerInterface
    {
        if (is_null($flags)) {
            $flags = $this->containerize();
        }
        $this->view = $this->getView('cli', null, $flags->toArray());
        $this->view->notice('Building a module :D');
        return $this;
    }

    /**
     * Builds an extension using the command line interface
     * 
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $arguments
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $flags
     * @return \Oroboros\core\interfaces\controller\CliControllerInterface
     */
    protected function buildExtension(\Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\CliControllerInterface
    {
        if (is_null($flags)) {
            $flags = $this->containerize();
        }
        $this->view = $this->getView('cli', null, $flags->toArray());
        $this->view->notice('Building an extension :D');
        return $this;
    }

    /**
     * Builds a service using the command line interface
     * 
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $arguments
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $flags
     * @return \Oroboros\core\interfaces\controller\CliControllerInterface
     */
    protected function buildService(\Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\CliControllerInterface
    {
        if (is_null($flags)) {
            $flags = $this->containerize();
        }
        $this->view = $this->getView('cli', null, $flags->toArray());
        $this->view->notice('Building a service :D');
        return $this;
    }

    /**
     * Builds a component using the command line interface
     * 
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $arguments
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $flags
     * @return \Oroboros\core\interfaces\controller\CliControllerInterface
     */
    protected function buildComponent(\Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\CliControllerInterface
    {
        if (is_null($flags)) {
            $flags = $this->containerize();
        }
        $this->view = $this->getView('cli', null, $flags->toArray());
        $this->view->notice('Building a component :D');
        return $this;
    }

    /**
     * Uses an interactive shell to confirm arguments from the user and returns the final resolved set.
     * 
     * @param string $type
     * @param array $existing
     * @param type $flags
     * @return array
     * @throws \Oroboros\core\exception\ErrorException
     *         if the interactive shell for `$type` is not implemented yet,
     *         or if the given type is not known.
     */
    private function getInteractiveShellArguments(string $type, array $existing, \Oroboros\core\interfaces\library\container\ContainerInterface $flags): array
    {
        switch ($type) {
            case 'application':
                $opts = $this->resolveInteractiveShellDefinitions('definer\\shellargs\\ApplicationInteractiveDefiner', $existing, $flags);
                break;
            case 'module':
            case 'extension':
            case 'service':
            case 'component':
                throw new \Oroboros\core\exception\ErrorException(
                        sprintf('Interactive mode is not implemented for build type [%1$s].', $type)
                );
            default:
                throw new \Oroboros\core\exception\ErrorException(
                        sprintf('Unknown build type [%1$s].', $type)
                );
        }
        return $this->getInteractiveShellResponse($type, $existing, $opts, $flags)->toArray();
    }

    /**
     * Launches an interactive shell and prompts the user for each subsequent argument,
     * creating or replacing any definitions that already existed based on their response.
     * 
     * @param string $type
     * @param array $existing
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $opts
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $flags
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     * @throws \Oroboros\core\exception\cli\ExitException
     *         If the user opts to terminate the operation. This will trigger
     *         the controller to exit the program with no changes.
     */
    private function getInteractiveShellResponse(string $type, array $existing, \Oroboros\core\interfaces\library\container\ContainerInterface $opts, \Oroboros\core\interfaces\library\container\ContainerInterface $flags): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $data = [];
        $this->view->debug(sprintf('Launching interactive shell for build type [%1$s]', $type));
        $view = $this->getView('cli-interactive-build', sprintf('%1$s-build-options', $type), $flags->toArray());
        foreach ($opts as $key => $opt) {
            $value = $view->getOpt($opt);
            $data[$key] = $value;
        }
        $data = array_replace_recursive($existing, $data);
        $this->view->debug(sprintf('Resolved arguments from interactive shell as [%1$s]', print_r($data, 1)));
        return $this->containerize($data);
    }

    /**
     * Resolves options for the interactive shell, which is used to create a
     * view that will guide the command line user through build setup.
     * 
     * @param string $definer the name of the definer used to determine
     *        what options to ask the user
     * @param array $existing any existing definitions automatically generated,
     *        handed in through explicit flags, or given through a definition template.
     *        These will act as the defaults for various argument options
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $flags
     *        Any flags passed in through cli when the request originated.
     *        These will be handed to the view to determine if any view options are relevant.
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     * @throws \Oroboros\core\exception\ErrorException If the view setup could not resolve.
     * @throws \Oroboros\core\exception\cli\ExitException If the user opts to
     *         abort the setup during the interactive shell portion of the logic.
     *         This will terminate the program without applying any changes.
     */
    private function resolveInteractiveShellDefinitions(string $definer, array $existing, \Oroboros\core\interfaces\library\container\ContainerInterface $flags): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $definer = $this->load('library', $definer, null, $flags->toArray());
        foreach ($existing as $key => $value) {
            if ($definer->hasRequiredKey($key)) {
                $definer->setRequiredKey($key, $value);
            }
            if ($definer->hasOptionalKey($key)) {
                $definer->setOptionalKey($key, $value);
            }
        }
        if (!$definer->canCompile()) {
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Could not compile interactive shell view definitions for [%1$s]', $definer)
            );
        }
        $definer->compile();
        return $definer->fetch();
    }

    /**
     * Returns defaults detected through environment.
     * These are expected to be overridden as required by preference.
     * 
     * @return array
     */
    private function getBuildDefaults(): array
    {
        $webserver_username = exec('echo "$(ps -ef | egrep \'(httpd|apache2|apache)\' | grep -v "$(whoami)" | grep -v root | head -n1 | awk \'{print $1}\')"');
        $webserver_usergroup = exec(sprintf('groups %1$s', $webserver_username));
        $defaults = [
            'path' => getcwd() . DIRECTORY_SEPARATOR,
            'core-path' => OROBOROS_CORE_BASE_DIRECTORY,
            'server-username' => OROBOROS_CORE_SERVER_USERNAME,
            'server-usergroup' => OROBOROS_CORE_SERVER_USERGROUP,
            'webserver-username' => $webserver_username,
            'webserver-usergroup' => $webserver_usergroup,
            'app' => true,
            'core' => false,
            'license-title' => false,
            'license' => false,
            'author' => false,
            'email' => false,
            'namespace' => null,
            'name' => null,
            'description' => null,
            'date' => (new \DateTime())->format('Y')
        ];
        return $defaults;
    }

    /**
     * 
     * @param array $defaults
     * @param array $flags
     * @return array
     */
    private function resolvePassedArguments(array $defaults, array $flags): array
    {
        $args = [];
        if (array_key_exists('from-config', $flags)) {
            $this->view->debug(sprintf('Attempting to load configuration data from provided file [%1$s]...', $flags['from-config']));
            $args = $this->resolveConfigFlagFile($flags['from-config']);
            unset($flags['from-config']);
        }
        foreach ($defaults as $key => $value) {
            if (array_key_exists($key, $flags)) {
                // Explicit flags have highest priority
                $args[$key] = $flags[$key];
                continue;
            }
            if (array_key_exists($key, $args)) {
                // Config file values have second priority
                continue;
            }
            // All others use default
            $args[$key] = $value;
        }
        return $args;
    }

    private function resolveConfigFlagFile(string $file): array
    {
        if (!is_readable($file)) {
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Provided config file [%1$s] does not exist or is not readable.', $file)
            );
        }
        $parsed = $this->load('library', 'parser\\JsonParser', $file)->fetch()->toArray();
        return $parsed;
    }

    /**
     * Injects the supplied arguments into the given filesystem definer
     * 
     * @param \Oroboros\core\interfaces\library\definer\DefinerInterface $definer
     * @param array $args
     * @return void
     * @throws \Oroboros\core\exception\ErrorException if not enough keys were satisfied to compile the definer
     */
    private function loadFilesystemDefinerVars(\Oroboros\core\interfaces\library\definer\DefinerInterface $definer, array $args): void
    {
        foreach ($args as $key => $value) {
            if ($definer->hasRequiredKey($key)) {
                $definer->setRequiredKey($key, $value);
            }
            if ($definer->hasOptionalKey($key)) {
                $definer->setOptionalKey($key, $value);
            }
        }
        if (!$definer->canCompile()) {
            $msg = sprintf('Cannot resolve filesystem, missing required build arguments [%1$s].', implode(', ', array_keys($definer->getUnsatisfiedRequiredKeys())));
            throw new \Oroboros\core\exception\ErrorException($msg);
        }
    }

    private function resolveFilesystem(string $type, \Oroboros\core\interfaces\library\container\ContainerInterface $arguments, \Oroboros\core\interfaces\library\container\ContainerInterface $filesystem, \Oroboros\core\interfaces\library\container\ContainerInterface $flags): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $this->view->info(sprintf('Resolving %1$s filesystem.', $type));
        return $this->createFilesystem($type, $arguments, $filesystem, $flags);
    }

    private function createFilesystem(string $type, \Oroboros\core\interfaces\library\container\ContainerInterface $arguments, \Oroboros\core\interfaces\library\container\ContainerInterface $filesystem, \Oroboros\core\interfaces\library\container\ContainerInterface $flags): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $start = microtime(true);
        $this->view->info(sprintf('Generating filesystem directories in [%1$s].', $arguments['path']));
        $operations = $this->recurseFilesystemDefinition($filesystem, $arguments['path'], [], $arguments, $flags);
        $end = microtime(true);
        $duration = $end - $start;
        $this->view->info(sprintf('Performed [%1$s] filesystem operations in [%2$s] seconds.', count($operations), $duration));
        return $this->containerize($operations);
    }

    private function recurseFilesystemDefinition(\Oroboros\core\interfaces\library\container\ContainerInterface $filesystem, string $path, array $operations, \Oroboros\core\interfaces\library\container\ContainerInterface $arguments, \Oroboros\core\interfaces\library\container\ContainerInterface $flags): array
    {
        if ($filesystem->has('directories')) {
            $this->view->info(sprintf('Generating filesystem directories in [%1$s].', $path));
            $operations = $this->generateFilesystemDirectories($filesystem['directories'], $path, $operations, $arguments, $flags);
        }
        if ($filesystem->has('files')) {
            $this->view->info(sprintf('Generating filesystem files in [%1$s].', $path));
            $operations = $this->generateFilesystemFiles($filesystem['files'], $path, $operations, $arguments, $flags);
        }
        return $operations;
    }

    private function generateFilesystemDirectories(\Oroboros\core\interfaces\library\container\ContainerInterface $directories, string $path, array $operations, \Oroboros\core\interfaces\library\container\ContainerInterface $arguments, \Oroboros\core\interfaces\library\container\ContainerInterface $flags): array
    {
        foreach ($directories as $key => $directory) {
            if (is_dir($dirname)) {
                $this->view->debug(sprintf('Directory [%1$s] already exists at [%2$s].', $directory['name'], $dirname));
            } else {
                $this->view->info(sprintf('Creating directory [%1$s] at [%2$s].', $directory['name'], $dirname));
                $dir = [
                    'type' => 'directory',
                    'method' => 'mkdir',
                    'name' => $dirname,
                    'fileperm' => $directory['fileperm'],
                    'file-owner' => $directory['file-owner'],
                    'file-group' => $directory['file-group'],
                ];
                if (!$flags->has('dryrun')) {
                    $dir['mode'] = 'created';
                    $this->view->debug(sprintf('Directory [%1$s] to be created with permission [%2$s] with user [%3$s] and group [%4$s].', $dirname, $directory['fileperm'], $directory['file-owner'], $directory['file-group']));
                    if (is_string($directory['fileperm'])) {
                        $directory['fileperm'] = intval($directory['fileperm'], 8);
                    }
                    mkdir($dirname, $directory['fileperm']);
                    if (chgrp($dirname, $directory['file-group'])) {
                        $this->view->debug(sprintf('File group for directory [%1$s] set to [%2$s] at location [%3$s].', $directory['name'], $directory['file-group'], $dirname));
                    } else {
                        $this->view->warning(sprintf('Unable to set file group for file [%1$s] as [%2$s] at location [%3$s].', $directory['name'], $directory['file-group'], $dirname));
                    }
                } else {
                    $dir['mode'] = 'emulated';
                    $this->view->info(sprintf('Skipped creating directory [%1$s] with permission [%2$s] with user [%3$s] and group [%4$s] because the dryrun flag was passed.', $dirname, $directory['fileperm'], $directory['file-owner'], $directory['file-group']));
                }
                $operations[] = $dir;
            }
            if ($directory->has('directories')) {
                $this->view->info(sprintf('Generating filesystem directories in [%1$s].', $dirname . DIRECTORY_SEPARATOR));
                $operations = $this->generateFilesystemDirectories($directory['directories'], $dirname . DIRECTORY_SEPARATOR, $operations, $arguments, $flags);
            }
            if ($directory->has('files')) {
                $this->view->info(sprintf('Generating filesystem files in [%1$s].', $dirname . DIRECTORY_SEPARATOR));
                $operations = $this->generateFilesystemFiles($directory['files'], $dirname . DIRECTORY_SEPARATOR, $operations, $arguments, $flags);
            }
        }
        return $operations;
    }

    private function generateFilesystemFiles(\Oroboros\core\interfaces\library\container\ContainerInterface $files, string $path, array $operations, \Oroboros\core\interfaces\library\container\ContainerInterface $arguments, \Oroboros\core\interfaces\library\container\ContainerInterface $flags): array
    {
        $writer = $this->load('library', 'file\\FileWriter', null, ['generate-directories' => true]);
        $writer->setDirectory($path);
        $this->view->debug(sprintf('Generating files for directory path [%1$s].', $path));
        foreach ($files as $key => $file) {
            $op = [
                'type' => $file['type'],
                'name' => $path . $file['filename'],
                'fileperm' => $file['fileperm'],
                'file-owner' => $file['file-owner'],
                'file-group' => $file['file-group'],
            ];
            if ($file['method'] === 'touch') {
                $content = '';
                $op['method'] = 'touch';
                $this->view->debug(sprintf('Create method for [%1$s] defined as [%2$s].', $path . $file['filename'], 'touch'));
            } elseif ($file['method'] === 'definer') {
                $op['method'] = 'component';
                $this->view->debug(sprintf('Create method for [%1$s] defined as [%2$s] with definer class [%3$s] and defined component [%4$s] and builder class [%5$s].', $path . $file['filename'], 'definer', get_class($file['definer']), get_class($file['component']), $file['builder']));
//                $data = $this->resolveDefiner($file['definer'], $arguments);
                foreach ($file['definer']->fetch() as $k => $v) {
                    $file['component']->addContext($k, $v);
                }
                $content = $file['component']->render();
            } elseif ($file['method'] === 'copy') {
                $op['method'] = 'copy';
                $this->view->debug(sprintf('Create method for [%1$s] defined as [%2$s] with source template [%3$s] located in base path [%4$s].', $path . $file['filename'], 'copy', $file['template'], $this->getArgument('modules')['scaffold']->path()));
                $op['template'] = $file['template'];
                $template = $this->getArgument('modules')['scaffold']->path() . $file['template'];
                if (is_dir($template)) {
                    $this->view->debug(sprintf('Could not complete operation. Specified template is a directory, not a file.'));
                    throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. Specified template [%2$s] is a directory.', get_class($this), $template));
                }
                if (!is_readable($template)) {
                    $this->view->debug(sprintf('Could not complete operation. Specified template is not readable or does not exist.'));
                    throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. Specified template [%2$s] does not exist or is not readable.', get_class($this), $template));
                }
                $content = file_get_contents($template);
            } else {
                $this->view->debug(sprintf('Could not complete operation. Specified render method [%2$s] is not known.', $file['method']));
                throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. Unknown render method [%2$s] for file key [%3$s] in path [%4$s].', get_class($this), $file['method'], $key, $path));
            }
            if (!$flags->has('dryrun')) {
                $op['mode'] = 'created';
                $this->view->debug(sprintf('File [%1$s] to be created with permission [%2$s] with user [%3$s] and group [%4$s].', $path . $file['filename'], $file['fileperm'], $file['file-owner'], $file['file-group']));
                $writer->setFile($file['filename']);
                if (is_string($file['fileperm'])) {
                    $file['fileperm'] = intval($file['fileperm'], 8);
                }
                $writer->setOption('file-permission', $file['fileperm']);
                $this->view->info(sprintf('Generating file [%1$s] at [%2$s].', $file['name'], $path . $file['filename']));
                $writer->write($content);
                if (chgrp($path . $file['filename'], $file['file-group'])) {
                    $this->view->debug(sprintf('File group for [%1$s] set to [%2$s] at location [%3$s].', $file['name'], $file['file-group'], $path . $file['filename']));
                } else {
                    $this->view->warning(sprintf('Unable to set file group for file [%1$s] as [%2$s] at location [%3$s]. Manual correction may be neccessary.', $file['name'], $file['file-group'], $path . $file['filename']));
                }
            } else {
                $op['mode'] = 'emulated';
                $this->view->info(sprintf('Skipped creating file [%1$s] with permission [%2$s] with user [%3$s] and group [%4$s] because the dryrun flag was passed.', $path . $file['filename'], $file['fileperm'], $file['file-owner'], $file['file-group']));
            }
            $operations[] = $op;
        }
        return $operations;
    }
}
