<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\coreBuild\controller\html;

/**
 * Build Controller
 * Provides Html relay to the underlying cli build logic securely for superusers
 *
 * @author Brian Dayhoff
 */
final class BuildController extends \Oroboros\core\abstracts\controller\AbstractPageController implements \Oroboros\core\interfaces\CoreInterface
{

    use \Oroboros\coreBuild\traits\controller\BuildControllerTrait;
    use \Oroboros\core\traits\html\HtmlModuleControllerUtilityTrait;

    /**
     * Use the standard html error controller
     */
    const ERROR_CONTROLLER = 'html\\ErrorController';
    const CLASS_SCOPE = 'frontend';
    const MODULE_NAME = 'build';

//    public function __construct(string $command = null, array $arguments = null, array $flags = null)
//    {
//        parent::__construct($command, $arguments, $flags);
////        d($this); exit;
//    }
//
//    public function update(\SplSubject $subject): void
//    {
////        if (strpos($subject->getObserverEvent(), 'event.register') !== false) {
////            d('Controller received observer event:', $subject->getObserverEvent());
////        }
//        parent::update($subject);
//    }
//
//    protected function setObserverEvent($event): void
//    {
////        d('Controller set observer event:', $event);
//        parent::setObserverEvent($event);
//    }
//
//    protected function handleEventCallbackError(string $queue, string $event_id, string $callback_id, \Exception $error, array $data, callable $callback, \Oroboros\core\interfaces\library\event\EventInterface $event, \Oroboros\core\interfaces\library\event\EventManagerInterface $manager): void
//    {
//        d('Handling an error...', $queue, $event_id, $callback_id, $error);
//        $this->markEventErrorRecoverable();
//        parent::handleEventCallbackError($queue, $event_id, $callback_id, $error, $data, $callback, $event, $manager);
//    }

    public function index(\Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null)
    {
//        $broker = $this->load('library', 'event\\EventBroker');
//        $this->importEventBroker($broker);
//        d(
//            $broker,
//            $broker->analyze($this),
//            $broker->resolve($this)->toArray(),
//            ($this instanceof \Oroboros\core\interfaces\library\event\EventAwareInterface)
//        );
//        exit;
//        try {
//            $event_manager = $this->getEventManager();
//            $this->registerEventQueue('event-floopsy');
//            $this->unregisterEventQueue('event-floopsy');
//            $this->registerFilterQueue('filter-floopsy');
//            $this->unregisterFilterQueue('filter-floopsy');
//            $this->registerEvent('noopsy', 'event-floopsy');
//            $this->unregisterEvent('noopsy', 'event-floopsy');
//            $this->registerFilter('noopsy', 'filter-floopsy');
//            $this->unregisterFilter('noopsy', 'filter-floopsy');
//            $this->registerEvent('noopsy', 'foo');
//            $this->registerEvent('noopsy', 'bar');
//            $this->registerEvent('noopsy', 'baz');
//            $this->registerFilter('noopsy', 'baz');
////            $this->bindEventCallback('bloopsy', 'doopsy', function (string $command = null, array $args = [], array $flags = []) {
////                throw new \Oroboros\core\exception\ErrorException('Whoopsie!');
////                return 'floopsy boopsie';
////            }, 'floopsy');
////            try {
////                $this->fireEventCallback('bloopsy', 'doopsy', [], 'floopsy');
////            } catch (\Exception $e) {
////                d('Event exception triggered');
////                $this->unbindEventCallback('bloopsy', 'doopsy', 'floopsy');
////            }
//            $this->registerEvent('hibbity');
//            $this->registerEvent('blarp');
//            $this->unregisterEvent('blarp');
//            $this->bindEventCallback('hibbity', 'bibbity', function (string $command = null, array $args = [], array $flags = []) {
//                return 'hibbity bibbity';
//            });
//            $this->bindEventCallback('hibbity', 'flibbity', function (string $command = null, array $args = [], array $flags = []) {
//                return 'hibbity bibbity flibbity';
//            });
//            $this->bindEventCallback('hibbity', 'whapow', function (string $command = null, array $args = [], array $flags = []) {
//                return 'WHAPOW!';
//            });
//            $test_args = [
//                'command' => 'this is a command',
//                'args' => $this->getArguments(),
//                'flags' => $this->getFlags(),
//            ];
//            $this->registerEvent('bibbity', 'shutdown');
//            d(
//                $this->hasEventQueue('init'),
//                $this->hasEventQueue('nope'),
//                $this->listEventQueues(),
//                $this->getCurrentEventQueue(),
//                $this->listEvents(),
//                $this->listEvents('shutdown'),
//                $this->listEventCallbacks('hibbity'),
//                $this->fireEventCallback('hibbity', 'bibbity', $test_args),
//                $this->fireEventCallback('hibbity', 'flibbity', $test_args),
//                $this->fireEventCallback('hibbity', 'whapow', $test_args),
//                $this->fireEvent('hibbity', $test_args),
//            );
//        } catch (\Exception $e) {
//            d($e->getMessage(), $e->getTrace());
//        }
//        exit;
//        phpinfo(); exit;
//        
        // Generic build data
//        $webserver_username = 'http';
//        $webserver_usergroup = 'http';
//        $defaults = [
//            'path' => getcwd() . DIRECTORY_SEPARATOR,
//            'core-path' => OROBOROS_CORE_BASE_DIRECTORY,
//            'server-username' => OROBOROS_CORE_SERVER_USERNAME,
//            'server-usergroup' => OROBOROS_CORE_SERVER_USERGROUP,
//            'webserver-username' => $webserver_username,
//            'webserver-usergroup' => $webserver_usergroup,
//            'app' => true,
//            'core' => false,
//            'license-title' => false,
//            'license' => false,
//            'author' => false,
//            'email' => false,
//            'namespace' => null,
//            'name' => null,
//            'date' => (new \DateTime())->format('Y')
//        ];
//        $test_data = array_replace_recursive($defaults, [
//            'name' => 'Test App',
//            'description' => 'This is just a test of the application builder.',
//            'namespace' => 'test\\app',
//        ]);
//        $builder = $this->load('library', 'component\\json\\ComponentBuilder');
//        $encoder = $this->load('component', 'json\\Json', 'defaults', [
//            'builder' => $builder,
//            'context' => $test_data
//        ]);
//        echo d($encoder->render()); exit;
//        
//        // Testing composer
//        $test_composer = $this->load('component', 'json\\config\\ComposerConfigComponent', 'test', [
//            'builder' => $builder
//        ], $this->getFlags());
//        foreach ($this->resolveDefiner('definer\\config\\php\\application\\ComposerDefiner', $this->containerize($test_data)) as $key => $value) {
//            $test_composer->addContext($key, $value);
//        }
//        
//        // Testing npm
//        $test_npm = $this->load('component', 'json\\config\\NpmConfigComponent', 'test', [
//            'builder' => $builder
//        ], $this->getFlags());
//        foreach ($this->resolveDefiner('definer\\config\\php\\application\\NpmDefiner', $this->containerize($test_data)) as $key => $value) {
//            $test_npm->addContext($key, $value);
//        }
//        
//        // Testing app config
//        $test_app = $this->load('component', 'json\\config\\app\\AppConfigComponent', 'test', [
//            'builder' => $builder
//        ], $this->getFlags());
//        foreach ($this->resolveDefiner('definer\\config\\php\\application\\AppDefiner', $this->containerize($test_data)) as $key => $value) {
//            $test_app->addContext($key, $value);
//        }
//        
//        // Results
//        d(
//            $test_composer,
//            $test_composer->render(),
//            $test_npm,
//            $test_npm->render(),
//            $test_app,
//            $test_app->render(),
//        );
//        exit;
        return $this->error(501, 'This functionality is not currently implemented.');
    }

    /**
     * Override this method to declare the default event queues
     * for the current class.
     * 
     * These will be registered with the event manager automatically.
     * 
     * @return array
     */
    protected function declareDefaultEventQueues(): array
    {
        return [
            'init',
            'ready',
            'error',
            'shutdown',
        ];
    }
}
