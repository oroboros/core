# Oroboros Build Module Documentation {#title}

- [Overview](#overview)

## Overview {#overview}

This module handles building applications, middleware, modules, extensions, services, and other complex groupings of programmatic logic automatically.

This module provides both a [web api][1] and a [command line api][2], however it should be noted that the _web api requires some additional setup to use_.

[1]: /documentation/modules/build/html/
[2]: /documentation/modules/build/cli/