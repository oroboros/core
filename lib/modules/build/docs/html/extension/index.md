# Oroboros Build Module Browser-Based Extension Builder Api Documentation {#title}

- [Overview](#overview)
    - [Prerequisites](#overview-prerequisites)

## Overview {#overview}

__note:__ _This functionality is not yet implemented._

This section outlines using the command line build utility to build an extension.

If you are looking for command line setup instructions, you may find them [here][1].

### Prerequisites {#overview-prerequisites}

Before you can use the web browser for build operations, you will need to set up a job queue and the build module websocket application.

The simplest way to accomplish this is with Beanstalkd as your job queue and Supervisord to monitor the websocket app.

[1]: /documentation/modules/build/cli/extension/