# Oroboros Build Module Cli Module Builder Api Documentation {#title}

- [Overview](#overview)

## Overview {#overview}

__note:__ _This functionality is not yet implemented._

This section outlines using the command line build utility to build a module.

If you are looking for web-based setup instructions, you may find them [here][1].

[1]: /documentation/modules/build/html/module/