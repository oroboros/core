# Oroboros Build Module Cli Api Documentation {#title}

- [Overview](#overview)

## Overview {#overview}

This section outlines using the command line build utility.

If you are looking for web-based setup instructions, you may find them [here][1].

[1]: /documentation/modules/build/html/