<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\coreDev\controller\html;

/**
 * Test Controller
 * Controller for testing. This will be deleted in the future.
 *
 * @author Brian Dayhoff
 */
final class TestController extends \Oroboros\core\abstracts\controller\AbstractPageController implements \Oroboros\core\interfaces\CoreInterface
{

    use \Oroboros\coreDev\traits\controller\DebugControllerTrait;

    /**
     * Use the standard html error controller
     */
    const ERROR_CONTROLLER = 'html\\ErrorController';
    const CLASS_SCOPE = 'frontend';
    const MODULE_NAME = 'dev';

    public function index()
    {
        $builder = $this->load('library', 'component\\javascript\\ComponentBuilder');
        $context = [
            'builder' => $builder,
        ];
        $fileopts = [
            'generate-directories' => true,
        ];
        // test file writing with directory scaffold
        $filepath = $this::app()->getApplicationPublicDirectory() . 'uploads/test-files/beep/boop/borp/write-test';
        $writer = $this->load('library', 'file\\FileWriter', $filepath, $fileopts);
        $content = 'this is some test content';
        $writer->write($content);
        // test streaming content
        $streampath = $this::app()->getApplicationRoot() . 'lib/modules/scaffold/etc/templates/ht/htaccess.no-exec.boilerplate';
        $writer->setFile('stream-test');
        $stream = fopen($streampath, 'r');
        $writer->stream($stream);
        fclose($stream);
        d($writer, file_get_contents($filepath));
        exit;
        $component = $this->load('component', 'javascript\\oroboros\\Component', 'test', $context);
        d($component, $component->render());
        exit;
    }
}
