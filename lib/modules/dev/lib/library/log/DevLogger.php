<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\coreDev\library\log;

/**
 * Dev Logger
 * Psr-3 logger for development.
 * Logs to a file that is overwritten on each subsequent request.
 * This log will always reflect only the last runtime in which it was used.
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
final class DevLogger extends \Oroboros\core\abstracts\library\log\AbstractFileLogger implements \Oroboros\core\interfaces\CoreInterface
{

    /**
     * Declare the logger scope as file
     */
    const LOGGER_SCOPE = 'dev';

    /**
     * Sets the prefix to the logger scope
     */
    const LOG_FILE_NAME_PREFIX = self::LOGGER_SCOPE;

    /**
     * Returns "dev.log"
     * 
     * @return string
     */
    protected function declareLogFileName(): string
    {
        return sprintf(
            '%1$s%2$s',
            static::LOG_FILE_NAME_PREFIX,
            static::LOG_FILE_NAME_SUFFIX
        );
    }
}
