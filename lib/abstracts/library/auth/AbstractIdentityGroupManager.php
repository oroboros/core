<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\auth;

/**
 * Abstract Identity Group Manager
 * Provides abstraction for identity group managers
 * 
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
abstract class AbstractIdentityGroupManager extends \Oroboros\core\abstracts\pattern\director\AbstractDirector implements \Oroboros\core\interfaces\library\auth\IdentityGroupManagerInterface
{

    use \Oroboros\core\traits\LoaderTrait;
    use \Oroboros\core\traits\library\auth\AuthCommonUtilityTrait;
    use \Oroboros\core\traits\library\auth\PermissionMatchableTrait;

    /**
     * This constant must be overridden by
     * concrete implementations to declare auth scope
     */
    const AUTH_SCOPE = null;

    /**
     * Use the default validator strategy unless otherwise specified.
     */
    const AUTH_VALIDATOR_STRATEGY_CLASS = 'auth\\ValidationStrategy';

    /**
     * Use the default authenticator strategy unless otherwise specified.
     */
    const AUTH_AUTHENTICATOR_STRATEGY_CLASS = 'auth\\AuthenticationStrategy';

    /**
     * This constant must be overridden to declare a
     * valid authentication identity group class or stub class name.
     */
    const AUTH_IDENTITY_GROUP_CLASS = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        $this->verifyManager();
        parent::__construct($command, $arguments, $flags);
        $this->initializeAuthCommon();
        $this->initializeManager();
    }

    public function group(string $group = null): \Oroboros\core\interfaces\library\auth\IdentityGroupInterface
    {
        return $this->load('library', static::AUTH_IDENTITY_GROUP_CLASS, $identity);
    }

    /**
     * This method may be overridden to perform any additional
     * initialization steps
     * 
     * @return void
     */
    protected function initializeManager(): void
    {
        // no-op
    }

    /**
     * Verifies that the manager class constant definitions are correct
     * 
     * @return void
     */
    private function verifyManager(): void
    {
        $this->verifyIdentityGroupClass();
    }

    /**
     * Checks that a valid identity class is defined.
     * Identity classes are mandatory.
     * 
     * @return void
     */
    private function verifyIdentityGroupClass(): void
    {
        $expected = \Oroboros\core\interfaces\library\auth\IdentityInterface::class;
        $this->preflightConstant('AUTH_IDENTITY_CLASS', $expected, true);
    }
}
