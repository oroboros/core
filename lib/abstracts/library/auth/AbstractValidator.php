<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\auth;

/**
 * Abstract Validator
 * Provides abstraction for authentication validators
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
abstract class AbstractValidator extends \Oroboros\core\abstracts\library\AbstractLibrary implements \Oroboros\core\interfaces\library\auth\ValidatorInterface
{

    use \Oroboros\core\traits\library\auth\AuthCommonUtilityTrait;

    /**
     * This constant must be overridden by
     * concrete implementations to declare auth scope
     */
    const AUTH_SCOPE = null;

    /**
     * Password must be a minimum length of this value
     */
    const AUTH_PASSWORD_MIN_LENGTH = 0;

    /**
     * Password cannot be more characters than this value
     */
    const AUTH_PASSWORD_MAX_LENGTH = 255;

    /**
     * This value [0-4] determines minimum password complexity.
     * It uses the zxcvbn library to check for easily
     * guessable patterns, where 0 is no complexity requirement
     * and 4 is maximum complexity requirement.
     */
    const AUTH_PASSWORD_MIN_COMPLEXITY = 0;

    /**
     * If not null, password must follow the given format
     */
    const AUTH_PASSWORD_REQUIRED_FORMAT = null;

    /**
     * Identity cannot be more characters than this value
     */
    const AUTH_IDENTITY_MIN_LENGTH = 0;

    /**
     * Identity cannot be more characters than this value
     */
    const AUTH_IDENTITY_MAX_LENGTH = 255;

    /**
     * If not null, password must follow the given format
     */
    const AUTH_IDENTITY_REQUIRED_FORMAT = null;

    /**
     * Token cannot be more characters than this value
     */
    const AUTH_TOKEN_MIN_LENGTH = 0;

    /**
     * Token cannot be more characters than this value
     */
    const AUTH_TOKEN_MAX_LENGTH = 255;

    /**
     * If not null, token must follow the given format
     */
    const AUTH_TOKEN_REQUIRED_FORMAT = null;

    /**
     * Nonce cannot be more characters than this value
     */
    const AUTH_NONCE_MIN_LENGTH = 0;

    /**
     * Nonce cannot be more characters than this value
     */
    const AUTH_NONCE_MAX_LENGTH = 255;

    /**
     * If not null, nonce must follow the given format
     */
    const AUTH_NONCE_REQUIRED_FORMAT = null;

    /**
     * Salt cannot be more characters than this value
     */
    const AUTH_SALT_MIN_LENGTH = 0;

    /**
     * Salt cannot be more characters than this value
     */
    const AUTH_SALT_MAX_LENGTH = 255;

    /**
     * If not null, salt must follow the given format
     */
    const AUTH_SALT_REQUIRED_FORMAT = null;

    /**
     * Private key cannot be more characters than this value
     */
    const AUTH_PRIVATE_KEY_MIN_LENGTH = 0;

    /**
     * Private key cannot be more characters than this value
     */
    const AUTH_PRIVATE_KEY_MAX_LENGTH = 255;

    /**
     * If not null, private key must follow the given format
     */
    const AUTH_PRIVATE_KEY_REQUIRED_FORMAT = null;

    /**
     * Public key cannot be more characters than this value
     */
    const AUTH_PUBLIC_KEY_MIN_LENGTH = 0;

    /**
     * Public key cannot be more characters than this value
     */
    const AUTH_PUBLIC_KEY_MAX_LENGTH = 255;

    /**
     * If not null, public key must follow the given format
     */
    const AUTH_PUBLIC_KEY_REQUIRED_FORMAT = null;

    /**
     * Key pair cannot be more characters than this value
     */
    const AUTH_KEY_PAIR_MIN_LENGTH = 0;

    /**
     * Key pair cannot be more characters than this value
     */
    const AUTH_KEY_PAIR_MAX_LENGTH = 255;

    /**
     * If not null, key pair must follow the given format
     */
    const AUTH_KEY_PAIR_REQUIRED_FORMAT = null;

    /**
     * Keychain cannot be more characters than this value
     */
    const AUTH_KEYCHAIN_MIN_LENGTH = 0;

    /**
     * Keychain cannot be more characters than this value
     */
    const AUTH_KEYCHAIN_MAX_LENGTH = 255;

    /**
     * If not null, keychain must follow the given format
     */
    const AUTH_KEYCHAIN_REQUIRED_FORMAT = null;

    /**
     * Represents any missing auth keys from the given credentials
     * 
     * this will be an indexed array of missing keys
     * 
     * @var array
     */
    private $missing = [];

    /**
     * Represents any invalid auth keys from the given credentials
     * 
     * this will be an indexed array of invalid keys
     * 
     * @var array
     */
    private $invalid = [];

    /**
     * Represents any error messages regarding auth key invalidity
     * 
     * This will be an associative array where the key is the auth
     * key name, and the value is an array of reasons the given key
     * is not valid.
     * 
     * @var array
     */
    private $errors = [];

    /**
     * Represents whether the last validated credentials are valid
     * 
     * @var bool
     */
    private $designation = true;

    /**
     * Represents whether validation has run
     * 
     * @var bool
     */
    private $is_validated = false;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeAuthCommon();
    }

    /**
     * Validates the given credentials.
     * 
     * This method will check if all
     * required auth credentials are
     * present, and validate each
     * individual one.
     * 
     * Returns true if **all** required credentials validate,
     * false if any are missing or malformed.
     * 
     * @note Validation only checks syntax, type, and format.
     *       If you want to verify validity, use the authenticator.
     *       The validator should always be called prior to calling
     *       an authenticator.
     * @param \Oroboros\core\interfaces\library\auth\CredentialContainerInterface $credentials
     * @return bool
     */
    public function validate(\Oroboros\core\interfaces\library\auth\CredentialContainerInterface $credentials): bool
    {
        $designation = true;
        $this->resetValidator();
        if (!$this->evaluateMissingKeys($credentials)) {
            $designation = false;
        }
        if (!$this->evaluateInvalidKeys($credentials)) {
            $designation = false;
        }
        $this->designation = $designation;
        $this->is_validated = true;
        return $designation;
    }

    /**
     * Returns whether the last run validate call was valid.
     * 
     * This will always return false if no validation has run.
     * 
     * @return bool
     */
    public function isValid(): bool
    {
        return $this->is_validated && $this->designation;
    }

    /**
     * Returns an array of any missing keys from the last validation call
     * 
     * @return array
     */
    public function getMissingKeys(): array
    {
        return $this->missing;
    }

    /**
     * Returns a boolean designation as to whether
     * the last validation call had any missing keys
     * 
     * @return bool
     */
    public function hasMissingKeys(): bool
    {
        return !empty($this->getMissingKeys());
    }

    /**
     * Returns an array of any invalid keys from the last validation call
     * 
     * @return array
     */
    public function getInvalidKeys(): array
    {
        return $this->invalid;
    }

    /**
     * Returns a boolean designation as to whether
     * the last validation call had any invalid keys
     * 
     * @return bool
     */
    public function hasInvalidKeys(): bool
    {
        return !empty($this->getInvalidKeys());
    }

    /**
     * Returns an array of any errors generated from the last validation call
     * 
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * Returns a boolean designation as to whether the last validation
     * call generated any errors
     * 
     * @return bool
     */
    public function hasErrors(): bool
    {
        return !empty($this->getErrors());
    }

    /**
     * Evaluates whether the given credentials are missing
     * any required credential keys
     * 
     * @param \Oroboros\core\interfaces\library\auth\CredentialContainerInterface $credentials
     * @return bool
     */
    protected function evaluateMissingKeys(\Oroboros\core\interfaces\library\auth\CredentialContainerInterface $credentials): bool
    {
        $designation = true;
        foreach ($this->getRequiredAuthSections() as $section) {
            if (!$credentials->has($section)) {
                $designation = false;
                $this->errorMessage($section, sprintf('Section %1$s is required.', $section));
            }
        }
        return $designation;
    }

    /**
     * Evaluates whether the given credentials have
     * any invalid credential keys
     * 
     * @param \Oroboros\core\interfaces\library\auth\CredentialContainerInterface $credentials
     * @return bool
     */
    protected function evaluateInvalidKeys(\Oroboros\core\interfaces\library\auth\CredentialContainerInterface $credentials): bool
    {
        $designation = true;
        if ($credentials->has('identity') && !$this->validateIdentity($credentials['identity'])) {
            $designation = false;
        }
        if ($credentials->has('password') && !$this->validatePassword($credentials['password'])) {
            $designation = false;
        }
        if ($credentials->has('hash') && !$this->validateHash($credentials['hash'])) {
            $designation = false;
        }
        if ($credentials->has('token') && !$this->validateToken($credentials['token'])) {
            $designation = false;
        }
        if ($credentials->has('salt') && !$this->validateSalt($credentials['salt'])) {
            $designation = false;
        }
        if ($credentials->has('nonce') && !$this->validateNonce($credentials['nonce'])) {
            $designation = false;
        }
        if ($credentials->has('private-key') && !$this->validatePrivateKey($credentials['private-key'])) {
            $designation = false;
        }
        if ($credentials->has('public-key') && !$this->validatePublicKey($credentials['public-key'])) {
            $designation = false;
        }
        if ($credentials->has('key-pair') && !$this->validateKeyPair($credentials['key-pair'])) {
            $designation = false;
        }
        if ($credentials->has('keychain') && !$this->validateKeychain($credentials['keychain'])) {
            $designation = false;
        }
        return $designation;
    }

    /**
     * Validates the given identity.
     * 
     * If `requiresValidIdentity` returns `false`,
     * this method will always return `true`.
     * 
     * @param \Oroboros\core\interfaces\library\auth\IdentityInterface $identity
     * @return bool
     */
    protected function validateIdentity(\Oroboros\core\interfaces\library\auth\IdentityInterface $identity): bool
    {
        if (!$this->requiresValidIdentity()) {
            return true;
        }
        if (!$this->checkObjectAuthScope($identity)) {
            return false;
        }
        return true;
    }

    /**
     * Validates the given password.
     * 
     * If `requiresValidPassword` returns `false`,
     * this method will always return `true`.
     * 
     * @param \Oroboros\core\interfaces\library\auth\PasswordInterface $password
     * @return bool
     */
    protected function validatePassword(\Oroboros\core\interfaces\library\auth\PasswordInterface $password): bool
    {
        if (!$this->requiresValidPassword()) {
            return true;
        }
        if (!$this->checkObjectAuthScope($password)) {
            return false;
        }
        if (static::AUTH_PASSWORD_MIN_COMPLEXITY > 0) {
            $check = new \ZxcvbnPhp\Zxcvbn();
            $complexity =  $check->passwordStrength($password->get());
            if ($complexity['score'] < static::AUTH_PASSWORD_MIN_COMPLEXITY) {
                if (isset($complexity['feedback']['warning'])) {
                    $this->errorMessage('password', $complexity['feedback']['warning']);
                } else {
                    $this->errorMessage('password', 'Supplied password is too easy to guess.');
                }
                return false;
            } 
        }
        return true;
    }

    /**
     * Validates the given hash.
     * 
     * If `requiresValidHash` returns `false`,
     * this method will always return `true`.
     * 
     * @param \Oroboros\core\interfaces\library\auth\PasswordHashInterface $hash
     * @return bool
     */
    protected function validateHash(\Oroboros\core\interfaces\library\auth\PasswordHashInterface $hash): bool
    {
        if (!$this->requiresValidHash()) {
            return true;
        }
        if (!$this->checkObjectAuthScope($hash)) {
            return false;
        }
        return true;
    }

    /**
     * Validates the given token.
     * 
     * If `requiresValidToken` returns `false`,
     * this method will always return `true`.
     * 
     * @param \Oroboros\core\interfaces\library\auth\TokenInterface $token
     * @return bool
     */
    protected function validateToken(\Oroboros\core\interfaces\library\auth\TokenInterface $token): bool
    {
        if (!$this->requiresValidToken()) {
            return true;
        }
        if (!$this->checkObjectAuthScope($token)) {
            return false;
        }
        return true;
    }

    /**
     * Validates the given nonce.
     * 
     * If `requiresValidNonce` returns `false`,
     * this method will always return `true`.
     * 
     * @param \Oroboros\core\interfaces\library\auth\NonceInterface $nonce
     * @return bool
     */
    protected function validateNonce(\Oroboros\core\interfaces\library\auth\NonceInterface $nonce): bool
    {
        if (!$this->requiresValidNonce()) {
            return true;
        }
        if (!$this->checkObjectAuthScope($nonce)) {
            return false;
        }
        return true;
    }

    /**
     * Validates the given salt.
     * 
     * If `requiresValidSalt` returns `false`,
     * this method will always return `true`.
     * 
     * @param \Oroboros\core\interfaces\library\auth\SaltInterface $salt
     * @return bool
     */
    protected function validateSalt(\Oroboros\core\interfaces\library\auth\SaltInterface $salt): bool
    {
        if (!$this->requiresValidSalt()) {
            return true;
        }
        if (!$this->checkObjectAuthScope($salt)) {
            return false;
        }
        return true;
    }

    /**
     * Validates the given public key.
     * 
     * If `requiresValidPublicKey` returns `false`,
     * this method will always return `true`.
     * 
     * @param \Oroboros\core\interfaces\library\auth\PublicKeyInterface $key
     * @return bool
     */
    protected function validatePublicKey(\Oroboros\core\interfaces\library\auth\PublicKeyInterface $key): bool
    {
        if (!$this->requiresValidPublicKey()) {
            return true;
        }
        if (!$this->checkObjectAuthScope($key)) {
            return false;
        }
        return true;
    }

    /**
     * Validates the given private key.
     * 
     * If `requiresValidPrivateKey` returns `false`,
     * this method will always return `true`.
     * 
     * @param \Oroboros\core\interfaces\library\auth\PrivateKeyInterface $key
     * @return bool
     */
    protected function validatePrivateKey(\Oroboros\core\interfaces\library\auth\PrivateKeyInterface $key): bool
    {
        if (!$this->requiresValidPrivateKey()) {
            return true;
        }
        if (!$this->checkObjectAuthScope($key)) {
            return false;
        }
        return true;
    }

    /**
     * Validates the given key pair.
     * 
     * If `requiresValidKeyPair` returns `false`,
     * this method will always return `true`.
     * 
     * @param \Oroboros\core\interfaces\library\auth\KeyPairInterface $keys
     * @return bool
     */
    protected function validateKeyPair(\Oroboros\core\interfaces\library\auth\KeyPairInterface $keys): bool
    {
        if (!$this->requiresValidKeyPair()) {
            return true;
        }
        if (!$this->checkObjectAuthScope($keys)) {
            return false;
        }
        return true;
    }

    /**
     * Validates the given keychain.
     * 
     * If `requiresValidKeychain` returns `false`,
     * this method will always return `true`.
     * 
     * @param \Oroboros\core\interfaces\library\auth\KeychainInterface $keychain
     * @return bool
     */
    protected function validateKeychain(\Oroboros\core\interfaces\library\auth\KeychainInterface $keychain): bool
    {
        if (!$this->requiresValidKeychain()) {
            return true;
        }
        if (!$this->checkObjectAuthScope($keychain)) {
            return false;
        }
        return true;
    }

    /**
     * Override this method and return true if
     * the validator requires a valid identity
     * 
     * @return bool
     */
    protected function requiresValidIdentity(): bool
    {
        return false;
    }

    /**
     * Override this method and return true if
     * the validator requires a valid password
     * 
     * @return bool
     */
    protected function requiresValidPassword(): bool
    {
        return false;
    }

    /**
     * Override this method and return true if
     * the validator requires a valid hash
     * 
     * @return bool
     */
    protected function requiresValidHash(): bool
    {
        return false;
    }

    /**
     * Override this method and return true if
     * the validator requires a valid token
     * 
     * @return bool
     */
    protected function requiresValidToken(): bool
    {
        return false;
    }

    /**
     * Override this method and return true if
     * the validator requires a valid nonce
     * 
     * @return bool
     */
    protected function requiresValidNonce(): bool
    {
        return false;
    }

    /**
     * Override this method and return true if
     * the validator requires a valid salt
     * 
     * @return bool
     */
    protected function requiresValidSalt(): bool
    {
        return false;
    }

    /**
     * Override this method and return true if
     * the validator requires a valid public key
     * 
     * @return bool
     */
    protected function requiresValidPublicKey(): bool
    {
        return false;
    }

    /**
     * Override this method and return true if
     * the validator requires a valid private key
     * 
     * @return bool
     */
    protected function requiresValidPrivateKey(): bool
    {
        return false;
    }

    /**
     * Override this method and return true if
     * the validator requires a valid key pair
     * 
     * @return bool
     */
    protected function requiresValidKeyPair(): bool
    {
        return false;
    }

    /**
     * Override this method and return true if
     * the validator requires a valid keychain
     * 
     * @return bool
     */
    protected function requiresValidKeychain(): bool
    {
        return false;
    }
    
    protected function validateCleanString(string $type, string $string): bool
    {
        $designation = true;
        
        return $designation;
    }

    /**
     * Buffers an error message for a failed validation result
     * 
     * @param string $key
     * @param string $message
     * @return void
     */
    protected function errorMessage(string $key, string $message): void
    {
        if (!array_key_exists($key, $this->errors)) {
            $this->errors[$key] = [];
        }
        $this->errors[$key][] = $message;
    }

    /**
     * Resets the validator to a clean state.
     * This is always called prior to running any new validation.
     * 
     * @return void
     */
    protected function resetValidator(): void
    {
        $this->resetMissing();
        $this->resetInvalid();
        $this->resetErrors();
        $this->designation = true;
        $this->is_validated = false;
    }

    /**
     * Checks that the given object matches scope
     * 
     * @param \Oroboros\core\interfaces\library\auth\AuthInterface $object
     * @return bool
     */
    protected function checkObjectAuthScope(\Oroboros\core\interfaces\library\auth\AuthInterface $object): bool
    {
        if ($this::AUTH_SCOPE !== $object::AUTH_SCOPE) {
            return false;
        }
        return true;
    }

    protected function checkObjectLength(\Oroboros\core\interfaces\library\auth\AuthInterface $object): bool
    {
        $designation = true;
        $slug = $this->getObjectTypeSlug($object);
        if ($slug === 'auth') {
            return true;
        }
        $min = constant(sprintf('%1$s::AUTH_%2$s_MIN_LENGTH', get_class($this), str_replace('-', '_', strtoupper($slug))));
        $max = constant(sprintf('%1$s::AUTH_%2$s_MAX_LENGTH', get_class($this), str_replace('-', '_', strtoupper($slug))));

        $test = (string) $object;
        $length = strlen($test);

        if ($length < $min) {
            $this->errorMessage($slug, sprintf('%1$s must be at least %2$s characters long.', ucfirst(str_replace('-', ' ', $slug)), $min));
            $designation = false;
        }
        if ($length > $max) {
            $this->errorMessage($slug, sprintf('%1$s cannot be longer than %2$s characters long.', ucfirst(str_replace('-', ' ', $slug)), $max));
            $designation = false;
        }
        return $designation;
    }

    /**
     * Checks object format, if a format is declared for the type.
     * 
     * Objects of type "auth" always return true. This keyword is reserved
     * for auth objects that perform programmatic tasks, and consequently
     * do not need to be validated.
     * 
     * @param \Oroboros\core\interfaces\library\auth\AuthInterface $object
     * @return bool
     */
    protected function checkObjectFormat(\Oroboros\core\interfaces\library\auth\AuthInterface $object): bool
    {
        $slug = $this->getObjectTypeSlug($object);
        if ($slug === 'auth') {
            return true;
        }
        $format = constant(sprintf('%1$s::AUTH_%2$s_REQUIRED_FORMAT', get_class($this), str_replace('-', '_', strtoupper($slug))));
        if (is_null($format)) {
            return true;
        }
        $method = $this->getAuthFormatCallbacks($format);
        return $this->$method($object);
    }

    /**
     * Gets the string representation of the object auth type identifier
     * 
     * @param \Oroboros\core\interfaces\library\auth\AuthInterface $object
     * @return string
     */
    protected function getObjectTypeSlug(\Oroboros\core\interfaces\library\auth\AuthInterface $object): string
    {
        $slug = 'auth';
        switch ($object) {
            case ($object instanceof \Oroboros\core\interfaces\library\auth\IdentityInterface):
                return 'identity';
            case ($object instanceof \Oroboros\core\interfaces\library\auth\IdentityInterface):
                return 'password';
            case ($object instanceof \Oroboros\core\interfaces\library\auth\IdentityInterface):
                return 'hash';
            case ($object instanceof \Oroboros\core\interfaces\library\auth\IdentityInterface):
                return 'token';
            case ($object instanceof \Oroboros\core\interfaces\library\auth\IdentityInterface):
                return 'salt';
            case ($object instanceof \Oroboros\core\interfaces\library\auth\IdentityInterface):
                return 'nonce';
            case ($object instanceof \Oroboros\core\interfaces\library\auth\IdentityInterface):
                return 'private-key';
            case ($object instanceof \Oroboros\core\interfaces\library\auth\IdentityInterface):
                return 'public-key';
            case ($object instanceof \Oroboros\core\interfaces\library\auth\IdentityInterface):
                return 'key-pair';
            case ($object instanceof \Oroboros\core\interfaces\library\auth\IdentityInterface):
                return 'keychain';
        }
        return $slug;
    }

    /**
     * Returns the callback method registered for the designated type
     * 
     * @param string $type
     * @return string
     * @throws \Oroboros\core\exception\ErrorException
     * @throws \Oroboros\core\exception\core\InvalidClassException
     */
    protected function getAuthFormatCallbacks(string $type): string
    {
        $callbacks = $this->declareAuthFormatCallbacks();
        if (!array_key_exists($type, $callbacks)) {
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. '
                        . 'Provided callback key [%2$s] does '
                        . 'not exist in this scope.'
                        , get_class($this), $key)
            );
        }
        $callback = $callbacks[$key];
        if (!method_exists($this, $callback)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. '
                        . 'Provided callback key [%2$s] '
                        . 'was declared but does not '
                        . 'resolve to a callable method '
                        . 'in this class. This class is '
                        . 'not useable in it\'s current state.'
                        , get_class($this), $key)
            );
        }
        return $callback;
    }

    /**
     * Override this method to declare callbacks for auth keys.
     * 
     * This should return an associative array where the key is the format
     * and the value is a callback method within this object that will handle
     * checking the format.
     * 
     * Format check callbacks must return boolean.
     * 
     * @return array
     */
    protected function declareAuthFormatCallbacks(): array
    {
        return [];
    }

    /**
     * Resets the missing key buffer
     * 
     * @return void
     */
    private function resetMissing(): void
    {
        $this->missing = [];
    }

    /**
     * Resets the invalid key buffer
     * 
     * @return void
     */
    private function resetInvalid(): void
    {
        $this->invalid = [];
    }

    /**
     * Resets the error message buffer
     * 
     * @return void
     */
    private function resetErrors(): void
    {
        $this->errors = [];
    }

    /**
     * Returns the auth sections that require validation
     * 
     * @return array
     */
    private function getRequiredAuthSections(): array
    {
        $results = [];
        $sections = [
            'identity' => $this->requiresValidIdentity(),
            'password' => $this->requiresValidPassword(),
            'hash' => $this->requiresValidHash(),
            'token' => $this->requiresValidToken(),
            'nonce' => $this->requiresValidNonce(),
            'salt' => $this->requiresValidSalt(),
            'public-key' => $this->requiresValidPublicKey(),
            'private-key' => $this->requiresValidPrivateKey(),
            'key-pair' => $this->requiresValidKeyPair(),
            'keychain' => $this->requiresValidKeychain(),
        ];
        foreach ($sections as $key => $value) {
            if ($value === true) {
                $results[] = $key;
            }
        }
        return $results;
    }
}
