<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\auth;

/**
 * Abstract Permission
 * Provides abstraction for generating and validating permission nodes
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
abstract class AbstractPermission extends \Oroboros\core\abstracts\library\entity\AbstractEntity implements \Oroboros\core\interfaces\library\auth\PermissionInterface
{

    use \Oroboros\core\traits\model\ModelAwareUtilityTrait;
    use \Oroboros\core\traits\library\auth\AuthCommonUtilityTrait;
    use \Oroboros\core\traits\library\auth\PermissionMatchableTrait;

    /**
     * This constant must be overridden by
     * concrete implementations to declare auth scope
     */
    const AUTH_SCOPE = null;

    /**
     * This constant may be overridden to declare a permission
     * type to use for the permission object.
     * The permission type corresponds to a field of personally
     * identifying information that can be confirmed
     * to be a 1:1 match with a user account and is
     * known by the end user.
     * 
     * Some typical examples of permission representations are username,
     * email, phone number, account id, or any other unique piece of
     * information identifying the account that is not shared
     * across accounts.
     */
    const AUTH_IDENTITY_TYPE = 'permission';

    /**
     * Declares the entity type as permission
     */
    const ENTITY_TYPE = self::AUTH_IDENTITY_TYPE;

    /**
     * Declares the entity scope as auth
     */
    const ENTITY_SCOPE = 'auth';

    /**
     * The type of permission 
     */
    const PERMISSION_TYPE = self::AUTH_IDENTITY_TYPE;

    /**
     * Optional permission scope
     */
    const PERMISSION_SCOPE = null;

    /**
     * Optional permission category
     */
    const PERMISSION_CATEGORY = null;

    /**
     * Optional permission subcategory
     */
    const PERMISSION_SUBCATEGORY = null;

    /**
     * Optional permission context
     */
    const PERMISSION_CONTEXT = null;

    /**
     * This constant may be overridden to add a fixed prefix to permissions.
     * The default behavior is no prefix
     */
    const PERMISSION_PREFIX = null;

    /**
     * This constant may be overridden to add a fixed suffix to permissions.
     * The default behavior is no suffix
     */
    const PERMISSION_SUFFIX = null;

    /**
     * This constant may be overridden to change the separator
     * used for generating permission nodes
     * 
     * Altering the separator will make permissions incompatible
     * with other formats, but exists as a vehicle to map
     * permissions to more generalized purposes or maintain
     * interoperability between implementations
     * 
     * This is left to further implementation
     */
    const PERMISSION_SEPARATOR = '.';

    /**
     * This constant may be overridden to change the permission wildcard pattern
     * used to authenticate wildcard matches
     * 
     * Altering the wildcard will make wildcarding permissions incompatible
     * with other formats but will not alter permission literal matches.
     * 
     * This exists as a vehicle to map permissions to more generalized purposes
     * or maintain interoperability between implementations
     * 
     * This is left to further implementation
     */
    const PERMISSION_WILDCARD = '*';

    /**
     * The master set of permissions
     * 
     * @var \Oroboros\core\interfaces\library\auth\PermissionContainerInterface
     */
    private static $master_set = null;

    /**
     * The parent permission, if any
     * 
     * @var \Oroboros\core\interfaces\library\auth\PermissionInterface|null
     */
    private $parent = null;

    /**
     * A set of child permission nodes, if any
     * 
     * @var \Oroboros\core\interfaces\library\auth\PermissionContainerInterface|null
     */
    private $children = null;

    /**
     * The literal representation of the permission name
     * @var string
     */
    private $permission = null;

    /**
     * The readable name of the permission, if one is defined.
     */
    private $name = null;

    /**
     * The readable description of the permission, if one is defined.
     */
    private $description = null;

    /**
     * An optional security key for the permission.
     * 
     * The key is used to resolve passed group sets and check if they were
     * correctly granted access to the permission
     * 
     * @var \Oroboros\core\interfaces\library\auth\KeyInterface
     */
    private $key = null;

    /**
     * If the permission is whitelisted to only be added to specific groups,
     * this will contain a list of those groups
     * 
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface|null
     */
    private $whitelist = null;

    /**
     * Whether or not the permission itself is marked as baseline
     * 
     * Baseline permissions are root category
     * permissions that do not have parents.
     * 
     * Any baseline permission will not accept a parent being set for it.
     * 
     * @var bool
     */
    private $baseline = false;

    /**
     * Whether or not the permission itself is marked final
     * 
     * Final permissions cannot be used as parents for
     * other permission nodes
     * 
     * Any final permission will not accept any children.
     * 
     * @var bool
     */
    private $final = false;

    /**
     * Whether or not the permission itself is enabled.
     * 
     * Disabling a permission will disable all of it's children.
     * Children still track their own enable value separately,
     * so re-enabling a permission will result in the original
     * permission set resolving identically to however it did
     * before the ancestor was disabled.
     * 
     * @var bool
     */
    private $enabled = true;

    /**
     * Whether or not the permission object is locked
     * Locked permissions cannot be edited or mutated
     * and are considered readonly.
     * 
     * Anchor permissions for subset families are
     * generally locked, as are root permissions.
     * 
     * @var bool
     */
    private $locked = false;

    /**
     * Whether or not the permission object is hidden.
     * Hidden permissions do not display in the permission
     * index or produce gui-edit pages without explicitly
     * granting permission to view and edit hidden permissions.
     * 
     * @var bool
     */
    private $hidden = false;

    /**
     * Whether or not the permission object is a system permission
     * System permissions cannot be edited through the gui, because
     * they will break important things if altered.
     * 
     * @var bool
     */
    private $system = false;

    /**
     * Determines whether the permission is initialized
     * 
     * @var bool
     */
    private $is_initialized = false;

    /**
     * Returns a copy of the permission master set.
     * The original set is preserved, this does not provide direct access
     * to the internal permission master set, only a copy of it.
     * 
     * @return \Oroboros\core\interfaces\library\auth\PermissionContainerInterface
     */
    public static function index(): \Oroboros\core\interfaces\library\auth\PermissionContainerInterface
    {
        $perms = self::$master_set->toArray();
        return static::containerizeInto(\Oroboros\core\library\auth\PermissionContainer::class, $perms);
    }

    /**
     * Imports a container of permission definitions into the master set
     * 
     * @param \Oroboros\core\interfaces\library\auth\PermissionContainerInterface $perms
     * @return void
     */
    public static function import(\Oroboros\core\interfaces\library\auth\PermissionContainerInterface $perms): void
    {
        foreach ($perms as $key => $perm) {
            static::register($perm);
        }
    }

    /**
     * Registers a permission node with the master set
     * 
     * @param \Oroboros\core\interfaces\library\auth\PermissionInterface $permission
     * @return void
     */
    public static function register(\Oroboros\core\interfaces\library\auth\PermissionInterface $permission): void
    {
        self::$master_set[$permission->get()] = $permission;
    }

    /**
     * Unregisters a permission from the master set.
     * To unregister, the exact same permission that exists
     * in the master set must be passed.
     * 
     * @param \Oroboros\core\interfaces\library\auth\PermissionInterface $permission
     * @oaran string|null $key
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException
     *         If the given permission identifier exists in the master set,
     *         but the given permission is not the originally registered permission.
     */
    public static function unregister(\Oroboros\core\interfaces\library\auth\PermissionInterface $permission, string $key = null): void
    {
        if (self::$master_set->has($permission->get()) && self::$master_set[$permission->get()] !== $permission) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Permission mismatch for permission [%2$s]. '
                        . 'Unregistration requires passing the same permission that is registered, '
                        . 'copies cannot be used to unregister the original.', static::class, $permission->get())
            );
        }
        if (self::$master_set->has($permission->get()) && self::$master_set[$permission->get()] === $permission) {
            unset(self::$master_set[$permission->get()]);
        }
    }

    /**
     * Permission nodes use the default constructor.
     * The value of `$command` will become the base permission id,
     * unless arguments explicitly define one.
     * 
     * Arguments will be parsed as permission properties
     * 
     * @param string $command
     * @param array $arguments
     * @param array $flags
     */
    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeAuthCommon();
        $this->initializePermission();
        if (!self::$master_set->has($this->get())) {
            // Only originals will be auto-registered
            static::register($this);
        }
    }

    /**
     * Casts the object to a string representation of itself
     * 
     * @return string
     */
    public function __toString(): string
    {
        return $this->get();
    }

    /**
     * Casts the permission object to a storable string representation
     * 
     * @return string|null
     */
    public function serialize(): ?string
    {
        return json_encode($this->details());
    }

    /**
     * Restores a serialized permission to it's object representation
     * 
     * @param string $data
     * @return void
     */
    public function unserialize(string $data): void
    {
        $details = json_decode($data, 1);
        $this->validatePermission($data['permission']);
        $this->permission = $data['permission'];
        $this->enabled = $data['enabled'];
        $this->locked = $data['locked'];
        $this->is_initialized = $data['initialized'];
    }

    /**
     * Returns whether or not the permission node is initialized
     * 
     * @return bool
     */
    public function isInitialized(): bool
    {
        return $this->is_initialized;
    }

    /**
     * Returns the parent permission, if one exists
     * 
     * @return \Oroboros\core\interfaces\library\auth\PermissionInterface|null
     */
    public function parent(): ?\Oroboros\core\interfaces\library\auth\PermissionInterface
    {
        return $this->parent;
    }

    /**
     * 
     * @param \Oroboros\core\interfaces\library\auth\PermissionInterface $parent
     * @return void
     * @throws \Oroboros\core\exception\OutOfBoundsException
     *         If the permission is baseline, indicating it cannot have a parent.
     */
    public function setParent(\Oroboros\core\interfaces\library\auth\PermissionInterface $parent): void
    {
        if ($this->isBaseline()) {
            throw new \Oroboros\core\exception\OutOfBoundsException(
                    sprintf('Error encountered in [%1$s]. '
                        . 'Baseline permission [%2$s] '
                        . 'cannot have accept a parent. '
                        . 'Attempted to set parent node [%3$s].'
                        , get_class($this), $this->get(), $parent->get())
            );
        }
        if ($parent === $this->parent) {
            // Already set, do not duplicate operation
            return;
        }
        $this->parent = $parent;
        $parent->addChild($this);
    }

    /**
     * Matches a given permission name check against the current permission.
     * This is the method used by permission nodes to determine access.
     * 
     * @param string $permission
     * @return bool
     */
    public function match(string $permission): bool
    {
        if (!$this->isEnabled()) {
            // Disabled permissions never match
            return false;
        }
        if ($permission === $this->get()) {
            // Direct match
            return true;
        }
        if ($permission === $this->getPermissionWildcard()) {
            // Global wildcard match
            return true;
        }
        if ($permission === $this->generateScopedWildcard()) {
            // Scoped wildcard match
            return true;
        }
        $parent = $this->parent();
        if (!is_null($parent)) {
            // Parent permission will handle it
            return $parent->match($permission);
        }
        // All other cases
        return false;
    }

    /**
     * Returns the child permissions of the current permission
     * 
     * @return \Oroboros\core\interfaces\library\auth\PermissionContainerInterface|null
     */
    public function children(): ?\Oroboros\core\interfaces\library\auth\PermissionContainerInterface
    {
        if (is_null($this->children)) {
            return null;
        }
        return $this->containerizeInto(\Oroboros\core\library\auth\PermissionContainer::class, $this->permissions->toArray());
    }

    /**
     * Adds a child permission to this permission
     * 
     * @param \Oroboros\core\interfaces\library\auth\PermissionInterface $child
     * @return void
     */
    public function addChild(\Oroboros\core\interfaces\library\auth\PermissionInterface $child): void
    {
        $this->initializeChildren();
        $key = $child->get();
        if ($this->children->has($key)) {
            // already set, do not overwrite
            return;
        }
        $this->children[$key] = $child;
        $child->setParent($this);
    }

    /**
     * The internal identifier for the permission
     * 
     * @return string
     */
    public function id(): string
    {
        return $this->permission;
    }

    /**
     * The human readable name of the permission, if any
     * 
     * @return string|null
     */
    public function name(): ?string
    {
        return $this->name;
    }

    /**
     * The human readable description of the permission, if any
     * 
     * @return string|null
     */
    public function description(): ?string
    {
        return $this->description;
    }

    /**
     * Returns the permission key if one exists.
     * 
     * Permissions with keys can be validated with the correct hash
     * to prevent unauthorized assignment.
     * 
     * @return \Oroboros\core\interfaces\library\auth\KeyInterface|null
     */
    public function key(): ?\Oroboros\core\interfaces\library\auth\KeyInterface
    {
        return $this->key;
    }

    /**
     * Returns the whitelist if one exists.
     * 
     * Whitelisted permissions can only be added
     * to the permission groups they define in their whitelist.
     * 
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface|null
     */
    public function whitelist(): ?\Oroboros\core\interfaces\library\container\ContainerInterface
    {
        return $this->whitelist;
    }

    /**
     * Returns whether or not this permission is baseline.
     * 
     * Baseline permissions are the underlying parent permissions
     * of entire categories of permissions.
     * 
     * @return bool
     */
    public function isBaseline(): bool
    {
        return $this->baseline;
    }

    /**
     * Returns whether or not this permission is currently enabled.
     * This will return `false` if this permission is disabled, or any
     * ancestor permission is disabled.
     * 
     * @return bool
     */
    public function isEnabled(): bool
    {
        $parent = $this->parent();
        if (!is_null($parent) && !$parent->isEnabled()) {
            // Disabled by parent
            return false;
        }
        return $this->enabled;
    }

    /**
     * Returns whether or not this permission is final.
     * 
     * Final permissions cannot be assigned as parents of other permissions.
     * @return bool
     */
    public function isFinal(): bool
    {
        return $this->final;
    }

    /**
     * Returns whether or not the permission is locked.
     * 
     * Locked permissions cannot be edited further.
     * @return bool
     */
    public function isLocked(): bool
    {
        return $this->locked;
    }

    /**
     * Returns an array of information about the permission
     * 
     * @return array
     */
//    public function details(): array
    public function details(): ?\Oroboros\core\interfaces\library\entity\EntityDetailCollectionInterface
    {
        return parent::details();
        return [
            'parent' => $this->parent(),
            'permission' => $this->get(),
            'literal' => $this->permission,
            'type' => $this->getPermissionType(),
            'scope' => $this->getPermissionScope(),
            'category' => $this->getPermissionCategory(),
            'subcategory' => $this->getPermissionSubCategory(),
            'context' => $this->getPermissionContext(),
            'prefix' => $this->getPermissionPrefix(),
            'suffix' => $this->getPermissionSuffix(),
            'separator' => $this->getPermissionSeparator(),
            'name' => $this->name(),
            'description' => $this->description(),
            'key' => $this->key(),
            'whitelist' => $this->whitelist(),
            'wildcard' => $this->getPermissionWildcard(),
            'baseline' => $this->isBaseline(),
            'final' => $this->isFinal(),
            'enabled' => $this->isEnabled(),
            'locked' => $this->isLocked(),
            'initialized' => $this->isInitialized(),
        ];
    }

    /**
     * Returns the permission type
     * 
     * @return string
     */
    public function getPermissionType(): string
    {
        return static::PERMISSION_TYPE;
    }

    /**
     * Returns the permission scope, if it is defined
     * 
     * @return string|null
     */
    public function getPermissionScope(): ?string
    {
        return static::PERMISSION_SCOPE;
    }

    /**
     * Returns the permission category, if it is defined
     * 
     * @return string|null
     */
    public function getPermissionCategory(): ?string
    {
        return static::PERMISSION_CATEGORY;
    }

    /**
     * Returns the permission subcategory, if it is defined
     * 
     * @return string|null
     */
    public function getPermissionSubCategory(): ?string
    {
        return static::PERMISSION_SUBCATEGORY;
    }

    /**
     * Returns the permission context, if it is defined
     * 
     * @return string|null
     */
    public function getPermissionContext(): ?string
    {
        return static::PERMISSION_CONTEXT;
    }

    /**
     * Returns the permission prefix, if it is defined
     * 
     * @return string|null
     */
    public function getPermissionPrefix(): ?string
    {
        return static::PERMISSION_PREFIX;
    }

    /**
     * Returns the permission suffix, if it is defined
     * 
     * @return string|null
     */
    public function getPermissionSuffix(): ?string
    {
        return static::PERMISSION_SUFFIX;
    }

    /**
     * Returns the permission separator, if it is defined
     * 
     * @return string|null
     */
    public function getPermissionSeparator(): ?string
    {
        return static::PERMISSION_SEPARATOR;
    }

    /**
     * Returns the permission wildcard, if it is defined
     * 
     * @return string|null
     */
    public function getPermissionWildcard(): ?string
    {
        return static::PERMISSION_WILDCARD;
    }

    /**
     * Enables the permission.
     * 
     * @return void
     */
    public function enable(): void
    {
        if ($this->enabled) {
            return;
        }
        $this->enabled = true;
    }

    /**
     * Disables the permission
     * 
     * @return void
     */
    public function disable(): void
    {
        if (!$this->enabled) {
            return;
        }
        $this->enabled = false;
    }

    /**
     * Locks the permission to prevent further edits
     * 
     * @return void
     */
    public function lock(): void
    {
        if ($this->locked) {
            return;
        }
        $this->locked = true;
    }

    /**
     * Returns the permission string
     * 
     * @return string
     */
    public function get(): string
    {
        $permission = $this->constructPermission($this->getConstructionDefaults());
        return $permission;
    }

    /**
     * Generates the valid wildcard string for the current permission
     * @return string
     */
    protected function generateScopedWildcard(): string
    {
        $current_id = $this->get();
        if (strpos($current_id, static::PERMISSION_SEPARATOR) === false) {
            return static::PERMISSION_WILDCARD;
        }
        $base = trim(substr($current_id, 0, strrpos($current_id, static::PERMISSION_SEPARATOR)), static::PERMISSION_SEPARATOR);
        $wildcard = sprintf('%1$s%2$s%3$s', $base, static::PERMISSION_SEPARATOR, static::PERMISSION_WILDCARD);
        return $wildcard;
    }

    /**
     * Override this method to perform validation of supplied permission strings
     * 
     * The default behavior is no validation.
     * 
     * @param string $permission
     * @return void
     */
    protected function validatePermission(string $permission): void
    {
        // no-op
    }

    /**
     * Performs the literal permission node representation display
     * and cleanup of duplicate separators
     * 
     * @param array $details
     * @return string
     */
    private function constructPermission(array $details): string
    {
        $permission = trim(sprintf(
                '%1$s%2$s%1$s%3$s%1$s%4$s',
                $details['separator'],
                (array_key_exists('prefix', $details) ? $details['prefix'] : null),
                (array_key_exists('permission', $details) ? $details['permission'] : null),
                (array_key_exists('suffix', $details) ? $details['suffix'] : null),
            ), '.');
        // Remove dupliacte separators from inside the string
        while (strpos($permission, $details['separator'] . $details['separator']) !== false) {
            $permission = str_replace($details['separator'] . $details['separator'], $details['separator'], $permission);
        }
        // Remove dupliacte wildcards from inside the string
        $wildcard = $this->getPermissionWildcard();
        while (strpos($permission, $wildcard . $wildcard) !== false) {
            $permission = str_replace($wildcard . $wildcard, $wildcard, $permission);
        }
        return $permission;
    }

    private function constructPermissionName(array $details): string
    {
        $name = trim(sprintf(
                '%1$s%2$s%1$s%3$s%1$s%4$s',
                $details['separator'],
                (array_key_exists('prefix', $details) ? $details['prefix'] : null),
                (array_key_exists('permission', $details) ? $details['permission'] : null),
                (array_key_exists('suffix', $details) ? $details['suffix'] : null),
            ), '.');
        return $name;
    }

    private function constructPermissionBase(array $details): string
    {
        $base = trim(sprintf(
                '%1$s%2$s%1$s%3$s%1$s%4$s',
                $details['separator'],
                (array_key_exists('prefix', $details) ? $details['prefix'] : null),
                (array_key_exists('permission', $details) ? $details['permission'] : null),
                (array_key_exists('suffix', $details) ? $details['suffix'] : null),
            ), '.');
        return $base;
    }

    /**
     * Returns the default permission details
     * 
     * @return array
     */
    private function getConstructionDefaults(): array
    {
        $details = [
            'separator' => $this->getPermissionSeparator(),
            'type' => $this->getPermissionType(),
            'scope' => $this->getPermissionScope(),
            'category' => $this->getPermissionCategory(),
            'subcategory' => $this->getPermissionSubCategory(),
            'context' => $this->getPermissionContext(),
            'prefix' => $this->getPermissionPrefix(),
            'permission' => $this->permission,
            'suffix' => $this->getPermissionSuffix(),
        ];
        return $details;
    }

    /**
     * Initializes the permission object
     * 
     * @return void
     */
    private function initializePermission(): void
    {
        $this->initializePermissionMasterSet();
        if (!$this->isInitialized()) {
            $permission = $this->getCommand();
            $this->validatePermission($permission);
            $this->permission = $permission;
            $this->setCommand($this->permission);
            $this->handlePermissionArguments();
            $this->is_initialized = true;
        }
    }

    /**
     * Imports arguments declaring permission definition if they exist
     * 
     * @return void
     */
    private function handlePermissionArguments(): void
    {
        if ($this->hasArgument('id')) {
            $this->permission = $this->getArgument('id');
        }
        if ($this->hasArgument('name')) {
            $this->name = $this->getArgument('name');
        }
        if ($this->hasArgument('description')) {
            $this->description = $this->getArgument('description');
        }
        if ($this->hasArgument('key')) {
            $this->key = $this->getArgument('key');
        }
        if ($this->hasArgument('whitelist')) {
            $this->whitelist = $this->getArgument('whitelist');
        }
        if ($this->hasArgument('baseline')) {
            $this->baseline = $this->getArgument('baseline');
        }
        if ($this->hasArgument('final')) {
            $this->final = $this->getArgument('final');
        }
        if ($this->hasArgument('parent')) {
            $this->initializeParent($this->getArgument('parent'));
        }
        $name = $this->get();
        if (!$this->isBaseline() && (strpos($name, static::PERMISSION_SEPARATOR . $this->id()) !== false)) {
            $parent_name = trim(substr($name, 0, strrpos(static::PERMISSION_SEPARATOR . $this->id())), static::PERMISSION_SEPARATOR);
            if (self::$master_set->has($parent_name)) {
                $this->initializeParent(self::$master_set[$parent_name]);
            }
        }
        if ($this->hasArgument('enabled')) {
            $this->enabled = $this->getArgument('enabled');
        }
        if ($this->hasArgument('locked')) {
            $this->locked = $this->getArgument('locked');
        }
    }

    /**
     * Initializes the child permission container if it is not already initialized.
     * 
     * @return void
     */
    private function initializeChildren(): void
    {
        if (!is_null($this->children)) {
            return;
        }
        $this->children = $this->containerizeInto(\Oroboros\core\library\auth\PermissionContainer::class);
    }

    /**
     * Initializes the parent permission from a string or object
     * 
     * @param null|string|\Oroboros\core\interfaces\library\auth\PermissionInterface $parent
     * @return void
     * @throws \Oroboros\core\exception\ErrorException
     *         If the given parent is malformed or not a valid permission
     */
    private function initializeParent($parent): void
    {
        if (is_null($parent)) {
            // nothing to do
            return;
        }
        if ($this->isBaseline()) {
            // Baseline permissions cannot have parents
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. Cannot append a parent to '
                        . 'baseline permission [%2$s].', get_class($this), $this->get())
            );
        }
        if (is_string($parent)) {
            if (self::$master_set->has($parent)) {
                $parent = self::$master_set[$parent];
            } else {
                // Parent not loaded yet, let it attach later
                return;
            }
        }
        $expected = \Oroboros\core\interfaces\library\auth\PermissionInterface::class;
        if (!(is_object($parent) && ($parent instanceof $expected))) {
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. Cannot resolve parent '
                        . 'permission, expected null, valid string notation, '
                        . 'or an instance of [%2$s].', get_class($this), $expected)
            );
        }
        if ($parent->isFinal()) {
            // Final permissions cannot have children
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. Given parent permission '
                        . '[%2$s] is final and cannot be set as parent of [%3$s]. '
                        , get_class($this), $parent->get(), $this->get())
            );
        }
        $this->parent = $parent;
        $parent->addChild($this);
    }

    /**
     * Initializes the permission master set if it is not already initialized
     * 
     * @return void
     */
    private function initializePermissionMasterSet(): void
    {
        if (!is_null(self::$master_set)) {
            return;
        }
        self::$master_set = $this::containerizeInto(\Oroboros\core\library\auth\PermissionContainer::class);
    }
}
