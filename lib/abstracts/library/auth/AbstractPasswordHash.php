<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\auth;

/**
 * Abstract Password Hash
 * Provides abstraction for representing a hashed password
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
abstract class AbstractPasswordHash extends \Oroboros\core\abstracts\library\AbstractLibrary implements \Oroboros\core\interfaces\library\auth\PasswordHashInterface
{

    use \Oroboros\core\traits\library\auth\AuthCommonUtilityTrait;

    /**
     * This constant must be overridden by
     * concrete implementations to declare auth scope
     */
    const AUTH_SCOPE = null;

    /**
     * The hashing algorithm to use for password hashes
     */
    const AUTH_HASH_ALGORITHM = PASSWORD_DEFAULT;

    /**
     * This constant determines the hashing cost of a hash operation.
     * It may be overridden for classes that operate on fast servers
     * in order to achieve a stronger hash
     */
    const AUTH_HASH_COST = 10;

    /**
     * Represents the hash value of the password
     * 
     * @var string
     */
    private $hash = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeAuthCommon();
        $this->initializeHash();
    }

    /**
     * Creates a hash object from a given hash string
     * 
     * The hash will remain unmodified from it's original state
     * 
     * @param string $hash
     * @param array|null $args Constructor arguments. Key "hash" is reserved.
     * @param array|null $flags Constructor flags
     * @return \Oroboros\core\interfaces\library\auth\PasswordHashInterface
     */
    public static function initFromHash(string $hash, array $args = null, array $flags = null): \Oroboros\core\interfaces\library\auth\PasswordHashInterface
    {
        $class = static::class;
        if (is_null($args)) {
            $args = [];
        }
        $args['hash'] = $hash;
        $object = new $class(null, $args, $flags);
        return $object;
    }

    /**
     * Creates a hash object from a given password string
     * 
     * The password will be hashed normally
     * 
     * @param string $password
     * @param array|null $args Constructor arguments
     * @param array|null $flags Constructor flags
     * @return \Oroboros\core\interfaces\library\auth\PasswordHashInterface
     */
    public static function initFromPassword(string $password, array $args = null, array $flags = null): \Oroboros\core\interfaces\library\auth\PasswordHashInterface
    {
        $class = static::class;
        $object = new $class($password, $args, $flags);
        return $object;
    }

    /**
     * Casts the object to it's string representation
     * 
     * @return string
     */
    public function __toString(): string
    {
        return $this->get();
    }

    /**
     * Verifies a password
     * 
     * @param \Oroboros\core\interfaces\library\auth\PasswordInterface $password
     * @return bool
     */
    public function validate(\Oroboros\core\interfaces\library\auth\PasswordInterface $password): bool
    {
        return password_verify($password->get(), $this->get());
    }

    /**
     * Returns the string representation of the password hash
     * 
     * @return string
     */
    public function get(): string
    {
        return $this->hash;
    }

    /**
     * Serializes the password hash in tiny format for later direct retrieval
     * The serialized data can be stored in a user session as a tiny
     * representation of the original hash object.
     * 
     * @return string|null
     */
    public function serialize(): ?string
    {
        return (string) $this;
    }

    /**
     * Restores a serialized hash directly. This can be used to retrieve a
     * previously generated password hash for validation purposes.
     * 
     * @param string $data
     * @return void
     */
    public function unserialize(string $data): void
    {
        $this->initializeAuthCommon();
        $this->hash = $data;
    }

    /**
     * Hashes a password
     * 
     * default functionality uses `password_hash`
     * 
     * @param string $password
     * @return string
     */
    protected function hash(string $password): string
    {
        return password_hash($password, static::AUTH_HASH_ALGORITHM, $this->declareHashOptions());
    }

    /**
     * Provides options for the `password_hash` function
     * 
     * @return array
     */
    protected function declareHashOptions(): array
    {
        return [
            'cost' => static::AUTH_HASH_COST
        ];
    }

    /**
     * Initializes the hash
     * 
     * @return void
     */
    private function initializeHash(): void
    {
        if (is_null($this->hash)) {
            if ($this->hasArgument('hash')) {
                $this->hash = $this->getArgument('hash');
                return;
            }
            $password = $this->getCommand();
            if (is_null($password)) {
                $password = '';
                $this->setCommand($password);
            }
            $this->hash = $this->hash($password);
            // Remove the plaintext password from the command
            $this->setCommand(null);
        }
    }
}
