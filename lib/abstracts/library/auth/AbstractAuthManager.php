<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\auth;

/**
 * Abstract Auth Manager
 * Provides abstraction for authentication managers
 * 
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
abstract class AbstractAuthManager extends \Oroboros\core\abstracts\pattern\director\AbstractDirector implements \Oroboros\core\interfaces\library\auth\AuthManagerInterface
{

    use \Oroboros\core\traits\LoaderTrait;
    use \Oroboros\core\traits\library\auth\AuthCommonUtilityTrait;
    use \Oroboros\core\traits\library\auth\PermissionMatchableTrait;

    /**
     * This constant must be overridden by
     * concrete implementations to declare auth scope
     */
    const AUTH_SCOPE = null;

    /**
     * Use the default validator strategy unless otherwise specified.
     */
    const AUTH_VALIDATOR_STRATEGY_CLASS = 'auth\\ValidationStrategy';

    /**
     * Use the default authenticator strategy unless otherwise specified.
     */
    const AUTH_AUTHENTICATOR_STRATEGY_CLASS = 'auth\\AuthenticatorStrategy';

    /**
     * Use the default credential container unless otherwise specified.
     */
    const AUTH_CREDENTIAL_CLASS = 'auth\\Credentials';

    /**
     * Use the default authenticator strategy unless otherwise specified.
     */
    const AUTH_IDENTITY_CLASS = 'auth\\Identity';

    /**
     * Use the default password class unless otherwise specified.
     */
    const AUTH_PASSWORD_CLASS = 'auth\\Password';

    /**
     * Use the default password hash unless otherwise specified.
     */
    const AUTH_PASSWORD_HASH_CLASS = 'auth\\PasswordHash';

    /**
     * Use the default nonce unless otherwise specified.
     */
    const AUTH_NONCE_CLASS = 'auth\\Nonce';

    /**
     * Use the default salt unless otherwise specified.
     */
    const AUTH_SALT_CLASS = 'auth\\Salt';

    /**
     * Use the default token unless otherwise specified.
     */
    const AUTH_TOKEN_CLASS = 'auth\\Token';

    /**
     * Use the default private key unless otherwise specified.
     */
    const AUTH_PRIVATE_KEY_CLASS = 'auth\\PrivateKey';

    /**
     * Use the default public key unless otherwise specified.
     */
    const AUTH_PUBLIC_KEY_CLASS = 'auth\\PublicKey';

    /**
     * Use the default key pair unless otherwise specified.
     */
    const AUTH_KEY_PAIR_CLASS = 'auth\\KeyPair';

    /**
     * Use the default keychain unless otherwise specified.
     */
    const AUTH_KEYCHAIN_CLASS = 'auth\\Keychain';

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        $this->verifyManager();
        parent::__construct($command, $arguments, $flags);
        $this->initializeAuthCommon();
        $this->initializeManager();
    }

    public function authenticator(): \Oroboros\core\interfaces\library\auth\AuthenticatorInterface
    {
        $strategy = $this->load('library', static::AUTH_AUTHENTICATOR_STRATEGY_CLASS, $this->getCommand(), $this->getArguments(), $this->getFlags());
        $authenticator = $strategy->getAuthenticator($this);
        return $authenticator;
    }

    public function authenticate(\Oroboros\core\interfaces\library\auth\CredentialContainerInterface $credentials): bool
    {
        return $this->authenticator()->authenticate($credentials);
    }

    public function validator(): \Oroboros\core\interfaces\library\auth\ValidatorInterface
    {
        $strategy = $this->load('library', static::AUTH_VALIDATOR_STRATEGY_CLASS, $this->getCommand(), $this->getArguments(), $this->getFlags());
        $validator = $strategy->getValidator($this);
        return $validator;
    }

    public function validate(\Oroboros\core\interfaces\library\auth\CredentialContainerInterface $credentials): bool
    {
        return $this->validator()->validate($credentials);
    }

    public function identity(string $identity = null): \Oroboros\core\interfaces\library\auth\IdentityInterface
    {
        return $this->load('library', static::AUTH_IDENTITY_CLASS, $identity);
    }

    public function password(string $password = null): ?\Oroboros\core\interfaces\library\auth\PasswordInterface
    {
        if (is_null(static::AUTH_PASSWORD_CLASS)) {
            return null;
        }
        $result = $this->load('library', static::AUTH_PASSWORD_CLASS, $password);
        return $result;
    }

    public function hash(string $password = null): ?\Oroboros\core\interfaces\library\auth\PasswordHashInterface
    {
        if (is_null(static::AUTH_PASSWORD_HASH_CLASS)) {
            return null;
        }
        $result = $this->load('library', static::AUTH_PASSWORD_HASH_CLASS, $password);
        return $result;
    }

    public function nonce(string $nonce = null): ?\Oroboros\core\interfaces\library\auth\NonceInterface
    {
        if (is_null(static::AUTH_NONCE_CLASS)) {
            return null;
        }
        $result = $this->load('library', static::AUTH_NONCE_CLASS, $nonce);
        return $result;
    }

    public function salt(string $salt = null): ?\Oroboros\core\interfaces\library\auth\SaltInterface
    {
        if (is_null(static::AUTH_SALT_CLASS)) {
            return null;
        }
        $result = $this->load('library', static::AUTH_SALT_CLASS, $salt);
        return $result;
    }

    public function token(string $token = null): ?\Oroboros\core\interfaces\library\auth\TokenInterface
    {
        if (is_null(static::AUTH_TOKEN_CLASS)) {
            return null;
        }
        $result = $this->load('library', static::AUTH_TOKEN_CLASS, $token);
        return $result;
    }

    public function keyPair(string $key = null): ?\Oroboros\core\interfaces\library\auth\KeyPairInterface
    {
        if (is_null(static::AUTH_KEY_PAIR_CLASS)) {
            return null;
        }
        $result = $this->load('library', static::AUTH_KEY_PAIR_CLASS, $key);
        return $result;
    }

    public function publicKey(string $key = null): ?\Oroboros\core\interfaces\library\auth\KeyInterface
    {
        if (is_null(static::AUTH_PUBLIC_KEY_CLASS)) {
            return null;
        }
        $result = $this->load('library', static::AUTH_PUBLIC_KEY_CLASS, $key);
        return $result;
    }

    public function privateKey(string $key = null): ?\Oroboros\core\interfaces\library\auth\KeyInterface
    {
        if (is_null(static::AUTH_PRIVATE_KEY_CLASS)) {
            return null;
        }
        $result = $this->load('library', static::AUTH_PRIVATE_KEY_CLASS, $key);
        return $result;
    }

    public function credentials(iterable $credentials): \Oroboros\core\interfaces\library\auth\CredentialContainerInterface
    {
        return $this->parseDefaultCredentials($credentials);
    }

    public function keychain(string $keychain = null): ?\Oroboros\core\interfaces\library\auth\KeychainInterface
    {
        if (is_null(static::AUTH_KEYCHAIN_CLASS)) {
            return null;
        }
        $keychain = $this->load('library', static::AUTH_KEYCHAIN_CLASS, $keychain, $this->getArguments(), $this->getFlags());
        return $keychain;
    }

    /**
     * Returns a set of credentials based on the credentials given.
     * 
     * If no credentials are given, a nulled credential set will be returned.
     * 
     * @param array $credentials
     * @return array
     */
    protected function getCredentials(array $credentials = null): array
    {
        $results = [];
        if (is_null($credentials)) {
            $credentials = [];
        }
        $credentials = $this->containerize($credentials);
        if ($credentials->has('identity')) {
            $results['identity'] = $this->identity($credentials['identity']);
        } else {
            // Default null identity
            $results['identity'] = $this->identity();
        }
        if ($credentials->has('password')) {
            $results['password'] = $this->password($credentials['password']);
            $results['hash'] = $this->hash($credentials['password']);
        } else {
            // Default null password
            $results['password'] = $this->password();
            // Default null hash
            $results['hash'] = $this->hash();
        }
        if ($credentials->has('nonce')) {
            $results['nonce'] = $this->nonce($credentials['nonce']);
        } else {
            // Default null nonce
            $results['nonce'] = $this->nonce();
        }
        if ($credentials->has('salt')) {
            $results['salt'] = $this->salt($credentials['salt']);
        } else {
            // Default null salt
            $results['salt'] = $this->salt();
        }
        if ($credentials->has('token')) {
            $results['token'] = $this->token($credentials['token']);
        } else {
            // Default null token
            $results['token'] = $this->token();
        }
        if ($credentials->has('key-pair')) {
            $results['key-pair'] = $this->keyPair($credentials['key-pair']);
        } else {
            // Default null key pair
            $results['key-pair'] = $this->keyPair();
        }
        if ($credentials->has('public-key')) {
            $results['public-key'] = $this->publicKey($credentials['public-key']);
        } else {
            // Default null public key
            $results['key-pair'] = $this->publicKey();
        }
        if ($credentials->has('private-key')) {
            $results['private-key'] = $this->privateKey($credentials['key-pair']);
        } else {
            // Default null private key
            $results['private-key'] = $this->privateKey();
        }
        if ($credentials->has('keychain')) {
            $keychain = $credentials['keychain'];
            if (is_object($keychain) && ($keychain instanceof \Oroboros\core\interfaces\library\auth\KeyInterface)) {
                $results['keychain'] = $keychain;
            } else {
                $results['keychain'] = $this->keychain($credentials['keychain']);
            }
        } else {
            // Default null keychain
            $results['keychain'] = $this->keychain();
        }
        return $results;
    }

    /**
     * This method is called by all request types that do not have
     * a case-specific handler declaration.
     * 
     * The default behavior is to hand back a nulled credential set.
     * 
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @return \Oroboros\core\interfaces\library\auth\CredentialContainerInterface
     */
    protected function parseDefaultCredentials(iterable $supplied): \Oroboros\core\interfaces\library\auth\CredentialContainerInterface
    {
        $results = [];
        foreach ($supplied as $key => $value) {
            $results[$this->filterCredentialKeyDefinition($key)] = $value;
        }
        $container = $this->getFullClassName('library', static::AUTH_CREDENTIAL_CLASS);
        $credentials = $this->getCredentials($results);
        foreach ($credentials as $key => $value) {
            if (is_null($value)) {
                unset($credentials[$key]);
            }
        }
        return $container::init(null, $credentials);
    }

    /**
     * Standardizes keys to the credential container format
     * 
     * @param string $key
     * @return string
     */
    protected function filterCredentialKeyDefinition(string $key): string
    {
        $prefix = sprintf('%1$s-', static::AUTH_SCOPE);
        if (strpos($key, $prefix) === 0) {
            return substr($key, strlen($prefix));
        }
        return $key;
    }

    /**
     * Override this method to perform any additional initialization tasks
     * 
     * @return void
     */
    protected function initializeManager(): void
    {
        // no-op
    }

    /**
     * Verifies that all class constant declarations are valid
     * 
     * @return void
     */
    private function verifyManager(): void
    {
        $this->verifyValidatorStrategyClass();
        $this->verifyAuthenticatorStrategClass();
        $this->verifyIdentityClass();
        $this->verifyPasswordClass();
        $this->verifyPasswordHashClass();
        $this->verifyNonceClass();
        $this->verifySaltClass();
        $this->verifyTokenClass();
        $this->verifyCredentialClass();
        $this->verifyKeyPairClass();
        $this->verifyKeychainClass();
        $this->verifyPrivateKeyClass();
        $this->verifyPublicKeyClass();
    }

    /**
     * Checks that a valid validator strategy is defined.
     * Validator Strategies are mandatory.
     * 
     * @return void
     */
    private function verifyValidatorStrategyClass(): void
    {
        $expected = \Oroboros\core\interfaces\library\auth\ValidatorStrategyInterface::class;
        $this->preflightConstant('AUTH_VALIDATOR_STRATEGY_CLASS', $expected, true);
    }

    /**
     * Checks that a valid authenticator strategy is defined.
     * Authenticator Strategies are mandatory.
     * 
     * @return void
     */
    private function verifyAuthenticatorStrategClass(): void
    {
        $expected = \Oroboros\core\interfaces\library\auth\AuthenticatorStrategyInterface::class;
        $this->preflightConstant('AUTH_AUTHENTICATOR_STRATEGY_CLASS', $expected, true);
    }

    /**
     * Checks that a valid credential container is defined.
     * Credential containers are mandatory.
     * 
     * @return void
     */
    private function verifyCredentialClass(): void
    {
        $expected = \Oroboros\core\interfaces\library\auth\CredentialContainerInterface::class;
        $this->preflightConstant('AUTH_CREDENTIAL_CLASS', $expected, true);
    }

    /**
     * Checks that a valid identity class is defined.
     * Identity classes are mandatory.
     * 
     * @return void
     */
    private function verifyIdentityClass(): void
    {
        $expected = \Oroboros\core\interfaces\library\auth\IdentityInterface::class;
        $this->preflightConstant('AUTH_IDENTITY_CLASS', $expected, true);
    }

    /**
     * Checks that a valid password class is defined.
     * Password classes are optional. If no password class exists,
     * either a token class must exist, or public/private key
     * classes and a keypair class.
     * 
     * @return void
     */
    private function verifyPasswordClass(): void
    {
        $expected = \Oroboros\core\interfaces\library\auth\PasswordInterface::class;
        $this->preflightConstant('AUTH_PASSWORD_CLASS', $expected, false);
    }

    /**
     * Checks that a valid password hash class is defined.
     * Password hash classes are optional.
     * 
     * @return void
     */
    private function verifyPasswordHashClass(): void
    {
        $expected = \Oroboros\core\interfaces\library\auth\PasswordHashInterface::class;
        $this->preflightConstant('AUTH_PASSWORD_HASH_CLASS', $expected, false);
    }

    /**
     * Checks that a valid nonce class is defined.
     * Nonce classes are optional.
     * 
     * @return void
     */
    private function verifyNonceClass(): void
    {
        $expected = \Oroboros\core\interfaces\library\auth\NonceInterface::class;
        $this->preflightConstant('AUTH_NONCE_CLASS', $expected, false);
    }

    /**
     * Checks that a valid salt class is defined.
     * Salt classes are optional.
     * 
     * @return void
     */
    private function verifySaltClass(): void
    {
        $expected = \Oroboros\core\interfaces\library\auth\SaltInterface::class;
        $this->preflightConstant('AUTH_SALT_CLASS', $expected, false);
    }

    /**
     * Checks that a valid token class is defined.
     * Password classes are optional. If no token class exists,
     * either a password class must exist, or public/private key
     * classes and a keypair class.
     * 
     * @return void
     */
    private function verifyTokenClass(): void
    {
        $expected = \Oroboros\core\interfaces\library\auth\TokenInterface::class;
        $this->preflightConstant('AUTH_TOKEN_CLASS', $expected, false);
    }

    /**
     * Checks that a valid private key class is defined.
     * Private key classes are optional. If one exists,
     * a valid public key
     * class and a keypair class must also exist.
     * 
     * @return void
     */
    private function verifyPrivateKeyClass(): void
    {
        $expected = \Oroboros\core\interfaces\library\auth\PrivateKeyInterface::class;
        $this->preflightConstant('AUTH_PRIVATE_KEY_CLASS', $expected, false);
    }

    /**
     * Checks that a valid public key class is defined.
     * Public key classes are optional. If one exists,
     * a valid private key
     * class and a keypair class must also exist.
     * 
     * @return void
     */
    private function verifyPublicKeyClass(): void
    {
        $expected = \Oroboros\core\interfaces\library\auth\PublicKeyInterface::class;
        $this->preflightConstant('AUTH_PUBLIC_KEY_CLASS', $expected, false);
    }

    /**
     * Checks that a valid key pair class is defined.
     * Key pair classes are optional. If one exists,
     * a valid private key class and public key class
     * must also exist.
     * 
     * @return void
     */
    private function verifyKeyPairClass(): void
    {
        $expected = \Oroboros\core\interfaces\library\auth\KeyPairInterface::class;
        $this->preflightConstant('AUTH_KEY_PAIR_CLASS', $expected, false);
    }

    /**
     * Checks that a valid keychain class is defined.
     * Keychain classes are optional.
     * 
     * @return void
     */
    private function verifyKeychainClass(): void
    {
        $expected = \Oroboros\core\interfaces\library\auth\KeychainInterface::class;
        $this->preflightConstant('AUTH_KEYCHAIN_CLASS', $expected, false);
    }
}
