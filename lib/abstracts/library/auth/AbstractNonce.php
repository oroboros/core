<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\auth;

/**
 * Abstract Nonce
 * Provides abstraction for generating nonces
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
abstract class AbstractNonce extends \Oroboros\core\abstracts\library\AbstractLibrary implements \Oroboros\core\interfaces\library\auth\NonceInterface
{

    use \Oroboros\core\traits\library\auth\AuthCommonUtilityTrait;

    /**
     * This constant must be overridden by
     * concrete implementations to declare auth scope
     */
    const AUTH_SCOPE = null;

    /**
     * The hashing algorithm used for generation of nonces
     */
    const NONCE_HASH_ALGORITHM = 'sha512';

    /**
     * The bitrate used for randomization of strings
     */
    const NONCE_BITRATE = 256;

    /**
     * The nonce represented by the object.
     * This will either be the nonce passed on instantiation,
     * or a freshly generated nonce if no nonce string was passed.
     * 
     * @var string
     */
    private $nonce = null;

    /**
     * If the given `$command` parameter is a string, the object will be
     * instantiated as a representation of the given string as the nonce.
     * 
     * If the given string has already been expired, this will raise an
     * `\Oroboros\core\exception\OutOfBoundsException`
     * 
     * @param string $command
     * @param array $arguments
     * @param array $flags
     * @throws \Oroboros\core\exception\OutOfBoundsException If a nonce string
     *         is passed as `$command` that has already been expired.
     * @throws \Oroboros\core\exception\core\InvalidClassException
     *         If the class constant `NONCE_HASH_ALGORITHM` does not define
     *         a valid hash method. These may be checked for validity with `hash_algos()`
     */
    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        $this->validateHashAlgorithm();
        parent::__construct($command, $arguments, $flags);
        $this->initializeAuthCommon();
        $this->initializeNonce();
    }

    /**
     * Returns the string representation of the nonce
     * 
     * @return string
     */
    public function __toString(): string
    {
        return $this->get();
    }
    
    /**
     * Returns the string representation of the nonce
     * @return string
     */
    public function get(): string
    {
        return $this->nonce;
    }

    /**
     * Serializes the nonce in tiny format for later direct retrieval
     * The serialized data can be stored in a user session as a tiny
     * representation of the original nonce object.
     * 
     * @return string|null
     */
    public function serialize(): ?string
    {
        return (string) $this;
    }

    /**
     * Restores a serialized nonce directly. This can be used to retrieve a
     * previously generated form nonce for validation purposes.
     * 
     * @param string $data
     * @return void
     */
    public function unserialize(string $data): void
    {
        $this->validateHashAlgorithm();
        $this->nonce = $data;
    }

    /**
     * Generates a valid nonce string.
     * This should not return a nonce that has
     * already been used.
     * 
     * @return string
     */
    public function generate(): string
    {
        do {
            $nonce = hash(static::NONCE_HASH_ALGORITHM, $this->generateNonceString(static::NONCE_BITRATE));
        } while (!$this->validate($nonce));
        return $nonce;
    }

    /**
     * Expires the nonce represented by the object.
     * The nonce represented by the object should not validate again when the
     * same class is called with the same string to initialize it.
     * 
     * @return void
     */
    public function expire(): void
    {
        // no-op
    }

    /**
     * Returns a boolean designation as to whether the given nonce
     * string matches the nonce internally stored in the object.
     * 
     * Calling this method will also expire the nonce, however
     * this object will still `match` as `true` as long as it
     * exists for further verification. The represented nonce
     * cannot be generated again though.
     * 
     * @param string $nonce
     * @return bool
     */
    public function match(string $nonce): bool
    {
        $designation = $nonce === $this->nonce;
        if ($this->validate($this->nonce)) {
            $this->expire();
        }
        return $designation;
    }

    /**
     * Validates that a given nonce has not been used already.
     * 
     * This will return false if a given nonce string already exists.
     * 
     * @param string $nonce
     * @return bool
     */
    public function validate(string $nonce): bool
    {
        return true;
    }

    /**
     * This method must generate a unique unhashed string.
     * It will be hashed via the declared hashing protocol, and then checked
     * with the `validate` method to verify it has not already been used.
     * This method will be called repeatedly if it generates nonce strings
     * that have already been used.
     * 
     * @return string
     */
    protected function generateNonceString($bits = 256): string
    {
        $bytes = ceil($bits / 8);
        $return = '';
        for ($i = 0; $i < $bytes; $i++) {
            $return .= chr(mt_rand(0, 255));
        }
        return $return;
    }

    /**
     * Sets the internal nonce to the command, or generates a
     * random one if no pre-existing nonce was passed.
     * 
     * @return void
     */
    private function initializeNonce(): void
    {
        if (is_null($this->nonce)) {
            $nonce = $this->getCommand();
            if (is_null($nonce)) {
                $nonce = $this->generate();
                $this->setCommand($nonce);
            }
            $this->nonce = $nonce;
        }
    }

    /**
     * Validates that the nonce class declares a valid hashing algorithm.
     * 
     * @return void
     * @throws \Oroboros\core\exception\core\InvalidClassException
     */
    private function validateHashAlgorithm(): void
    {
        $algorithms = hash_algos();
        if (!in_array(static::NONCE_HASH_ALGORITHM, $algorithms)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. Declared hashing '
                        . 'algorithm [%2$s] is not valid. Valid hash algorithms '
                        . 'are [%3$s]. This class is not useable in it\'s '
                        . 'current state.', get_class($this), static::NONCE_HASH_ALGORITHM, implode(', ', $algorithms))
            );
        }
    }
}
