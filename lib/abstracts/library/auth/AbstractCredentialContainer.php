<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\auth;

/**
 * Abstract Credential Container
 * Provides abstraction for credential containers
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
abstract class AbstractCredentialContainer extends \Oroboros\core\abstracts\library\data\AbstractDataContainer implements \Oroboros\core\interfaces\library\auth\CredentialContainerInterface
{

    use \Oroboros\core\traits\library\auth\AuthCommonUtilityTrait;

    /**
     * This constant must be overridden by
     * concrete implementations to declare auth scope
     */
    const AUTH_SCOPE = null;

    /**
     * Lists acceptable credential keys and their
     * corresponding interfaces their values must implement
     * 
     * @var array
     */
    private static $valid_keys = [
        'identity' => \Oroboros\core\interfaces\library\auth\IdentityInterface::class,
        'password' => \Oroboros\core\interfaces\library\auth\PasswordInterface::class,
        'hash' => \Oroboros\core\interfaces\library\auth\PasswordHashInterface::class,
        'token' => \Oroboros\core\interfaces\library\auth\TokenInterface::class,
        'nonce' => \Oroboros\core\interfaces\library\auth\NonceInterface::class,
        'salt' => \Oroboros\core\interfaces\library\auth\SaltInterface::class,
        'public-key' => \Oroboros\core\interfaces\library\auth\PublicKeyInterface::class,
        'private-key' => \Oroboros\core\interfaces\library\auth\PrivateKeyInterface::class,
        'key-pair' => \Oroboros\core\interfaces\library\auth\KeyPairInterface::class,
        'keychain' => \Oroboros\core\interfaces\library\auth\KeychainInterface::class,
    ];

    /**
     * Verifies that the given keys are only credential objects.
     * 
     * @param type $key
     * @param type $value
     * @param type $method
     * @return void
     * @throws \Oroboros\core\exception\container\ContainerException
     */
    protected function onSet($key, $value, $method = null): void
    {
        if (!array_key_exists($key, self::$valid_keys)) {
            throw new \Oroboros\core\exception\container\ContainerException(
                    sprintf('Error encountered in [%1$s]. Provided key [%2$s] is '
                        . 'not a valid key. Valid keys are [%3$s].'
                        , get_class($this), $key, implode(', ', array_keys(self::$valid_keys)))
            );
        }
        $expected = self::$valid_keys[$key];
        if (!is_object($value) && ($value instanceof $expected)) {
            throw new \Oroboros\core\exception\container\ContainerException(
                    sprintf('Error encountered in [%1$s]. Provided key [%2$s] does '
                        . 'not supply a valid value. Expected instance of [%3$s].'
                        , get_class($this), $key, $expected)
            );
        }
        parent::onSet($key, $value, $method);
    }

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeAuthCommon();
    }

    public function __toString(): string
    {
        return json_encode($this);
    }
}
