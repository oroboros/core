<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\auth;

/**
 * Abstract Permission Definer
 * Definer implementation for permissions
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
abstract class AbstractPermissionDefiner extends \Oroboros\core\abstracts\library\definer\AbstractDefiner implements \Oroboros\core\interfaces\library\auth\AuthInterface
{

    use \Oroboros\core\traits\library\auth\AuthCommonUtilityTrait;
    use \Oroboros\core\traits\model\ModelAwareUtilityTrait;

    /**
     * Designates that content definers use DataHookSets
     * 
     * Must be a fully qualified classname, and must resolve to a class
     * implementing `\Oroboros\core\interfaces\library\hook\HookSetInterface`
     */
    const HOOKSET_CLASS = \Oroboros\core\library\hook\DataHookSet::class;

    /**
     * Sets the class scope to permission definer
     */
    const CLASS_SCOPE = 'permission-definer';

    /**
     * Designates that this class is part of the general auth group
     */
    const AUTH_SCOPE = self::CLASS_SCOPE;

    /**
     * Defines the default permission class used by the definer to
     * generate permissions that do not declare their own class.
     */
    const DEFAULT_PERMISSION_CLASS = \Oroboros\core\library\auth\Permission::class;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        $this->verifyAuthScope();
        $this->verifyDefaultPermissionClass();
        parent::__construct($command, $arguments, $flags);
        $this->initializePermissionDefiner();
    }

    /**
     * Packages permission objects into the compiled set
     * 
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $data
     * @return \Oroboros\core\interfaces\library\auth\PermissionContainerInterface
     */
    protected function formatCompiled(\Oroboros\core\interfaces\library\container\ContainerInterface $data): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $result = $this->containerizeInto(\Oroboros\core\library\auth\PermissionContainer::class);
        foreach ($data as $key => $value) {
            $class = static::DEFAULT_PERMISSION_CLASS;
            if ($value->has('class')) {
                $class = $value['class'];
            }
            $result[$key] = $this->load('library', $class, $key, $value->toArray());
            if ($value->has('children')) {
                $result = $this->recurseChildren($result[$key], $value['children'], $result);
            }
        }
        return $result;
    }

    /**
     * Recursively extracts child permission nodes from permission trees in definer files.
     * 
     * @param \Oroboros\core\interfaces\library\auth\PermissionInterface $base
     *        The parent node
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $children
     *        The set of children to append to the data set
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $data
     *        The full dataset of existing permissions
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     *         Returns a copy of the updated full set of permissions
     *         with the children appended
     */
    private function recurseChildren(\Oroboros\core\interfaces\library\auth\PermissionInterface $base, \Oroboros\core\interfaces\library\container\ContainerInterface $children, \Oroboros\core\interfaces\library\container\ContainerInterface $data): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $container_class = get_class($data);
        $results = $this::containerizeInto($container_class);
        foreach ($data as $key => $value) {
            $results[$key] = $value;
        }
        $prefix = sprintf('%1$s%2$s', $base->get(), $base->getPermissionSeparator());
        foreach ($children as $key => $value) {
            $class = get_class($base);
            $keyname = $prefix . $key;
            if ($value->has('id')) {
                $keyname = $prefix . $value['id'];
                $value['id'] = $keyname;
            }
            if ($value->has('class')) {
                $class = $value['class'];
            }
            $permission = $this->load('library', $class, $key, $value->toArray(), $this->getFlags());
            $results[$keyname] = $permission;
            if ($value->has('children')) {
                $results = $this->recurseChildren($permission, $value['children'], $results);
            }
        }
        return $results;
    }

    /**
     * Initializes the permission definer
     * 
     * @return void
     */
    private function initializePermissionDefiner(): void
    {
        
    }

    /**
     * Verifies that the default permission class constant is properly defined.
     * 
     * @return void
     * @throws \Oroboros\core\exception\core\InvalidClassException
     */
    private function verifyDefaultPermissionClass(): void
    {
        $expected = \Oroboros\core\interfaces\library\auth\PermissionInterface::class;
        $const = 'DEFAULT_PERMISSION_CLASS';
        $value = static::DEFAULT_PERMISSION_CLASS;
        $class = $this->getFullClassName('library', $value);
        if ($class === false || !in_array($expected, class_implements($class))) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. Expected class constant '
                        . '[%2$s] with value [%3$s] must resolve to a valid class '
                        . 'or stub class name implementing expected interface [%4$s]. '
                        . 'This class is not usable in it\'s current state.'
                        , get_class($this), $const, $value, $expected)
            );
        }
    }
}
