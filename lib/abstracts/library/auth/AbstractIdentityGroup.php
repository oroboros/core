<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\auth;

/**
 * Abstract Identity Group
 * Provides base abstraction for permission groups
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
abstract class AbstractIdentityGroup extends \Oroboros\core\abstracts\library\entity\AbstractGroupEntity implements \Oroboros\core\interfaces\library\auth\IdentityGroupInterface
{

    use \Oroboros\core\traits\model\ModelAwareUtilityTrait;
    use \Oroboros\core\traits\library\auth\AuthCommonUtilityTrait;
    use \Oroboros\core\traits\library\auth\PermissionMatchableTrait;

    /**
     * Declares the auth identity type as permission group
     */
    const AUTH_IDENTITY_TYPE = 'identity-group';

    /**
     * Declares the entity type as identity group
     */
    const ENTITY_TYPE = self::AUTH_IDENTITY_TYPE;

    /**
     * Declares the entity scope as auth
     */
    const ENTITY_SCOPE = 'auth';

    /**
     * Match the default identity type
     */
    const IDENTITY_GROUP_TYPE = AbstractIdentity::IDENTITY_TYPE;

    /**
     * Match the default identity scope
     */
    const IDENTITY_GROUP_SCOPE = AbstractIdentity::IDENTITY_SCOPE;

    /**
     * Match the default identity category
     */
    const IDENTITY_GROUP_CATEGORY = AbstractIdentity::IDENTITY_CATEGORY;

    /**
     * Match the default identity subcategory
     */
    const IDENTITY_GROUP_SUBCATEGORY = AbstractIdentity::IDENTITY_SUBCATEGORY;

    /**
     * Match the default identity context
     */
    const IDENTITY_GROUP_CONTEXT = AbstractIdentity::IDENTITY_CONTEXT;

    /**
     * This constant may be overridden to add a fixed prefix to identity groups.
     * The default behavior is no prefix
     */
    const IDENTITY_GROUP_PREFIX = AbstractIdentity::IDENTITY_PREFIX;

    /**
     * This constant may be overridden to add a fixed suffix to identity groups.
     * The default behavior is no suffix
     */
    const IDENTITY_GROUP_SUFFIX = AbstractIdentity::IDENTITY_SUFFIX;

    /**
     * This constant may be overridden to change the separator
     * used for generating identity groups
     * 
     * Altering the separator will make identity groups incompatible
     * with other formats, but exists as a vehicle to map
     * identity groups to more generalized purposes or maintain
     * interoperability between implementations
     * 
     * This is left to further implementation
     */
    const IDENTITY_GROUP_SEPARATOR = AbstractIdentity::IDENTITY_SEPARATOR;

    /**
     * The master set of identity groups
     * 
     * @var \Oroboros\core\interfaces\library\auth\IdentityGroupContainerInterface
     */
    private static $master_set = null;

    /**
     * The permission groups assigned to the identity group
     * 
     * @var \Oroboros\core\interfaces\library\auth\PermissionGroupContainer
     */
    private $permission_groups = null;

    /**
     * The permission groups negated by the group
     * 
     * Any permission groups in this container will be removed from the output
     * set prior to delivery.
     * 
     * @var \Oroboros\core\interfaces\library\auth\PermissionGroupContainer
     */
    private $negations = null;

    /**
     * The parent identity group, if any
     * 
     * @var \Oroboros\core\interfaces\library\auth\IdentityGroupInterface|null
     */
    private $parent = null;

    /**
     * The literal representation of the identity group name
     * @var string
     */
    private $group = null;

    /**
     * The readable name of the identity group, if one is defined.
     */
    private $name = null;

    /**
     * The readable description of the identity group, if one is defined.
     */
    private $description = null;

    /**
     * An optional security key for the identity group.
     * 
     * The key is used to resolve passed group sets and check if they were
     * correctly granted access to the identity group.
     * 
     * @var \Oroboros\core\interfaces\library\auth\KeyInterface
     */
    private $key = null;

    /**
     * If the identity group is whitelisted to only be added to specific identities,
     * this will contain a list of those identities
     * 
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface|null
     */
    private $whitelist = null;

    /**
     * Whether or not the identity group itself is marked as baseline
     * 
     * Baseline identity groups are bottom level
     * identity groups that do not have parents.
     * @var bool
     */
    private $baseline = false;

    /**
     * Whether or not the identity group itself is marked final
     * 
     * Final identity groups cannot be used as parents for
     * other identity groups
     * @var bool
     */
    private $final = false;

    /**
     * Whether or not the identity group itself is enabled
     * @var bool
     */
    private $enabled = true;

    /**
     * Whether or not the identity group object is locked
     * Locked identity groups cannot be edited or mutated
     * 
     * @var type
     */
    private $locked = false;

    /**
     * Determines whether the identity group is initialized
     * 
     * @var bool
     */
    private $is_initialized = false;

    /**
     * Returns a copy of the identity group master set.
     * The original set is preserved, this does not provide direct access
     * to the internal identity group master set, only a copy of it.
     * 
     * @return \Oroboros\core\interfaces\library\auth\IdentityGroupContainerInterface
     */
    public static function index(): \Oroboros\core\interfaces\library\auth\IdentityGroupContainerInterface
    {
        $groups = self::$master_set->toArray();
        return static::containerizeInto(\Oroboros\core\library\auth\IdentityGroupContainer::class, $groups);
    }

    /**
     * Imports a container of identity group definitions into the master set
     * 
     * @param \Oroboros\core\interfaces\library\auth\IdentityGroupContainerInterface $groups
     * @return void
     */
    public static function import(\Oroboros\core\interfaces\library\auth\IdentityGroupContainerInterface $groups): void
    {
        foreach ($groups as $key => $group) {
            static::register($group);
        }
    }

    /**
     * Registers an identity group with the master set
     * 
     * @param \Oroboros\core\interfaces\library\auth\IdentityGroupInterface $group
     * @return void
     */
    public static function register(\Oroboros\core\interfaces\library\auth\IdentityGroupInterface $group): void
    {
        self::$master_set[$group->get()] = $group;
    }

    /**
     * Unregisters an identity group from the master set.
     * To unregister, the exact same identity group that exists
     * in the master set must be passed.
     * 
     * @param \Oroboros\core\interfaces\library\auth\IdentityGroupInterface $group
     * @oaran string|null $key
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException
     *         If the given identity group identifier exists in the master set,
     *         but the given identity group is not the originally registered permission group.
     */
    public static function unregister(\Oroboros\core\interfaces\library\auth\IdentityGroupInterface $group, string $key = null): void
    {
        if (self::$master_set->has($group->get()) && self::$master_set[$group->get()] !== $group) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Identity group mismatch for identity group [%2$s]. '
                        . 'Unregistration requires passing the same identity group that is registered, '
                        . 'copies cannot be used to unregister the original.', static::class, $group->get())
            );
        }
        if (self::$master_set->has($group->get()) && self::$master_set[$group->get()] === $group) {
            unset(self::$master_set[$group->get()]);
        }
    }

    /**
     * Permission groups use the default constructor.
     * The value of `$command` will become the base permission group id
     * unless arguments explicitly define one.
     * 
     * @param string $command
     * @param array $arguments
     * @param array $flags
     */
    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeAuthCommon();
        $this->initializeIdentityGroup();
        if (!self::$master_set->has($this->get())) {
            // Only originals will be auto-registered
            static::register($this);
        }
    }

    /**
     * Matches a given permission node and returns a boolean designation
     * as to whether this group authorizes the given permission.
     * 
     * If the group is disabled, will always return `false`.
     * 
     * If the group contains any permission group that evaluates
     * to `true` for the given permission, returns `true`.
     * 
     * If the group has a parent and the parent returns `true`, returns `true`.
     * 
     * Returns `false` in all other cases.
     * 
     * @param string $permission
     * @return bool
     */
    public function match(string $permission): bool
    {
        if (!$this->isEnabled()) {
            // Disabled groups cannot match permissions
            return false;
        }
        foreach ($this->groups() as $key => $permgroup) {
            if ($permgroup->match($permission)) {
                return true;
            }
        }
        $parent = $this->parent();
        if (!is_null($parent)) {
            // Parent group can handle it
            return $parent->match($permission);
        }
        // All other cases are false
        return false;
    }

    /**
     * Returns the permission groups associated with the group,
     * including those inherited from any number of ancestors.
     * 
     * @return \Oroboros\core\interfaces\library\auth\PermissionGroupContainerInterface
     */
    public function groups(): \Oroboros\core\interfaces\library\auth\PermissionGroupContainerInterface
    {
        $permission_groups = $this::containerizeInto(\Oroboros\core\library\auth\PermissionGroupContainer::class);
        if (!is_null($this->parent()) && !$this->isBaseline()) {
            foreach ($this->parent()->groups() as $key => $group) {
                $permission_groups[$key] = $group;
            }
        }
        foreach ($this->permission_groups as $key => $group) {
            $permission_groups[$key] = $group;
        }
        return $permission_groups;
    }
    
    /**
     * Returns the permissions associated with the group,
     * including those inherited from any number of ancestors.
     * 
     * @return \Oroboros\core\interfaces\library\auth\PermissionGroupContainerInterface
     */
    public function permissions(): \Oroboros\core\interfaces\library\auth\PermissionContainerInterface
    {
        $permissions = $this::containerizeInto(\Oroboros\core\library\auth\PermissionContainer::class);
        if (!is_null($this->parent()) && !$this->isBaseline()) {
            foreach ($this->parent()->permissions() as $key => $node) {
                $permissions[$key] = $node;
            }
        }
        foreach ($this->groups() as $key => $group) {
            foreach($group->permissions() as $k => $node) {
                $permissions[$k] = $node;
            }
        }
        return $permissions;
    }

    /**
     * Casts the object to a string representation of itself
     * 
     * @return string
     */
    public function __toString(): string
    {
        return $this->get();
    }

    public function serialize(): string
    {
        ;
    }

    public function unserialize(string $serialized): void
    {
        ;
    }

    /**
     * Returns whether or not the permission node is initialized
     * 
     * @return bool
     */
    public function isInitialized(): bool
    {
        return $this->is_initialized;
    }

    /**
     * Returns the parent permission group, if one exists
     * 
     * @return \Oroboros\core\interfaces\library\auth\IdentityGroupInterface|null
     */
    public function parent(): ?\Oroboros\core\interfaces\library\auth\IdentityGroupInterface
    {
        $this->resolveParent();
        return $this->parent;
    }

    /**
     * The internal identifier for the permission group
     * 
     * @return string
     */
    public function id(): string
    {
        return $this->group;
    }

    /**
     * The human readable name of the permission group, if any
     * 
     * @return string|null
     */
    public function name(): ?string
    {
        return $this->name;
    }

    /**
     * The human readable description of the permission group, if any
     * 
     * @return string|null
     */
    public function description(): ?string
    {
        return $this->description;
    }

    /**
     * Returns the permission group key if one exists.
     * 
     * Permission groups with keys can be validated with the correct hash
     * to prevent unauthorized assignment.
     * 
     * @return \Oroboros\core\interfaces\library\auth\KeyInterface|null
     */
    public function key(): ?\Oroboros\core\interfaces\library\auth\KeyInterface
    {
        return $this->key;
    }

    /**
     * Returns the whitelist if one exists.
     * 
     * Whitelisted permission groups can only be added
     * to the identity groups they define in their whitelist.
     * 
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface|null
     */
    public function whitelist(): ?\Oroboros\core\interfaces\library\container\ContainerInterface
    {
        return $this->whitelist;
    }

    /**
     * Returns whether or not this permission group is baseline.
     * 
     * Baseline permission groups are the underlying parent permission groups
     * of entire categories of permission groups.
     * 
     * @return bool
     */
    public function isBaseline(): bool
    {
        return $this->baseline;
    }

    /**
     * Returns whether or not this identity group is currently enabled.
     * This will return `false` if this group is disabled, or any
     * ancestor group is disabled.
     * 
     * @return bool
     */
    public function isEnabled(): bool
    {
        $parent = $this->parent();
        if (!is_null($parent) && !$parent->isEnabled()) {
            // Disabled by parent
            return false;
        }
        return $this->enabled;
    }

    /**
     * Returns whether or not this permission group is final.
     * 
     * Final permission groups cannot be assigned as parents of other permission groups.
     * @return bool
     */
    public function isFinal(): bool
    {
        return $this->final;
    }

    /**
     * Returns whether or not the permission is locked.
     * 
     * Locked permission groups cannot be edited further.
     * @return bool
     */
    public function isLocked(): bool
    {
        return $this->locked;
    }

    /**
     * Returns an array of information about the permission group
     * 
     * @return array
     */
//    public function details(): array
    public function details(): ?\Oroboros\core\interfaces\library\entity\EntityDetailCollectionInterface
    {
        return parent::details();
        return [
            'parent' => $this->parent(),
            'permission' => $this->get(),
            'literal' => $this->permission,
            'type' => $this->getIdentityType(),
            'scope' => $this->getIdentityScope(),
            'category' => $this->getIdentityCategory(),
            'subcategory' => $this->getIdentitySubCategory(),
            'context' => $this->getIdentityContext(),
            'prefix' => $this->getIdentityGroupPrefix(),
            'suffix' => $this->getIdentityGroupSuffix(),
            'separator' => $this->getIdentityGroupSeparator(),
            'name' => $this->name(),
            'description' => $this->description(),
            'key' => $this->key(),
            'whitelist' => $this->whitelist(),
            'baseline' => $this->isBaseline(),
            'final' => $this->isFinal(),
            'enabled' => $this->isEnabled(),
            'locked' => $this->isLocked(),
            'initialized' => $this->isInitialized(),
        ];
    }

    /**
     * Returns the permission group type
     * 
     * @return string
     */
    public function getIdentityType(): string
    {
        return static::IDENTITY_GROUP_TYPE;
    }

    /**
     * Returns the permission group scope, if it is defined
     * 
     * @return string|null
     */
    public function getIdentityScope(): ?string
    {
        return static::IDENTITY_GROUP_SCOPE;
    }

    /**
     * Returns the permission group category, if it is defined
     * 
     * @return string|null
     */
    public function getIdentityCategory(): ?string
    {
        return static::IDENTITY_GROUP_CATEGORY;
    }

    /**
     * Returns the permission group subcategory, if it is defined
     * 
     * @return string|null
     */
    public function getIdentitySubCategory(): ?string
    {
        return static::IDENTITY_GROUP_SUBCATEGORY;
    }

    /**
     * Returns the permission group context, if it is defined
     * 
     * @return string|null
     */
    public function getIdentityContext(): ?string
    {
        return static::IDENTITY_GROUP_CONTEXT;
    }

    /**
     * Returns the permission group prefix, if it is defined
     * 
     * @return string|null
     */
    public function getIdentityGroupPrefix(): ?string
    {
        return static::IDENTITY_GROUP_PREFIX;
    }

    /**
     * Returns the permission group suffix, if it is defined
     * 
     * @return string|null
     */
    public function getIdentityGroupSuffix(): ?string
    {
        return static::IDENTITY_GROUP_SUFFIX;
    }

    /**
     * Returns the permission group separator, if it is defined
     * 
     * @return string|null
     */
    public function getIdentityGroupSeparator(): ?string
    {
        return static::IDENTITY_GROUP_SEPARATOR;
    }

    /**
     * Enables the permission group.
     * 
     * @return void
     */
    public function enable(): void
    {
        if ($this->enabled) {
            return;
        }
        $this->enabled = true;
    }

    /**
     * Disables the permission group.
     * 
     * @return void
     */
    public function disable(): void
    {
        if (!$this->enabled) {
            return;
        }
        $this->enabled = false;
    }

    /**
     * Locks the permission group to prevent further edits.
     * 
     * @return void
     */
    public function lock(): void
    {
        if ($this->locked) {
            return;
        }
        $this->locked = true;
    }

    /**
     * Returns the permission group string
     * 
     * @return string
     */
    public function get(): string
    {
        $group = $this->constructIdentityGroup($this->getConstructionDefaults());
        return $group;
    }

    /**
     * Override this method to perform validation of supplied permission group strings
     * 
     * The default behavior is no validation.
     * 
     * @param string $group
     * @return void
     */
    protected function validateIdentityGroup(string $group): void
    {
        // no-op
    }

    /**
     * Performs the literal identity group representation display
     * and cleanup of duplicate separators
     * 
     * @param array $details
     * @return string
     */
    private function constructIdentityGroup(array $details): string
    {
        $group = trim(sprintf(
                '%1$s%2$s%1$s%3$s%1$s%4$s',
                $details['separator'],
                (array_key_exists('prefix', $details) ? $details['prefix'] : null),
                (array_key_exists('group', $details) ? $details['group'] : null),
                (array_key_exists('suffix', $details) ? $details['suffix'] : null),
            ), '.');
        // Remove dupliacte separators from inside the string
        while (strpos($group, $details['separator'] . $details['separator']) !== false) {
            $group = str_replace($details['separator'] . $details['separator'], $details['separator'], $group);
        }
        return $group;
    }

    /**
     * Returns the default permission group details
     * 
     * @return array
     */
    private function getConstructionDefaults(): array
    {
        $details = [
            'separator' => $this->getIdentityGroupSeparator(),
            'type' => $this->getIdentityType(),
            'scope' => $this->getIdentityScope(),
            'category' => $this->getIdentityCategory(),
            'subcategory' => $this->getIdentitySubCategory(),
            'context' => $this->getIdentityContext(),
            'prefix' => $this->getIdentityGroupPrefix(),
            'group' => $this->group,
            'suffix' => $this->getIdentityGroupSuffix(),
        ];
        return $details;
    }

    /**
     * Initializes the identity group object
     * 
     * @return void
     */
    private function initializeIdentityGroup(): void
    {
        $this->initializeIdentityGroupMasterSet();
        if (!$this->isInitialized()) {
            $group = $this->getCommand();
            $this->validateIdentityGroup($group);
            $this->group = $group;
            $this->setCommand($this->group);
            $this->handleIdentityGroupArguments();
            $this->initializePermissionGroups();
            $this->is_initialized = true;
        }
    }

    /**
     * Resolves parent group
     * 
     * @return void
     */
    private function resolveParent(): void
    {
        if (!is_object($this->parent) || !($this->parent instanceof \Oroboros\core\interfaces\library\auth\PermissionGroupInterface)) {
            if (!$this->hasArgument('parent')) {
                return;
            }
            $parent = $this->getArgument('parent');
            if (in_array($parent, [null, false, ''])) {
                $this->parent = null;
                return;
            }
            if (self::$master_set->has($parent)) {
                $this->parent = self::$master_set[$parent];
            }
        }
        if (is_null($this->parent)) {
            return;
        }
        if ($this->parent !== self::$master_set[$this->parent->id()]) {
            $this->parent = self::$master_set[$this->parent->id()];
        }
    }

    /**
     * Imports arguments declaring permission group definition if they exist
     * 
     * @return void
     */
    private function handleIdentityGroupArguments(): void
    {
        if ($this->hasArgument('id')) {
            $this->group = $this->getArgument('id');
        }
        if ($this->hasArgument('name')) {
            $this->name = $this->getArgument('name');
        }
        if ($this->hasArgument('description')) {
            $this->description = $this->getArgument('description');
        }
        if ($this->hasArgument('key')) {
            $this->key = $this->getArgument('key');
        }
        if ($this->hasArgument('parent')) {
            $this->parent = $this->getArgument('parent');
        }
        if ($this->hasArgument('whitelist')) {
            $this->whitelist = $this->getArgument('whitelist');
        }
        if ($this->hasArgument('baseline')) {
            $this->baseline = $this->getArgument('baseline');
        }
        if ($this->hasArgument('final')) {
            $this->final = $this->getArgument('final');
        }
        if ($this->hasArgument('enabled')) {
            $this->enabled = $this->getArgument('enabled');
        }
        if ($this->hasArgument('locked')) {
            $this->locked = $this->getArgument('locked');
        }
        $this->initializePermissionGroups();
    }

    /**
     * Loads the permissions associated with the permission group
     * @return void
     */
    private function initializePermissionGroups(): void
    {
        if (!is_null($this->permission_groups)) {
            // already done
            return;
        }
        $this->permission_groups = $this::containerizeInto(\Oroboros\core\library\auth\PermissionContainer::class);
        if (!$this->hasArgument('permission-groups')) {
            //nothing to add
            return;
        }
        $required = $this->getArgument('permission-groups');
        if (
            is_null($required) || (is_array($required) && empty($required) || (is_object($required) && ($required instanceof \Oroboros\core\interfaces\library\container\ContainerInterface) && empty($required->toArray()))
            )) {
            // nothing to add
            return;
        }
        // Update the permission groups from the model
        $permissions = $this->getModel('permission-group')->fetch();
        $satisfied = [];
        $invalid = [];
        foreach ($required as $key) {
            if (!$permissions->has($key)) {
                // Track for logging
                $invalid[] = $key;
                continue;
            }
            $this->permission_groups[$key] = $permissions[$key];
            $satisfied[] = $key;
        }
        if (!empty($invalid)) {
            $this->getLogger()->error('[type][scope][class] Identity group [identity-group] requested the following non-existent permission groups [permission-groups-invalid].', [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
                'identity-group' => $this->get(),
                'permission-groups-invalid' => implode(', ', $invalid),
            ]);
        }
    }

    /**
     * Initializes the identity group master set if it is not already initialized
     * 
     * @return void
     */
    private function initializeIdentityGroupMasterSet(): void
    {
        if (!is_null(self::$master_set)) {
            return;
        }
        self::$master_set = $this::containerizeInto(\Oroboros\core\library\auth\IdentityGroupContainer::class);
    }
}
