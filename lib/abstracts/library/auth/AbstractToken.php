<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\auth;

/**
 * Abstract Token
 * Provides abstraction for generating and validating tokens
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
abstract class AbstractToken extends \Oroboros\core\abstracts\library\AbstractLibrary implements \Oroboros\core\interfaces\library\auth\TokenInterface
{

    use \Oroboros\core\traits\library\auth\AuthCommonUtilityTrait;

    /**
     * This constant must be overridden by
     * concrete implementations to declare auth scope
     */
    const AUTH_SCOPE = null;
    
    const TOKEN_BYTE_COUNT = 128;

    private $token = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeAuthCommon();
        $this->initializeToken();
    }

    public function __toString(): string
    {
        return $this->get();
    }
    
    public function get(): string
    {
        return $this->token;
    }
    
    public function serialize(): string
    {
        return $this->get();
    }
    
    public function unserialize(string $serialized): void
    {
        $this->token = $serialized;
    }
    
    protected function generateToken(): string
    {
        $binary = openssl_random_pseudo_bytes(static::TOKEN_BYTE_COUNT);
        $token = bin2hex($binary);
        return $token;
    }
    
    private function initializeToken(): void
    {
        if (is_null($this->token)) {
            $token = $this->getCommand();
            if (is_null($token)) {
                $token = $this->generateToken();
            }
            $this->token = $token;
        }
    }
}
