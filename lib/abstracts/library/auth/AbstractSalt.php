<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\auth;

/**
 * Abstract Salt
 * Provides abstraction for generating and removing salts from passwords
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
abstract class AbstractSalt extends \Oroboros\core\abstracts\library\AbstractLibrary implements \Oroboros\core\interfaces\library\auth\SaltInterface
{

    use \Oroboros\core\traits\library\auth\AuthCommonUtilityTrait;

    /**
     * This constant must be overridden by
     * concrete implementations to declare auth scope
     */
    const AUTH_SCOPE = null;

    /**
     * The default salt character length used if no specific length is passed.
     */
    const AUTH_SALT_DEFAULT_LENGTH = 256;

    private $salt = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeAuthCommon();
        $this->initializeSalt();
    }

    /**
     * Casts the object to it's string representation
     * 
     * @return string
     */
    public function __toString(): string
    {
        return $this->get();
    }

    /**
     * Returns the salt string
     * 
     * @return string
     */
    public function get(): string
    {
        return $this->salt;
    }
    
    public function serialize(): string
    {
        return $this->get();
    }
    
    public function unserialize(string $serialized): void
    {
        $this->salt = $serialized;
    }

    /**
     * Generates a salt. Uses the length defined in the constructor
     * if one is passed, otherwise uses the value of `declareSaltLength`
     * 
     * @return string
     * @throws \Oroboros\core\exception\InvalidArgumentException
     *         if a length argument is given to the constructor
     *         that is not an integer
     */
    protected function generateSalt(): string
    {
        $length = $this->declareSaltLength();
        if ($this->hasArgument('length')) {
            $length = $this->getArgument('length');
        }
        if (!is_int($length)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Provided argument [%2$s] '
                        . 'must be of type [%3$s].', get_class($this), 'length', 'int')
            );
        }
        $bytes = ceil($length / 8);
        $return = '';
        for ($i = 0; $i < $bytes; $i++) {
            $return .= chr(mt_rand(0, 255));
        }
        return bin2hex($return);
    }

    /**
     * Override this method to declare the default salt length used
     * if no length parameter is passed.
     * 
     * The default behavior returns the value of the class constant `AUTH_SALT_DEFAULT_LENGTH`
     * 
     * @return int
     */
    protected function declareSaltLength(): int
    {
        return static::AUTH_SALT_DEFAULT_LENGTH;
    }

    private function initializeSalt(): void
    {
        $salt = $this->getCommand();
        if (is_null($salt)) {
            $salt = $this->generateSalt();
        }
        $this->salt = $salt;
        $this->setCommand($this->salt);
    }
}
