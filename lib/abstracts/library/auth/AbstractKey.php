<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\auth;

/**
 * Abstract Key
 * Provides abstraction for keys
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
abstract class AbstractKey extends \Oroboros\core\abstracts\library\AbstractLibrary implements \Oroboros\core\interfaces\library\auth\KeyInterface
{

    use \Oroboros\core\traits\library\auth\AuthCommonUtilityTrait;

    /**
     * This constant must be overridden by
     * concrete implementations to declare auth scope
     */
    const AUTH_SCOPE = null;

    private $key = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeAuthCommon();
        $this->initializeKey();
    }

    public function __toString(): string
    {
        return $this->get();
    }

    public function serialize(): string
    {
        return $this->get();
    }

    public function unserialize(string $serialized): void
    {
        $this->key = $serialized;
    }

    public function get(): string
    {
        return $this->key;
    }

    /**
     * Generates a key when no key is passed.
     * 
     * This method will be called from the constructor when
     * the key initializes if no key was passed into the object.
     * 
     * The default behavior generates a fast key of random characters
     * for ephemeral use that should not affect performance.
     * Generation of cryptographically secure keys should
     * override this method with a more expensive but secure
     * key generation method.
     * 
     * @return string
     */
    protected function generateKey(): string
    {
        $bytes = ceil(128 / 8);
        $binary = '';
        for ($i = 0; $i < $bytes; $i++) {
            $binary .= chr(mt_rand(0, 255));
        }
       $key = hash('sha1', $binary);
       return $key;
    }

    /**
     * Override this method to provide key validation for the specific key format
     * This will be called when the key is instantiated, and will verify both
     * a generated key as well as a passed key in the command of the constructor.
     * 
     * The default behavior performs no validation
     * 
     * @param string $key the potential key to validate
     * @return void
     * @throws \Oroboros\core\exception\auth\ValidationException
     *         All cases where the key fails validation
     */
    protected function validateKey(string $key): void
    {
        // no-op
    }

    /**
     * Initializes the key
     * 
     * @return void
     * @throws \Oroboros\core\exception\auth\ValidationException
     *         if the key does not pass validation.
     */
    private function initializeKey(): void
    {
        if (is_null($this->key)) {
            $key = $this->getCommand();
            if (is_null($key)) {
                $key = $this->generateKey();
            }
            try {
                $this->validateKey($key);
            } catch (\Oroboros\core\exception\auth\ValidationException $e) {
                // correctly handled validation error
                throw $e;
            } catch (\Exception $e) {
                // incorrectly handled validation error
                $expected = \Oroboros\core\exception\auth\ValidationException::class;
                throw new \Oroboros\core\exception\core\InvalidClassException(
                        sprintf('Error encountered in [%1$s]. Key validation '
                            . 'method failed to throw the expected exception. '
                            . 'Expected [%2$s], but received exception of '
                            . 'type [%3$s]. This class is not useable in '
                            . 'it\'s current state.', get_class($this), $expected, get_class($e))
                        , $e->getCode(), E_ERROR, __FILE__, __LINE__, $e);
            }
            $this->key = $key;
        }
    }
}
