<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\auth;

/**
 * Abstract Permission Manager
 * Provides abstraction for permission managers
 * 
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
abstract class AbstractPermissionManager extends \Oroboros\core\abstracts\pattern\director\AbstractDirector implements \Oroboros\core\interfaces\library\auth\PermissionManagerInterface
{

    use \Oroboros\core\traits\LoaderTrait;
    use \Oroboros\core\traits\library\auth\AuthCommonUtilityTrait;
    use \Oroboros\core\traits\library\auth\PermissionMatchableTrait;

    /**
     * This constant must be overridden by
     * concrete implementations to declare auth scope
     */
    const AUTH_SCOPE = null;

    /**
     * This constant may be overridden to declare a
     * valid permission class or stub name.
     */
    const AUTH_PERMISSION_CLASS = \Oroboros\core\library\auth\Permission::class;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        $this->verifyManager();
        parent::__construct($command, $arguments, $flags);
        $this->initializeAuthCommon();
        $this->initializeManager();
    }

    public function permission(string $permission = null): \Oroboros\core\interfaces\library\auth\PermissionInterface
    {
        return $this->load('library', static::AUTH_PERMISSION_CLASS, $identity);
    }

    /**
     * This method may be overridden to perform any additional
     * initialization steps
     * 
     * @return void
     */
    protected function initializeManager(): void
    {
        // no-op
    }

    private function verifyManager(): void
    {
        $this->verifyPermissionClass();
    }

    /**
     * Checks that a valid permission class is defined.
     * 
     * @return void
     */
    private function verifyKeychainClass(): void
    {
        $expected = \Oroboros\core\interfaces\library\auth\PermissionInterface::class;
        $this->preflightConstant('AUTH_PERMISSION_CLASS', $expected, false);
    }
}
