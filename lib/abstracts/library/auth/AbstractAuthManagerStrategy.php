<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\auth;

/**
 * Abstract Auth Manager Strategy
 * Provides abstraction for authentication manager strategies
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
abstract class AbstractAuthManagerStrategy extends \Oroboros\core\abstracts\pattern\strategy\AbstractStrategy implements \Oroboros\core\interfaces\library\auth\AuthManagerStrategyInterface
{

    use \Oroboros\core\traits\library\auth\AuthCommonUtilityTrait;

    /**
     * This constant must be overridden by
     * concrete implementations to declare auth scope
     */
    const AUTH_SCOPE = null;
    
    /**
     * Scopes the strategy to auth managers
     */
    const STRATEGY_TARGET_INTERFACE = \Oroboros\core\interfaces\library\auth\AuthManagerInterface::class;

    /**
     * A list of all currently known auth managers
     * @var array
     */
    private static $auth_managers = [
        'user' => \Oroboros\core\library\auth\login\LoginManager::class,
        'login' => \Oroboros\core\library\auth\login\LoginManager::class,
    ];

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeAuthCommon();
        $this->initializeStrategy();
    }

    /**
     * Registration method for auth managers.
     * Only the string class name or stub class name is needed.
     * 
     * If the passed string does not resolve to a class implementing
     * `\Oroboros\core\interfaces\library\auth\AuthManagerInterface`, then
     * an `\Oroboros\core\exception\InvalidArgumentException` will be raised.
     * 
     * @param string $manager
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    public static function registerManager(string $manager): void
    {
        self::verifyManager($manager);
        $class = $this->getFullClassName('library', $manager);
        $scope = $class::getAuthScope();
        self::$auth_managers[$scope] = $class;
    }

    /**
     * Returns a boolean designation as to whether a given type
     * identifier can resolve to a given auth manager.
     * 
     * If this method returns `true`, the method `getManager`
     * must not raise an exception for the same key.
     * 
     * If this method returns `false`, the method `getManager` MUST
     * raise an exception for the same key.
     * 
     * @param string $type
     * @return bool
     */
    public function hasManager(string $type): bool
    {
        return array_key_exists($type, self::$auth_managers);
    }

    /**
     * Returns the manager used to satisfy the specific type of authentication.
     * 
     * If the method `hasManager` returns `true` for the same key, this method MUST
     * return an instance of `\Oroboros\core\interfaces\library\auth\AuthManagerInterface`
     * 
     * If the method `hasManager` returns `false`, for the same key, this method
     * MUST throw an `\Oroboros\core\exception\InvalidArgumentException`
     * 
     * @param string $type
     * @return \Oroboros\core\interfaces\library\auth\AuthManagerInterface
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    public function getManager(string $type): \Oroboros\core\interfaces\library\auth\AuthManagerInterface
    {
        if (!$this->hasManager($type)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Provided key [%2$s] '
                        . 'does not resolve to a known manager.', get_class($this), $type)
            );
        }
        $manager = $this->load('library', self::$auth_managers[$type], $this->getCommand(), $this->getArguments(), $this->getFlags());
        $manager->setLogger($this->getLogger());
        return $manager;
    }

    /**
     * Initializes the strategy object
     * 
     * @return void
     */
    private function initializeStrategy(): void
    {
        
    }

    /**
     * Verifies a manager class or stub class name.
     * Raises an exception if it is not valid.
     * 
     * @param string $manager
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    private static function verifyManager(string $manager): void
    {
        $expected = \Oroboros\core\interfaces\library\auth\AuthManagerInterface::class;
        $class = $this->getFullClassName('library', $manager);
        if ($class === false) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Expected class constant '
                        . '[%2$s] with value [%3$s] does not define a valid class. '
                        . 'This class is not useable in it\'s current state.'
                        , get_class($this), 'AUTH_MANAGER_CLASS', $manager)
            );
        }
        if (!in_array($expected, class_implements($class))) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Expected class constant '
                        . '[%2$s] with value [%3$s] does not define a valid class '
                        . 'implementing expected interface [%4%s]. '
                        . 'This class is not useable in it\'s current state.'
                        , get_class($this), 'AUTH_MANAGER_CLASS', $manager, $expected)
            );
        }
    }
}
