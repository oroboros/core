<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\auth;

/**
 * Abstract Identity Strategy
 * Provides abstraction for acting as an identity strategy
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
abstract class AbstractIdentityStrategy extends \Oroboros\core\abstracts\pattern\strategy\AbstractStrategy implements \Oroboros\core\interfaces\library\auth\IdentityStrategyInterface
{

    use \Oroboros\core\traits\ContainerPackagerUtility;
    use \Oroboros\core\traits\LoaderTrait;
    use \Oroboros\core\traits\StringUtilityTrait;
    use \Oroboros\core\traits\library\resolver\ResolverUtilityTrait;
    use \Oroboros\core\traits\library\auth\AuthCommonUtilityTrait;
    
    /**
     * Scopes the strategy to identities
     */
    const STRATEGY_TARGET_INTERFACE = \Oroboros\core\interfaces\library\auth\IdentityInterface::class;
    
    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
    }
}
