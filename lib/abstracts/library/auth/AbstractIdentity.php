<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\auth;

/**
 * Abstract Identity
 * Provides abstraction for generating and validating identity strings
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
abstract class AbstractIdentity extends \Oroboros\core\abstracts\library\entity\AbstractEntity implements \Oroboros\core\interfaces\library\auth\IdentityInterface
{

    use \Oroboros\core\traits\model\ModelAwareUtilityTrait;
    use \Oroboros\core\traits\library\auth\AuthCommonUtilityTrait;
    use \Oroboros\core\traits\library\auth\PermissionMatchableTrait;

    /**
     * Declares the entity type as identity
     */
    const ENTITY_TYPE = 'identity';

    /**
     * Defines the entity scope as auth
     */
    const ENTITY_SCOPE = 'auth';

    /**
     * This constant must be overridden by
     * concrete implementations to declare auth scope
     */
    const AUTH_SCOPE = null;

    /**
     * This constant may be overridden to declare a identity
     * type to use for the identity object.
     * The identity type corresponds to an entity type the identity represents.
     */
    const AUTH_IDENTITY_TYPE = self::ENTITY_TYPE;

    /**
     * This constant may be overridden to declare a identity group
     * type to use for the identity object.
     * The identity group type corresponds to a type of identity group
     * that is used to resolve access rights and account restrictions
     * for the identity. Identities cannot be members of out-of-scope groups.
     */
    const AUTH_IDENTITY_GROUP_TYPE = AbstractIdentityGroup::AUTH_IDENTITY_TYPE;

    /**
     * This constant may be overridden to declare an organization type
     * to use for the identity.
     * 
     * All identities aside from the base organization must declare an organization.
     * 
     * Identities can only remain enabled so long as their underlying
     * organization is also enabled. System organizations are always enabled,
     * user generated organizations can be enabled or disabled by either their
     * respective ownership or administration.
     */
    const IDENTITY_ORGANIZATION_TYPE = \Oroboros\core\abstracts\library\entity\AbstractOrganizationEntity::ENTITY_SCOPE;

    /**
     * Match entity type
     */
    const IDENTITY_TYPE = self::ENTITY_TYPE;

    /**
     * Match entity scope
     */
    const IDENTITY_SCOPE = self::ENTITY_SCOPE;

    /**
     * Match entity category
     */
    const IDENTITY_CATEGORY = self::ENTITY_CATEGORY;

    /**
     * Match entity subcategory
     */
    const IDENTITY_SUBCATEGORY = self::ENTITY_SUBCATEGORY;

    /**
     * Match entity context
     */
    const IDENTITY_CONTEXT = self::ENTITY_CONTEXT;

    /**
     * This constant may be overridden to add a fixed prefix to permissions.
     * The default behavior is no prefix
     */
    const IDENTITY_PREFIX = AbstractPermission::PERMISSION_PREFIX;

    /**
     * This constant may be overridden to add a fixed suffix to permissions.
     * The default behavior is no suffix
     */
    const IDENTITY_SUFFIX = AbstractPermission::PERMISSION_SUFFIX;

    /**
     * This constant may be overridden to change the separator
     * used for generating permission nodes
     * 
     * Altering the separator will make identities incompatible
     * with other formats, but exists as a vehicle to map
     * permissions to more generalized purposes or maintain
     * interoperability between implementations
     * 
     * This is left to further implementation
     */
    const IDENTITY_SEPARATOR = AbstractPermission::PERMISSION_SEPARATOR;

    /**
     * The master set of identities
     * 
     * @var \Oroboros\core\interfaces\library\auth\IdentityCollectionInterface
     */
    private static $master_set = null;

    /**
     * The identity groups assigned to the identity
     * 
     * @var \Oroboros\core\interfaces\library\auth\IdentityGroupContainer
     */
    private $identity_groups = null;

    /**
     * The loose permissions assigned to the identity
     * 
     * Any permissions in this container will be added to the output
     * set prior to delivery.
     * 
     * This overrides all other inheritance and specification
     * at the group or permission level.
     * 
     * @var \Oroboros\core\interfaces\library\auth\PermissionsGroupContainer
     */
    private $permissions = null;

    /**
     * The loosse permissions negated by the identity
     * 
     * Any permissions in this container will be removed from the output
     * set prior to delivery.
     * 
     * This overrides all other inheritance and specification
     * at the group or permission level.
     * 
     * @var \Oroboros\core\interfaces\library\auth\PermissionContainer
     */
    private $negations = null;

    /**
     * The host of the identity
     * @var string
     */
    private $host = null;

    /**
     * The literal representation of the identity name
     * @var string
     */
    private $id = null;

    /**
     * The identifying marker used to associate with the account.
     * This an email address, phone number, account number, etc
     * 
     * @var string
     */
    private $identity = null;

    /**
     * The organization that the identity belongs to, if any
     * 
     * @var \Oroboros\core\interfaces\library\entity\OrganizationEntityInterface
     */
    private $organization = null;

    /**
     * Identity supplied information about itself
     * 
     * @var \Oroboros\core\interfaces\library\data\DataContainerInterface
     */
    private $details = null;

    /**
     * Identity supplied valid credentials
     * 
     * @var \Oroboros\core\interfaces\library\auth\CredentialContainerInterface
     */
    private $credentials = null;

    /**
     * The readable name of the identity, if one is defined.
     * 
     * @var string
     */
    private $name = null;

    /**
     * The readable description or bio of the identity, if one is defined.
     * 
     * @var string
     */
    private $description = null;

    /**
     * An optional security key for the identity.
     * 
     * This represents the login hash for the identity if one exists.
     * 
     * @var \Oroboros\core\interfaces\library\auth\KeyInterface
     */
    private $key = null;

    /**
     * If the identity is whitelisted to only be allowed by specific endpoints,
     * this will contain a list of those endpoints
     * 
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface|null
     */
    private $whitelist = null;

    /**
     * Whether or not the identity itself is marked as baseline
     * 
     * Baseline identities are bottom level
     * identities provided by the system. Details on baseline identities
     * can only be changes by app administration if at all.
     * @var bool
     */
    private $baseline = false;

    /**
     * Whether or not the identity itself is marked final
     * 
     * Final identities cannot be edited at all except by internal processes
     * @var bool
     */
    private $final = false;

    /**
     * Whether or not the identity itself is enabled
     * 
     * Disabled identities cannot be used but remain in the registry
     * 
     * @var bool
     */
    private $enabled = true;

    /**
     * Whether or not the identity object is locked
     * Locked identities cannot be edited or mutated by administrators,
     * but can be edited by root or system processes
     * 
     * @var bool
     */
    private $locked = false;

    /**
     * Determines whether the identity is initialized
     * 
     * @var bool
     */
    private $is_initialized = false;

    /**
     * Returns a copy of the identity master set in the current identity type scope.
     * The original set is preserved, this does not provide direct access
     * to the internal identity master set, only a copy of it.
     * 
     * @return \Oroboros\core\interfaces\library\auth\IdentityContainerInterface
     */
    public static function index(): \Oroboros\core\interfaces\library\auth\IdentityContainerInterface
    {
        static::initializeIdentityMasterSet();
        $identities = self::$master_set;
        if ($identities->has(static::IDENTITY_TYPE)) {
            $identities = $identities[static::IDENTITY_TYPE];
        }
        $identities = $identities->toArray();
        return static::containerizeInto(\Oroboros\core\library\auth\IdentityContainer::class, $identities);
    }

    /**
     * Imports a container of identity definitions into the master set
     * 
     * @param \Oroboros\core\interfaces\library\auth\IdentityContainerInterface $identities
     * @return void
     */
    public static function import(\Oroboros\core\interfaces\library\auth\IdentityContainerInterface $identities): void
    {
        static::initializeIdentityMasterSet();
        foreach ($identities as $key => $identity) {
            static::register($identity);
        }
    }

    /**
     * Registers an identity with the master set
     * 
     * @param \Oroboros\core\interfaces\library\auth\IdentityInterface $identity
     * @return void
     */
    public static function register(\Oroboros\core\interfaces\library\auth\IdentityInterface $identity): void
    {
        static::initializeIdentityMasterSet();
        $type = $identity->getIdentityType();
        if (!self::$master_set->has($type)) {
            self::$master_set[$type] = static::containerizeInto(\Oroboros\core\library\auth\IdentityContainer::class);
        }
        self::$master_set[$type][$identity->id()] = $identity;
    }

    /**
     * Unregisters an identity from the master set.
     * To unregister, the exact same identity that exists
     * in the master set must be passed.
     * 
     * @param \Oroboros\core\interfaces\library\auth\$identityInterface $identity
     * @oaran string|null $key
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException
     *         If the given identity group identifier exists in the master set,
     *         but the given identity group is not the originally registered permission group.
     */
    public static function unregister(\Oroboros\core\interfaces\library\auth\IdentityInterface $identity, string $key = null): void
    {
        static::initializeIdentityMasterSet();
        $type = $identity->getIdentityType();
        if (!self::$master_set->has($type)) {
            // nothing to do
            return;
        }
        if (self::$master_set[$type]->has($identity->get()) && self::$master_set[$type][$identity->get()] !== $identity) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Identity mismatch for identity [%2$s]. '
                        . 'Unregistration requires passing the same identity that is registered, '
                        . 'copies cannot be used to unregister the original.', static::class, $identity->get())
            );
        }
        unset(self::$master_set[$type][$identity->get()]);
    }

    /**
     * Identities use the default constructor.
     * The value of `$command` will become the base id
     * unless arguments explicitly define one.
     * 
     * @param string $command
     * @param array $arguments
     * @param array $flags
     */
    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeAuthCommon();
        $this->initializeIdentity();
        if (!self::$master_set->has($this->get())) {
            // Only originals will be auto-registered
            static::register($this);
        }
        $this->updateFromIndex();
    }

    /**
     * Matches a given permission node and returns a boolean designation
     * as to whether this identity has access to the specified permission.
     * 
     * If the identity is disabled, will always return `false`.
     * 
     * If the identity is assigned direct permissions that evaluates
     * to `true` for the given permission, returns `true`.
     * This takes precedence over group access rights when it applies.
     * 
     * If an identity group is assigned to this identity that
     * returns `true` on `$group->match($permission)` and
     * that group is not disabled, this will return `true`.
     * 
     * If the group has a parent and the parent returns `true`, returns `true`.
     * 
     * Returns `false` in all other cases.
     * 
     * @see \Oroboros\core\interfaces\library\auth\PermissionInterface::match()
     * @see \Oroboros\core\interfaces\library\auth\IdentityGroupInterface::match()
     * @param string $permission
     * @return bool
     */
    public function can(string $permission): bool
    {
        if (!$this->isEnabled()) {
            // Disabled identities cannot match permissions
            return false;
        }
        // Check permission assignments first
        foreach ($this->permissions() as $key => $node) {
            if ($node->match($permission)) {
                return true;
            }
        }
        // Check identity group assignments
        foreach ($this->groups() as $key => $identity_group) {
            if ($identity_group->match($permission)) {
                return true;
            }
        }
        // All other cases are false
        return false;
    }

    /**
     * Casts the object to a string representation of itself
     * 
     * @return string
     */
    public function __toString(): string
    {
        return $this->get();
    }

    public function serialize(): string
    {
        ;
    }

    public function unserialize(string $serialized): void
    {
        ;
    }

    /**
     * Returns a boolean designation as to whether
     * a given credential set allows access to the identity.
     * 
     * If the given credentials are sufficient to authenticate as the identity, returns `true`
     * 
     * @param \Oroboros\core\interfaces\library\auth\CredentialContainerInterface $credentials
     * @return bool
     */
    public function authenticate(\Oroboros\core\interfaces\library\auth\CredentialContainerInterface $credentials): bool
    {
        return $this->evaluateAuthentication($credentials);
    }

    /**
     * Returns whether or not the identity is initialized
     * 
     * @return bool
     */
    public function isInitialized(): bool
    {
        return $this->is_initialized;
    }

    /**
     * Returns whether or not this identity is baseline.
     * 
     * Baseline identities are provided by the system and cannot be deleted
     * or edited except by internal processes.
     * 
     * @return bool
     */
    public function isBaseline(): bool
    {
        return $this->baseline;
    }

    /**
     * Returns whether or not this identity is currently enabled
     * 
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * Returns whether or not this identity is final.
     * 
     * Final identities cannot be edited or assigned except by system processes or root.
     * @return bool
     */
    public function isFinal(): bool
    {
        return $this->final;
    }

    /**
     * Returns whether or not the identity is locked.
     * 
     * Locked identities cannot be edited further except by system processes or root.
     * @return bool
     */
    public function isLocked(): bool
    {
        return $this->locked;
    }

    /**
     * Returns the identity groups associated with the identity.
     * 
     * @return \Oroboros\core\interfaces\library\auth\IdentityGroupContainerInterface
     */
    public function groups(): \Oroboros\core\interfaces\library\auth\IdentityGroupContainerInterface
    {
        $identity_groups = $this::containerizeInto(\Oroboros\core\library\auth\IdentityGroupContainer::class);
        if (!is_null($this->identity_groups)) {
            foreach ($this->identity_groups as $key => $group) {
                $identity_groups[$key] = $group;
            }
        }
        return $identity_groups;
    }

    /**
     * Returns the loose permissions associated with the identity,
     * and any granted by identity group association.
     * 
     * @return \Oroboros\core\interfaces\library\auth\PermissionContainerInterface
     */
    public function permissions(): \Oroboros\core\interfaces\library\auth\PermissionContainerInterface
    {
        $permissions = $this::containerizeInto(\Oroboros\core\library\auth\PermissionContainer::class);
        foreach ($this->groups() as $key => $group) {
            foreach ($group->permissions() as $permkey => $permission) {
                if (!$permissions->has($permkey)) {
                    $permissions[$permkey] = $permission;
                }
            }
        }
        foreach ($this->permissions as $permkey => $permission) {
            if (!$permissions->has($permkey)) {
                $permissions[$permkey] = $permission;
            }
        }
        // Deduct negations from the output set
        foreach ($this->negations() as $key => $negation) {
            if ($permissions->has($key)) {
                unset($permissions[$key]);
            }
        }
        return $permissions;
    }

    /**
     * Returns the loose permission negations associated with the identity.
     * 
     * @return \Oroboros\core\interfaces\library\auth\PermissionContainerInterface
     */
    public function negations(): \Oroboros\core\interfaces\library\auth\PermissionContainerInterface
    {
        $negations = $this::containerizeInto(\Oroboros\core\library\auth\PermissionContainer::class);
        foreach ($this->negations as $permkey => $negation) {
            if (!$negations->has($permkey)) {
                $negations[$permkey] = $negation;
            }
        }
        return $negations;
    }

    /**
     * Returns the identity id
     * 
     * @return string
     */
    public function get(): string
    {
        return $this->identity;
    }

    /**
     * The allowed host for the identity.
     * 
     * @return string
     */
    public function host(): string
    {
        return $this->host;
    }

    /**
     * The internal identifier for the identity
     * 
     * @return string
     */
    public function id(): string
    {
        return $this->identity;
    }

    /**
     * The human readable name of the identity, if any
     * 
     * @return string|null
     */
    public function name(): ?string
    {
        return $this->name;
    }

    /**
     * The organization the entity belongs to, if any
     * 
     * @return \Oroboros\core\interfaces\library\entity\OrganizationEntityInterface|null
     */
    public function organization(): ?\Oroboros\core\interfaces\library\entity\OrganizationEntityInterface
    {
        return $this->organization;
    }

    /**
     * The human readable description of the identity, if any
     * 
     * @return string|null
     */
    public function description(): ?string
    {
        return $this->description;
    }

    /**
     * The identity details, if any
     * 
     * @return public function details(): ?\Oroboros\core\interfaces\library\entity\EntityDetailCollectionInterface
     */
    public function details(): ?\Oroboros\core\interfaces\library\entity\EntityDetailCollectionInterface
    {
        return parent::details();
    }

    /**
     * Returns the identity key if one exists.
     * 
     * Identities with keys can be validated with the correct hash
     * to prevent unauthorized assignment.
     * 
     * @return \Oroboros\core\interfaces\library\auth\KeyInterface|null
     */
    public function key(): ?\Oroboros\core\interfaces\library\auth\KeyInterface
    {
        return $this->key;
    }

    /**
     * Returns the whitelist if one exists.
     * 
     * Whitelisted identities can only be bound to specific origin hosts,
     * like localhost or a trusted network. Whitelisted identities bypass
     * some minor authorization checks that apply to all global users,
     * but are still beholden to permissions just like everything else is.
     * 
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface|null
     */
    public function whitelist(): ?\Oroboros\core\interfaces\library\container\ContainerInterface
    {
        return $this->whitelist;
    }

    /**
     * Returns the identity type
     * 
     * @return string
     */
    public function getIdentityType(): string
    {
        return static::IDENTITY_TYPE;
    }

    /**
     * Returns the identity scope, if it is defined
     * 
     * @return string|null
     */
    public function getIdentityScope(): ?string
    {
        return static::IDENTITY_SCOPE;
    }

    /**
     * Returns the identity category, if it is defined
     * 
     * @return string|null
     */
    public function getIdentityCategory(): ?string
    {
        return static::IDENTITY_CATEGORY;
    }

    /**
     * Returns the identity subcategory, if it is defined
     * 
     * @return string|null
     */
    public function getIdentitySubCategory(): ?string
    {
        return static::IDENTITY_SUBCATEGORY;
    }

    /**
     * Returns the identity context, if it is defined
     * 
     * @return string|null
     */
    public function getIdentityContext(): ?string
    {
        return static::IDENTITY_CONTEXT;
    }

    /**
     * Returns the identity prefix, if it is defined
     * 
     * @return string|null
     */
    public function getIdentityGroupPrefix(): ?string
    {
        return static::IDENTITY_PREFIX;
    }

    /**
     * Returns the identity suffix, if it is defined
     * 
     * @return string|null
     */
    public function getIdentityGroupSuffix(): ?string
    {
        return static::IDENTITY_SUFFIX;
    }

    /**
     * Returns the identity separator, if it is defined
     * 
     * @return string|null
     */
    public function getIdentityGroupSeparator(): ?string
    {
        return static::IDENTITY_SEPARATOR;
    }

    /**
     * Enables the identity.
     * 
     * @return void
     */
    public function enable(): void
    {
        if ($this->enabled) {
            return;
        }
        $this->enabled = true;
    }

    /**
     * Disables the identity.
     * 
     * @return void
     */
    public function disable(): void
    {
        if (!$this->enabled) {
            return;
        }
        $this->enabled = false;
    }

    /**
     * Locks the identity to prevent further edits.
     * 
     * @return void
     */
    public function lock(): void
    {
        if ($this->locked) {
            return;
        }
        $this->locked = true;
    }

    /**
     * Override this method to perform validation of supplied identity strings
     * 
     * The default behavior is no validation.
     * 
     * @param string $identity
     * @return void
     */
    protected function validateIdentity(string $identity): void
    {
        // no-op
    }

    /**
     * Override this method to perform evaluation of passed credentials.
     * 
     * This method must return `true` if credentials are sufficient
     * to access the identity, and `false` otherwise.
     * 
     * The default behavior always returns `false`.
     * 
     * @param \Oroboros\core\interfaces\library\auth\CredentialContainerInterface $credentials
     * @return bool
     */
    protected function evaluateAuthentication(\Oroboros\core\interfaces\library\auth\CredentialContainerInterface $credentials): bool
    {
        if ($credentials->identity->id() !== $this->id()) {
            // id mismatch
            return false;
        }
        if ($this->host() === 'localhost') {
            $params = $this::user()->request()->getServerParams();
            if ($params['SERVER_ADDR'] !== $params['REMOTE_ADDR']) {
                // Not a localhost request
                return false;
            }
        }
        if ($this->credentials->password->get() === 'NO_PASSWORD_DEFINED') {
            // No password set, always `false`
            return false;
        }
        if (in_array($this->credentials->password->get(), ['', '*', null], true)) {
            // Wildcard password, always `true`
            return false;
        }
        if ($this->credentials->password->get() === $credentials->password->get()) {
            // Default password, exact match.
            // This will pass, but we need to nag the user to reset it
            return true;
        }
        // All other cases return the hash match result
        return password_verify($this->credentials->password->get(), $credentials->password->get());
    }

    /**
     * Initializes the identity object
     * 
     * @return void
     */
    private function initializeIdentity(): void
    {
        $this->initializeIdentityMasterSet();
        $identity = $this->getCommand();
        if (is_null($identity)) {
            $identity = '';
        }
        if ($this->hasArgument('id')) {
            $identity = $this->getArgument('id');
        } elseif ($this->hasArgument('identity')) {
            $identity = $this->getArgument('identity');
        }
        $this->validateIdentity($identity);
        $this->identity = $identity;
        $this->setCommand($this->identity);
        $this->initializeHost();
        $this->initializeDetails();
        $this->initializeCredentials();
        $this->initializeOrganization();
        $this->initializeIdentityGroups();
        $this->initializePermissions();
        $this->initializeNegations();
    }

    private function updateFromIndex(): void
    {
        $index = $this::index();
        if ($index->has($this->id())) {
            $indexed = $index[$this->id()];
            $this->host = $indexed->host();
            $this->name = $indexed->name();
            $this->description = $indexed->description();
            $this->details = $indexed->details();
            $this->organization = $indexed->organization();
            $this->whitelist = $indexed->whitelist();
            $this->key = $indexed->key();
            $this->baseline = $indexed->isBaseline();
            $this->enabled = $indexed->isEnabled();
            $this->final = $indexed->isFinal();
            $this->locked = $indexed->isLocked();
            $this->is_initialized = $indexed->isInitialized();
        }
    }

    /**
     * Initializes the identity allowed host.
     * 
     * @return void
     */
    private function initializeHost(): void
    {
        if (!$this->hasArgument('host')) {
            // Use the default global host if not supplied
            $this->host = '*';
            return;
        }
        $this->host = $this->getArgument('host');
    }

    /**
     * Pulls the details from the model if they exist,
     * generates defaults if they do not.
     * 
     * @return void
     */
    private function initializeDetails(): void
    {
        
    }

    /**
     * Pulls the details from the model if they exist,
     * generates defaults if they do not.
     * 
     * @return void
     */
    private function initializeCredentials(): void
    {
        if (!is_null($this->credentials)) {
            // Already done
            return;
        }
        $credentials = $this::containerizeInto(\Oroboros\core\library\auth\Credentials::class);
        $credentials->identity = $this;
        if ($this->hasArgument('password')) {
            $credentials->password = $this->load('library', 'auth\\Password', $this->getArgument('password'), $this->getArguments(), $this->getFlags());
        }
        if ($this->hasArgument('token')) {
            $credentials->token = $this->load('library', 'auth\\Token', $this->getArgument('token'), $this->getArguments(), $this->getFlags());
        }
        $this->credentials = $credentials;
    }

    /**
     * Initializes the identity groups that the identity belongs to
     * 
     * @return void
     */
    private function initializeIdentityGroups(): void
    {
        if (!is_null($this->identity_groups)) {
            // Nothing to do
            return;
        }
        if (!$this->hasArgument('identity-groups')) {
            return;
        }
        $identity_groups = $this::containerizeInto(\Oroboros\core\library\auth\IdentityGroupContainer::class);
        $group_set = $this->getModel(static::AUTH_IDENTITY_GROUP_TYPE, null, $this->getArguments(), $this->getFlags())->fetch();
        foreach ($this->getArgument('identity-groups') as $group) {
            if ($group_set->has($group)) {
                $identity_groups[$group] = $group_set[$group];
            }
        }
        $this->identity_groups = $identity_groups;
    }

    /**
     * Initializes any loose permissions assigned directly to the identity
     * 
     * @return void
     */
    private function initializePermissions(): void
    {
        if (!is_null($this->permissions)) {
            // Nothing to do
            return;
        }
        $permissions = $this::containerizeInto(\Oroboros\core\library\auth\PermissionContainer::class);
        if ($this->hasArgument('permissions')) {
            $permission_set = $this->getModel('permission', null, $this->getArguments(), $this->getFlags())->fetch();
            foreach ($this->getArgument('permissions') as $permission) {
                if ($permission_set->has($permission)) {
                    $permissions[$permission] = $permission_set[$permission];
                }
            }
        }
        $this->permissions = $permissions;
    }

    /**
     * Initializes any loose permission negations assigned directly to the identity
     * 
     * @return void
     */
    private function initializeNegations(): void
    {
        if (!is_null($this->negations)) {
            // Nothing to do
            return;
        }
        $negations = $this::containerizeInto(\Oroboros\core\library\auth\PermissionContainer::class);
        if ($this->hasArgument('negations')) {
            $permission_set = $this->getModel('permission', null, $this->getArguments(), $this->getFlags())->fetch();
            foreach ($this->getArgument('negations') as $negation) {
                if ($permission_set->has($permission)) {
                    $negations[$negation] = $permission_set[$negation];
                }
            }
        }
        $this->negations = $negations;
    }

    /**
     * Initializes the organization the identity belongs to
     * 
     * @return void
     */
    private function initializeOrganization(): void
    {
        if (!is_null($this->organization)) {
            // Nothing to do
            return;
        }
        $organizations = $this->getModel('organization', null, $this->getArguments(), $this->getFlags())->fetch();
        $organization = $organizations->public;
        if ($this->hasArgument('organization') && $organizations->has($this->getArgument('organization'))) {
            $organization = $organizations[$this->getArgument('organization')];
        }
        $this->organization = $organization;
    }

    /**
     * Initializes the identity group master set if it is not already initialized
     * 
     * @return void
     */
    private static function initializeIdentityMasterSet(): void
    {
        if (is_null(self::$master_set)) {
            self::$master_set = static::containerizeInto(\Oroboros\core\library\auth\IdentityCollection::class);
        }
    }
}
