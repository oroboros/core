<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\auth;

/**
 * Abstract Password
 * Provides abstraction for generating and validating passwords
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
abstract class AbstractPassword extends \Oroboros\core\abstracts\library\AbstractLibrary implements \Oroboros\core\interfaces\library\auth\PasswordInterface
{

    use \Oroboros\core\traits\library\auth\AuthCommonUtilityTrait;
    
    /**
     * This constant may be overridden to declare a
     * valid authentication password class or stub name.
     */
    const AUTH_PASSWORD_HASH_CLASS = null;

    /**
     * This constant must be overridden by
     * concrete implementations to declare auth scope
     */
    const AUTH_SCOPE = null;

    private $password = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeAuthCommon();
        $this->initializePassword();
    }
    
    public function __toString(): string
    {
        return $this->get();
    }
    
    public function get(): string
    {
        return $this->password;
    }
    
    public function hash(): ?\Oroboros\core\interfaces\library\auth\PasswordHashInterface
    {
        if (is_null(static::AUTH_PASSWORD_HASH_CLASS)) {
            return null;
        }
        $result = $this->load('library', static::AUTH_PASSWORD_HASH_CLASS, $this->get());
        return $result;
    }

    private function initializePassword(): void
    {
        $this->verifyPasswordHashClass();
        $password = $this->getCommand();
        if (is_null($password)) {
            $password = '';
        }
        $this->password = $password;
    }
    
    /**
     * Checks that a valid password hash class is defined.
     * Password hash classes are optional.
     * 
     * @return void
     */
    private function verifyPasswordHashClass(): void
    {
        $expected = \Oroboros\core\interfaces\library\auth\PasswordHashInterface::class;
        $this->preflightConstant('AUTH_PASSWORD_HASH_CLASS', $expected, false);
    }
}
