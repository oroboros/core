<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\auth;

/**
 * Abstract Identity Definer
 * Definer implementation for identities
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
abstract class AbstractIdentityDefiner extends \Oroboros\core\abstracts\library\definer\AbstractDefiner implements \Oroboros\core\interfaces\library\auth\AuthInterface
{

    use \Oroboros\core\traits\library\auth\AuthCommonUtilityTrait;
    use \Oroboros\core\traits\model\ModelAwareUtilityTrait;

    /**
     * Designates that identity definers use DataHookSets
     * 
     * Must be a fully qualified classname, and must resolve to a class
     * implementing `\Oroboros\core\interfaces\library\hook\HookSetInterface`
     */
    const HOOKSET_CLASS = \Oroboros\core\library\hook\DataHookSet::class;

    /**
     * Sets the class scope to identity definer
     */
    const CLASS_SCOPE = 'identity-definer';

    /**
     * Designates that this class is part of the identity scope
     */
    const AUTH_SCOPE = 'identity';

    /**
     * Defines the default permission group class used by the definer to
     * generate permission groups that do not declare their own class.
     */
    const DEFAULT_IDENTITY_CLASS = \Oroboros\core\library\auth\Identity::class;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        $this->verifyAuthScope();
        $this->verifyDefaultIdentityClass();
        parent::__construct($command, $arguments, $flags);
        $this->initializeIdentityDefiner();
    }

    /**
     * Packages identity objects into the compiled set
     * 
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $data
     * @return \Oroboros\core\interfaces\library\auth\IdentityContainerInterface
     */
    protected function formatCompiled(\Oroboros\core\interfaces\library\container\ContainerInterface $data): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $result = $this->containerizeInto(\Oroboros\core\library\auth\IdentityContainer::class);
        foreach ($data as $key => $value) {
            $class = static::DEFAULT_IDENTITY_CLASS;
            if ($value->has('class')) {
                $class = $value['class'];
            }
            $result[$key] = $this->load('library', $class, $key, $value->toArray(), $this->getFlags());
        }
        return $result;
    }

    /**
     * Initializes the permission group definer
     * 
     * @return void
     */
    private function initializeIdentityDefiner(): void
    {
        
    }

    /**
     * Verifies that the default identity class constant is properly defined.
     * 
     * @return void
     * @throws \Oroboros\core\exception\core\InvalidClassException
     */
    private function verifyDefaultIdentityClass(): void
    {
        $expected = \Oroboros\core\interfaces\library\auth\IdentityInterface::class;
        $const = 'DEFAULT_IDENTITY_CLASS';
        $value = static::DEFAULT_IDENTITY_CLASS;
        $class = $this->getFullClassName('library', $value);
        if ($class === false || !in_array($expected, class_implements($class))) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. Expected class constant '
                        . '[%2$s] with value [%3$s] must resolve to a valid class '
                        . 'or stub class name implementing expected interface [%4$s]. '
                        . 'This class is not usable in it\'s current state.'
                        , get_class($this), $const, $value, $expected)
            );
        }
    }
}
