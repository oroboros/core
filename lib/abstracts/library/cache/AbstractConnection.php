<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\cache;

/**
 * Abstract Cache Connector
 * Represents a connection to a cache source
 *
 * @author Brian Dayhoff
 */
abstract class AbstractConnection extends \Oroboros\core\abstracts\library\AbstractLibrary implements \Oroboros\core\interfaces\library\connection\ConnectionInterface
{

    /**
     * The connection instance, if one exists.
     * @var mixed|null
     */
    private $connection = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->connection = $this->loadConnection($this->getCommand());
    }

    /**
     * Loads the required cache 
     * @var string|null $key The connection key.
     * If none is supplied and a default exists,
     * the default will be used.
     */
    abstract protected function loadConnection(string $key = null);

    protected function getConnection()
    {
        return $this->connection;
    }
}
