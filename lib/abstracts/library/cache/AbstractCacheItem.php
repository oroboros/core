<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\cache;

/**
 * Abstract Psr-6 Cache Item
 *
 * @author Brian Dayhoff
 */
abstract class AbstractCacheItem extends \Oroboros\core\abstracts\library\AbstractLibrary implements \Psr\Cache\CacheItemInterface
{

    /**
     * Minimum cache key length
     */
    const KEY_MIN_LENGTH = 1;

    /**
     * Maximum cache key length
     */
    const KEY_MAX_LENGTH = 64;

    /**
     * Cache key valid characters (uses preg_match)
     */
    const KEY_VALID_CHARS = '[a-zA-Z0-9\.\_]';

    /**
     * The value of the cache object
     * @var mixed
     */
    private $value = null;

    /**
     * The TTL for the cache item.
     * Null means non-expiring (or use the underlying maximum for the cache engine).
     * A \DateTimeInterface means expire at the given time.
     * @var null|\DateTimeInterface
     */
    private $ttl = null;
    private $pool = null;
    private $connection = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->verifyConnection();
        $this->verifyPool();
        $this->validateKey($command);
        $this->pool = $this->getArgument('pool');
        $this->connection = $this->getArgument('connection');
    }

    /**
     * Returns the key for the current cache item.
     *
     * The key is loaded by the Implementing Library, but should be available to
     * the higher level callers when needed.
     *
     * @return string
     *   The key string for this cache item.
     */
    public function getKey()
    {
        return $this->getCommand();
    }

    /**
     * Retrieves the value of the item from the cache associated with this object's key.
     *
     * The value returned must be identical to the value originally stored by set().
     *
     * If isHit() returns false, this method MUST return null. Note that null
     * is a legitimate cached value, so the isHit() method SHOULD be used to
     * differentiate between "null value was found" and "no value was found."
     *
     * @return mixed
     *   The value corresponding to this cache item's key, or null if not found.
     */
    public function get()
    {
        return (is_null($this->value) ? null : unserialize($this->value));
    }

    /**
     * Confirms if the cache item lookup resulted in a cache hit.
     *
     * Note: This method MUST NOT have a race condition between calling isHit()
     * and calling get().
     *
     * @return bool
     *   True if the request resulted in a cache hit. False otherwise.
     */
    public function isHit()
    {
        return false;
    }

    /**
     * Sets the value represented by this cache item.
     *
     * The $value argument may be any item that can be serialized by PHP,
     * although the method of serialization is left up to the Implementing
     * Library.
     *
     * @param mixed $value
     *   The serializable value to be stored.
     *
     * @return static
     *   The invoked object.
     */
    public function set($value)
    {
        if (is_resource($value)) {
            throw new \Oroboros\core\exception\cache\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Resources cannot be serialized, and are not valid cache values.', get_class($this)));
        }
        $this->value = serialize($value);
        return $this;
    }

    /**
     * Sets the expiration time for this cache item.
     *
     * @param \DateTimeInterface|null $expiration
     *   The point in time after which the item MUST be considered expired.
     *   If null is passed explicitly, a default value MAY be used. If none is set,
     *   the value should be stored permanently or for as long as the
     *   implementation allows.
     *
     * @return static
     *   The called object.
     */
    public function expiresAt($expiration)
    {
        if (is_null($expiration)) {
            // Persist as long as the underlying layer will allow, up to indefinitely.
        } elseif (is_object($expiration) && ($expiration instanceof \DateTimeInterface)) {
            // Extract the TTL from the \DateTimeInterface
            $expiration = (new \DateTime())->diff($expiration);
        } else {
            throw new \Oroboros\core\exception\cache\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided expiration in [%2$s] must be null or an instance of \\DateTimeInterface.', get_class($this), __METHOD__));
        }
        $this->ttl = $expiration;
        return $this;
    }

    /**
     * Sets the expiration time for this cache item.
     *
     * @param int|\DateInterval|null $time
     *   The period of time from the present after which the item MUST be considered
     *   expired. An integer parameter is understood to be the time in seconds until
     *   expiration. If null is passed explicitly, a default value MAY be used.
     *   If none is set, the value should be stored permanently or for as long as the
     *   implementation allows.
     *
     * @return static
     *   The called object.
     */
    public function expiresAfter($time)
    {
        if (is_int($time)) {
            // Generate a DateInterval object from the provided seconds
            $time = \DateInterval::createFromDateString($time . ' seconds');
        }
        if (!(is_object($time) && ($time instanceof \DateInterval))) {
            throw new \Oroboros\core\exception\cache\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided expiration in [%2$s] must be an integer or an instance of \\DateInterval.', get_class($this), __METHOD__));
        }
        $this->ttl = $time;
        return $this;
    }

    protected function getConnection(): \Oroboros\core\interfaces\library\connection\ConnectionInterface
    {
        return $this->connection;
    }

    protected function getStoredValue()
    {
        return $this->value;
    }

    protected function getStoredTtl()
    {
        return $this->ttl;
    }

    protected function validateKey($key)
    {
        if (!(
            is_string($key) && strlen($key) >= static::KEY_MIN_LENGTH && strlen($key) <= static::KEY_MAX_LENGTH && preg_match(static::KEY_VALID_CHARS, $key) !== false
            )) {
            throw new \Oroboros\core\exception\cache\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided key [%2$s] is not valid. Valid keys must ba a string between %3$s-%4$s characters,'
                        . 'consisting of the following valid characters [%5$s].', get_class($this), $key, static::KEY_MIN_LENGTH, static::KEY_MAX_LENGTH, static::KEY_VALID_CHARS));
        }
    }

    private function verifyPool(): void
    {
        if (!$this->hasArgument('pool')) {
            throw new \Oroboros\core\exception\InvalidArgumentException('Error encountered in [%1$s]. Expected argument [%2$s] was not supplied.', get_class($this), 'pool');
        }
        $subject = $this->getArgument('pool');
        $expected = 'Psr\\Cache\\CacheItemPoolInterface';
        if (!is_object($subject) || !($subject instanceof $expected)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Expected argument [%2$s] must be an instance of [%3$s].', get_class($this), 'pool', $expected));
        }
    }

    private function verifyConnection(): void
    {
        if (!$this->hasArgument('connection')) {
            throw new \Oroboros\core\exception\InvalidArgumentException('Error encountered in [%1$s]. Expected argument [%2$s] was not supplied.', get_class($this), 'connection');
        }
        $subject = $this->getArgument('connection');
        $expected = 'Oroboros\\core\\interfaces\\library\\connection\\ConnectionInterface';
        if (!is_object($subject) || !($subject instanceof $expected)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Expected argument [%2$s] must be an instance of [%3$s].', get_class($this), 'connection', $expected));
        }
    }
}
