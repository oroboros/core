<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\cache;

/**
 * Description of AbstractCacheStrategy
 *
 * @author Brian Dayhoff
 */
abstract class AbstractCacheStrategy extends \Oroboros\core\abstracts\pattern\strategy\AbstractStrategy implements \Oroboros\core\interfaces\library\cache\StrategyInterface
{

    /**
     * Designates that the given interface loads cache pools,
     * which are both Psr-6 compliant, and also oroboros libraries
     */
    const STRATEGY_TARGET_INTERFACE = 'Oroboros\\core\\interfaces\\library\\cache\\CachePoolInterface';

    /**
     * An array of valid cache pool classes that may be returned for a given request.
     * @var array
     */
    private static $cache_pools = [
        'null' => 'cache\\NullCache',
        'redis' => 'cache\\RedisCache'
    ];

    /**
     * Takes a cache type and connection name, and returns an instantiated 
     * corresponding cache pool of the given type and connection.
     * @param string $type The identifier keyword for the cache pool type
     * @param string $connection (optional) Required if the cache pool type
     *     requires a connection, otherwise unneccessary
     * @return \Oroboros\core\interfaces\library\cache\CachePoolInterface
     * @throws \InvalidArgumentException If the specified cache pool type is not known
     * @throws \ErrorException If the cache pool could not load with the given credentials
     */
    public function evaluate(string $type, string $connection = null): \Oroboros\core\interfaces\library\cache\CachePoolInterface
    {
        if (is_null($type)) {
            $cache = $this->load('library', 'cache\\NullCache');
        }
        if (!array_key_exists($type, self::$cache_pools)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'No known cache pool for type [%2$s]', get_class($this), $type));
        }
        if ($connection === 'default') {
            $connection = $this->getDefaultConnection($type);
        }
        try {
            return $this->load('library', self::$cache_pools[$type], $connection);
        } catch (\Exception $e) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. '
                        . 'Cache type [%2$s] with connection [%3$s] failed to load. '
                        . 'Reason [%4$s].', get_class($this), $type, $connection, $e->getMessage()), $e->getCode());
        }
    }

    /**
     * Registers a new option for a given cache strategy, and a corresponding identifier.
     * @param string $key The keyword to recognize a request for the class by.
     * @param string $class The class name to register. May be a stub.
     * @throws \InvalidArgumentException If the given class (or stub class)
     *     does not exist, or does not implement the cache pool interface
     */
    public static function setCachePoolHandler(string $key, string $class)
    {
        $proxy = new \Oroboros\core\pattern\factory\LoaderProxy();
        $expected = static::STRATEGY_TARGET_INTERFACE;
        $classname = $proxy->getFullClassName('library', $class);
        if (!$classname) {
            // No such class
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'No corresponding class available for [%2$s] [%3$s].', get_class($this), 'library', $class));
        }
        if (!in_array($expected, class_implements($classname))) {
            // Not a valid cache pool
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided cache pool class [%2$s] does not implement expected interface [%3$s].', get_class($this), $class, $expected));
        }
        self::$cache_pools[$key] = $class;
    }

    private function getDefaultConnection(string $type): ?string
    {
        if ($type === 'null') {
            // Null caches do not use connections.
            return null;
        }
        $connections = $this::app()->environment()->get('application')->get('connections');
        if (!array_key_exists('defaults', $connections) || !array_key_exists('cache', $connections['defaults']) || !array_key_exists($type, $connections['defaults']['cache'])
        ) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. '
                        . 'No default connection defined for cache type [%2$s].', get_class($this), $type));
        }
        return $connections['defaults']['cache'][$type];
    }
}
