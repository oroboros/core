<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\cache;

/**
 * Abstract PSR-6 Cache Item Pool
 *
 * @author Brian Dayhoff
 */
abstract class AbstractCacheItemPool extends \Oroboros\core\abstracts\library\AbstractLibrary implements \Oroboros\core\interfaces\library\cache\CachePoolInterface
{

    /**
     * Designates the cache item class that corresponds to the specific pool.
     * Must be overridden by child classes.
     */
    const CACHE_ITEM_CLASS = null;

    /**
     * Designates the connection class that corresponds to the specific pool.
     */
    const CONNECTION_CLASS = null;

    /**
     * Minimum cache key length
     */
    const KEY_MIN_LENGTH = 1;

    /**
     * Maximum cache key length
     */
    const KEY_MAX_LENGTH = 64;

    /**
     * Cache key valid characters (uses preg_match)
     */
    const KEY_VALID_CHARS = '[a-zA-Z0-9\.\_]';

    /**
     * The connection to the cache server
     * @var \Oroboros\core\interfaces\library\connection\ConnectionInterface
     */
    private $connection = null;

    /**
     * Pool of cache items.
     * @var Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $pool = null;

    /**
     * Any elements expected to be persisted later.
     * @var Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $deferred = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        $this->verifyCacheItemClass();
        $this->verifyConnectionClass();
        parent::__construct($command, $arguments, $flags);
        $this->initializePool();
        $this->initializeDeferred();
        $this->loadConnection();
    }

    /**
     * Returns a Cache Item representing the specified key.
     *
     * This method must always return a CacheItemInterface object, even in case of
     * a cache miss. It MUST NOT return null.
     *
     * @param string $key
     *   The key for which to return the corresponding Cache Item.
     *
     * @throws InvalidArgumentException
     *   If the $key string is not a legal value a \Psr\Cache\InvalidArgumentException
     *   MUST be thrown.
     *
     * @return CacheItemInterface
     *   The corresponding Cache Item.
     */
    public function getItem($key)
    {
        $this->validateKey($key);
        $cache_item = $this->load('library', static::CACHE_ITEM_CLASS, $key, ['pool' => $this, 'connection' => $this->connection]);
        $this->pool[$key] = $cache_item;
        return $cache_item;
    }

    /**
     * Returns a traversable set of cache items.
     *
     * @param string[] $keys
     *   An indexed array of keys of items to retrieve.
     *
     * @throws InvalidArgumentException
     *   If any of the keys in $keys are not a legal value a \Psr\Cache\InvalidArgumentException
     *   MUST be thrown.
     *
     * @return array|\Traversable
     *   A traversable collection of Cache Items keyed by the cache keys of
     *   each item. A Cache item will be returned for each key, even if that
     *   key is not found. However, if no keys are specified then an empty
     *   traversable MUST be returned instead.
     */
    public function getItems(array $keys = array())
    {
        $result = $this::containerize();
        foreach ($keys as $key) {
            $result[$key] = $this->getItem($key);
        }
        return $result;
    }

    /**
     * Confirms if the cache contains specified cache item.
     *
     * Note: This method MAY avoid retrieving the cached value for performance reasons.
     * This could result in a race condition with CacheItemInterface::get(). To avoid
     * such situation use CacheItemInterface::isHit() instead.
     *
     * @param string $key
     *   The key for which to check existence.
     *
     * @throws InvalidArgumentException
     *   If the $key string is not a legal value a \Psr\Cache\InvalidArgumentException
     *   MUST be thrown.
     *
     * @return bool
     *   True if item exists in the cache, false otherwise.
     */
    public function hasItem($key)
    {
        $this->validateKey($key);
        if (!$this->pool->has($key)) {
            $cache_item = $this->load('library', static::CACHE_ITEM_CLASS, $key, ['pool' => $this, 'connection' => $this->connection]);
            $this->pool[$key] = $cache_item;
        }
        return $this->pool->get($key)->isHit();
        return false;
    }

    /**
     * Deletes all items in the pool.
     *
     * @return bool
     *   True if the pool was successfully cleared. False if there was an error.
     */
    public function clear()
    {
        $clean = true;
        foreach ($this->pool as $key => $value) {
            if (!$this->deleteItem($value)) {
                $clean = false;
            }
        }
        return $clean;
    }

    /**
     * Removes the item from the pool.
     *
     * @param string $key
     *   The key to delete.
     *
     * @throws InvalidArgumentException
     *   If the $key string is not a legal value a \Psr\Cache\InvalidArgumentException
     *   MUST be thrown.
     *
     * @return bool
     *   True if the item was successfully removed. False if there was an error.
     */
    public function deleteItem($key)
    {
        $this->validateKey($key);
        if ($this->pool->has($key)) {
            unset($this->pool[$key]);
        }
        if ($this->deferred->has($key)) {
            unset($this->deferred[$key]);
        }
        return true;
    }

    /**
     * Removes multiple items from the pool.
     *
     * @param string[] $keys
     *   An array of keys that should be removed from the pool.

     * @throws InvalidArgumentException
     *   If any of the keys in $keys are not a legal value a \Psr\Cache\InvalidArgumentException
     *   MUST be thrown.
     *
     * @return bool
     *   True if the items were successfully removed. False if there was an error.
     */
    public function deleteItems(array $keys)
    {
        $result = true;
        foreach ($keys as $key) {
            if (!$this->deleteItem($key)) {
                $result = false;
            }
        }
        return $result;
    }

    /**
     * Persists a cache item immediately.
     *
     * @param CacheItemInterface $item
     *   The cache item to save.
     *
     * @return bool
     *   True if the item was successfully persisted. False if there was an error.
     */
    public function save(\Psr\Cache\CacheItemInterface $item)
    {
        try {
            $this->pool[$item->getKey()] = $item;
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }

    /**
     * Sets a cache item to be persisted later.
     *
     * @param CacheItemInterface $item
     *   The cache item to save.
     *
     * @return bool
     *   False if the item could not be queued or if a commit was attempted and failed. True otherwise.
     */
    public function saveDeferred(\Psr\Cache\CacheItemInterface $item)
    {
        try {
            $this->pool[$item->getKey()] = $item;
            $this->deferred[$item->getKey()] = $item;
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }

    /**
     * Persists any deferred cache items.
     *
     * @return bool
     *   True if all not-yet-saved items were successfully saved or there were none. False otherwise.
     */
    public function commit()
    {
        $clean = true;
        foreach ($this->deferred as $key => $item) {
            if (!$this->save($item)) {
                $clean = false;
            } else {
                unset($this->deferred[$key]);
            }
        }
        return $clean;
    }

    protected function getConnection(): \Oroboros\core\interfaces\library\connection\ConnectionInterface
    {
        return $this->connection;
    }

    protected function validateKey($key)
    {
        if (!(
            is_string($key) && strlen($key) >= static::KEY_MIN_LENGTH && strlen($key) <= static::KEY_MAX_LENGTH && preg_match(static::KEY_VALID_CHARS, $key) !== false
            )) {
            throw new \Oroboros\core\exception\cache\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided key [%2$s] is not valid. Valid keys must ba a string between %3$s-%4$s characters,'
                        . 'consisting of the following valid characters [%5$s].', get_class($this), $key, static::KEY_MIN_LENGTH, static::KEY_MAX_LENGTH, static::KEY_VALID_CHARS));
        }
    }

    private function initializePool(): void
    {
        $this->pool = $this::containerize();
    }

    private function initializeDeferred(): void
    {
        $this->deferred = $this::containerize();
    }

    private function loadConnection(): void
    {
        try {
            $this->connection = $this->load('library', static::CONNECTION_CLASS, $this->getCommand());
            $this->connection->connect();
        } catch (\Exception $e) {
            throw new \Oroboros\core\exception\cache\CacheException(sprintf('Error encountered in [%1$s]. '
                        . 'Unable to connect to the cache server.', get_class($this)), $e->getCode());
        }
    }

    private function verifyCacheItemClass(): void
    {
        if (is_null(static::CACHE_ITEM_CLASS)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(sprintf('Class [%1$s] is invalid. '
                        . 'Class constant [%2$s] must be defined with a valid Psr-6 cache item class.'
                        , get_class($this), 'CACHE_ITEM_CLASS'));
        }
    }

    private function verifyConnectionClass(): void
    {
        if (is_null(static::CONNECTION_CLASS)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(sprintf('Class [%1$s] is invalid. '
                        . 'Class constant [%2$s] must be defined with a class implementing [%3$s] (stub classes accepted).'
                        , get_class($this), 'CONNECTION_CLASS', 'Oroboros\\core\\interfaces\\library\\connection\\ConnectionInterface'));
        }
    }
}
