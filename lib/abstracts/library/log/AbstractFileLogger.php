<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\log;

/**
 * Abstract File Logger
 * Provides abstraction for a PSR-3 compliant loggers that write to files
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
abstract class AbstractFileLogger extends AbstractLogger implements \Oroboros\core\interfaces\library\log\FileLoggerInterface
{

    /**
     * Defines the logger type as file
     */
    const LOGGER_TYPE = 'file';

    /**
     * Default logger scope
     */
    const LOGGER_SCOPE = null;

    /**
     * The class used to write to logfiles.
     * Must implement `\Oroboros\core\interfaces\library\file\FileWriterInterface`
     * 
     * This constant can be overridden with any other valid class or stub class name
     */
    const LOG_FILE_WRITER_CLASS = \Oroboros\core\library\file\FileWriter::class;

    /**
     * The constant used to define where the base path for log files is located.
     * Defaults to the system declared temp directory.
     * 
     * This constant can be overridden with any other valid path.
     * 
     * This will be prefixed to the value of `OROBOROS_CORE_LOGS` to determine
     * the correct directory pathing.
     * 
     * The directory for `OROBOROS_CORE_LOGS` may be explicitly specified in your `.env` file
     */
    const LOG_FILE_BASE = self::PACKAGE_LOG_ROOT;

    /**
     * The constant used to define where the base path for log files is located.
     * Defaults to the system declared temp directory.
     * 
     * This constant can be overridden with any other valid path.
     * 
     * This will be prefixed to the value of `LOG_FILE_PATH` to determine
     * the correct directory pathing.
     */
    const LOG_FILE_PATH = 'logs';

    /**
     * This constant is used to define the prefix for the log file name.
     * Defaults to null
     * 
     * You may override this to determine an alternate prefix
     */
    const LOG_FILE_NAME_PREFIX = null;

    /**
     * This constant is used to define the suffix for the log file name.
     * Defaults to ".log"
     * 
     * You may override this to determine an alternate suffix
     */
    const LOG_FILE_NAME_SUFFIX = '.log';

    /**
     * The file writer used to write to log files.
     * 
     * @var \Oroboros\core\interfaces\library\file\FileWriterInterface
     */
    private $log_file_writer = null;

    /**
     * Uses the standard constructor
     * 
     * @param string $command
     * @param array $arguments
     * @param array $flags
     * @throws \Oroboros\core\exception\core\InvalidClassException
     *         If an invalid file writer class is declared
     * @throws \Oroboros\core\exception\ErrorException
     *         If the log file could not be opened, created, or written to
     */
    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        $this->verifyFileLogger();
        parent::__construct($command, $arguments, $flags);
        $this->initializeFileLogger();
    }

    /**
     * Logs messages directly to the console via the cli view object
     * 
     * @param string $level
     * @param string $message
     * @param type $context
     * @return void
     */
    protected function handleLogMessage(string $level, string $message, $context = []): void
    {
        parent::handleLogMessage($level, $message, $context);
        $output = $this->formatLogFileMessage($level, $message, $context);
        if (!is_null($this->log_file_writer)) {
            $this->getLogWriter()->write($output);
        }
    }
    
    /**
     * Perform any final formatting to the log message
     * string before it is written to the file.
     * Default behavior just returns exactly the same message
     * with a timestamp prefixed to it in atom format
     * 
     * The level and context are provided for evaluation also.
     * 
     * You may override this method to filter messages before they are written.
     * 
     * @param string $level
     * @param string $message
     * @param type $context
     * @return string
     */
    protected function formatLogFileMessage(string $level, string $message, $context): string
    {
        $timestamp = (new \DateTime())->format(\DateTime::ATOM);
        return sprintf('[%1$s] %2$s%3$s', $timestamp, $message, PHP_EOL);
    }

    /**
     * Internal getter for the log writer object
     * @return \Oroboros\core\interfaces\library\file\FileWriterInterface
     */
    protected function getLogWriter(): \Oroboros\core\interfaces\library\file\FileWriterInterface
    {
        return $this->log_file_writer;
    }

    /**
     * Declares the log file directory path.
     * Defaults to `/%sys-tmp-dir%/%app-name%/logs/`
     * 
     * @return string
     */
    protected function declareLogFilePath(): string
    {
        $path = sprintf(
            '%1$s%2$s%3$s',
            rtrim(static::LOG_FILE_BASE, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR,
            rtrim($this::app()->getApplicationDefinition()->name, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR,
            rtrim(static::LOG_FILE_PATH, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR,
        );
        return $path;
    }

    /**
     * Declares the log file name.
     * Default behavior is `%LOG_FILE_NAME_PREFIX%%YEAR%_%MONTH%_%DAY%_%LOG_FILE_NAME_SUFFIX%`
     * eg: `2021_09_07.log` with the default no prefix, today's date as of writing, and the standard suffix.
     * 
     * You may override this to return any filename
     * 
     * @return string
     */
    protected function declareLogFileName(): string
    {
        $date = new \DateTime();
        $date_string = $date->format('Y_m_d');
        return sprintf(
            '%1$s%2$s%3$s',
            static::LOG_FILE_NAME_PREFIX,
            $date_string,
            static::LOG_FILE_NAME_SUFFIX
        );
    }

    /**
     * Declares the arguments to pass to the log writer upon instantiation.
     * By default, passes the same arguments passed to the file logger.
     * 
     * You may override this method to customize instantiation arguments
     * for the log writer
     * 
     * @return array|null
     */
    protected function declareLogWriterArguments(): ?array
    {
        return $this->getArguments();
    }

    /**
     * Declares the arguments to pass to the log writer upon instantiation.
     * By default, passes the same flags passed to the file logger.
     * 
     * You may override this method to customize instantiation flags
     * for the log writer
     * 
     * @return array|null
     */
    protected function declareLogWriterFlags(): ?array
    {
        return $this->getFlags();
    }

    /**
     * Determines the options to pass to the
     * file writer (eg write mode, create directories, etc)
     * Default behavior is to set write mode to append and
     * create directories if they do not exist.
     * 
     * These will be applied after it instantiates.
     * 
     * @return array
     */
    protected function declareLogWriterOptions(): array
    {
        return [
            'generate-directories' => true,
            'write-mode' => 'a',
        ];
    }

    /**
     * Initializes the file logger
     * @return void
     * @throws \Oroboros\core\exception\ErrorException
     *         If the log file could not be opened, created, or written to
     */
    private function initializeFileLogger(): void
    {
        if (is_null($this->log_file_writer)) {
            $class = static::LOG_FILE_WRITER_CLASS;
            $writer = $this->load('library', $class, null, $this->declareLogWriterArguments(), $this->declareLogWriterFlags());
            foreach ($this->declareLogWriterOptions() as $key => $option) {
                $writer->setOption($key, $option);
            }
            $writer->setDirectory($this->declareLogFilePath());
            $writer->setFile($this->declareLogFileName());
            if (!$writer->isWriteable()) {
                throw new \Oroboros\core\exception\ErrorException(
                        sprintf('Error encountered in [%1$s]. Provided log file path '
                            . '[%2$s] is not writeable or could not be created.'
                            , get_class($this), $writer->getFullPath())
                );
            }
            $this->log_file_writer = $writer;
        }
    }

    /**
     * Verifies the file logger
     * 
     * @return void
     * @throws \Oroboros\core\exception\core\InvalidClassException
     */
    private function verifyFileLogger(): void
    {
        $expected = \Oroboros\core\interfaces\library\file\FileWriterInterface::class;
        $class = static::LOG_FILE_WRITER_CLASS;
        if ($this->getFullClassName('library', $class) === false || !in_array($expected, class_implements($this->getFullClassName('library', $class)))) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. Expected class constant '
                        . '[%2$s] with value [%3$s] must resolve to a valid class '
                        . 'or stub class name implementing expected interface [%4$s]. '
                        . 'This class is not useable in it\'s current state.',
                        get_class($this), 'LOG_FILE_WRITER_CLASS', $class, $expected)
            );
        }
    }
}
