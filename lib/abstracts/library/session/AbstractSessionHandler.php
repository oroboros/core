<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\session;

/**
 * Abstract Session
 * Provides abstraction for interacting with the PHP session in
 * an extensible and standards compliant manner.
 *
 * @author Brian Dayhoff
 */
abstract class AbstractSessionHandler extends \Oroboros\core\abstracts\library\AbstractLibrary implements \Oroboros\core\interfaces\library\session\SessionHandlerInterface
{

    use \Oroboros\core\traits\library\session\SessionCommonTrait;
    use \Oroboros\core\traits\StringUtilityTrait;

    /**
     * Uncomment for debugging
     */
//    const DEFAULT_LOGGER = 'log\\DebugLogger';

    /**
     * Defines the class scope as session
     */
    const CLASS_SCOPE = 'session';

    /**
     * This constant can optionally be overridden to designate a session type.
     * Session type should correspond to the class scope
     * of the runtime (eg "cli", "http", etc), or a derivative ancestor
     * of the current runtime (eg http is an ancestor of html, rest, ajax, etc)
     */
    const SESSION_TYPE = null;

    /**
     * This constant can optionally be overridden to designate a session scope.
     * Session scope should correspond to the save method of the session handler
     * (eg "file", "redis", "opcache", "memcache", "mysql", etc)
     * 
     * A value of null indicates that no save method is used and sessions
     * are ephemeral, only persisting through a single runtime. A null session
     * can use this to satisfy architecture without actually using
     * persistent sessions. In this case the session will be treated
     * as fresh on each subsequent runtime. Cookies will still be sent as
     * per normal, except nothing will persist. There will be no frontend
     * indication that sessions are not active. Likewise, this can be used to
     * prevent specific IP addresses from attempting to login or
     * submit forms/ajax/api calls.
     */
    const SESSION_SCOPE = null;

    /**
     * Determines the serialization method for the session
     */
    const SERIALIZATION_ENGINE = 'php_serialize';

    /**
     * Defines the default token class used to generate a session ID.
     * This may be overridden to use an alternate token identifier.
     * 
     * Must declare a fully qualified class or stub class name resolving
     * to a class implementing
     * `Oroboros\core\interfaces\library\auth\TokenInterface`
     */
    const SESSION_TOKEN_CLASS = 'session\\SessionToken';

    /**
     * Declares a prefix for the session name to be used for the save path.
     * The default prefix is identical to the normal php session prefix.
     * 
     * This constant may be overridden to generate an alternate session prefix.
     * 
     * This can be any valid string or null
     */
    const SESSION_PREFIX = 'sess_';

    /**
     * Declares a prefix for the session name to be used for the save path.
     * There is no default session suffix
     * 
     * This constant may be overridden to generate an alternate session suffix.
     * 
     * This can be any valid string or null
     */
    const SESSION_SUFFIX = '';

    /**
     * The container of all existing session values for the current session.
     * 
     * @var \Oroboros\core\interfaces\library\session\SessionContainerInterface
     */
    private static $session_values = null;

    /**
     * The name of the current session
     * 
     * @var string|null
     */
    private static $name = null;

    /**
     * The id of the current session
     * 
     * @var string|null
     */
    private static $id = null;

    /**
     * The path of the current session
     * 
     * @var string|null
     */
    private static $path = null;

    /**
     * The update timestamp for the current session
     * 
     * @var int|null
     */
    private static $update_timestamp = null;

    /**
     * The file writer object used to write session files.
     * 
     * This will always be the default file writer
     * and cannot be overridden.
     * 
     * This is to insure session stability is
     * not disrupted by custom file writers.
     * 
     * @var \Oroboros\core\library\file\FileWriter
     */
    private static $session_file_writer = null;

    /**
     * Use the session container
     */
    const CONTAINER_CLASS = \Oroboros\core\library\session\SessionContainer::class;

    /**
     * Session handlers use the standard constructor.
     * 
     * If any internals are undeclared or mis-declared,
     * an invalid class exception will be raised.
     * 
     * @param string $command
     * @param array $arguments
     * @param array $flags
     * @throws \Oroboros\core\exception\core\InvalidClassException
     */
    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        $this->verifySessionCommon();
        $this->verifySessionHandler();
        parent::__construct($command, $arguments, $flags);
        $this->initializeSessionCommon();
        $this->initializeSessionHandler();
        $this->initializeSessionOptions();
    }

    /**
     * Opens the session
     * 
     * The open callback works like a constructor in classes and is executed
     * when the session is being opened.
     * 
     * It is the first callback function executed when the session is started
     * automatically or manually with `session_start()`.
     * 
     * Return value is `true` for success, `false` for failure.
     * 
     * @param string $path
     * @param string $name
     * @return bool
     * @see \SessionHandlerInterface
     * @link https://www.php.net/manual/en/function.session-set-save-handler.php
     */
    public function open(string $path, string $name)
    {
        $this->getLogger()->debug('[type][scope][class] Opening session in path '
            . '[session-path] with name [session-name].', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'session-path' => $path,
            'session-name' => $name,
        ]);
        try {
            $oldpath = $this->getSessionPath();
            $oldname = $this->getSessionName();
            $this->verifySessionName($name);
            $this->verifySessionPath($path);
            $this->setSessionPath($path);
            $this->setSessionName($name);
            return true;
        } catch (\Exception $e) {
            $this->getLogger()->warning(
                sprintf('[type][scope][class] Unable to open session in path '
                    . '[session-path] with name [session-name].'
                    . '%1$s%2$sAn exception was raised of type [exception-type] '
                    . 'at line [exception-line] of file [exception-file]'
                    . '%1$s%2$sWith message: [exception-message]'
                    . '%1$s%2$sTrace: [exception-trace]'
                    , PHP_EOL, '    '
                ), [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
                'session-path' => $path,
                'session-name' => $name,
                'exception-type' => get_class($e),
                'exception-file' => $e->getFile(),
                'exception-line' => $e->getLine(),
                'exception-message' => $e->getMessage(),
                'exception-trace' => ltrim($this->indent($e->getTraceAsString(), 8), ' '),
            ]);
            if (is_null($oldpath)) {
                $this->resetSessionPath();
            } else {
                $this->setSessionPath($oldpath);
            }
            if (is_null($oldname)) {
                $this->resetSessionName();
            } else {
                $this->setSessionName($oldname);
            }
            return false;
        }
    }

    /**
     * Closes the session
     * 
     * The close callback works like a destructor in classes and is executed
     * after the session write callback has been called.
     * 
     * It is also invoked when `session_write_close()` is called.
     * Return value should be `true` for success, `false` for failure.
     * 
     * @return bool
     * @see \SessionHandlerInterface
     * @link https://www.php.net/manual/en/function.session-set-save-handler.php
     */
    public function close(): bool
    {
        $this->getLogger()->debug('[type][scope][class] Closing session of type '
            . '[session-type] with name [session-name].', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'session-type' => $this->getSessionPath(),
            'session-name' => $this->getSessionName(),
        ]);
        try {
            $this->resetSessionContainer();
            $this->resetSessionId();
            $this->resetSessionName();
            $this->resetSessionPath();
            return true;
        } catch (\Exception $e) {
            $this->getLogger()->warning(
                sprintf('[type][scope][class] Unable to close session in path '
                    . '[session-path] with name [session-name].'
                    . '%1$s%2$sAn exception was raised of type [exception-type] '
                    . 'at line [exception-line] of file [exception-file]'
                    . '%1$s%2$sWith message: [exception-message]'
                    . '%1$s%2$sTrace: [exception-trace]'
                    , PHP_EOL, '    '
                ), [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
                'session-path' => static::$path,
                'session-name' => static::$name,
                'exception-type' => get_class($e),
                'exception-file' => $e->getFile(),
                'exception-line' => $e->getLine(),
                'exception-message' => $e->getMessage(),
                'exception-trace' => ltrim($this->indent($e->getTraceAsString(), 8), ' '),
            ]);
            return false;
        }
    }

    /**
     * Reads from the session
     * 
     * The read callback must always return a session encoded (serialized) string,
     * or an empty string if there is no data to read.
     * 
     * This callback is called internally by PHP when the session starts or
     * when `session_start()` is called.
     * 
     * Before this callback is invoked PHP will invoke the `open` callback.
     * 
     * The value this callback returns must be in exactly the same serialized format
     * that was originally passed for storage to the write callback.
     * 
     * The value returned will be unserialized automatically by PHP and used to
     * populate the `$_SESSION` superglobal.
     * 
     * While the data looks similar to `serialize()` please note it is a
     * different format which is specified in the `session.serialize_handler`
     * ini setting.
     * 
     * @param type $name
     * @return string
     * @see \SessionHandlerInterface
     * @link https://www.php.net/manual/en/function.session-set-save-handler.php
     */
    public function read(string $id): string
    {
        try {
            $this->verifySessionId($id);
            $this->getLogger()->debug('[type][scope][class] Reading session of '
                . 'type [session-type] with name [session-name] and session id '
                . '[session-id].', [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
                'session-type' => $this->getSessionPath(),
                'session-name' => $this->getSessionName(),
                'session-id' => $id,
            ]);
            $this->setSessionId($id);
            return serialize(unserialize($this->retrieveSession($id))->toArray());
        } catch (\Oroboros\core\exception\session\NotFoundException $e) {
            // New session file needed
            $this->createSession($id);
            return serialize(unserialize($this->retrieveSession($id))->toArray());
        } catch (\Oroboros\core\exception\session\ReadErrorException $e) {
            // Session file not readable. This needs to be escalated.
            $this->getLogger()->critical(
                sprintf('[type][scope][class] Session cache is not readable. '
                    . '%1$s%2$sIntervention to correct directory permissions is needed. Session cannot be loaded at all. '
                    . '%1$s%2$sEvaluating [session-path] with name [session-name] and session id [session-id].'
                    . '%1$s%2$sAn exception was raised of type [exception-type] at line [exception-line] of file [exception-file]'
                    . '%1$s%2$sWith message: [exception-message]'
                    . '%1$s%2$sTrace: [exception-trace]'
                    , PHP_EOL, '    '
                ), [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
                'session-path' => $this->getSessionPath(),
                'session-name' => $this->getSessionName(),
                'session-id' => $id,
                'exception-type' => get_class($e),
                'exception-file' => $e->getFile(),
                'exception-line' => $e->getLine(),
                'exception-message' => $e->getMessage(),
                'exception-trace' => ltrim($this->indent($e->getTraceAsString(), 8), ' '),
            ]);
        } catch (\Oroboros\core\exception\session\WriteErrorException $e) {
            // Session file not writable. This needs to be escalated.
            $this->getLogger()->critical(
                sprintf('[type][scope][class] Session cache is not writable. '
                    . '%1$s%2$sIntervention to correct directory permissions is needed. No additional session data can be stored. '
                    . '%1$s%2$sEvaluating [session-path] with name [session-name] and session id [session-id]. '
                    . '%1$s%2$sAn exception was raised of type [exception-type] at line [exception-line] of file [exception-file]'
                    . '%1$s%2$sWith message: [exception-message]'
                    . '%1$s%2$sTrace: [exception-trace]'
                    , PHP_EOL, '    '
                ), [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
                'session-path' => $this->getSessionPath(),
                'session-name' => $this->getSessionName(),
                'session-id' => $id,
                'exception-type' => get_class($e),
                'exception-file' => $e->getFile(),
                'exception-line' => $e->getLine(),
                'exception-message' => $e->getMessage(),
                'exception-trace' => ltrim($this->indent($e->getTraceAsString(), 8), ' '),
            ]);
        } catch (\Exception $e) {
            $this->getLogger()->warning(
                sprintf('[type][scope][class] Unable to read session in path '
                    . '[session-path] with name [session-name] and session id [session-id].'
                    . '%1$s%2$sAn exception was raised of type [exception-type] at line [exception-line] of file [exception-file]'
                    . '%1$s%2$sWith message: [exception-message]'
                    . '%1$s%2$sTrace: [exception-trace]'
                    , PHP_EOL, '    '
                ), [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
                'session-path' => $this->getSessionPath(),
                'session-name' => $this->getSessionName(),
                'session-id' => $id,
                'exception-type' => get_class($e),
                'exception-file' => $e->getFile(),
                'exception-line' => $e->getLine(),
                'exception-message' => $e->getMessage(),
                'exception-trace' => ltrim($this->indent($e->getTraceAsString(), 8), ' '),
            ]);
        }
        return '';
    }

    /**
     * Writes to the session
     * 
     * The write callback is called when the session needs to be saved and closed.
     * 
     * This callback receives the current session ID and a serialized version
     * the `$_SESSION` superglobal.
     * 
     * The serialization method used internally by PHP is specified in the
     * `session.serialize_handler` ini setting.
     * 
     * The serialized session data passed to this callback should be stored against
     * the passed session ID.
     * When retrieving this data, the read callback must return the exact value
     * that was originally passed to the write callback.
     * 
     * This callback is invoked when PHP shuts down or explicitly
     * when `session_write_close()` is called.
     * 
     * Note that after executing this function PHP will internally execute
     * the `close` callback.
     * 
     * @note The "write" handler is not executed until after the output stream
     *       is closed. Thus, output from debugging statements in the "write"
     *       handler will never be seen in the browser. If debugging output
     *       is necessary, it is suggested that the debug output be written
     *       to a file instead.
     * 
     * @param string $id
     * @param string $data
     * @see \SessionHandlerInterface
     * @link https://www.php.net/manual/en/function.session-set-save-handler.php
     */
    public function write(string $id, string $data)
    {
        try {
            $this->getLogger()->debug('[type][scope][class] Writing session of '
                . 'type [session-type] with name [session-name] and session id '
                . '[session-id].', [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
                'session-type' => $this->getSessionPath(),
                'session-name' => $this->getSessionName(),
                'session-id' => $id,
            ]);
            $this->setSessionId($id);
            $this->saveSession($id, $data);
            return true;
        } catch (\Exception $e) {
            $this->getLogger()->warning(
                sprintf('[type][scope][class] Unable to write session in path '
                    . '[session-path] with name [session-name] and session id [session-id].'
                    . '%1$s%2$sAn exception was raised of type [exception-type] at line [exception-line] of file [exception-file]'
                    . '%1$s%2$sWith message: [exception-message]'
                    . '%1$s%2$sTrace: [exception-trace]'
                    , PHP_EOL, '    '
                ), [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
                'session-path' => $this->getSessionPath(),
                'session-name' => $this->getSessionName(),
                'session-id' => $id,
                'exception-type' => get_class($e),
                'exception-file' => $e->getFile(),
                'exception-line' => $e->getLine(),
                'exception-message' => $e->getMessage(),
                'exception-trace' => ltrim($this->indent($e->getTraceAsString(), 8), ' '),
            ]);
            return false;
        }
    }

    /**
     * Destroys the session
     * 
     * This callback is executed when a session is destroyed
     * with `session_destroy()` or with `session_regenerate_id()` with the
     * destroy parameter set to `true`.
     * 
     * Return value should be `true` for success, `false` for failure.
     * 
     * @param string $name
     * @return bool
     * @see \SessionHandlerInterface
     * @link https://www.php.net/manual/en/function.session-set-save-handler.php
     */
    public function destroy(string $id): bool
    {
        try {
            $this->getLogger()->debug('[type][scope][class] Destroying session '
                . 'of type [session-type] with name [session-name] and session '
                . 'id [session-id].', [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
                'session-type' => $this->getSessionPath(),
                'session-name' => $this->getSessionName(),
                'session-id' => $id,
            ]);
            $this->deleteSession($id);
            $this->resetSessionPath();
            $this->resetSessionName();
            $this->resetSessionId();
            $this->destroySessionContainer();
            return true;
        } catch (\Exception $e) {
            $this->getLogger()->warning(
                sprintf('[type][scope][class] Unable to destroy session in path '
                    . '[session-path] with name [session-name] and session id [session-id].'
                    . '%1$s%2$sAn exception was raised of type [exception-type] at line [exception-line] of file [exception-file]'
                    . '%1$s%2$sWith message: [exception-message]'
                    . '%1$s%2$sTrace: [exception-trace]'
                    , PHP_EOL, '    '
                ), [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
                'session-path' => $this->getSessionPath(),
                'session-name' => $this->getSessionName(),
                'session-id' => $id,
                'exception-type' => get_class($e),
                'exception-file' => $e->getFile(),
                'exception-line' => $e->getLine(),
                'exception-message' => $e->getMessage(),
                'exception-trace' => ltrim($this->indent($e->getTraceAsString(), 8), ' '),
            ]);
            return false;
        }
    }

    /**
     * Garbage collection
     * 
     * The garbage collector callback is invoked internally by PHP periodically
     * in order to purge old session data.
     * 
     * The frequency is controlled by `session.gc_probability` and `session.gc_divisor`.
     * 
     * The value of lifetime which is passed to this callback can be set
     * in `session.gc_maxlifetime`.
     * 
     * Return value should be `true` for success, `false` for failure.
     * 
     * @param int $max_lifetime
     * @return int|false
     * @see \SessionHandlerInterface
     * @link https://www.php.net/manual/en/function.session-set-save-handler.php
     */
    public function gc(int $max_lifetime)
    {
        try {
            $this->getLogger()->debug('[type][scope][class] Running session garbage collection for '
                . 'stale sessions of type [session-type] with name [session-name] with max-lifetime [session-max-lifetime] seconds.', [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
                'session-type' => $this->getSessionPath(),
                'session-name' => $this->getSessionName(),
                'session-max-lifetime' => $max_lifetime,
            ]);
            $base = $this->getSessionPath();
            foreach (glob(sprintf('%1$s%2$s*%3$s', $base, static::SESSION_PREFIX, static::SESSION_SUFFIX)) as $file) {
                $this->checkSessionFileExpired($base, $file, $max_lifetime);
            }
            return true;
        } catch (\Exception $e) {
            $this->getLogger()->warning(
                sprintf('[type][scope][class] Session garbage collection failure in path '
                    . '[session-path] with name [session-name] with max-lifetime [session-max-lifetime].'
                    . '%1$s%2$sAn exception was raised of type [exception-type] at line [exception-line] of file [exception-file]'
                    . '%1$s%2$sWith message: [exception-message]'
                    . '%1$s%2$sTrace: [exception-trace]'
                    , PHP_EOL, '    '
                ), [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
                'session-path' => $this->getSessionPath(),
                'session-name' => $this->getSessionName(),
                'session-max-lifetime' => $max_lifetime,
                'exception-type' => get_class($e),
                'exception-file' => $e->getFile(),
                'exception-line' => $e->getLine(),
                'exception-message' => $e->getMessage(),
                'exception-trace' => ltrim($this->indent($e->getTraceAsString(), 8), ' '),
            ]);
            return false;
        }
    }

    /**
     * Creates a session id
     * 
     * This callback is executed when a new session ID is required.
     * 
     * No parameters are provided, and the return value should be a string that
     * is a valid session ID for your handler.
     * 
     * @return string
     * @see \SessionIdInterface
     * @link https://www.php.net/manual/en/function.session-set-save-handler.php
     */
    public function create_sid(): string
    {
        try {
            return $this->generateSessionId();
        } catch (\Exception $e) {
            return '';
        }
    }

    /**
     * 
     * This callback is executed when a session is updated.
     * `id` is the session ID, `data` is the session data.
     * 
     * The return value should be `true` for success, `false` for failure.
     * 
     * @param string $name
     * @param string $data
     * @return bool
     * @see \SessionUpdateTimestampHandlerInterface
     * @link https://www.php.net/manual/en/function.session-set-save-handler.php
     */
    public function updateTimestamp(string $id, string $data): bool
    {
        try {
            $this->getLogger()->debug('[type][scope][class] Updating timestamp '
                . 'for session of type [session-type] with name [session-name] '
                . 'and session id [session-id].', [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
                'session-type' => $this->getSessionPath(),
                'session-name' => $this->getSessionName(),
                'session-id' => $id,
            ]);
            $this->setSessionId($id);
            $this->updateSessionTimestamp();
            return true;
        } catch (\Exception $e) {
            $this->getLogger()->warning(
                sprintf('[type][scope][class] Unable to update timestamp for session in path '
                    . '[session-path] with name [session-name] and session id [session-id].'
                    . '%1$s%2$sAn exception was raised of type [exception-type] at line [exception-line] of file [exception-file]'
                    . '%1$s%2$sWith message: [exception-message]'
                    . '%1$s%2$sTrace: [exception-trace]'
                    , PHP_EOL, '    '
                ), [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
                'session-path' => $this->getSessionPath(),
                'session-name' => $this->getSessionName(),
                'session-id' => $id,
                'exception-type' => get_class($e),
                'exception-file' => $e->getFile(),
                'exception-line' => $e->getLine(),
                'exception-message' => $e->getMessage(),
                'exception-trace' => ltrim($this->indent($e->getTraceAsString(), 8), ' '),
            ]);
            return false;
        }
    }

    /**
     * Session ID validation
     * 
     * This callback is executed when a session is to be started, a session ID
     * is supplied and `session.use_strict_mode` is enabled.
     * 
     * The key is the session ID to validate. A session ID is valid, if a session
     * with that ID already exists.
     * 
     * The return value should be `true` for success, `false` for failure.
     * 
     * @param string $id
     * @return bool
     * @see \SessionUpdateTimestampHandlerInterface
     * @link https://www.php.net/manual/en/function.session-set-save-handler.php
     */
    public function validateId(string $id): bool
    {
        try {
            $this->getLogger()->debug('[type][scope][class] Validating id for '
                . 'session of type [session-type] with name [session-name] '
                . 'and session id [session-id].', [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
                'session-type' => $this->getSessionPath(),
                'session-name' => $this->getSessionName(),
                'session-id' => $id,
            ]);
            $base = $this->getSessionPath();
            $file = sprintf('%1$s%2$s%3$s', static::SESSION_PREFIX, $id, static::SESSION_SUFFIX);
            return is_readable($base . $file);
        } catch (\Exception $e) {
            $this->getLogger()->warning(
                sprintf('[type][scope][class] Failed to validate session in path '
                    . '[session-path] with name [session-name] and session id [session-id].'
                    . '%1$s%2$sAn exception was raised of type [exception-type] at line [exception-line] of file [exception-file]'
                    . '%1$s%2$sWith message: [exception-message]'
                    . '%1$s%2$sTrace: [exception-trace]'
                    , PHP_EOL, '    '
                ), [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
                'session-path' => $this->getSessionPath(),
                'session-name' => $this->getSessionName(),
                'session-id' => $id,
                'exception-type' => get_class($e),
                'exception-file' => $e->getFile(),
                'exception-line' => $e->getLine(),
                'exception-message' => $e->getMessage(),
                'exception-trace' => ltrim($this->indent($e->getTraceAsString(), 8), ' '),
            ]);
            return false;
        }
    }

    /**
     * Performs the internal session data save action
     * This will be deferred to by all write operations
     * 
     * @param string $id
     * @return void
     */
    protected function saveSession(string $id, string $data): void
    {
        $path = $this->getSessionPath();
        $file = $this->resolveSessionIdentifier($id);
        $this->getSessionFileWriter()->setDirectory($path);
        $this->getSessionFileWriter()->setFile($file);
        $fullpath_file = $this->getSessionFileWriter()->getFullPath();
        if (!$this->getSessionFileWriter()->isWriteable()) {
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. Cache path [%2$s] '
                        . 'is not writeable. Cannot write session session '
                        . 'file [%3$s].', get_class($this),
                        $this->getSessionFileWriter()->getDirectory(),
                        $this->getSessionFileWriter()->getFullPath())
            );
        }
        $session = unserialize(file_get_contents($fullpath_file));
        if (is_array($session)) {
            $this->generateSessionContainer($session);
            $data = serialize($this->getSessionContainer());
        }
        $this->getSessionFileWriter()->write($data);
        clearstatcache(true, $fullpath_file);
        $this->updateSessionTimestamp();
    }

    /**
     * Retrieves the stored session data
     * This will be deferred to by all session load operations
     * 
     * The default behavior looks for a flatfile in the standard session location
     * in accordance with default php session handling behavior
     * 
     * @note if path is an empty string, use `sys_get_temp_dir()` to retrieve the correct path
     * @param string $path
     * @param string $name
     * @return string
     * @throws \Oroboros\core\exception\ErrorException
     *         If the provided path and name cannot resolve
     *         to a valid session storage entry
     */
    protected function retrieveSession(string $id): string
    {
        $this->getLogger()->debug('[type][scope][class] Retrieving session '
            . '[session-id] of session type: [session-type][session-scope].', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'session-type' => $this->getSessionType(),
            'session-scope' => $this->getSessionScope(),
            'session-id' => $id,
        ]);
        $path = $this->getSessionPath();
        $file = $this->resolveSessionIdentifier($id);
        $this->getSessionFileWriter()->setDirectory($path);
        $this->getSessionFileWriter()->setFile($file);
        $fullpath_file = $this->getSessionFileWriter()->getFullPath();
        clearstatcache(true, $fullpath_file);
        if (!is_readable($fullpath_file)) {
            $this->getLogger()->error('[type][scope][class] Session not found '
                . 'with id [session-id] of session type: [session-type][session-scope] '
                . 'at expected path [session-file].', [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
                'session-type' => $this->getSessionType(),
                'session-scope' => $this->getSessionScope(),
                'session-id' => $id,
                'session-file' => $fullpath_file,
            ]);
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. Expected session storage '
                        . 'file at [%2$s] does not exist or is not readable.', get_class($this), $fullpath_file)
            );
        }
        $session = unserialize(file_get_contents($fullpath_file));
        if (is_array($session)) {
            $this->generateSessionContainer($session);
            return serialize($this->getSessionContainer());
        }
        return serialize($session);
    }

    /**
     * Deletes the internal session data for the given session id
     * This will be deferred to for all session deletion actions
     * 
     * @param string $name
     * @return void
     */
    protected function deleteSession($id): void
    {
        $this->getLogger()->debug('[type][scope][class] Deleting session '
            . '[session-id] of session type: [session-type][session-scope].', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'session-type' => $this->getSessionType(),
            'session-scope' => $this->getSessionScope(),
            'session-id' => $id,
        ]);
        $path = $this->getSessionPath();
        $file = $this->resolveSessionIdentifier($id);
        $this->getSessionFileWriter()->setDirectory($path);
        $this->getSessionFileWriter()->setFile($file);
        $fullpath_file = $this->getSessionFileWriter()->getFullPath();
        unlink($fullpath_file);
        clearstatcache(true, $fullpath_file);
    }

    /**
     * Creates the actual session stored data
     * This will be deferred to for all session creation actions
     * 
     * @param string $id
     * @return void
     */
    protected function createSession(string $id): void
    {
        $path = $this->getSessionPath();
        $file = $this->resolveSessionIdentifier($id);
        $this->getSessionFileWriter()->setDirectory($path);
        $this->getSessionFileWriter()->setFile($file);
        $fullpath_file = $this->getSessionFileWriter()->getFullPath();
        $this->generateSessionContainer();
        $data = serialize($this->getSessionContainer());
        $this->getSessionFileWriter()->write($data);
        clearstatcache(true, $fullpath_file);
    }

    /**
     * Generates a valid session id string
     * 
     * @return string
     */
    protected function generateSessionId(): string
    {
        $token = $this->load('library', static::SESSION_TOKEN_CLASS);
        return $token->get();
    }

    /**
     * Returns the string name of the cookie that the session will be stored in,
     * or the name of the session save file in the event of cli.
     * 
     * The default `PHPSESSID` is not used so it does not collide with other
     * application scopes for the same domain, in the event Oroboros is used
     * as a bridge platform.
     * 
     * Default format is "session.sitedomain"
     * 
     * @return string
     */
    protected function generateSessionName(): string
    {
        return trim(sprintf('%1$s.%2$s', 'session', $this::app()->environment()->application->domain), '.');
    }

    /**
     * Resolves the session identifier name into an expected string
     * This will apply the declared prefix and suffix declared by
     * the session handler class to the raw id
     * 
     * @param string $id
     * @return string
     */
    protected function resolveSessionIdentifier(string $id)
    {
        $id = sprintf('%1$s%2$s%3$s', static::SESSION_PREFIX, $id, static::SESSION_SUFFIX);
        return $id;
    }

    /**
     * Internal getter for the session container
     * 
     * @return \Oroboros\core\interfaces\library\session\SessionContainerInterface
     */
    protected function getSessionContainer(): ?\Oroboros\core\interfaces\library\session\SessionContainerInterface
    {
        return self::$session_values;
    }

    /**
     * Sets the session container passed in from php internals serialized string
     * 
     * @param array $data An unserialized session container
     * 
     * @return void
     */
    protected function setSessionContainer(\Oroboros\core\interfaces\library\session\SessionContainerInterface $session): void
    {
        self::$session_values = $session;
    }

    /**
     * Generates a new session container.
     * If a previous one exists, it will be overwritten.
     * 
     * @param array $data Any data to create upon
     * session generation of a new container.
     * 
     * @return void
     */
    protected function generateSessionContainer(array $data = []): void
    {
        self::$session_values = $this->containerize($data);
    }

    /**
     * Destroys the session container and generates a fresh one.
     * 
     * @return void
     */
    protected function resetSessionContainer(): void
    {
        $this->destroySessionContainer();
        $this->generateSessionContainer();
    }

    /**
     * Destroys the existing session container, leaving it in a null state
     * 
     * @return void
     */
    protected function destroySessionContainer(): void
    {
        self::$session_values = null;
    }

    /**
     * Returns the current update timestamp
     * @return int
     */
    protected function getSessionTimestamp(): int
    {
        return self::$update_timestamp;
    }

    /**
     * Updates the timestamp to a microtime reflection of right now.
     * 
     * @return void
     */
    protected function updateSessionTimestamp(): void
    {
        self::$update_timestamp = microtime();
    }

    /**
     * Declares the session save path.
     * 
     * By default, this just returns whatever the php.ini setting is.
     * 
     * You may override this method to declare an
     * alternate save path location.
     * 
     * @note an empty string indicates that the system tmp directory is used
     * @return string
     */
    protected function declareSessionSavePath(): string
    {
        return ini_get('session.save_path');
    }

    /**
     * Performs any session initialization tasks required
     * for a usable session handler.
     * 
     * You may override this method if you have additional
     * session initialization logic to perform.
     * 
     * @return void
     * @throws \Oroboros\core\exception\ErrorException
     *         If the given session handler object cannot be set
     */
    protected function initializeSessionHandler(): void
    {
        $this->getLogger()->debug('[type][scope][class] Initializing session '
            . 'handler for session type: [session-type][session-scope].', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'session-type' => $this->getSessionType(),
            'session-scope' => $this->getSessionScope(),
        ]);
        $this->initializeSessionFileWriter();
        $this->initializeSessionSerialization();
        $this->initializeSessionContainer();
    }

    /**
     * Verifies that the session id exists, is readable, and is writable
     * 
     * Name should equal either null or the same
     * value returned by `$this->generateSessionId()`
     * 
     * If these are not the case, it is out of scope
     * for this session handler.
     * 
     * @param string $id
     * @return void
     * @throws \Oroboros\core\exception\session\NotFoundException
     *         If the session does not exist
     * @throws \Oroboros\core\exception\session\ReadErrorException
     *         If the session exists but is not readable.
     *         This indicates file system permission errors
     * @throws \Oroboros\core\exception\session\WriteErrorException
     *         If the session exists but is not writable.
     *         This indicates file system permission errors.
     */
    protected function verifySessionId(string $id): void
    {
        $path = $this->getSessionPath();
        $file = $this->resolveSessionIdentifier($id);
        $this->getSessionFileWriter()->setDirectory($path);
        $this->getSessionFileWriter()->setFile($file);
        $fullpath_file = $this->getSessionFileWriter()->getFullPath();
        clearstatcache(true, $fullpath_file);
        $this->getLogger()->debug('[type][scope][class] Looking up session id '
            . '[session-id] for session type [session-type] with name '
            . '[session-name] and expected path [session-fullpath].', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'session-type' => $this->getSessionPath(),
            'session-name' => $this->getSessionName(),
            'session-id' => $id,
            'session-fullpath' => $fullpath_file,
        ]);
        if (!file_exists($fullpath_file)) {
            $dir = scandir(dirname($fullpath_file));
//            d($dir, $file, in_array($file, $dir), $id, $this->getArgument('request')->getCookieParams());
            $this->getLogger()->debug('[type][scope][class] Session id [session-id] '
                . 'not found for session type [session-type] with name [session-name] '
                . 'and expected path [session-fullpath].', [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
                'session-type' => $this->getSessionPath(),
                'session-name' => $this->getSessionName(),
                'session-id' => $id,
                'session-fullpath' => $fullpath_file,
            ]);
            throw new \Oroboros\core\exception\session\NotFoundException(
                    sprintf('Error encountered in [%1$s]. Provided session '
                        . 'id [%2$s] does not exist at expected file location [%3$s].', get_class($this), $id, $fullpath_file)
            );
        }
        if (!is_readable($fullpath_file)) {
            // Session folder is not writable
            throw new \Oroboros\core\exception\session\ReadErrorException(
                    sprintf('Error encountered in [%1$s]. Provided session '
                        . 'id [%2$s] with file [%3$s] exists but is not '
                        . 'readable in the current scope.', get_class($this), $id, $fullpath_file)
            );
        }
        if (!is_writable($fullpath_file)) {
            // Session folder is not writable
            throw new \Oroboros\core\exception\session\WriteErrorException(
                    sprintf('Error encountered in [%1$s]. Provided session '
                        . 'id [%2$s] with file [%3$s] exists but is not '
                        . 'writable in the current scope.', get_class($this), $id, $fullpath_file)
            );
        }
    }

    /**
     * Verifies that the session name is expected
     * 
     * Name should equal either null or the same
     * value returned by `$this->generateSessionName()`
     * 
     * If these are not the case, it is out of scope
     * for this session handler.
     * 
     * @param string|null $name
     * @return void
     * @throws \Oroboros\core\exception\auth\ValidationException
     */
    protected function verifySessionName(string $name = null): void
    {
        if (is_null($name)) {
            // Not set yet or not active
            return;
        }
        $expected = $this->generateSessionName();
        if ($name !== $expected) {
            throw new \Oroboros\core\exception\auth\ValidationException(
                    sprintf('Error encountered in [%1$s]. Provided session '
                        . 'name [%2$s] does not match expected session '
                        . 'name [%3$s].', get_class($this), $name, $expected)
            );
        }
    }

    /**
     * Verifies that the session path accurately reflects the expected path.
     * 
     * @param string $path
     * @return void
     * @throws \Oroboros\core\exception\auth\ValidationException
     */
    protected function verifySessionPath(string $path): void
    {
        $expected = self::$path;
        if (is_null($expected)) {
            return;
        }
        if ($this->resolveSessionPath($path) !== $expected) {
            throw new \Oroboros\core\exception\auth\ValidationException(
                    sprintf('Error encountered in [%1$s]. Provided session '
                        . 'path [%2$s] does not match expected session '
                        . 'path [%3$s].', get_class($this), $this->resolveSessionPath($path), $expected)
            );
        }
    }

    /**
     * Sets the internal session path,
     * used to determine where session
     * storage is located.
     * 
     * @param string $path
     * @return void
     */
    protected function setSessionPath(string $path): void
    {
        self::$path = $this->resolveSessionPath($path);
    }

    /**
     * Expands the path if it is the default empty string,
     * otherwise returns it as is
     * 
     * @param string $path
     * @return string
     */
    protected function resolveSessionPath(string $path): string
    {
        if ($path === '') {
            // Default behavior is to use the sys tmp dir if an empty string is passed
            $path = realpath(sys_get_temp_dir()) . DIRECTORY_SEPARATOR;
        }
        return $path;
    }

    /**
     * Returns the current session path, if one is defined.
     * If no path is in current scope, returns null.
     * 
     * folder of the oroboros temp folderin a directory with the application name
     * Default behavior is to enclose session data within the "session"
     * that they hold in scope
     * 
     * Eg: `/tmp/oroboros/app-name/session/sess_2348925sdf935n4353`
     * 
     * @return string|null
     */
    protected function getSessionPath(): ?string
    {
        return self::$path
            . 'oroboros' . DIRECTORY_SEPARATOR
            . $this::app()->getApplicationDefinition()->name . DIRECTORY_SEPARATOR
            . static::CLASS_SCOPE . DIRECTORY_SEPARATOR;
    }

    /**
     * Resets the internally scoped session path
     * 
     * @return void
     */
    protected function resetSessionPath(): void
    {
        self::$path = null;
    }

    /**
     * Sets the session name. This will correspond to the http cookie
     * for html/ajax requests, or an internal identifier to derive from
     * the environment or headers to identify it otherwise.
     * 
     * @param string $name
     * @return void
     */
    protected function setSessionName(string $name): void
    {
        self::$name = $name;
    }

    /**
     * Sets the session name. This will be used to name cookies,
     * header token entries, or some local environment variable
     * to determine the session id designator for the current scope.
     * 
     * @return string|null
     */
    protected function getSessionName(): ?string
    {
        return self::$name;
    }

    /**
     * Resets the internally scoped session name
     * 
     * @return void
     */
    protected function resetSessionName(): void
    {
        self::$name = null;
    }

    /**
     * Sets the session id. This will resolve to a unique hash of some sort.
     * 
     * @param string $id
     * @return void
     */
    protected function setSessionId(string $id): void
    {
        self::$id = $id;
    }

    /**
     * Returns the currently scoped session id,
     * or null if there is no session id currently in scope.
     * 
     * @return string|null
     */
    protected function getSessionId(): ?string
    {
        return self::$id;
    }

    /**
     * Clears the internally scoped session id
     * 
     * @return void
     */
    protected function resetSessionId(): void
    {
        self::$id = null;
    }

    /**
     * Internal getter for the session file writer object.
     * Used to allow overrides to fetch it and write directly
     * using their own criteria.
     * 
     * @return \Oroboros\core\library\file\FileWriter
     */
    protected function getSessionFileWriter(): \Oroboros\core\library\file\FileWriter
    {
        return self::$session_file_writer;
    }

    /**
     * Deletes session files older than the given time offset
     * and deletes them if they are expired.
     * 
     * @param string $path
     * @param string $file
     * @param int $timeout
     * @return void
     */
    protected function checkSessionFileExpired(string $path, string $file, int $timeout): void
    {
        $fullpath = $path . $file;
        if (!is_readable($fullpath)) {
            return;
        }
        $current = \DateTime::getTimestamp();
        $edited = filemtime($fullpath);
        $offset = $current - $edited;
        if ($offset > $timeout) {
            unlink($fullpath);
        }
    }

    /**
     * Initializes the session container if it is not already initialized.
     * 
     * @return void
     */
    private function initializeSessionContainer(): void
    {
        if (is_null(self::$session_values)) {
            $this->generateSessionContainer();
        }
    }

    /**
     * Loads the session file writer if it is not already loaded.
     * This is used by default functionality to write session data
     * to their flatfile.
     * 
     * This object will only be loaded one time
     * 
     * @return void
     */
    private function initializeSessionFileWriter(): void
    {
        if (is_null(self::$session_file_writer)) {
            $writer = $this->load('library', \Oroboros\core\library\file\FileWriter::class, null, [
                'write-mode' => 'w', // Truncate the file to zero length before each write
                'generate-directories' => true      // Generate directories if missing
            ]);
            self::$session_file_writer = $writer;
        }
    }

    /**
     * Initializes the serialization format for the session
     * 
     * @return void
     * @throws \Oroboros\core\exception\DomainException
     *         if the serialization engine cannot be set to
     *         the declared format handled by the object.
     */
    private function initializeSessionSerialization(): void
    {
        ini_set('session.serialize_handler', static::SERIALIZATION_ENGINE);
        if (ini_get('session.serialize_handler') === static::SERIALIZATION_ENGINE) {
            return;
        }
        // Nope no good.
        throw new \Oroboros\core\exception\DomainException(
                sprintf('Error encountered in [%1$s]. Unable to set '
                    . 'serialize handler to required format [%2$s]. '
                    . 'Please update your php.ini to this format '
                    . 'or allow `ini_set` at runtime to use this '
                    . 'functionality.'
                    , get_class($this), static::SERIALIZATION_ENGINE)
        );
    }
    
    /**
     * Sets variable options based on environment.
     * 
     * @return void
     */
    private function initializeSessionOptions(): void
    {
        if (PHP_VERSION_ID >= 70300) {
            // This directive is only avaliable on php 7.3 or later
            ini_set('session.cookie_samesite', 'Strict');
        }
    }

    /**
     * Verifies that the session handler has properly declared its type and scope
     * if they are declared. Both are optional.
     * 
     * @return void
     * @throws \Oroboros\core\exception\core\InvalidClassException
     *         If either of the class constants `SESSION_TYPE`,
     *         or `SESSION_SCOPE` are defined and use invalid values,
     *         or if `CONTAINER_CLASS` does not define a valid
     *         session container.
     */
    private function verifySessionHandler(): void
    {
        $this->verifySessionHandlerType();
        $this->verifySessionHandlerScope();
        $this->verifySessionContainerClass();
        $this->verifySessionTokenClass();
    }

    /**
     * Verifies that the session handler type definition is valid if declared
     * 
     * @return void
     * @throws \Oroboros\core\exception\core\InvalidClassException
     */
    private function verifySessionHandlerType(): void
    {
        if (is_null(static::SESSION_TYPE)) {
            // Null handler types are valid
            return;
        }
        if (!is_string(static::SESSION_TYPE)) {
            // Invalid type
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. Expected class constant '
                        . '[%2$s] must define a string value or null.'
                        , get_class($this), 'SESSION_TYPE')
            );
        }
    }

    /**
     * Verifies that the session handler scope definition is valid if declared
     * 
     * @return void
     * @throws \Oroboros\core\exception\core\InvalidClassException
     */
    private function verifySessionHandlerScope(): void
    {
        if (is_null(static::SESSION_SCOPE)) {
            // Null handler scopes are valid
            return;
        }
        if (!is_string(static::SESSION_SCOPE)) {
            // Invalid scope
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. Expected class constant '
                        . '[%2$s] must define a string value or null.'
                        , get_class($this), 'SESSION_SCOPE')
            );
        }
    }

    /**
     * Verifies that the session handler class is valid
     * 
     * @return void
     * @throws \Oroboros\core\exception\core\InvalidClassException
     */
    private function verifySessionContainerClass(): void
    {
        $expected = \Oroboros\core\interfaces\library\session\SessionContainerInterface::class;
        $class = $this->getFullClassName('library', static::CONTAINER_CLASS);
        if ($class === false || (!in_array($expected, class_implements($class)))) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error Encountered in [%1$s]. Expected class constant '
                        . '[%2$s] with value [%3$s] must resolve to a class or stub '
                        . 'class name implementing expected interface [%4$s]. '
                        . 'This class is not useable in it\'s current state.',
                        get_class($this), 'CONTAINER_CLASS', static::CONTAINER_CLASS, $expected),
                    0, E_ERROR, __FILE__, __LINE__
            );
        }
    }

    /**
     * Verifies that the session token class is valid
     * 
     * @return void
     * @throws \Oroboros\core\exception\core\InvalidClassException
     */
    private function verifySessionTokenClass(): void
    {
        $expected = \Oroboros\core\interfaces\library\auth\TokenInterface::class;
        $class = $this->getFullClassName('library', static::SESSION_TOKEN_CLASS);
        if ($class === false || (!in_array($expected, class_implements($class)))) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error Encountered in [%1$s]. Expected class constant '
                        . '[%2$s] with value [%3$s] must resolve to a class or stub '
                        . 'class name implementing expected interface [%4$s]. '
                        . 'This class is not useable in it\'s current state.',
                        get_class($this), 'SESSION_TOKEN_CLASS', static::SESSION_TOKEN_CLASS, $expected),
                    0, E_ERROR, __FILE__, __LINE__
            );
        }
    }
}
