<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\session;

/**
 * Abstract HTTP Session
 * Provides abstraction for emulating a session for http requests.
 *
 * @author Brian Dayhoff
 */
abstract class AbstractHttpSession extends AbstractSession implements \Oroboros\core\interfaces\library\session\HttpSessionInterface
{

    /**
     * Defines the session type as an http session
     */
    const SESSION_TYPE = 'http';

    /**
     * Use the http session container
     */
    const CONTAINER_CLASS = \Oroboros\core\library\session\http\HttpSessionContainer::class;

    /**
     * Uses the standard http session container by default.
     */
    const SESSION_HANDLER_CLASS = \Oroboros\core\library\session\http\HttpSessionHandler::class;

    /**
     * Defines the flag used to persist arguments passed into the object into the session.
     */
    const FLAG_PERSIST_ARGUMENTS = 'persist-arguments';

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
    }
}
