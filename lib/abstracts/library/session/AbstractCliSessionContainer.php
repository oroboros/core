<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\session;

/**
 * Abstract Cli Session Container
 * Provides abstraction for session containers.
 * Session containers interact directly with the current session.
 * 
 * This class does not start the session, it only operates on values within it
 * and makes no decisions about whether or not it is appropriate for it to be
 * currently enabled or not.
 * 
 * All methods in this container class will throw a \Psr\Container\ContainerExceptionInterface
 * if the session is not currently active.
 * 
 * @author Brian Dayhoff
 */
abstract class AbstractCliSessionContainer extends AbstractSessionContainer implements \Oroboros\core\interfaces\library\session\CliSessionContainerInterface
{

    /**
     * Defines the session type as a cli session
     */
    const SESSION_TYPE = 'cli';
    
    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeCliSession();
    }

    /**
     * Initializes the cli session
     * 
     * @return void
     */
    private function initializeCliSession(): void
    {
        
    }
}
