<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\session;

/**
 * Abstract CLI Session Handler
 * Provides abstraction for emulating a session for command line usage
 * using the normal php internal mechanics.
 *
 * @author Brian Dayhoff
 */
abstract class AbstractCliSessionHandler extends AbstractSessionHandler implements \Oroboros\core\interfaces\library\session\CliSessionHandlerInterface
{

    /**
     * Defines the session type as a cli session
     */
    const SESSION_TYPE = 'cli';

    /**
     * Use the cli session container
     */
    const CONTAINER_CLASS = \Oroboros\core\library\session\cli\CliSessionContainer::class;

    /**
     * CLI Session storage directory, relative to the temp storage folder.
     * 
     * This may be overridden to point to an alternate directory.
     */
    const CLI_SESSION_STORAGE_PATH = 'session/cli';

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
    }
    
    /**
     * Sets up the cli session handler.
     * 
     * @return void
     */
    protected function initializeSessionHandler(): void
    {
//        parent::initializeSessionHandler();
    }
}
