<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\session;

/**
 * Abstract Http Session Handler
 * Provides abstraction for handling php session call internals from http requests.
 * 
 * Default behavior uses the strictest possible secure
 * session cookie settings that are not cumbersome to work around,
 * and defers to php.ini defaults for all other settings.
 *
 * @author Brian Dayhoff
 */
abstract class AbstractHttpSessionHandler extends AbstractSessionHandler implements \Oroboros\core\interfaces\library\session\CliSessionHandlerInterface
{

    /**
     * Defines the session type as an http session
     */
    const SESSION_TYPE = 'http';

    /**
     * Use the http session container
     */
    const CONTAINER_CLASS = \Oroboros\core\library\session\http\HttpSessionContainer::class;

    /**
     * Defines the default timeout for sessions.
     * If the php.ini value is equal to or greater than this, it will be used.
     * If this value is greater, this value will be used.
     * 
     * The default timeout is one month.
     */
    const SESSION_DEFAULT_TIMEOUT = 2592000;
    
    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
    }

    /**
     * Sets the http cookie name to reflect the declared session name.
     * 
     * @return void
     */
    protected function initializeSessionHandler(): void
    {
        parent::initializeSessionHandler();
        $name = $this->generateSessionName();
        $existing = session_name();
        if ($name !== $existing) {
            session_name($name);
        }
        // Enable http cookies explicitly
        ini_set('session.use_cookies', true);
        // Prevent session url attacks
        ini_set('session.use_only_cookies', true);
        // invalidate unknown session cookies
        ini_set('session.use_strict_mode', true);
        // block session cookies not from the current domain
        ini_set('session.referer_check', $this->declareSessionReferer());
        // set cookie parameters
        if (PHP_VERSION_ID < 70300) {
            session_set_cookie_params(
                $this->declareSessionLifespan(),
                $this->declareSessionPath() . '; samesite=' . $this->declareSessionSameSite(),
                $this->declareSessionDomain(),
                $this->declareSessionSecure(),
                $this->declareSessionHttpOnly()
            );
        } else {
            session_set_cookie_params([
                'lifetime' => $this->declareSessionLifespan(),
                'path' => $this->declareSessionPath(),
                'domain' => $this->declareSessionDomain(),
                'secure' => $this->declareSessionSecure(),
                'httponly' => $this->declareSessionHttpOnly(),
                'samesite' => $this->declareSessionSameSite()
            ]);
        }
    }

    /**
     * Declares the referrer check substring for session cookies.
     * By default, this will return the domain name of the site from
     * the local environment, which all subdomains should also carry.
     * 
     * This helps prevent session fixation attacks.
     * 
     * If you wish to disable this, override this method
     * and return an empty string.
     * 
     * @return string
     */
    protected function declareSessionReferer(): string
    {
        return $this::app()->environment()->application->domain;
    }

    /**
     * Declares the lifespan for the session cookie.
     * By default, returns the larger value of either the existing
     * php.ini setting `session.cookie_lifetime`
     * or the value of `static::SESSION_DEFAULT_TIMEOUT`
     * 
     * You may override this method to return an alternate
     * time span for session timeout.
     * 
     * @return int
     */
    protected function declareSessionLifespan(): int
    {
        $server =  intval(ini_get('session.cookie_lifetime'));
        $declared = static::SESSION_DEFAULT_TIMEOUT;
        if ($server >= $declared) {
            return $server;
        }
        return $declared;
    }

    /**
     * Declares the path for the domain in which the cookie will work.
     * By default, applies to all paths.
     * 
     * @return string
     */
    protected function declareSessionPath(): string
    {
        return '/';
    }

    /**
     * Declares the http session cookie domain availability.
     * 
     * By default, this will return a subdomain-compatible declaration
     * for the current domain name based on your environment.
     * 
     * You may override this method to constrain to only the current domain (just trim the left dot),
     * or to modify the domain altogether.
     * 
     * @return string
     */
    protected function declareSessionDomain(): string
    {
        return sprintf('.%1$s', trim($this::app()->environment()->application->domain, '.'));
    }

    /**
     * Declares whether the http session cookie should be served over SSL only.
     * By default, this will return `true` if SSL is active for your site,
     * and `false` if it is not.
     * 
     * The default functionality will use the highest security available
     * for your current environment. You may override this to always
     * return `true` or `false` if neccessary.
     * 
     * @return bool
     */
    protected function declareSessionSecure(): bool
    {
        if ($this::IS_SSL) {
            return true;
        }
        return false;
    }

    /**
     * Declares whether the http session cookie should be http-only.
     * By default this method always returns `true`.
     * 
     * For security purposes, you should not override this unless you have
     * a very good reason to do so, as you may introduce
     * XSS attack vulnerability or CSRF issues.
     * 
     * @return bool
     */
    protected function declareSessionHttpOnly(): bool
    {
        return true;
    }

    /**
     * Declares whether the http session cookie should be constrained
     * only to the same site.
     * 
     * By default, this returns the value in your php.ini.
     * 
     * Valid values are 'Lax' or 'Strict'
     * 
     * You may override this method if you require an explicit value.
     * 
     * @return bool
     */
    protected function declareSessionSameSite(): bool
    {
        return 'Strict';
        return ini_get('session.cookie_samesite');
    }
}
