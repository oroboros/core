<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\session;

/**
 * Abstract Session Container
 * Provides abstraction for session containers.
 * Session containers interact directly with the current session.
 * 
 * This class does not start the session, it only operates on values within it
 * and makes no decisions about whether or not it is appropriate for it to be
 * currently enabled or not.
 * 
 * All methods in this container class will throw a \Psr\Container\ContainerExceptionInterface
 * if the session is not currently active.
 * 
 * @author Brian Dayhoff
 */
abstract class AbstractSessionContainer extends \Oroboros\core\abstracts\library\container\AbstractContainer implements \Oroboros\core\interfaces\library\session\SessionContainerInterface
{

    use \Oroboros\core\traits\ContainerPackagerUtility;
    use \Oroboros\core\traits\library\session\SessionCommonTrait;

    /**
     * Session type must be provided by abstraction
     * This must correspond to a runtime scope (eg cli, http, html, ajax, rest, etc)
     */
    const SESSION_TYPE = null;

    /**
     * Session scope may be provided by abstraction
     */
    const SESSION_SCOPE = null;
    
    /**
     * Adds the value to the literal session storage
     * 
     * Raises a `\Oroboros\core\exception\container\ContainerException`
     * if the session is not active
     * 
     * @param type $key
     * @param type $value
     * @param type $method
     * @throws \Oroboros\core\exception\container\ContainerException
     */
    protected function onSet($key, $value, $method = null)
    {
//        if ($this->status() === 'disabled') {
//            throw new \Oroboros\core\exception\container\ContainerException(sprintf('Error encountered in [%1$s]. '
//                        . 'Values cannot be set because the session is not currently active.', get_class($this)));
//        }
        parent::onSet($key, $value, $method);
    }

    /**
     * Container Setter Value Filter Method
     * This method triggers whenever a set operation occurs, providing a means
     * of mutating the value if need be, or performing other operations with it.
     *
     * The value actually used in the set operation will be the return result of this method.
     * This method fires after the key filter, and will receive the filtered key,
     * if it was changed.
     *
     * @param scalar|stringable $key The passed key that the setter is supposed to have
     * @param mixed $value The value passed for the setter
     * @param string $method The internal method that the operation arose from.
     * @return mixed
     */
    protected function filterSetValue($key, $value, $method = null)
    {
        return parent::filterSetValue($key, $value, $method);
    }

    /**
     * Adds the value to the literal session storage
     * 
     * Raises a `\Oroboros\core\exception\container\ContainerException`
     * if the session is not active
     * 
     * @param type $key
     * @param type $method
     * @throws \Oroboros\core\exception\container\ContainerException
     */
    protected function onGet($key, $method = null)
    {
//        if ($this->status() === 'disabled') {
//            throw new \Oroboros\core\exception\container\ContainerException(sprintf('Error encountered in [%1$s]. '
//                        . 'Values cannot be retrieved because the session is not currently active.', get_class($this)));
//        }
        parent::onGet($key, $method);
    }

    /**
     * Fetches the value from the literal session storage
     * 
     * Raises a `\Oroboros\core\exception\container\ContainerException`
     * if the session is not active
     * 
     * @param type $key
     * @param type $value
     * @param type $method
     * @return type
     * @throws \Oroboros\core\exception\container\ContainerException
     */
    protected function filterGet($key, $value, $method = null)
    {
//        if ($this->status() === 'disabled') {
//            throw new \Oroboros\core\exception\container\ContainerException(sprintf('Error encountered in [%1$s]. '
//                        . 'Values cannot be fetched because the session is not currently active.', get_class($this)));
//        }
        return parent::filterGet($key, $value, $method);
    }

    /**
     * Unsets the value from the literal session storage
     * 
     * Raises a `\Oroboros\core\exception\container\ContainerException`
     * if the session is not active
     * 
     * @param type $key
     * @param type $method
     * @throws \Oroboros\core\exception\container\ContainerException
     */
    protected function onUnset($key, $method = null)
    {
//        if ($this->status() === 'disabled') {
//            throw new \Oroboros\core\exception\container\ContainerException(sprintf('Error encountered in [%1$s]. '
//                        . 'Values cannot be set because the session is not currently active.', get_class($this)));
//        }
        parent::onUnset($key, $method);
    }

    /**
     * Synchronizes the set value with the literal session storage
     * 
     * Raises a `\Oroboros\core\exception\container\ContainerException`
     * if the session is not active
     * 
     * @param type $key
     * @param type $method
     * @throws \Oroboros\core\exception\container\ContainerException
     */
    protected function onExists($key, $method = null)
    {
//        if ($this->status() === 'disabled') {
//            throw new \Oroboros\core\exception\container\ContainerException(sprintf('Error encountered in [%1$s]. '
//                        . 'Values cannot be checked because the session is not currently active.', get_class($this)));
//        }
        parent::onExists($key, $method);
    }

    /**
     * Raises a `\Oroboros\core\exception\container\ContainerException`
     * if the session is not active
     * 
     * @param type $count
     * @param type $mode
     * @param type $method
     * @return type
     * @throws \Oroboros\core\exception\container\ContainerException
     */
    protected function filterCount($count, $mode, $method = null)
    {
//        if ($this->status() === 'disabled') {
//            throw new \Oroboros\core\exception\container\ContainerException(sprintf('Error encountered in [%1$s]. '
//                        . 'Count cannot be taken because the session is not currently active.', get_class($this)));
//        }
        return parent::filterCount($count, $mode, $method);
    }

    /**
     * Returns a boolean designation as to whether the key exists
     * in the literal session
     * 
     * @param type $key
     * @param type $designation
     * @param type $method
     * @return bool
     */
    protected function filterExists($key, $designation, $method = null)
    {
//        if ($this->status() === 'disabled') {
//            throw new \Oroboros\core\exception\container\ContainerException(sprintf('Error encountered in [%1$s]. '
//                        . 'Count cannot be checked because the session is not currently active.', get_class($this)));
//        }
        return parent::filterExists($key, $designation, $method);
    }

    /**
     * internal master session.
     * All other sessions will sync to this session if they are not detached
     * 
     * @return void
     */
    private function initializeSessionContainer(): void
    {
        
    }
}
