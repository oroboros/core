<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\session;

/**
 * Abstract Session
 * Provides abstraction for interacting with the PHP session in
 * an extensible and standards compliant manner.
 *
 * @author Brian Dayhoff
 */
abstract class AbstractSession extends \Oroboros\core\abstracts\library\AbstractLibrary implements \Oroboros\core\interfaces\library\session\SessionInterface
{

    use \Oroboros\core\traits\library\session\SessionCommonTrait;

    /**
     * Defines the class scope as session
     */
    const CLASS_SCOPE = 'session';

    /**
     * Session type must be provided by abstraction
     * This must correspond to a runtime scope (eg cli, http, html, ajax, rest, etc)
     */
    const SESSION_TYPE = null;

    /**
     * Session scope may be provided by abstraction
     */
    const SESSION_SCOPE = null;

    /**
     * Use the session container
     */
    const CONTAINER_CLASS = \Oroboros\core\library\session\SessionContainer::class;

    /**
     * Represents the session handler class used in the current scope.
     * 
     * The default definition is a null session handler that does nothing.
     */
    const SESSION_HANDLER_CLASS = \Oroboros\core\library\session\NullSessionHandler::class;

    /**
     * This value may be overridden to alter the keyname
     * of the session auth token identifier.
     * 
     * This is useful if multiple tokens need to be stored
     * in the session based on various object scopes of
     * session objects.
     */
    const SESSION_AUTH_TOKEN_IDENTIFIER = 'session-auth-token';

    /**
     * Use the default token class
     * This may be overridden to specify an alternate session token class.
     */
    const SESSION_AUTH_TOKEN_CLASS = \Oroboros\core\library\auth\Token::class;

    /**
     * Defines the flag used to persist arguments passed into the object into the session.
     */
    const FLAG_PERSIST_ARGUMENTS = 'persist-arguments';

    /**
     * Resolves to the current session id, or null if no session is active
     * 
     * @var string|null
     */
    private static $session_id = null;

    /**
     * The runtime session handler loaded when the session was instantiated.
     * 
     * @var \Oroboros\core\interfaces\library\session\SessionHandlerInterface
     */
    private static $session_handler = null;

    /**
     * The session token for input authentication
     * 
     * This will be created if it does not exist when the session is loaded,
     * and will be rotated each time the session changes.
     * 
     * @var \Oroboros\core\interfaces\library\auth\TokenInterface
     */
    private static $token = null;

    /**
     * Represents whether the current session is loaded
     * @var bool
     */
    private static $session_loaded = false;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        $this->verifySessionCommon();
        $this->verifySessionHandlerClass();
        parent::__construct($command, $arguments, $flags);
        $this->initializeSessionCommon();
        $this->initializeSession();
        if ($this->hasFlag(self::FLAG_PERSIST_ARGUMENTS) && $this->getFlag(self::FLAG_PERSIST_ARGUMENTS)) {
            $this->persistValues();
        }
    }

    /**
     * Scopes the session to the given PSR request
     * 
     * If a valid session identifier exists within the given request,
     * the session will be opened and the existing session,
     * if any, will be closed.
     * 
     * If there is no valid session within the given request,
     * any current sessions will be closed and a null session will be opened.
     * 
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @return void
     */
    public function scope(\Psr\Http\Message\ServerRequestInterface $request): void
    {
        $identifier_key = str_replace('.', '_', sprintf('%1$s.%2$s', static::CLASS_SCOPE, $this::app()->environment()->application->domain));
        if (array_key_exists($identifier_key, $request->getCookieParams())) {
            $identifier = $request->getCookieParams()[$identifier_key];
            if (self::$session_loaded === false || session_id() === false) {
                session_id($identifier);
                self::$session_id = $identifier;
            }
        }
    }

    /**
     * Getter for the session handler object.
     * 
     * Only one session handler object can be defined per runtime.
     * 
     * @return \Oroboros\core\interfaces\library\session\SessionHandlerInterface
     */
    public function getHandler(): \Oroboros\core\interfaces\library\session\SessionHandlerInterface
    {
        return self::$session_handler;
    }

    /**
     * Starts the session if it is not already started.
     * This method may be safely called on an already-active session.
     * @return $this (method chainable)
     */
    public function start(): \Oroboros\core\interfaces\library\session\SessionInterface
    {
        $this->startSession();
        return $this;
    }

    /**
     * Destroys the current session.
     * @return $this (method chainable)
     */
    public function destroy(): \Oroboros\core\interfaces\library\session\SessionInterface
    {
        $this->destroySession();
        return $this;
    }

    /**
     * Synchronizes the current session container to
     * the currently defined session keys.
     * 
     * This method can be used to synchronize the current session object
     * in the event that external changes directly to the superglobal occur.
     * 
     * The session will be started if it is not currently active to enable this operation.
     * 
     * @return $this (method chainable)
     */
    public function sync(): \Oroboros\core\interfaces\library\session\SessionInterface
    {
        if (!$this->isActive()) {
            $this->start();
        }
        $this->syncSession();
        return $this;
    }

    /**
     * Resets the session, so a new session token will be issued.
     * All existing keys are persisted into the new session,
     * but a new session cookie will be issued for the new one.
     * 
     * The session will be started if it is not currently active to enable this operation.
     * 
     * @return $this (method chainable)
     */
    public function reset(): \Oroboros\core\interfaces\library\session\SessionInterface
    {
        $this->resetSession();
        return $this;
    }

    /**
     * Flushes all keys from the current session so it is in an empty state.
     * 
     * The session will be started if it is not currently active to enable this operation.
     * 
     * @return $this (method chainable)
     */
    public function flush(): \Oroboros\core\interfaces\library\session\SessionInterface
    {
        $this->clearSession();
        return $this;
    }

    /**
     * Populates the session with the provided array of values.
     * Existing keys are replaced if provided.
     * Existing keys not provided are retained.
     * 
     * The session will be started if it is not currently active to enable this operation.
     * 
     * @param array $values
     * @return $this (method chainable)
     */
    public function populate(array $values): \Oroboros\core\interfaces\library\session\SessionInterface
    {
        if (!$this->isActive()) {
            $this->start();
        }
        if (!$this::IS_CLI) {
            $this->getLogger()->debug('[type][scope][class] Populating session keys [session-keys].', [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
                'session-key' => implode(', ', array_keys($values)),
            ]);
            foreach ($values as $key => $value) {
                $this->set($key, $value);
            }
        }
        return $this;
    }

    /**
     * Returns a boolean determination as to whether the currently active session is empty.
     * 
     * The session will be started if it is not currently active to make this determination.
     * 
     * @return bool
     */
    public function isEmpty(): bool
    {
        if (!$this->isActive()) {
            $this->start();
        }
        if (!$this::IS_CLI) {
            return empty($_SESSION[$this::app()->config()->app->name]);
        }
        return true;
    }

    /**
     * Returns a boolean determination as to whether the session is currently active.
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->verifySessionLoaded();
    }

    /**
     * Retrieves the given key from the session
     * 
     * The session will be started if it is not currently active to enable this operation.
     * 
     * @param string $key
     * @return mixed
     * @throws \InvalidArgumentException if the provided key does not exist in the session
     */
    public function get(string $key)
    {
        if (!$this->isActive()) {
            $this->start();
        }
        if (!$this->has($key)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf(
                        'Error encountered in [%1$s]. '
                        . 'Provided key [%2$s] does not exist.', get_class($this), $key));
        }
        if (!$this::IS_CLI) {
            return $_SESSION[$this::app()->config()->app->name][$key];
        }
        return null;
    }

    /**
     * Sets the given key into the session as the provided value
     * 
     * The session will be started if it is not currently active to enable this operation.
     * 
     * @param string $key
     * @param type $value
     * @return $this (method chainable)
     */
    public function set(string $key, $value): \Oroboros\core\interfaces\library\session\SessionInterface
    {
        if (!$this->isActive()) {
            $this->start();
        }
        if (!$this::IS_CLI) {
            $this->getLogger()->debug('[type][scope][class] Setting session key [session-key].', [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
                'session-key' => $key,
            ]);
            $_SESSION[$this::app()->config()->app->name][$key] = $value;
        }
        return $this;
    }

    /**
     * Removes the given key from the session
     * 
     * The session will be started if it is not currently active to enable this operation.
     * 
     * @param string $key
     * @return void
     * @throws \InvalidArgumentException if the provided key does not exist in the session
     */
    public function unset(string $key): void
    {
        if (!$this->isActive()) {
            $this->start();
        }
        if (!$this->has($key)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf(
                        'Error encountered in [%1$s]. '
                        . 'Provided key [%2$s] does not exist.', get_class($this), $key));
        }
        if (!$this::IS_CLI) {
            $this->getLogger()->debug('[type][scope][class] Deleting session key [session-key].', [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
                'session-key' => $key,
            ]);
            unset($_SESSION[$this::app()->config()->app->name][$key]);
        }
    }

    /**
     * Returns a boolean determination as to whether the given key exists in the session.
     * 
     * The session will be started if it is not currently active to make this determination.
     * 
     * @param string $key
     * @return bool
     */
    public function has(string $key): bool
    {
        if (!$this->isActive()) {
            $this->start();
        }
        if (!$this::IS_CLI) {
            return array_key_exists($key, $_SESSION[$this::app()->config()->app->name]);
        }
        return false;
    }

    /**
     * Returns all keys currently defined in the session as an array.
     * The returned array is decoupled from the actual session superglobal,
     * and represents only a snapshot of their state when the method is called.
     * 
     * The session will be started if it is not currently active to enable this operation.

     * @return array
     */
    public function fetch(): array
    {
        if (!$this->isActive()) {
            $this->start();
        }
        if (!$this::IS_CLI) {
            return $_SESSION[$this::app()->config()->app->name];
        }
        return [];
    }

    /**
     * Closes the session and discards any variables altered since runtime started.
     * Does not destroy the session, it can be started on another execution
     * in the same state it was in when this runtime operation started.
     * 
     * Does nothing if the session is not currently open.
     * 
     * @return $this (method chainable)
     */
    public function abort(): \Oroboros\core\interfaces\library\session\SessionInterface
    {
        if ($this->isActive()) {
            if (!$this::IS_CLI) {
                $this->getLogger()->debug('[type][scope][class] Aborting HTTP session.', [
                    'class' => get_class($this),
                    'type' => static::CLASS_TYPE,
                    'scope' => static::CLASS_SCOPE,
                ]);
                session_abort();
            }
        }
        return $this;
    }

    /**
     * Closes the session and persists any variables altered since runtime started.
     * Does not destroy the session, it can be started on another execution
     * in the same state it was closed in.
     * 
     * Does nothing if the session is not currently open.
     * 
     * @return $this (method chainable)
     */
    public function close(): \Oroboros\core\interfaces\library\session\SessionInterface
    {
        if ($this->isActive()) {
            if (!$this::IS_CLI) {
                $this->getLogger()->debug('[type][scope][class] Writing and closing HTTP session.', [
                    'class' => get_class($this),
                    'type' => static::CLASS_TYPE,
                    'scope' => static::CLASS_SCOPE,
                ]);
                session_write_close();
            }
        }
        return $this;
    }

    /**
     * Public getter for the session auth token
     * 
     * This value will be used for authenticating that form submissions,
     * commands, api calls, ajax callbacks, etc have been made by the
     * current user to protect from csrf and argument injection attacks.
     * 
     * @return \Oroboros\core\interfaces\library\auth\TokenInterface
     */
    public function getAuthToken(): \Oroboros\core\interfaces\library\auth\TokenInterface
    {
        if (!$this::IS_CLI) {
            if (is_null(self::$token) && !$this->has(static::SESSION_AUTH_TOKEN_IDENTIFIER)) {
                $this->setSessionAuthToken();
            } elseif (is_null(self::$token) && $this->has(static::SESSION_AUTH_TOKEN_IDENTIFIER)) {
                $this->restoreSessionAuthToken();
            }
        } elseif (is_null(self::$token)) {
            $this->setSessionAuthToken();
        }
        return self::$token;
    }

    /**
     * Tells the session to rotate to a new auth token.
     * The previous token will be destroyed and replaced
     * with a new one.
     * 
     * @return void
     */
    public function rotateAuthToken(): void
    {
        $this->setSessionAuthToken();
    }

    /**
     * Returns a boolean designation as to whether
     * the current session has a session auth token.
     * 
     * @return bool
     */
    protected function hasSessionAuthToken(): bool
    {
        return (is_null(self::$token));
    }

    /**
     * Internal setter for the session auth token.
     * If no value is passed and no token exists, a token will be generated.
     * If no value is passed and a token does exist, a token will be generated
     * and the previous token will be replaced.
     * If a token is passed, it will be set or replace the existing token.
     * 
     * @param \Oroboros\core\interfaces\library\auth\TokenInterface $token
     * @return void
     */
    protected function setSessionAuthToken(\Oroboros\core\interfaces\library\auth\TokenInterface $token = null): void
    {
        if (is_null($token)) {
            $token = $this->generateSessionAuthToken();
        }
        if (!$this::IS_CLI) {
            $this->getLogger()->debug('[type][scope][class] Setting HTTP session authentication token into session.', [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
            ]);
            $_SESSION[$this::app()->config()->app->name][static::SESSION_AUTH_TOKEN_IDENTIFIER] = serialize($token);
        }
        self::$token = $token;
    }

    /**
     * Returns the session auth token
     * 
     * @return \Oroboros\core\interfaces\library\auth\TokenInterface
     */
    protected function generateSessionAuthToken(): \Oroboros\core\interfaces\library\auth\TokenInterface
    {
        $this->getLogger()->debug('[type][scope][class] Generating new session authentication token.', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
        ]);
        $token = $this->load('library', static::SESSION_AUTH_TOKEN_CLASS, null, $this->getArguments(), $this->getFlags());
        return $token;
    }
    
    /**
     * Receives a psr request object and returns the session id token,
     * or null if one does not exist.
     * 
     * This may extract from a cookie, bearer header, or whatever
     * other method is used to relay session id.
     * 
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @return string|null
     */
    protected function extractSessionId(\Psr\Http\Message\ServerRequestInterface $request): ?string
    {
        $this->scope($request);
        return self::$session_id; 
    }

    /**
     * Starts the session if it is not already started.
     * A new auth token will be automatically generated.
     * 
     * @return void
     */
    protected function startSession(): void
    {
        if (!$this->verifySessionLoaded()) {
            if (!$this::IS_CLI) {
                $this->extractSessionId($this::user()->request());
                $this->getLogger()->debug('[type][scope][class] Starting HTTP session with session id [session-id].', [
                    'class' => get_class($this),
                    'type' => static::CLASS_TYPE,
                    'scope' => static::CLASS_SCOPE,
                    'session-id' => self::$session_id,
                ]);
                session_start();
                self::$session_id = session_id();
                self::$session_loaded = true;
                if (!array_key_exists($this::app()->config()->app->name, $_SESSION)) {
                    $_SESSION[$this::app()->config()->app->name] = [];
                }
            }
            if (!$this::IS_CLI && !$this->has(static::SESSION_AUTH_TOKEN_IDENTIFIER)) {
                $this->setSessionAuthToken();
            }
        }
        self::$session_loaded = true;
    }

    /**
     * Destroys the session if it is loaded
     * @return void
     */
    protected function destroySession(): void
    {
        if ($this->verifySessionLoaded()) {
            if (!$this::IS_CLI) {
                $this->getLogger()->debug('[type][scope][class] Destroying HTTP session.', [
                    'class' => get_class($this),
                    'type' => static::CLASS_TYPE,
                    'scope' => static::CLASS_SCOPE,
                ]);
                session_destroy();
            }
        }
        self::$session_loaded = false;
    }

    /**
     * Destroys the session if it is loaded and starts a new one
     * 
     * @return void
     */
    protected function resetSession(): void
    {
        $this->getLogger()->debug('[type][scope][class] Resetting session.', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
        ]);
        if ($this->verifySessionLoaded()) {
            $this->destroySession();
        }
        $this->startSession();
    }

    /**
     * Synchronizes the session with the internal values
     * 
     * @return void
     */
    protected function syncSession(): void
    {
        $this->getLogger()->debug('[type][scope][class] Synchronizing session.', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
        ]);
//        if ($this->verifySessionLoaded()) {
//            $this->destroySession();
//        }
//        $this->startSession();
    }

    /**
     * Clears all existing values from the session without destroying it.
     * This only does anything if the session is started.
     * A new auth token will be automatically generated.
     * 
     * @return void
     */
    protected function clearSession(): void
    {
        if ($this->verifySessionLoaded()) {
            if (!$this::IS_CLI) {
                $this->getLogger()->debug('[type][scope][class] Clearing all values from session.', [
                    'class' => get_class($this),
                    'type' => static::CLASS_TYPE,
                    'scope' => static::CLASS_SCOPE,
                ]);
                foreach ($_SESSION[$this::app()->config()->app->name] as $key => $value) {
                    unset($_SESSION[$this::app()->config()->app->name][$key]);
                }
            }
            $this->setSessionAuthToken();
        }
    }

    /**
     * Persists the arguments handed to the constructor into the session.
     * This will start the session if it is not already started.
     * 
     * @return void
     */
    protected function persistValues(): void
    {
        $this->startSession();
        if (!$this::IS_CLI) {
            $this->getLogger()->debug('[type][scope][class] Persisting object arguments into session.', [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
            ]);
            foreach ($this->getArguments() as $key => $value) {
                $_SESSION[$this::app()->config()->app->name][$key] = $value;
            }
        }
    }

    /**
     * Persists the arguments handed to the constructor into the session.
     * This will start the session if it is not already started.
     * 
     * @return void
     */
    protected function retrieveValues(): void
    {
        $this->startSession();
        if (!$this::IS_CLI) {
            $this->getLogger()->debug('[type][scope][class] Retrieving session keys into local arguments.', [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
            ]);
            foreach ($_SESSION[$this::app()->config()->app->name] as $key => $value) {
                $this->setArgument($key, $value);
            }
        }
    }

    /**
     * Performs any session initialization tasks required for a useable session.
     * 
     * You may override this method if you have additional
     * session initialization logic to perform.
     * 
     * @return void
     */
    protected function initializeSession(): void
    {
        $this->getLogger()->debug('[type][scope][class] Initializing session object.', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
        ]);
        $this->initializeSessionHandler();
    }

    /**
     * Restores a token from the session back into the object
     * 
     * This should fire on each call to `start`,
     * as well as any time the token is referenced
     * and not already present in the object scope.
     * 
     * @return void
     */
    protected function restoreSessionAuthToken(): void
    {
        if (!$this::IS_CLI) {
            $this->getLogger()->debug('[type][scope][class] Restoring existing session auth token from session.', [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
            ]);
            $token = unserialize($this->get(static::SESSION_AUTH_TOKEN_IDENTIFIER));
            self::$token = $token;
            return;
        }
        $this->setSessionAuthToken();
    }

    /**
     * Verifies whether or not the session is loaded
     * @return bool
     */
    protected function verifySessionLoaded(): bool
    {
        if (!$this::IS_CLI) {
            return session_status() !== PHP_SESSION_NONE && self::$session_loaded;
        }
        return self::$session_loaded;
    }

    /**
     * Performs any session initialization tasks required
     * for a usable session handler.
     * 
     * You may override this method if you have additional
     * session initialization logic to perform.
     * 
     * @return void
     * @throws \Oroboros\core\exception\ErrorException
     *         If the given session handler object cannot be set
     */
    protected function initializeSessionHandler(): void
    {
        if (is_null(self::$session_handler)) {
            $this->getLogger()->debug('[type][scope][class] Initializing session handler object.', [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
            ]);
            $handler = $this->load('library', static::SESSION_HANDLER_CLASS, null, $this->getArguments(), $this->getFlags());
            $handler->setLogger($this->getLogger());
            if (session_set_save_handler($handler, true) === false) {
                // Error setting save handler
                throw new \Oroboros\core\exception\ErrorException(sprintf(
                            'Error encountered in [%1$s]. Unable to set session handler of type [%2$s].', get_class($this), static::SESSION_HANDLER_CLASS,
                        ), 0, E_ERROR, __FILE__, __LINE__);
            }
            self::$session_handler = $handler;
        }
    }

    /**
     * Verifies that the session handler class is valid
     * 
     * @return void
     * @throws \Oroboros\core\exception\core\InvalidClassException
     */
    private function verifySessionHandlerClass(): void
    {
        $expected = \Oroboros\core\interfaces\library\session\SessionHandlerInterface::class;
        $class = $this->getFullClassName('library', static::SESSION_HANDLER_CLASS);
        if ($class === false || (!in_array($expected, class_implements($class)))) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error Encountered in [%1$s]. Expected class constant '
                        . '[%2$s] with value [%3$s] must resolve to a class or stub '
                        . 'class name implementing expected interface [%4$s]. '
                        . 'This class is not useable in it\'s current state.',
                        get_class($this), 'SESSION_HANDLER_CLASS', static::SESSION_HANDLER_CLASS, $expected),
                    0, E_ERROR, __FILE__, __LINE__
            );
        }
    }
}
