<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\error;

/**
 * Abstract Error Handler
 * Provides abstraction for error handlers
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
abstract class AbstractErrorHandler extends \Oroboros\core\abstracts\library\AbstractLibrary implements \Oroboros\core\interfaces\library\error\ErrorHandlerInterface
{

    /**
     * Represents whether or not the error handler is currently active
     * 
     * @var bool
     */
    private $is_active = false;

    /**
     * Represents whether or not the current error handler is registered
     * 
     * @var bool
     */
    private $is_registered = false;

    /**
     * The currently scoped error code
     * 
     * @var int|null
     */
    private $code = null;

    /**
     * The currently scoped error message
     * 
     * @var string|null
     */
    private $message = null;

    /**
     * The currently scoped error line number
     * 
     * @var int|null
     */
    private $line = null;

    /**
     * The currently scoped error file name
     * 
     * @var file|null
     */
    private $file = null;

    /**
     * The currently scoped error context
     * 
     * @var file|null
     */
    private $context = null;

    /**
     * The currently scoped error handling status
     * `true` if handled, `false` otherwise
     * 
     * @var file|null
     */
    private $status = false;

    /**
     * The log of all errors handed to this error handler.
     * 
     * @var array
     */
    private $log = null;

    /**
     * The previous error handler, if any
     * 
     * @var string|array|object|null
     */
    private $previous_error_handler = null;

    /**
     * Oroboros error handlers use the default constructor
     * 
     * @param string $command
     * @param array $arguments
     * @param array $flags
     */
    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeErrorLog();
    }

    /**
     * Registers the error handler
     * 
     * @return void
     */
    public function register(): void
    {
        $this->resetError();
        $existing = set_error_handler($handler = set_error_handler('var_dump'));
        if ($this === $existing) {
            // Already the existing error handler
            $this->is_registered = true;
            return;
        }
        $this->previous_error_handler = set_error_handler([$this, 'handle'], $this->declareErrorHandlerScope());
        $this->is_registered = true;
    }

    /**
     * Unregisters the error handler
     * 
     * @return void
     */
    public function unregister(): void
    {
        $this->resetError();
        $this->restorePrevious();
        $this->is_registered = false;
    }
    
    /**
     * Restores the previous error handler if this error handler is currently registered.
     * If this error handler is not currently registered, does nothing
     * 
     * @return void
     */
    public function restorePrevious(): void
    {
        if (!$this->isRegistered()) {
            // do nothing
        }
        restore_error_handler();
        $this->is_registered = false;
    }

    /**
     * Returns a boolean designation as to whether the error handler
     * is currently registered.
     * If the error handler is registered directly, this must return `true`
     * If the error handler is encapsulated within an error handler
     * aggregate that returns `true` for the same method call,
     * this must also return `true`
     * 
     * If none of the above apply, this must return `false`.
     * 
     * @return bool
     */
    public function isRegistered(): bool
    {
        return $this->is_registered;
    }

    /**
     * Activates the error handler
     * 
     * @return void
     */
    public function activate(): void
    {
        $this->resetError();
        $this->is_active = true;
    }

    /**
     * Deactivates the error handler
     * 
     * @return void
     */
    public function deactivate(): void
    {
        $this->resetError();
        $this->is_active = false;
    }

    /**
     * Returns a boolean designation as to whether the error handler
     * is currently activated.
     * If the error handler is not registered, this must return `false`
     * If the error handler is activated directly, this must return `true`
     * If the error handler is encapsulated within an error handler
     * aggregate that returns `true` for the same method call,
     * this must also return `true`
     * 
     * If none of the above apply, this must return `false`.
     * 
     * @return bool
     */
    public function isActive(): bool
    {
        if (!$this->isRegistered()) {
            return false;
        }
        return $this->is_active;
    }

    /**
     * Receives an error and determines if the error was handled.
     * If the error was handled, this must return `true`.
     * If the error was not handled, this must return `false`,
     * If the error handler is not registered, this must return `false`.
     * If the error handler is not activated, this must return `false`.
     * 
     * @param int $errno The level of error raised
     * @param string $errstr The error message
     * @param string $errfile The file the error originated from
     * @param int $errline The line number within the file that the error originated from
     * @param array $errcontext An array of all variables existing in the error scope
     * @return bool
     */
    public function handle(int $errno, string $errstr, string $errfile = null, int $errline = null, array $errcontext = null): bool
    {
        $this->resetError();
        if (!$this->isActive() && $this->isRegistered()) {
            return false;
        }
        $this->setCode($errno);
        $this->setMessage($errstr);
        $this->setFile($errfile);
        $this->setLine($errline);
        $this->setContext($errcontext);
        return $this->getStatus();
    }

    /**
     * Returns the code of the currently scoped error,
     * or null if no error is currently scoped
     * 
     * @return int|null
     */
    public function getCode(): ?int
    {
        return $this->code;
    }

    /**
     * Returns the message of the currently scoped error,
     * or null if no error is currently scoped
     * 
     * @return string|null
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * Returns the file name of the currently scoped error,
     * or null if no error is currently scoped
     * 
     * @return string|null
     */
    public function getFile(): ?string
    {
        return $this->file;
    }

    /**
     * Returns the line number of the currently scoped error,
     * or null if no error is currently scoped
     * 
     * @return int|null
     */
    public function getLine(): ?int
    {
        return $this->line;
    }

    /**
     * Returns the context of the currently scoped error,
     * or null if no error is currently scoped
     * 
     * @return array|null
     */
    public function getContext(): ?array
    {
        return $this->context;
    }

    /**
     * Returns whether or not the currently scoped error was handled.
     * Returns `true` if this object handled the error successfully,
     * `false` in all other cases.
     * @return bool
     */
    public function getStatus(): bool
    {
        if (!$this->isActive() || !$this->isRegistered()) {
            return false;
        }
        return $this->status;
    }

    /**
     * Returns a log of all errors passed to this error handler,
     * and a boolean designation as to whether the each of them
     * was marked as handled by this error handler.
     */
    public function getLog()
    {
        return $this->log;
    }

    /**
     * Sets the error code for the currently scoped error
     * 
     * @param int $code
     * @return void
     */
    protected function setCode(int $code = null): void
    {
        $this->code = $code;
    }

    /**
     * Sets the message for the currently scoped error
     * 
     * @param string $message
     * @return void
     */
    protected function setMessage(string $message = null): void
    {
        $this->message = $message;
    }

    /**
     * Sets the line number for the currently scoped error
     * 
     * @param int $line
     * @return void
     */
    protected function setLine(int $line = null): void
    {
        $this->line = $line;
    }

    /**
     * Sets the file for the currently scoped error
     * 
     * @param string $file
     * @return void
     */
    protected function setFile(string $file = null): void
    {
        $this->file = $file;
    }

    /**
     * Sets the error context for the currently scoped error
     * 
     * @param array $context
     * @return void
     */
    protected function setContext(array $context = null): void
    {
        $this->context = $context;
    }

    /**
     * Sets the handled status for the currently scoped error
     * 
     * @param bool $status
     * @return void
     */
    protected function setStatus(bool $status): void
    {
        $this->status = $status;
    }

    /**
     * Resets all error attributes
     * 
     * @return void
     */
    protected function resetError(): void
    {
        $this->resetCode();
        $this->resetError();
        $this->resetFile();
        $this->resetLine();
        $this->resetMessage();
        $this->resetContext();
        $this->resetStatus();
    }

    /**
     * Resets the error code for the currently scoped error
     * 
     * @return void
     */
    protected function resetCode(): void
    {
        $this->code = null;
    }

    /**
     * Resets the message for the currently scoped error
     * 
     * @return void
     */
    protected function resetMessage(): void
    {
        $this->message = null;
    }

    /**
     * Resets the line number for the currently scoped error
     * 
     * @return void
     */
    protected function resetLine(): void
    {
        $this->line = null;
    }

    /**
     * Resets the file name for the currently scoped error
     * 
     * @return void
     */
    protected function resetFile(): void
    {
        $this->file = null;
    }

    /**
     * Resets the error context for the currently scoped error
     * 
     * @return void
     */
    protected function resetContext(): void
    {
        $this->context = null;
    }

    /**
     * Resets the handled status of the currently scoped error
     * 
     * @return void
     */
    protected function resetStatus(): void
    {
        $this->status = false;
    }

    /**
     * Returns the error scopes handled by the error handler.
     * 
     * This must return a bitmask of valid error codes.
     * 
     * @return int
     */
    protected function declareErrorHandlerScope(): int
    {
        return E_DEPRECATED | E_NOTICE | E_RECOVERABLE_ERROR | E_STRICT | E_USER_DEPRECATED | E_USER_NOTICE | E_USER_WARNING | E_WARNING;
    }

    /**
     * Initializes the error log
     * 
     * @return void
     */
    private function initializeErrorLog(): void
    {
        if (!is_null($this->log)) {
            return;
        }
    }
}
