<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\database\view;

/**
 * Abstract Database View
 *
 * @author Brian Dayhoff
 */
abstract class AbstractView extends \Oroboros\core\abstracts\library\database\AbstractDatabaseLibrary implements \Oroboros\core\interfaces\library\database\view\ViewInterface
{

    use \Oroboros\core\traits\database\DatabaseObjectUtilityTrait;

    const CLASS_SCOPE = 'view';

    /**
     * The associated database schema
     * @var string
     */
    private $schema = null;

    /**
     * The columns associated with the table
     * @var \Oroboros\core\interfaces\container\ContainerInterface
     */
    private $columns = null;

    /**
     * The columns associated with the table
     * @var \Oroboros\core\interfaces\container\ContainerInterface
     */
    private $indexes = null;

    /**
     * The columns associated with the table
     * @var \Oroboros\core\interfaces\container\ContainerInterface
     */
    private $relationships = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->verifyName();
        $this->initializeView();
    }

    public function __toString()
    {
        return sprintf('%1$s%4$s%2$s%3$s%1$s%1$s%5$s%2$s', static::LEFT_DELIMITER, static::RIGHT_DELIMITER, static::SEPARATOR, $this->getSchema(), $this->getName());
    }

    public function getSchema(): string
    {
        if (is_null($this->schema)) {
            $this->parseSchema();
        }
        return $this->schema;
    }

    public function getName(): string
    {
        return $this->getCommand();
    }

    public function getColumns()
    {
        if (is_null($this->columns)) {
            $this->parseColumns();
        }
        return $this->columns;
    }

    public function getIndexes()
    {
        if (is_null($this->indexes)) {
            $this->parseIndexes();
        }
        return $this->indexes;
    }

    public function getRelationships()
    {
        if (is_null($this->relationships)) {
            $this->parseRelationships();
        }
        return $this->relationships;
    }

    private function initializeView(): void
    {
        $this->setConnection($this->getArgument('connection'));
        $this->initializeTableAliases();
        $this->initializeColumnAliases();
        parent::addView($this);
    }

    private function parseSchema(): void
    {
        $this->schema = $this->getArgument(static::INFORMATION_SCHEMA_SELECTOR);
    }

    private function parseColumns(): void
    {
        try {
            $this->columns = parent::getColumnObjectsForTable($this->getSchema(), $this->getName());
        } catch (\InvalidArgumentException $e) {
            $this->columns = $this::containerize();
            $schema = $this->getSchema();
            $tablename = $this->getName();
            $table = sprintf('%1$s%4$s%2$s%3$s%1$s%5$s%2$s%', static::LEFT_DELIMITER, static::RIGHT_DELIMITER, static::SEPARATOR, static::INFORMATION_SCHEMA, static::INFORMATION_TABLE_COLUMNS);
            $sql = sprintf('SELECT * FROM %1$s%2$s', $table, PHP_EOL);
            $sql .= sprintf('WHERE %1$s%3$s%2$s=:%3$s%4$s', static::LEFT_DELIMITER, static::RIGHT_DELIMITER, static::INFORMATION_SCHEMA_SELECTOR, PHP_EOL);
            $sql .= sprintf('AND %1$s%3$s%2$s=:%3$s%4$s', static::LEFT_DELIMITER, static::RIGHT_DELIMITER, static::INFORMATION_TABLE_SELECTOR, PHP_EOL);
            $sql .= static::DELIMITER;
            $sth = $this->getConnection()->prepare($sql);
            $sth->bindParam(static::INFORMATION_SCHEMA_SELECTOR, $schema);
            $sth->bindParam(static::INFORMATION_TABLE_SELECTOR, $tablename);
            $sth->execute();
            while ($row = $sth->fetch()) {
                $row['connection'] = $this->getArgument('connection');
                $this->columns[$row[static::INFORMATION_COLUMN_SELECTOR]] = $this->load('library', static::COLUMN_CLASS, $row[static::INFORMATION_COLUMN_SELECTOR], $row);
            }
        }
    }

    private function parseIndexes(): void
    {
        $this->indexes = $this::containerize();
    }

    private function parseRelationships(): void
    {
        $this->relationships = $this::containerize();
    }

    private function verifyName(): void
    {
        if (is_null($this->getCommand())) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'View must declare a name.', get_class($this)));
        }
    }
}
