<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\database\relationship;

/**
 * Abstract Database Relationship
 *
 * @author Brian Dayhoff
 */
abstract class AbstractRelationship extends \Oroboros\core\abstracts\library\database\AbstractDatabaseLibrary implements \Oroboros\core\interfaces\library\database\relationship\RelationshipInterface
{

    use \Oroboros\core\traits\database\DatabaseObjectUtilityTrait;

    const CLASS_SCOPE = 'relationship';

    private $schema = null;
    private $table = null;
    private $columns = null;
    private $remote_table = null;
    private $remote_schema = null;
    private $remote_columns = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->verifyName();
        $this->initializeRelationship();
    }

    public function __toString()
    {
        return sprintf('%1$s%3$s%2$s', static::LEFT_DELIMITER, static::RIGHT_DELIMITER, $this->getName());
    }

    public function getSchema(): string
    {
        return $this->schema;
    }

    public function getTable(): string
    {
        return $this->table;
    }

    public function getName(): string
    {
        return $this->getCommand();
    }

    private function initializeRelationship(): void
    {
        $this->setConnection($this->getArgument('connection'));
        $this->columns = $this::containerize();
        $this->remote_columns = $this::containerize();
        $this->parseDefinition();
        parent::addRelationship($this);
    }

    private function parseDefinition(): void
    {
        $this->schema = $this->getArgument(static::INFORMATION_SCHEMA_SELECTOR);
        $this->table = $this->getArgument(static::INFORMATION_TABLE_SELECTOR);
        $this->remote_schema = $this->getArgument(static::RELATIONSHIP_SCHEMA_SELECTOR);
        $this->remote_table = $this->getArgument(static::RELATIONSHIP_TABLE_SELECTOR);
    }

    private function verifyName(): void
    {
        if (is_null($this->getCommand())) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Relationships must declare a name.', get_class($this)));
        }
    }
}
