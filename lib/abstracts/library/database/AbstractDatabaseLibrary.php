<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\database;

/**
 * Abstract Database Library
 * Baseline abstraction for database libraries.
 *
 * @author Brian Dayhoff
 */
abstract class AbstractDatabaseLibrary extends \Oroboros\core\abstracts\library\AbstractLibrary implements \Serializable
{

    const CLASS_TYPE = 'database-library';

    /**
     * Represents the default configuration, which is always used
     * if no driver-specific configuration is defined.
     * @var Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private static $config_default = null;

    /**
     * Represents details about specific database elements,
     * and which connections and interdependencies they match to.
     * 
     * These populate a static cache, so they are not duplicated,
     * and analysis of a database layer is consistent everywhere.
     * 
     * @var \Oroboros\core\interfaces\library\container\CollectionInterface
     */
    private static $database_registry = null;

    /**
     * Cache layers (or null cache objects) for each driver type,
     * as per their schema config settings.
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private static $cache_pool = null;

    /**
     * Disk location of the schema cache
     * @var string
     */
    private static $schema_cache = null;

    /**
     * Represents the driver configuration. If not explicitly defined
     * for the current driver, the default is used instead.
     * @var Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $config = null;

    /**
     * Represents the currently associated connection, if any.
     * @var \Oroboros\core\interfaces\library\database\connection\ConnectionInterface
     */
    private $connection = null;

    /**
     * The currently scoped cache pool for schema caching.
     * If this is a null cache, the schema is not cached,
     * but a valid Psr-6 cache pool object always exists
     * even if it intentionally does nothing.
     * @var \Oroboros\core\interfaces\library\cache\CachePoolInterface
     */
    private $cache = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        $this->verifyFormat();
        parent::__construct($command, $arguments, $flags);
        $this->initializeDatabaseConfig();
        $this->initializeCacheConfig();
        $this->initializeDatabaseRegistry();
    }

    public function __destruct()
    {
        /**
         * Commit any deferred updates to the cache back into the cache
         */
        $this->cache->commit();
    }

    public function serialize()
    {
        $data = [
            'command' => $this->getCommand(),
            'arguments' => $this->getArguments(),
            'flags' => $this->getFlags()
        ];
        return serialize($data);
    }

    public function unserialize($data)
    {
        $data = unserialize($data);
        if (!$data) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. Provided serialized data is not valid. Could not unserialize object.', get_class($this)));
        }
        parent::__construct($data['command'], $data['arguments'], $data['flags']);
        $this->initializeDatabaseConfig();
        $this->initializeCacheConfig();
        $this->initializeDatabaseRegistry();
    }

    /**
     * Returns the current connection object associated with the database object.
     * @return \Oroboros\core\interfaces\library\connection\ConnectionInterface
     * @throws \ErrorException If no connection is currently defined
     */
    protected function getConnection(): \Oroboros\core\interfaces\library\connection\ConnectionInterface
    {
        if (is_null($this->connection)) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. No connection is currently defined.', get_class($this)));
        }
        return $this->connection;
    }

    protected function addConnection(string $key, \Oroboros\core\interfaces\library\connection\ConnectionInterface $connection): void
    {
        self::$database_registry[static::DATABASE_DRIVER]['connection'][$key] = $connection;
    }

    protected function setConnection(string $key): void
    {
        if (!self::$database_registry[static::DATABASE_DRIVER]['connection']->has($key)) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. No known connection exists for key [%2$s].', get_class($this), $key));
        }
        $this->connection = self::$database_registry[static::DATABASE_DRIVER]['connection']->get($key);
    }

    protected function addSchema(\Oroboros\core\interfaces\library\database\schema\SchemaInterface $schema): void
    {
        $name = $schema->getName();
        self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\schema\AbstractSchema::CLASS_SCOPE][$name] = $schema;
//        if ($this->config->get('map')->get('schema')) {
//            $cache_key = sha1(static::DATABASE_DRIVER . '.' . $this->getConnection()->getName() . '.schema.' . $name);
//            $cache_item = $this->cache->getItem($cache_key);
//            $cache_item->set($schema);
//            $cache_item->expiresAfter($this->config->get('cache')->get('ttl'));
//            $this->cache->save($cache_item);
//        }
    }

    protected function getSchemaObject(string $schema): \Oroboros\core\interfaces\library\database\schema\SchemaInterface
    {
        if (!self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\schema\AbstractSchema::CLASS_SCOPE]->has($schema)) {
            $cache_key = sha1(static::DATABASE_DRIVER . '.' . $this->getConnection()->getName() . '.schema.' . $schema);
            if ($this->config->get('map')->get('schema') && $this->cache->hasItem($cache_key)) {
                self::$database_registry[\Oroboros\core\abstracts\library\database\schema\AbstractSchema::CLASS_SCOPE][$schema] = $this->cache->getItem($cache_key)->get();
            } else {
                throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                            . 'Specified schema [%2$s] is not known.', get_class($this), $schema));
            }
        }
        return self::$database_registry[\Oroboros\core\abstracts\library\database\schema\AbstractSchema::CLASS_SCOPE][$schema];
    }

    protected function addTable(\Oroboros\core\interfaces\library\database\table\TableInterface $table): void
    {
        $schema = $table->getSchema()->getName();
        $name = $table->getName();
        if (!self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\table\AbstractTable::CLASS_SCOPE]->has($schema)) {
            self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\table\AbstractTable::CLASS_SCOPE][$schema] = $this->containerize();
        }
        self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\table\AbstractTable::CLASS_SCOPE][$schema][$name] = $table;
//        if ($this->config->get('map')->get('table')) {
//            $cache_key = sha1(static::DATABASE_DRIVER . '.' . $this->getConnection()->getName() . '.table.' . $schema . '.' . $name);
//            $cache_item = $this->cache->getItem($cache_key);
//            $cache_item->set($table);
//            $cache_item->expiresAfter($this->config->get('cache')->get('ttl'));
//            $this->cache->save($cache_item);
//        }
    }

    protected function getTableObject(string $schema, string $table): \Oroboros\core\interfaces\library\database\table\TableInterface
    {
        if (!self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\table\AbstractTable::CLASS_SCOPE]->has($schema) || !self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\table\AbstractTable::CLASS_SCOPE][$schema]->has($table)) {
            $cache_key = sha1(static::DATABASE_DRIVER . '.' . $this->getConnection()->getName() . '.table.' . $schema . '.' . $table);
            if ($this->config->get('map')->get('table') && $this->cache->hasItem($cache_key)) {
                self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\table\AbstractTable::CLASS_SCOPE][$schema][$table] = $this->cache->getItem($cache_key)->get();
            } else {
                throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                            . 'Specified table [%2$s] in schema [%3$s] is not known.', get_class($this), $table, $schema));
            }
        }
        return self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\table\AbstractTable::CLASS_SCOPE][$schema][$table];
    }

    protected function getTableObjectsForSchema(string $schema): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        if (!self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\table\AbstractTable::CLASS_SCOPE]->has($schema)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Specified schema [%2$s] is not known.', get_class($this), $schema));
        }
        return self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\table\AbstractTable::CLASS_SCOPE][$schema];
    }

    protected function addView(\Oroboros\core\interfaces\library\database\view\ViewInterface $view): void
    {
        $schema = $view->getSchema()->getName();
        $name = $view->getName();
        if (!self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\view\AbstractView::CLASS_SCOPE]->has($schema)) {
            self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\view\AbstractView::CLASS_SCOPE][$schema] = $this->containerize();
        }
        self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\view\AbstractView::CLASS_SCOPE][$schema][$name] = $view;
//        if ($this->config->get('map')->get('view')) {
//            $cache_key = sha1(static::DATABASE_DRIVER . '.' . $this->getConnection()->getName() . '.view.' . $schema . '.' . $name);
//            $cache_item = $this->cache->getItem($cache_key);
//            $cache_item->set($view);
//            $cache_item->expiresAfter($this->config->get('cache')->get('ttl'));
//            $this->cache->save($cache_item);
//        }
    }

    protected function getViewObject(string $schema, string $view): \Oroboros\core\interfaces\library\database\table\TableInterface
    {
        if (!self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\view\AbstractView::CLASS_SCOPE]->has($schema) || !self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\view\AbstractView::CLASS_SCOPE][$schema]->has($view)) {
            $cache_key = sha1(static::DATABASE_DRIVER . '.' . $this->getConnection()->getName() . '.view.' . $schema . '.' . $view);
            if ($this->config->get('map')->get('view') && $this->cache->hasItem($cache_key)) {
                self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\view\AbstractView::CLASS_SCOPE][$schema][$view] = $this->cache->getItem($cache_key)->get();
            } else {
                throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                            . 'Specified view [%2$s] in schema [%3$s] is not known.', get_class($this), $view, $schema));
            }
        }
        return self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\view\AbstractView::CLASS_SCOPE][$schema][$view];
    }

    protected function getViewObjectsForSchema(string $schema): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        if (!self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\view\AbstractView::CLASS_SCOPE]->has($schema)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Specified schema [%2$s] is not known.', get_class($this), $schema));
        }
        return self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\view\AbstractView::CLASS_SCOPE][$schema];
    }

    protected function addColumn(\Oroboros\core\interfaces\library\database\column\ColumnInterface $column)
    {
        $schema = $column->getSchema()->getName();
        $table = $column->getTable()->getName();
        if (!self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\column\AbstractColumn::CLASS_SCOPE]->has($schema)) {
            self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\column\AbstractColumn::CLASS_SCOPE][$schema] = $this->containerizeInto('Oroboros\\core\\library\\container\\Collection');
        }
        if (!self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\column\AbstractColumn::CLASS_SCOPE][$schema]->has($table)) {
            self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\column\AbstractColumn::CLASS_SCOPE][$schema][$table] = $this->containerize();
        }
        self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\column\AbstractColumn::CLASS_SCOPE][$schema][$table][$column->getName()] = $column;
    }

    protected function getColumnObject(string $schema, string $table, string $column): \Oroboros\core\interfaces\library\database\column\ColumnInterface
    {
        if (!self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\column\AbstractColumn::CLASS_SCOPE]->has($schema)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Specified schema [%2$s] is not known for column [%3$s].', get_class($this), $schema, $column));
        }
        if (!self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\column\AbstractColumn::CLASS_SCOPE][$schema]->has($table)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Specified table [%2$s] is not known for schema [%3$s] with column [%4$s].', get_class($this), $schema, $table, $column));
        }
        if (!self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\column\AbstractColumn::CLASS_SCOPE][$schema][$table]->has($column)) {
            $cache_key = sha1(static::DATABASE_DRIVER . '.' . $this->getConnection()->getName() . '.column.' . $schema . '.' . $table . '.' . $column);
            if ($this->config->get('map')->get('column') && $this->cache->hasItem($cache_key)) {
                self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\column\AbstractColumn::CLASS_SCOPE][$schema][$table][$column] = $this->cache->getItem($cache_key)->get();
            } else {
                throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                            . 'Specified column [%2$s] is not known for table [%3$s] of schema [%4$s].', get_class($this), $column, $table, $schema));
            }
        }
        return self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\column\AbstractColumn::CLASS_SCOPE][$schema][$table][$column];
    }

    protected function getColumnObjectsForTable(string $schema, string $table): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        if (!self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\column\AbstractColumn::CLASS_SCOPE]->has($schema)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Specified table [%2$s] is not known for schema [%3$s].', get_class($this), $table, $schema));
        }
        if (!self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\column\AbstractColumn::CLASS_SCOPE][$schema]->has($table)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Specified table [%2$s] is not known for schema [%3$s] with column [%4$s].', get_class($this), $schema, $table, $column));
        }
        return self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\column\AbstractColumn::CLASS_SCOPE][$schema][$table];
    }

    protected function addIndex(\Oroboros\core\interfaces\library\database\index\IndexInterface $index)
    {
        $schema = $index->getSchema()->getName();
        $table = $index->getTable()->getName();
        if (!self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\index\AbstractIndex::CLASS_SCOPE]->has($schema)) {
            self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\index\AbstractIndex::CLASS_SCOPE][$schema] = $this->containerizeInto('Oroboros\\core\\library\\container\\Collection');
        }
        if (!self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\index\AbstractIndex::CLASS_SCOPE][$schema]->has($table)) {
            self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\index\AbstractIndex::CLASS_SCOPE][$schema][$table] = $this->containerize();
        }
        self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\index\AbstractIndex::CLASS_SCOPE][$schema][$table][$index->getName()] = $index;
//        if ($this->config->get('map')->get('index')) {
//            $cache_key = sha1(static::DATABASE_DRIVER . '.' . $this->getConnection()->getName() . '.index.' . $schema . '.' . $table . '.' . $index->getName());
//            $cache_item = $this->cache->getItem($cache_key);
//            $cache_item->set($index);
//            $cache_item->expiresAfter($this->config->get('cache')->get('ttl'));
//            $this->cache->save($cache_item);
//        }
    }

    protected function getIndexObject(string $schema, string $table, string $index): \Oroboros\core\interfaces\library\database\index\IndexInterface
    {
        if (!self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\index\AbstractIndex::CLASS_SCOPE]->has($schema)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Specified schema [%2$s] is not known for index [%3$s].', get_class($this), $schema, $index));
        }
        if (!self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\index\AbstractIndex::CLASS_SCOPE][$schema]->has($table)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Specified table [%2$s] is not known for schema [%3$s] for index [%4$s].', get_class($this), $schema, $table, $index));
        }
        if (!self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\index\AbstractIndex::CLASS_SCOPE][$schema][$table]->has($index)) {
            $cache_key = sha1(static::DATABASE_DRIVER . '.' . $this->getConnection()->getName() . '.index.' . $schema . '.' . $table . '.' . $index);
            if ($this->config->get('map')->get('index') && $this->cache->hasItem($cache_key)) {
                self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\index\AbstractIndex::CLASS_SCOPE][$schema][$table][$index] = $this->cache->getItem($cache_key)->get();
            } else {
                throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                            . 'Specified index [%2$s] is not known for table [%3$s] of schema [%4$s].', get_class($this), $index, $table, $schema));
            }
        }
        return self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\index\AbstractIndex::CLASS_SCOPE][$schema][$table][$index];
    }

    protected function getIndexObjectsForTable(string $schema, string $table): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        if (!self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\index\AbstractIndex::CLASS_SCOPE]->has($schema)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Specified table [%2$s] is not known for schema [%3$s].', get_class($this), $table, $schema));
        }
        if (!self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\index\AbstractIndex::CLASS_SCOPE][$schema]->has($table)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Specified table [%2$s] is not known for schema [%3$s] for index [%4$s].', get_class($this), $schema, $table, $index));
        }
        return self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\index\AbstractIndex::CLASS_SCOPE][$schema][$table];
    }

    protected function addRelationship(\Oroboros\core\interfaces\library\database\relationship\RelationshipInterface $relationship)
    {
        $schema = $relationship->getSchema()->getName();
        $table = $relationship->getTable()->getName();
        if (!self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\relationship\AbstractRelationship::CLASS_SCOPE]->has($schema)) {
            self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\relationship\AbstractRelationship::CLASS_SCOPE][$schema] = $this->containerizeInto('Oroboros\\core\\library\\container\\Collection');
        }
        if (!self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\relationship\AbstractRelationship::CLASS_SCOPE][$schema]->has($table)) {
            self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\relationship\AbstractRelationship::CLASS_SCOPE][$schema][$table] = $this->containerize();
        }
        self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\relationship\AbstractRelationship::CLASS_SCOPE][$schema][$table][$relationship->getName()] = $relationship;
//        if ($this->config->get('map')->get('relationship')) {
//            $cache_key = sha1(static::DATABASE_DRIVER . '.' . $this->getConnection()->getName() . '.relationship.' . $schema . '.' . $table . '.' . $relationship->getName());
//            $cache_item = $this->cache->getItem($cache_key);
//            $cache_item->set($relationship);
//            $cache_item->expiresAfter($this->config->get('cache')->get('ttl'));
//            $this->cache->save($cache_item);
//        }
    }

    protected function getRelationshipObject(string $schema, string $table, string $relationship): \Oroboros\core\interfaces\library\database\relationship\RelationshipInterface
    {
        if (!self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\relationship\AbstractRelationship::CLASS_SCOPE]->has($schema)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Specified schema [%2$s] is not known for relationship [%3$s].', get_class($this), $schema, $relationship));
        }
        if (!self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\relationship\AbstractRelationship::CLASS_SCOPE][$schema]->has($table)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Specified table [%2$s] is not known for schema [%3$s] for relationship [%4$s].', get_class($this), $schema, $table, $relationship));
        }
        if (!self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\relationship\AbstractRelationship::CLASS_SCOPE][$schema][$table]->has($relationship)) {
            $cache_key = sha1(static::DATABASE_DRIVER . '.' . $this->getConnection()->getName() . '.relationship.' . $schema . '.' . $table . '.' . $relationship);
            if ($this->config->get('map')->get('relationship') && $this->cache->hasItem($cache_key)) {
                self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\relationship\AbstractRelationship::CLASS_SCOPE][$schema][$table][$relationship] = $this->cache->getItem($cache_key)->get();
            } else {
                throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                            . 'Specified relationship [%2$s] is not known for table [%3$s] of schema [%4$s].', get_class($this), $relationship, $table, $schema));
            }
        }
        return self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\relationship\AbstractRelationship::CLASS_SCOPE][$schema][$table][$relationship];
    }

    protected function getRelationshipObjectsForTable(string $schema, string $table): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        if (!self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\relationship\AbstractRelationship::CLASS_SCOPE]->has($schema)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Specified table [%2$s] is not known for schema [%3$s].', get_class($this), $table, $schema));
        }
        if (!self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\relationship\AbstractRelationship::CLASS_SCOPE][$schema]->has($table)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Specified table [%2$s] is not known for schema [%3$s] for relationship [%4$s].', get_class($this), $schema, $table, $relationship));
        }
        return self::$database_registry[static::DATABASE_DRIVER][\Oroboros\core\abstracts\library\database\relationship\AbstractRelationship::CLASS_SCOPE][$schema][$table];
    }

    private function verifyFormat(): void
    {
        $expected = 'Oroboros\\core\\interfaces\\library\\database\\format\\DatabaseFormatInterface';
        $credentials = 'Oroboros\\core\\interfaces\\library\\database\\credentials\\CredentialsInterface';
        if (!in_array($expected, class_implements($this)) && !in_array($credentials, class_implements($this))) {
            throw new \Oroboros\core\exception\core\InvalidClassException(sprintf('Class [%1$s] is invalid. '
                        . 'Database libraries must implement an interface extending [%2$s].', get_class($this), $expected . ' | ' . $credentials));
        }
    }

    /**
     * Localizes the schema configuration on instantiation
     * @return void
     */
    private function initializeDatabaseConfig(): void
    {
        if (is_null(self::$config_default)) {
            // Fetch default one time statically
            self::$config_default = $this::app()->config()->get('schemas')->get('default');
        }
        if ($this::app()->config()->get('schemas')->has(static::DATABASE_DRIVER)) {
            // Fetch at instantiation
            $target = $this::app()->config()->get('schemas')->get(static::DATABASE_DRIVER);
        } else {
            // Use default
            $target = self::$config_default;
        }
        $this->config = $this->containerizeInto('Oroboros\\core\\library\\container\\ConfigCollection');
        foreach ($target as $key => $section) {
            $this->config[$key] = $this->containerizeInto('Oroboros\\core\\library\\container\\ConfigContainer', $section);
        }
    }

    private function initializeCacheConfig(): void
    {
        if (is_null(self::$cache_pool)) {
            self::$cache_pool = $this->containerizeInto('Oroboros\\core\\library\\container\\Container');
        }
        if (!self::$cache_pool->has(static::DATABASE_DRIVER)) {
            $cache_type = ($this->config->get('cache')->get('enabled') ? $this->config->get('cache')->get('type') : 'null');
            $connection = ($cache_type === null ? null : $this->config->get('cache')->get('connection'));
            self::$cache_pool[static::DATABASE_DRIVER] = $this->load('library', 'cache\\CacheStrategy')->evaluate($cache_type, $connection);
//            $ttl = $this->config->get('cache')->get('ttl');
        }
        $this->cache = self::$cache_pool[static::DATABASE_DRIVER];
    }

    private function initializeDatabaseRegistry(): void
    {
        if ($this::app()->config()->get('schemas')->has(static::DATABASE_DRIVER)) {
            $config = $this::app()->config()->get('schemas')->get(static::DATABASE_DRIVER);
        } else {
            $config = $this::app()->config()->get('schemas')->get('default');
        }
        if (is_null(self::$database_registry)) {
            // This should only be done one time. This is shared for all database classes.
            self::$database_registry = $this->containerizeInto('Oroboros\\core\\library\\container\\Collection');
        }
        if (!self::$database_registry->has(static::DATABASE_DRIVER)) {
            $this->initializeDatabaseRegistryKeys();
        }
        foreach (self::$database_registry as $key => $section) {
            if ($key === 'connection') {
                // Connections do not adhere to schema declarations.
                // Data layer matching is handled by their credential objects.
                continue;
            }
            if (!$section->has(static::DATABASE_DRIVER)) {
                $section[static::DATABASE_DRIVER] = $this->containerizeInto('Oroboros\\core\\library\\container\\Collection');
            }
            if (!$section->get(static::DATABASE_DRIVER)->has(static::DATABASE_ELEMENT_SCOPE)) {
                $section->get(static::DATABASE_DRIVER)[static::DATABASE_ELEMENT_SCOPE] = $this::containerize();
            }
        }
    }

    private function initializeDatabaseRegistryKeys(): void
    {
        self::$database_registry[static::DATABASE_DRIVER] = $this->containerizeInto('Oroboros\\core\\library\\container\\Collection');
        foreach ([
        \Oroboros\core\abstracts\library\database\connection\AbstractConnection::CLASS_SCOPE,
        \Oroboros\core\abstracts\library\database\schema\AbstractSchema::CLASS_SCOPE,
        \Oroboros\core\abstracts\library\database\table\AbstractTable::CLASS_SCOPE,
        \Oroboros\core\abstracts\library\database\column\AbstractColumn::CLASS_SCOPE,
        \Oroboros\core\abstracts\library\database\index\AbstractIndex::CLASS_SCOPE,
        \Oroboros\core\abstracts\library\database\relationship\AbstractRelationship::CLASS_SCOPE,
        \Oroboros\core\abstracts\library\database\trigger\AbstractTrigger::CLASS_SCOPE,
        \Oroboros\core\abstracts\library\database\view\AbstractView::CLASS_SCOPE,
        \Oroboros\core\abstracts\library\database\procedure\AbstractProcedure::CLASS_SCOPE,
        \Oroboros\core\abstracts\library\database\dbfunction\AbstractDatabaseFunction::CLASS_SCOPE,
        ] as $key) {
            $base = $this->containerizeInto('Oroboros\\core\\library\\container\\Collection');
            self::$database_registry[static::DATABASE_DRIVER][$key] = $this->containerize();
        }
    }

    private function initializeDatabaseCredentials()
    {
        $env = $this::app()->environment()->get('application');
        if (!$env->has('connections')) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. '
                        . 'Expected environment key [%2$s] is not defined.', get_class($this), 'connections'));
        }
        $connections = $env->get('connections');
    }
}
