<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\database\column;

/**
 * Abstract Database Column
 *
 * @author Brian Dayhoff
 */
abstract class AbstractColumn extends \Oroboros\core\abstracts\library\database\AbstractDatabaseLibrary implements \Oroboros\core\interfaces\library\database\column\ColumnInterface
{

    use \Oroboros\core\traits\database\DatabaseObjectUtilityTrait;

    const CLASS_SCOPE = 'column';

    /**
     * The database that the column belongs to
     * @var \Oroboros\core\interfaces\library\database\schema\SchemaInterface
     */
    private $schema = null;

    /**
     * The table that the column belongs to
     * @var \Oroboros\core\interfaces\library\database\table\TableInterface
     */
    private $table = null;

    /**
     * The data type of the column
     * @var string
     */
    private $type = null;

    /**
     * Access privileges for queries under the current connection
     * @var array
     */
    private $privileges = [];

    /**
     * The ordinal position of the column
     * @var int
     */
    private $position = null;
    private $indexes = null;
    private $relationships = null;

    /**
     * Any result values requested from the column.
     * These are bound when they exist,
     * so all similar values everywhere are consistent.
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $values = null;

    /**
     * Whether or not this column is the primary key of its table
     * @var bool
     */
    private $is_primary = false;

    /**
     * The column alias, if one exists.
     * Aliases are not retained, and should be set on a per-use case basis.
     * @var string
     */
    private $alias = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->verifyName();
        $this->initializeColumn();
    }

    public function __toString()
    {
        return sprintf('%4$s%3$s%1$s%5$s%2$s', static::LEFT_DELIMITER, static::RIGHT_DELIMITER, static::SEPARATOR, (string) $this->getTable(), $this->getName());
    }

    public function serialize()
    {
        $data = unserialize(parent::serialize());
        $data['connection'] = $this->getConnection()->getName();
        $data['schema'] = $this->schema->getName();
        $data['table'] = $this->table->getName();
        $data['type'] = $this->type;
        $data['privileges'] = $this->privileges;
        $data['position'] = $this->position;
        $data['indexes'] = $this->indexes;
        $data['relationships'] = $this->relationships;
        $data['is_primary'] = $this->is_primary;
        return serialize($data);
    }

    public function unserialize($data)
    {
        try {
            parent::unserialize($data);
            $data = unserialize($data);
            $this->setConnection($data['connection']);
            $this->schema = $this->verifySchema($data['schema']);
            $this->table = $this->verifyTable($data['table']);
            $this->type = $data['type'];
            $this->privileges = $data['privileges'];
            $this->position = $data['position'];
            $this->indexes = $data['indexes'];
            $this->relationships = $data['relationships'];
            $this->is_primary = $data['is_primary'];
        } catch (\Exception $e) {
            d('Error unserializing column', $e->getMessage(), $e->getTraceAsString());
            exit;
        }
    }

    public function getName(): string
    {
        return $this->getCommand();
    }

    public function getFullName(): string
    {
        $result = $this->getSchema()->getName()
            . static::SEPARATOR . $this->getTable()->getName()
            . static::SEPARATOR . $this->getName();
        return $result;
    }

    public function as(string $alias = null)
    {
        $this->alias = $alias;
        $key = $this->getSchema()->getName() . static::SEPARATOR . $this->getTable()->getName() . static::SEPARATOR . $this->getName();
        if (is_null($alias)) {
            if ($this->column_aliases->has($key)) {
                unset($this->column_aliases[$key]);
            }
        } else {
            $this->column_aliases[$key] = $alias;
        }
        return $this;
    }

    public function hasAlias(): bool
    {
        return !is_null($this->alias);
    }

    public function getAlias(): ?string
    {
        return $this->alias;
    }

    public function getSchema(): \Oroboros\core\interfaces\library\database\schema\SchemaInterface
    {
        return $this->schema;
    }

    public function getTable(): \Oroboros\core\interfaces\library\database\table\TableInterface
    {
        return $this->table;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getPosition(): int
    {
        return $this->position;
    }

    public function getPrivileges(): array
    {
        return $this->privileges;
    }

    public function hasPrivilege(string $privilege): bool
    {
        return in_array(strtolower($privilege), $this->privileges);
    }

    public function isPrimary(): bool
    {
        return $this->is_primary;
    }

    private function initializeColumn(): void
    {
        $this->setConnection($this->getArgument('connection'));
        $this->initializeTableAliases();
        $this->initializeColumnAliases();
        $this->values = $this->containerize();
        $this->parseDefinition();
        $this->parseIndexes();
        $this->parseRelationships();
        $this->checkPrimary();
        parent::addColumn($this);
    }

    private function parseDefinition(): void
    {
        $this->schema = $this->verifySchema($this->getArgument(static::INFORMATION_SCHEMA_SELECTOR));
        $this->table = $this->verifyTable($this->getArgument(static::INFORMATION_TABLE_SELECTOR));
        $this->type = $this->getArgument(static::INFORMATION_TYPE_SELECTOR);
        $this->position = $this->getArgument(static::INFORMATION_POSITION_SELECTOR);
        foreach (explode(',', $this->getArgument(static::INFORMATION_PRIVILEGES_SELECTOR)) as $privilege) {
            $this->privileges[] = trim(strtolower($privilege), ' ');
        }
    }

    private function parseIndexes(): void
    {
        
    }

    private function parseRelationships(): void
    {
        
    }

    private function checkPrimary(): void
    {
        
    }

    private function verifyName(): void
    {
        if (is_null($this->getCommand())) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Column must declare a name.', get_class($this)));
        }
    }
}
