<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\database\query;

/**
 * Abstract Clause
 * Represents a WHERE/ON clause.
 * Clauses may be composite.
 * Sub-clauses will be evaluated in parentheses.
 * @author Brian Dayhoff
 */
abstract class AbstractClause extends \Oroboros\core\abstracts\library\database\AbstractDatabaseLibrary implements \Oroboros\core\interfaces\library\database\query\ClauseInterface
{

    use \Oroboros\core\traits\database\DatabaseObjectUtilityTrait;

    const CLASS_SCOPE = 'clause';
    const FLAG_RAW = 'raw';
    const FLAG_PREPARE = 'prepare';
    const FLAG_COMPACT = 'compact';
    const FLAG_SUBCLAUSE = 'subclause';

    private static $valid_types = [
        'WHERE',
        'AND',
        'OR',
        'ON'
    ];
    private static $valid_mode_flags = [
        self::FLAG_RAW,
        self::FLAG_PREPARE
    ];
    private static $valid_operands = [
        '=',
        '>',
        '<',
        '>=',
        '<=',
        '<>',
        'IS NULL',
        'IS NOT NULL',
        'LIKE',
        'NOT LIKE',
        'IN',
        'NOT IN',
    ];

    /**
     * Alias names of tables
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface 
     */
    private $table_aliases = null;

    /**
     * Alias names of columns
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $column_aliases = null;

    /**
     * Container of known column objects
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $columns = null;

    /**
     * The clause type
     * @var string
     */
    private $type = null;

    /**
     * The comparator operand that applies to the clause
     * @var string
     */
    private $operand = null;

    /**
     * Container of bindings to apply to the clause
     * @var Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $bindings = null;

    /**
     * Container of conditions to apply to the clause
     * @var Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $conditions = null;

    /**
     * Whether or not the clause represents a compound clause.
     * Compound clauses are represented in parentheses, and resolve
     * fully prior to the outer scope evaluating them
     * @var bool
     */
    private $compound_clause = false;

    /**
     * The statement, with prepared statement binding markers
     * @var string
     */
    private $statement = null;

    /**
     * The raw statement, without prepared statement binding markers
     * @var string
     */
    private $raw_statement = null;

    /**
     * Designates that the comparison is between two columns
     * @var bool
     */
    private $is_column_comparison = false;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeBindings();
        $this->initializeConditions();
        $this->initializeAliases();
        $this->initializeColumns();
        $this->setConnection($this->getArgument('connection'));
        $this->initializeTableAliases();
        $this->initializeColumnAliases();
        $this->verifyType();
        $this->verifyOperand();
        $this->verifyComparator();
        if ($this->hasFlag(self::FLAG_SUBCLAUSE) && $this->getFlag(self::FLAG_SUBCLAUSE)) {
            $this->compound_clause = true;
        }
    }

    public function __toString()
    {
        return $this->getStatement();
    }

    public function serialize()
    {
        $data = unserialize(parent::serialize());
        $data['connection'] = $this->getConnection()->getName();
        $data['type'] = $this->type;
        $data['operand'] = $this->operand;
        $data['compound_clause'] = $this->compound_clause;
        $data['bindings'] = $this->bindings;
        $data['conditions'] = $this->conditions;
        $data['table_aliases'] = $this->table_aliases;
        $data['column_aliases'] = $this->column_aliases;
        return serialize($data);
    }

    public function unserialize($data)
    {
        parent::unserialize($data);
        $data = unserialize($data);
        $this->setConnection($data['connection']);
        $this->type = $data['type'];
        $this->operand = $data['operand'];
        $this->compound_clause = $data['compound_clause'];
        $this->bindings = $data['bindings'];
        $this->conditions = $data['conditions'];
        $this->table_aliases = $data['table_aliases'];
        $this->column_aliases = $data['column_aliases'];
    }

    public function getSchema()
    {
        return $this->getConnection()->schema();
    }

    /**
     * Creates an additional and clause. If the object is not already
     * designated as compound, it will be marked compound.
     * @param type $subject
     * @param string $operand
     * @param type $comparator
     * @return $this (method chainable)
     */
    public function and($subject, string $operand, $comparator = null)
    {
        $this->compound_clause = true;
        $this->addCondition($subject, $operand, $comparator, 'AND');
        return $this;
    }

    /**
     * Creates an additional or clause. If the object is not
     * already designated as compound, it will be marked compound.
     * @param type $subject
     * @param string $operand
     * @param type $comparator
     * @return $this (method chainable)
     */
    public function or($subject, string $operand, $comparator = null)
    {
        $this->compound_clause = true;
        $this->addCondition($subject, $operand, $comparator, 'OR');
        return $this;
    }

    /**
     * Creates a nested subclause and returns the generated subclause to work with further.
     * @param string $type
     * @param type $subject
     * @param string $operand
     * @param type $comparator
     */
    public function subclause(string $type, $subject, string $operand, $comparator = null)
    {
        $class = get_class($this);
        $args = [
            'subject' => $subject,
            'operand' => $operand
        ];
        if (!is_null($comparator)) {
            $args['comparator'] = $comparator;
        }
        $flags = $this->getFlags();
        $flags[self::FLAG_SUBCLAUSE] = true;
        $object = new $class($type, $args, $this->getFlags());
        $this->addCondition($object);
        return $object;
    }

    /**
     * Gets the clause statement with bindings for prepared statements
     * @return string
     */
    public function getStatement(): string
    {
        $this->buildStatement();
        return $this->statement;
    }

    /**
     * Gets the clause statement as raw sql that can be run directly
     * @return string
     */
    public function getRaw(): string
    {
        $this->buildStatement();
        return $this->raw_statement;
    }

    public function getBindings(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        return $this->bindings;
    }

    private function resetStatement(): void
    {
        $this->statement = null;
        $this->raw_statement = null;
        $this->bindings = $this::containerize();
    }

    private function buildStatement(): void
    {
        $this->resetStatement();
        $result = '';
        foreach ($this->conditions as $key => $condition) {
            if (is_object($condition) && ($condition instanceof \Oroboros\core\interfaces\library\database\query\ClauseInterface)) {
                $result .= ' ' . (string) $condition;
            } else {
                $result .= ' ';
                if ($condition->has('type')) {
                    $result .= $condition['type'] . ' ';
                }
                $result .= sprintf('%3$s%1$s%4$s %2$s', trim((string) implode(static::LEFT_DELIMITER . static::SEPARATOR . static::RIGHT_DELIMITER, explode(static::SEPARATOR, $condition['subject'])), static::LEFT_DELIMITER . static::RIGHT_DELIMITER), $condition['operand'], static::LEFT_DELIMITER, static::RIGHT_DELIMITER);
                if (!is_null($condition['comparator'])) {
                    $column_expected = 'Oroboros\\core\\interfaces\\library\\database\\column\\ColumnInterface';
                    if (is_object($condition['comparator']) && ($condition['comparator'] instanceof $column_expected)) {
                        // Perform a column comparison. Do not apply binds.
                        $result .= ' ' . (string) $condition['comparator'];
                    } elseif (is_iterable($condition['comparator'])) {
                        $comparators = [];
                        foreach ($condition['comparator'] as $comparator_key => $comparator_value) {
                            $key = str_replace(static::SEPARATOR, '_', str_replace(static::LEFT_DELIMITER, null, str_replace(static::RIGHT_DELIMITER, null, $condition['subject']))) . '_' . count($this->bindings);
                            $this->bindings[$key] = $comparator_value;
                            $comparators[] = sprintf(':%1$s', $key);
                        }
                        $result .= ' (' . implode(', ', $comparators) . ')';
                    } else {
                        $key = str_replace(static::SEPARATOR, '_', str_replace(static::LEFT_DELIMITER, null, str_replace(static::RIGHT_DELIMITER, null, $condition['subject']))) . '_' . count($this->bindings);
                        $this->bindings[$key] = $condition['comparator'];
                        $result .= sprintf(' :%1$s', $key);
                    }
                }
            }
        }
        $result = trim($result);
        if ($this->compound_clause) {
            $result = '(' . $result . ')';
        }
        $result = $this->type . ' ' . $result;
        $this->statement = $result;
        $this->raw_statement = $result;
        foreach ($this->bindings as $key => $value) {
            $this->raw_statement = str_replace(':' . $key, (is_null($value) ? 'NULL' : (is_numeric($value) ? $value : sprintf('"%1$s"', $value))), $this->raw_statement);
        }
    }

    private function addCondition($subject, string $operand = null, $comparator = null, $type = null): void
    {
        $expected = get_class($this);
        if (is_null($operand) && !(is_object($subject) && ( $subject instanceof $expected )
            )) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'No operand provided and subject is not a valid subclause.', get_class($this)));
        }
        $key = 'condition_' . count($this->conditions);
        if ($subject instanceof $expected) {
            $this->conditions[$key] = $subject;
        } else {
            $this->conditions[$key] = $this::containerize([
                    'subject' => $subject,
                    'operand' => $operand,
                    'comparator' => $comparator
            ]);
            if (!is_null($type)) {
                $this->conditions[$key]['type'] = $type;
            }
        }
    }

    private function initializeBindings(): void
    {
        $this->bindings = $this->containerize();
    }

    private function initializeConditions(): void
    {
        $this->conditions = $this->containerize();
    }

    private function initializeAliases(): void
    {
        if ($this->hasArgument('table_aliases')) {
            $this->table_aliases = $this->getArgument('table_aliases');
        } else {
            $this->table_aliases = $this::containerize();
        }
        if ($this->hasArgument('column_aliases')) {
            $this->column_aliases = $this->getArgument('column_aliases');
        } else {
            $this->column_aliases = $this::containerize();
        }
    }

    private function initializeColumns(): void
    {
        if ($this->hasArgument('columns')) {
            $this->columns = $this->getArgument('columns');
        } else {
            $this->columns = $this::containerize();
        }
    }

    private function verifyType(): void
    {
        if (!in_array(strtoupper($this->getCommand()), self::$valid_types)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided clause type [%2$s] is not valid. '
                        . 'Valid clause types are [%3$s] (case insensitive)', get_class($this), $this->getCommand(), implode(', ', self::$valid_types)));
        }
        $this->type = strtoupper($this->getCommand());
    }

    private function verifySubject(): void
    {
        if (!$this->hasArgument('subject')) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Expected argument [%2$s] was not provided.', get_class($this), 'subject'));
        }
    }

    private function verifyOperand(): void
    {
        if (!$this->hasArgument('operand')) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Expected argument [%2$s] was not provided.', get_class($this), 'operand'));
        }
        if (!in_array(strtoupper($this->getArgument('operand')), self::$valid_operands)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided argument [%2$s] is invalid. '
                        . 'Valid arguments for [%2$s] are [%3$s] (case insensitive).', get_class($this), 'operand', implode(', ', self::$valid_operands)));
        }
        $this->operand = strtoupper($this->getArgument('operand'));
    }

    private function verifyComparator(): void
    {
        switch ($this->operand) {
            case 'IS NULL':
            case 'IS NOT NULL':
                // verify as null
                $this->verifyNullComparator();
                break;
            case 'IN':
            case 'NOT IN':
                // verify as traversable
                $this->verifyTraversableComparator();
                break;
            case 'LIKE':
            case 'NOT LIKE':
                // verify as wildcard
                $this->verifyWildcardComparator();
                break;
            default:
                // verify as strict comparison
                $this->verifyComparisonComparator();
                break;
        }
    }

    private function verifyNullComparator(): void
    {
        if ($this->hasArgument('comparator') && !is_null($this->getArgument('comparator'))) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided operand [%2$s] requires a null comparator.', get_class($this), $this->operand));
        }
        $this->addCondition($this->getArgument('subject'), $this->operand);
    }

    private function verifyTraversableComparator(): void
    {
        if (!$this->hasArgument('comparator') || !is_iterable($this->getArgument('comparator'))) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided operand [%2$s] requires a traversable comparator.', get_class($this), $this->operand));
        }
        $this->addCondition($this->getArgument('subject'), $this->operand, $this->getArgument('comparator'));
    }

    private function verifyWildcardComparator(): void
    {
        if (!$this->hasArgument('comparator') || !(is_string($this->getArgument('comparator') || (is_object($this->getArgument('comparator') && method_exists('__toString', $this->getArgument('comparator'))))))) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided operand [%2$s] requires a string comparator, or an object implementing __toString.', get_class($this), $this->operand));
        }
        $this->addCondition($this->getArgument('subject'), $this->operand, $this->getArgument('comparator'));
    }

    private function verifyComparisonComparator(): void
    {
        $err = false;
        if (!$this->hasArgument('comparator')) {
            $err = true;
        } elseif (!is_scalar($this->getArgument('comparator'))) {
            // Must be an acceptable database column object or stringable object.
            // Otherwise error.
            if (is_object($this->getArgument('comparator')) && (method_exists('__toString', $this->getArgument('comparator') || (
                    $this->getArgument('comparator') instanceof \Oroboros\core\interfaces\library\database\column\ColumnInterface
                )))) {
                // Valid.
            } else {
                // Not valid.
                $err = true;
            }
        }
        if ($err) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided operand [%2$s] requires a scalar value comparator, or acceptable database class.', get_class($this), $this->operand));
        }
        $this->addCondition($this->getArgument('subject'), $this->operand, $this->getArgument('comparator'));
    }
}
