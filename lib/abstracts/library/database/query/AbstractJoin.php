<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\database\query;

/**
 * Abstract Join
 * Represents a Join against a table or subquery
 * 
 * @author Brian Dayhoff
 */
abstract class AbstractJoin extends \Oroboros\core\abstracts\library\database\AbstractDatabaseLibrary implements \Oroboros\core\interfaces\library\database\query\JoinInterface
{

    use \Oroboros\core\traits\database\DatabaseObjectUtilityTrait;

    const CLASS_SCOPE = 'join';
    const FLAG_RAW = 'raw';
    const FLAG_PREPARE = 'prepare';
    const FLAG_COMPACT = 'compact';

    private static $valid_types = [
        null,
        'INNER',
        'LEFT',
        'RIGHT'
    ];
    private static $valid_mode_flags = [
        self::FLAG_RAW,
        self::FLAG_PREPARE
    ];

    /**
     * The schema associated with the join.
     * Should reflect the schema the referenced table lives in,
     * or the schema reflected by the primary query.
     * @var Oroboros\core\interfaces\library\database\schema\SchemaInterface|null
     */
    private $schema = null;

    /**
     * The subject of the join.
     * May be a table object or a query object.
     * @var \Oroboros\core\interfaces\library\database\table|TableInterface|\Oroboros\core\interfaces\library\database\query\QueryInterface
     */
    private $subject = null;

    /**
     * Designates whether the subject is a table or a subquery.
     * @var string [table|query]
     */
    private $subject_type = null;

    /**
     * The alias for the join subject, if any.
     * @note subquery joins MUST have an alias.
     * @var string|null
     */
    private $alias = null;

    /**
     * Alias names of tables
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface 
     */
    private $table_aliases = null;

    /**
     * Alias names of columns
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $column_aliases = null;

    /**
     * Container of known column objects
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $columns = null;

    /**
     * A container of any ON|OR|AND clauses applied to the join.
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $conditions = null;

    /**
     * A container of any parameter bindings applied to clauses for prepared statements.
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $bindings = null;

    /**
     * The clause type
     * @var string
     */
    private $type = null;

    /**
     * Represents the sql statement with parameter bindings for prepared statements
     * @var string
     */
    private $statement = null;

    /**
     * Represents the raw sql statement, with literal values instead of parameter bindings
     * @var string
     */
    private $raw_statement = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeBindings();
        $this->initializeConditions();
        $this->initializeTableAliases();
        $this->initializeColumnAliases();
        $this->initializeColumns();
        $this->setConnection($this->getArgument('connection'));
        $this->initializeTableAliases();
        $this->initializeColumnAliases();
        $this->verifyType();
        $this->verifySubject();
        $this->verifyAlias();
        $this->verifyConditions();
    }

    /**
     * Casts the join (and corresponding conditionals) to a string
     * @return string
     */
    public function __toString(): string
    {
        return $this->getStatement();
    }

    public function serialize()
    {
        $data = unserialize(parent::serialize());
        $data['connection'] = $this->getConnection()->getName();
        $data['schema'] = $this->schema;
        $data['type'] = $this->type;
        $data['subject'] = $this->subject;
        $data['subject_type'] = $this->subject_type;
        $data['alias'] = $this->alias;
        $data['conditions'] = $this->conditions;
        $data['bindings'] = $this->bindings;
        $data['columns'] = $this->columns;
        $data['table_aliases'] = $this->table_aliases;
        $data['column_aliases'] = $this->column_aliases;
        return serialize($data);
    }

    public function unserialize($data)
    {
        parent::unserialize($data);
        $data = unserialize($data);
        $this->setConnection($data['connection']);
        $this->schema = $data['schema'];
        $this->type = $data['type'];
        $this->subject = $data['subject'];
        $this->subject_type = $data['subject_type'];
        $this->alias = $data['alias'];
        $this->conditions = $data['conditions'];
        $this->bindings = $data['bindings'];
        $this->columns = $data['columns'];
        $this->table_aliases = $data['table_aliases'];
        $this->column_aliases = $data['column_aliases'];
    }

    /**
     * Returns the currently scoped schema for the join, if any exists.
     * @return string|null
     */
    public function getSchema(): ?\Oroboros\core\interfaces\library\database\schema\SchemaInterface
    {
        return $this->schema;
    }

    /**
     * Creates an initial constraint clause for the join
     * @param type $column
     * @param string $operand
     * @param type $comparator
     * @return Oroboros\core\interfaces\library\database\query\ClauseInterface
     */
    public function on($column, string $operand = null, $comparator = null): \Oroboros\core\interfaces\library\database\query\ClauseInterface
    {
        if (!empty($this->conditions->toArray())) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. '
                        . 'Initial on statement already exists. '
                        . 'Use the [and|or] methods to add additional clauses.', get_class($this)));
        }
        $condition = $this->load('library', static::QUERY_CONDITION_CLASS, 'ON', [
            'connection' => $this->getArgument('connection'),
            'subject' => $column,
            'operand' => $operand,
            'comparator' => $comparator
            ], $this->getFlags());
        $key = 'clause_' . count($this->conditions);
        $this->conditions[$key] = $condition;
        return $condition;
    }

    /**
     * Creates an additional AND constraint clause for the join
     * @param type $column
     * @param string $operand
     * @param type $comparator
     * @return \Oroboros\core\interfaces\library\database\query\ClauseInterface
     */
    public function and($column, string $operand = null, $comparator = null): \Oroboros\core\interfaces\library\database\query\ClauseInterface
    {
        if (empty($this->conditions->toArray())) {
            return $this->on($column, $operand, $comparator);
        }
        $condition = $this->load('library', static::QUERY_CONDITION_CLASS, 'AND', [
            'connection' => $this->getArgument('connection'),
            'subject' => $column,
            'operand' => $operand,
            'comparator' => $comparator
            ], $this->getFlags());
        $key = 'clause_' . count($this->conditions);
        $this->conditions[$key] = $condition;
        return $condition;
    }

    /**
     * Creates an additional OR constraint clause for the join
     * @param type $column
     * @param string $operand
     * @param type $comparator
     * @return \Oroboros\core\interfaces\library\database\query\ClauseInterface
     */
    public function or($column, string $operand = null, $comparator = null): \Oroboros\core\interfaces\library\database\query\ClauseInterface
    {
        if (empty($this->conditions->toArray())) {
            return $this->on($column, $operand, $comparator);
        }
        $condition = $this->load('library', static::QUERY_CONDITION_CLASS, 'OR', [
            'connection' => $this->getArgument('connection'),
            'subject' => $column,
            'operand' => $operand,
            'comparator' => $comparator
            ], $this->getFlags());
        $key = 'clause_' . count($this->conditions);
        $this->conditions[$key] = $condition;
        return $condition;
    }

    /**
     * Sets the subject alias, or removes it if no argument is passed.
     * @param string $alias
     * @return $this (method chainable)
     * @note Subquery joins MUST have an alias.
     */
    public function as(string $alias = null): \Oroboros\core\interfaces\library\database\query\JoinInterface
    {
        $this->alias = $alias;
        if ($this->subject_type === 'table') {
            $this->subject->as($this->alias);
        }
        return $this;
    }

    /**
     * Returns the alias for the join, if any (or null if none)
     * @return string|null
     */
    public function getAlias(): ?string
    {
        return $this->alias;
    }

    /**
     * Returns a boolean determination as to whether the join has an alias
     * @return bool
     */
    public function hasAlias(): bool
    {
        return !is_null($this->alias);
    }

    /**
     * Removes the alias if one exists
     * @return $this (method chainable)
     */
    public function withoutAlias(): \Oroboros\core\interfaces\library\database\query\JoinInterface
    {
        $this->alias = null;
        return $this;
    }

    public function getStatement(): string
    {
        $this->buildStatement();
        return $this->statement;
    }

    public function getRaw(): string
    {
        if (is_null($this->raw_statement)) {
            $this->buildStatement();
        }
        return $this->raw_statement;
    }

    public function getBindings(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        if (empty($this->bindings->toArray())) {
            $this->buildStatement();
        }
        return $this->bindings;
    }

    private function resetStatement(): void
    {
        $this->statement = null;
        $this->raw_statement = null;
        $this->bindings = $this::containerize();
    }

    private function buildStatement(): void
    {
        $this->resetStatement();
        $statement = '';
        if (!is_null($this->type)) {
            $statement .= $this->type . ' ';
        }
        $statement .= 'JOIN';
        switch ($this->subject_type) {
            case 'subquery':
                $statement = $this->buildSubqueryJoinStatement($statement, $this->subject);
                break;
            case 'table':
                $statement = $this->buildTableJoinStatement($statement, $this->subject);
                break;
            default:
                // Someone didn't set the subject type. Derp.
                throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. '
                            . 'No recognized subject type definition. '
                            . 'Cannot build join statement.', get_class($this)));
        }
        // Parse conditions
        foreach ($this->conditions as $key => $clause) {
            $stub = $clause->getStatement();
            foreach ($clause->getBindings() as $key => $binding) {
                $new_key = 'clause_' . $key . '_param_' . count($this->bindings);
                $this->bindings[$new_key] = $binding;
                $stub = str_replace(':' . $key, ':' . $new_key, $stub);
                $statement .= sprintf('%2$s%1$s', $stub, (($this->hasFlag(self::FLAG_COMPACT) && $this->getFlag(self::FLAG_COMPACT)) ? ' ' : PHP_EOL . '    '));
            }
        }
        $this->statement = $statement;
        $this->raw_statement = $statement;
        foreach ($this->bindings as $key => $value) {
            $this->raw_statement = str_replace(':' . $key, (is_null($value) ? 'NULL' : (is_numeric($value) ? $value : sprintf('"%1$s"', $value))), $this->raw_statement);
        }
    }

    private function buildSubqueryJoinStatement(string $statement, \Oroboros\core\interfaces\library\database\query\QueryInterface $subject): string
    {
        if (!$this->hasAlias()) {
            throw new \Oroboros\core\exception\ErrorException('Error encountered in [%1$s]. '
                    . 'Subquery joins must declare an alias. No alias currently defined.', get_class($this));
        }
        $query = $subject->getStatement();
        foreach ($subject->getBindings() as $key => $binding) {
            $new_key = 'subquery_' . $key . '_param_' . count($this->bindings);
            $this->bindings[$new_key] = $binding;
            $query = str_replace(':' . $key, ':' . $new_key, $query);
        }
        $result = sprintf('%1$s (%2$s) AS %4$s%3$s%5$s', $statement, $query, $this->getAlias(), static::LEFT_DELIMITER, static::RIGHT_DELIMITER);
        return $result;
    }

    private function buildTableJoinStatement(string $statement, \Oroboros\core\interfaces\library\database\table\TableInterface $subject): string
    {
        if ($this->subject_type === 'table') {
            $base = sprintf('%1$s%4$s%2$s%3$s%1$s%5$s%2$s', static::LEFT_DELIMITER, static::RIGHT_DELIMITER, static::SEPARATOR, $subject->getSchema()->getName(), $subject->getName());
        } else {
            $base = (string) $subject;
        }
        $result = sprintf('%1$s %2$s', $statement, $base);
        if ($this->hasAlias()) {
            $result .= sprintf(' AS %2$s%1$s%3$s', $this->getAlias(), static::LEFT_DELIMITER, static::RIGHT_DELIMITER);
        } elseif ($subject->hasAlias()) {
            $result .= sprintf(' AS %2$s%1$s%3$s', $subject->getAlias(), static::LEFT_DELIMITER, static::RIGHT_DELIMITER);
        }
        foreach ($this->conditions as $key => $clause) {
            $result .= sprintf('%1$s%2$s', (($this->hasFlag(static::FLAG_COMPACT) && $this->getFlag(static::FLAG_COMPACT)) ? ' ' : PHP_EOL . '    '), (string) $clause);
        }
        return $result;
    }

    private function initializeBindings(): void
    {
        $this->bindings = $this::containerize();
    }

    private function initializeConditions(): void
    {
        $this->conditions = $this::containerize();
    }

    private function initializeColumns(): void
    {
        if ($this->hasArgument('columns')) {
            $this->columns = $this->getArgument('columns');
        } else {
            $this->columns = $this::containerize();
        }
    }

    private function verifySubject(): void
    {
        if (!$this->hasArgument('subject')) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. No subject provided.'));
        }
        $subject = $this->getArgument('subject');
        $expected_as_table = 'Oroboros\\core\\interfaces\\library\\database\\table\\TableInterface';
        $expected_as_query = 'Oroboros\\core\\interfaces\\library\\database\\query\\QueryInterface';
        if (is_string($subject) || (is_object($subject) && ($subject instanceof $expected_as_table))) {
            $this->subject_type = 'table';
            $this->subject = $this->verifyTable($subject);
            return;
        } elseif (is_object($subject) && ($subject instanceof $expected_as_query)) {
            $this->subject_type = 'subquery';
        } else {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided argument [%2$s] is not a valid table definition', get_class($this), 'table'));
        }
        $this->subject = $subject;
        $this->schema = $subject->getSchema();
        return;
    }

    private function verifyAlias(): void
    {
        
    }

    private function verifyConditions(): void
    {
        if ($this->hasArgument('conditions')) {
            foreach ($this->getArgument('conditions') as $condition) {
                $this->verifyCondition($condition);
            }
        }
    }

    private function verifyCondition($condition): void
    {
        
    }

    private function verifyType(): void
    {
        if (!is_null($this->getCommand()) && !in_array(strtoupper($this->getCommand()), self::$valid_types)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided type is not valid. '
                        . 'Valid types are [%2$s] (case insensitive).', get_class($this), implode(', ', self::$valid_types)));
        }
        $this->type = $this->getCommand();
    }
}
