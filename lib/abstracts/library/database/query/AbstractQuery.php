<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\database\query;

/**
 * Abstract Database Query
 * This class provides a multipurpose general query object
 * that is compatible with multiple database layers.
 * 
 * Child classes must implement a FormatInterface that details
 * constants associated with the given database layer,
 * although the underlying internals are written to conform
 * to pure standard SQL, and should work regardless of which
 * SQL database type you are using.
 *
 * @author Brian Dayhoff
 */
abstract class AbstractQuery extends \Oroboros\core\abstracts\library\database\AbstractDatabaseLibrary implements \Oroboros\core\interfaces\library\database\query\QueryInterface
{

    use \Oroboros\core\traits\StringUtilityTrait;
    use \Oroboros\core\traits\database\DatabaseObjectUtilityTrait;

    const CLASS_SCOPE = 'query';
    const FLAG_DISTINCT = 'distinct';           // Flags the query to use a DISTINCT clause, if possible for the current statement.
    const FLAG_EXISTS = 'exists';               // Flags the query to use an EXISTS clause, if possible for the current statement. This will return a boolean value typically.
    const FLAG_NOTEXISTS = 'not-exists';        // Flags the query to use a IF NOT EXISTS clause, if possible for the current statement
    const FLAG_ONDUP = 'on-duplicate';          // Flags the query to take an additional action on duplicate key detection (spoofed for some database types)
    const FLAG_TEMPORARY = 'temp';              // Flags the query to use a TEMPORARY clause on CREATE TABLE queries, if possible for the current statement.
    const FLAG_IGNORE = 'ignore';               // Flags the query to use IGNORE for inserts and updates, if possible for the current statement
    const FLAG_COMPACT = 'compact';             // Removes extraneous line breaks and padding from the statement

    private static $valid_types = [
        'ALTER',
        'CREATE',
        'DROP',
        'SELECT', // done
        'INSERT',
        'UPDATE',
        'DELETE',
    ];

    /**
     * The query type (select, insert, update, delete, etc)
     * @var string
     */
    private $type = null;

    /**
     * The alias for the base table, if any
     * @var string|null
     */
    private $alias = null;

    /**
     * The object representing the base table
     * @var \Oroboros\core\interfaces\library\database\table\TableInterface
     */
    private $table = null;

    /**
     * The schema object that contains the base table
     * @var \Oroboros\core\interfaces\library\database\schema\SchemaInterface
     */
    private $schema = null;

    /**
     * Container of fixed value declarations used as selectors in the query
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $fixedvals = null;

    /**
     * Container of update value declarations used as selectors in the query
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $update_values = null;

    /**
     * Container of insert value declarations used as selectors in the query
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $insert_values = null;

    /**
     * Container of column ordering, which designates the return order for selectors
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $column_order = null;

    /**
     * Container of ClauseInterface objects used to establish WHERE/AND/OR clauses for the query
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $where = null;

    /**
     * Container of JoinInterface objects used to establish join clauses in the query
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $joins = null;

    /**
     * Container of orderby statements, used to establish the ORDER BY clause, if any
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $order = null;

    /**
     * Container of groupby statements, used to establish the GROUP BY clause, if any
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $grouping = null;

    /**
     * Container of limit statements, used to establish the LIMIT clause, if any
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $limit = null;

    /**
     * Container of union statements (QueryInterface), used to establish UNION statements, if any
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $unions = null;

    /**
     * Container of subqueries (QueryInterface), used to contain any subqueries used in the root query
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $subqueries = null;

    /**
     * Container of parameter bindings. Used for mapping values to prepared statements
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $bindings = null;

    /**
     * The raw sql string used to construct the PDO statement.
     * @var string
     */
    private $sql = null;

    /**
     * The PDO statement used to execute the query
     * @var \PDOStatement
     */
    private $statement = null;

    /**
     * The raw sql, as it would be run natively in the SQL command line.
     * Does not contain parameter bindings. This is only used for debugging
     * and is never actually run, although it is returned in error messages
     * for readability and developer evaluation of the real values being executed.
     * @var string
     */
    private $raw_statement = null;

    /**
     * Container of results returned from the query, if applicable
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $results = null;

    /**
     * Container of error messages encountered during query execution.
     * Resets on each subsequent execution.
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $errors = null;

    /**
     * Count of rows affected after last execution.
     * Resets on each subsequent execution.
     * @var int
     */
    private $rowcount = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->verifyName();
        $this->initializeQuery();
    }

    public function serialize()
    {
        $data = unserialize(parent::serialize());
        $data['type'] = $this->type;
        $data['alias'] = $this->alias;
        $data['table'] = $this->table;
        $data['table_aliases'] = $this->table_aliases;
        $data['schema'] = $this->schema;
        $data['columns'] = $this->columns;
        $data['column_order'] = $this->column_order;
        $data['column_aliases'] = $this->column_aliases;
        $data['where'] = $this->where;
        $data['joins'] = $this->joins;
        $data['order'] = $this->order;
        $data['grouping'] = $this->grouping;
        $data['limit'] = $this->limit;
        $data['unions'] = $this->unions;
        $data['subqueries'] = $this->subqueries;
        $data['update_values'] = $this->update_values;
        $data['insert_values'] = $this->insert_values;
        return serialize($data);
    }

    public function unserialize($data)
    {
        parent::unserialize($data);
        $data = unserialize($data);
        $this->type = $data['type'];
        $this->alias = $data['alias'];
        $this->table = $data['table'];
        $this->table_aliases = $data['table_aliases'];
        $this->schema = $data['schema'];
        $this->columns = $data['columns'];
        $this->column_order = $data['column_order'];
        $this->column_aliases = $data['column_aliases'];
        $this->where = $data['where'];
        $this->joins = $data['joins'];
        $this->order = $data['order'];
        $this->grouping = $data['grouping'];
        $this->limit = $data['limit'];
        $this->unions = $data['unions'];
        $this->subqueries = $data['subqueries'];
        $this->update_values = $data['update_values'];
        $this->insert_values = $data['insert_values'];
    }

    /**
     * Returns the schema name, if a schema is defined
     * @return \Oroboros\core\interfaces\library\database\schema\SchemaInterface|null
     */
    public function getSchema(): ?\Oroboros\core\interfaces\library\database\schema\SchemaInterface
    {
        return $this->schema;
    }

    /**
     * Returns the base table name, if a table is defined
     * @return \Oroboros\core\interfaces\library\database\table\TableInterface|null
     */
    public function getTable(): ?\Oroboros\core\interfaces\library\database\table\TableInterface
    {
        return $this->table;
    }

    /**
     * Executes the current query
     * @return $this (method chainable)
     * @throws \PDOException If the query is not valid or the database cannot be reached
     */
    public function execute()
    {
        $this->buildResults();
        return $this;
    }

    /**
     * Returns the result set from the last executed query.
     * @return \Oroboros\core\interface\library\container\ContainerInterface
     */
    public function fetch()
    {
        if (is_null($this->statement)) {
            $this->buildResults();
        }
        return $this->results;
    }

    /**
     * Returns a container with detailed information about any errors encountered from the last execution.
     * @return \Oroboros\core\interface\library\container\ContainerInterface
     */
    public function errors()
    {
        return $this->errors;
    }

    /**
     * Returns the count of rows affected by the last execution.
     * @return int
     */
    public function affected(): int
    {
        return $this->rowcount;
    }

    /**
     * Sets the schema and table to run the query against.
     * @param string $schema
     * @param string $table
     * @return $this (method chainable)
     */
    public function from(string $schema, string $table)
    {
        $this->schema = $this->verifySchema($schema);
        $this->table = $this->verifyTable($table);
        if (!is_null($this->alias)) {
            $this->table_aliases[$this->schema->getName() . static::SEPARATOR . $this->table->getName()] = $this->alias;
        }
        return $this;
    }

    /**
     * Sets the alias for the base table,
     * or clears it if no parameter is passed.
     * Can be used as a prefix on column
     * names for selectors, joins and clauses.
     * @param string $alias
     * @return $this (method chainable)
     */
    public function as(string $alias = null)
    {
        $this->alias = $alias;
        if (!is_null($this->table) && !is_null($this->schema)) {
            $this->table_aliases[$this->schema->getName() . static::SEPARATOR . $this->table->getName()] = $alias;
        }
        return $this;
    }

    /**
     * Sets provided values for insert statements
     * @param array $values
     * return $this (method chainable)
     */
    public function values(array $values)
    {
        $key = 'insert_values_' . count($this->insert_values);
        $this->insert_values[$key] = $values;
        return $this;
    }

    /**
     * Sets provided values for update statements
     * @param string $column
     * @param scalar|null $values
     * @return $this (method chainable)
     */
    public function set(string $column, $value)
    {
        $this->update_values[$column] = $value;
        return $this;
    }

    /**
     * Returns the alias of the base table, if one exists
     * @return string|null
     */
    public function getAlias(): ?string
    {
        if ($this->table_aliases->has($this->schema->getName() . static::SEPARATOR . $this->table->getName())) {
            return $this->table_aliases[$this->schema->getName() . static::SEPARATOR . $this->table->getName()];
        }
        return $this->alias;
    }

    /**
     * Adds a where clause to the query.
     * This may be repeated multiple times.
     * Only top-level where clauses are set with this method.
     * 
     * @param scalar|string|\Oroboros\core\interfaces\library\database\column\ColumnInterface $subject The column object, column name string, column alias string, or scalar value to compare
     * @param string $operand (optional) The operand [eg: =, >=, <=, <>, IN, NOT IN, LIKE, NOT LIKE, IS NULL, IS NOT NULL, etc]
     * @param scalar|string|\Oroboros\core\interfaces\library\database\column\ColumnInterface $comparator (optional) The comparison operator. Can be any logical operator, or "IS NULL"/"IS NOT NULL"
     * @return \Oroboros\core\interfaces\library\database\query\ClauseInterface
     */
    public function where($subject, string $operand = '=', $comparator = null): \Oroboros\core\interfaces\library\database\query\ClauseInterface
    {
        if (!empty($this->where->toArray())) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. '
                        . 'Initial on statement already exists. '
                        . 'Use the [and|or] methods to add additional clauses.', get_class($this)));
        }
        $object = $this->load('library', static::QUERY_CONDITION_CLASS, 'WHERE', [
            'subject' => $subject,
            'operand' => $operand,
            'comparator' => $comparator,
            'connection' => $this->getArgument('connection')
            ], $this->getFlags());
        $key = 'clause_' . count($this->where);
        $this->where[$key] = $object;
        return $object;
    }

    /**
     * Adds an additional "AND" condition to the last set where clause.
     * If no previous where clause was set, it will create a top level where clause.
     * 
     * @param scalar|string|\Oroboros\core\interfaces\library\database\column\ColumnInterface $subject The column object, column name string, column alias string, or scalar value to compare
     * @param string $operand (optional) The operand [eg: =, >=, <=, <>, IN, NOT IN, LIKE, NOT LIKE, IS NULL, IS NOT NULL, etc]
     * @param scalar|string|\Oroboros\core\interfaces\library\database\column\ColumnInterface $comparator (optional) The comparison operator. Can be any logical operator, or "IS NULL"/"IS NOT NULL"
     * @return \Oroboros\core\interfaces\library\database\query\ClauseInterface
     */
    public function and($subject, string $operand = '=', $comparator = null): \Oroboros\core\interfaces\library\database\query\ClauseInterface
    {
        if (empty($this->where->toArray())) {
            return $this->where($subject, $operand, $comparator);
        }
        $object = $this->load('library', static::QUERY_CONDITION_CLASS, 'AND', [
            'subject' => $subject,
            'operand' => $operand,
            'comparator' => $comparator,
            'connection' => $this->getArgument('connection'),
            'table_aliases' => $this->table_aliases,
            'column_aliases' => $this->column_aliases
            ], $this->getFlags());
        $key = 'clause_' . count($this->where);
        $this->where[$key] = $object;
        return $object;
    }

    /**
     * Adds an additional "OR" condition to the last set where clause.
     * If no previous where clause was set, it will create a top level where clause.
     * 
     * @param scalar|string|\Oroboros\core\interfaces\library\database\column\ColumnInterface $subject The column object, column name string, column alias string, or scalar value to compare
     * @param string $operand (optional) The operand [eg: =, >=, <=, <>, IN, NOT IN, LIKE, NOT LIKE, IS NULL, IS NOT NULL, etc]
     * @param scalar|string|\Oroboros\core\interfaces\library\database\column\ColumnInterface $comparator (optional) The comparison operator. Can be any logical operator, or "IS NULL"/"IS NOT NULL"
     * @return \Oroboros\core\interfaces\library\database\query\ClauseInterface
     */
    public function or($subject, string $operand = '=', $comparator = null): \Oroboros\core\interfaces\library\database\query\ClauseInterface
    {
        if (empty($this->where->toArray())) {
            return $this->where($subject, $operand, $comparator);
        }
        $object = $this->load('library', static::QUERY_CONDITION_CLASS, 'OR', [
            'subject' => $subject,
            'operand' => $operand,
            'comparator' => $comparator,
            'connection' => $this->getArgument('connection'),
            'table_aliases' => $this->table_aliases,
            'column_aliases' => $this->column_aliases
            ], $this->getFlags());
        $key = 'clause_' . count($this->where);
        $this->where[$key] = $object;
        return $object;
    }

    /**
     * Adds another table or subquery to join.
     * May be a join, left join, right join, or inner join.
     * @param string|\Oroboros\core\interfaces\library\database\table\TableInterface|\Oroboros\core\interfaces\library\database\query\QueryInterface $subject
     *     May be a string table name or table alias, a table object, or a query object if joining to a subquery
     * @param string $type [INNER, LEFT, RIGHT, null]
     * @return \Oroboros\core\interfaces\library\database\query\JoinInterface
     */
    public function join($subject, string $type = null): \Oroboros\core\interfaces\library\database\query\JoinInterface
    {
        $object = $this->load('library', static::QUERY_JOIN_CLASS, $type, [
            'subject' => $subject,
            'connection' => $this->getArgument('connection'),
            'table_aliases' => $this->table_aliases,
            'column_aliases' => $this->column_aliases
            ], $this->getFlags());
        $key = 'join_' . count($this->joins);
        $this->joins[$key] = $object;
        return $object;
    }

    /**
     * Sets the limit clause, or removes it if no parameters are passed
     * @param int $start
     * @param int $end
     * @return $this (method chainable)
     */
    public function limit(int $start = null, int $end = null)
    {
        $this->limit['start'] = $start;
        $this->limit['end'] = $end;
        return $this;
    }

    /**
     * Adds an "ORDER BY" clause to the statement.
     * @param string $column
     * @param string $sort [asc|desc] (case insensitive)
     * @return $this (method chainable)
     */
    public function order(string $column, string $sort = 'asc')
    {
        $expected = 'Oroboros\\core\\interfaces\\library\\database\\column\\ColumnInterface';
        if (!is_string($column) && !(is_object($column) && ($column instanceof $expected))) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided column must be a string or instance of [%2$s].', get_class($this), $expected));
        }
        if (!in_array(strtoupper($sort), ['ASC', 'DESC'])) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Provided sort mode must be either ASC or DESC (case insensitive).', get_class($this)));
        }
        $column = $this->verifyColumn($column);
        $this->order[$column->getFullName()] = strtoupper($sort);
        return $this;
    }

    /**
     * Adds a "GROUP BY" clause to the statement.
     * @param string $column
     * @return $this (method chainable)
     */
    public function group(string $column)
    {
        $expected = 'Oroboros\\core\\interfaces\\library\\database\\column\\ColumnInterface';
        if (!is_string($column) && !(is_object($column) && ($column instanceof $expected))) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided column must be a string or instance of [%2$s].', get_class($this), $expected));
        }
        $column = $this->verifyColumn($column);
        $this->grouping[$column->getFullName()] = $column;
        return $this;
    }

    /**
     * Adds a "UNION" clause to the statement.
     * The provided statement must be a fully executable query
     * with the same rows as the existing query.
     * @param type $statement
     * @return $this (method chainable)
     */
    public function union($statement)
    {
        return $this;
    }

    /**
     * Adds a set of given columns to the queue for the current query.
     * @param traversable $columns Any iterable. Values must be string (column names) or column objects
     * @return $this (method chainable)
     * @throws \InvalidArgumentException
     */
    public function columns($columns)
    {
        if (!is_iterable($columns)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided argument [%2$s] must be iterable.', get_class($this), '$columns'));
        }
        foreach ($columns as $column) {
            $this->withColumn($column);
        }
        return $this;
    }

    /**
     * Adds a given column to the queue for the current query.
     * @param string|\Oroboros\core\interfaces\library\database\column\ColumnInterface $column
     * @return $this (method chainable)
     * @throws \InvalidArgumentException
     */
    public function withColumn($column)
    {
        $expected = 'Oroboros\\core\\interfaces\\library\\database\\column\\ColumnInterface';
        if (!is_string($column) && !(is_object($column) && ($column instanceof $expected))) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided column must be a string or instance of [%2$s].', get_class($this), $expected));
        }
        $column = $this->verifyColumn($column);
        $this->columns[$column->getFullName()] = $column;
        return $this;
    }

    /**
     * Removes a given column from the queue for the current query,
     * if it is currently queued.
     * @param string|\Oroboros\core\interfaces\library\database\column\ColumnInterface $column
     * @return $this (method chainable)
     */
    public function withoutColumn($column)
    {
        $expected = 'Oroboros\\core\\interfaces\\library\\database\\column\\ColumnInterface';
        if (!is_string($column) && !(is_object($column) && ($column instanceof $expected))) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided column must be a string or instance of [%2$s].', get_class($this), $expected));
        }
        $column = $this->verifyColumn($column);
        if ($this->columns->has($column->getFullName())) {
            unset($this->columns[$column->getFullName()]);
        }
        return $this;
    }

    /**
     * Returns a boolean determination as to whether
     * a given column is queued for the current query.
     * @param string|\Oroboros\core\interfaces\library\database\column\ColumnInterface $column
     * @return bool
     */
    public function hasColumn($column): bool
    {
        $expected = 'Oroboros\\core\\interfaces\\library\\database\\column\\ColumnInterface';
        if (!is_string($column) && !(is_object($column) && ($column instanceof $expected))) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided column must be a string or instance of [%2$s].', get_class($this), $expected));
        }
        $column = $this->verifyColumn($column);
        return $this->columns->has($column - getFullName());
    }

    /**
     * Sets a fixed value to be returned in the query alongside column selectors.
     * @param string $key The alias for the value
     * @param mixed $value The fixed value to return. variable type is retained as their closest SQL format equivalent (eg null will be null, floats will be decimals, integers and booleans will be integers, etc)
     * @return $this (method chainable)
     */
    public function setFixedValue(string $key, $value)
    {
        $this->fixedvals[$key] = $value;
        return $this;
    }

    /**
     * Returns a boolean determination as to whether a given fixed value key exists for the current statement
     * @param string $key
     * @return bool
     */
    public function hasFixedValue(string $key): bool
    {
        return $this->fixedvals->has($key);
    }

    /**
     * Returns the value associated with a fixed value key
     * @param string $key
     * @return mixed
     * @throws \InvalidArgumentException if the given fixed value key does not exist.
     *     This can be circumvented by using the `hasFixedValue` method to check
     *     prior to requesting
     */
    public function getFixedValue(string $key)
    {
        if (!$this->hasFixedValue($key)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'No fixed value exists for key [%2$s].', get_class($this), $key));
        }
        return $this->fixedvals->get($key);
    }

    /**
     * Removes a given fixed value key if it exists,
     * so it will not be returned in the result set of the statement.
     * @param string $key
     * @return $this (method chainable)
     */
    public function removeFixedValue(string $key)
    {
        if ($this->hasFixedValue($key)) {
            unset($this->fixedvals[$key]);
        }
        return $this;
    }

    /**
     * Sets an alias for a given column.
     * Used to generate the AS clause for the column selector.
     * @param string $column
     * @param string $alias
     * @return $this (method chainable)
     */
    public function setColumnAlias(string $column, string $alias)
    {
        $this->column_aliases[$column] = $alias;
        return $this;
    }

    /**
     * Returns a boolean determination as to whether a given column has an alias
     * @param string $column
     * @return bool
     */
    public function hasColumnAlias(string $column): bool
    {
        return $this->column_aliases->has($column);
    }

    /**
     * Returns the name of the column alias for the given column, if one exists
     * @param string $column
     * @return string
     * @throws \InvalidArgumentException If the given column does not have an alias.
     *     This can be circumvented by using the `hasColumnAlias` method to check
     *     prior to executing this method.
     */
    public function getColumnAlias(string $column): string
    {
        if (!$this->hasColumnAlias($column)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'No alias exists for column [%2$s].', get_class($this), $column));
        }
        return $this->column_aliases->get($column);
    }

    /**
     * Removes the alias for a given column, if it exists
     * @param string $column The column name (not the alias name)
     * @return $this (method chainable)
     */
    public function removeColumnAlias(string $column)
    {
        if ($this->hasColumnAlias($column)) {
            unset($this->column_aliases[$column]);
        }
        return $this;
    }

    /**
     * Sets the value to be used for a bound parameter.
     * This can be used to recycle the query with slightly varying values
     * to avoid recompiling the entire sql statement.
     * @param string $key
     * @param type $binding
     * @return $this (method chainable)
     */
    public function setBinding(string $key, $binding)
    {
        $this->bindings[$key] = $binding;
        return $this;
    }

    /**
     * Returns a boolean determination as to whether a given parameter binding exists.
     * @param string $key
     * @return bool
     */
    public function hasBinding(string $key): bool
    {
        return $this->bindings->has($key);
    }

    /**
     * Returns the currently bound value for a given binding key
     * @param string $key The parameter name (must exactly match the internal designator,
     *     as displayed in the sql statement (without the colon prefix)
     * @return mixed The value currently bound to the parameter
     * @throws \InvalidArgumentException If the given binding key does not exist.
     *     This can be circumvented by checking with `hasBinding` prior
     *     to executing this method.
     */
    public function getBinding(string $key)
    {
        if (!$this->bindings->has($key)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Binding [%2$s] does not exist.', get_class($this), $key));
        }
        return $this->bindings->get($key);
    }

    /**
     * Returns the compiled PDOStatement. Will compile it if it has not already been compiled.
     * @return \PDOStatement
     * @throws \PDOException if the statement is uncompiled and fails to compile
     *     due to malformed or incomplete sql.
     */
    public function getStatement(): \PDOStatement
    {
        if (is_null($this->statement)) {
            $this->buildStatement();
        }
        return $this->statement;
    }

    /**
     * Returns the raw sql statement, which has literal values and can
     * be run in a SQL client directly.
     * 
     * The sql statement returned by this method is never actually run internally.
     * It is just a point of reference for debugging and manual external testing.
     * 
     * @return string
     */
    public function getRawSql(): string
    {
        if (is_null($this->raw_statement)) {
            $this->buildSql();
        }
        return $this->raw_statement;
    }

    /**
     * Resets the object to a completely blank unused state.
     * @return void
     */
    protected function resetAll(): void
    {
        $this->table_aliases = $this->containerize();
        $this->columns = $this->containerize();
        $this->fixedvals = $this->containerize();
        $this->column_order = $this->containerize();
        $this->column_aliases = $this->containerize();
        $this->where = $this->containerize();
        $this->joins = $this->containerize();
        $this->order = $this->containerize();
        $this->grouping = $this->containerize();
        $this->limit = $this->containerize();
        $this->unions = $this->containerize();
        $this->subqueries = $this->containerize();
        $this->bindings = $this->containerize();
        $this->statement = null;
        $this->raw_statement = null;
        $this->results = $this->containerize();
        $this->errors = $this->containerize();
        $this->rowcount = 0;
        $this->parseArguments();
    }

    /**
     * Builds the sql statement string and raw sql statement
     * based on the current object state.
     * This does not compile the PDOStatement, and will not error if the SQL is invalid.
     * @return void
     */
    protected function buildSql(): void
    {
        $linebuffer = ($this->hasFlag(static::FLAG_COMPACT) && $this->getFlag(static::FLAG_COMPACT) ? ' ' : PHP_EOL);
        $segments = [
            $this->buildQueryType(),
            $this->buildQueryColumns(),
            $this->buildQueryFrom(),
            $this->buildInsertValues(),
            $this->buildQueryJoins(),
            $this->buildUpdateSet(),
            $this->buildQueryWhere(),
            $this->buildQueryUnions(),
            $this->buildQueryGroupBy(),
            $this->buildQueryOrderBy(),
            $this->buildQueryLimit()
        ];
        foreach ($segments as $key => $segment) {
            if ($segment === '') {
                // Non-applicable section
                unset($segments[$key]);
            } else {
                $segments[$key] = trim($segment, PHP_EOL . ' ');
            }
        }
        $sql = sprintf('%1$s%2$s', trim(implode($linebuffer, $segments), PHP_EOL . ' '), static::DELIMITER);
        $this->sql = $sql;
        $this->buildRawSql();
    }

    /**
     * Builds the raw sql statement.
     * This does not compile the PDOStatement, and will not error if the SQL is invalid.
     * @return void
     */
    protected function buildRawSql(): void
    {
        $bound = $this->sql;
        foreach ($this->bindings as $key => $value) {
            if (strpos($bound, ':' . $key) !== false) {
                $bound = str_replace(':' . $key, (is_null($value) ? 'NULL' : (is_numeric($value) ? $value : sprintf('"%1$s"', $value))), $bound);
            }
        }
        $this->raw_statement = $bound;
    }

    private function buildQueryType(): string
    {
        $result = $this->type . ' ';
        $this->buildTypeModifiers($result, $this->type);
        return $result;
    }

    private function buildQueryColumns(): string
    {
        switch ($this->type) {
            case 'SELECT':
                return $this->buildSelectColumns();
                break;
            case 'INSERT':
                return $this->buildInsertColumns();
                break;
            case 'UPDATE':
                return $this->buildUpdateColumns();
                break;
            case 'DELETE':
                return $this->buildDeleteColumns();
                break;
            case 'ALTER':
                return $this->buildAlterColumns();
                break;
            case 'CREATE':
                return $this->buildCreateColumns();
                break;
            case 'DROP':
                return $this->buildDropColumns();
                break;
        }
        return '';
    }

    private function buildSelectColumns(): string
    {
        $values = [];
        foreach ($this->fixedvals as $alias => $value) {
            $values[$alias] = sprintf('%1$s AS %3$s%2$s%4$s'
                , (is_null($value) ? 'NULL' : (is_numeric($value) ? $value : sprintf('"%1$s"', $value)))
                , $alias, static::LEFT_DELIMITER, static::RIGHT_DELIMITER);
        }
        foreach ($this->columns as $fullname => $column) {
            $fullname = str_replace(static::LEFT_DELIMITER, null, str_replace(static::RIGHT_DELIMITER, null, $fullname));
            $val = $this->getTableAliasedColumnName((string) $column);
            if ($this->column_aliases->has($fullname)) {
                $val .= sprintf(' AS %2$s%1$s%3$s', $this->column_aliases->get($fullname), static::LEFT_DELIMITER, static::RIGHT_DELIMITER);
            } elseif ($column->hasAlias()) {
                $val .= sprintf(' AS %2$s%1$s%3$s', $column->getAlias(), static::LEFT_DELIMITER, static::RIGHT_DELIMITER);
            }
            $values[$fullname] = $val;
        }
        if (empty($values)) {
            return ($this->alias == null ? null : sprintf('%2$s%1$s%3$s%4$s'
                    , $this->alias, static::LEFT_DELIMITER
                    , static::RIGHT_DELIMITER, static::SEPARATOR)) . '*' . PHP_EOL;
        }
        return implode(', ', $values) . PHP_EOL;
    }

    private function buildUpdateColumns(): string
    {
        return '';
    }

    private function buildInsertColumns(): string
    {
        return '';
    }

    private function buildDeleteColumns(): string
    {
        return '';
    }

    private function buildCreateColumns(): string
    {
        return '';
    }

    private function buildAlterColumns(): string
    {
        return '';
    }

    private function buildDropColumns(): string
    {
        return '';
    }

    private function buildQueryFrom(): string
    {
        switch ($this->type) {
            case 'SELECT':
                return $this->buildSelectFrom();
                break;
            case 'INSERT':
                return $this->buildInsertFrom();
                break;
            case 'UPDATE':
                return $this->buildUpdateFrom();
                break;
            case 'DELETE':
                return $this->buildDeleteFrom();
                break;
            case 'ALTER':
                return $this->buildAlterFrom();
                break;
            case 'CREATE':
                return $this->buildCreateFrom();
                break;
            case 'DROP':
                return $this->buildDropFrom();
                break;
        }
        return '';
    }

    private function buildSelectFrom(): string
    {
        if (!is_null($this->schema) && !is_null($this->table)) {
            $from = sprintf('FROM %3$s%1$s%4$s%5$s%3$s%2$s%4$s'
                , $this->schema->getName(), $this->table->getName(), static::LEFT_DELIMITER, static::RIGHT_DELIMITER, static::SEPARATOR);
        } elseif (is_null($this->schema) && !is_null($this->table)) {
            $from = sprintf('FROM %2$s%1$s%3$s'
                , $this->table->getName(), static::LEFT_DELIMITER, static::RIGHT_DELIMITER, static::SEPARATOR);
        } elseif (is_null($this->schema) && is_null($this->table)) {
            return '';
        }
        if (!is_null($this->alias)) {
            $from = sprintf('%1$s AS %3$s%2$s%4$s', $from, $this->getAlias(), static::LEFT_DELIMITER, static::RIGHT_DELIMITER);
        }
        return $from . PHP_EOL;
    }

    private function buildInsertFrom(): string
    {
        $linebuffer = ($this->hasFlag(static::FLAG_COMPACT) && $this->getFlag(static::FLAG_COMPACT) ? ' ' : PHP_EOL);
        if (!is_null($this->schema) && !is_null($this->table)) {
            $from = sprintf('INTO %3$s%1$s%4$s%5$s%3$s%2$s%4$s'
                , $this->schema->getName(), $this->table->getName(), static::LEFT_DELIMITER, static::RIGHT_DELIMITER, static::SEPARATOR);
        } elseif (is_null($this->schema) && !is_null($this->table)) {
            $from = sprintf('INTO %2$s%1$s%3$s'
                , $this->table->getName(), static::LEFT_DELIMITER, static::RIGHT_DELIMITER, static::SEPARATOR);
        } elseif (is_null($this->schema) && is_null($this->table)) {
            return '';
        }
        if (!is_null($this->alias)) {
            $from = sprintf('%1$s AS %3$s%2$s%4$s', $from, $this->getAlias(), static::LEFT_DELIMITER, static::RIGHT_DELIMITER);
        }
        $values = [];
        foreach ($this->columns as $fullname => $column) {
            $fullname = str_replace(static::LEFT_DELIMITER, null, str_replace(static::RIGHT_DELIMITER, null, $fullname));
            $val = $this->getTableAliasedColumnName((string) $column);
            if ($this->column_aliases->has($fullname)) {
                $val .= sprintf(' AS %2$s%1$s%3$s', $this->column_aliases->get($fullname), static::LEFT_DELIMITER, static::RIGHT_DELIMITER);
            } elseif ($column->hasAlias()) {
                $val .= sprintf(' AS %2$s%1$s%3$s', $column->getAlias(), static::LEFT_DELIMITER, static::RIGHT_DELIMITER);
            }
            $values[$fullname] = $val;
        }
        $from .= sprintf('%2$s(%1$s)%2$s', implode(',', $values), $linebuffer);
        return $from . ' ';
    }

    private function buildUpdateFrom(): string
    {
        if (!is_null($this->schema) && !is_null($this->table)) {
            $from = sprintf('%3$s%1$s%4$s%5$s%3$s%2$s%4$s'
                , $this->schema->getName(), $this->table->getName(), static::LEFT_DELIMITER, static::RIGHT_DELIMITER, static::SEPARATOR);
        } elseif (is_null($this->schema) && !is_null($this->table)) {
            $from = sprintf('%2$s%1$s%3$s'
                , $this->table->getName(), static::LEFT_DELIMITER, static::RIGHT_DELIMITER, static::SEPARATOR);
        } elseif (is_null($this->schema) && is_null($this->table)) {
            return '';
        }
        if (!is_null($this->alias)) {
            $from = sprintf('%1$s AS %3$s%2$s%4$s', $from, $this->getAlias(), static::LEFT_DELIMITER, static::RIGHT_DELIMITER);
        }
        return sprintf('%1$s%2$s', $from, ($this->hasFlag(static::FLAG_COMPACT) && $this->getFlag(static::FLAG_COMPACT) ? ' ' : PHP_EOL . '    '));
    }

    private function buildUpdateSet(): string
    {
        if ($this->type !== 'UPDATE') {
            return '';
        }
        $from = sprintf('%1$sSET', ($this->hasFlag(static::FLAG_COMPACT) && $this->getFlag(static::FLAG_COMPACT) ? ' ' : PHP_EOL . '    '));
        $values = [];
//        $from .= sprintf('%2$s(%1$s)%2$sVALUES%2$s', implode(', ', $values), ($this->hasFlag(static::FLAG_COMPACT) && $this->getFlag(static::FLAG_COMPACT) ? ' ' : PHP_EOL . '    '));
        foreach ($this->update_values as $column => $val) {
            $column = $this->verifyColumn($column);
            $key = 'update_set_' . $column->getName() . '_' . count($this->bindings);
            $values[] = $this->getTableAliasedColumnName((string) $column) . '= :' . $key;
            $this->bindings[$key] = $val;
//            foreach ($vals as $col => $val) {
//                $key = 'update_set_' . $col . '_' . count($this->bindings);
//            }
            $from .= sprintf('%2$s%1$s' . PHP_EOL, implode(', ', $values), ($this->hasFlag(static::FLAG_COMPACT) && $this->getFlag(static::FLAG_COMPACT) ? ' ' : PHP_EOL . '    '));
        }
        return $from;
    }

    private function buildInsertValues(): string
    {
        if ($this->type !== 'INSERT') {
            return '';
        }
        $linebuffer = ($this->hasFlag(static::FLAG_COMPACT) && $this->getFlag(static::FLAG_COMPACT) ? ' ' : PHP_EOL);
        $valset = sprintf('%1$sVALUES%1$s', $linebuffer);
        $values = [];
        foreach ($this->insert_values as $row) {
            $tmprow = [];
            foreach ($row as $column => $value) {
                $key = 'insert_value_' . $column . '_' . count($this->bindings);
                $this->bindings[$key] = $value;
                $tmprow[] = ':' . $key;
            }
            $values[] = sprintf('(%1$s)', implode(', ', $tmprow));
        }
        $valset .= sprintf('%1$s%2$s', implode(',' . $linebuffer, $values), $linebuffer);
        return $valset;
    }

    private function buildDeleteFrom(): string
    {
        $from = '';
        if (!empty($this->joins->toArray())) {
            if (!is_null($this->alias)) {
                $from .= sprintf('%2$s%1$s%3$s ', $this->getAlias(), static::LEFT_DELIMITER, static::RIGHT_DELIMITER);
            } elseif (!is_null($this->schema) && !is_null($this->table)) {
                $from .= sprintf('%3$s%1$s%4$s%5$s%3$s%2$s%4$s '
                    , $this->schema->getName(), $this->table->getName(), static::LEFT_DELIMITER, static::RIGHT_DELIMITER, static::SEPARATOR);
            } elseif (is_null($this->schema) && !is_null($this->table)) {
                $from .= sprintf('%2$s%1$s%3$s '
                    , $this->table->getName(), static::LEFT_DELIMITER, static::RIGHT_DELIMITER, static::SEPARATOR);
            }
        }
        if (!is_null($this->schema) && !is_null($this->table)) {
            $from .= sprintf('FROM %3$s%1$s%4$s%5$s%3$s%2$s%4$s'
                , $this->schema->getName(), $this->table->getName(), static::LEFT_DELIMITER, static::RIGHT_DELIMITER, static::SEPARATOR);
        } elseif (is_null($this->schema) && !is_null($this->table)) {
            $from = sprintf('FROM %2$s%1$s%3$s'
                , $this->table->getName(), static::LEFT_DELIMITER, static::RIGHT_DELIMITER, static::SEPARATOR);
        } elseif (is_null($this->schema) && is_null($this->table)) {
            return '';
        }
        if (!is_null($this->alias)) {
            $from = sprintf('%1$s AS %3$s%2$s%4$s', $from, $this->getAlias(), static::LEFT_DELIMITER, static::RIGHT_DELIMITER);
        }
        return $from . PHP_EOL;
    }

    private function buildCreateFrom(): string
    {
        return '';
    }

    private function buildAlterFrom(): string
    {
        return '';
    }

    private function buildDropFrom(): string
    {
        return '';
    }

    private function buildQueryJoins(): string
    {
        $results = [];
        foreach ($this->joins as $key => $join) {
            $statement = $join->getStatement();
            foreach ($join->getBindings() as $bind_key => $bind_val) {
                $new_bind_key = $key . '_' . $bind_key;
                $this->bindings[$new_bind_key] = $bind_val;
                $statement = str_replace(':' . $bind_key, ':' . $new_bind_key, $statement);
            }
            $results[] = $statement;
        }
        return implode(($this->hasFlag(self::FLAG_COMPACT) && $this->getFlag(self::FLAG_COMPACT) ? ' ' : PHP_EOL), $results) . ($this->hasFlag(self::FLAG_COMPACT) && $this->getFlag(self::FLAG_COMPACT) ? ' ' : PHP_EOL);
    }

    private function buildQueryWhere(): string
    {
        $results = [];
        foreach ($this->where as $key => $clause) {
            $statement = $clause->getStatement();
            foreach ($clause->getBindings() as $bind_key => $bind_val) {
                $new_bind_key = $key . '_' . $bind_key;
                $this->bindings[$new_bind_key] = $bind_val;
                $statement = str_replace(':' . $bind_key, ':' . $new_bind_key, $statement);
            }
            $results[] = $statement;
        }
        return implode(($this->hasFlag(self::FLAG_COMPACT) && $this->getFlag(self::FLAG_COMPACT) ? ' ' : PHP_EOL), $results) . ($this->hasFlag(self::FLAG_COMPACT) && $this->getFlag(self::FLAG_COMPACT) ? ' ' : PHP_EOL);
    }

    private function buildQueryOrderBy(): string
    {
        if ($this->type !== 'SELECT' || empty($this->order->toArray())) {
            return '';
        }
        $order = [];
        foreach ($this->order as $column => $sort) {
            $column = implode('`.`', explode('.', $column));
            $column = str_replace('``', '`', $column);
            $order[] = sprintf('`%1$s` %2$s', trim($column, '`'), $sort);
        }
        return trim(sprintf('ORDER BY %1$s', implode(', ', $order)), ', ');
    }

    private function buildQueryUnions(): string
    {
        if ($this->type !== 'SELECT') {
            return '';
        }
        return '';
    }

    private function buildQueryLimit(): string
    {
        $start = ($this->limit->has('start') ? $this->limit->get('start') : null);
        $end = ($this->limit->has('end') ? $this->limit->get('end') : null);
        if (!is_null($start) && !is_null($end)) {
            $this->bindings['limit_start'] = $start;
            $this->bindings['limit_end'] = $end;
            return sprintf('LIMIT %1$s, %2$s', ':limit_start', ':limit_end') . PHP_EOL;
        } elseif (!is_null($start)) {
            unset($this->bindings['limit_end']);
            $this->bindings['limit_start'] = $start;
            $this->bindings['limit_end'] = PHP_INT_MAX;
            // Must specify an upper bound. Use the max integer value.
            return sprintf('LIMIT %1$s, %2$s', ':limit_start', ':limit_end') . PHP_EOL;
        } elseif (!is_null($end)) {
            unset($this->bindings['limit_start']);
            $this->bindings['limit_end'] = $end;
            return sprintf('LIMIT %1$s', ':limit_end') . PHP_EOL;
        }
        unset($this->bindings['limit_start']);
        unset($this->bindings['limit_end']);
        return '';
    }

    private function buildQueryGroupBy(): string
    {
        if ($this->type !== 'SELECT') {
            return '';
        }
        return '';
    }

    private function expireStatement(): void
    {
        $this->statement = null;
    }

    private function buildStatement(): void
    {
        $this->expireStatement();
        $this->buildSql();
        try {
            $this->statement = $this->getConnection()->prepare($this->sql);
        } catch (\ErrorException $e) {
            // Unable to prepare statement
            throw $e;
        }
    }

    private function buildTypeModifiers(string $result, string $type): string
    {
        switch ($type) {
            case 'SELECT':
                if ($this->hasFlag(static::FLAG_DISTINCT) && $this->getFlag(static::FLAG_DISTINCT)) {
                    $result .= 'DISTINCT ';
                }
                break;
            case 'INSERT':
            case 'UPDATE':
                if ($this->hasFlag(static::FLAG_IGNORE) && $this->getFlag(static::FLAG_IGNORE)) {
                    $result .= 'IGNORE ';
                }
                break;
            case 'CREATE':
                if ($this->hasFlag(static::FLAG_NOTEXISTS) && $this->getFlag(static::FLAG_NOTEXISTS)) {
                    $result .= 'IF NOT EXISTS ';
                }
                break;
            case 'DROP':
            case 'ALTER':
                if ($this->hasFlag(static::FLAG_EXISTS) && $this->getFlag(static::FLAG_EXISTS)) {
                    $result .= 'IF EXISTS ';
                }
                break;
        }
        return $result;
    }

    private function expireResults(): void
    {
        $this->results = null;
    }

    private function buildResults(): void
    {
        $this->expireResults();
        $this->results = $this::containerize();
        try {
            $this->buildStatement();
            $this->buildBindings();
            $this->statement->execute();
            $this->rowcount = $this->statement->rowCount();
            while ($row = $this->statement->fetch()) {
                $this->addResult($row);
            }
        } catch (\PDOException $e) {
            $this->rowcount = $this->statement->rowCount();
            $this->errors[(string) $e->getCode()] = [
                'code' => $this->statement->errorCode(),
                'message' => $e->getMessage(),
                'statement' => $this->statement,
                'sql' => $this->sql,
                'bindings' => $this->bindings,
                'info' => $this->statement->errorInfo(),
                'trace' => $e->getTrace()
            ];
            throw $e;
        }
    }

    private function buildBindings(): void
    {
        foreach ($this->bindings as $key => $binding) {
            $args = [
                'key' => ':' . $key,
                'binding' => $binding,
                'flag' => (is_null($binding) ? \PDO::PARAM_NULL : (is_bool($binding) ? \PDO::PARAM_BOOL : (is_numeric($binding) ? \PDO::PARAM_INT : \PDO::PARAM_STR) ) )
            ];
            if (strpos($this->sql, ':' . $key) !== false) {
                $this->statement->bindParam($args['key'], $args['binding'], $args['flag']);
            }
        }
    }

    private function addResult(array $result): void
    {
        $this->results[(string) count($this->results)] = $result;
    }

    private function verifyName(): void
    {
        if (is_null($this->getCommand()) || !in_array(strtoupper($this->getCommand()), self::$valid_types)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Query must declare a valid type [%2$s].', get_class($this), implode(', ', self::$valid_types)));
        }
    }

    private function initializeQuery(): void
    {
        $this->setConnection($this->getArgument('connection'));
        $this->initializeTableAliases();
        $this->initializeColumnAliases();
        $this->update_values = $this::containerize();
        $this->insert_values = $this::containerize();
        $this->resetAll();
//        parent::addQuery($this);
    }

    private function parseArguments(): void
    {
        if ($this->hasArgument(\Oroboros\core\abstracts\library\database\schema\AbstractSchema::CLASS_SCOPE)) {
            $this->schema = $this->verifySchema($this->getArgument(\Oroboros\core\abstracts\library\database\schema\AbstractSchema::CLASS_SCOPE));
        }
        if ($this->hasArgument(\Oroboros\core\abstracts\library\database\table\AbstractTable::CLASS_SCOPE)) {
            $this->table = $this->verifyTable($this->getArgument(\Oroboros\core\abstracts\library\database\table\AbstractTable::CLASS_SCOPE));
        }
        $this->type = $this->getCommand();
    }
}
