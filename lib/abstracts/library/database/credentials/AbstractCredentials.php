<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\database\credentials;

/**
 * Abstract Database Credentials
 * Represents a set of database credentials used to establish a connection to the database.
 * 
 * @author Brian Dayhoff
 */
abstract class AbstractCredentials extends \Oroboros\core\abstracts\library\database\AbstractDatabaseLibrary implements \Oroboros\core\interfaces\library\database\credentials\CredentialsInterface
{

    const CLASS_SCOPE = 'credentials';

    /**
     * The connector object for the database.
     * @var Oroboros\core\interfaces\library\connection\ConnectionInterface
     */
    private $connection = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->verifyCredentials();
        $this->verifyKey();
    }

    /**
     * Returns the key name associated with the credential set.
     * @return string
     */
    public function getKey(): string
    {
        return $this->getCommand();
    }

    /**
     * Only a single connection ever exists.
     * This prevents the application from locking up
     * numerous database channels and blocking other usage.
     * @return \Oroboros\core\interfaces\library\connection\ConnectionInterface
     */
    public function getConnection(): \Oroboros\core\interfaces\library\connection\ConnectionInterface
    {
        try {
            return parent::getConnection();
        } catch (\Exception $e) {
            $this->connection = $this->load('library', static::CONNECTION_CLASS, $this->getKey(), $this->getArguments(), $this->getFlags());
            parent::addConnection($this->getKey(), $this->connection);
            parent::setConnection($this->getKey());
        }
        return parent::getConnection();
    }

    private function verifyCredentials(): void
    {
        if (!$this->hasArgument('type')) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Provided credentials must include a type.', get_class($this)));
        } elseif ($this->getArgument('type') !== static::DATABASE_DRIVER) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Incorrect credential type. '
                        . 'Expected [%1$s], received [%3$s].', get_class($this), static::DATABASE_DRIVER, $this->getArgument('type')));
        } elseif (!$this->hasArgument('user')) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Provided credentials must include a user.', get_class($this)));
        } elseif (!$this->hasArgument('password')) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Provided credentials must include a password.', get_class($this)));
        }
        if (!$this->hasArgument('schema')) {
            // Nullify the schema
            $this->setArgument('schema', null);
        }
        if (!$this->hasArgument('host')) {
            $this->setArgument('host', static::DEFAULT_HOST);
        }
        if (!$this->hasArgument('port')) {
            $this->setArgument('port', static::DEFAULT_PORT);
        }
    }

    private function verifyKey(): void
    {
        if (is_null($this->getCommand())) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. No credential key provided.', get_class($this)));
        }
    }
}
