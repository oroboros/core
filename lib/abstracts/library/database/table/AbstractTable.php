<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\database\table;

/**
 * Abstract Database Table
 *
 * @author Brian Dayhoff
 */
abstract class AbstractTable extends \Oroboros\core\abstracts\library\database\AbstractDatabaseLibrary implements \Oroboros\core\interfaces\library\database\table\TableInterface
{

    use \Oroboros\core\traits\database\DatabaseObjectUtilityTrait;

    const CLASS_SCOPE = 'table';

    /**
     * The associated database schema
     * @var \Oroboros\core\interfaces\library\database\schema\SchemaInterface
     */
    private $schema = null;

    /**
     * The columns associated with the table
     * @var \Oroboros\core\interfaces\container\ContainerInterface
     */
    private $columns = null;

    /**
     * The columns associated with the table
     * @var \Oroboros\core\interfaces\container\ContainerInterface
     */
    private $indexes = null;

    /**
     * The columns associated with the table
     * @var \Oroboros\core\interfaces\container\ContainerInterface
     */
    private $relationships = null;

    /**
     * Represents the primary key, if one exists
     * @var \Oroboros\core\interfaces\library\database\index\IndexInterface
     */
    private $primary_key = null;

    /**
     * The column alias, if one exists.
     * Aliases are not retained, and should be set on a per-use case basis.
     * @var string
     */
    private $alias = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->verifyName();
        $this->initializeTable();
    }

    /**
     * Gets the fully qualified escaped table name.
     * Does not return the alias.
     * @return string
     */
    public function __toString()
    {
        if ($this->hasAlias()) {
            return sprintf('%1$s%3$s%2$s', static::LEFT_DELIMITER, static::RIGHT_DELIMITER, $this->getAlias());
        }
        return sprintf('%1$s%4$s%2$s%3$s%1$s%5$s%2$s', static::LEFT_DELIMITER, static::RIGHT_DELIMITER, static::SEPARATOR, $this->getSchema()->getName(), $this->getName());
    }

    public function serialize()
    {
        $data = unserialize(parent::serialize());
        $data['connection'] = $this->getConnection()->getName();
        $data['schema'] = $this->schema;
        $data['columns'] = $this->columns;
        $data['indexes'] = $this->indexes;
        $data['relationships'] = $this->relationships;
        $data['primary_key'] = $this->primary_key;
        return serialize($data);
    }

    public function unserialize($data)
    {
        try {
            parent::unserialize($data);
            $data = unserialize($data);
            $this->setConnection($data['connection']);
            $this->schema = $data['schema'];
            $this->columns = $data['columns'];
            $this->indexes = $data['indexes'];
            $this->relationships = $data['relationships'];
            $this->primary_key = $data['primary_key'];
        } catch (\Exception $e) {
            throw $e;
        }
        try {
            $this->verifyName();
        } catch (\Exception $e) {
            d($e->getMessage(), $e->getTraceAsString());
            exit;
        }
    }

    /**
     * Gets the associated schema object.
     * @return \Oroboros\core\interfaces\library\database\schema\SchemaInterface
     */
    public function getSchema(): \Oroboros\core\interfaces\library\database\schema\SchemaInterface
    {
        if (is_null($this->schema)) {
            $this->parseSchema();
        }
        return $this->schema;
    }

    /**
     * Gets the table name without the schema name (unescaped).
     * Does not return the alias.
     * @return string
     */
    public function getName(): string
    {
        return $this->getCommand();
    }

    /**
     * Gets the fully qualified table name (unescaped).
     * Does not return the alias.
     * @return string
     */
    public function getFullName(): string
    {
        $result = $this->getSchema()->getName()
            . static::SEPARATOR . $this->getName();
        return $result;
    }

    /**
     * Sets the table alias, or removes it if no parameters are passed.
     * @param string $alias
     * @return $this (method chainable)
     */
    public function as(string $alias = null)
    {
        $this->alias = $alias;
        $key = $this->getSchema()->getName() . static::SEPARATOR . $this->getName();
        if (is_null($alias) && $this->table_aliases->has($key)) {
            unset($this->table_aliases[$key]);
        } else {
            $this->table_aliases[$key] = $alias;
        }
        return $this;
    }

    /**
     * Returns a boolean determination as to whether an alias for this table is set.
     * @return bool
     */
    public function hasAlias(): bool
    {
        return !is_null($this->alias);
    }

    /**
     * Returns the defined table alias, or null if one does not exist.
     * @return string|null
     */
    public function getAlias(): ?string
    {
        return $this->alias;
    }

    /**
     * Returns a container of column objects corresponding to the table.
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    public function getColumns(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        if (is_null($this->columns)) {
            $this->parseColumns();
        }
        return $this->columns;
    }

    /**
     * Returns a container of index objects corresponding to the table.
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    public function getIndexes(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        if (is_null($this->indexes)) {
            $this->parseIndexes();
        }
        return $this->indexes;
    }

    /**
     * Returns an index object corresponding to the primary key of the table,
     * if one exists. If a primary key does not exist, returns null.
     * @return \Oroboros\core\interfaces\library\database\index\IndexInterface|null
     */
    public function getPrimaryKey(): ?\Oroboros\core\interfaces\library\database\index\IndexInterface
    {
        if (is_null($this->indexes)) {
            $this->parseIndexes();
        }
        return $this->primary_key;
    }

    public function getRelationships()
    {
        if (is_null($this->relationships)) {
            $this->parseRelationships();
        }
        return $this->relationships;
    }

    private function initializeTable(): void
    {
        $this->setConnection($this->getArgument('connection'));
        $this->initializeTableAliases();
        $this->initializeColumnAliases();
        parent::addTable($this);
    }

    private function parseSchema(): void
    {
        $this->schema = $this->verifySchema($this->getArgument(static::INFORMATION_SCHEMA_SELECTOR));
    }

    private function parseColumns(): void
    {
        try {
            $this->columns = parent::getColumnObjectsForTable($this->getSchema(), $this->getName());
        } catch (\InvalidArgumentException $e) {
            $this->columns = $this::containerize();
            $schema = $this->getSchema();
            $tablename = $this->getName();
            $exclude = 'VIEW';
            $table = sprintf('%1$s%4$s%2$s%3$s%1$s%5$s%2$s%', static::LEFT_DELIMITER, static::RIGHT_DELIMITER, static::SEPARATOR, static::INFORMATION_SCHEMA, static::INFORMATION_TABLE_COLUMNS);
            $sql = sprintf('SELECT * FROM %1$s%2$s', $table, PHP_EOL);
            $sql .= sprintf('WHERE %1$s%3$s%2$s=:%3$s%4$s', static::LEFT_DELIMITER, static::RIGHT_DELIMITER, static::INFORMATION_SCHEMA_SELECTOR, PHP_EOL);
            $sql .= sprintf('AND %1$s%3$s%2$s=:%3$s%4$s', static::LEFT_DELIMITER, static::RIGHT_DELIMITER, static::INFORMATION_TABLE_SELECTOR, PHP_EOL);
            $sql .= static::DELIMITER;
            $sth = $this->getConnection()->prepare($sql);
            $sth->bindParam(static::INFORMATION_SCHEMA_SELECTOR, $schema);
            $sth->bindParam(static::INFORMATION_TABLE_SELECTOR, $tablename);
            $sth->execute();
            while ($row = $sth->fetch()) {
                $row['connection'] = $this->getArgument('connection');
                $this->columns[$row[static::INFORMATION_COLUMN_SELECTOR]] = $this->load('library', static::COLUMN_CLASS, $row[static::INFORMATION_COLUMN_SELECTOR], $row);
            }
        }
    }

    private function parseIndexes(): void
    {
        try {
            $this->indexes = parent::getIndexObjectsForTable($this->getSchema(), $this->getName());
        } catch (\InvalidArgumentException $e) {
            $this->indexes = $this::containerize();
            $schema = $this->getSchema()->getName();
            $tablename = $this->getName();
            $table = sprintf('%1$s%4$s%2$s%3$s%1$s%5$s%2$s%', static::LEFT_DELIMITER, static::RIGHT_DELIMITER, static::SEPARATOR, static::INFORMATION_SCHEMA, static::INFORMATION_TABLE_INDEXES);
            $sql = sprintf('SELECT * FROM %1$s%2$s', $table, PHP_EOL);
            $sql .= sprintf('WHERE %1$s%3$s%2$s=:%3$s%4$s', static::LEFT_DELIMITER, static::RIGHT_DELIMITER, static::INFORMATION_SCHEMA_SELECTOR, PHP_EOL);
            $sql .= sprintf('AND %1$s%3$s%2$s=:%3$s%4$s', static::LEFT_DELIMITER, static::RIGHT_DELIMITER, static::INFORMATION_TABLE_SELECTOR, PHP_EOL);
            $sql .= static::DELIMITER;
            $sth = $this->getConnection()->prepare($sql);
            $sth->bindParam(static::INFORMATION_SCHEMA_SELECTOR, $schema);
            $sth->bindParam(static::INFORMATION_TABLE_SELECTOR, $tablename);
            $sth->execute();
            while ($row = $sth->fetch()) {
                $row['connection'] = $this->getArgument('connection');
                if (!$this->indexes->has($row[static::INFORMATION_INDEX_SELECTOR])) {
                    $this->indexes[$row[static::INFORMATION_INDEX_SELECTOR]] = $this->load('library', static::INDEX_CLASS, $row[static::INFORMATION_INDEX_SELECTOR], $row);
                }
                $this->indexes[$row[static::INFORMATION_INDEX_SELECTOR]]->addColumn($this->verifyColumn($row[static::INFORMATION_COLUMN_SELECTOR]));
            }
            if ($this->indexes->has(static::PRIMARY_KEY_SELECTOR)) {
                $this->primary_key = $this->indexes->get(static::PRIMARY_KEY_SELECTOR);
            }
        }
    }

    private function parseRelationships(): void
    {
        try {
            $this->relationships = parent::getRelationshipObjectsForTable($this->getSchema(), $this->getName());
        } catch (\InvalidArgumentException $e) {
            $this->relationships = $this::containerize();
            $schema = $this->getSchema();
            $tablename = $this->getName();
            $table = sprintf('%1$s%4$s%2$s%3$s%1$s%5$s%2$s%', static::LEFT_DELIMITER, static::RIGHT_DELIMITER, static::SEPARATOR, static::INFORMATION_SCHEMA, static::INFORMATION_TABLE_RELATIONSHIPS);
            $sql = sprintf('SELECT * FROM %1$s%2$s', $table, PHP_EOL);
            $sql .= sprintf('WHERE %1$s%3$s%2$s=:%3$s%4$s', static::LEFT_DELIMITER, static::RIGHT_DELIMITER, static::INFORMATION_SCHEMA_SELECTOR, PHP_EOL);
            $sql .= sprintf('AND %1$s%3$s%2$s=:%3$s%4$s', static::LEFT_DELIMITER, static::RIGHT_DELIMITER, static::INFORMATION_TABLE_SELECTOR, PHP_EOL);
            $sql .= static::DELIMITER;
            $sth = $this->getConnection()->prepare($sql);
            $sth->bindParam(static::INFORMATION_SCHEMA_SELECTOR, $schema);
            $sth->bindParam(static::INFORMATION_TABLE_SELECTOR, $tablename);
            $sth->execute();
            while ($row = $sth->fetch()) {
                $row['connection'] = $this->getArgument('connection');
                if (!$this->relationships->has($row[static::INFORMATION_RELATIONSHIP_SELECTOR])) {
                    $this->relationships[$row[static::INFORMATION_RELATIONSHIP_SELECTOR]] = $this->load('library', static::RELATIONSHIP_CLASS, $row[static::INFORMATION_RELATIONSHIP_SELECTOR], $row);
                }
                $this->relationships[$row[static::INFORMATION_RELATIONSHIP_SELECTOR]]->addColumn($this->getColumnObject($this->getSchema(), $this->getName(), $row[static::INFORMATION_COLUMN_SELECTOR]));
            }
        }
    }

    private function verifyName(): void
    {
        if (is_null($this->getCommand())) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Table must declare a name.', get_class($this)));
        }
    }
}
