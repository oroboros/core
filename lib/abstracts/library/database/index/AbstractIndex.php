<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\database\index;

/**
 * Abstract Database Index
 *
 * @author Brian Dayhoff
 */
abstract class AbstractIndex extends \Oroboros\core\abstracts\library\database\AbstractDatabaseLibrary implements \Oroboros\core\interfaces\library\database\index\IndexInterface
{

    use \Oroboros\core\traits\database\DatabaseObjectUtilityTrait;

    const CLASS_SCOPE = 'index';

    /**
     * The database that the column belongs to
     * @var string
     */
    private $schema = null;

    /**
     * The table that the column belongs to
     * @var string
     */
    private $table = null;

    /**
     * Container of related column objects associated with the index
     * @var Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $columns = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->verifyName();
        $this->initializeIndex();
    }

    public function __toString()
    {
        return sprintf('%1$s%3$s%2$s', static::LEFT_DELIMITER, static::RIGHT_DELIMITER, $this->getName());
    }

    public function addColumn(\Oroboros\core\interfaces\library\database\column\ColumnInterface $column)
    {
        parent::addColumn($column);
        $this->columns[$column->getName()] = $column;
    }

    public function getSchema(): \Oroboros\core\interfaces\library\database\schema\SchemaInterface
    {
        return $this->schema;
    }

    public function getTable(): \Oroboros\core\interfaces\library\database\table\TableInterface
    {
        return $this->table;
    }

    public function getName(): string
    {
        return $this->getCommand();
    }

    public function getColumns(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        return $this->columns;
    }

    public function hasColumn(string $column): bool
    {
        return $this->columns->has($column);
    }

    public function getColumn(string $column): \Oroboros\core\interfaces\library\database\column\ColumnInterface
    {
        if (!$this->hasColumn($column)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Column [%2$s] is not known to index [%3$s].', get_class($this), $column, $this->getName()));
        }
        return $this->columns->get($column);
    }

    private function verifyName(): void
    {
        if (is_null($this->getCommand())) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Index must declare a name.', get_class($this)));
        }
    }

    private function initializeIndex(): void
    {
        $this->setConnection($this->getArgument('connection'));
        $this->columns = $this::containerize();
        $this->parseDefinition();
        parent::addIndex($this);
    }

    private function parseDefinition(): void
    {
        $this->schema = $this->verifySchema($this->getArgument(static::INFORMATION_SCHEMA_SELECTOR));
        $this->table = $this->verifyTable($this->getArgument(static::INFORMATION_TABLE_SELECTOR));
    }
}
