<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\database\schema;

/**
 * Abstract Database Schema
 *
 * @author Brian Dayhoff
 */
abstract class AbstractSchema extends \Oroboros\core\abstracts\library\database\AbstractDatabaseLibrary implements \Oroboros\core\interfaces\library\database\schema\SchemaInterface
{

    use \Oroboros\core\traits\database\DatabaseObjectUtilityTrait;

    const CLASS_SCOPE = 'schema';

    /**
     * Any table associations with the schema
     * @var Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $tables = null;

    /**
     * Any view associations with the schema
     * @var Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $views = null;

    /**
     * Any view associations with the schema
     * @var Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $columns = null;

    /**
     * Any index associations with the schema
     * @var Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $indexes = null;

    /**
     * Any relationship associations with the schema
     * @var Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $relationships = null;

    /**
     * Any functions associations with the schema
     * @var Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $functions = null;

    /**
     * Any procedure associations with the schema
     * @var Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $procedures = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->verifyName();
        $this->initializeSchema();
    }

    public function __toString()
    {
        return sprintf('%1$s%3$s%2$s', static::LEFT_DELIMITER, static::RIGHT_DELIMITER, $this->getName());
    }

    public function serialize()
    {
        $data = unserialize(parent::serialize());
        $data['connection'] = $this->getConnection()->getName();
        $data['tables'] = $this->tables;
        $data['views'] = $this->views;
        $data['columns'] = $this->columns;
        $data['indexes'] = $this->indexes;
        $data['relationships'] = $this->relationships;
        $data['functions'] = $this->functions;
        $data['procedures'] = $this->procedures;
        return serialize($data);
    }

    public function unserialize($data)
    {
        parent::unserialize($data);
        $data = unserialize($data);
        $this->setConnection($data['connection']);
        $this->tables = $data['tables'];
        $this->views = $data['views'];
        $this->columns = $data['columns'];
        $this->indexes = $data['indexes'];
        $this->relationships = $data['relationships'];
        $this->functions = $data['functions'];
        $this->procedures = $data['procedures'];
        $data = unserialize($data);
    }

    public function getName(): string
    {
        return $this->getCommand();
    }

    public function getTables(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        if (is_null($this->tables)) {
            $this->parseTables();
        }
        return $this->tables;
    }

    public function getTable(string $table): \Oroboros\core\interfaces\library\database\table\TableInterface
    {
        if (is_null($this->tables)) {
            $this->tables = $this::containerize();
        }
        if (!$this->tables->has($table)) {
            $object = $this->load('library', static::TABLE_CLASS, $table, [
                'connection' => $this->getConnection()->getName(),
                static::INFORMATION_SCHEMA_SELECTOR => $this->getName(),
                static::INFORMATION_TABLE_SELECTOR => $table,
                ], $this->getFlags());
            return $object;
        }
        return $this->tables->get($table);
    }

    public function getViews(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        if (is_null($this->views)) {
            $this->parseViews();
        }
        return $this->views;
    }

    public function getView(string $view): \Oroboros\core\interfaces\library\database\view\ViewInterface
    {
        if (is_null($this->views) || !$this->views->has($view)) {
            $this->parseViews([$view]);
        }
        if (!$this->views->has($view)) {
            $object = $this->load('library', static::VIEW_CLASS, $table, [
                'connection' => $this->getConnection()->getName(),
                static::INFORMATION_SCHEMA_SELECTOR => $this->getName(),
                static::INFORMATION_TABLE_SELECTOR => $table,
                ], $this->getFlags());
            return $object;
        }
        return $this->views->get($view);
    }

    private function initializeSchema(): void
    {
        $this->setConnection($this->getArgument('connection'));
        $this->initializeTableAliases();
        $this->initializeColumnAliases();
        parent::AddSchema($this);
    }

    private function initializeTables(): void
    {
        if (is_null($this->tables)) {
            try {
                $this->tables = parent::getTableObjectsForSchema($this->getName());
            } catch (\InvalidArgumentException $e) {
                $this->tables = $this::containerize();
            }
        }
    }

    private function parseTables(array $tables = null): void
    {
        $this->initializeTables();

        $schema = $this->getName();
        $exclude = 'VIEW';
        $table = sprintf('%1$s%4$s%2$s%3$s%1$s%5$s%2$s%', static::LEFT_DELIMITER, static::RIGHT_DELIMITER, static::SEPARATOR, static::INFORMATION_SCHEMA, static::INFORMATION_TABLE_TABLES);
        $sql = sprintf('SELECT * FROM %1$s%2$s', $table, PHP_EOL);
        $sql .= sprintf('WHERE %1$s%3$s%2$s=:%3$s', static::LEFT_DELIMITER, static::RIGHT_DELIMITER, static::INFORMATION_SCHEMA_SELECTOR);
        if (is_array($tables)) {
            $sql .= sprintf('%4$sAND %1$s%3$s%2$s IN (%4$s', static::LEFT_DELIMITER, static::RIGHT_DELIMITER, static::INFORMATION_TABLE_SELECTOR, PHP_EOL);
            $sql .= '    "' . implode('",' . PHP_EOL . '    "', $tables) . '"' . PHP_EOL; // sprintf('    :%1$s,%2$s', 'table-' . $key, PHP_EOL);
            $sql .= sprintf(')');
        }
        $sql .= static::DELIMITER;
        $sth = $this->getConnection()->prepare($sql);
        $sth->bindParam(static::INFORMATION_SCHEMA_SELECTOR, $schema);
        $sth->execute();
        while ($row = $sth->fetch()) {
            try {
                $row['connection'] = $this->getArgument('connection');
                $this->tables[$row[static::INFORMATION_TABLE_SELECTOR]] = $this->load('library', static::TABLE_CLASS, $row[static::INFORMATION_TABLE_SELECTOR], $row);
                parent::addTable($this->tables[$row[static::INFORMATION_TABLE_SELECTOR]]);
            } catch (\Exception $e) {
                d($e->getMessage(), $e->getTraceAsString());
                exit;
            }
        }
    }

    private function initializeViews(): void
    {
        if (is_null($this->views)) {
            try {
                $this->views = parent::getViewObjectsForSchema($this->getName());
            } catch (\InvalidArgumentException $e) {
                $this->views = $this::containerize();
            }
        }
    }

    private function parseViews(array $views = null): void
    {
        $this->initializeViews();
        $schema = $this->getName();
        $table = sprintf('%1$s%4$s%2$s%3$s%1$s%5$s%2$s%', static::LEFT_DELIMITER, static::RIGHT_DELIMITER, static::SEPARATOR, static::INFORMATION_SCHEMA, static::INFORMATION_TABLE_VIEWS);
        $sql = sprintf('SELECT * FROM %1$s%2$s', $table, PHP_EOL);
        $sql .= sprintf('WHERE %1$s%3$s%2$s=:%3$s%4$s', static::LEFT_DELIMITER, static::RIGHT_DELIMITER, static::INFORMATION_SCHEMA_SELECTOR, PHP_EOL);
        if (is_array($views)) {
            $sql .= sprintf('%4$sAND %1$s%3$s%2$s IN (%4$s', static::LEFT_DELIMITER, static::RIGHT_DELIMITER, static::INFORMATION_VIEW_SELECTOR, PHP_EOL);
            $sql .= '    "' . implode('",' . PHP_EOL . '    "', $views) . '"' . PHP_EOL; // sprintf('    :%1$s,%2$s', 'table-' . $key, PHP_EOL);
            $sql .= sprintf(')');
        }
        $sql .= static::DELIMITER;
        $sth = $this->getConnection()->prepare($sql);
        $sth->bindParam(static::INFORMATION_SCHEMA_SELECTOR, $schema);
        $sth->execute();
        while ($row = $sth->fetch()) {
            $row['connection'] = $this->getArgument('connection');
            $this->views[$row[static::INFORMATION_VIEW_SELECTOR]] = $this->load('library', static::VIEW_CLASS, $row[static::INFORMATION_VIEW_SELECTOR], $row);
        }
        foreach ($this->views as $view) {
            parent::addView($view);
        }
    }

    private function verifyName(): void
    {
        if (is_null($this->getCommand())) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Schema must declare a name.', get_class($this)));
        }
    }
}
