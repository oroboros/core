<?php
namespace Oroboros\core\abstracts\library\database\connection;

/**
 * Provides an abstract basis for establishing a PDO connection to a data layer.
 * 
 * Manages statements, binds, prepares, etc and returns the statement handler,
 * keeping the connection as a private property, to insure that connections can
 * be closed as expected without being held open by external references to the
 * connection.
 *
 * @author Brian Dayhoff
 */
abstract class AbstractConnection extends \Oroboros\core\abstracts\library\database\AbstractDatabaseLibrary implements \Oroboros\core\interfaces\library\connection\ConnectionInterface
{

    use \Oroboros\core\traits\database\DatabaseObjectUtilityTrait;

    /**
     * Defines further child classes as connections
     */
    const CLASS_SCOPE = 'connection';

    /**
     * Determines which database type the connection string will establish a connection to.
     * This must be overridden in child classes (eg mysql, postgres, mssql, sqlite, etc).
     * The value should correspond to a valid PDO driver for the given database layer.
     */
    const CONNECTION_TYPE = null;

    /**
     * Define the object as a connection.
     */
    const DATABASE_ELEMENT_SCOPE = 'connection';

    /**
     * The default PDO options that are always set.
     * These may be explicitly overridden if required.
     * @var array
     */
    private $default_pdo_options = [
        \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
        \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
        \PDO::ATTR_EMULATE_PREPARES => false,
        \PDO::ATTR_CASE => \PDO::CASE_NATURAL
    ];

    /**
     * Determines whether or not the connection is currently active.
     * @var bool
     */
    private $is_connected = false;

    /**
     * The active connection, if it exists.
     * @var \PDO
     */
    private $connection = null;

    /**
     * The bound schema for the connection.
     * @var \Oroboros\core\interfaces\library\database\schema\SchemaInterface
     */
    private $schema = null;

    /**
     * The bound tables for the connection.
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $tables = null;

    /**
     * A container of all persisted PDO connections,
     * to prevent taking up too many database connection slots.
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private static $connections = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        $this->checkConnectionType();
        if (is_null($arguments)) {
            $arguments = [];
        }
        if (is_null($flags)) {
            $flags = [];
        }
        $this->checkPdoDriver();
        $this->checkConnectionDetails($arguments);
        $flags = $this->getPdoOptions($flags);
        parent::__construct($command, $arguments, $flags);
        $this->initializePdoConnections();
        parent::addConnection($this->getCommand(), self::$connections[static::CONNECTION_TYPE][$this->getCommand()]);
        parent::setConnection($this->getCommand());
        $this->initializeTableAliases();
        $this->initializeColumnAliases();
    }

    public function serialize()
    {
        if (!$this->isRootInstance()) {
            return self::$connections[static::CONNECTION_TYPE][$this->getCommand()]->serialize();
        }
        $was_connected = false;
        $data = unserialize(parent::serialize());
        if ($this->is_connected) {
            $was_connected = true;
            $this->disconnect();
        }
        $data['schema'] = $this->schema;
        $result = serialize($data);
        if ($was_connected) {
            $this->connect();
        }
        return $result;
    }

    public function unserialize($data)
    {
        if (!$this->isRootInstance()) {
            return self::$connections[static::CONNECTION_TYPE][$this->getCommand()]->unserialize($data);
        }
        parent::unserialize($data);
        $data = unserialize($data);
        $this->addConnection($this->getName(), $this);
        $this->setConnection($this->getName());
        $this->schema = $data['schema'];
    }

    public function isConnected()
    {
        if (!$this->isRootInstance()) {
            return self::$connections[static::CONNECTION_TYPE][$this->getCommand()]->isConnected();
        }
        return $this->is_connected;
    }

    /**
     * Returns the name of the connection
     * @return string
     */
    public function getName(): string
    {
        if (!$this->isRootInstance()) {
            return self::$connections[static::CONNECTION_TYPE][$this->getCommand()]->getName();
        }
        return $this->getCommand();
    }

    /**
     * Establishes a connection to the database (no-op if already connected).
     * @return $this (method chainable)
     * @throws \ErrorException If a connection could not be established.
     *  The message will disclose the reason.
     *  The code will correspond to the original code returned from the PDOException.
     */
    public function connect()
    {
        if (!$this->isRootInstance()) {
            return self::$connections[static::CONNECTION_TYPE][$this->getCommand()]->connect();
        }
        if (!$this->is_connected) {
            $connection_string = static::CONNECTION_TYPE
                . ':host=' . $this->getArgument('host')
                . ';dbname=' . $this->getArgument('schema')
                . ';port=' . $this->getArgument('port');
            try {
                $pdo = new \PDO($connection_string, $this->getArgument('user'), $this->getArgument('password'), $this->getFlags());
            } catch (\PDOException $e) {
                throw new \Oroboros\core\exception\ErrorException(sprintf('Database connection of type [%1$s] could not be established in [%2$s]. '
                            . 'Reason: [%3$s].', static::CONNECTION_TYPE, get_class($this), $e->getMessage()), intval($e->getCode()));
            }
            $this->is_connected = true;
            $this->connection = $pdo;
        }
        return $this;
    }

    /**
     * Terminates the connection to the database (no-op if not connected).
     * @return $this (method chainable)
     */
    public function disconnect()
    {
        if (!$this->isRootInstance()) {
            return self::$connections[static::CONNECTION_TYPE][$this->getCommand()]->disconnect();
        }
        if ($this->is_connected) {
            $this->is_connected = false;
            $this->connection = null;
        }
        return $this;
    }

    /**
     * Prepares a SQL statement, and returns the statement handler object.
     * The connection will be established prior to preparing the statement if it is not already.
     * 
     * @param string $statement The sql statement
     * @param array $binds (optional) Associative array of prepared statement bindings
     * @param array $options (optional) Specific PDO options to run with this statement
     * @return \PDOStatement
     * @throws \ErrorException if the statement cannot be prepared.
     *  The error will be disclosed within the response message of the exception.
     *  The code will equate to the PDOException error code.
     * @throws \ErrorException If no connection was active, and a connection could not be established.
     *  The message will disclose the reason.
     *  The code will correspond to the original code returned from the PDOException.
     */
    public function prepare(string $statement, array $binds = null, array $options = null)
    {
        if (!$this->isRootInstance()) {
            return self::$connections[static::CONNECTION_TYPE][$this->getCommand()]->prepare($statement, $binds, $options);
        }
        $is_connected = $this->is_connected;
        if (!$is_connected) {
            $this->connect();
        }
        try {
            if (!is_null($options)) {
                $sth = $this->connection->prepare($statement, $options);
            } else {
                $sth = $this->connection->prepare($statement);
            }
            if (!is_null($binds)) {
                foreach ($binds as $field => $value) {
                    $sth->bindParam($field, $value);
                }
            }
        } catch (\PDOException $e) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Unable to prepare provided statement [%1$s] for connection [%2$s] in class [%3$s]. Reason [%4$s].'
                        , $statement,
                        static::CONNECTION_TYPE,
                        get_class($this),
                        $e->getMessage()), $e->getCode());
        }
        return $sth;
    }

    public function schema()
    {
        if (!$this->isRootInstance()) {
            return self::$connections[static::CONNECTION_TYPE][$this->getCommand()]->schema();
        }
        if (is_null($this->schema)) {
            $this->parseSchema();
        }
        return $this->schema;
    }

    public function tables()
    {
        if (!$this->isRootInstance()) {
            return self::$connections[static::CONNECTION_TYPE][$this->getCommand()]->tables();
        }
        if (is_null($this->tables)) {
            $this->parseTables();
        }
        return $this->tables;
    }

    public function select(string $schema = null, string $table = null, array $flags = null)
    {
        if (!$this->isRootInstance()) {
            return self::$connections[static::CONNECTION_TYPE][$this->getCommand()]->select($schema, $table, $flags);
        }
        $keys = [
            'connection' => $this->getCommand(),
            'schema' => $schema,
            'table' => $table,
        ];
        return $this->load('library', static::QUERY_CLASS, 'SELECT', $keys, $flags);
    }

    public function insert(string $schema = null, string $table = null, array $flags = null)
    {
        if (!$this->isRootInstance()) {
            return self::$connections[static::CONNECTION_TYPE][$this->getCommand()]->insert($schema, $table, $flags);
        }
        $keys = [
            'connection' => $this->getCommand(),
            'schema' => $schema,
            'table' => $table,
        ];
        return $this->load('library', static::QUERY_CLASS, 'INSERT', $keys, $flags);
    }

    public function update(string $schema = null, string $table = null, array $flags = null)
    {
        if (!$this->isRootInstance()) {
            return self::$connections[static::CONNECTION_TYPE][$this->getCommand()]->update($schema, $table, $flags);
        }
        $keys = [
            'connection' => $this->getCommand(),
            'schema' => $schema,
            'table' => $table,
        ];
        return $this->load('library', static::QUERY_CLASS, 'UPDATE', $keys, $flags);
    }

    public function delete(string $schema = null, string $table = null, array $flags = null)
    {
        if (!$this->isRootInstance()) {
            return self::$connections[static::CONNECTION_TYPE][$this->getCommand()]->delete($schema, $table, $flags);
        }
        $keys = [
            'connection' => $this->getCommand(),
            'schema' => $schema,
            'table' => $table,
        ];
        return $this->load('library', static::QUERY_CLASS, 'DELETE', $keys, $flags);
    }

    public function create(string $schema = null, string $table = null, array $flags = null)
    {
        if (!$this->isRootInstance()) {
            return self::$connections[static::CONNECTION_TYPE][$this->getCommand()]->create($schema, $table, $flags);
        }
        $keys = [
            'connection' => $this->getCommand(),
            'schema' => $schema,
            'table' => $table,
        ];
        return $this->load('library', static::QUERY_CLASS, 'CREATE', $keys, $flags);
    }

    public function drop(string $schema = null, string $table = null, array $flags = null)
    {
        if (!$this->isRootInstance()) {
            return self::$connections[static::CONNECTION_TYPE][$this->getCommand()]->drop($schema, $table, $flags);
        }
        $keys = [
            'connection' => $this->getCommand(),
            'schema' => $schema,
            'table' => $table,
        ];
        return $this->load('library', static::QUERY_CLASS, 'DROP', $keys, $flags);
    }

    public function columns(string $table)
    {
        if (!$this->isRootInstance()) {
            return self::$connections[static::CONNECTION_TYPE][$this->getCommand()]->columns($table);
        }
    }

    public function indexes(string $table, string $column = null)
    {
        if (!$this->isRootInstance()) {
            return self::$connections[static::CONNECTION_TYPE][$this->getCommand()]->indexes($table, $column);
        }
    }

    public function relationships(string $table, string $column = null)
    {
        if (!$this->isRootInstance()) {
            return self::$connections[static::CONNECTION_TYPE][$this->getCommand()]->relationships($table, $columns);
        }
    }

    private function parseSchema(): void
    {
        $this->schema = $this->verifySchema($this->getArgument(\Oroboros\core\abstracts\library\database\schema\AbstractSchema::CLASS_SCOPE));
    }

    private function parseTables(): void
    {
        $this->tables = $this->schema()->tables();
    }

    /**
     * Checks that the connection type is defined in the child class correctly.
     * @throws \OutOfRangeException if the child class has not defined its connection type.
     */
    private function checkConnectionType()
    {
        if (is_null(static::CONNECTION_TYPE)) {
            throw new \OutOfRangeException(sprintf('Connection class [%1$s] is not valid, as it has not declared a connection class constant to determine which PDO connection instance it corresponds to.', get_class($this)));
        }
    }

    /**
     * Checks if there is a corresponding PDO driver installed for this connection type.
     * @throws \OutOfBoundsException if there is no driver available on the
     *  current server for the current connection type
     */
    private function checkPdoDriver()
    {
        $drivers = \PDO::getAvailableDrivers();
        if (!in_array(static::CONNECTION_TYPE, $drivers)) {
            throw new \OutOfBoundsException(sprintf('A database connection of type [%1$s] cannot be established in class [%2$s], because there is no corresponding PDO driver installed on this server.', static::CONNECTION_TYPE, get_class($this)));
        }
    }

    /**
     * Checks that all required connection properties have been supplied.
     * @param array $arguments
     * @throws \OutOfBoundsException if any required connection properties
     *  are not supplied to establish a connection.
     */
    private function checkConnectionDetails(array $arguments)
    {
        $required = [
            "host",
            "port",
            "user",
            "password",
            "schema"
        ];
        $invalid = [];
        foreach ($required as $require) {
            if (!array_key_exists($require, $arguments)) {
                $invalid[] = $require;
            }
        }
        if (!empty($invalid)) {
            throw new \OutOfBoundsException(sprintf('Invalid connection configuration passed to [%1$s]. '
                        . 'The following required connection properties are missing: [%2$s].', get_class($this), implode(', ', $invalid)));
        }
    }

    /**
     * Establishes the PDO options from the defaults and any provided flags given,
     * and returns the full set.
     * 
     * Provided keys that have defaults will override the existing default values.
     * @param array $flags
     * @return array
     */
    private function getPdoOptions(array $flags)
    {
        foreach ($this->default_pdo_options as $key => $value) {
            if (!array_key_exists($key, $flags)) {
                $flags[$key] = $value;
            }
        }
        return $flags;
    }

    private function registerPdoConnection()
    {
        
    }

    private function fetchPdoConnection()
    {
        
    }

    private function initializePdoConnections()
    {
        if (is_null(self::$connections)) {
            self::$connections = $this::containerize();
        }
        if (!self::$connections->has(static::CONNECTION_TYPE)) {
            self::$connections[static::CONNECTION_TYPE] = $this::containerize();
        }
        if (!self::$connections[static::CONNECTION_TYPE]->has($this->getCommand())) {
            self::$connections[static::CONNECTION_TYPE][$this->getCommand()] = $this;
        }
    }

    private function isRootInstance(): bool
    {
        return self::$connections[static::CONNECTION_TYPE][$this->getCommand()] === $this;
    }
}
