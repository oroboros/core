<?php
namespace Oroboros\core\abstracts\library;

/**
 * Abstract Library
 * 
 * Sets the abstract foundation for how
 * libraries perform sets of unopinionated logic.
 *
 * @author Brian Dayhoff
 */
class AbstractLibrary extends \Oroboros\core\abstracts\AbstractBase implements \Oroboros\core\interfaces\library\LibraryInterface
{

    use \Oroboros\core\traits\LoaderTrait;
    use \Oroboros\core\traits\ContainerPackagerUtility;

    /**
     * Designates further child classes as libraries.
     */
    const CLASS_TYPE = 'library';

    /**
     * Class scope must be provided by abstraction, if applicable.
     */
    const CLASS_SCOPE = null;

    /**
     * Use the default container
     */
    const CONTAINER_CLASS = 'Oroboros\\core\\library\\container\\Container';

}
