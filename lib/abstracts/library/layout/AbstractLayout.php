<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\layout;

/**
 * Abstract Layout
 * Provides abstraction for a page layout.
 * Page layouts provide a baseline skeleton for component driven templating,
 * and organize baseline containers for content injection.
 *
 * @author Brian Dayhoff
 */
abstract class AbstractLayout extends \Oroboros\core\abstracts\library\AbstractLibrary implements \Oroboros\core\interfaces\library\layout\LayoutInterface
{

    use \Oroboros\core\traits\html\HtmlComponentUtility;
    use \Oroboros\core\traits\html\HtmlAssetUtilityTrait;

    const CLASS_SCOPE = 'layout';
    const CONTAINER_CLASS = 'Oroboros\\core\\library\\container\\ResponseContainer';
    const TEMPLATE_ENGINE = 'template\\twig\\Twig';
    const DEBUG_COMPONENT = 'debug';
    const ERROR_COMPONENT = 'error-debug';

    /**
     * The templating engine
     * @var Oroboros\core\interfaces\library\template\TemplateInterface
     */
    private $template_engine = null;

    /**
     * Any required data assets.
     * @var Oroboros\core\interfaces\library\container\ResponseContainerInterface
     */
    private $data = null;

    /**
     * Any required data assets.
     * @var Oroboros\core\interfaces\library\container\ResponseCollectionInterface
     */
    private $payload = null;

    /**
     * The response director object
     * @var \Oroboros\core\interfaces\library\http\ResponseDirectorInterface
     */
    private $director = null;

    /**
     * The base components used to contain sections for the layout.
     * @var Oroboros\core\interfaces\library\container\ResponseCollectionInterface
     */
    private $section_components = null;
    
    
    /**
     * The default universal debug sections.
     * "debug" is for when debug mode is enabled as per the app definition,
     * and provides a section that is packaged with additional info to display
     * on the frontend in some popout section that can vary by layout.
     * 
     * "error-debug" is for displaying error messages underneath the default
     * error page description, for developer information as to why the page
     * didn't load correctly. These are likewise only enabled when the
     * application debug definition returns true.
     * 
     * Layout sections only enabled when debug mode is active
     * @var array
     */
    private static $layout_debug_sections = [
        'debug' => [
            'component' => self::DEBUG_COMPONENT,
            'attributes' => [
                'id' => self::DEBUG_COMPONENT,
                'class' => 'd-flex row d-block m-0 p-0'
            ],
        ],
        'error' => [
            'component' => self::ERROR_COMPONENT,
            'attributes' => [
                'id' => self::ERROR_COMPONENT,
                'class' => 'd-flex row d-block m-0 p-0'
            ],
        ]
    ];

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        $this->verifyTemplateEngineDeclaration();
        $expected = 'Oroboros\\core\\interfaces\\library\\http\\ResponseDirectorInterface';
        if (!array_key_exists('director', $arguments) || !is_object($arguments['director']) || !in_array($expected, class_implements($arguments['director']))) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Expected instance of [%2$s] for argument key [%3$s]', get_class($this), $expected, 'director'));
        }
        $this->setResponseDirector($arguments['director']);
        parent::__construct($command, $arguments, $flags);
        $this->initializeHtmlAssets();
        $this->instantiateDataContainer();
        $this->instantiatePayloadContainer();
        $this->handleArguments();
        $this->verifyTemplateEngine();
        $this->instantiateSectionComponentContainer();
    }

    /**
     * Dependency injection method for the template engine. The template engine must
     * implement the template interface, and must be an instance of the
     * template engine defined by the class constant TEMPLATE_ENGINE
     * @param \Oroboros\core\interfaces\library\template\TemplateInterface $engine
     * @return void
     * @throws \InvalidArgumentException If the provided template engine does not
     *     match the defined acceptable template engine for the layout class,
     *     which is defined by the class constant TEMPLATE_ENGINE
     */
    public function setTemplateEngine(\Oroboros\core\interfaces\library\template\TemplateInterface $engine): void
    {
        $expected = $this->getFullClassName('library', static::TEMPLATE_ENGINE);
        if (!($engine instanceof $expected)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Provided template engine [%2$s] '
                        . 'is not an instance of expected template engine [%3$s] '
                        . 'required by this class.', get_class($this), get_class($engine), $expected));
        }
        $this->template_engine = $engine;
    }

    /**
     * Dependency injection method for the response director
     * @param \Oroboros\core\interfaces\library\http\ResponseDirectorInterface $director
     * @return $this (method chainable)
     */
    public function setResponseDirector(\Oroboros\core\interfaces\library\http\ResponseDirectorInterface $director): \Oroboros\core\interfaces\library\layout\LayoutInterface
    {
        $this->director = $director;
        return $this;
    }

    /**
     * Returns the response director object
     * @return \Oroboros\core\interfaces\library\http\ResponseDirectorInterface
     */
    public function getResponseDirector(): \Oroboros\core\interfaces\library\http\ResponseDirectorInterface
    {
        return $this->director;
    }

    /**
     * Adds content to a given section.
     * @param string $section The name of the section to add content to.
     * @param string $key The identifying key of the content.
     * @param string|array|Oroboros\core\interfaces\library\component\ComponentInterface $content The content payload.
     *     This may be a string, array, or component instance.
     * @return $this (method chainable)
     * @throws \InvalidArgumentException If the provided content section does not exist in this layout.
     */
    public function setContent(string $section, string $key, $content): \Oroboros\core\interfaces\library\layout\LayoutInterface
    {
        $this->validateContentSection($section);
        $this->evaluateContentType($content, $section, $key);
        return $this;
    }

    /**
     * Adds context to a given section.
     * @param string $section The name of the section to add context to.
     * @param string $key The identifying key of the context data.
     * @param mixed $context The context data to pass to the layout section.
     * @return \Oroboros\core\interfaces\library\layout\LayoutInterface
     * @throws \InvalidArgumentException
     */
    public function setContext(string $section, string $key, $context): \Oroboros\core\interfaces\library\layout\LayoutInterface
    {
        $this->validateContentSection($section);
        if ($key === 'content') {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Content cannot be set with this method. Use method [%2$s].',
                        get_class($this), 'setContent'));
        }
        $component = $this->getSectionComponents()[$section];
        if ($section === 'debug' && $component instanceof \Oroboros\core\interfaces\library\container\ContainerInterface) {
            $component['component']->addContext($key, $context);
            return $this;
        }
        $component->addContext($key, $context);
        return $this;
    }

    /**
     * Checks if a given context key is valid for the given section.
     * @param string $section The name of the section to add context to.
     * @param string $key The identifying key of the context data.
     * @return bool
     */
    public function checkContext(string $section, string $key): bool
    {
        $this->validateContentSection($section);
        $component = $this->getSectionComponents()[$section];
        return $component->isValidContext($key);
    }

    /**
     * Returns a list of the valid layout sections for the given layout
     * @return array
     */
    public function listContentSections(): array
    {
        $keys = array_keys($this->payload->toArray());
        return $keys;
    }

    /**
     * Returns true if a given section key exists, false otherwise
     * @return bool
     */
    public function hasSection(string $key): bool
    {
        $keys = array_keys($this->payload->toArray());
        return in_array($key, $keys);
    }

    /**
     * Removes content if it exists.
     * @param string $section The name of the section to add content to.
     * @param string key The identifying key of the content.
     * @return $this (method chainable)
     * @throws \InvalidArgumentException If the provided content section does not exist in this layout.
     */
    public function removeContent(string $section, string $key): \Oroboros\core\interfaces\library\layout\LayoutInterface
    {
        $this->validateContentSection($section);
        if ($this->payload->get($section)->has($key)) {
            unset($this->payload[$section][$key]);
        }
        return $this;
    }

    /**
     * Renders the layout output into a string of dom elements
     * that can be inserted into a page template.
     * @return string
     */
    public function render(): string
    {
        if ($this::app()->debugEnabled()) {
            $this->registerDebugInfo();
        }
        foreach ($this->getSectionComponents() as $section => $section_component) {
            if (!$this->getPayload()->has($section)) {
                continue;
            }
            foreach ($this->getPayload()[$section] as $key => $content) {
                $section_component->addContent($key, $content);
            }
        }
        $this->getTemplateEngine()->setTemplate($this->declareLayoutBase());
        $this->getTemplateEngine()->setData($this->getSectionComponents());
        return $this->getTemplateEngine()->renderTemplate();
    }

    /**
     * Returns the existing content designated by section (and optional key), if it exists. Returns null if it does not exist.
     * @param string $section The name of the section to add content to.
     * @param string key (optional) The identifying key of the content. If not provided, the entire section will be returned.
     * @return $this (method chainable)
     * @throws \InvalidArgumentException If the provided content section does not exist in this layout.
     */
    public function getContent(string $section, string $key = null)
    {
        $this->validateContentSection($section);
        if (is_null($key)) {
            return $this->payload->get($section);
        }
        if ($this->payload->get($section)->has($key)) {
            return $this->payload->get($section)->get($key);
        }
        return null;
    }

    public function getData(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        return $this->data;
    }

    /**
     * Override this method to define the container sections
     * that content can be added to.
     * @return array
     */
    abstract protected function declareSections(): array;

    /**
     * Override this method to define the base layout template.
     */
    abstract protected function declareLayoutBase(): string;
    
    /**
     * Returns the debug sections for the given template, if debug mode is enabled.
     * This may be overrridden for custom debug section handling.
     * 
     * If this is overridden, simply return:
     * `parent::declareDebugSections($your_customized_sections);`
     * 
     * This will recursively replace the default definitions without
     * invalidating the bits that still apply, and will prevent the need
     * to check manually if debug mode is enabled.
     * 
     * @return array
     */
    protected function declareDebugSections(array $child_sections = []): array
    {
        if ($this::app()->debugEnabled()) {
            $sections = self::$layout_debug_sections;
            // Use the static definitions in the event child classes override the constant
            $sections['debug']['component'] = $sections['debug']['attributes']['id'] = static::DEBUG_COMPONENT;
            $sections['error-debug']['component'] = $sections['error-debug']['attributes']['id'] = static::ERROR_COMPONENT;
            return array_replace_recursive($sections, $child_sections);
        }
        // Does not apply if debug is not enabled.
        return [];
    }

    protected function registerDebugInfo(): void
    {
        if ($this->getPayload()->has('debug')) {
            $this->setContext('debug', 'layout', $this->containerize([
                    'class' => get_class($this),
                    'sections' => array_replace_recursive($this->declareSections(), $this->declareDebugSections()),
                    'base' => $this->declareLayoutBase(),
                    'engine' => get_class($this->template_engine),
            ]));
        }
    }

    protected function getSectionComponents(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        return $this->section_components;
    }

    protected function getPayload(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        return $this->payload;
    }

    protected function getTemplateEngine(): \Oroboros\core\interfaces\library\template\TemplateInterface
    {
        return $this->template_engine;
    }

    private function handleArguments(): void
    {
        if ($this->hasArgument('template-engine')) {
            $provided = $this->getArgument('template-engine');
            $expected = 'Oroboros\\core\\interfaces\\library\\template\\TemplateInterface';
            if (!is_object($provided) || !($provided instanceof $expected)) {
                throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. '
                            . 'Provided parameter [%2$s] must be an object implementing '
                            . '[%3$s].', get_class($this), 'template-engine', $expected));
            }
            $this->setTemplateEngine($this->getArgument('template-engine'));
        }
    }

    /**
     * Analyzes the content provided, and handles various types appropriately.
     * @param mixed $content
     * @return void
     */
    private function evaluateContentType($content, string $section, string $key): void
    {
        $this->validateContentSection($section);
        if (is_string($content)) {
            // This is raw content.
            $this->payload[$section][$key] = $content;
            return;
        }
        if (is_array($content) && array_key_exists('component', $content)) {
            //list info to generate a component. Create one.
            $content = $this->getComponent($content['component'], $key, $content);
        }
        if (is_iterable($content)) {
            // Evaluate each element of the iterable subset individually
            // Keys will be generated as an iterative suffix of the original passed key
            $count = 0;
            foreach ($content as $key => $element) {
                try {
                    $this->evaluateContentType($element, $section, $key);
                } catch (\Exception $e) {
                    d($key, $element, $e->getMessage(), $e->getTraceAsString());
                    exit;
                    if (in_array($key, ['template', 'id'])) {
                        continue;
                    }
                    $k = $key . '-' . (string) $count;
                    $this->evaluateContentType($element, $section, $k);
                    $count++;
                }
            }
            return;
        }
        if (is_object($content) && ($content instanceof \Oroboros\core\interfaces\library\component\ComponentInterface)) {
            // Aggregate the component.
            $this->aggregateComponent($content);
            // Render the output and set the result as payload.
            $this->section_components->get($section)->addChild($content);
            $this->payload[$section][$key] = $content;
            return;
        }
    }

    /**
     * Registers the required scripts, stylesheets, images, and font dependencies
     * of a component, and their sub-dependencies also.
     * @param \Oroboros\core\interfaces\library\component\ComponentInterface $component
     * @return void
     */
    private function aggregateComponent(\Oroboros\core\interfaces\library\component\ComponentInterface $component): void
    {
        foreach ($component->getFonts() as $font) {
            $this->addFont($font);
        }
        foreach ($component->getScripts() as $section => $scripts) {
            foreach ($scripts as $script) {
                $this->addScript($section, $script);
            }
        }
        foreach ($component->getStyles() as $style) {
            $this->addStyle($style);
        }
        foreach ($component->getImages() as $image) {
            $this->addImage($image);
        }
    }

    /**
     * Recursively registers dependencies (scripts, fonts, styles, images, etc).
     * Uses the magic of common dependency interfaces to keep this super simple.
     * @param \Oroboros\core\interfaces\library\http\DependencyInterface $dependency
     * @param string $type [scripts, styles, images, fonts]
     * @return void
     */
    private function aggregateDependency(\Oroboros\core\interfaces\library\http\DependencyInterface $dependency, string $type): void
    {
        foreach ($dependency->getDependencies() as $subdep) {
            $this->aggregateDependency($subdep, $type);
        }
        if ($this->$type->has($dependency->getKey())) {
            // Already registered
            return;
        }
        $this->$type[$dependency->getKey()] = $dependency;
    }

    private function instantiateDataContainer(): void
    {
        $this->data = $this::containerize();
    }

    /**
     * Generate the container that stores content for defined layout sections
     * @return void
     */
    private function instantiatePayloadContainer(): void
    {
        $this->payload = $this->containerizeInto('Oroboros\\core\\library\\container\\ResponseCollection');
        foreach (array_replace_recursive($this->declareSections(), $this->declareDebugSections()) as $section => $details) {
            // Do not use the static container class here.
            // This presents the flexibility for child classes to redefine it without
            // the integrity check of the response collection denying payload containers.
            // Child classes are not forced to adhere to the constructs of abstraction internals.
            $this->payload[$section] = $this->containerizeInto(self::CONTAINER_CLASS);
        }
    }

    /**
     * Generates the baseline components that compose the layout sections
     * @return void
     */
    private function instantiateSectionComponentContainer(): void
    {
        $this->section_components = $this->containerizeInto('Oroboros\\core\\library\\container\\ResponseContainer');
        foreach (array_replace_recursive($this->declareSections(), $this->declareDebugSections()) as $section => $details) {
            $command = (array_key_exists('command', $details) ? $details['command'] : $section);
            $attributes = (array_key_exists('attributes', $details) ? $details['attributes'] : []);
            $flags = (array_key_exists('flags', $details) ? $details['flags'] : []);
            $component = $this->getComponent($details['component'], $attributes['id'], $attributes, $flags);
            $this->aggregateComponent($component);
            $this->section_components[$section] = $component;
        }
    }

    private function validateContentSection(string $section): void
    {
        if (!$this->payload->has($section)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided content section [%2$s] does not exist in this layout.', get_class($this), $section));
        }
    }

    private function verifyTemplateEngine(): void
    {
        $expected = 'Oroboros\\core\\interfaces\\library\\template\\TemplateInterface';
        if (is_null($this->template_engine)) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. '
                        . 'No template engine provided. '
                        . 'Expected instance of [%2$s].', get_class($this), $expected));
        }
    }

    private function verifyTemplateEngineDeclaration(): void
    {
        $expected = 'Oroboros\\core\\interfaces\\library\\template\\TemplateInterface';
        $defined = $this->getFullClassName('library', static::TEMPLATE_ENGINE);
        if (!class_exists($defined) || !in_array($expected, class_implements($defined))) {
            throw new \Oroboros\core\exception\core\InvalidClassException(sprintf('Class [%1$s] is invalid. Class constant [%2$s] '
                        . 'must define a concrete implementation of [%3$s] (stub classes are acceptable). '
                        . 'Provided value [%4$s] of class constant [%2$s] does not resolve to a valid '
                        . 'implementation.', get_class($this), 'TEMPLATE_ENGINE', $expected, static::TEMPLATE_ENGINE));
        }
    }
}
