<?php
namespace Oroboros\core\abstracts\library\container;

/**
 * The base abstraction for all container classes
 *
 * @author Brian Dayhoff
 */
abstract class AbstractContainer extends \Oroboros\core\abstracts\AbstractBase implements \Oroboros\core\interfaces\library\container\ContainerInterface
{

    /**
     * Designates further child classes as containers.
     */
    const CLASS_TYPE = 'container';
    
    const CLASS_SCOPE = null;

    use \Oroboros\core\traits\ContainerUtilityTrait;

    /**
     * Makes containers compatible with standard factorization.
     *
     * @param type $size
     * @param type $contents
     * @return type
     */
    public static function init($size = null, array $contents = array())
    {
        if (is_null($size)) {
            $size = -1;
        }
        $class = get_called_class();
        $instance = new $class($size);
        return $instance->fromArray($contents);
    }

    /**
     * Used to cast a container to a different type.
     *
     * This method creates an instance of itself from the contents
     * of a provided container, so that container contents can be
     * easily transferred between container types without
     * additional effort by external logic.
     *
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $container
     * @return type
     */
    public static function from(\Oroboros\core\interfaces\library\container\ContainerInterface $container)
    {
        $class = get_called_class();
        $size = $container->getSize();
        return $class::init($size, $container->toArray());
    }

    /**
     * Returns all container debug information if debug mode is enabled,
     * or false if it is not.
     *
     * @return array|false
     */
    public static function getDebug()
    {
        return self::debugGetAll();
    }
}
