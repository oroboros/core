<?php
namespace Oroboros\core\abstracts\library\container;

/**
 * The base abstraction for all collection classes
 *
 * @author Brian Dayhoff
 */
abstract class AbstractCollection extends AbstractContainer implements \Oroboros\core\interfaces\library\container\CollectionInterface
{

    use \Oroboros\core\traits\ContainerUtilityTrait;

    /**
     * Constrain acceptable values to only containers.
     * @return string
     */
    public function getExpectedContainerType()
    {
        return \Oroboros\core\interfaces\library\container\ContainerInterface::class;
    }
}
