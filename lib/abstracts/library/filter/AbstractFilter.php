<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\filter;

/**
 * Abstract Filter
 * Provides abstraction for defining and executing filters
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
abstract class AbstractFilter extends \Oroboros\core\abstracts\library\action\AbstractAction implements \Oroboros\core\interfaces\library\filter\FilterInterface
{

    /**
     * Use the default container
     */
    const CONTAINER_CLASS = 'Oroboros\\core\\library\\filter\\FilterPool';

    /**
     * Designates the class type as an filter
     */
    const CLASS_SCOPE = 'filter';

    /**
     * The scope required of workers. Worker classes must define a WORKER_SCOPE,
     * which is used by directors to filter for valid workers in scope for their
     * subset of directives.
     */
    const WORKER_SCOPE = 'filter';

    /**
     * The task accomplished by the worker. Worker classes must define a WORKER_TASK,
     * which is a keyword used by the director to isolate tasks appropriate for a given worker.
     */
    const WORKER_TASK = 'filter';

    /**
     * Required keys for registering event queues
     * @var array
     */
    private static $required_queue_keys = [];

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
    }

    public function executeTask(array $arguments = [], array $flags = [])
    {
        ;
    }

    /**
     * Returns the keys required for registering an event queue
     * 
     * @see \Oroboros\core\interfaces\library\resolver\ResolverInterface
     * @return array
     */
    protected function declareRequiredQueueDetailKeys(): array
    {
        return array_replace_recursive(parent::declareRequiredQueueDetailKeys(), self::$required_queue_keys);
    }
    
    /**
     * Verifies that the given subject is Filter Aware
     * 
     * @param \Oroboros\core\interfaces\library\action\ActionAwareInterface $subject
     * @return void
     * @throws \Oroboros\core\exception\ErrorException if the given subject is
     * invalid for the current scope.
     */
    protected function verifySubject(\Oroboros\core\interfaces\library\action\ActionManagerInterface $subject): void
    {
        $expected = 'Oroboros\\core\\interfaces\\library\\filter\\FilterManagerInterface';
        if (!($subject instanceof $expected)) {
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. Provided subject of type '
                        . '[%2$s] does not implement expected interface [%3$s].'
                        , get_class($this), get_class($subject), $expected)
            );
        }
    }
    
    /**
     * Performs filter binding steps.
     * 
     * @param \Oroboros\core\interfaces\library\action\ActionManagerInterface $subject
     * @return void
     */
    protected function bindSubject(\Oroboros\core\interfaces\library\action\ActionManagerInterface $subject): void
    {
        parent::bindSubject($subject);
        // Additional binding steps here
    }
    
    /**
     * Removes filter binding steps.
     * 
     * @param \Oroboros\core\interfaces\library\action\ActionManagerInterface $subject
     * @return void
     */
    protected function unbindSubject(\Oroboros\core\interfaces\library\action\ActionManagerInterface $subject): void
    {
        // Additional unbinding steps here
        parent::unbindSubject($subject);
    }
}
