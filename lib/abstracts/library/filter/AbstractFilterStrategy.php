<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\filter;

/**
 * Abstract Filter Strategy
 * Provides abstraction for filter strategies
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
abstract class AbstractFilterStrategy extends \Oroboros\core\abstracts\library\action\AbstractActionStrategy implements \Oroboros\core\interfaces\library\filter\FilterStrategyInterface
{

    const CLASS_SCOPE = 'filter-strategy';
    const STRATEGY_TYPE = 'filter';
    const CONTAINER_CLASS = 'Oroboros\\core\\library\\filter\\FilterQueueIndex';
    const STRATEGY_TARGET_INTERFACE = 'Oroboros\\core\\interfaces\\library\\filter\\FilterInterface';
    const ACTION_POOL_CLASS = 'Oroboros\\core\\library\\filter\\FilterPool';
    const STRATEGY_MANAGER_INTERFACE = 'Oroboros\\core\\interfaces\\library\\filter\\FilterManagerInterface';

    /**
     * Represents the default filter manager classes known to the filter strategy
     * @var type
     */
    private static $action_manager_classes = [
        'default' => 'filter\\FilterManager',
        'adapter' => 'filter\\AdapterFilterManager',
        'bootstrap' => 'filter\\BootstrapFilterManager',
        'component' => 'filter\\ComponentFilterManager',
        'controller' => 'filter\\ControllerFilterManager',
        'exception' => 'filter\\ExceptionFilterManager',
        'extension' => 'filter\\ExtensionFilterManager',
        'library' => 'filter\\LibraryFilterManager',
        'model' => 'filter\\ModelFilterManager',
        'module' => 'filter\\ModuleFilterManager',
        'service' => 'filter\\ServiceFilterManager',
        'view' => 'filter\\ViewFilterManager',
    ];
    
    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
    }
    
    /**
     * Declares the default action managers for filters
     * 
     * @return array
     */
    protected static function declareActionManagers(): array
    {
        return array_replace_recursive(parent::declareActionManagers(), self::$action_manager_classes);
    }
    
    /**
     * Performs the binding process for an event manager.
     * This will match the spl observer bindings between the two objects
     * and register the pair internally for reference.
     * 
     * @return void
     */
    protected function bindSubject(\Oroboros\core\interfaces\library\action\ActionAwareInterface $subject, \Oroboros\core\interfaces\library\action\ActionManagerInterface $manager): void
    {
        $subject->attach($manager);
        $manager->attach($subject);
    }
}
