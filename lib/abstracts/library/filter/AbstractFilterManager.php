<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\filter;

/**
 * Abstract Filter Manager
 * Provides abstraction for managing filters
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
abstract class AbstractFilterManager extends \Oroboros\core\abstracts\library\action\AbstractActionManager implements \Oroboros\core\interfaces\library\filter\FilterManagerInterface
{

    const CLASS_SCOPE = 'filter-manager';

    /**
     * Job managers use the job queue container
     */
    const CONTAINER_CLASS = 'Oroboros\\core\\library\\filter\\FilterQueue';

    /**
     * Job managers use the job queue container
     */
    const COLLECTION_CLASS = 'Oroboros\\core\\library\\filter\\FilterQueueIndex';

    /**
     * Child classes must provide a worker class.
     */
    const WORKER_CLASS = null;
    
    /**
     * Scopes worker classes to filters.
     */
    const WORKER_SCOPE = AbstractFilter::WORKER_SCOPE;

    /**
     * The filter strategy object
     * @var \Oroboros\core\interfaces\library\filter\FilterStrategyInterface
     */
    private static $filter_strategy = null;
    
    /**
     * Designates which spl objects are declared valid to bypass scope lock
     * 
     * @var array
     */
    private static $allowed_filter_spl_objects = [
        \Oroboros\core\interfaces\library\filter\FilterInterface::class,
        \Oroboros\core\interfaces\library\filter\FilterAwareInterface::class,
        \Oroboros\core\interfaces\library\filter\FilterListenerInterface::class,
        \Oroboros\core\interfaces\library\filter\FilterWatcherInterface::class,
    ];

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        \Oroboros\core\library\filter\FilterStrategy::registerActionType(static::ACTION_GROUP, static::WORKER_CLASS);
        parent::__construct($command, $arguments, $flags);
    }
    
    /**
     * Filter for handleable SplSubjects
     * @param \SplSubject $subject
     * @return void
     * @throws \InvalidArgumentException
     */
    public function update(\SplSubject $subject): void
    {
        $valid = false;
        foreach (self::$allowed_filter_spl_objects as $allowed) {
            if ($subject instanceof $allowed) {
                $valid = true;
                break;
            }
        }
        if (!$valid) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided object of type [%2$s] must be an instance of one of [%3$s].'
                , get_class($this), get_class($subject), implode(', ', self::$allowed_filter_spl_objects)));
        }
        parent::update($subject);
    }
}
