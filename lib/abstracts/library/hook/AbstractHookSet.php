<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\hook;

/**
 * Abstract Hook Set
 * Provides abstraction for creating a set of hooks from an associative array
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
abstract class AbstractHookSet extends \Oroboros\core\abstracts\library\container\AbstractContainer implements \Oroboros\core\interfaces\library\hook\HookSetInterface
{

    /**
     * The interface that the given hook classname is expected to honor.
     * This may be overridden with any other interface,
     * provided the replacement extends this interface.
     * 
     * Interface constraints may be tightened, but not loosened.
     * 
     * Interface definitions cannot use stub names, they must be fully qualified.
     */
    public const EXPECTED_INTERFACE = 'Oroboros\\core\\interfaces\\library\\hook\\HookInterface';

    /**
     * Represents the class to use as the defined hook in this hook set.
     * 
     * This may be a fully qualified class name or a stub class name.
     */
    const HOOK_CLASS = 'hook\\Hook';

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        $this->validateDeclaredInterface();
        $this->validateHookClass();
        parent::__construct($command, $arguments, $flags);
    }

    /**
     * Iterates over all contained hooks, sequentially replacing hooks 
     * in the given string with their values where they are relevant.
     * 
     * @param string $string
     * @return string
     */
    public function replace(string $string)
    {
        foreach ($this->getInternalValues() as $hook) {
            if (is_iterable($string) || is_object($string)) {
                // Was already matched to a non-scalar value
                break;
            } else {
                $string = $hook->replace($string);
            }
        }
        return $string;
    }

    /**
     * Iterates over all contained hooks, sequentially nullifying
     * their existence in the given string.
     * 
     * @param string $string
     * @return string
     */
    public function nullify(string $string): string
    {
        foreach ($this->getInternalValues() as $hook) {
            $string = $hook->nullify($string);
        }
        return $string;
    }

    /**
     * Fires on the container key setter, enforcing that keys are strings and
     * values are either scalar or null, as required by PSR-11 and the Hook api.
     *
     * @param scalar|stringable $key The passed key that the setter is supposed to have
     * @param mixed $value The value passed for the setter
     * @param string $method The internal method that the operation arose from.
     * @return void
     * @tbrows \Oroboros\core\exception\container\ContainerException if the key is not a string or the value is not scalar or null
     */
    protected function onSet($key, $value, $method = null)
    {
        if (!is_string($key)) {
            throw new \Oroboros\core\exception\container\ContainerException(
                    sprintf('Error encountered in [%1$s]. Provided key must be a string.', get_class($this))
            );
        }
        if (!is_scalar($value) && !is_null($value)) {
            throw new \Oroboros\core\exception\container\ContainerException(
                    sprintf('Error encountered in [%1$s]. Provided value must be scalar or null.', get_class($this))
            );
        }
    }

    /**
     * Stores a Hook class internally that contains the key as it's hook definer 
     * and the value as it's replacement value, which allows strings to be 
     * filtered through this container to replace hooks iteratively.
     *
     * @param string $key The passed key that the setter is supposed to have
     * @param scalar|null $value The value passed for the setter
     * @param string $method The internal method that the operation arose from.
     * @return void
     */
    protected function filterSetValue($key, $value, $method = null)
    {
        $proxy = new \Oroboros\core\pattern\factory\LoaderProxy();
        $hook = $proxy->load('library', static::HOOK_CLASS, $key, ['value' => $value]);
        return $hook;
    }

    /**
     * Returns the hook object value, which makes this work
     * functionally the same as returning the value set originally.
     *
     * @param scalar|stringable $key The passed key that the setter is supposed to have
     * @param mixed $value The value passed for the getter
     * @param string $method The internal method that the operation arose from.
     * @return scalar|null
     */
    protected function filterGet($key, $value, $method = null)
    {
        return $value->getValue();
    }

    /**
     * Insures that the declared hook class is a
     * valid implementation of the declared hook interface.
     * @return void
     * @throws \Oroboros\core\exception\core\InvalidClassException
     *         if the provided class does not resolve to a concrete class,
     *         or does not implement the declare hook interface.
     */
    private function validateHookClass(): void
    {
        $class = static::HOOK_CLASS;
        $proxy = new \Oroboros\core\pattern\factory\LoaderProxy();
        $classname = $proxy->getFullClassName('library', $class);
        if ($classname === false) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. Defined class constant'
                        . ' [%2$s] does not resolve to a valid class.'
                        . ' This class is unuseable in it\'s current form.'
                        , get_class($this), 'HOOK_CLASS')
            );
        }
        $interfaces = class_implements($classname);
        if (!($interfaces && in_array(static::EXPECTED_INTERFACE, $interfaces))) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s].'
                        . ' Declared hook class [%2$s] does not implement'
                        . ' expected interface [%3$s].'
                        . ' This class is unuseable in it\'s current form.'
                        , get_class($this), $class, static::EXPECTED_INTERFACE)
            );
        }
    }

    /**
     * If the interface constant declaration is overridden,
     * insures the replacement honors the underlying interface declaration.
     * 
     * @return void
     * @throws \Oroboros\core\exception\core\InvalidClassException If the class 
     *         constant `EXPECTED_INTERFACE` is overridden with an invalid value.
     *         This indicates that the class is not usable, which will produce an
     *         InvalidClassException, which should be treated as a fatal error.
     */
    private function validateDeclaredInterface(): void
    {
        if (static::EXPECTED_INTERFACE === self::EXPECTED_INTERFACE) {
            return;
        }
        $expected_interface = self::EXPECTED_INTERFACE;
        if (!interface_exists(static::EXPECTED_INTERFACE)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. '
                        . 'Provided interface designated by class constant [%2$s]'
                        . ' with value [%3$s] is not a valid interface.'
                        . ' This class is unuseable in it\'s current form.'
                        , get_class($this), 'EXPECTED_INTERFACE', static::EXPECTED_INTERFACE)
            );
        }
        $interfaces = class_implements(static::EXPECTED_INTERFACE);
        if (!($interfaces && in_array(self::EXPECTED_INTERFACE, $interfaces))) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. '
                        . 'Provided interface designated by class constant [%2$s]'
                        . ' with value [%3$s] does not extend expected interface [%4$s].'
                        . ' This class is unuseable in it\'s current form.'
                        , get_class($this), 'EXPECTED_INTERFACE', static::EXPECTED_INTERFACE, self::EXPECTED_INTERFACE)
            );
        }
    }
}
