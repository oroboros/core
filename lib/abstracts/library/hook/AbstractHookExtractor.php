<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\hook;

/**
 * Abstract Hook Extractor
 * Provides abstraction for extracting existing hooks from a given string.
 * 
 * This will provide a set of existing hooks
 * that can be replaced for the supplied string.
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
abstract class AbstractHookExtractor extends \Oroboros\core\abstracts\library\AbstractLibrary implements \Oroboros\core\interfaces\library\hook\HookExtractorInterface
{

    /**
     * Represents the hook match pattern to match valid hook format
     * between the suffixes for the supplied hook.
     * 
     * A regex will be constructed using the supplied prefix and suffix of
     * the supplied hook as literal strings, which will then be applied to
     * the content string to return matches.
     */
    public const MATCH_BASE_PATTERN = '([a-zA-Z0-9]{1,}(?:[\-\_][a-zA-Z0-9]{1,}){0,})';

    /**
     * The class name of the default pattern match hook if none is supplied.
     * The extractor will check to insure that the supplied hook is a valid 
     * class name that implements the HookInterface.
     * 
     * If this is the case, it will obtain it's prefix and suffix from it's 
     * class constants to construct the final search regex for the given hook set.
     * 
     * An object representation is not required,
     * information required can be obtained statically.
     * 
     * Class definitions cannot use stub names, they must be fully qualified.
     */
    public const DEFAULT_MATCH_HOOK_DEFINITION = 'Oroboros\\core\\abstracts\\library\\hook\\AbstractHook';

    /**
     * The interface that the given hook classname is expected to honor.
     * This may be overridden with any other interface,
     * provided the replacement extends this interface.
     * 
     * Interface constraints may be tightened, but not loosened.
     * 
     * Interface definitions cannot use stub names, they must be fully qualified.
     */
    public const EXPECTED_INTERFACE = 'Oroboros\\core\\interfaces\\library\\hook\\HookInterface';

    /**
     * The constructed match regex for extracting hooks
     * requested within a given string.
     * 
     * This will be automatically generated from a supplied valid hook classname,
     * or from the default abstract if none is supplied.
     * 
     * @var string
     */
    private $match_regex = null;

    /**
     * Resolves to a classname of a hook class that implements the 
     * given interface designating validity.
     * 
     * If none is supplied, this will resolve to `static::DEFAULT_MATCH_HOOK_DEFINITION`
     * 
     * @var string
     */
    private $classname = null;

    /**
     * Standard Oroboros Constructor
     * Accepts one optional argument in `$arguments` with a key named "hook"
     * that resolves to a valid class implementing the hook interface.
     * 
     * If this argument is not supplied, the default hook class 
     * will be used as per `static::DEFAULT_MATCH_HOOK_DEFINITION`.
     * 
     * It is not neccessary to supply an object or concrete class.
     * Any classname of a class implementing the HookInterface is sufficient,
     * or any stub class name that resolves to one.
     * 
     * @param string $command
     * @param array $arguments
     * @param array $flags
     */
    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->validateDeclaredInterface();
        $this->initializeHook();
    }

    /**
     * Returns a boolean designation as to whether the given
     * string contains hooks as per the current definitions.
     * 
     * @param string $string The string to evaluate
     * @return bool
     */
    public function hasKeys(string $string): bool
    {
        return !empty($this->getKeys($string));
    }

    /**
     * Returns an indexed array of hook keys, if any exists.
     * Return array will be empty if none exist.
     * 
     * @param string $string
     * @return array
     */
    public function getKeys(string $string): array
    {
        $regex = $this->getRegex();
        $matches = [];
        $results = [];
        $result = preg_match_all($regex, $string, $matches, PREG_PATTERN_ORDER);
        if ($result) {
            foreach ($matches[1] as $match) {
                $results[] = $match;
            }
        }
        return $results;
    }

    /**
     * Returns an indexed array of literal hooks, as they appear
     * within the string literal.
     * Return array will be empty if none exist.
     * @param string $string
     * @return array
     */
    public function getLiteral(string $string): array
    {
        $regex = $this->getRegex();
        $matches = [];
        $results = [];
        $result = preg_match_all($regex, $string, $matches, PREG_PATTERN_ORDER);
        if ($result) {
            foreach ($matches[0] as $match) {
                $results[] = $match;
            }
        }
        return $results;
    }

    /**
     * Returns the regular expression used to extract
     * hook definitions in the current scope.
     * 
     * @return string
     */
    protected function getRegex(): string
    {
        return $this->match_regex;
    }

    /**
     * Validates the declared hook class, and constructs the
     * appropriate regex definition if it is valid.
     * 
     * If it is not valid, an exception will be raised.
     * 
     * @param string $classname
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException via `validateHookClass`
     */
    protected function initializeHookDeclaration(string $classname): void
    {
        $this->validateHookClass($classname);
        $this->classname = $classname;
        $this->constructMatchRegex();
    }

    /**
     * Constructs the hook pattern matching regex from the declared base
     * and the delimiters of the declared hook class.
     * @return void
     */
    private function constructMatchRegex(): void
    {
        $open_delimiter = implode('\\', str_split($this->classname::OPEN_BRACE));
        $close_delimiter = implode('\\', str_split($this->classname::CLOSE_BRACE));
        $match_base = static::MATCH_BASE_PATTERN;
        $regex = sprintf('/%1$s%2$s%3$s/', $open_delimiter, $match_base, $close_delimiter);
        $this->match_regex = $regex;
    }

    /**
     * Checks that a given classname implements the expected hook interface
     * 
     * @param string $classname
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException if classname is
     *         not a class, or does not implement the expected interface
     */
    private function validateHookClass(string $classname): void
    {
        $expected_interface = static::EXPECTED_INTERFACE;
        if (!class_exists($classname)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. '
                        . 'Provided classname [%2$s] is not a valid class'
                        . ' or stub class name.'
                        , get_class($this), $classname)
            );
        }
        $interfaces = class_implements($classname);
        if (!($interfaces && in_array($expected_interface, $interfaces))) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. '
                        . 'Provided classname [%2$s] does not implement '
                        . 'expected interface [%3$s].'
                        , get_class($this), $classname, $expected_interface)
            );
        }
    }

    /**
     * If the interface constant declaration is overridden,
     * insures the replacement honors the underlying interface declaration.
     * 
     * @return void
     * @throws \Oroboros\core\exception\core\InvalidClassException If the class 
     *         constant `EXPECTED_INTERFACE` is overridden with an invalid value.
     *         This indicates that the class is not usable, which will produce an
     *         InvalidClassException, which should be treated as a fatal error.
     */
    private function validateDeclaredInterface(): void
    {
        if (static::EXPECTED_INTERFACE === self::EXPECTED_INTERFACE) {
            return;
        }
        $expected_interface = self::EXPECTED_INTERFACE;
        if (!interface_exists(static::EXPECTED_INTERFACE)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. '
                        . 'Provided interface designated by class constant [%2$s]'
                        . ' with value [%3$s] is not a valid interface.'
                        . ' This class is unuseable in it\'s current form.'
                        , get_class($this), 'EXPECTED_INTERFACE', static::EXPECTED_INTERFACE)
            );
        }
        $interfaces = class_implements(static::EXPECTED_INTERFACE);
        if (!($interfaces && in_array(self::EXPECTED_INTERFACE, $interfaces))) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. '
                        . 'Provided interface designated by class constant [%2$s]'
                        . ' with value [%3$s] does not extend expected interface [%4$s].'
                        . ' This class is unuseable in it\'s current form.'
                        , get_class($this), 'EXPECTED_INTERFACE', static::EXPECTED_INTERFACE, self::EXPECTED_INTERFACE)
            );
        }
    }

    /**
     * Initializes the default setup upon instantiation, or the replacement
     * if an argument designating a custom hook class is supplied.
     * 
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException if the supplied
     *         argument with key "hook" is not a valid classname or object
     *         implementing the declared hook interface
     */
    private function initializeHook(): void
    {
        $classname = static::DEFAULT_MATCH_HOOK_DEFINITION;
        if ($this->hasArgument('hook')) {
            $class = $this->getArgument('hook');
            if (is_string($class)) {
                $classname = $class;
            } elseif (is_object($class)) {
                $classname = get_class($class);
            } else {
                throw new \Oroboros\core\exception\InvalidArgumentException(
                        sprintf('Error encountered in [%1$s]. Supplied argument [%2$s] '
                            . 'is not valid. Must supply an object or string resolving '
                            . 'to a classname that implements [%3$s]. Type recieved: [%4$s]'
                            , get_class($this), 'hook', static::EXPECTED_INTERFACE, gettype($class))
                );
            }
        }
        $this->initializeHookDeclaration($classname);
    }
}
