<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\hook;

/**
 * Abstract Hook
 * Provides abstraction for a hook object.
 * 
 * Hooks allow dynamic replacement of values within a static configuration 
 * using predefined keys and pattern matching.
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
abstract class AbstractHook extends \Oroboros\core\abstracts\library\AbstractLibrary implements \Oroboros\core\interfaces\library\hook\HookInterface
{

    /**
     * Defines the brace designated as declaring the opening of a hook.
     */
    const OPEN_BRACE = '{::';

    /**
     * Defines the brace designated as declaring the close of an open hook.
     */
    const CLOSE_BRACE = '::}';

    /**
     * The data to inject into the hook
     * 
     * @var scalar|null
     */
    private $data = null;

    /**
     * The key that the hook will replace if found
     * 
     * @var string
     */
    private $key = null;

    /**
     * Standard Oroboros Constructor
     * 
     * @param string $command Sets the key that will be used as the hook definition. Case sensitive.
     * @param array $arguments may contain a key `value` which will set the value if it exists.
     *                         If implementations require dependencies,
     *                         they should also be present as keys in this argument.
     * @param array $flags optional flags to modify operations internally as per implementation
     */
    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeKey();
        $this->initializeValue();
    }
    
    /**
     * Replaces any instances of the hook definition in the supplied string with the hook value.
     * If no instances exist, returns the string exactly as it was supplied.
     * This method is exception safe.
     * 
     * @param string $string
     * @return string
     */
    public function replace(string $string)
    {
        if ($this->isMatch($string)) {
            $string = str_replace($this->getHook(), $this->getData(), $string);
        }
        return $string;
    }
    
    /**
     * Removes any instances of the supplied hook from the supplied string entirely,
     * without replacing them with any data.
     * If no instances exist, returns the string exactly as it was supplied.
     * This method is exception safe.
     * 
     * @param string $string
     * @return string
     */
    public function nullify(string $string): string
    {
        if ($this->isMatch($string)) {
            $string = str_replace($this->getHook(), null, $string);
        }
        return $string;
    }
    
    /**
     * Returns a boolean designation as to whether a supplied string has matches for this hook.
     * This method is exception safe.
     * 
     * @param string $string
     * @return bool
     */
    public function isMatch(string $string): bool
    {
        return strpos($string, $this->getHook()) !== false;
    }

    /**
     * Provides the key that will be matched within the hook definition.
     * This method is exception safe.
     * 
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }
    
    /**
     * Returns the exact hook that will be matched if it exists.
     * This method is exception safe.
     * 
     * @return string
     */
    public function getHook(): string
    {
        return $this->getConstructedHookDefinition();
    }

    /**
     * Provides the value that will be injected into a match.
     * This method is exception safe.
     * 
     * @return scalar|null
     */
    public function getValue()
    {
        return $this->getData();
    }

    /**
     * Sets the data that will be injected into a hook match.
     * 
     * @param type $data
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException if the given data is not scalar or null
     */
    protected function setData($data): void
    {
        $this->data = $data;
    }

    /**
     * Returns the currently set data
     * 
     * @return scalar|null
     */
    protected function getData()
    {
        return $this->data;
    }

    /**
     * Returns the constructed hook definition,
     * as it will be searched for within a string of supplied text.
     * 
     * @return string
     */
    private function getConstructedHookDefinition(): string
    {
        return sprintf('%1$s%2$s%3$s', static::OPEN_BRACE, $this->getKey(), static::CLOSE_BRACE);
    }

    /**
     * Sets the key the hook is designated to match.
     * 
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException if no key name is provided to the constructor
     */
    private function initializeKey(): void
    {
        if (is_null($this->getCommand())) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. A command must be provided '
                        . 'designating what hook this object is intended to honor. Received null.')
            );
        }
        $this->key = $this->getCommand();
    }

    /**
     * Sets the value the hook is designated to match.
     * This will search the constructor arguments (second parameter) for a key 
     * named "value", and attempt to set that as the hook value via `setData`
     * 
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException if the value passed is not scalar or null (via `setData`)
     */
    private function initializeValue(): void
    {
        if ($this->hasArgument('value')) {
            $this->setData($this->getArgument('value'));
        }
    }
}
