<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\file;

/**
 * Abstract File Writer
 * Provides abstraction for writing file contents.
 * This provides stream-based file writing capacities,
 * to prevent memory overhead issues or blocking operations.
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
abstract class AbstractFileWriter extends \Oroboros\core\abstracts\library\AbstractLibrary implements \Oroboros\core\interfaces\library\file\FileWriterInterface
{

    /**
     * The filename of the file.
     * By default, this will be extracted from the `$command` argument of the constructor.
     * @var string
     */
    private $file = null;

    /**
     * The directory path leading to the file.
     * By default, this will be extracted from the `$command` argument of the constructor.
     * @var type
     */
    private $directory = null;

    /**
     * The currently scoped options for file writing
     * @var array
     */
    private $options = null;

    /**
     * The stream resource for writing to, when it is in use.
     * This will not be left open when not in use.
     * @var resource|null
     */
    private $pointer = null;
    private static $default_options = [
        // Use an exclusive lock strategy for flock
        'lock-mode' => LOCK_EX,
        // The write mode to use for fopen. All valid php methods that write are valid. Read-only variants are not valid.
        'write-mode' => 'wb+',
        // Do not generate missing directories
        'generate-directories' => false,
        // Whether to resolve paths that contain ../ to navigate up the directory tree
        // This is set to false for security considerations, and must be explicitly enabled
        'resolve-backpathing' => false,
        // The permission to set on created files
        'file-permission' => self::DEFAULT_FILE_PERMISSION,
        // The permission to set on created directories
        'directory-permission' => self::DEFAULT_DIRECTORY_PERMISSION,
    ];

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeOptions();
        $this->initializeFile();
    }

    /**
     * Sets the name of the currently scoped file
     * @param string $filename
     * @return void
     */
    public function setFile(string $filename): void
    {
        $this->file = $filename;
    }

    /**
     * Returns the currently scoped filename
     * @return string
     * @throws \ErrorException if no filename has been supplied
     */
    public function getFile(): string
    {
        if (is_null($this->file)) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. Cannot return the file name because no filename has been supplied.'));
        }
        return $this->file;
    }

    /**
     * Returns the currently scoped directory
     * @return string
     * @throws \ErrorException if no filename has been supplied
     */
    public function getDirectory(): string
    {
        if (is_null($this->directory)) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. Cannot return the directory name because no directory has been supplied.'));
        }
        return $this->directory;
    }

    /**
     * returns the full path to the currently scoped file.
     * @return string
     * @throws \ErrorException if no filename or no directory have been supplied
     */
    public function getFullPath(): string
    {
        return $this->getDirectory() . $this->getFile();
    }

    /**
     * Sets the path of the currently scoped directory
     * @param string $directory
     * @return void
     */
    public function setDirectory(string $directory): void
    {
        $this->validateDirectory($directory);
        $this->directory = $directory;
    }

    /**
     * Returns a boolean designation as to whether
     * the object can be used to write in its current state.
     * 
     * If this returns true, a write operation should occur without error or exceptions.
     * 
     * If this returns false, a write operation attempt will throw an ErrorException.
     * 
     * @return bool Returns true if a write operation can occur in the current state, false otherwise
     */
    public function isWriteable(): bool
    {
        try {
            $this->getFullPath();
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }

    /**
     * Returns the current value of a requested option key
     * @param string $key The name of the requested key
     * @return mixed
     * @throws \InvalidArgumentException if an invalid option key is supplied
     */
    public function getOption(string $key)
    {
        if (!array_key_exists($key, $this->options)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Provided option key [%2$s] is not valid. Valid keys are [%3$s].', get_class($this), $key, implode(', ', array_keys($this->options))));
        }
        return $this->options[$key];
    }

    /**
     * Sets a file write option.
     * @param string $key
     * @param type $value
     * @return void
     * @throws \InvalildArgumentException If the keyname is not known, or the supplied value is not valid for the given key
     */
    public function setOption(string $key, $value): void
    {
        $this->validateOption($key, $value);
        $this->options[$key] = $value;
    }

    /**
     * Writes the given content to the file.
     * @param string $content The content to write to the file.
     * @return void
     * @throws \ErrorException If there is no currently writeable file and directory set for the current options.
     */
    public function write(string $content): void
    {
        if (!$this->isWriteable()) {
            $this->getLogger()->warning('[type][scope][class] File [file] is not writable in this scope.', [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
                'file' => $this->getFullPath(),
            ]);
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. No valid writeable definition currently declared.'));
        }
        $this->buildDirectories();
        $this->openFileStream();
        $this->lockFileStream();
        $this->writeToFileStream($content);
        $this->closeFileStream();
        $this->getLogger()->debug('[type][scope][class] Wrote to file [file].', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'file' => $this->getFullPath(),
        ]);
    }

    /**
     * Copies a given stream resource to the file.
     * @param resource $stream A stream resource to copy to the file.
     * @return void
     * @throws \ErrorException If there is no currently writeable file and directory set for the current options.
     * @throws \InvalidArgumentException If `$stream` is not a resource.
     */
    public function stream($stream): void
    {
        if (!$this->isWriteable()) {
            $this->getLogger()->warning('[type][scope][class] File [file] is not writable in this scope.', [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
                'file' => $this->getFullPath(),
            ]);
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. No valid writeable definition currently declared.'));
        }
        if (!is_resource($stream)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Method [%2$s] requires a resource, received [%3$s].', get_class($this), __METHOD__, gettype($stream)));
        }
        $this->buildDirectories();
        $this->openFileStream();
        $this->lockFileStream();
        $this->streamToFileStream($stream);
        $this->closeFileStream();
        $this->getLogger()->debug('[type][scope][class] Wrote to stream to file [file].', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'file' => $this->getFullPath(),
        ]);
    }

    /**
     * Checks that given options are valid before setting them
     * @param string $option The name of the option key
     * @param type $value The value to evaluate
     * @return void
     * @throws \InvalidArgumentException if a given option value is not a valid option
     */
    protected function validateOption(string $option, $value): void
    {
        switch ($option) {
            case 'lock-mode':
                $valid = [LOCK_EX, LOCK_EX | LOCK_NB];
                break;
            case 'write-mode':
                $valid = ['r+', 'rb+', 'w', 'w+', 'wb', 'wb+', 'a', 'a+', 'ab', 'ab+', 'x', 'x+', 'xb', 'xb+', 'c', 'c+', 'cb', 'cb+'];
                break;
            case 'generate-directories':
                $valid = [true, false];
            case 'resolve-backpathing':
                $valid = [true, false];
                break;
            case 'file-permission':
            case 'directory-permission':
                if (!is_int($value) && decoct(octdec($value)) == $value) {
                    throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Provided value [%2$s] for option key [%3$s] is invalid. Only octal values are allowed.', get_class($this), (string) $value, $option));
                }
                return;
            default:
                throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Provided option [%2$s] is not a valid file option key. Valid option keys are [%3$s].', get_class($this), $option, implode(', ', array_keys(self::$default_options))));
        }
        if (!in_array($value, $valid)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Provided value [%2$s] for option key [%3$s] is invalid. Valid values are [%4$s].', get_class($this), (string) $value, $option, implode(', ', $valid)));
        }
    }

    private function initializeFile(): void
    {
        $filename = $this->getCommand();
        if (is_null($filename)) {
            return;
        }
        $this->setFile($this->getFilename($filename));
        if ($this->file !== $filename) {
            $directory = substr($filename, 0, strrpos($filename, $this->file));
            $this->setDirectory($directory);
        }
    }

    private function getFilename(string $path): string
    {
        $separator = DIRECTORY_SEPARATOR;
        if (strpos($path, $separator) !== false) {
            $path = substr($path, strrpos($path, $separator) + 1);
        }
        return $path;
    }

    private function resolveDirectory(string $path): string
    {
        $separator = DIRECTORY_SEPARATOR;
        if (strpos($path, $separator) !== false) {
            $path = substr($path, 0, strrpos($path, $separator));
        }
        return $path;
    }

    private function validateDirectory(string $directory): void
    {
        if ($this->getOption('resolve-backpathing') === false) {
            // Check the directory for backwards path navigation
            if (strpos($directory, '..') !== false) {
                throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Backwards path navigation is not allowed in directory path [%2$s] under the current settings.', get_class($this), $directory));
            }
        }
        if ($this->getOption('generate-directories') === false && !is_dir($directory)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Supplied directory path [%2$s] does not exist, and directory generation is not enabled. This cannot resolve to a writeable file.', get_class($this), $directory));
        }
        if ($this->getOption('generate-directories') === false && is_dir($directory) && !is_writable($directory)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Supplied directory path [%2$s] is not writeable for current system user [%3$s]. This cannot resolve to a writeable file.', get_class($this), $directory, $this::SYSTEM_USER));
        }
        if ($this->getOption('generate-directories') === true && !is_dir($directory)) {
            // Check if there is a writeable root path
            $separator = DIRECTORY_SEPARATOR;
            $path = $directory;
            while (realpath($path) === false) {
                $path = substr($path, 0, strrpos($path, $separator));
                if (is_dir($path) && !is_writable($path)) {
                    throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Supplied directory path [%2$s] does not have a writeable base folder for current system user [%3$s]. This cannot resolve to a writeable file.', get_class($this), $directory, $this::SYSTEM_USER));
                }
                if ($path === '') {
                    throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. No writeable base directory found for supplied directory path [%2$s]. This cannot resolve to a writeable file.', get_class($this), $directory, $this::SYSTEM_USER));
                }
            }
        }
    }

    private function initializeOptions(): void
    {
        $options = self::$default_options;
        foreach ($this->getArguments() as $arg => $value) {
            if (array_key_exists($arg, self::$default_options)) {
                $this->validateOption($arg, $value);
                $options[$arg] = $value;
            }
        }
        $this->options = $options;
    }

    private function buildDirectories(): void
    {
        if ($this->getOption('generate-directories') !== true) {
            // scaffolding directories is disabled
            return;
        }
        if (realpath($this->getDirectory()) !== false) {
            // nothing to do
            return;
        }
        $this->getLogger()->debug('[type][scope][class] Building directory structure for [file].', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'file' => $this->getFullPath(),
        ]);
        $path = $this->getDirectory();
        $base = $this->getWritableBaseDirectory($path);
        $missing_dirs = trim(str_replace($base, null, $path), DIRECTORY_SEPARATOR);
        $permission = $this->getOption('directory-permission');
        if (is_string($permission)) {
            $permission = octdec($permission);
        }
        while (rtrim($path, DIRECTORY_SEPARATOR) !== rtrim($base, DIRECTORY_SEPARATOR)) {
            $segment = DIRECTORY_SEPARATOR . substr($missing_dirs, 0, (strpos($missing_dirs, DIRECTORY_SEPARATOR) !== false ? strpos($missing_dirs, DIRECTORY_SEPARATOR) : strlen($missing_dirs)));
            $base = $base . $segment;
            mkdir($base, $permission);
            chmod($base, $permission);
            $missing_dirs = trim(substr($missing_dirs, strpos($missing_dirs, DIRECTORY_SEPARATOR)), DIRECTORY_SEPARATOR);
        }
    }

    private function getWritableBaseDirectory(string $destination_path): string
    {
        $path = $destination_path;
        while (realpath($path) === false) {
            $path = substr($path, 0, strrpos($path, DIRECTORY_SEPARATOR));
            if ($path === '') {
                throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. No writeable base directory found for supplied directory path [%2$s]. This cannot resolve to a writeable file.', get_class($this), $directory, $this::SYSTEM_USER));
            }
        }
        $this->getLogger()->debug('[type][scope][class] Writable base directory found at [path].', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'path' => $path,
        ]);
        return $path;
    }

    private function openFileStream(): void
    {
        $chmod = false;
        $permission = $this->getOption('file-permission');
        if (is_string($permission)) {
            $permission = octdec($permission);
        }
        $write_mode = $this->getOption('write-mode');
        $file = $this->getFullPath();
        if (!file_exists($file)) {
            $chmod = true;
        }
        $pointer = fopen($file, $write_mode, false);
        $this->pointer = $pointer;
        if ($chmod) {
            chmod($file, $permission);
        }
    }

    private function closeFileStream(): void
    {
        flock($this->pointer, LOCK_UN);
        fclose($this->pointer);
        $this->pointer = null;
        clearstatcache(true, $this->getFullPath());
    }

    private function lockFileStream(): void
    {
        $lock_mode = $this->getOption('lock-mode');
        flock($this->pointer, $lock_mode);
    }

    private function writeToFileStream(string $content): void
    {
        fwrite($this->pointer, $content);
    }

    private function streamToFileStream($stream): void
    {
        stream_copy_to_stream($stream, $this->pointer);
    }
}
