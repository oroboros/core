<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\environment;

/**
 * Abstract Environment Builder
 * Provides abstraction for building composite containers of environment variables
 *
 * @author Brian Dayhoff
 */
abstract class AbstractEnvironmentBuilder extends \Oroboros\core\abstracts\pattern\builder\AbstractBuilder implements \Oroboros\core\interfaces\library\environment\EnvironmentBuilderInterface
{

    use \Oroboros\core\traits\StringUtilityTrait;

    const CLASS_SCOPE = 'environment';
    const WORKER_SCOPE = 'environment';
    const WORKER_TASK = 'environment';

    /**
     * Environment variable prefixes used to categorize environment variables
     * Key: prefix, value: base container key
     * @var array
     */
    private static $prefixes = [
        'OROBOROS_CORE_' => 'baseline',
        'APP_' => 'application',
    ];

    /**
     * Environment variable keys that should always be omitted if present.
     * @var array
     */
    private static $blacklist_keys = [
        'SYMFONY_DOTENV_VARS'
    ];

    public function executeTask(array $arguments = [], array $flags = [])
    {
        $result = $this->containerizeInto('Oroboros\\core\\library\\container\\EnvironmentCollection');
        $environment = $this->get('environment');
        foreach ($arguments as $key => $value) {
            $environment[$key] = $value;
        }
        $parsed = \Oroboros\core\library\environment\EnvironmentFormatter::format($environment->toArray());
        foreach ($this->filterPrefixes($parsed) as $key => $value) {
            $result[$key] = $this->containerizeInto('Oroboros\\core\\library\\container\\EnvironmentContainer', $value);
        }
        return $result;
    }

    protected function declareCategories(): array
    {
        return [
            'environment' => ['string', false]
        ];
    }

    private function filterPrefixes(array $data): array
    {
        $result = [];
        foreach ($data as $key => $value) {
            if (in_array($key, self::$blacklist_keys)) {
                continue;
            }
            $container_key = 'system';
            foreach (self::$prefixes as $prefix => $container) {
                if (strpos($key, $prefix) === 0) {
                    $container_key = $container;
                    $key = substr($key, strlen($prefix));
                    break;
                }
            }
            if (!array_key_exists($container_key, $result)) {
                $result[$container_key] = [];
            }
            if ($container_key === 'system') {
                $result[$container_key][strtolower($key)] = $value;
                continue;
            }
            $this->packageValueTree($key, $value, $result[$container_key]);
        }
        return $result;
    }

    private function packageValueTree(string $key, $value, array &$existing, array $keyset = null): void
    {
        if (is_null($keyset)) {
            $keyset = explode('_', $key);
            if (strpos($key, '_') !== false) {
                $key = substr($key, 0, strpos($key, '_'));
                array_shift($keyset);
            }
        }
        if (empty($keyset)) {
            $existing[strtolower($key)] = $value;
        } else {
            $next = array_shift($keyset);
            if (!array_key_exists(strtolower($key), $existing)) {
                $existing[strtolower($key)] = [];
            }
            if (empty($keyset) && strtolower($key) === strtolower($next)) {
                $existing[strtolower($next)] = $value;
            } else {
                $this->packageValueTree($next, $value, $existing[strtolower($key)], $keyset);
            }
        }
    }
}
