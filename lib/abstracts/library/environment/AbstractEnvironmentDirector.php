<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\environment;

/**
 * Abstract Environment Director
 * Provides abstraction for composite sets of environment variables
 *
 * @author Brian Dayhoff
 */
abstract class AbstractEnvironmentDirector extends \Oroboros\core\abstracts\pattern\director\AbstractDirector implements \Oroboros\core\interfaces\library\environment\EnvironmentDirectorInterface
{

    const CLASS_SCOPE = 'environment';
    const EXPECTED_WORKER_INTERFACE = 'Oroboros\\core\\interfaces\\library\\environment\\EnvironmentBuilderInterface';

    public function build(array $flags = [])
    {
        return $this->getWorker('environment')->executeTask();
    }
}
