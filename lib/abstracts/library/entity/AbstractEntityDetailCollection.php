<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\entity;

/**
 * Abstract Entity Detail Container
 * Provides abstraction for categories of entity details
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
abstract class AbstractEntityDetailCollection extends \Oroboros\core\abstracts\library\data\AbstractDataCollection implements \Oroboros\core\interfaces\library\entity\EntityDetailCollectionInterface
{

    use \Oroboros\core\traits\library\entity\EntityCommonTrait;
    use \Oroboros\core\traits\library\auth\AuthCommonUtilityTrait;
    use \Oroboros\core\traits\library\entity\EntityDetailMatchableTrait;
    use \Oroboros\core\traits\library\resolver\ResolverUtilityTrait;
    use \Oroboros\core\traits\LoaderTrait;
    use \Oroboros\core\traits\ContainerPackagerUtility;
    

    /**
     * Use the default resolver
     */
    const RESOLVER_CLASS = 'resolver\\ArgumentResolver';

    /**
     * This constant must be overridden by
     * concrete implementations to declare auth scope
     */
    const AUTH_SCOPE = AbstractEntity::AUTH_SCOPE;

    /**
     * Defines the class scope as entity details
     */
    const CLASS_SCOPE = 'entity-details';

    /**
     * Abstraction must provide an entity type
     */
    const ENTITY_TYPE = null;

    /**
     * Entity scope for detail containers is details categories
     */
    const ENTITY_SCOPE = 'details-categories';

    /**
     * Abstraction may optionally provide an entity category
     */
    const ENTITY_CATEGORY = null;

    /**
     * Abstraction may optionally provide an entity subcategory
     */
    const ENTITY_SUBCATEGORY = null;

    /**
     * Entity context is details
     */
    const ENTITY_CONTEXT = 'details';

    /**
     * Verifies that the given keys are only credential objects.
     * 
     * @param type $key
     * @param type $value
     * @param type $method
     * @return void
     * @throws \Oroboros\core\exception\container\ContainerException
     *         Key must be a string, and value must be an instance of
     *         `\Oroboros\core\interfaces\library\auth\EntityDetailInterface`
     */
    protected function onSet($key, $value, $method = null): void
    {
        if (!is_string($key)) {
            throw new \Oroboros\core\exception\container\ContainerException(
                    sprintf('Error encountered in [%1$s]. Provided key '
                        . 'must be a string.'
                        , get_class($this))
            );
        }
        parent::onSet($key, $value, $method);
    }

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeAuthCommon();
    }

    public function __toString(): string
    {
        return json_encode($this);
    }

    /**
     * Constrain acceptable values to only permission containers.
     * @return string
     */
    public function getExpectedContainerType()
    {
        return \Oroboros\core\interfaces\library\entity\EntityDetailContainerInterface::class;
    }
}
