<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\entity;

/**
 * Abstract Entity Detail Container
 * Provides abstraction for sets of related entity details
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
abstract class AbstractEntityMetaData extends AbstractEntityDetailContainer implements \Oroboros\core\interfaces\library\entity\EntityMetaDataInterface
{

    /**
     * Use the default resolver
     */
    const RESOLVER_CLASS = 'resolver\\ArgumentResolver';

    /**
     * Sets the entity category as metadata
     */
    const ENTITY_CATEGORY = 'metadata';

    /**
     * The default class used to generate the meta type key
     */
    const METADATA_DEFAULT_TYPE_CLASS = \Oroboros\core\library\entity\meta\EntityType::class;

    /**
     * The default class used to generate the meta scope key
     */
    const METADATA_DEFAULT_SCOPE_CLASS = \Oroboros\core\library\entity\meta\EntityScope::class;

    /**
     * The default class used to generate the meta category key
     */
    const METADATA_DEFAULT_CATEGORY_CLASS = \Oroboros\core\library\entity\meta\EntityCategory::class;

    /**
     * The default class used to generate the meta sub-category key
     */
    const METADATA_DEFAULT_SUBCATEGORY_CLASS = \Oroboros\core\library\entity\meta\EntitySubCategory::class;

    /**
     * The default class used to generate the meta context key
     */
    const METADATA_DEFAULT_CONTEXT_CLASS = \Oroboros\core\library\entity\meta\EntityContext::class;

    /**
     * Verifies that the given keys are only credential objects.
     * 
     * @param type $key
     * @param type $value
     * @param type $method
     * @return void
     * @throws \Oroboros\core\exception\container\ContainerException
     *         Key must be a string, and value must be an instance of
     *         `\Oroboros\core\interfaces\library\auth\EntityDetailInterface`
     */
    protected function onSet($key, $value, $method = null): void
    {
        if (is_null($value)) {
            parent::onSet($key, $value, $method);
            return;
        }
        if ($key === 'entity' && (is_object($value) && ($value instanceof \Oroboros\core\interfaces\library\entity\EntityInterface))) {
            parent::onSet($key, $value, $method);
            $this->setEntity($value);
            return;
        }
        $expected = \Oroboros\core\interfaces\library\entity\EntityDetailInterface::class;
        if (!is_string($key)) {
            throw new \Oroboros\core\exception\container\ContainerException(
                    sprintf('Error encountered in [%1$s]. Provided key '
                        . 'must be a string.'
                        , get_class($this))
            );
        }
        if (!is_object($value) && ($value instanceof $expected)) {
            throw new \Oroboros\core\exception\container\ContainerException(
                    sprintf('Error encountered in [%1$s]. Provided value for key '
                        . '[%2$s] must be an instance of [%3$s].'
                        , get_class($this), $key, $expected)
            );
        }
        parent::onSet($key, $value, $method);
    }

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
    }

    public function setEntity(\Oroboros\core\interfaces\library\entity\EntityInterface $entity): void
    {
        $this->resetEntity();
        $this->evaluateEntity($entity);
    }

    protected function evaluateEntity(\Oroboros\core\interfaces\library\entity\EntityInterface $entity): void
    {
        $this->entity = $entity;
        $this->generateEntityType($entity);
        $this->generateEntityScope($entity);
        $this->generateEntityCategory($entity);
        $this->generateEntitySubCategory($entity);
        $this->generateEntityContext($entity);
    }

    protected function resetEntity(): void
    {
        $this->entity = null;
        $this['type'] = null;
        $this['scope'] = null;
        $this['category'] = null;
        $this['sub-category'] = null;
        $this['context'] = null;
    }

    private function generateEntityType(\Oroboros\core\interfaces\library\entity\EntityInterface $entity): void
    {
        $value = $entity->getEntityType();
        $meta = $this->load('library', static::METADATA_DEFAULT_TYPE_CLASS, $value, $this->getArguments(), $this->getFlags());
        $meta->setLogger($this->getLogger());
        $this['type'] = $meta;
    }

    private function generateEntityScope(\Oroboros\core\interfaces\library\entity\EntityInterface $entity): void
    {
        $value = $entity->getEntityScope();
        $meta = $this->load('library', static::METADATA_DEFAULT_SCOPE_CLASS, $value, $this->getArguments(), $this->getFlags());
        $meta->setLogger($this->getLogger());
        $this['scope'] = $meta;
    }

    private function generateEntityCategory(\Oroboros\core\interfaces\library\entity\EntityInterface $entity): void
    {
        $value = $entity->getEntityCategory();
        $meta = $this->load('library', static::METADATA_DEFAULT_CATEGORY_CLASS, $value, $this->getArguments(), $this->getFlags());
        $meta->setLogger($this->getLogger());
        $this['category'] = $meta;
    }

    private function generateEntitySubCategory(\Oroboros\core\interfaces\library\entity\EntityInterface $entity): void
    {
        $value = $entity->getEntitySubCategory();
        $meta = $this->load('library', static::METADATA_DEFAULT_SUBCATEGORY_CLASS, $value, $this->getArguments(), $this->getFlags());
        $meta->setLogger($this->getLogger());
        $this['sub-category'] = $meta;
    }

    private function generateEntityContext(\Oroboros\core\interfaces\library\entity\EntityInterface $entity): void
    {
        $value = $entity->getEntityContext();
        $meta = $this->load('library', static::METADATA_DEFAULT_CONTEXT_CLASS, $value, $this->getArguments(), $this->getFlags());
        $meta->setLogger($this->getLogger());
        $this['context'] = $meta;
    }
}
