<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\entity;

/**
 * Abstract Entity
 * Provides baseline abstraction for entity objects
 *
 * @author Brian Dayhoff
 */
abstract class AbstractEntity extends \Oroboros\core\abstracts\library\AbstractLibrary implements \Oroboros\core\interfaces\library\entity\EntityInterface
{

    use \Oroboros\core\traits\library\entity\EntityCommonTrait {
        \Oroboros\core\traits\library\entity\EntityCommonTrait::initializeEntity as private traitInitializeEntity;
    }

    /**
     * Auth scope is entity
     */
    const AUTH_SCOPE = 'entity';

    /**
     * Defines the class scope as entity
     */
    const CLASS_SCOPE = 'entity';

    /**
     * Abstraction must provide an entity type
     */
    const ENTITY_TYPE = null;

    /**
     * Abstraction may optionally provide an entity scope
     */
    const ENTITY_SCOPE = null;

    /**
     * Abstraction may optionally provide an entity category
     */
    const ENTITY_CATEGORY = null;

    /**
     * Abstraction may optionally provide an entity subcategory
     */
    const ENTITY_SUBCATEGORY = null;

    /**
     * Abstraction may optionally provide an entity context
     */
    const ENTITY_CONTEXT = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeEntity();
    }

    /**
     * Designates the container class used to house a related set of entity details
     */
    const ENTITY_DETAILS_CONTAINER_CLASS = \Oroboros\core\library\entity\EntityDetailContainer::class;

    /**
     * Designates the collection class used to house all entity details
     */
    const ENTITY_DETAILS_COLLECTION_CLASS = \Oroboros\core\library\entity\EntityDetailCollection::class;

    /**
     * Designates the collection class used to house all entity details
     */
    const ENTITY_METADATA_CLASS = \Oroboros\core\library\entity\meta\EntityMetaData::class;

    /**
     * The master set of entity details
     * 
     * @var \Oroboros\core\interfaces\library\entity\EntityDetailCollectionInterface
     */
    private $details = null;

    public function details(): ?\Oroboros\core\interfaces\library\entity\EntityDetailCollectionInterface
    {
        return $this->details;
    }

    protected function registerDetailSection(string $key, \Oroboros\core\interfaces\library\entity\EntityDetailContainerInterface $details = null): void
    {
        if (!is_null($details)) {
            $this->details[$key] = $details;
            return;
        }
        $class = static::ENTITY_DETAILS_CONTAINER_CLASS;
        $this->details[$key] = $class::init();
    }

    protected function unregisterDetailSection(string $key): void
    {
        if ($key === 'meta') {
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. Cannot unregister metadata.', get_class($this))
            );
        }
        if ($this->details->has($key)) {
            unset($details[$key]);
        }
    }

    private function initializeEntity(): void
    {
        $this->traitInitializeEntity();
        $this->initializeEntityDetailsCollection();
        $this->initializeEntityMeta();
    }

    private function initializeEntityDetailsCollection(): void
    {
        $class = static::ENTITY_DETAILS_COLLECTION_CLASS;
        $collection = $class::init();
        $this->details = $collection;
    }

    private function initializeEntityMeta(): void
    {
        $class = static::ENTITY_METADATA_CLASS;
        $meta = $class::init();
        $meta->setEntity($this);
        $this->registerDetailSection('meta', $meta);
    }
}
