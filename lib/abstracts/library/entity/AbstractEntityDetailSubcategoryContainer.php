<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\entity;

/**
 * Abstract Entity Detail Container
 * Provides abstraction for sets of related entity details
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
abstract class AbstractEntityDetailSubcategoryContainer extends AbstractEntityDetailContainer implements \Oroboros\core\interfaces\library\entity\EntityDetailSubCategoryContainerInterface
{

    use \Oroboros\core\traits\library\entity\EntityCommonTrait;
    use \Oroboros\core\traits\library\auth\AuthCommonUtilityTrait;
    use \Oroboros\core\traits\library\entity\EntityDetailMatchableTrait;
    use \Oroboros\core\traits\library\resolver\ResolverUtilityTrait;
    use \Oroboros\core\traits\LoaderTrait;

    use \Oroboros\core\traits\library\entity\EntityCommonTrait;
    use \Oroboros\core\traits\library\resolver\ResolverUtilityTrait;

    /**
     * Defines the class scope as entity
     */
    const CLASS_SCOPE = 'entity-details-subcategory';

    /**
     * Sets entity as the default type
     */
    const ENTITY_TYPE = 'entity';

    /**
     * Entity scope for detail containers is details category
     */
    const ENTITY_SCOPE = 'details-category';

    /**
     * Abstraction may optionally provide an entity category
     */
    const ENTITY_CATEGORY = null;

    /**
     * Abstraction may optionally provide an entity subcategory
     */
    const ENTITY_SUBCATEGORY = null;

    /**
     * Abstraction may optionally provide an entity context
     */
    const ENTITY_CONTEXT = 'subcategory';

    /**
     * This is the default keyname used if child classes
     * do not declare an alternate keyname.
     * 
     * For simple key name changes,
     * they may override this class constant.
     * 
     * For more complex keyname resolution,
     * they must override `declareEntityDetailKey`
     */
    const ENTITY_DETAIL_DEFAULT_KEY = 'undefined';

    /**
     * This is the default value used if child classes
     * do not declare an alternate default value.
     * 
     * For simple default value changes,
     * they may override this class constant.
     * 
     * For complex default values,
     * they must override `declareEntityDetailDefaultValue`
     */
    const ENTITY_DETAIL_DEFAULT_VALUE = null;

    /**
     * Use the default resolver
     */
    const RESOLVER_CLASS = 'resolver\\ArgumentResolver';

    /**
     * This constant must be overridden by
     * concrete implementations to declare auth scope
     */
    const AUTH_SCOPE = AbstractEntity::AUTH_SCOPE;

    /**
     * The entity represented by the detail container
     * @var \Oroboros\core\interfaces\library\entity\EntityInterface
     */
    private $entity = null;

    /**
     * Verifies that the given keys are only credential objects.
     * 
     * @param type $key
     * @param type $value
     * @param type $method
     * @return void
     * @throws \Oroboros\core\exception\container\ContainerException
     *         Key must be a string, and value must be an instance of
     *         `\Oroboros\core\interfaces\library\auth\EntityDetailInterface`
     */
    protected function onSet($key, $value, $method = null): void
    {
        $expected = \Oroboros\core\interfaces\library\entity\EntityDetailInterface::class;
        if (!is_string($key)) {
            throw new \Oroboros\core\exception\container\ContainerException(
                    sprintf('Error encountered in [%1$s]. Provided key '
                        . 'must be a string.'
                        , get_class($this))
            );
        }
        if (!is_object($value) && ($value instanceof $expected)) {
            throw new \Oroboros\core\exception\container\ContainerException(
                    sprintf('Error encountered in [%1$s]. Provided value for key '
                        . '[%2$s] must be an instance of [%3$s].'
                        , get_class($this), $key, $expected)
            );
        }
        parent::onSet($key, $value, $method);
    }

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeAuthCommon();
    }

    public function __toString(): string
    {
        return json_encode($this);
    }

    /**
     * Returns the currently scoped entity,
     * or null if no entity is currently scoped.
     * 
     * @return \Oroboros\core\interfaces\library\entity\EntityInterface|null
     */
    public function getEntity(): ?\Oroboros\core\interfaces\library\entity\EntityInterface
    {
        return $this->entity;
    }
    
    /**
     * Replaces internal values with the provided ones
     * 
     * @param type $value
     * @return void
     */
    public function setValue($value): void
    {
//        d('fix subcategory value setter', $this, $value);
    }

    /**
     * Scopes the details to the given entity.
     * this will remove all details from the previous entity if they do not match.
     * 
     * @param \Oroboros\core\interfaces\library\entity\EntityInterface $entity
     * @return void
     */
    public function setEntity(\Oroboros\core\interfaces\library\entity\EntityInterface $entity): void
    {
        $this->resetEntity();
        $this->evaluateEntity($entity);
    }

    /**
     * Sets the current entity, and defines any associated container keys associated with it.
     * 
     * @param \Oroboros\core\interfaces\library\entity\EntityInterface $entity
     * @return void
     */
    protected function evaluateEntity(\Oroboros\core\interfaces\library\entity\EntityInterface $entity): void
    {
        $this->entity = $entity;
    }

    /**
     * Resets the currently scoped entity
     * This should clear all details associated
     * with the entity from the container
     * 
     * @return void
     */
    protected function resetEntity(): void
    {
        $this->entity = null;
    }
}
