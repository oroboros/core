<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\entity;

/**
 * Abstract Application Organization Entity
 * Provides baseline abstraction for organizations owned by the application
 *
 * @author Brian Dayhoff
 */
abstract class AbstractApplicationOrganizationEntity extends AbstractOrganizationEntity implements \Oroboros\core\interfaces\library\entity\ApplicationOrganizationEntityInterface
{

    /**
     * Defines the entity scope as application organizations
     */
    const ENTITY_SCOPE = 'application-organization';
    
    /**
     * Initializes the application organization
     * 
     * @param string $command
     * @param array $arguments
     * @param array $flags
     * @throws \Oroboros\core\exception\core\InvalidClassException
     */
    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeApplicationOrganization();
    }
    
    /**
     * Initializes the organization to match the application details
     * that are not provided directly for the organization definition itself.
     * 
     * @return void
     */
    private function initializeApplicationOrganization(): void
    {
        
    }

}
