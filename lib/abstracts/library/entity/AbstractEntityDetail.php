<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\entity;

/**
 * Abstract Entity Detail
 * Provides baseline abstraction for entity detail objects
 *
 * @author Brian Dayhoff
 */
abstract class AbstractEntityDetail extends \Oroboros\core\abstracts\library\AbstractLibrary implements \Oroboros\core\interfaces\library\entity\EntityDetailInterface
{

    use \Oroboros\core\traits\library\entity\EntityCommonTrait;
    use \Oroboros\core\traits\library\resolver\ResolverUtilityTrait;

    /**
     * Defines the class scope as entity
     */
    const CLASS_SCOPE = 'entity';

    /**
     * Sets entity as the default type
     */
    const ENTITY_TYPE = 'entity';

    /**
     * Abstraction may optionally provide an entity scope
     */
    const ENTITY_SCOPE = null;

    /**
     * Abstraction may optionally provide an entity category
     */
    const ENTITY_CATEGORY = null;

    /**
     * Abstraction may optionally provide an entity subcategory
     */
    const ENTITY_SUBCATEGORY = null;

    /**
     * Abstraction may optionally provide an entity context
     */
    const ENTITY_CONTEXT = 'detail';

    /**
     * This is the default keyname used if child classes
     * do not declare an alternate keyname.
     * 
     * For simple key name changes,
     * they may override this class constant.
     * 
     * For more complex keyname resolution,
     * they must override `declareEntityDetailKey`
     */
    const ENTITY_DETAIL_DEFAULT_KEY = 'undefined';

    /**
     * This is the default value used if child classes
     * do not declare an alternate default value.
     * 
     * For simple default value changes,
     * they may override this class constant.
     * 
     * For complex default values,
     * they must override `declareEntityDetailDefaultValue`
     */
    const ENTITY_DETAIL_DEFAULT_VALUE = null;

    /**
     * Represents the baseline default valid value types
     * @var array
     */
    private static $valid_detail_value_types = [
        null,
        "bool",
        "string",
        "int",
        "array",
        Oroboros\core\interfaces\library\container\ContainerInterface::class,
    ];

    /**
     * Represents the scoped entity
     * 
     * @var \Oroboros\core\interfaces\library\entity\EntityInterface
     */
    private $entity = null;

    /**
     * Represents the detail key identifier associated with the entity detail
     * 
     * @var string
     */
    private $key = null;

    /**
     * Represents the detail value associated with the entity detail
     * 
     * @var string
     */
    private $value = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeResolver();
        $this->initializeEntityDetail();
    }
    
    /**
     * Returns a string representation of the entity detail value
     * 
     * @return string
     */
    public function __toString(): string
    {
        return (string) $this->value();
    }

    /**
     * Public setter for the entity
     * 
     * @param \Oroboros\core\interfaces\library\entity\EntityInterface $entity
     * @return void
     */
    public function setEntity(\Oroboros\core\interfaces\library\entity\EntityInterface $entity): void
    {
        $this->evaluateEntity($entity);
    }

    public function getEntity(): ?\Oroboros\core\interfaces\library\entity\EntityInterface
    {
        return $this->entity;
    }

    /**
     * Returns the keyname of the entity detail
     * 
     * @return string
     */
    public function key(): string
    {
        return $this->key;
    }

    /**
     * Returns the value associated with the entity detail
     * 
     * @return mixed
     */
    public function value()
    {
        return $this->value;
    }
    
    /**
     * Sets the entity detail value.
     * Values must pass validaton before being set.
     * 
     * @param type $value
     * @return void
     */
    public function setValue($value): void
    {
        $this->validateValue($value);
        $this->value = $value;
    }

    /**
     * Evaluates an entity, resets it's values, and scopes to it
     * 
     * @param \Oroboros\core\interfaces\library\entity\EntityInterface $entity
     * @return void
     */
    protected function evaluateEntity(\Oroboros\core\interfaces\library\entity\EntityInterface $entity): void
    {
        $this->resetEntity();
        $this->entity = $entity;
    }

    protected function resetEntity(): void
    {
        $this->entity = null;
    }

    /**
     * Validates a passed entity detail value.
     * 
     * @param mixed $value
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException
     *         If the entity detail value does not match the detail
     *         criteria for the key represented by this object.
     */
    protected function validateValue($value): void
    {
        if (!$this->getResolver()->resolve($value, $this->declareEntityDetailValidValueTypes())) {
            $recieved = gettype($value);
            if (is_object($value)) {
                $recieved = get_class($value);
            }
            d(
                'fail.',
                $this->getResolver()->resolve($value, $this->declareEntityDetailValidValueTypes()),
                $value,
                $this->declareEntityDetailValidValueTypes(),
            );
            throw new \Oroboros\core\exception\InvalidArgumentException(
                sprintf('Error encountered in [%1$s]. '
                    . 'Provided value for entity detail key [%1$s] is not valid. '
                    . 'Valid value types for this key are [%3$s]. '
                    . 'Received value of type [%4$s].'
                    , get_class($this), $this->key(), implode(', ', $this->declareEntityDetailValidValueTypes()), $recieved)
            );
        }
    }

    /**
     * Returns the default entity detail keyname.
     * 
     * By default, this simply returns the value of
     * the class constant `ENTITY_DETAIL_DEFAULT_KEY`.
     * 
     * You may override this method to use alternate key lookup logic if
     * the keyname cannot be easily represented by a class constant.
     * 
     * @return string
     */
    protected function declareEntityDetailKey(): string
    {
        return static::ENTITY_DETAIL_DEFAULT_KEY;
    }

    /**
     * Returns the valid value types for the key represented by this object.
     * This must return an array that matches the resolver api evaluation criteria
     * 
     * @return array
     */
    protected function declareEntityDetailValidValueTypes(): array
    {
        return self::$valid_detail_value_types;
    }

    /**
     * Declares the default value to use if none is passed.
     * 
     * This must return a value that can pass the resolver
     * criteria defined for this key.
     * 
     * @return type
     */
    protected function declareEntityDetailDefaultValue()
    {
        return static::ENTITY_DETAIL_DEFAULT_VALUE;
    }
    
    /**
     * This method will be called from initialization,
     * and should resolve the entity detail value from any passed arguments.
     * 
     * The default behavior is to take no action.
     * 
     * @return void
     */
    protected function resolveEntityDetail(): void
    {
        // no-op
    }

    /**
     * Initializes the entity detail object.
     * 
     * @return void
     * @throws \Oroboros\core\exception\core\InvalidClassException
     */
    protected function initializeEntityDetail(): void
    {
        try {
            $this->key = $this->declareEntityDetailKey();
            $this->setValue($this->declareEntityDetailDefaultValue());
        } catch (\Oroboros\core\exception\InvalidArgumentException $e) {
            // Invalid default value. This indicates an invalid class.
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. Default value for '
                        . 'entity detail key [%2$s] did not pass the specified '
                        . 'validation for this key. Class cannot declare an '
                        . 'invalid default value based on it\'s own '
                        . 'acceptance criteria. This class is not useable '
                        . 'in it\'s current state.', get_class($this), $this->key())
            );
        }
        $this->resolveEntityDetail();
    }
}
