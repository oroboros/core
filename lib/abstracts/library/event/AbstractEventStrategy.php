<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\event;

/**
 * Abstract Event Strategy
 * Provides abstraction for event strategies
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
abstract class AbstractEventStrategy extends \Oroboros\core\abstracts\library\action\AbstractActionStrategy implements \Oroboros\core\interfaces\library\event\EventStrategyInterface
{

    const CLASS_SCOPE = 'event-strategy';
    const STRATEGY_TYPE = 'event';
    const CONTAINER_CLASS = 'Oroboros\\core\\library\\event\\EventQueueIndex';
    const STRATEGY_TARGET_INTERFACE = 'Oroboros\\core\\interfaces\\library\\event\\EventInterface';
    const STRATEGY_MANAGER_INTERFACE = 'Oroboros\\core\\interfaces\\library\\event\\EventManagerInterface';
    const ACTION_POOL_CLASS = 'Oroboros\\core\\library\\event\\EventPool';

    /**
     * Represents the default event manager classes known to the event strategy
     * @var type
     */
    private static $action_manager_classes = [
        'default' => 'event\\EventManager',
        'adapter' => 'event\\AdapterEventManager',
        'bootstrap' => 'event\\BootstrapEventManager',
        'component' => 'event\\ComponentEventManager',
        'controller' => 'event\\ControllerEventManager',
        'exception' => 'event\\ExceptionEventManager',
        'extension' => 'event\\ExtensionEventManager',
        'library' => 'event\\LibraryEventManager',
        'model' => 'event\\ModelEventManager',
        'module' => 'event\\ModuleEventManager',
        'service' => 'event\\ServiceEventManager',
        'view' => 'event\\ViewEventManager',
    ];

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
    }

    /**
     * Declares the default action managers for events
     * 
     * @return array
     */
    protected static function declareActionManagers(): array
    {
        return array_replace_recursive(parent::declareActionManagers(), self::$action_manager_classes);
    }

    /**
     * Performs the binding process for an event manager.
     * This will match the spl observer bindings between the two objects
     * and register the pair internally for reference.
     * 
     * @return void
     */
    protected function bindSubject(\Oroboros\core\interfaces\library\action\ActionAwareInterface $subject, \Oroboros\core\interfaces\library\action\ActionManagerInterface $manager): void
    {
        $subject->attach($manager);
        $manager->attach($subject);
    }
}
