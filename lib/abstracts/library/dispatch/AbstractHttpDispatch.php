<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\dispatch;

/**
 * Abstract Http Line Dispatch
 * Provides abstraction for dispatching an http message to a client
 *
 * @author Brian Dayhoff
 */
abstract class AbstractHttpDispatch extends AbstractDispatch implements \Oroboros\core\interfaces\library\dispatch\HttpDispatchInterface
{

    public function dispatch(): void
    {
        $subject = $this->getSubject();
        $this->sendStatusCode($subject);
        $this->sendHeaders($subject);
        $this->sendContent($subject);
    }

    protected function verifySubject($subject): void
    {
        $expected = 'Psr\\Http\\Message\\ResponseInterface';
        if (!is_object($subject) || !(in_array($expected, class_implements($subject)))) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided dispatch subject is invalid. Expected instance of [%2$s].', get_class($this), $expected));
        }
    }

    private function sendStatusCode(\Psr\Http\Message\ResponseInterface $subject)
    {
        http_response_code($subject->getStatusCode());
    }

    private function sendHeaders(\Psr\Http\Message\ResponseInterface $subject)
    {
        foreach ($subject->getHeaders() as $name => $value) {
            header($name, implode(', ', $value));
        }
    }

    private function sendContent(\Psr\Http\Message\ResponseInterface $subject)
    {
        $stream = $subject->getBody();
        echo $stream;
    }
}
