<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\dispatch;

/**
 * Abstract Dispatch
 * Provides baseline abstraction for dispatching a message back to the connecting client.
 *
 * @author Brian Dayhoff
 */
abstract class AbstractDispatch extends \Oroboros\core\abstracts\library\AbstractLibrary implements \Oroboros\core\interfaces\library\dispatch\DispatchInterface
{

    const CLASS_SCOPE = 'dispatch';

    private $subject = null;

    public function setSubject($subject)
    {
        $this->verifySubject($subject);
        $this->subject = $subject;
    }

    abstract public function dispatch(): void;

    abstract protected function verifySubject($subject): void;

    protected function getSubject()
    {
        return $this->subject;
    }
}
