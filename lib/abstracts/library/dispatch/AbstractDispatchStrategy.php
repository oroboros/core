<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\dispatch;

/**
 * Abstract Dispatch Strategy
 * Provides abstraction for dispatch strategies, which encapsulate obtaining
 * the correct dispatcher to output a message to a client or server based on
 * its interface, type, and scope
 *
 * @author Brian Dayhoff
 */
abstract class AbstractDispatchStrategy extends \Oroboros\core\abstracts\pattern\strategy\AbstractStrategy implements \Oroboros\core\interfaces\library\dispatch\DispatchStrategyInterface
{

    const STRATEGY_TARGET_INTERFACE = 'Oroboros\\core\\interfaces\\controller\\ControllerInterface';

    /**
     * Contains the dispatcher classes that serve the message body
     * @var Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private static $dispatch_handlers = null;

    /**
     * Contains the callable conditional evaluators for dispatcher types
     * @var Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private static $dispatch_conditions = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeDispatcherContainers();
        $this->initializeDefaultDispatchers();
    }

    public function evaluate($subject): \Oroboros\core\interfaces\library\dispatch\DispatchInterface
    {
        $type = null;
        $this->verifySubjectInterface($subject);
        $response = $subject->getResponse();
        foreach (self::$dispatch_conditions as $key => $condition) {
            if ($condition($response)) {
                $type = $key;
                break;
            }
        }
        if (is_null($type)) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. '
                        . 'Unable to determine dispatch class for provided controller [%2$s]', get_class($this), get_class($subject)));
        }
        $dispatcher = $this->load('library', $this->getDispatchHandler($type));
        $dispatcher->setSubject($response);
        return $dispatcher;
    }

    /**
     * Defines a new dispatch object and match condition, or replaces an existing one
     * @param string $key
     * @param string $class
     * @param callable $condition
     * @return void
     */
    public static function setDispatchHandler(string $key, string $class, callable $condition): void
    {
        $details = [
            'class' => $class,
            'condition' => $condition
        ];
        $this->validateDispatcher($key, $details);
        self::$dispatch_handlers[$key] = $details['class'];
        self::$dispatch_conditions[$key] = $details['condition'];
    }

    protected function declareDefaultDispatchHandlers()
    {
        return [
            'http' => [
                'class' => 'dispatch\\http\\Dispatch',
                'condition' => function ($subject) {
                    return is_object($subject) && ($subject instanceof \Psr\Http\Message\ResponseInterface);
                }
            ],
            'cli' => [
                'class' => 'dispatch\\cli\\Dispatch',
                'condition' => function ($subject) {
                    return false; // Not yet implemented
//                    return is_object($subject) && ($subject instanceof \Psr\Http\Message\ResponseInterface);
                }
            ],
        ];
    }

    private function getDispatchHandler(string $type): ?string
    {
        if (self::$dispatch_handlers->has($type)) {
            return self::$dispatch_handlers->get($type);
        }
        throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. '
                    . 'No dispatch handler registered for type [%2$s].', get_class($this), $type));
    }

    private function initializeDispatcherContainers(): void
    {
        if (is_null(self::$dispatch_handlers)) {
            self::$dispatch_handlers = \Oroboros\core\library\container\Container::init();
        }
        if (is_null(self::$dispatch_conditions)) {
            self::$dispatch_conditions = \Oroboros\core\library\container\Container::init();
        }
    }

    private function initializeDefaultDispatchers(): void
    {
        foreach ($this->declareDefaultDispatchHandlers() as $key => $details) {
            if (!self::$dispatch_handlers->has($key)) {
                $this->validateDispatcher($key, $details);
                self::$dispatch_handlers[$key] = $details['class'];
                self::$dispatch_conditions[$key] = $details['condition'];
            }
        }
    }

    private function validateDispatcher(string $key, array $details)
    {
        $message = 'Error encountered in [%1$s]. Missing expected key [%2$s] for dispatcher [%3$s]';
        if (!array_key_exists('class', $details)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf($message, get_class($this), 'class', $key));
        }
        if (!array_key_exists('condition', $details)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf($message, get_class($this), 'condition', $key));
        }
        $message = 'Error encountered in [%1$s]. Provided dispatcher class [%2$s] for key [%3$s] is not valid. '
            . 'It must be a string name of a library implementing [%4$s] (relative stub class names are permitted).';
        $expected = 'Oroboros\\core\\interfaces\\library\\dispatch\\DispatchInterface';
        $fullclass = $this->getFullClassName('library', $details['class']);
        if (!is_string($details['class']) || !$fullclass || !class_exists($fullclass) || !in_array($expected, class_implements($fullclass))) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf($message, get_class($this), $details['class'], $key, $expected));
        }
        if (!is_callable($details['condition'])) {
            $message = 'Error encountered in [%1$s]. Provided dispatcher condition is not callable for key [%3$s] is not valid.';
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf($message, get_class($this), $key));
        }
    }
}
