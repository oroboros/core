<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\job;

/**
 * Abstract Job Manager
 * Provides abstraction for managing job worker instances
 *
 * @author Brian Dayhoff
 */
abstract class AbstractJobManager extends \Oroboros\core\abstracts\pattern\director\AbstractDirector implements \Oroboros\core\interfaces\library\job\JobManagerInterface
{

    use \Oroboros\core\traits\LoaderTrait;
    use \Oroboros\core\traits\pattern\ObserverTrait;
    use \Oroboros\core\traits\pattern\ObservableTrait;

    const CLASS_TYPE = 'library';
    const CLASS_SCOPE = 'job-manager';
    const JOB_TYPE = null;

    /**
     * The adapter used to connect to the queue, if any
     */
    const ADAPTER = null;

    /**
     * Job managers use the job queue container
     */
    const CONTAINER_CLASS = 'Oroboros\\core\\library\\job\\JobQueue';

    /**
     * Job managers use the job queue container
     */
    const COLLECTION_CLASS = 'Oroboros\\core\\library\\job\\JobQueueIndex';

    /**
     * Child classes must provide a worker class.
     */
    const WORKER_CLASS = null;

    /**
     * Default delay before the job is made available.
     * Default is no delay
     * @type int
     */
    const DEFAULT_DELAY = 0; // no delay

    /**
     * Default job priority.
     * @type int
     */
    const DEFAULT_PRIORITY = 1024; // most urgent: 0, least urgent: 4294967295

    /**
     * Default acceptable resolution time for the job.
     * In some job queues, this will allow a job that is
     * unresolved to be put back into the work queue.
     * Default is one minute
     * @type int
     */
    const DEFAULT_TTR = 60; // 1 minute

    /**
     * The worker strategy object
     * @var \Oroboros\core\interfaces\library\job\JobWorkerStrategyInterface
     */
    private static $worker_strategy = null;

    /**
     * The adapter used to connect to the job queue
     * @var \Oroboros\core\interfaces\library\job\JobQueueAdapterInterface
     */
    private $adapter = null;

    /**
     * The queue currently being worked
     * @var \Oroboros\core\interfaces\library\job\JobQueueInterface
     */
    private $queue = null;

    /**
     * The message transmitted from the worker, designating its operational status
     * @var string
     */
    private $message = null;

    /**
     * The id of a newly queued job.
     * This will only hold a non-null integer value when a job is
     * successfully queued and the observer event for a queued job
     * is currently firing.
     * @var int
     */
    private $queued_id = null;

    /**
     * The data payload for a newly queued job.
     * This will only hold a non-null string value when a job is
     * successfully queued and the observer event for a queued job
     * is currently firing.
     * @var string
     */
    private $queued_data = null;

    /**
     * The job queue for a newly queued job.
     * This will only hold a non-null string value when a job is
     * successfully queued and the observer event for a queued job
     * is currently firing.
     * @var string
     */
    private $queued_queue = null;

    /**
     * Any error thrown by a job in progress.
     * This will only hold a non-null exception object when a job is
     * successfully queued and the observer event for a queued job
     * is currently firing.
     * @var \Exception
     */
    private $queued_error = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        $this->verifyJobType();
        $this->verifyWorkerType();
        \Oroboros\core\library\job\JobWorkerStrategy::registerWorkerType(static::JOB_TYPE, static::WORKER_CLASS);
        parent::__construct($command, $arguments, $flags);
        $this->initializeObserver();
        $this->initializeObservable();
        $this->loadWorkerStrategy();
        $this->loadAdapter();
        $this->loadQueue();
        $this->initializeManager();
    }

    /**
     * Queues a new job in the specified work queue.
     * @param string $queue
     * @param array $details
     * @return $this (method chainable)
     * @throws \InvalidArgumentException If the given queue is not valid
     */
    public function queueJob(string $queue, array $details): \Oroboros\core\interfaces\library\job\JobManagerInterface
    {
        if (!$this->hasQueue($queue)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided queue [%2$s] does not exist.', get_class($this), $queue));
        }
        try {
            $this->verifyJobDetails($details);
            $worker = $this->getStrategy()->getWorker(static::JOB_TYPE, $queue);
            $worker->attach($this);
            $worker->setAdapter($this->getAdapter());
            $worker->queueJob($queue, $details);
        } catch (\InvalidArgumentException $e) {
            throw $e;
        } catch (\Exception $e) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. Unable to queue new job in queue [%2$s]. Reason: [%3$s].', get_class($this), $queue, $e->getMessage()), $e->getCode());
        }
        return $this;
    }

    /**
     * Lists the queues that exist for the current job queue type
     * @return array
     */
    public function listQueues(): array
    {
        return array_keys($this->queue->toArray());
    }

    /**
     * Lists the jobs registered within a queue, if it is possible to do so
     * @param string $queue
     * @return array
     * @throws \InvalidArgumentException If the given queue is not valid
     */
    public function listJobs(string $queue): array
    {
        if (!$this->hasQueue($queue)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided queue [%2$s] does not exist.', get_class($this), $queue));
        }
        return $this->getQueue($queue)->toArray();
    }

    /**
     * Returns a JobQueue object corresponding to the specified queue identifier
     * @param string $queue
     * @return \Oroboros\core\interfaces\library\job\JobQueueInterface
     * @throws \InvalidArgumentException If the given queue is not valid
     */
    public function getQueue(string $queue): \Oroboros\core\interfaces\library\job\JobQueueInterface
    {
        if (!$this->hasQueue($queue)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided queue [%2$s] does not exist.', get_class($this), $queue));
        }
        return $this->queue->get($queue);
    }

    /**
     * Returns a boolean determination as to whether a given queue is valid
     * @param string $queue
     * @return bool
     */
    public function hasQueue(string $queue): bool
    {
        return $this->queue->has($queue);
    }

    /**
     * Spawns a worker to begin watching a queue and processing jobs.
     * @param string $queue
     * @return void
     * @throws \InvalidArgumentException If the given queue is not valid
     */
    public function watch(string $queue): bool
    {
        $queue_object = $this->getQueue($queue);
        $this->setJobQueue($queue);
        $worker = $this->getStrategy()->getWorker(static::JOB_TYPE, $queue);
        $worker->attach($this);
        $worker->setAdapter($this->getAdapter());
        // pick a job and process it
        $job = $this->getAdapter()->getJob($queue);
        if (is_null($job)) {
            // No jobs found.
            return false;
        }
        while (!is_null($job)) {
            $worker->executeTask($job);
            $job = $this->getAdapter()->getJob($queue);
////            $received = json_decode($job->getData(), true);
////            $action = $received['action'];
////            if (isset($received['data'])) {
////                $data = $received['data'];
////            } else {
////                $data = array();
////            }
////
////            echo "Received a $action (" . current($data) . ") ...";
////            if (method_exists($worker, $action)) {
////                $outcome = $worker->$action($data);
////
////                // how did it go?
////                if ($outcome) {
////                    echo "done \n";
////                    $queue->delete($job);
////                } else {
////                    echo "failed \n";
////                    $queue->bury($job);
////                }
////            } else {
////                echo "action not found\n";
////                $queue->bury($job);
////            }
        }
        return true;
    }

    /**
     * Verifies that a worker can be called for the given queue.
     * This will throw an exception if the queue/worker is not valid,
     * but will not call any heavy operations during validation.
     * @param string $queue
     * @return void
     * @throws \InvalidArgumentException If the given queue or worker is not valid
     */
    public function verify(string $queue): void
    {
        $this->getStrategy()->verifyWorker(static::JOB_TYPE, $queue);
    }

    /**
     * Returns the string name of the registered job queue when a new job is registered.
     * This only returns a non-null string result when the observer event `queue`
     * is currently firing.
     * @return string|null
     */
    public function getJobQueue(): ?string
    {
        return $this->queued_queue;
    }

    /**
     * Observer method for retrieving the job id of a newly registered job.
     * Only returns an integer during the observer event `queue`
     * @return int|null
     */
    public function getJobId(): ?int
    {
        return $this->queued_id;
    }

    /**
     * Observer method for retrieving the job data of a newly registered job.
     * Only returns a string during the observer event `queue`.
     * @return string|null
     */
    public function getJobData(): ?string
    {
        return $this->queued_data;
    }

    /**
     * Observer method for retrieving the job error of an ongoing job.
     * Only returns a exception during the observer event `error` or `failure`.
     * @return \Exception|null
     */
    public function getJobError(): ?\Exception
    {
        return $this->queued_error;
    }

    /**
     * Observer event binding for successful job queueing
     * @param \Oroboros\core\interfaces\library\job\JobWorkerInterface $worker
     * @return void
     */
    public function handleQueuedJob(\Oroboros\core\interfaces\library\job\JobWorkerInterface $worker): void
    {
        $this->evaluateQueuedJob($worker);
    }

    /**
     * Observer event binding for started jobs
     * @param \Oroboros\core\interfaces\library\job\JobWorkerInterface $worker
     * @return void
     */
    public function handleStart(\Oroboros\core\interfaces\library\job\JobWorkerInterface $worker): void
    {
        $this->evaluateStart($worker);
    }

    /**
     * Observer event binding for paused jobs
     * @param \Oroboros\core\interfaces\library\job\JobWorkerInterface $worker
     * @return void
     */
    public function handlePause(\Oroboros\core\interfaces\library\job\JobWorkerInterface $worker): void
    {
        $this->evaluatePause($worker);
    }

    /**
     * Observer event binding for resumed jobs
     * @param \Oroboros\core\interfaces\library\job\JobWorkerInterface $worker
     * @return void
     */
    public function handleResume(\Oroboros\core\interfaces\library\job\JobWorkerInterface $worker): void
    {
        $this->evaluateResume($worker);
    }

    /**
     * Observer event binding for killed jobs
     * @param \Oroboros\core\interfaces\library\job\JobWorkerInterface $worker
     * @return void
     */
    public function handleKill(\Oroboros\core\interfaces\library\job\JobWorkerInterface $worker): void
    {
        $this->evaluateKill($worker);
    }

    /**
     * Observer event binding for force-killed jobs
     * @param \Oroboros\core\interfaces\library\job\JobWorkerInterface $worker
     * @return void
     */
    public function handleForceKill(\Oroboros\core\interfaces\library\job\JobWorkerInterface $worker): void
    {
        $this->evaluateForceKill($worker);
    }

    /**
     * Observer event binding for completed jobs
     * @param \Oroboros\core\interfaces\library\job\JobWorkerInterface $worker
     * @return void
     */
    public function handleComplete(\Oroboros\core\interfaces\library\job\JobWorkerInterface $worker): void
    {
        $this->evaluateComplete($worker);
    }

    /**
     * Observer event binding for messages emitted by jobs
     * @param \Oroboros\core\interfaces\library\job\JobWorkerInterface $worker
     * @return void
     */
    public function handleMessage(\Oroboros\core\interfaces\library\job\JobWorkerInterface $worker): void
    {
        $this->evaluateMessage($worker);
    }

    /**
     * Observer event binding for job errors
     * @param \Oroboros\core\interfaces\library\job\JobWorkerInterface $worker
     * @return void
     */
    public function handleError(\Oroboros\core\interfaces\library\job\JobWorkerInterface $worker): void
    {
        $this->evaluateError($worker);
    }

    /**
     * Observer event binding for failed jobs
     * @param \Oroboros\core\interfaces\library\job\JobWorkerInterface $worker
     * @return void
     */
    public function handleFailure(\Oroboros\core\interfaces\library\job\JobWorkerInterface $worker): void
    {
        $this->evaluateFailure($worker);
    }

    /**
     * Registers successful job registration, notifying the message queue that
     * a new job has been entered into the job queue.
     * @param \Oroboros\core\interfaces\library\job\JobWorkerInterface $worker
     * @return void
     */
    protected function evaluateQueuedJob(\Oroboros\core\interfaces\library\job\JobWorkerInterface $worker): void
    {
        $this->setJobQueue($worker->getJobQueue());
        $this->setJobId($worker->getJobId());
        $this->setJobData($worker->getJobData());
        $this->setObserverEvent('queue');
        $this->resetJobQueue();
        $this->resetJobId();
        $this->resetJobData();
    }

    /**
     * Registers job start, notifying the message queue that a specific job
     * is running and should not be allotted to other workers.
     * @param \Oroboros\core\interfaces\library\job\JobWorkerInterface $worker
     * @return void
     */
    protected function evaluateStart(\Oroboros\core\interfaces\library\job\JobWorkerInterface $worker): void
    {
        $this->setJobId($worker->getJobId());
        $this->setJobData($worker->getJobData());
        $this->setObserverEvent('start');
        $this->resetJobId();
        $this->resetJobData();
    }

    /**
     * Registers job pause and pushes an update to the message queue indicating
     * that the job is reserved, but has been temporarily halted.
     * @param \Oroboros\core\interfaces\library\job\JobWorkerInterface $worker
     * @return void
     */
    protected function evaluatePause(\Oroboros\core\interfaces\library\job\JobWorkerInterface $worker): void
    {
        $this->setJobId($worker->getJobId());
        $this->setJobData($worker->getJobData());
        $this->setObserverEvent('pause');
        $this->resetJobId();
        $this->resetJobData();
    }

    /**
     * Registers job completion and pushes an update to the message queue indicating
     * that the job has been resumed.
     * @param \Oroboros\core\interfaces\library\job\JobWorkerInterface $worker
     * @return void
     */
    protected function evaluateResume(\Oroboros\core\interfaces\library\job\JobWorkerInterface $worker): void
    {
        $this->setJobId($worker->getJobId());
        $this->setJobData($worker->getJobData());
        $this->setObserverEvent('resume');
        $this->resetJobId();
        $this->resetJobData();
    }

    /**
     * Registers job completion and pushes an update to the message queue indicating
     * that the job has been ordered to halt gracefully.
     * Cleanup operations should occur when this event fires.
     * @param \Oroboros\core\interfaces\library\job\JobWorkerInterface $worker
     * @return void
     */
    protected function evaluateKill(\Oroboros\core\interfaces\library\job\JobWorkerInterface $worker): void
    {
        $this->setJobId($worker->getJobId());
        $this->setJobData($worker->getJobData());
        $this->getAdapter()->failJob();
        $this->setObserverEvent('kill');
        $this->resetJobId();
        $this->resetJobData();
    }

    /**
     * Registers job completion and pushes an update to the message queue indicating
     * that the job has been ordered to halt immediately.
     * Cleanup operations should not occur when this event fires.
     * @param \Oroboros\core\interfaces\library\job\JobWorkerInterface $worker
     * @return void
     */
    protected function evaluateForceKill(\Oroboros\core\interfaces\library\job\JobWorkerInterface $worker): void
    {
        $this->setJobId($worker->getJobId());
        $this->setJobData($worker->getJobData());
        $this->getAdapter()->failJob();
        $this->setObserverEvent('force-kill');
        $this->resetJobId();
        $this->resetJobData();
    }

    /**
     * Registers job completion and pushes an update to the message queue.
     * Will spawn a new worker for another task if any tasks remain in the queue,
     * otherwise removes itself from the worker pool and exits.
     * @param \Oroboros\core\interfaces\library\job\JobWorkerInterface $worker
     * @return void
     */
    protected function evaluateComplete(\Oroboros\core\interfaces\library\job\JobWorkerInterface $worker): void
    {
        $this->setJobId($worker->getJobId());
        $this->setJobData($worker->getJobData());
        $this->getAdapter()->deleteJob();
        $this->setObserverEvent('complete');
        $this->resetJobId();
        $this->resetJobData();
    }

    /**
     * Pushes a message from the worker to the message queue
     * @param \Oroboros\core\interfaces\library\job\JobWorkerInterface $worker
     * @return void
     */
    protected function evaluateMessage(\Oroboros\core\interfaces\library\job\JobWorkerInterface $worker): void
    {
        $this->setJobId($worker->getJobId());
        $this->setJobData($worker->getJobData());
        $this->setObserverEvent('message');
        $this->resetJobId();
        $this->resetJobData();
    }

    /**
     * Logs errors associated with the job worker
     * @param \Oroboros\core\interfaces\library\job\JobWorkerInterface $worker
     * @return void
     */
    protected function evaluateError(\Oroboros\core\interfaces\library\job\JobWorkerInterface $worker): void
    {
        $this->setJobId($worker->getJobId());
        $this->setJobData($worker->getJobData());
        $this->setJobError($worker->getJobError());
        $this->setObserverEvent('error');
        $this->resetJobId();
        $this->resetJobData();
        $this->resetJobError();
    }

    /**
     * Updates the message queue that a job failed
     * @param \Oroboros\core\interfaces\library\job\JobWorkerInterface $worker
     * @return void
     */
    protected function evaluateFailure(\Oroboros\core\interfaces\library\job\JobWorkerInterface $worker): void
    {
        $this->setJobId($worker->getJobId());
        $this->setJobData($worker->getJobData());
        $this->getAdapter()->failJob();
        $this->setObserverEvent('fail');
        $this->resetJobId();
        $this->resetJobData();
    }

    /**
     * Verifies that the job details match the specification for a valid job of this type
     * @param array $details
     * @return void
     * @throws \InvalidArgumentException If the provided details are not acceptable
     */
    abstract function verifyJobDetails(array $details): void;

    protected function setJobQueue(string $queue): void
    {
        $this->queued_queue = $queue;
    }

    protected function setJobId(int $id): void
    {
        $this->queued_id = $id;
    }

    protected function setJobData(string $data): void
    {
        $this->queued_data = $data;
    }

    protected function setJobError(\Exception $exception): void
    {
        $this->queued_error = $exception;
    }

    protected function resetJobQueue(): void
    {
        $this->queued_queue = null;
    }

    protected function resetJobId(): void
    {
        $this->queued_id = null;
    }

    protected function resetJobData(): void
    {
        $this->queued_data = null;
    }

    protected function resetJobError(): void
    {
        $this->queued_error = null;
    }

    /**
     * Override this method to provide initialization logic for the specific manager.
     * Any additional setup operations that should occur on instantiation should
     * be called from within this method.
     * @return void
     */
    abstract protected function initializeManager(): void;

    /**
     * Registers the observer event bindings for monitoring workers
     * @return array
     */
    protected function declareObserverEventBindings(): array
    {
        return [
            'start' => [$this, 'handleStart'],
            'pause' => [$this, 'handlePause'],
            'resume' => [$this, 'handleResume'],
            'kill' => [$this, 'handleKill'],
            'force-kill' => [$this, 'handleForceKill'],
            'message' => [$this, 'handleMessage'],
            'complete' => [$this, 'handleComplete'],
            'error' => [$this, 'handleError'],
            'failure' => [$this, 'handleFailure'],
            'queue' => [$this, 'handleQueuedJob'],
        ];
    }

    protected function declareObservableName(): string
    {
        return trim(sprintf('%1$s:%2$s', static::CLASS_TYPE, static::CLASS_SCOPE), ':');
    }

    /**
     * Returns the adapter listed in the child class implementation, if one was listed.
     * If the class constant `ADAPTER` is not null, this should be expected to return a valid object.
     * If the class constant `ADAPTER` is null, this should be expected to return null.
     * @return \Oroboros\core\interfaces\library\job\JobQueueAdapterInterface|null
     */
    protected function getAdapter(): ?\Oroboros\core\interfaces\library\job\JobQueueAdapterInterface
    {
        return $this->adapter;
    }

    /**
     * Registers a new queue for the current job type
     * @param string $queue
     * @param array $jobs
     * @return void
     */
    protected function setQueue(string $queue, array $jobs = []): void
    {
        $classname = $this->getFullClassName('library', static::CONTAINER_CLASS);
        $queue_index = $classname::init(null, $jobs);
        $this->queue[$queue] = $queue_index;
    }

    /**
     * Returns the worker strategy object, for spawning worker instances
     * @return \Oroboros\core\interfaces\library\job\JobWorkerStrategyInterface
     */
    protected function getStrategy(): \Oroboros\core\interfaces\library\job\JobWorkerStrategyInterface
    {
        return self::$worker_strategy;
    }

    /**
     * Initializes the worker strategy object
     * @return void
     */
    private function loadWorkerStrategy(): void
    {
        if (is_null(self::$worker_strategy)) {
            self::$worker_strategy = $this->load('library', 'job\\JobWorkerStrategy', static::CLASS_SCOPE);
        }
    }

    private function loadAdapter(): void
    {
        if (!is_null(static::ADAPTER)) {
            $this->adapter = $this->load('library', static::ADAPTER, static::CLASS_SCOPE);
        }
    }

    private function loadQueue(): void
    {
        if (is_null($this->queue)) {
            $classname = $this->getFullClassName('library', static::COLLECTION_CLASS);
            $this->queue = $classname::init(null, []);
        }
    }

    private function verifyJobType(): void
    {
        if (is_null(static::JOB_TYPE)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(sprintf('Class [%2$s] is misconfigured. '
                        . 'Expected class constant [%2$s] must define a corresponding job queue type.', get_class($this), 'JOB_TYPE'));
        }
    }

    private function verifyWorkerType(): void
    {
        if (is_null(static::WORKER_CLASS)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(sprintf('Class [%2$s] is misconfigured. '
                        . 'Expected class constant [%2$s] must define a corresponding worker class to handle job queue type [%3$s].', get_class($this), 'WORKER_CLASS', static::JOB_TYPE));
        }
    }
}
