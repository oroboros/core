<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\job;

/**
 * AbstractJobManagerStrategy
 *
 * @author Brian Dayhoff
 */
abstract class AbstractJobManagerStrategy extends \Oroboros\core\abstracts\pattern\strategy\AbstractStrategy implements \Oroboros\core\interfaces\library\job\JobManagerStrategyInterface
{

    const CLASS_TYPE = 'library';
    const CLASS_SCOPE = 'job-manager-strategy';
    const STRATEGY_TYPE = 'job';
    const STRATEGY_SCOPE = 'manager';
    const CONTAINER_CLASS = 'Oroboros\\core\\library\\job\\JobQueueIndex';
    const STRATEGY_TARGET_INTERFACE = 'Oroboros\\core\\interfaces\\library\\job\\JobManagerInterface';

    private static $manager_targets = null;
    private static $default_managers_initialized = false;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeManagerTargets();
    }

    /**
     * Registers a new job queue manager
     * @param string $key
     * @param string $class
     * @return void
     * @throws \InvalidArgumentException If the given class does not exist,
     *     is not a job queue manager class, or provides an identification key
     *     that is already registered.
     */
    public static function registerManager(string $key, string $class): void
    {
        self::initializeManagerTargets();
        $proxy = new \Oroboros\core\pattern\factory\LoaderProxy();
        if (!$proxy->getFullClassName('library', $class)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Provided class [%2$s] for key [%3$s] is not a valid job queue manager class.', get_called_class(), $class, $key));
        }
        if (property_exists(self::$manager_targets, $key) && $class !== self::$manager_targets[$key]) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Provided queue manager key [%2$s] is already registered.', get_called_class(), $key));
        }
        $expected = 'Oroboros\\core\\interfaces\\library\\job\\JobManagerInterface';
        if (!in_array($expected, class_implements($proxy->getFullClassName('library', $class)))) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided job queue class [%2$s] for provided key [%3$s] is not '
                        . 'a valid job queue manager object. '
                        . 'Expected classname implementing [%4$s].', get_called_class(), $class, $key, $expected));
        }
        self::$manager_targets[$key] = $class;
    }

    public function getManager(string $type, string $queue = null, array $arguments = null, array $flags = null): \Oroboros\core\interfaces\library\job\JobManagerInterface
    {
        if (!self::$manager_targets->has($type)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. No known queue manager exists for key [%2$s].', get_class($this), $type));
        }
        $manager = $this->load('library', self::$manager_targets[$type], $queue, $arguments, $flags);
        return $manager;
    }

    private static function initializeManagerTargets()
    {
        if (is_null(self::$manager_targets)) {
            self::$manager_targets = self::containerize();
            self::initializeDefaultManagers();
        }
    }

    private static function initializeDefaultManagers(): void
    {
        if (!self::$default_managers_initialized) {
            $proxy = new \Oroboros\core\pattern\factory\LoaderProxy();
            $managers = $proxy->load('library', 'parser\\JsonParser', static::CONFIG_ROOT . 'jobs.json')
                ->fetch();
            foreach ($managers as $key => $class) {
                if (!property_exists(self::$manager_targets, $key) && $class !== false) {
                    static::registerManager($key, $class);
                }
            }
            self::$default_managers_initialized = true;
        }
    }
}
