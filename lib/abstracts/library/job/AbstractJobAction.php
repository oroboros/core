<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\job;

/**
 * Abstract Job Action
 * Provides abstraction for a class that resolves a specific job task.
 * These are separated from the worker objects, so that they can be reused
 * across multiple job queue types.
 * 
 * Workers are expected to load the correct action to resolve the job,
 * and report to their job manager with the outcome.
 * 
 * Job actions are called by workers, and simply succeed or fail.
 * 
 * Workers may break actions before they are completed, with either
 * a call to close (exit gracefully), or kill (exit immediately).
 * 
 * Actions are expected to clean up their operation prior to exiting
 * when a call to close is made prior to completion.
 * 
 * Actions are expected to terminate immediately without cleanup
 * when a call to kill is made prior to completion.
 *
 * @author Brian Dayhoff
 */
abstract class AbstractJobAction extends \Oroboros\core\abstracts\pattern\director\AbstractWorker implements \Oroboros\core\interfaces\library\job\JobActionInterface
{

    use \Oroboros\core\traits\LoaderTrait;

    const CLASS_TYPE = 'worker';
    const CLASS_SCOPE = 'job-action';
    const WORKER_SCOPE = 'job-action';

    /**
     * Gracefully stops an ongoing job action.
     * Actions may run cleanup operations prior
     * to terminating when this method is called.
     * @return bool Returns true if executed successfully, false if not.
     *     False does not indicate an error, only that the job should be re-queued.
     * @throws \ErrorException If an error occurs while the action is processing.
     * @throws \InvalidArgumentException If the provided data is not valid.
     */
    public function run(array $data): bool
    {
        try {
            $this->executeTask($data);
        } catch (\InvalidArgumentException $e) {
            throw $e;
        } catch (\ErrorException $e) {
            throw $e;
        } catch (\Exception $e) {
            throw new \Oroboros\core\exception\ErrorException($e->getMessage(), $e->getCode());
        }
        return true;
    }

    /**
     * Gracefully stops an ongoing job action.
     * Actions may run cleanup operations prior
     * to terminating when this method is called.
     * @return $this (method chainable)
     */
    public function close(): \Oroboros\core\interfaces\library\job\JobActionInterface
    {
        // @todo
        return $this;
    }

    /**
     * Immediately stops an ongoing job action.
     * Actions must immediately terminate without
     * running cleanup when this method is called.
     * @return $this (method chainable)
     */
    public function kill(): \Oroboros\core\interfaces\library\job\JobActionInterface
    {
        // @todo
        return $this;
    }
}
