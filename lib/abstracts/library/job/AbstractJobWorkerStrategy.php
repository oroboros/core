<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\job;

/**
 * Abstract Job Worker Strategy
 *
 * @author Brian Dayhoff
 */
abstract class AbstractJobWorkerStrategy extends \Oroboros\core\abstracts\pattern\strategy\AbstractStrategy implements \Oroboros\core\interfaces\library\job\JobWorkerStrategyInterface
{

    const CLASS_TYPE = 'library';
    const CLASS_SCOPE = 'job-worker-strategy';
    const STRATEGY_TYPE = 'job';
    const STRATEGY_SCOPE = 'worker';
    const CONTAINER_CLASS = 'Oroboros\\core\\library\\job\\JobQueueIndex';
    const STRATEGY_TARGET_INTERFACE = 'Oroboros\\core\\interfaces\\library\\job\\JobWorkerInterface';

    /**
     * The worker pool, which maps worker classes to the corresponding job type.
     * Job types are defined by the job manager object.
     * @var \Oroboros\core\library\job\JobWorkerPool 
     */
    private static $worker_targets = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeWorkerTargets();
    }

    public static function registerWorkerType(string $type, string $worker): void
    {
        self::initializeWorkerTargets();
        $proxy = new \Oroboros\core\pattern\factory\LoaderProxy();
        $classname = $proxy->getFullClassName('library', $worker);
        if (!$classname) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided worker [%2$s] is not a valid class.', get_called_class(), $worker));
        }
        $expected = 'Oroboros\\core\\interfaces\\library\\job\\JobWorkerInterface';
        if (!in_array($expected, class_implements($classname))) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided worker [%2$s] is not a valid job worker. '
                        . 'Expected implementation of [%3$s].', get_called_class(), $worker, $expected));
        }
        self::$worker_targets[$type] = $worker;
    }

    /**
     * Preflight method to insure that a given worker/queue is valid.
     * @param string $type
     * @param string $queue
     * @return void
     * @throws \InvalidArgumentException If the given worker or queue is not valid.
     */
    public function verifyWorker(string $type, string $queue): void
    {
        if (!self::$worker_targets->has($type)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'No worker class exists of type [%2$s].', get_class($this), $type));
        }
    }

    public function getWorker(string $type, string $queue): \Oroboros\core\interfaces\library\job\JobWorkerInterface
    {
        $this->verifyWorker($type, $queue);
        $worker = $this->load('library', self::$worker_targets[$type], $queue);
        return $worker;
    }

    private static function initializeWorkerTargets()
    {
        if (is_null(self::$worker_targets)) {
            $worker_pool = \Oroboros\core\library\job\JobWorkerPool::init();
            self::$worker_targets = $worker_pool;
        }
    }
}
