<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\job;

/**
 * Abstract Job Worker
 * Provides a set of methods for operating on a job queue
 *
 * @author Brian Dayhoff
 */
abstract class AbstractJobWorker extends \Oroboros\core\abstracts\pattern\builder\AbstractBuilder implements \Oroboros\core\interfaces\library\job\JobWorkerInterface
{

    use \Oroboros\core\traits\pattern\ObservableTrait;

    const CLASS_TYPE = 'library';
    const CLASS_SCOPE = 'worker';
    const WORKER_TYPE = 'job';
    const WORKER_SCOPE = null;

    private static $action_handlers = null;

    /**
     * The registered job id when a job is acted upon or registered
     * @var int
     */
    private $registered_id = null;

    /**
     * The registered data when a job is acted upon or registered
     * @var string
     */
    private $registered_data = null;

    /**
     * The registered queue that the job is operating against when
     * a job is acted upon or registered.
     * @var string
     */
    private $registered_queue = null;

    /**
     * The registered error that the job generated when
     * a job is acted upon and encounters problems.
     * @var \Exception
     */
    private $registered_error = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeObservable();
        $this->initializeActions();
    }

    /**
     * Registers a job action that workers will use to handle specific job entries.
     * Job actions are agnostic of specific workers, and can be called by any
     * valid worker to handle the payload for the job.
     * @param string $key
     * @param string $class
     * @throws \InvalidArgumentException If the registered job action class
     *     is not valid, or if it is already registered
     *     (actions cannot be overridden once registered).
     */
    public static function registerAction(string $key, string $class)
    {
        self::initializeActions();
        $proxy = new \Oroboros\core\pattern\factory\LoaderProxy();
        $fullclass = $proxy->getFullClassName($class);
        if (!$fullclass) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Provided class [%2$s] for key [%3$s] is not a valid job action class.', get_called_class(), $class, $key));
        }
        if (property_exists(self::$action_handlers, $key) && $class !== self::$action_handlers[$key]) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Provided job action key [%2$s] is already registered.', get_called_class(), $key));
        }
        $expected = 'Oroboros\\core\\interfaces\\library\\job\\JobActionInterface';
        if (!in_array($expected, class_implements($proxy->getFullClassName('library', $class)))) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided job queue class [%2$s] for provided key [%3$s] is not '
                        . 'a valid job queue manager object. '
                        . 'Expected classname implementing [%4$s].', get_called_class(), $class, $key, $expected));
        }
    }

    /**
     * Executes the given job queue task based on the arguments provided
     * @param array $arguments (optional) Any arguments that define the parameters of task execution
     * @param array $flags (optional) any flags that should modify the behavior of the task execution
     * @return $this (method chainable)
     */
    public function executeTask(array $arguments = [], array $flags = []): \Oroboros\core\interfaces\library\job\JobWorkerInterface
    {
        try {
            $this->validateJobArguments($arguments);
            $id = $arguments['id'];
            $action = $arguments['data'][0];
            $data = $arguments['data'][1];
            $this->setJobId($id);
            $this->setJobData($data);
            $this->validateJobAction($action);
            if (is_string($data)) {
                $data = json_decode($data, 1);
            }
            $this->setObserverEvent('start');
            if ($this->loadAction($action)->run($data)) {
                // Task succeeded
                $this->setObserverEvent('complete');
            } else {
                // Task failed
                $this->setObserverEvent('failure');
            }
        } catch (\InvalidArgumentException $e) {
            // Invalid job declaration
            $this->setJobError($e);
            $this->setObserverEvent('error');
            $this->setObserverEvent('failure');
            $this->resetJobError();
            return $this;
        } catch (\ErrorException $e) {
            // Invalid job declaration
            $this->setJobError($e);
            $this->setObserverEvent('error');
            $this->setObserverEvent('failure');
            $this->resetJobError();
            return $this;
        }
        return $this;
    }

    /**
     * Queues a given job, and fires an observer event `queue`
     * for callbacks to extract job references for external registration.
     * @param string $queue
     * @param array $details
     * @return $this (method chainable)
     */
    public function queueJob(string $queue, array $details): \Oroboros\core\interfaces\library\job\JobWorkerInterface
    {
        $job = $this->getAdapter()->queueJob($queue, $details);
        $this->setJobQueue($queue);
        $this->setJobId($job['id']);
        $this->setJobData($job['data']);
        $this->setObserverEvent('queue');
        $this->resetJobQueue();
        $this->resetJobId();
        $this->resetJobData();
        return $this;
    }

    /**
     * Sets the adapter object for interfacing with the job queue
     * @param \Oroboros\core\interfaces\library\job\JobQueueAdapterInterface $adapter
     * @return $this (method chainable)
     */
    public function setAdapter(\Oroboros\core\interfaces\library\job\JobQueueAdapterInterface $adapter): \Oroboros\core\interfaces\library\job\JobWorkerInterface
    {
        $this->adapter = $adapter;
        return $this;
    }

    public function getJobId(): ?int
    {
        return $this->registered_id;
    }

    public function getJobData(): ?string
    {
        return $this->registered_data;
    }

    public function getJobQueue(): ?string
    {
        return $this->registered_queue;
    }

    public function getJobError(): ?\Exception
    {
        return $this->registered_error;
    }

    protected function setJobQueue(string $queue): void
    {
        $this->registered_queue = $queue;
    }

    protected function setJobId(int $id): void
    {
        $this->registered_id = $id;
    }

    protected function setJobData(string $data): void
    {
        $this->registered_data = $data;
    }

    protected function setJobError(\Exception $error): void
    {
        $this->registered_error = $error;
    }

    protected function resetJobQueue(): void
    {
        $this->registered_queue = null;
    }

    protected function resetJobId(): void
    {
        $this->registered_id = null;
    }

    protected function resetJobData(): void
    {
        $this->registered_data = null;
    }

    protected function resetJobError(): void
    {
        $this->registered_error = null;
    }

    protected function getAdapter(): \Oroboros\core\interfaces\library\job\JobQueueAdapterInterface
    {
        return $this->adapter;
    }

    protected function declareObservableName(): string
    {
        return trim(sprintf('%1$s:%2$s', static::CLASS_TYPE, static::CLASS_SCOPE), ':');
    }

    protected function loadAction(string $key): \Oroboros\core\interfaces\library\job\JobActionInterface
    {
        $this->validateJobAction($key);
        return $this->load('library', self::$action_handlers[$key]);
    }

    protected function declareCategories(): array
    {
        $categories = $this->getModel('worker')->fetch();
        return $categories->toArray();
    }

    /**
     * Declares any default callable events the child class designates
     * should be automatically available.
     * 
     * Override this method if you wish to declare default events
     * 
     * @return array
     */
    protected static function declareDefaultActions(): array
    {
        $defaults = $this->getModel('job-action')->fetch();
        return $defaults->toArray();
    }

    private static function initializeActions()
    {
        if (is_null(self::$action_handlers)) {
            self::$action_handlers = self::containerize();
            self::loadDefaultActions();
        }
    }

    private function validateJobArguments(array $arguments): void
    {
        if (!array_key_exists('id', $arguments)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Provided job payload does not contain an id.', get_class($this)));
        }
        if (!array_key_exists('data', $arguments)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Provided job payload does not contain data.', get_class($this)));
        }
    }

    private function validateJobAction(string $action): void
    {
        if (!self::$action_handlers->has($action)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Provided job action [%2$s] does not exist.', get_class($this), $action));
        }
    }

    private static function loadDefaultActions(): void
    {
        foreach (static::declareDefaultActions() as $key => $class) {
            self::$action_handlers[$key] = $class;
        }
    }
}
