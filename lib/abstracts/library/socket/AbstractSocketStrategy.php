<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\socket;

/**
 * Abstract Socket Strategy
 * Provides abstraction for obtaining socket applications
 *
 * @author Brian Dayhoff
 */
abstract class AbstractSocketStrategy extends \Oroboros\core\abstracts\pattern\strategy\AbstractStrategy implements \Oroboros\core\interfaces\library\socket\SocketStrategyInterface
{

    use \Oroboros\core\traits\LoaderTrait;
    use \Oroboros\core\traits\model\ModelAwareUtilityTrait;

    const CLASS_TYPE = 'library';
    const STRATEGY_TARGET_INTERFACE = 'Oroboros\\core\\interfaces\\library\\socket\\SocketApplicationInterface';

    /**
     * The list of all available socket applications
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private static $socket_applications = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeSocketApplicationList();
    }

    /**
     * Manually registers a new socket application
     * @param string $key The identifier slug of the socket application
     * @param string $class The full or stub classname of the socket application
     * @param string $host The hostname of the socket application.
     *     If not provided, the default host will be used.
     * @param int $port The port of the socket application.
     *     If not provided, the default will be used.
     * @return void
     * @throws \InvalidArgumentException If the given class does not exist,
     *     is not a socket application class, or provides an identification key
     *     that is already registered.
     */
    public static function registerSocketApplication(string $key, string $class, string $host = null, int $port = null): void
    {
        self::initializeSocketApplicationList();
        if (is_null($host)) {
            $host = static::DEFAULT_SOCKET_HOST;
        }
        if (is_null($port)) {
            $host = static::DEFAULT_SOCKET_PORT;
        }
        $proxy = new \Oroboros\core\pattern\factory\LoaderProxy();
        if (!$proxy->getFullClassName('library', $class)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Provided class [%2$s] for key [%3$s] is not a socket application class.', get_called_class(), $class, $key));
        }
        if (property_exists(self::$socket_applications, $key) && $class !== self::$socket_applications[$key]['class']) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Provided socket application key [%2$s] is already registered.', get_class($this), $key));
        }
        $expected = 'Oroboros\\core\\interfaces\\library\\socket\\SocketApplicationInterface';
        if (!in_array($expected, class_implements($proxy->getFullClassName('library', $class)))) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided socket application class [%2$s] for provided key [%3$s] is not '
                        . 'a valid socket application. '
                        . 'Expected classname implementing [%4$s].', get_called_class(), $class, $key, $expected));
        }
        self::$socket_applications[$key] = [
            'application' => $class,
            'host' => $host,
            'port' => $port
        ];
    }

    /**
     * Takes a socket application, and returns an instantiated 
     * corresponding websocket server of the given type.
     * @param string $type The identifier keyword for the socket application type
     * @param array|null $arguments (optional) Any arguments to pass into the application instance
     * @param array|null $flags (optional) Any flags to pass into the application instance
     * @return \Oroboros\core\interfaces\library\socket\SocketApplicationInterface
     * @throws \InvalidArgumentException If the specified socket application type is not known
     * @throws \ErrorException If the socket application already has a loaded instance
     */
//    public function evaluate(string $type, array $arguments = null, array $flags = null): \Oroboros\core\interfaces\library\socket\SocketApplicationInterface
    public function evaluate(string $type, array $arguments = null, array $flags = null): \Ratchet\Server\IoServer
    {
        if (!self::$socket_applications->has($type)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'No known socket application for type [%2$s]', get_class($this), $type));
        }
        try {
            return $this->loadSocketApplication($type, $arguments, $flags);
        } catch (\InvalidArgumentException $e) {
            throw $e;
        } catch (\Exception $e) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. '
                        . 'Socket application type [%2$s] failed to load. '
                        . 'Reason [%3$s].', get_class($this), $type, $e->getMessage()));
        }
    }

    /**
     * Returns the identifiers of all registered socket applications.
     * @return array
     */
    public function list(): array
    {
        return array_keys(self::$socket_applications->toArray());
    }

    /**
     * Returns information about a given socket application
     * @param string $application
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     * @throws \InvalidArgumentException If the provided application identifier does not exist.
     */
    public function info(string $application): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        if (!self::$socket_applications->has($application)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided application identifier [%2$s] does not exist.', get_class($this), $application));
        }
        return $this::containerize(self::$socket_applications->get($application));
    }

    /**
     * Returns the configuration details for a socket server application
     * @param string $application
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     * @throws \InvalidArgumentException If the given socket app identifier is not known
     */
    protected function getApplicationDetails(string $application): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        if (!self::$socket_applications->has($application)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided socket application identifier [%2$s] does not exist.', get_class($this), $application));
        }
        return $this::containerize(self::$socket_applications->get($application));
    }

    /**
     * Loads the server instance for a socket application
     * @param string $application
     * @param array|null $arguments (optional) Any arguments to pass into the application instance
     * @param array|null $flags (optional) Any flags to pass into the application instance
     * @return void
     */
    protected function loadSocketApplication(string $application, array $arguments = null, array $flags = null): \Ratchet\Server\IoServer
    {
        $config = $this->getApplicationDetails($application);
        $server = \Ratchet\Server\IoServer::factory(
                new \Ratchet\Http\HttpServer(
                    new \Ratchet\WebSocket\WsServer(
                        $this->load('library', $config->get('application'), null, $arguments, $flags)
                    )
                )
                , $config->get('port')
                , $config->get('host'));
        return $server;
    }

    /**
     * This method can be used to provide a list of socket applications.
     * By default, it will look for a configuration file named "sockets.json"
     * in the application configuration directory, and return that files contents.
     * This will be merged with the default configuration from oroboros.
     * 
     * This method can be overridden to provide alternate socket application
     * lookup functionality.
     * Return `parent::declareSocketApplications($your_socket_applications)`
     * to add the baseline socket application definitions.
     * Provided keys will override defaults when supplied this way.
     * 
     * You may also just set up a "sockets.json" config file if you do not want
     * to override this method, and they will be automatically parsed.
     * 
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $apps
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    protected static function declareSocketApplications(\Oroboros\core\interfaces\library\container\ContainerInterface $apps = null): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $proxy = new \Oroboros\core\pattern\factory\LoaderProxy();
        if (is_null($apps)) {
            $apps = static::containerize();
        }
        // Parse the baseline socket applications
        foreach ($this->getModel('socket')->fetch() as $key => $value) {
            if (!$apps->has($key)) {
                $apps[$key] = static::formatSocketConfigDetails($value);
            }
        }
        // Parse the app socket applications, if they differ from the base framework
        if (static::app()->getApplicationConfigDirectory() !== OROBOROS_CORE_CONFIG) {
            try {
                foreach ($this->getModel('socket')->fetch() as $key => $value) {
                    if (!$apps->has($key)) {
                        $apps[$key] = static::formatSocketConfigDetails($value);
                    }
                }
            } catch (\Exception $e) {
                // no-op if no socket.json in the current application scope
            }
        }
        return $apps;
    }

    /**
     * Initializes the socket application list
     * @return void
     * @private
     */
    private static function initializeSocketApplicationList(): void
    {
        if (is_null(self::$socket_applications)) {
            self::$socket_applications = static::declareSocketApplications();
        }
    }

    /**
     * Applies the default socket port and host if they
     * do not exist in the provided configuration
     * @param array $details
     * @return array
     * @private
     */
    private static function formatSocketConfigDetails(array $details): array
    {
        if (!array_key_exists('port', $details)) {
            $details['port'] = static::DEFAULT_SOCKET_PORT;
        }
        if (!array_key_exists('host', $details)) {
            $details['port'] = static::DEFAULT_SOCKET_HOST;
        }
        return $details;
    }
}
