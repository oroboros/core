<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\http;

/**
 * Abstract Http Cookie Builder
 * Provides abstraction for building http cookies
 *
 * @author Brian Dayhoff
 */
abstract class AbstractCookieBuilder extends \Oroboros\core\abstracts\pattern\builder\AbstractBuilder implements \Oroboros\core\interfaces\library\LibraryInterface
{

    const CLASS_SCOPE = 'http';
    const WORKER_SCOPE = 'http';
    const WORKER_TASK = 'cookie';

    /**
     * Use the default response container
     */
    const CONTAINER_CLASS = 'Oroboros\\core\\library\\container\\ResponseContainer';

    public function executeTask(array $arguments = [], array $flags = [])
    {
        $cookies = $this->containerize();
        foreach (array_keys($this->getCategories()->toArray()) as $category) {
            $cookies = $this->packageCookies($category, $cookies);
        }
        return $cookies;
    }

    protected function declareCategories(): array
    {
        return [
            'cookie' => ['array', 'Oroboros\\core\\interfaces\\library\\container\\ContainerInterface'],
        ];
    }

    protected function packageCookies(string $category, \Oroboros\core\interfaces\library\container\ResponseContainerInterface $output): \Oroboros\core\interfaces\library\container\ResponseContainerInterface
    {
        $properties = $this->get($category);
        foreach ($properties as $name => $value) {
            $output[$name] = $value;
        }
        return $output;
    }
}
