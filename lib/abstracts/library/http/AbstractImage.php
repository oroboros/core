<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\http;

/**
 * Abstract Image
 * Defines base abstraction for including images performantly
 * 
 * @author Brian Dayhoff
 */
abstract class AbstractImage extends AbstractDependency implements \Oroboros\core\interfaces\library\http\ImageInterface
{

    /**
     * Defines further child classes as images.
     */
    const RESOURCE_TYPE = 'image';

    /**
     * Determines whether or not the image is inline (eg in the event of inline svg).
     * @var bool
     */
    private $is_inline = false;

    public function __toString()
    {
        $source = $this->getSource();
        $crossorigin = $this->getCrossOrigin();
        $integrity = $this->getIntegrity();
        $tag = '<img %1$s>';
        if ($this->is_inline) {
            $attributes = sprintf(' src="data:image/svg+xml;base64,%1$s"', base64_encode($source));
        } else {
            $attributes = sprintf(' src="%1$s"', $source);
            if (!is_null($integrity) && $this->useCdn()) {
                $attributes .= sprintf(' integrity="%1$s"', $integrity);
            }
            if (!is_null($crossorigin) && $this->useCdn()) {
                $attributes .= sprintf(' crossorigin="%1$s"', $crossorigin);
            }
        }
        $output = sprintf($tag, $attributes, '');
        return $output;
    }
}
