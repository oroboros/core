<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\http;

/**
 * Abstract Http Message Builder
 * Provides abstraction for building restful response messages
 *
 * @author Brian Dayhoff
 */
abstract class AbstractMessageBuilder extends \Oroboros\core\abstracts\pattern\builder\AbstractBuilder implements \Oroboros\core\interfaces\library\LibraryInterface
{

    const CLASS_SCOPE = 'http';
    const WORKER_SCOPE = 'http';
    const WORKER_TASK = 'message';

    /**
     * Use the default response container
     */
    const CONTAINER_CLASS = 'Oroboros\\core\\library\\container\\ResponseContainer';

    /**
     * Set of known data details
     * @var Oroboros\core\interfaces\library\container\ContainerInterface 
     */
    private static $data = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeData();
    }

    public function executeTask(array $arguments = [], array $flags = [])
    {
        $data = $this->containerizeInto('Oroboros\\core\\library\\container\\ResponseCollection', []);
        foreach (array_keys(self::$data->toArray()) as $category) {
            if ($category === 'layout') {
                continue;
            }
            if (in_array($category, ['message', 'error', 'debug']) && empty($this->get($category)->toArray())) {
                // Do not include empty response sections
                continue;
            }
            $this->packageData($category, $data);
        }
        return $data;
    }

    protected function declareCategories(): array
    {
        return [
            'status' => ['integer'],
            'message' => ['boolean', 'null', 'integer', 'float', 'string', 'array', 'Oroboros\\core\\interfaces\\library\\container\\ContainerInterface'],
            'error' => ['boolean', 'null', 'integer', 'float', 'string', 'array', 'Oroboros\\core\\interfaces\\library\\container\\ContainerInterface'],
            'debug' => ['boolean', 'null', 'integer', 'float', 'string', 'array', 'Oroboros\\core\\interfaces\\library\\container\\ContainerInterface'],
        ];
    }

    protected function packageData(string $category, \Oroboros\core\interfaces\library\container\ResponseCollectionInterface $existing): void
    {
        if (!$existing->has($category)) {
            $existing[$category] = $this->containerize();
        }
        $output = $existing->get($category);
        $properties = $this->get($category);
        foreach ($properties as $name => $value) {
            if ($this->checkExisting($name, $existing)) {
                // Do not double-package data
                continue;
            }
            $existing[$category][$name] = $value;
        }
    }

    /**
     * Returns whether a provided data was already added to the response collection
     * @param string $key
     * @param \Oroboros\core\interfaces\library\container\ResponseCollectionInterface $existing
     * @return bool
     */
    private function checkExisting(string $key, \Oroboros\core\interfaces\library\container\ResponseCollectionInterface $existing): bool
    {
        foreach ($existing as $section) {
            if ($section->has($key)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Establishes a container for message data.
     * @return void
     */
    private function initializeData()
    {
        $data_collection = $this->containerizeInto('Oroboros\\core\\library\\container\\Collection');
        if (is_null(self::$data)) {
            foreach ($this->declareCategories() as $format => $allow) {
                $data = [];
                $data_collection[$format] = $this->containerizeInto('Oroboros\\core\\library\\container\\Container', []);
            }
            self::$data = $data_collection;
        }
    }
}
