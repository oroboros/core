<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\http;

/**
 * Abstract Dependency
 * Base level abstraction for defining dependencies sent to the client via a http request.
 * This class works extensively with Psr-7 UriInterface objects to standardize how assets are served.
 * @author Brian Dayhoff
 */
abstract class AbstractDependency extends \Oroboros\core\abstracts\library\AbstractLibrary implements \Oroboros\core\interfaces\library\http\DependencyInterface
{

    /**
     * Defines further child classes as message dependencies
     */
    const CLASS_SCOPE = 'dependency';

    /**
     * Child classes must define a resource type to be valid.
     */
    const RESOURCE_TYPE = null;

    /**
     * An array of valid resource types, as per RFC 8297
     * @var array
     */
    private static $valid_resource_types = [
        'script',
        'style',
        'image',
        'font',
        'document'
    ];

    /**
     * The slug name of the dependency
     * @var string
     */
    private $key = null;

    /**
     * The version, if applicable, for cache-busting.
     * @var string
     */
    private $version = null;

    /**
     * The dependency location. May be a relative or fully qualified Uri.
     * If the location is relative, the existence of the dependency will be checked against the filesystem.
     * @var \Psr\Http\Message\UriInterface|null
     */
    private $source = null;

    /**
     * The dependency sourcemap uri, if applicable.
     * @var \Psr\Http\Message\UriInterface|null
     */
    private $sourcemap = null;

    /**
     * An array of keywords corresponding to dependency resources, if any exist.
     * A dependency can only be registered into the output source after all of its
     * dependencies (and their dependencies) have been included.
     * @var array
     */
    private $dependencies = [];

    /**
     * An array of keywords corresponding to expected font resources, if any exist.
     * @var array
     */
    private $fonts = [];

    /**
     * An array of keywords corresponding to expected stylesheet resources, if any exist.
     * @var array
     */
    private $stylesheets = [];

    /**
     * An array of keywords corresponding to expected script resources, if any exist.
     * @var array
     */
    private $scripts = [];

    /**
     * The dependency CDN location, if applicable. Must be a fully qualified Uri
     * @var \Psr\Http\Message\UriInterface|null
     */
    private $cdn = null;

    /**
     * Whether or not the dependency is served from the local domain,
     * which indicates whether or not to modify the access control headers.
     * @var bool
     */
    private $is_local = false;

    /**
     * The hostname of the server providing the resource. Will always be local when
     * a relative link is provided, otherwise will be extracted from the provided
     * source automatically.
     * @var \Psr\Http\Message\UriInterface|null
     */
    private $hostname = null;

    /**
     * The SHA integrity signing if applicable, for use with SHA integrity checks
     * @var string
     */
    private $integrity = null;

    /**
     * The crossorigin signing, for used with SHA integrity checks
     * @var string
     */
    private $crossorigin = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        $this->verifyResourceType();
        parent::__construct($command, $arguments, $flags);
        $this->verifyKey($command);
        $this->key = $command;
        $this->initializeDependency();
    }

    /**
     * Returns the semantic tag used to identify the resource in html source.
     * @return string
     */
    abstract public function __toString();

    /**
     * Sets the source location.
     * May be a relative or absolute uri.
     * If a relative link is passed, the filesystem will be checked for the resource.
     * 
     * @param string $source
     * @return $this (method chainable)
     */
    public function withSource(string $source): \Oroboros\core\interfaces\library\http\DependencyInterface
    {
        $this->source = $this->getUriObject($source);
        $this->updateHost($this->source);
        if ($this->is_local && !is_null($this->version)) {
            // Update the uri string to include the version as a get param.
            $this->source = $this->getUriObject((string) $this->source . '?version=' . $this->getVersion());
        }
        return $this;
    }

    /**
     * Sets the sourcemap location, if applicable.
     * May be a relative or absolute uri.
     * If a relative link is passed, the filesystem will be checked for the resource.
     * 
     * @param string $source_map
     * @return $this (method chainable)
     */
    public function withSourceMap(string $source_map): \Oroboros\core\interfaces\library\http\DependencyInterface
    {
        $this->sourcemap = $this->getUriObject($source_map);
        return $this;
    }

    /**
     * Removes the registered sourcemap location, if any exists.
     * @param string $source_map
     * @return $this (method chainable)
     */
    public function withoutSourceMap(): \Oroboros\core\interfaces\library\http\DependencyInterface
    {
        $this->sourcemap = null;
        return $this;
    }

    /**
     * Sets the cdn location, if applicable.
     * Must be an absolute uri.
     * If integrity and crossorigin user are required, those methods must be called separately.
     * 
     * @param string $cdn
     * @return $this (method chainable)
     */
    public function withCdn(string $cdn): \Oroboros\core\interfaces\library\http\DependencyInterface
    {
        $this->cdn = $this->getUriObject($cdn);
        return $this;
    }

    /**
     * Removes the cdn location, if any exists.
     * @return $this (method chainable)
     */
    public function withoutCdn(): \Oroboros\core\interfaces\library\http\DependencyInterface
    {
        $this->cdn = null;
        return $this;
    }

    public function withVersion(string $version): \Oroboros\core\interfaces\library\http\DependencyInterface
    {
        $this->version = $version;
        return $this;
    }

    /**
     * Returns the key name of the dependency.
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * Returns the source uri for the dependency.
     * @return \Psr\Http\Message\UriInterface|null
     */
    public function getSource(): ?\Psr\Http\Message\UriInterface
    {
        return $this->source;
    }

    /**
     * Returns the source uri for the cdn, if applicable.
     * Returns null if no cdn is registered, or if the USE_CDN flag is not not set or is false.
     * @return \Psr\Http\Message\UriInterface|null
     */
    public function getCdn(): ?\Psr\Http\Message\UriInterface
    {
        return $this->cdn;
    }

    /**
     * Gets the host name of the dependency.
     * If the dependency supplies a relative uri, this will always be the local host name.
     * 
     * @return \Psr\Http\Message\UriInterface|null
     */
    public function getHost(): ?\Psr\Http\Message\UriInterface
    {
        return $this->hostname;
    }

    /**
     * Returns the version, if it exists.
     * @return string|null
     */
    public function getVersion(): ?string
    {
        return $this->version;
    }

    /**
     * Returns a list of any expected fonts required by the dependency.
     * @return array
     */
    public function getExpectedFonts(): array
    {
        return $this->fonts;
    }

    /**
     * Returns a list of any expected scripts required by the dependency.
     * @return array
     */
    public function getExpectedScripts(): array
    {
        return $this->scripts;
    }

    /**
     * Returns a list of any expected stylesheets required by the dependency.
     * @return array
     */
    public function getExpectedStylesheets(): array
    {
        return $this->stylesheets;
    }

    /**
     * Gets the prefetch link for the dependency.
     * @return string
     */
    public function getPrefetch(): string
    {
        $result = sprintf('<link rel="prefetch" href="%1$s">', $this->getSource());
        return $result;
    }

    /**
     * Gets the preload link for the dependency.
     * @return string
     */
    public function getPreload(): string
    {
        $result = sprintf('<link rel="preload" href="%1$s" as="%2$s">', $this->getSource(), static::RESOURCE_TYPE);
        return $result;
    }

    /**
     * Gets the preconnect link for the dependency host.
     * @return string
     */
    public function getPreconnect(): string
    {
        $result = sprintf('<link href="%1$s" rel="preconnect" crossorigin>', $this->getHost());
        return $result;
    }

    /**
     * Gets the sha1 integrity hash if one exists, or null if one does not exist.
     * @return string|null
     */
    public function getIntegrity(): ?string
    {
        return $this->integrity;
    }

    /**
     * Gets the crossorigin username if one exists, or null if one does not exist.
     * @return string|null
     */
    public function getCrossOrigin(): ?string
    {
        return $this->crossorigin;
    }

    /**
     * Gets an array of the dependencies that must be included
     * prior to the inclusion of the dependency.
     * @return array
     */
    public function getDependencies(): array
    {
        return $this->dependencies;
    }

    /**
     * Returns whether an asset is local or not.
     * Crossorigin and integrity can be omitted for locally served uris.
     * @return bool
     */
    protected function isLocal(): bool
    {
        return $this->is_local;
    }

    /**
     * Returns whether or not to use the cdn or the local copy.
     * @return bool
     * @note not yet implemented
     */
    protected function useCdn(): bool
    {
        return false;
    }

    /**
     * Builds the dependency details based on injected arguments, if any exist.
     * @return void
     */
    protected function initializeDependency()
    {
        if ($this->hasArgument('version')) {
            $this->withVersion($this->getArgument('version'));
        }
        if ($this->hasArgument('source') && !is_null($this->getArgument('source'))) {
            if (is_array($this->getArgument('source'))) {
                foreach ($this->getArgument('source') as $key => $value) {
                    $this->withSource($value);
                }
            } else {
                $this->withSource($this->getArgument('source'));
            }
        }
        if ($this->hasArgument('sourcemap') && !is_null($this->getArgument('sourcemap'))) {
            $this->withSourceMap($this->getArgument('sourcemap'));
        }
        if ($this->hasArgument('cdn') && !is_null($this->getArgument('cdn'))) {
            $this->withCdn($this->getArgument('cdn'));
        }
        if ($this->hasArgument('integrity')) {
            $this->integrity = $this->getArgument('integrity');
        }
        if ($this->hasArgument('crossorigin')) {
            $this->crossorigin = $this->getArgument('crossorigin');
        }
        if ($this->hasArgument('dependencies') && !is_null($this->getArgument('dependencies'))) {
            $this->dependencies = $this->getArgument('dependencies');
        }
        if ($this->hasArgument('fonts') && !is_null($this->getArgument('fonts'))) {
            $this->fonts = $this->getArgument('fonts');
        }
        if ($this->hasArgument('scripts') && !is_null($this->getArgument('scripts'))) {
            $this->scripts = $this->getArgument('scripts');
        }
        if ($this->hasArgument('stylesheets') && !is_null($this->getArgument('stylesheets'))) {
            $this->stylesheets = $this->getArgument('stylesheets');
        }
    }

    /**
     * Verifies that child classes are correctly configured with a valid resource type,
     * and prevents instantiation if they are not.
     * 
     * @throws \Oroboros\core\exception\core\InvalidClassException
     * @private
     * @final
     */
    private function verifyResourceType()
    {
        if (is_null(static::RESOURCE_TYPE) || !in_array(static::RESOURCE_TYPE, self::$valid_resource_types)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(sprintf('Class [%1$s] is invalid. '
                        . 'Class constant [%2$s] must be defined with a valid resource type [%3$s].', get_class($this), 'RESOURCE_TYPE', implode(', ', self::$valid_resource_types)));
        }
    }

    private function verifyKey($key)
    {
        if (!is_string($key)) {
            throw new \Oroboros\core\exception\ErrorException('Error encountered in [%1$s]. '
                    . 'Invalid dependency key provided. Expected [%2$s] but '
                    . 'received [%3$s].', get_class($this), 'string', gettype($key));
        }
    }

    /**
     * Updates the host name
     * @param \Psr\Http\Message\UriInterface $uri
     */
    private function updateHost(\Psr\Http\Message\UriInterface $uri)
    {
        $host = $uri->getScheme() . '://' . $uri->getHost();
        if ($host === OROBOROS_CORE_HOST) {
            $this->is_local = true;
        } else {
            $this->is_local = false;
        }
        $this->hostname = new \Zend\Diactoros\Uri($host);
    }

    /**
     * Returns a Psr-7 Uri object representing the provided uri.
     * This allows the host/uri/etc to be easily extracted.
     * 
     * @param string $uri
     * @return \Psr\Http\Message\UriInterface
     */
    private function getUriObject(string $uri)
    {
        $uri = new \Zend\Diactoros\Uri($this->checkLocalAsset($uri));
        return $uri;
    }

    private function checkLocalAsset(string $uri)
    {
        $prefix = OROBOROS_CORE_BASE_DIRECTORY;
        $local_url = OROBOROS_CORE_HOST;
        if (strpos($uri, $local_url) === 0) {
            // This is a local asset, but it already has a fully qualified uri. Do nothing.
            return $uri;
        }
        if (file_exists($prefix . ltrim($uri, '\\/'))) {
            // This is a local asset that uses a relative uri. Update it to use the fully qualified uri.
            $uri = $local_url . '/' . ltrim($uri, '\\/');
        }
        return $uri;
    }
}
