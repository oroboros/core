<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\http;

/**
 * Abstract Http Response Builder
 * Provides abstraction for building http responses
 *
 * @author Brian Dayhoff
 */
abstract class AbstractResponseBuilder extends \Oroboros\core\abstracts\pattern\builder\AbstractBuilder implements \Oroboros\core\interfaces\library\LibraryInterface
{

    const CLASS_SCOPE = 'http';
    const WORKER_SCOPE = 'http';
    const WORKER_TASK = 'response';

    /**
     * Use the default response container
     */
    const CONTAINER_CLASS = 'Oroboros\\core\\library\\container\\ResponseContainer';

    public function executeTask(array $arguments = [], array $flags = [])
    {
        return $this->build($flags);
    }

    public function build(array $flags = [])
    {
        $result = $this->containerizeInto('Oroboros\\core\\library\\container\\ResponseContainer');
        foreach (array_keys($this->getCategories()->toArray()) as $category) {
            $this->packageResponse($category, $this->get($category)->toArray(), $result);
        }
        $output = $this->containerize([
            'code' => is_null($result->get('code')) ? 200 : $result->get('code'),
            'cookie' => $result->get('cookie'),
            'header' => $result->get('header'),
            'context' => $this->getDirector()->getWorker('message')->build($flags)
        ]);
        return $output;
    }

    protected function declareCategories(): array
    {
        return [
            'code' => 'integer',
            'cookie' => ['array', 'Oroboros\\core\\interfaces\\library\\container\\ContainerInterface'],
            'header' => ['array', 'Oroboros\\core\\interfaces\\library\\container\\ContainerInterface'],
            'content' => ['array', 'Oroboros\\core\\interfaces\\library\\container\\ContainerInterface']
        ];
    }

    protected function packageResponse(string $category, array $data, \Oroboros\core\interfaces\library\container\ContainerInterface $existing): void
    {
        switch ($category) {
            case 'code':
                if (array_key_exists($category, $data) && property_exists($data[$category], 'http')) {
                    $existing[$category] = $data[$category]['http'];
                } elseif (array_key_exists($category, $data) && property_exists($data[$category], 'http')) {
                    $existing[$category] = $data[$category]['cli'];
                } else {
                    $existing[$category] = 200;
                }
                break;
            case 'cookie':
            case 'header':
            case 'content':
                $existing[$category] = $this->containerize($data);
                break;
            default:
                d($category, $data, $existing->toArray());
                exit;
                break;
        }
    }
}
