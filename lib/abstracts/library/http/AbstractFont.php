<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\http;

/**
 * Abstract Font
 * Defines base abstraction for including fonts
 * 
 * @author Brian Dayhoff
 * @note It is best practice to keep the font declaration within the stylesheet.
 *     Thereby, this object should typically only be leveraged for using preload/prefetch
 *     to optimize page load time, and should not be used for direct font declaration or inclusion.
 */
abstract class AbstractFont extends AbstractDependency implements \Oroboros\core\interfaces\library\http\FontInterface
{

    /**
     * Defines further child classes as fonts.
     */
    const RESOURCE_TYPE = 'font';

    /**
     * Defines the default font format used for the font [eot, ttf, woff, woff2]
     * This can be overridden by passing the `font-format` flag to the constructor.
     */
    const DEFAULT_FONT_FORMAT = 'woff2';

    /**
     * Defines the localized font format used to define the font.
     * If the constructor flag `font-format` was not passed,
     * this will be the class constant `DEFAULT_FONT_FORMAT`.
     * @var string
     */
    private $font_format = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        $this->initializeFontFormat(is_null($flags) ? [] : $flags);
        parent::__construct($command, $arguments, $flags);
    }

    public function __toString()
    {
        $source = $this->getSource();
        $crossorigin = $this->getCrossOrigin();
        $integrity = $this->getIntegrity();
        $tag = '<link type="text/css" rel="stylesheet"%1$s>';
        $attributes = sprintf(' href="%1$s"', $source);
        if (!is_null($integrity) && $this->useCdn()) {
            $attributes .= sprintf(' integrity="%1$s"', $integrity);
        }
        if (!is_null($crossorigin) && $this->useCdn()) {
            $attributes .= sprintf(' crossorigin="%1$s"', $crossorigin);
        }
        $output = sprintf($tag, $attributes, '');
        return $output;
    }

    public function getFormat(): string
    {
        return $this->font_format;
    }

    /**
     * Scopes the font to the declared font format.
     * @return void
     */
    protected function initializeDependency()
    {
        if ($this->hasArgument('source') && !is_null($this->getArgument('source'))) {
            $source = $this->getArgument('source');
            if (array_key_exists($this->font_format, $source)) {
                $this->setArgument('source', $source[$this->font_format]);
            }
        }
        parent::initializeDependency();
    }

    private function initializeFontFormat(array $flags)
    {
        if (array_key_exists('font-format', $flags)) {
            $this->font_format = $flags['font-format'];
        } else {
            $this->font_format = static::DEFAULT_FONT_FORMAT;
        }
    }
}
