<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\component\html;

/**
 * Abstract HTML Form Textarea Component
 * Provides abstraction for HTML form textarea components
 * @author Brian Dayhoff
 */
abstract class AbstractHtmlFormTextareaComponent extends AbstractHtmlComponent implements \Oroboros\core\interfaces\library\component\HtmlFormFieldComponentInterface
{

    use \Oroboros\core\traits\component\html\form\ComponentHtmlFormFieldUtilitiesTrait;

    /**
     * Declares the form parser used to resolve values for the given
     * form represented by the component.
     * 
     * This may be overridden with a custom form parser
     * that suits the needs of the specific component.
     */
    const FORM_PARSER_CLASS = \Oroboros\core\library\form\FormParser::class;

    /**
     * Override this constant to declare a custom form field label
     * class to use for labeling form fields
     */
    const FORM_FIELD_LABEL_CLASS = null;

    /**
     * This method may be overridden to declare a different default input type
     * If no input type context is given, this type will be used.
     */
    const FORM_FIELD_DEFAULT_TYPE = 'input';

    /**
     * Form specific valid context types
     * @var array
     */
    private $form_context_keys = [
        "name", // Sets the name of the form textarea
        "type", // Sets the type of the form textarea
        "label", // Sets the label for the form textarea
        "autofocus", // Sets the autofocus parameter for the form textarea
        "cols", // Sets the cols parameter for the form textarea
        "dirname", // Sets the dirname parameter for the form textarea
        "disabled", // Sets disabled parameter label for the form textarea
        "form", // Sets the form parameter for the form textarea
        "maxlength", // Sets the maxlength parameter for the form textarea
        "placeholder", // Sets the placeholder parameter for the form textarea
        "readonly", // Sets the readonly parameter for the form textarea
        "required", // Sets the required parameter for the form textarea
        "rows", // Sets the rows parameter for the form textarea
        "wrap", // Sets the wrap parameter for the form textarea
    ];

    /**
     * Validation filters for form field specific context types
     * @var array
     */
    private $context_form_filters = [
        'name' => 'filterName',
        'type' => 'filterType',
        'value' => 'filterValue',
        'label' => 'filterLabel',
        'autofocus' => 'filterAutofocus',
        'cols' => 'filterCols',
        'dirname' => 'filterDirname',
        'disabled' => 'filterDisabled',
        'form' => 'filterForm',
        'maxlength' => 'filterMaxlength',
        'placeholder' => 'filterPlaceholder',
        'readonly' => 'filterReadonly',
        'required' => 'filterRequired',
        'rows' => 'filterRows',
        'wrap' => 'filterWrap',
    ];

    /**
     * Represents resolvable parameters for textarea parameters
     * @var array
     */
    private static $textarea_valid_parameters = [
        'name' => [null, false, 'string'],
        'type' => ['textarea'],
        'value' => [null, false, 'string'],
        'label' => [null, false, 'string'],
        'autofocus' => [null, 'bool'],
        'cols' => [null, false, 'integer'],
        'dirname' => [null, 'bool'],
        'disabled' => [null, 'bool'],
        'form' => [null, false, 'string'],
        'maxlength' => [null, false, 'integer'],
        'placeholder' => [null, false, 'string'],
        'readonly' => [null, 'bool'],
        'required' => [null, 'bool'],
        'rows' => [null, false, 'integer'],
        'wrap' => [null, false, 'hard', 'soft'],
    ];

    /**
     * The resolver used to validate passed parameters
     * 
     * @var \Oroboros\core\interfaces\library\resolver\ResolverInterface
     */
    private static $resolver = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        $this->verifyFormLabelClass();
        $this->verifyFormParserClass();
        parent::__construct($command, $arguments, $flags);
        $this->initializeTextarea();
    }

    /**
     * Initializes the default form context
     * 
     * @return void
     */
    protected function initializeContext(): void
    {
        parent::initializeContext();
        // Default tagname is "textarea"
        $this->addContext('tagname', 'textarea');
        // Default type is "textarea"
        $this->addContext('type', 'textarea');
        // No assumption of a textarea value
        $this->addContext('value', null);
        // Default id is the passed command, if any
        $this->addContext('id', $this->getCommand());
        // Default id is the passed command, if any
        $this->addContext('name', $this->getCommand());
        // The default type is "textarea". This is the only valid type
        $this->addContext('type', 'textarea');
        // No assumption of a form field label
        $this->addContext('label', null);
        // No assumption of a textarea autofocus parameter
        $this->addContext('autofocus', null);
        // No assumption of a textarea cols parameter
        $this->addContext('cols', null);
        // Dirname is true by default
        $this->addContext('dirname', true);
        // No assumption of a textarea disabled parameter
        $this->addContext('disabled', null);
        // No assumption of a textarea form parameter
        $this->addContext('form', null);
        // No assumption of a textarea maxlength parameter
        $this->addContext('maxlength', null);
        // No assumption of a textarea placeholder parameter
        $this->addContext('placeholder', null);
        // No assumption of a textarea readonly parameter
        $this->addContext('readonly', null);
        // No assumption of a textarea required parameter
        $this->addContext('required', null);
        // No assumption of a textarea rows parameter
        $this->addContext('rows', null);
        // No assumption of a textarea wrap parameter
        $this->addContext('wrap', null);
    }
    
    /**
     * Override this method to expand or constrain
     * the valid field types for the given component.
     * 
     * The default behavior is to return a list of
     * all valid html5 input types.
     * 
     * This method's return value is used to validate
     * a given field type entry.
     * 
     * @return array
     */
    protected function declareValidFieldTypes(): array
    {
        return ['textarea'];
    }

    /**
     * Initializes the textarea component
     * 
     * @return void
     */
    private function initializeTextarea(): void
    {
        
    }

}
