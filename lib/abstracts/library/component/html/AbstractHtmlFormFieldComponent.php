<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\component\html;

/**
 * Abstract HTML Form Field Component
 * Provides abstraction for HTML form field components
 * @author Brian Dayhoff
 */
abstract class AbstractHtmlFormFieldComponent extends AbstractHtmlComponent implements \Oroboros\core\interfaces\library\component\HtmlFormFieldComponentInterface
{

    use \Oroboros\core\traits\component\html\form\ComponentHtmlFormFieldUtilitiesTrait;

    /**
     * Declares the form parser used to resolve values for the given
     * form represented by the component.
     * 
     * This may be overridden with a custom form parser
     * that suits the needs of the specific component.
     */
    const FORM_PARSER_CLASS = \Oroboros\core\library\form\FormParser::class;

    /**
     * Override this constant to declare a custom form field label
     * class to use for labeling form fields
     */
    const FORM_FIELD_LABEL_CLASS = null;

    /**
     * This method may be overridden to declare a different default input type
     * If no input type context is given, this type will be used.
     */
    const FORM_FIELD_DEFAULT_TYPE = 'input';

    /**
     * Form specific valid context types
     * @var array
     */
    private $form_context_keys = [
        "name", // Sets the name of the form field
        "type", // Sets the type of the form field
        "label", // Sets the label for the form field
        'value', // Sets the value for the form field
        'accept', // Sets the accept parameter for the form field
        'alt', // Sets the alt parameter for the form field
        'autocomplete', // Sets the autocomplete parameter for the form field
        'autofocus', // Sets the autofocus parameter for the form field
        'checked', // Sets the checked parameter for the form field
        'dirname', // Sets the dirname parameter for the form field
        'disabled', // Sets the disabled parameter for the form field
        'form', // Sets the form parameter for the form field
        'formaction', // Sets the formaction parameter for the form field
        'formenctype', // Sets the formenctype parameter for the form field
        'formmethod', // Sets the formmethod parameter for the form field
        'formnovalidate', // Sets the formnovalidate parameter for the form field
        'formtarget', // Sets the formtarget parameter for the form field
        'list', // Sets the list parameter for the form field
        'max', // Sets the max parameter for the form field
        'maxlength', // Sets the maxlength parameter for the form field
        'min', // Sets the min parameter for the form field
        'minlength', // Sets minlength accept parameter for the form field
        'multiple', // Sets the multiple parameter for the form field
        'pattern', // Sets the pattern parameter for the form field
        'placeholder', // Sets the placeholder parameter for the form field
        'readonly', // Sets the readonly parameter for the form field
        'required', // Sets the required parameter for the form field
        'size', // Sets the size parameter for the form field
        'src', // Sets the src parameter for the form field
        'step', // Sets the step parameter for the form field
        'width', // Sets the width parameter for the form field
    ];

    /**
     * Validation filters for form field specific context types
     * @var array
     */
    private $context_form_filters = [
        'name' => 'filterName',
        'type' => 'filterType',
        'value' => 'filterValue',
        'label' => 'filterLabel',
        'autofocus' => 'filterAutofocus',
        'dirname' => 'filterDirname',
        'disabled' => 'filterDisabled',
        'form' => 'filterForm',
        'maxlength' => 'filterMaxlength',
        'placeholder' => 'filterPlaceholder',
        'readonly' => 'filterReadonly',
        'required' => 'filterRequired',
        'accept' => 'filterAccept',
        'alt' => 'filterAlt',
        'autocomplete' => 'filterAutocomplete',
        'checked' => 'filterChecked',
        'formaction' => 'filterFormAction',
        'formenctype' => 'filterFormEnctype',
        'formmethod' => 'filterFormMethod',
        'formnovalidate' => 'filterValidate',
        'formtarget' => 'filterFormTarget',
        'list' => 'filterList',
        'max' => 'filterMax',
        'min' => 'filterMin',
        'minlength' => 'filterMinLength',
        'multiple' => 'filterMultiple',
        'pattern' => 'filterPattern',
        'size' => 'filterSize',
        'src' => 'filterSrc',
        'step' => 'filterStep',
        'width' => 'filterWidth',
    ];

    /**
     * Valid field parameters and their corresponding valid values
     * 
     * @var array
     */
    private static $field_valid_parameters = [
        'name' => [null, false, 'string'],
        'type' => [
            null,
            'input',
            'button',
            'checkbox',
            'color',
            'date',
            'datetime-local',
            'email',
            'file',
            'hidden',
            'image',
            'month',
            'number',
            'password',
            'radio',
            'range',
            'reset',
            'search',
            'submit',
            'tel',
            'text',
            'time',
            'url',
            'week',
        ],
        'value' => [null, false, 'bool', 'string', 'int'],
        'accept' => [null, false, 'file_extension', 'audio/*', 'video/*', 'image/*', 'media_type'],
        'alt' => [null, false, 'string'],
        'autocomplete' => [null, false, 'on', 'off'],
        'autofocus' => [null, 'bool'],
        'checked' => [null, 'bool'],
        'dirname' => [null, 'bool'],
        'disabled' => [null, 'bool'],
        'form' => [null, false, 'string'],
        'formaction' => [null, false, 'string'],
        'formenctype' => [null, false, 'application/x-www-form-urlencoded', 'multipart/form-data', 'text/plain'],
        'formmethod' => [null, false, 'get', 'post'],
        'formnovalidate' => [null, false, 'formnovalidate'],
        'formtarget' => [null, false, '_blank', '_self', '_parent', '_top'],
        'list' => [null, false, 'string'],
        'max' => [null, false, 'string', 'int'],
        'maxlength' => [null, false, 'int'],
        'min' => [null, false, 'string', 'int'],
        'minlength' => [null, false, 'int'],
        'multiple' => [null, false, 'multiple'],
        'pattern' => [null, false, 'regex'],
        'placeholder' => [null, false, 'string'],
        'readonly' => [null, 'bool'],
        'required' => [null, 'bool'],
        'size' => [null, 'int'],
        'src' => [null, 'string'],
        'step' => [null, 'int', 'any'],
        'value' => [null, 'string', 'int', 'bool'],
        'width' => [null, false, 'int'],
        'label' => [null, false, 'string'],
    ];

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        $this->verifyFormFieldDefaultType();
        $this->verifyFormLabelClass();
        $this->verifyFormParserClass();
        parent::__construct($command, $arguments, $flags);
    }
    
    /**
     * Override this method to expand or constrain
     * the valid field types for the given component.
     * 
     * The default behavior is to return a list of
     * all valid html5 input types.
     * 
     * This method's return value is used to validate
     * a given field type entry.
     * 
     * @return array
     */
    protected function declareValidFieldTypes(): array
    {
        return self::$field_valid_parameters['type'];
    }

    /**
     * Initializes the default form context
     * 
     * @return void
     */
    protected function initializeContext(): void
    {
        parent::initializeContext();
        // Default tagname is "input"
        $this->addContext('tagname', 'input');
        // Default id is the passed command, if any
        $this->addContext('id', $this->getCommand());
        // Default id is the passed command, if any
        $this->addContext('name', $this->getCommand());
        // The input type will be set to the default for the class
        $this->addContext('type', static::FORM_FIELD_DEFAULT_TYPE);
        // No assumption of a form field label
        $this->addContext('label', null);
        // Dirname is true by default
        $this->addContext('dirname', true);
        // Default type is "input"
        $this->addContext('type', 'input');
        // No assumption of a form field value
        $this->addContext('value', null);
        // No assumption of autofocus
        $this->addContext('autofocus', null);
        // No assumption of disable
        $this->addContext('disabled', null);
        // No assumption of a form
        $this->addContext('form', null);
        // No assumption of maxlength
        $this->addContext('maxlength', null);
        // No assumption of placeholder text
        $this->addContext('placeholder', null);
        // No assumption of readonly
        $this->addContext('readonly', null);
        // No assumption of required
        $this->addContext('required', null);
        // No assumption of accept criteria
        $this->addContext('accept', null);
        // No assumption of alt
        $this->addContext('alt', null);
        // No assumption of autocomplete
        $this->addContext('autocomplete', null);
        // No assumption of checked status
        $this->addContext('checked', null);
        // No assumption of a form action
        $this->addContext('formaction', null);
        // No assumption of a form encoding type
        $this->addContext('formenctype', null);
        // No assumption of a form submit method
        $this->addContext('formmethod', null);
        // No assumption of a form validation regex
        $this->addContext('formnovalidate', null);
        // No assumption of a form target
        $this->addContext('formtarget', null);
        // No assumption of a form options list
        $this->addContext('list', null);
        // No assumption of a form max range
        $this->addContext('max', null);
        // No assumption of a form min range
        $this->addContext('min', null);
        // No assumption of a form min length
        $this->addContext('minlength', null);
        // No assumption of multiple
        $this->addContext('multiple', null);
        // No assumption of a pattern
        $this->addContext('pattern', null);
        // No assumption of a size
        $this->addContext('size', null);
        // No assumption of a src attribute
        $this->addContext('src', null);
        // No assumption of a step counter
        $this->addContext('step', null);
        // No assumption of width
        $this->addContext('width', null);
    }
}
