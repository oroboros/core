<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\component\html;

/**
 * Abstract HTML Component
 * Provides abstraction for HTML components
 * @author Brian Dayhoff
 */
abstract class AbstractHtmlComponent extends \Oroboros\core\abstracts\library\component\AbstractComponent implements \Oroboros\core\interfaces\library\component\HtmlComponentInterface
{

    use \Oroboros\core\traits\component\ComponentContextFilterUtility;

    use \Oroboros\core\traits\html\HtmlComponentUtility;
    use \Oroboros\core\traits\html\HtmlAssetUtilityTrait;

    const COMPONENT_SCOPE = \Oroboros\core\abstracts\view\AbstractHtmlView::CLASS_SCOPE;
    const RESPONSE_DIRECTOR_CLASS = 'html\\ResponseDirector';
    
    /**
     * Sets a default permission for the component, which corresponds
     * to default view access.
     * 
     * You may override this constant to determine access
     * rights on a per-item basis.
     */
    const DEFAULT_PERMISSION = 'http.frontend.view';
    
    /**
     * The currently scoped permission for the component
     * 
     * @var string
     */
    private $permission = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeHtmlAssets();
        $this->initializeContext();
        $this->addContext('component', static::COMPONENT_KEY);
    }
    
    public function render(): string
    {
        if(!is_null($this->permission) && !$this::user()->can($this->permission)) {
            // not authorized
            // nullify content
            return '';
        }
        return parent::render();
    }
    
    public function setParent(\Oroboros\core\interfaces\library\component\ComponentInterface $parent): \Oroboros\core\interfaces\library\component\ComponentInterface
    {
        $parent = parent::setParent($parent);
        $this->addContext('parent', $parent->getKey());
        return $parent;
    }

    /**
     * Override this method to declare keys that should be passed from arguments 
     * into the context for the template, if they are provided.
     * @return array
     */
    protected function declareValidContextKeys(): array
    {
        return array_merge(parent::declareValidContextKeys(), [
            "tagname", // Sets the wrapper element tag name
            "id", // Sets the wrapper element id
            "class", // Sets the wrapper element class
            "component", // Sets the wrapper element data-component attribute
            "style", // Sets the inline styles for the wrapper element
            "role", // Sets the role attribute for the wrapper element
            "context", // Sets the data-context attribute for the wrapper element
            "category", // Sets the data-category attribute for the wrapper element
            "parent", // Sets the data-parent attribute for the wrapper element
            "attributes", // Provides an associative array of additional attributes
            "aria", // Sets accessibility attributes for the wrapper element
            "data", // Sets data-* elements for the wrapper element
            "permission", // determines access rights
        ]);
    }
    
    protected function filterPermission($permission): ?string
    {
        $this->permission = $permission;
        return null;
    }

    /**
     * Override  this function to perform internal transformations 
     * on context added to the component. This method will be called 
     * from the addContext method prior to setting the value into 
     * the context container to pass to the template. The return value
     * of this method will be the value used for the given key.
     * 
     * @note If a filter returns a component or a container,
     *       it will be returned directly.
     *       Arrays will be assumed to need component transformation.
     *       If you are encountering an error for no known valid component
     *       on a custom key, return a container or pre-package it as a component
     *       in your filter method.
     * 
     * @param string $key
     * @param type $value
     * @return mixed
     */
    protected function filterContext(string $key, $value)
    {
        $filters = $this->declareContextFilters();
        $valid = array_keys($filters);
        // Do not filter keys not belonging to this component
        if (!in_array($key, $valid)) {
            return $value;
        }
        // Do not filter keys that are already component instances
        if (is_object($value) && $value instanceof \Oroboros\core\interfaces\library\component\ComponentInterface) {
            return $value;
        }
        // If the given value is a string, wrap it in an array to pass to the designated component on load
        if (is_string($value)) {
            $value = [$key => $value];
        }
        if ($this->hasContext('id') && is_iterable($value)) {
            $value['parent'] = $this->getContext('id');
            $value['id'] = sprintf('%1$s-%2$s', $this->getContext('id'), $key);
            $value['context'] = sprintf('%1$s-%2$s', static::COMPONENT_KEY, $key);
            $value['category'] = static::COMPONENT_KEY;
        }
        // Run filter method
        if (array_key_exists($key, $filters) && method_exists($this, $filters[$key])) {
            $method = $filters[$key];
            if (is_object($value) && ($value instanceof \Oroboros\core\interfaces\library\container\ContainerInterface)) {
                $value = $value->toArray();
            }
            $value = $this->$method($value);
        }
        // Return non-arrays directly
        if (!is_array($value)) {
            return $value;
        }
        $component_name = $key;
        if (array_key_exists('component', $value)) {
            $component_name = $value['component'];
        }
        try {
            $component = $this->getComponent($component_name, $component_name, $value);
            $component->addContext('component', $component_name);
            return $component;
        } catch (\Oroboros\core\exception\InvalidArgumentException $e) {
            d($key, $component_name, $value, $e->getMessage());
            throw $e;
        }
    }

    protected function initializeContext(): void
    {
        parent::initializeContext();
        if (!$this->hasContext('attributes')) {
            $this->addContext('attributes', $this::containerize());
        }
        // use the default permission
        $this->addContext('permission', static::DEFAULT_PERMISSION);
    }
}
