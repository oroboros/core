<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\component\html;

/**
 * Abstract HTML Component
 * Provides abstraction for HTML components
 * @author Brian Dayhoff
 */
abstract class AbstractHtmlFormComponent extends AbstractHtmlComponent implements \Oroboros\core\interfaces\library\component\HtmlFormComponentInterface
{

    /**
     * Override this constant to declare a default class to use
     * for fieldset form section generation
     */
    const FORM_FIELDSET_CLASS = \Oroboros\core\library\component\form\Fieldset::class;

    /**
     * Declares the form parser used to resolve values for the given
     * form represented by the component.
     * 
     * This may be overridden with a custom form parser
     * that suits the needs of the specific component.
     */
    const FORM_PARSER_CLASS = \Oroboros\core\library\form\FormParser::class;

    /**
     * Form specific valid context types
     * @var array
     */
    private $form_context_keys = [
        "name", // Sets the name of the form
        "method", // Sets the method the form will use to respond to the server
        "action", // Sets uri for the form to send it's submission to
        "fields", // Sets uri for the form to send it's submission to
    ];

    /**
     * Validation filters for form specific context types
     * @var array
     */
    private $context_form_filters = [
        'name' => 'filterName',
        'method' => 'filterMethod',
        'action' => 'filterAction',
        'fields' => 'filterFields',
    ];

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        $this->verifyFormFieldsetClass();
        $this->verifyFormParserClass();
        parent::__construct($command, $arguments, $flags);
    }

    /**
     * Returns the parsed form data for the given form
     * 
     * @param \Psr\Http\Message\ServerRequestInterface $response
     * @return \Oroboros\core\interfaces\library\form\FormDataInterface
     * @throws \Oroboros\core\exception\OutOfBoundsException
     */
    public function unpackFormResponse(\Psr\Http\Message\ServerRequestInterface $response): \Oroboros\core\interfaces\library\form\FormDataInterface
    {
        throw new \Oroboros\core\exception\OutOfBoundsException(
                sprintf('Error encountered in [%1$s]. '
                    . 'Provided response does not match the '
                    . 'criteria of this form.', get_class($this))
        );
        return $this->getFormParser()->fetch();
    }

    /**
     * Override this method to declare keys that should be passed from arguments 
     * into the context for the template, if they are provided.
     * @return array
     */
    protected function declareValidContextKeys(): array
    {
        return array_merge(parent::declareValidContextKeys(), $this->form_context_keys);
    }

    /**
     * Declares the context filters used to handle form context
     * 
     * @return array
     */
    protected function declareContextFilters(): array
    {
        return array_merge(parent::declareContextFilters(), $this->context_form_filters);
    }

    /**
     * Verifies that the given form method is a valid http form protocol
     * 
     * @param string $method
     * @return string|null
     * @throws \Oroboros\core\exception\InvalidArgumentException
     *         only `null`, `get` or `post` are valid
     */
    protected function filterMethod(string $method = null): ?string
    {
        $valid = ['get', 'post'];
        if (is_null($method)) {
            return $method;
        }
        if (!in_array(strtolower($method), $valid, true)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Provided method [%2$s] '
                        . 'is not a valid form method. Valid methods are [%2$s], case-insensitive.'
                        , get_class($this), $method, implode(', ', $valid))
            );
        }
        return strtolower($method);
    }

    /**
     * Validates the form action context
     * 
     * @param string $action
     * @return string|null
     */
    protected function filterAction(string $action = null): ?string
    {
        if (is_null($action)) {
            // No form action
            return $action;
        }
        if ($action === '#') {
            // Javascript handles this form
            return $action;
        }
        // Verify uri
        return $action;
    }

    /**
     * Validates the form name context
     * 
     * @param string $name
     * @return string|null
     */
    protected function filterName(string $name = null): ?string
    {
        if (is_null($name)) {
            return $name;
        }
        return $name;
    }

    /**
     * Validates the form field context and packages form
     * fieldset components for each set passed
     * 
     * @param iterable $fields
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface|null
     */
    protected function filterFields(iterable $fields = null): ?\Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $results = [];
        if (is_null($fields)) {
            return $fields;
        }
        if (is_array($fields)) {
            $fields = $this->containerize($fields);
        }
        return $this->containerize($results);
    }

    /**
     * Returns the form parser valid for the form represented by the component
     * 
     * @param string $command
     * @return \Oroboros\core\interfaces\library\form\FormParserInterface
     */
    protected function getFormParser(string $command = null): \Oroboros\core\interfaces\library\form\FormParserInterface
    {
        if (is_null($command)) {
            $command = $this->getCommand();
        }
        $expected = \Oroboros\core\interfaces\library\form\FormParserInterface::class;
        $class = static::FORM_PARSER_CLASS;
        return $this->load('library', $class, $command, $this->getArguments(), $this->getFlags());
    }

    /**
     * Override  this function to perform internal transformations 
     * on context added to the component. This method will be called 
     * from the addContext method prior to setting the value into 
     * the context container to pass to the template. The return value
     * of this method will be the value used for the given key.
     * 
     * @note If a filter returns a component or a container,
     *       it will be returned directly.
     *       Arrays will be assumed to need component transformation.
     *       If you are encountering an error for no known valid component
     *       on a custom key, return a container or pre-package it as a component
     *       in your filter method.
     * 
     * @param string $key
     * @param type $value
     * @return mixed
     */
    protected function filterContext(string $key, $value)
    {
        $filters = $this->declareContextFilters();
        $valid = array_keys($filters);
        // Run filter method
        if (array_key_exists($key, $filters) && method_exists($this, $filters[$key])) {
            $method = $filters[$key];
            $value = $this->$method($value);
            return $value;
        }
        return parent::filterContext($key, $value);
    }

    /**
     * Initializes the default form context
     * 
     * @return void
     */
    protected function initializeContext(): void
    {
        parent::initializeContext();
        // Default tagname is "fieldset"
        $this->addContext('tagname', 'form');
        // Default id is the passed command, if any
        $this->addContext('id', $this->getCommand());
        // Default id is the passed command, if any
        $this->addContext('name', $this->getCommand());
        // No assumption of a form action
        $this->addContext('action', null);
        // No assumption of a form method
        $this->addContext('method', null);
        // No assumption of fields
        $this->addContext('fields', null);
    }

    /**
     * Verifies that the form fieldset class declaration is valid
     * 
     * @return void
     * @throws \Oroboros\core\exception\core\InvalidClassException
     */
    private function verifyFormFieldsetClass(): void
    {
        $expected = \Oroboros\core\interfaces\library\component\HtmlFormFieldsetComponentInterface::class;
        if (is_null(self::FORM_FIELDSET_CLASS)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. Expected class constant '
                        . '[%2$s] is not defined. Expected class or stub class name '
                        . 'resolving to an instance of [%3$s]. This class is not '
                        . 'useable in it\'s current state.'
                        , get_class($this), 'FORM_FIELDSET_CLASS', $expected)
            );
        }
        $class = $this->getFullClassName('component', static::FORM_FIELDSET_CLASS);
        if ($class === false || !in_array($expected, class_implements($class))) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. Class constant '
                        . '[%2$s] with value [%3$s] does not resolve to a '
                        . 'class implementing interface [%4$s]. This class is not '
                        . 'useable in it\'s current state.'
                        , get_class($this), 'FORM_FIELDSET_CLASS', static::FORM_FIELDSET_CLASS, $expected)
            );
        }
    }

    /**
     * Verifies that the form parser class declaration is valid
     * 
     * @return void
     * @throws \Oroboros\core\exception\core\InvalidClassException
     */
    private function verifyFormParserClass(): void
    {
        $expected = \Oroboros\core\interfaces\library\form\FormParserInterface::class;
        if (is_null(self::FORM_PARSER_CLASS)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. Expected class constant '
                        . '[%2$s] is not defined. Expected class or stub class name '
                        . 'resolving to an instance of [%3$s]. This class is not '
                        . 'useable in it\'s current state.'
                        , get_class($this), 'FORM_PARSER_CLASS', $expected)
            );
        }
        $class = $this->getFullClassName('library', static::FORM_PARSER_CLASS);
        if ($class === false || !in_array($expected, class_implements($class))) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. Class constant '
                        . '[%2$s] with value [%3$s] does not resolve to a '
                        . 'class implementing interface [%4$s]. This class is not '
                        . 'useable in it\'s current state.'
                        , get_class($this), 'FORM_PARSER_CLASS', static::FORM_PARSER_CLASS, $expected)
            );
        }
    }
}
