<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\component\javascript\oroboros;

/**
 * Abstract AbstractOroborosJs
 * Provides baseline abstraction for generating javascript compatible with oroboros.js
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
abstract class AbstractOroborosJs extends \Oroboros\core\abstracts\library\component\javascript\AbstractJavascriptComponent
{

    const COMPONENT_SCOPE = \Oroboros\core\abstracts\view\AbstractJavascriptView::CLASS_SCOPE;
    const RESPONSE_DIRECTOR_CLASS = 'javascript\\ResponseDirector';
    const JS_CLASS_TYPE = null;

    private static $base_default_context = [
        'closure' => [
            'args' => [
                'core'
            ],
            'dependencies' => [
                'Oroboros'
            ],
        ],
    ];
    private static $default_valid_context_keys = [
        'type',
        'name',
        'classname',
    ];
    private static $default_context_filters = [
        'classname' => 'filterClassName'
    ];

    /**
     * Defines the default context required to construct a valid oroboros.js class
     * @var array
     */
    private $default_context;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        $this->validateJsClassType();
        parent::__construct($command, $arguments, $flags);
    }

    protected function filterClassName(string $classname): string
    {
        $suffix = static::JS_CLASS_TYPE;
        $canonical = sprintf('%1$s-%2$s', $classname, $suffix);
        $formatted = $this->brutecase($canonical);
        return $formatted;
    }

    /**
     * Override this method to declare keys that should be passed from arguments 
     * into the context for the template, if they are provided.
     * @return array
     */
    protected function declareValidContextKeys(): array
    {
        return array_merge(parent::declareValidContextKeys(), self::$default_valid_context_keys);
    }

    /**
     * Override this method to declare filters for given context keys.
     * This should return an associative array, where the key is the context key,
     * and the value is a function to pass the given context value into.
     * The given function must accept a single mixed argument,
     * and return the formatted value.
     * The function must also be either protected or public.
     * @return array
     */
    protected function declareContextFilters(): array
    {
        return array_merge(parent::declareContextFilters(), self::$default_context_filters);
    }

    protected function initializeContext(): void
    {
        parent::initializeContext();
        $this->setupDefaultContext();
        foreach ($this->default_context as $key => $value) {
            $this->addContext($key, $value);
        }
    }

    private function setupDefaultContext()
    {
        $context = self::$base_default_context;
        $context['type'] = static::JS_CLASS_TYPE;
        $context['name'] = $this->getCommand();
        $context['classname'] = $this->getCommand();
        $this->default_context = $context;
    }

    private function validateJsClassType()
    {
        $expected = 'JS_CLASS_TYPE';
        $classname = get_class($this);
        $definer = sprintf('%1$s::%2$s', $classname, $expected);
        if (is_null($definer)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(sprintf('Error encountered in [%1$s]. Class [%2$s] is malformed. Expected class constant [%3$s] was not provided. Requires a valid string designator.', __CLASS__, $classname, $expected));
        }
    }
}
