<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\component\javascript;

/**
 * Abstract Javascript Component
 * Provides baseline abstraction for generating javascript dynamically via components
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
abstract class AbstractJavascriptComponent extends \Oroboros\core\abstracts\library\component\AbstractComponent implements \Oroboros\core\interfaces\library\component\JavascriptComponentInterface
{

    use \Oroboros\core\traits\component\ComponentContextFilterUtility;

    const COMPONENT_SCOPE = \Oroboros\core\abstracts\view\AbstractJavascriptView::CLASS_SCOPE;
    const RESPONSE_DIRECTOR_CLASS = 'javascript\\ResponseDirector';

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
    }

    /**
     * Override this method to declare keys that should be passed from arguments 
     * into the context for the template, if they are provided.
     * @return array
     */
    protected function declareValidContextKeys(): array
    {
        return array_merge(parent::declareValidContextKeys(), [
            "license", // Sets the license header
            "strict", // Sets strict mode for the generated script
            "closure", // Wraps the output in a closure
            "constants", // Sets the constants defined at the beginning of the generated script
            "vars", // Sets the vars defined at the beginning of the generated script
        ]);
    }

    protected function initializeContext(): void
    {
        parent::initializeContext();
        // no assumption that a license is desired
        $this->addContext('license', false);
        // no assumption that a closure is desired
        $this->addContext('closure', false);
        // no assumption that strict mode is desired
        $this->addContext('strict', false);
        // no assumption that constants exist
        $this->addContext('constants', false);
        // no assumption that vars exist
        $this->addContext('vars', false);
    }
}
