<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\component\json;

/**
 * Abstract Json Component
 * Provides baseline abstraction for generating Json dynamically via components
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
abstract class AbstractJsonComponent extends \Oroboros\core\abstracts\library\component\AbstractComponent implements \Oroboros\core\interfaces\library\component\JsonComponentInterface
{
    
    use \Oroboros\core\traits\component\ComponentContextFilterUtility;

    const COMPONENT_SCOPE = \Oroboros\core\abstracts\view\AbstractJsonView::CLASS_SCOPE;
    const RESPONSE_DIRECTOR_CLASS = 'json\\ResponseDirector';
    const COMPONENT_TEMPLATE = 'null';
    
    /**
     * Indicates that json components *should not* inherently validate context keys.
     * This constant can be overridden and changed to `true` to enable this functionality.
     */
    const REQUIRE_VALID_CONTEXT = false;
    
    /**
     * Renders the component output, and returns it as a string.
     * @return string
     */
    public function render(): string
    {
        $context = [];
        $omit = $this->declareOmittedContext();
        foreach ($this->getContextObject() as $key => $value) {
            if (!in_array($key, $omit)) {
                $context[$key] = $value;
            }
        }
        $output = json_encode($context, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        return $output;
    }
    
    /**
     * Override this method to add component context keys that
     * SHOULD NOT be compiled into the final json output.
     * 
     * Any internal context that shouldn't become final output should
     * have their keys added to this.
     * 
     * @return array
     */
    protected function declareOmittedContext(): array
    {
        return [
            'content',
            'components',
            'debug',
        ];
    }
}
