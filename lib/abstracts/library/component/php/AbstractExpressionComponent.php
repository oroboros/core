<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\component\php;

/**
 * Abstract PHP Expression Component
 * Provides baseline abstraction for generating PHP expressions dynamically via components
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
abstract class AbstractExpressionComponent extends AbstractPhpComponent
{

    use \Oroboros\core\traits\component\php\PhpFunctionComponentUtilityTrait;

    const COMPONENT_TEMPLATE = 'expression/base';
    const COMPONENT_KEY = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
//        d($this); exit;
        parent::__construct($command, $arguments, $flags);
    }
    
    /**
     * Renders the component output, and returns it as a string.
     * @return string
     */
    public function render(): string
    {
        $engine = $this->getTemplateEngine();
        $data = $this->getContextObject();
        $engine->setTemplate($this->getTemplate());
        $engine->setData($data);
        $output = $engine->renderTemplate();
        return $output;
    }

    /**
     * Override this method to declare keys that should be passed from arguments 
     * into the context for the template, if they are provided.
     * @return array
     */
    protected function declareValidContextKeys(): array
    {
        return array_merge(parent::declareValidContextKeys(), [
            "comment",      // Declares a comment explaining what the expression does, if applicable
            "inline",       // Declares whether the expression is inline within another segment of code, or whether it exists on it's own line
            "expression",   // Declares the expression content
        ]);
    }

    /**
     * Override this method to declare filters for given context keys.
     * This should return an associative array, where the key is the context key,
     * and the value is a function to pass the given context value into.
     * The given function must accept a single mixed argument,
     * and return the formatted value.
     * The function must also be either protected or public.
     * @return array
     */
    protected function declareContextFilters(): array
    {
        return array_merge(parent::declareContextFilters(), [
            'expression' => 'filterExpression',
        ]);
    }

    protected function initializeContext(): void
    {
        parent::initializeContext();
        // no assumption that a comment exists
        $this->addContext('comment', false);
        // no assumption that the expression is inline
        $this->addContext('inline', false);
        // by default, the command from the constructor is the expression
        $this->addContext('expression', $this->getCommand());
    }
}
