<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\component\php;

/**
 * Abstract PHP Interface Component
 * Provides baseline abstraction for generating PHP interfaces dynamically via components
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
abstract class AbstractInterfaceComponent extends AbstractPhpComponent
{

    use \Oroboros\core\traits\component\php\PhpClassComponentUtilityTrait;

    const COMPONENT_TEMPLATE = 'interface/base';
    const COMPONENT_KEY = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
    }

    /**
     * Override this method to declare keys that should be passed from arguments 
     * into the context for the template, if they are provided.
     * @return array
     */
    protected function declareValidContextKeys(): array
    {
        return array_merge(parent::declareValidContextKeys(), [
            "name", // Declares name of the interface
            "docblock", // Declares docblock comment preceding the interface
            "interfaces", // Declares interfaces extended by the interface
            "constants", // Declares the class constants defined within the interface
            "methods", // Declares the methods for the interface
        ]);
    }

    /**
     * Override this method to declare filters for given context keys.
     * This should return an associative array, where the key is the context key,
     * and the value is a function to pass the given context value into.
     * The given function must accept a single mixed argument,
     * and return the formatted value.
     * The function must also be either protected or public.
     * @return array
     */
    protected function declareContextFilters(): array
    {
        return array_merge(parent::declareContextFilters(), [
            'interfaces' => 'filterInterfaces',
            'docblock' => 'filterDocblock',
            'constants' => 'filterConstants',
            'methods' => 'filterMethods',
        ]);
    }

    /**
     * Insures interface visibility declarations are only public, as per valid php syntax.
     * @param string $visibility
     * @return string
     * @throws \Oroboros\core\exception\InvalidArgumentException if the given value is not one of public, protected, or private
     */
    protected function filterVisibility($visibility): string
    {
        if (!is_string($visibility)) {
            $visibility = 'public';
        }
        if ($visibility !== 'public') {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Interface visibility may only be public.', get_class($this))
            );
        }
        return $visibility;
    }

    protected function preflightMethodVisibility(string $key, array $method): array
    {
        if (!array_key_exists('visibility', $method)) {
            $method['visibility'] = 'public';
        }
        if ($method['visibility'] !== 'public') {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Supplied interface method [%2$s] cannot have a non-public visibility.', get_class($this), $key)
            );
        }
        return $method;
    }

    /**
     * Returns the stub name of the method component to use for generating method components
     * @return string
     */
    protected function getMethodComponentName(): string
    {
        return 'php\\InterfaceMethodComponent';
    }

    protected function initializeContext(): void
    {
        parent::initializeContext();
        // no assumption that interfaces are included
        $this->addContext('interfaces', false);
        // no assumption that constants are included
        $this->addContext('constants', false);
        // no assumption that methods are included
        $this->addContext('methods', false);
    }
}
