<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\component\cli;

/**
 * Abstract Shell Console
 * Provides a set of methods for providing
 * an interactive command line shell console.
 *
 * @author Brian Dayhoff
 */
abstract class AbstractShellConsole extends AbstractCliComponent implements \Oroboros\core\interfaces\library\cli\ShellConsoleInterface
{

    use \Oroboros\core\traits\cli\CliProcessUtilityTrait;
    use \Oroboros\core\traits\cli\CliFilesystemUtilityTrait;
    use \Oroboros\core\traits\cli\CliOutputUtilityTrait;
    use \Oroboros\core\traits\cli\CliSystemUtilityTrait;
    use \Oroboros\core\traits\pattern\ObservableTrait;
    use \Oroboros\core\traits\pattern\ObserverTrait;
    use \Oroboros\core\traits\StringUtilityTrait;

    const CLASS_SCOPE = 'console';

    /**
     * Defines the component scope as cli
     */
    const COMPONENT_SCOPE = \Oroboros\core\abstracts\view\AbstractCliView::CLASS_SCOPE;

    /**
     * The default command class, if not supplied
     */
    const CONSOLE_COMMAND_CLASS = 'cli\\ConsoleCommand';

    /**
     * The default cli adapter, if not supplied
     */
    const DEFAULT_ADAPTER = 'cli\\ShellAdapter';

    private static $cli_initialized = false;
    private static $cli_default_setup_methods = [
        'initializePid',
        'initializeUid',
        'initializeGid',
        'initializeInode',
        'initializeOs',
        'initializeOsVersion',
        'initializeCwd',
        'initializeCurrentCliUser',
        'initializeIOStreams',
        'initializeObservable',
        'initializeObserver',
    ];
    private $cli_setup_methods = null;
    private $delimiter = ';';
    private $prefix = ' > ';
    private $prefix_color = 'reset';
    private $buffer_color = 'reset';
    private $is_open = false;
    private $buffer = '';
    private $error = null;
    private $adapter = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeCliConsoleSetup();
    }

    public function open(): \Oroboros\core\interfaces\library\cli\ShellConsoleInterface
    {
        $this->is_open = true;
        while ($this->is_open === true) {
            $this->handleLineEntry($this->prompt());
        }
        return $this;
    }

    public function close(): \Oroboros\core\interfaces\library\cli\ShellConsoleInterface
    {
        $this->is_open = false;
        $this->output('Terminal closed' . PHP_EOL, $this->prefix_color, true);
        return $this;
    }

    public function output(string $output, string $color = 'reset', $skip_indent = false): \Oroboros\core\interfaces\library\cli\ShellConsoleInterface
    {
        if (!$skip_indent) {
            $output = $this::indent($output, strlen($this->prefix) - 1);
        }
        $this->verifyFormatKey($color);
        $msg_code = $this->getFormatCode($color);
        $reset_code = $this->getFormatCode('reset');
        $parameter_code = $this->getFormatCode('reset');
        $this->renderCliMessage(self::$stdout, $output, $msg_code, $reset_code, $parameter_code);
        return $this;
    }

    public function setHeader(string $header = null, string $color = 'reset'): \Oroboros\core\interfaces\library\cli\ShellConsoleInterface
    {
        if (is_null($header)) {
            $header = sprintf('%1$s%5$s%2$s %3$s%5$s%4$s'
                , $this::app()->getApplicationDefinition()->get('name')
                , $this->getOs()
                , $this->getOsVersion()
                , sprintf('Use [%2$s%1$s%3$s] to exit the console.', 'exit' . $this->delimiter, $msg_code = $this->getFormatCode('yellow'), $reset_code = $this->getFormatCode($color))
                , PHP_EOL);
        }
        $this->clear_screen();
        $this->output($header . PHP_EOL, $color, true);
        return $this;
    }

    public function getInput(): string
    {
        return trim($this->buffer);
    }

    public function getCommand(): \Oroboros\core\interfaces\library\cli\ShellCommandInterface
    {
        $command = $this->load('library', static::CONSOLE_COMMAND_CLASS, $this->getInput(), $this->getArguments(), $this->getFlags());
        $command->setSubject($this->getAdapter());
        return $command;
    }

    public function getError(): ?\Exception
    {
        return $this->error;
    }

    public function setDelimiter(string $delimiter): \Oroboros\core\interfaces\library\cli\ShellConsoleInterface
    {
        $this->delimiter = $delimiter;
        return $this;
    }

    public function setPrefix(string $prefix): \Oroboros\core\interfaces\library\cli\ShellConsoleInterface
    {
        $this->prefix = $prefix;
        return $this;
    }

    public function setPrefixFormat(string $format): \Oroboros\core\interfaces\library\cli\ShellConsoleInterface
    {
        $this->verifyFormatKey($format);
        $this->prefix_color = $format;
        return $this;
    }

    public function setInputFormat(string $format): \Oroboros\core\interfaces\library\cli\ShellConsoleInterface
    {
        $this->verifyFormatKey($format);
        $this->buffer_color = $format;
        return $this;
    }

    public function handleOutput(\Oroboros\core\interfaces\adapter\cli\ShellAdapterInterface $adapter): void
    {
        $this->print_raw($this::indent($adapter->getOutput(), strlen($this->prefix) - 1));
    }

    public function handleError(\Oroboros\core\interfaces\adapter\cli\ShellAdapterInterface $adapter): void
    {
        $this->print_raw($this::indent($adapter->getOutput(), strlen($this->prefix) - 1), 'stderr');
    }

    public function handleSuccess(\Oroboros\core\interfaces\adapter\cli\ShellAdapterInterface $adapter): void
    {
        $msg = $this::indent('Operation successful.', strlen($this->prefix) - 1);
        $this->success_message($msg);
    }

    public function handleFailure(\Oroboros\core\interfaces\adapter\cli\ShellAdapterInterface $adapter): void
    {
        $msg = $this::indent(sprintf('Operation failed with status code [%1$s]', $adapter->getCode()), strlen($this->prefix) - 1);
        $this->error_message($msg);
    }

    public function handleLineEntry(string $line): \Oroboros\core\interfaces\library\cli\ShellConsoleInterface
    {
        if (strpos($line, $this->delimiter) !== false) {
            $cmd = substr($line, 0, strpos($line, $this->delimiter));
            $line = substr($line, strpos($line, $this->delimiter));
            $this->buffer = trim(sprintf('%1$s%3$s%2$s', $this->buffer, $cmd, PHP_EOL));
            try {
                // Close the console if the exit command is passed
                if ((string) $this->buffer === 'exit') {
                    $this->close();
                    exit(0);
                }
                // Otherwise process the command
                $this->setObserverEvent('command');
            } catch (\Exception $e) {
                $this->error = $e;
                $this->setObserverEvent('error');
            }
            $this->error = null;
            $this->buffer = '';
        } else {
            $this->buffer = sprintf('%1$s%3$s%2$s', $this->buffer, $line, PHP_EOL);
        }
        return $this;
    }

    protected function prompt(): string
    {
        $prefix = $this->format_message($this->prefix, $this->prefix_color);
        $input = $this->read_input($prefix);
        return $input;
    }

    protected function getAdapter(): \Oroboros\core\interfaces\adapter\cli\ShellAdapterInterface
    {
        return $this->adapter;
    }

    protected function loadAdapter(string $id, string $adapter = null, $arguments = [], $flags = []): \Oroboros\core\interfaces\adapter\cli\ShellAdapterInterface
    {
        if (is_null($adapter)) {
            $adapter = static::DEFAULT_ADAPTER;
        }
        $object = $this->load('adapter', $adapter, $id, $this->getAdapterArguments($arguments), $this->getAdapterFlags($flags));
        $object->attach($this);
        return $object;
    }

    protected function getAdapterArguments(array $arguments = null): ?array
    {
        return $arguments;
    }

    protected function getAdapterFlags(array $flags = null): ?array
    {
        return $flags;
    }

    protected function declareObservableName(): string
    {
        return trim(sprintf('%1$s:%2$s', static::CLASS_TYPE, static::CLASS_SCOPE), ':');
    }

    protected function declareObserverEventBindings(): array
    {
        return [
            'output' => [$this, 'handleOutput'],
            'error' => [$this, 'handleError'],
            'success' => [$this, 'handleSuccess'],
            'failure' => [$this, 'handleFailure']
        ];
    }

    protected function declareCliSetupMethods(): array
    {
        return self::$cli_default_setup_methods;
    }

    private function initializeCliConsoleSetup(): void
    {
        if (!self::$cli_initialized) {
            self::$cli_initialized = true;
        }
        $this->cli_setup_methods = $this->declareCliSetupMethods();
        foreach ($this->cli_setup_methods as $method) {
            $this->$method();
        }
        $this->syncPath();
        $this->adapter = $this->loadAdapter(static::CLASS_SCOPE, static::DEFAULT_ADAPTER, $this->getAdapterArguments($this->getArguments()), $this->getAdapterFlags($this->getFlags()));
    }
}
