<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\component\cli;

/**
 * Abstract Html Component Builder
 * Provides abstraction for building html components
 *
 * @author Brian Dayhoff
 */
abstract class AbstractComponentBuilder extends \Oroboros\core\abstracts\library\component\AbstractComponentBuilder
{

    const CLASS_SCOPE = \Oroboros\core\abstracts\view\AbstractCliView::CLASS_SCOPE;
    const WORKER_SCOPE = \Oroboros\core\abstracts\view\AbstractCliView::CLASS_SCOPE;

    public function executeTask(array $arguments = [], array $flags = [])
    {
        $components = $this->containerizeInto('Oroboros\\core\\library\\container\\ResponseCollection');
        foreach (array_keys($this->getCategories()->toArray()) as $category) {
            $this->packageComponents($category, $this->get($category)->toArray(), $components);
        }
        return $components;
    }

    protected function packageComponents(string $category, array $components, \Oroboros\core\interfaces\library\container\CollectionInterface $existing)
    {
        $full_details = $this->getComponentDetails();
        if (!$existing->has($category)) {
            $existing[$category] = $this::containerize();
        }
        $details = $full_details->get($category);
        foreach ($components as $key => $value) {
            if (!property_exists($value, 'id')) {
                $value['id'] = $key;
            }
            if ($existing[$category]->has($key)) {
                // Do not double-build components
                continue;
            }
            $class = $details['object'];
            unset($details['object']);
            $component = $this->load('library', $class, null, ['context' => $value]);
            $existing[$category][$key] = $component;
        }
    }
}
