<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\component;

/**
 * Abstract Component Builder
 * Provides baseline abstraction for building components.
 * This needs to be extended based on the output scope (http, html, cli, etc)
 *
 * @author Brian Dayhoff
 */
abstract class AbstractComponentBuilder extends \Oroboros\core\abstracts\pattern\builder\AbstractBuilder implements \Oroboros\core\interfaces\library\LibraryInterface
{

    const WORKER_TASK = 'component';

    /**
     * Use the default response container
     */
    const CONTAINER_CLASS = 'Oroboros\\core\\library\\container\\ResponseContainer';

    private static $components = null;
    private static $components_instances = null;
    private static $component_defaults = [
        "parent" => false,
        "slug" => null,
        "name" => null,
        "template" => null,
        "object" => null,
        "styles" => [],
        "scripts" => [],
        "fonts" => [],
        "callbacks" => []
    ];
    private $scope_components = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeComponents();
    }

    protected function declareCategories(): array
    {
        $valid = [];
        $config = $this->load('library', 'parser\\JsonParser', $this::CONFIG_ROOT . 'components.json')
            ->fetch();
        if ($config->has(static::WORKER_SCOPE)) {
            foreach ($config->get(static::WORKER_SCOPE) as $key => $value) {
                $valid[$key] = [
                    'array',
                    'Oroboros\\core\\interfaces\\library\\container\\ContainerInterface',
                    'Oroboros\\core\\interfaces\\library\\component\\ComponentInterface'
                ];
            }
        }
        return $valid;
    }

    public function registerComponent(string $scope, string $key, array $details)
    {
        if (!self::$components->has($scope)) {
            self::$components[$scope] = $this->containerizeInto('Oroboros\\core\\library\\container\\Collection');
        }
        if (self::$components[$scope]->has($key)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Provided component with key [%2$s] is already registered in component scope [%3$s].', get_class($this), $key, $scope));
        }
        $definition = array_merge(self::$component_defaults, $details);
        $class = $this->getFullClassName('component', $definition['object']);
        if (!$class) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Component with key [%2$s] in component scope [%4$s] could not '
                        . 'resolve given object parameter [%3$s] to a valid class name.',
                        get_class($this), $key, $definition['object'], $scope));
        }
        $definition['template'] = $class::COMPONENT_TEMPLATE;
        if ($key !== $class::COMPONENT_KEY) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Component key mismatch for component [%2$s]. Provided key [%3$s] '
                        . 'does not match component class key [%4$s]. '
                        . 'Please update your configuration.',
                        get_class($this), $class, $key, $class::COMPONENT_KEY));
        }
        self::$components[$scope][$key] = $this->containerizeInto('Oroboros\\core\\library\\container\\Container', $definition);
        if ($scope === static::WORKER_SCOPE) {
            $this->scope_components[$key] = self::$components[$scope][$key];
        }
    }

    public function addComponentInstance(string $type, \Oroboros\core\interfaces\library\component\ComponentInterface $component)
    {
        if (!self::$components->has($type)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Unknown component scope [%2$s] while evaluating component [%3$s].', get_class($this), $type, get_class($component)));
        }
        $identifier = $component->getKey();
        if (!self::$components[$type]->has($identifier)) {
//            d(self::$components[$type], static::CLASS_SCOPE);
//            exit;
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Component of type [%2$s] with identifier [%3$s] of class [%4$s] '
                        . 'is not registered as a known instance.', get_class($this), $type, $identifier, get_class($component)));
        }
        if (!self::$components_instances[$type]->has($identifier)) {
            self::$components_instances[$type][$identifier] = $this->containerizeInto('Oroboros\\core\\library\\container\\Container');
        }
        self::$components_instances[$type][$identifier][count(self::$components_instances[$type][$identifier])] = $component;
    }

    public function getComponentDefinitions()
    {
        return self::$components;
    }

    public function getComponents(string $type = null, string $identifier = null)
    {
        if (is_null($type)) {
            return self::$components_instances;
        }
        if (!self::$components_instances->has($type)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Unknown component scope [%2$s]', get_class($this), $type));
        }
        if (is_null($identifier)) {
            return self::$components_instances[$type];
        }
        if (!self::$components_instances[$type]->has($identifier)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Component of type [%2$s] with identifier [%3$s] of class [%4$s] '
                        . 'is not registered as a known instance.', get_class($this), $type, $identifier, get_class($component)));
        }
        return self::$components_instances[$type][$identifier];
    }

    protected function getComponentDetails()
    {
        return $this->scope_components;
    }

    protected function declareComponents(): array
    {
        $components = $this::app()->getComponents();
        if ($this->hasArgument('extensions')) {
            foreach ($this->getArgument('extensions') as $key => $extension) {
                $components = $this->updatePluggableComponents($extension, $components);
            }
        }
        if ($this->hasArgument('services')) {
            foreach ($this->getArgument('services') as $key => $service) {
                $components = $this->updatePluggableComponents($service, $components);
            }
        }
        if ($this->hasArgument('modules')) {
            foreach ($this->getArgument('modules') as $key => $module) {
                $components = $this->updatePluggableComponents($module, $components);
            }
        }
        return $components;
    }

    /**
     * Parses the set of all known component details, and locally assigns
     * the set corresponding to the class constant WORKER_SCOPE
     * @return void
     */
    private function initializeComponents(): void
    {
        $this->scope_components = $this->containerizeInto('Oroboros\\core\\library\\container\\Container');
        if (is_null(self::$components)) {
            self::$components = $this->containerizeInto('Oroboros\\core\\library\\container\\Collection');
            foreach ($this->declareComponents() as $type => $components) {
                foreach ($components as $key => $component) {
                    try {
                        $this->registerComponent($type, $key, $component);
                    } catch (\Oroboros\core\exception\InvalidArgumentException $e) {
                        // expected
                    }
                }
            }
        }
        if (is_null(self::$components_instances)) {
            self::$components_instances = $this->containerizeInto('Oroboros\\core\\library\\container\\Collection');
            foreach ($this->declareComponents() as $key => $value) {
                self::$components_instances[$key] = $this->containerizeInto('Oroboros\\core\\library\\container\\Container', []);
                foreach ($value as $identifier => $details) {
                    self::$components_instances[$key][$identifier] = $this->containerizeInto('Oroboros\\core\\library\\container\\Container', []);
                }
            }
        }
        if (self::$components->has(static::WORKER_SCOPE)) {
            $this->scope_components = self::$components->get(static::WORKER_SCOPE);
        }
    }

    /**
     * Imports component declarations from a pluggable
     * @param \Oroboros\core\interfaces\pluggable\PluggableInterface $pluggable
     */
    private function updatePluggableComponents(\Oroboros\core\interfaces\pluggable\PluggableInterface $pluggable, array $existing): array
    {
        $this->updatePluggableDefinitions($pluggable);
        $set = $pluggable->components();
        if (array_key_exists(static::CLASS_SCOPE, $set)) {
            foreach ($set[static::CLASS_SCOPE] as $id => $add) {
                $existing[static::CLASS_SCOPE][$id] = $add;
            }
        }
        return $existing;
    }

    /**
     * Imports any pluggable-declared components to the underlying registry if they do not already exist.
     * 
     * @param \Oroboros\core\interfaces\pluggable\PluggableInterface $pluggable
     * @return void
     */
    private function updatePluggableDefinitions(\Oroboros\core\interfaces\pluggable\PluggableInterface $pluggable): void
    {
        if ($pluggable instanceof \Oroboros\core\interfaces\library\component\ComponentInterface) {
            // Do not register individual standalone components. They must be provided by a package.
            return;
        }
        $set = $pluggable->components();
        foreach ($set as $type => $components) {
            foreach ($components as $id => $add) {
                try {
                    $this->registerComponent($type, $id, $add);
                } catch (\Oroboros\core\exception\InvalidArgumentException $e) {
                    // Expected. Double registration attempts will throw this.
                    continue;
                }
            }
        }
    }
}
