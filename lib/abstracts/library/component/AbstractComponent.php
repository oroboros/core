<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\component;

/**
 * Abstract Component
 * Provides abstraction for representing elements of a page 
 * as encapsulated component objects.
 *
 * @author Brian Dayhoff
 */
abstract class AbstractComponent extends \Oroboros\core\abstracts\library\AbstractLibrary implements \Oroboros\core\interfaces\library\component\ComponentInterface
{

    use \Oroboros\core\traits\StringUtilityTrait;
    use \Oroboros\core\traits\pattern\ResponseDirectorUtilityTrait;
    use \Oroboros\core\traits\library\resolver\ResolverUtilityTrait;

    /**
     * Defines further child classes as components
     */
    const CLASS_SCOPE = 'component';

    /**
     * Component scope must be defined in child classes
     * It should correspond to an output type
     */
    const COMPONENT_SCOPE = null;

    /**
     * Template must be defined in child classes
     */
    const COMPONENT_TEMPLATE = null;

    /**
     * Component key must be defined in child classes
     * This is the default key provider. Internals
     * will check for the key using the `getKey` method.
     * If a non-static key is required, that method should be
     * overridden to provide it.
     */
    const COMPONENT_KEY = null;

    /**
     * Child classes must define a response builder that will be used to collect
     * output data and package it for the view correctly.
     */
    const RESPONSE_DIRECTOR_CLASS = null;
    
    /**
     * Indicates that json components *should not* inherently validate context keys.
     * This constant can be overridden and changed to `false` to disable this functionality.
     */
    const REQUIRE_VALID_CONTEXT = true;

    /**
     * An array of valid component scopes.
     * Component scopes may only correspond with known view class scopes.
     * @var array
     */
    private static $valid_component_scopes = [
        \Oroboros\core\abstracts\view\AbstractCliView::CLASS_SCOPE,
        \Oroboros\core\abstracts\view\AbstractCssView::CLASS_SCOPE,
        \Oroboros\core\abstracts\view\AbstractHtmlView::CLASS_SCOPE,
        \Oroboros\core\abstracts\view\AbstractJavascriptView::CLASS_SCOPE,
        \Oroboros\core\abstracts\view\AbstractJsonView::CLASS_SCOPE,
        \Oroboros\core\abstracts\view\AbstractPlaintextView::CLASS_SCOPE,
        \Oroboros\core\abstracts\view\AbstractXmlView::CLASS_SCOPE,
        \Oroboros\core\abstracts\view\AbstractPhpView::CLASS_SCOPE,
    ];

    /**
     * The templating engine used to render the component.
     * @var \Oroboros\core\interfaces\library\template\TemplateInterface
     */
    private static $template_engine = null;

    /**
     * The name of the template associated with the component
     * @var string
     */
    private $template = null;

    /**
     * Data values required by the component to serve with the page
     * to frontend scripts
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $data = null;

    /**
     * Context passed to the template on render
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $context = null;

    /**
     * Any child components contained within the component
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $children = null;

    /**
     * The parent component, if any exists.
     * @var Oroboros\core\interfaces\library\component\ComponentInterface
     */
    private $parent = null;

    /**
     * Designates whether the component is correctly initialized
     * @var bool
     */
    private $initialized = false;

    /**
     * The key name of the component instance.
     * @var string
     */
    private $component_key = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        if (is_null($command)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Provided parameter [%2$s] must be a valid string.', get_class($this), 'command'));
        }
        if (is_null($arguments)) {
            $arguments = [];
        }
        if (is_null($flags)) {
            $flags = [];
        }
        $expected = 'Oroboros\\core\\interfaces\\pattern\\builder\\BuilderInterface';
        if (!array_key_exists('builder', $arguments) || !is_object($arguments['builder']) || !in_array($expected, class_implements($arguments['builder']))) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Expected instance of [%2$s] for argument key [%3$s]', get_class($this), $expected, 'builder'));
        }
        if (!is_null(static::COMPONENT_KEY)) {
            $this->setKey(static::COMPONENT_KEY);
        }
        $this->setBuilder($arguments['builder']);
        $this->setKey($command);
        $this->verifyComponentScope();
        $this->verifyComponentTemplate();
        $this->verifyTemplateEngine();
        $this->verifyResponseDirector();
        parent::__construct($command, $arguments, $flags);
        $this->initializeData();
        $this->initializeTemplate();
        $this->initializeResponseBuilder();
        $this->initializeContext();
        $this->initializeArgumentContext();
        $this->initializeChildren();
        $this->initialized = true;
    }

    /**
     * Dependency Injection method for the template engine.
     * @param \Oroboros\core\interfaces\library\template\TemplateInterface $template_engine
     * return void
     * @todo remove static assignment
     */
    public static function setTemplateEngine(\Oroboros\core\interfaces\library\template\TemplateInterface $template_engine): void
    {
        self::$template_engine = $template_engine;
    }

    public function getIdentifier(): ?string
    {
        return $this->getCommand();
    }

    /**
     * Dependency injection method for the builder object associated
     * with collecting data from the component
     * @param \Oroboros\core\interfaces\pattern\builder\BuilderInterface $builder
     * @return $this (method chainable)
     */
    public function setBuilder(\Oroboros\core\interfaces\pattern\builder\BuilderInterface $builder): \Oroboros\core\interfaces\library\component\ComponentInterface
    {
        $this->builder = $builder;
        $this->builder->addComponentInstance(static::COMPONENT_SCOPE, $this);
        return $this;
    }

    /**
     * Returns the associated builder object
     * @return \Oroboros\core\interfaces\pattern\builder\BuilderInterface
     */
    public function getBuilder(): \Oroboros\core\interfaces\pattern\builder\BuilderInterface
    {
        return $this->builder;
    }

    /**
     * Sets the key name for the component.
     * @param string $key
     * @return $this (method chainable)
     */
    public function setKey(string $key): \Oroboros\core\interfaces\library\component\ComponentInterface
    {
        $this->component_key = $key;
        return $this;
    }

    /**
     * Returns the key name of the component instance
     * @return string
     */
    public function getKey(): string
    {
        return $this->component_key;
    }

    /**
     * String casting method
     * @return string
     * @note PHP will crash with a fatal error if an exception occurs inside
     *     a `__toString` method. For this reason, a safe empty string will
     *     be returned by default, and the exception message and stacktrace
     *     will be returned if debug mode is enabled, so the error can be
     *     detected on the page content in a development environment safely.
     * @todo Exceptions here need to log when Psr-3 is implemented, as they
     *     cannot otherwise be safely detected.
     */
    public function __toString(): string
    {
        $msg = '';
        try {
            if (!$this->initialized) {
                throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. '
                            . 'Could not render component [%2$s] because it is not initialized.', get_class($this), $this->getKey()));
            }
            $msg = $this->render();
        } catch (\Exception $e) {
            // This key should always be defined by the environment defaults.
            if ($this::app()->debugEnabled()) {
                // a log entry should go here when Psr-3 is implemented
                $msg = $e->getMessage() . PHP_EOL . $e->getTraceAsString();
            }
        }
        return $msg;
    }

    /**
     * Renders the component output, and returns it as a string.
     * @return string
     */
    public function render(): string
    {
        $this->context['component_key'] = $this->getKey();
        $this->context['component_class'] = get_class($this);
        self::$template_engine->setTemplate($this->template);
        self::$template_engine->setData($this->context);
        $output = self::$template_engine->renderTemplate();
        return $output;
    }

    /**
     * Adds a child component
     * @param \Oroboros\core\interfaces\library\component\ComponentInterface $child
     * @return $this (method chainable)
     */
    public function addChild(\Oroboros\core\interfaces\library\component\ComponentInterface $child): \Oroboros\core\interfaces\library\component\ComponentInterface
    {
        if ($child === $this) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Cannot set a component as a child of itself.', get_class($this)));
        }
        if ($this->hasChild($child->getKey())) {
            unset($this->children[$child->getKey()]);
        }
        $this->children[$child->getKey()] = $child;
        if ($child->getParent() !== $this) {
            $child->setParent($this);
        }
        $this->context['components'][$child->getKey()] = $child;
        return $this;
    }

    /**
     * Returns whether a child exists or not
     * @param string $key
     * @return bool
     */
    public function hasChild(string $key): bool
    {
        if (is_null($this->children)) {
            $this->initializeChildren();
        }
        return $this->children->has($key);
    }

    /**
     * Removes a child component if it exists
     * @param string $key
     * @return $this (method chainable)
     */
    public function removeChild(string $key): \Oroboros\core\interfaces\library\component\ComponentInterface
    {
        if ($this->hasChild($key)) {
            if ($this->children->get($key)->getParent() === $this) {
                $this->children->get($key)->removeParent();
            }
            unset($this->children[$key]);
            unset($this->context['components'][$key]);
        }
        return $this;
    }

    /**
     * Returns the child associated with the given key, or null if it does not exist.
     * @param string $key
     * @return \Oroboros\core\interfaces\library\component\ComponentInterface|null
     */
    public function getChild(string $key): ?\Oroboros\core\interfaces\library\component\ComponentInterface
    {
        if (!$this->hasChild($key)) {
            return null;
        }
        return $this->children->get($key);
    }

    /**
     * Sets the component parent instance.
     * The component will set itself as a child of the parent when this occurs.
     * @param \Oroboros\core\interfaces\library\component\ComponentInterface $parent
     * @return \Oroboros\core\interfaces\library\component\ComponentInterface
     */
    public function setParent(\Oroboros\core\interfaces\library\component\ComponentInterface $parent): \Oroboros\core\interfaces\library\component\ComponentInterface
    {
        if ($parent === $this) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Cannot set a component as a parent of itself.', get_Class($this)));
        }
        $this->parent = $parent;
        if (!($this->parent->hasChild($this->getKey()) && $this->parent->getChild($this->getKey()) === $this)) {
            $this->parent->addChild($this);
        }
        return $this;
    }

    /**
     * Removes the parent/child association if any exists.
     * The current instance will also be removed as a child
     * of the parent when this occurs.
     * @return $this (method chainable)
     */
    public function removeParent(): \Oroboros\core\interfaces\library\component\ComponentInterface
    {
        if (!$this->parent === null) {
            // Remove self as a child of the parent.
            if ($this->parent->hasChild($this->getKey() && $this->parent->getChild() === $this)) {
                $this->parent->removeChild($this->getKey());
            }
            $this->parent = null;
        }
        return $this;
    }

    /**
     * Returns the parent component instance, or null if no parent exists.
     * @return \Oroboros\core\interfaces\library\component\ComponentInterface|null
     */
    public function getParent(): ?\Oroboros\core\interfaces\library\component\ComponentInterface
    {
        return $this->parent;
    }

    /**
     * Returns a boolean determination as to whether or not the component has a parent.
     * @return bool
     */
    public function hasParent(): bool
    {
        return is_null($this->parent);
    }

    /**
     * Returns an array of keys that are considered valid context for this component
     * @return array
     */
    public function getContextValidKeys(): array
    {
        return $this->declareValidContextKeys();
    }

    /**
     * Returns a boolean designation as to whether a given context key is valid
     * 
     * if class constant `REQUIRE_VALID_CONTEXT` is false,
     * this will allow any keys to be set.
     * 
     * if it has any other value, it will use the default functionality.
     * 
     * @return array
     */
    public function isValidContext(string $key): bool
    {
        if (static::REQUIRE_VALID_CONTEXT === false) {
            return true;
        }
        return (in_array($key, $this->declareValidContextKeys(), true));
    }

    /**
     * Adds a content key to the component
     * @param string $key
     * @param mixed $value
     * @return $this (method chainable)
     */
    public function addContent(string $key, $value): \Oroboros\core\interfaces\library\component\ComponentInterface
    {
        $content = $this->getContext('content');
        $content[$key] = $value;
        $this->addContext('content', $content);
        return $this;
    }

    /**
     * Adds context, which translates into variables
     * made available to the template.
     * @param string $key
     * @param type $value
     * @return $this (method chainable)
     * @throws \InvalidArgumentException if an unrecognized context key is passed
     */
    public function addContext(string $key, $value): \Oroboros\core\interfaces\library\component\ComponentInterface
    {
        if (!$this->isValidContext($key)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Provided context key [%2$s] is not valid for component [%3$s]. Valid context keys are [%4$s].', get_class($this), $key, static::COMPONENT_KEY, implode(', ', $this->declareValidContextKeys())));
        }
        $this->context[$key] = $this->filterContext($key, $value);
        return $this;
    }

    /**
     * Returns whether a given context key exists
     * @param string $key
     * @return bool
     */
    public function hasContext(string $key): bool
    {
        return $this->context->has($key);
    }

    /**
     * Fetches a given context key, if it exists
     * @param string $key
     * @return mixed
     */
    public function getContext(string $key)
    {
        if (!$this->hasContext($key)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Requested context [%2$s] does not exist in this scope. Valid contexts are [%3$s].', get_class($this), $key, implode(', ', array_keys($this->context->toArray()))));
        }
        return $this->context[$key];
    }

    /**
     * Removes a given context key, if it exists
     * @param string $key
     * @return $this (method chainable)
     */
    public function removeContext(string $key): \Oroboros\core\interfaces\library\component\ComponentInterface
    {
        if ($this->context->has($key)) {
            unset($this->context[$key]);
        }
        return $this;
    }

    /**
     * Adds a data section to the component
     * @param string $key
     * @param array $value
     * @return $this (method chainable)
     */
    public function addData(string $key, array $value): \Oroboros\core\interfaces\library\component\ComponentInterface
    {
        $data = $this->containerize($value);
        $this->data[$key] = $data;
        return $this;
    }

    /**
     * Removes a given data key if it exists
     * @param string $key
     * @return $this (method chainable)
     */
    public function removeData(string $key): \Oroboros\core\interfaces\library\component\ComponentInterface
    {
        if ($this->hasData($key)) {
            unset($this->data[$key]);
        }
        return $this;
    }

    /**
     * Returns whether a given data key exists
     * @param string $key
     * @return bool
     */
    public function hasData(string $key): bool
    {
        return $this->data->has($key);
    }

    /**
     * 
     * @param string $key
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     * @throws \InvalidArgumentException If the provided data key does not exist.
     */
    public function getData(string $key = null): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        if (is_null($key)) {
            return $this->data;
        }
        if (!$this->hasData($key)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Provided data key [%2$s] does not exist.', get_class($this), $key));
        }
        return $this->data->get($key);
    }

    /**
     * Returns the currently scoped template engine.
     * 
     * @return \Oroboros\core\interfaces\library\template\TemplateInterface|null
     */
    protected function getTemplateEngine(): ?\Oroboros\core\interfaces\library\template\TemplateInterface
    {
        return self::$template_engine;
    }

    /**
     * Returns the currently scoped template.
     * 
     * @return string
     */
    protected function getTemplate(): string
    {
        return $this->template;
    }

    /**
     * Overrides the current template with an arbitrarily designated one.
     * 
     * @return string
     */
    protected function setTemplate(string $template): void
    {
        $this->template = $template;
    }

    /**
     * Internal getter for the full context object
     * 
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    protected function getContextObject(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        return $this->context;
    }

    /**
     * Override  this function to perform internal transformations 
     * on context added to the component. This method will be called 
     * from the addContext method prior to setting the value into 
     * the context container to pass to the template. The return value
     * of this method will be the value used for the given key.
     * @param string $key
     * @param type $value
     * @return mixed
     */
    protected function filterContext(string $key, $value)
    {
        return $value;
    }

    /**
     * Override this method to declare keys that should be passed from arguments 
     * into the context for the template, if they are provided.
     * @return array
     */
    protected function declareValidContextKeys(): array
    {
        return [
            "content",
            "components"
        ];
    }

    /**
     * Override to provide default data values.
     * Must return an associative array of data key identifiers
     * for data values to compile.
     * @return array
     */
    protected function getDefaultData(): array
    {
        return [];
    }

    protected function getChildComponents(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        return $this->children;
    }

    protected function initializeTemplate(): void
    {
        $template = static::COMPONENT_SCOPE . DIRECTORY_SEPARATOR . static::COMPONENT_TEMPLATE;
        $this->template = $template;
    }

    protected function initializeChildren(): void
    {
        if (is_null($this->children)) {
            $this->children = $this::containerize([]);
        }
        if ($this->hasArgument('children')) {
            $context = $this->getArgument('children');
            if (!is_iterable($context)) {
                throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                            . 'Provided argument [%2$s] must be iterable.', get_class($this), 'children'));
            }
            $expected = 'Oroboros\\core\\interfaces\\library\\component\\ComponentInterface';
            foreach ($context as $key => $value) {

                if (!is_object($value) || !in_array($expected, class_implements($expected))) {
                    throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                                . 'Provided argument [%2$s] must be an object implementing [%3$s].', get_class($this), 'children:' . $key, $expected));
                }
                $this->addChild($value);
            }
        }
    }

    /**
     * Prepares the underlying context container
     * @return void
     */
    protected function initializeContext(): void
    {
        if (is_null($this->context)) {
            $this->context = $this::containerize([]);
        }
        if (!$this->hasContext('content')) {
            $this->addContext('content', $this::containerize());
        }
        if (!$this->hasContext('components')) {
            $this->addContext('components', $this::containerize());
        }
    }

    /**
     * Initializes any context passed in via the constructor
     * @return void
     */
    protected function initializeArgumentContext(): void
    {
        if ($this->hasArgument('context')) {
            $context = $this->getArgument('context');
            foreach ($context as $key => $value) {
                $this->addContext($key, $value);
            }
        }
    }

    protected function initializeData(): void
    {
        if (is_null($this->data)) {
            $defaults = $this->getDefaultData();
            $this->data = $this::containerize($defaults);
        }
        if ($this->hasArgument('data')) {
            foreach ($this->getArgument('data') as $key => $data) {
                $this->addData($key, $data);
            }
        }
    }

    private function verifyTemplateEngine(): void
    {
        if (is_null(self::$template_engine)) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. '
                        . 'Template engine of type [%2$s] must be set prior to object '
                        . 'instantiation.', get_class($this), 'Oroboros\\core\\interfaces\\library\\template\\TemplateInterface'));
        }
    }

    private function verifyComponentScope(): void
    {
        if (is_null(static::COMPONENT_SCOPE) || !in_array(static::COMPONENT_SCOPE, self::$valid_component_scopes)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(sprintf('Component class [%1$s] is invalid. '
                        . 'Class constant [%2$s] must be defined with a valid component scope. '
                        . 'Recieved [%3$s]. Expected [%4$s].', get_class($this), 'COMPONENT_SCOPE', (is_null(static::COMPONENT_SCOPE) ? 'NULL' : static::COMPONENT_SCOPE), implode(', ', self::$valid_component_scopes)));
        }
    }

    private function verifyComponentTemplate(): void
    {
        if (is_null(static::COMPONENT_TEMPLATE)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(sprintf('Component class [%1$s] is invalid. '
                        . 'Class constant [%2$s] must be defined with a valid template location.', get_class($this), 'COMPONENT_TEMPLATE'));
        }
    }
}
