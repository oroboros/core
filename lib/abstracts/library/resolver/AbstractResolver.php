<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\resolver;

/**
 * Abstract Resolver
 * Provides abstraction for a resolver class.
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
abstract class AbstractResolver extends \Oroboros\core\abstracts\library\AbstractLibrary implements \Oroboros\core\interfaces\library\resolver\ResolverInterface
{

    /**
     * Factorizable class types that can have stub names
     * @var array
     */
    private static $class_types = [
        'adapter',
        'component',
        'controller',
        'extension',
        'factory',
        'layout',
        'library',
        'model',
        'module',
        'view',
    ];

    /**
     * Literal types that `gettype` resolves to, or
     * common shorthand for some of them.
     * @var array
     */
    private static $literal_types = [
        'string',
        'bool',
        'boolean',
        'integer',
        'double',
        'array',
        'object',
        'resource',
        'null',
        'NULL'
    ];

    /**
     * Standard constuctor
     * 
     * @param string $command
     * @param array $arguments
     * @param array $flags
     */
    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
    }

    /**
     * Returns a boolean designation as to whether the given
     * subject resolves against the supplied definitions.
     * 
     * @param type $subject the argument to evaluate
     * @param type $allowed the valid options
     * @return bool
     */
    public function resolve($subject, $allowed): bool
    {
        // Unpack containers
        if (is_object($allowed) && $allowed instanceof \Oroboros\core\interfaces\library\container\ContainerInterface) {
            $allowed = $allowed->toArray();
        }
        // if a given allowed value is a constant or class constant, resolve it before proceeding
        if ($this->isDefined($allowed)) {
            $allowed = $this->getDefinedValue($allowed);
        }
        // if a given subject value is a constant or class constant, resolve it before proceeding
        if ($this->isDefined($subject)) {
            $subject = $this->getDefinedValue($subject);
        }
        if (is_array($allowed)) {
            return $this->resolveArrayDefinition($allowed, $subject);
        }
        if (is_string($allowed)) {
            return $this->resolveStringDefinition($allowed, $subject);
        }
        if (is_null($allowed) || is_bool($allowed) || is_float($allowed) || is_int($allowed)) {
            return $this->resolveLiteralDefinition($allowed, $subject);
        }
        return false;
    }

    /**
     * Resolves all cases of string comparison
     * 
     * @param string $allowed
     * @param type $subject
     * @return bool
     */
    protected function resolveStringDefinition(string $allowed, $subject): bool
    {
        // If the allowed value is defined, evaluate against the defined value
        if ($this->isDefined($allowed)) {
            $allowed = $this->getDefinedValue($allowed);
        }
        // If the subject value is defined, evaluate against the defined value
        if ($this->isDefined($subject)) {
            $subject = $this->getDefinedValue($subject);
        }
        // If the allowed value is a variable type, resolve subject against the variable type
        if ($this->isType($allowed)) {
            return $this->resolveType($allowed, $subject);
        }
        // If the allowed value is an interface, resolve subject against the interface
        if ($this->isInterface($allowed)) {
            return $this->resolveInterface($allowed, $subject);
        }
        // If the allowed value is a class or stub class, resolve subject against the classname
        if ($this->isClass($allowed)) {
            return $this->resolveClass($allowed, $subject);
        }
        // Resolve all others literally
        return $this->resolveLiteralDefinition($allowed, $subject);
    }

    protected function resolveArrayDefinition(array $allowed, $subject): bool
    {
        foreach ($allowed as $evaluate) {
            if (is_object($evaluate) && $evaluate instanceof \Oroboros\core\interfaces\library\container\ContainerInterface) {
                $evaluate = $evaluate->toArray();
            }
            // Expand falsy values to strict bool
            if ($this->isFalsy($evaluate)) {
                $evaluate = $this->getFalsy($evaluate);
            }
            // Expand definitions, do not alter definitions that are falsy.
            // classes should declare strict values in constants, 
            // so a zero is a literal zero and a 1 is a literal 1
            if ($this->isDefined($evaluate)) {
                $evaluate = $this->getDefinedValue($evaluate);
            }
            // Defer string definitions to the string resolver
            if (is_string($evaluate) && $this->resolveStringDefinition($evaluate, $subject)) {
                return true;
            }
            // Resolve literals
            if ($this->resolveLiteralDefinition($evaluate, $subject))
            {
                return true;
            }
        }
        return false;
    }

    /**
     * Compares literal values
     * returns true if exact match, false otherwise.
     * 
     * @param type $allowed
     * @param type $subject
     * @return bool
     */
    protected function resolveLiteralDefinition($allowed, $subject): bool
    {
        return $subject === $allowed;
    }

    /**
     * Checks if a subject can be resolved with `gettype` against the allowed value,
     * and resolves common aliases of `gettype` return values.
     * (eg bool instead of boolean, null instead of NULL, etc)
     * 
     * @note this will NOT return true when comparing 0 to false or 1 to true.
     * 
     * @param string $allowed
     * @param type $subject
     * @return bool
     */
    protected function resolveType(string $allowed, $subject): bool
    {
        if (strtolower($allowed) === 'null') {
            return is_null($subject);
        }
        if (in_array($allowed, ['bool', 'boolean'])) {
            return is_bool($subject);
        }
        return $allowed === gettype($subject);
    }

    /**
     * Checks if a given subject is an object with the same class name
     * or an instance of the given allowed classname, or if it is a string
     * that resolves to a classname that is the same or an instance of the
     * given allowed classname.
     * 
     * @param string $allowed
     * @param type $subject
     * @return bool
     */
    protected function resolveClass(string $allowed, $subject): bool
    {
        $allowed = $this->getClassName($allowed);
        // Non class strings are not valid, don't bother with other checks
        if (is_string($subject) && !$this->isClass($subject)) {
            return false;
        }
        // Standardize the classname if it is a class or stub class
        if (is_string($subject) && $this->isClass($subject)) {
            $subject = $this->getClassName($subject);
        }
        // direct match, object
        if (is_object($subject) && get_class($subject) === $allowed) {
            return true;
        }
        // subclass of, object
        if (is_object($subject) && $subject instanceof $allowed) {
            return true;
        }
        // direct match, string
        if (is_string($subject) && $subject === $allowed) {
            return true;
        }
        // subclass of, string
        if (is_string($subject) && is_subclass_of($subject, $allowed)) {
            return true;
        }
        // all others false
        return false;
    }

    /**
     * Checks if a given subject is either an object implementing the given interface,
     * or if it is a string representing a class or stub class implementing the given interface.
     * Returns true if any of these apply, false otherwise.
     * 
     * @param string $allowed
     * @param mixed $subject
     * @return bool
     */
    protected function resolveInterface(string $allowed, $subject): bool
    {
        $allowed = $this->getInterfaceName($allowed);
        if (is_object($subject) && $subject instanceof $allowed) {
            return true;
        }
        if (is_string($subject) && $this->isClass($subject)) {
            $subject = $this->getClassName($subject);
            $interfaces = class_implements($subject);
            if ($interfaces === false) {
                return false;
            }
            return in_array($allowed, $interfaces, true);
        }
        return false;
    }

    /**
     * Checks if a given subject is a constant, class constant,
     * or array/container that has a class or interface name and
     * a constant name that can resolve to a defined class constant.
     * 
     * @param type $subject
     * @return bool
     */
    private function isDefined($subject): bool
    {
        if (is_string($subject)) {
            return defined($subject);
        }
        if (is_object($subject) && $subject instanceof \Oroboros\core\interfaces\library\container\ContainerInterface) {
            $subject = $subject->toArray();
        }
        if (is_object($subject) || is_null($subject) || is_int($subject) || is_bool($subject)) {
            return false;
        }
        if (is_array($subject) && count($subject) === 2) {
            $class = array_shift($subject);
            $const = array_shift($subject);
            if (!is_string($class)) {
                return false;
            }
            if (!is_string($const)) {
                return false;
            }
            if (!$this->isClass($class) && !$this->isInterface($class)) {
                return false;
            }
            $dec = sprintf('%1$s::%2$s', $class, $const);
            return defined($dec);
        }

        return false;
    }

    /**
     * Returns a defined value for a given subject.
     * Expects preflighting with `isDefined` prior to calling this.
     * 
     * @param type $subject
     * @return mixed
     */
    private function getDefinedValue($subject)
    {
        if (is_string($subject)) {
            return constant($subject);
        }
        if (is_object($subject) && $subject instanceof \Oroboros\core\interfaces\library\container\ContainerInterface) {
            $subject = $subject->toArray();
        }
        $class = array_shift($subject);
        $const = array_shift($subject);
        $dec = sprintf('%1$s::%2$s', $class, $const);
        return constant($dec);
    }

    /**
     * Checks if a given string represents a literal type
     * or shorthand for a literal type
     * 
     * @param string $allowed
     * @return bool
     */
    private function isType(string $allowed): bool
    {
        return in_array($allowed, self::$literal_types);
    }

    /**
     * Checks if a given string is a class or a stub name of a class
     * 
     * @param string $allowed
     * @return bool
     */
    private function isClass(string $allowed): bool
    {
        if (class_exists($allowed)) {
            return true;
        }
        foreach (self::$class_types as $type) {
            if ($this->getFullClassName($type, $allowed) !== false) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if a given variable is falsy (eg true, false, 0, or 1)
     * 
     * @param mixed $var
     * @return bool
     */
    private function isFalsy($var): bool
    {
        return in_array($var, [true, false, 0, 1], true);
    }

    /**
     * Standardizes falsy values to boolean
     * 
     * @param type $var
     * @return bool
     */
    private function getFalsy($var): bool
    {
        if ($var) {
            return true;
        }
        return false;
    }

    /**
     * Checks if a given string is an interface
     * 
     * @param string $allowed
     * @return bool
     */
    private function isInterface(string $allowed): bool
    {
        return interface_exists($allowed);
    }

    /**
     * Cleans preceding slashes from interface names and returns the
     * system call compatible representation.
     * 
     * This corrects issues where names with preceding backslashes will
     * resolve true when a call to `interface_exists` is made, but will
     * resolve false when comparing via `class_implements` without removing
     * the preceding backslash.
     * 
     * @param string $stub
     * @return string
     */
    private function getInterfaceName(string $stub): string
    {
        $stub = ltrim($stub, '\\');
        if (interface_exists($stub)) {
            return $stub;
        }
        return $stub;
    }

    /**
     * Cleans up the classname, removes preceding backslashes,
     * and expands the name if it is a stub class.
     * 
     * @param string $stub
     * @return string
     */
    private function getClassName(string $stub): string
    {
        $stub = ltrim($stub, '\\');
        if (class_exists($stub)) {
            return $stub;
        }
        foreach (self::$class_types as $type) {
            if ($this->getFullClassName($type, $stub) !== false) {
                return $this->getFullClassName($type, $stub);
            }
        }
        return $stub;
    }
}
