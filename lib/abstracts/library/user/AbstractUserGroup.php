<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\user;

/**
 * Abstract User Group
 * Provides abstraction for user-based identity groups
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
abstract class AbstractUserGroup extends \Oroboros\core\abstracts\library\auth\AbstractIdentityGroup implements \Oroboros\core\interfaces\library\user\UserGroupInterface
{

    /**
     * Declares the auth identity type as user group
     */
    const AUTH_IDENTITY_TYPE = 'user-group';

    /**
     * Declares the entity type as identity group
     */
    const ENTITY_TYPE = self::AUTH_IDENTITY_TYPE;

    /**
     * Declares the entity scope as user
     */
    const ENTITY_SCOPE = AbstractUserIdentity::ENTITY_TYPE;

    /**
     * Match the user identity type
     */
    const IDENTITY_GROUP_TYPE = AbstractUserIdentity::IDENTITY_TYPE;

    /**
     * Match the user identity scope
     */
    const IDENTITY_GROUP_SCOPE = AbstractUserIdentity::IDENTITY_SCOPE;

    /**
     * Match the user identity category
     */
    const IDENTITY_GROUP_CATEGORY = AbstractUserIdentity::IDENTITY_CATEGORY;

    /**
     * Match the user identity subcategory
     */
    const IDENTITY_GROUP_SUBCATEGORY = AbstractUserIdentity::IDENTITY_SUBCATEGORY;

    /**
     * Match the user identity context
     */
    const IDENTITY_GROUP_CONTEXT = AbstractUserIdentity::IDENTITY_CONTEXT;

    /**
     * Match the user identity prefix
     */
    const IDENTITY_GROUP_PREFIX = AbstractUserIdentity::IDENTITY_PREFIX;

    /**
     * Match the user identity suffix
     */
    const IDENTITY_GROUP_SUFFIX = AbstractUserIdentity::IDENTITY_SUFFIX;

    /**
     * Match the user identity separator
     */
    const IDENTITY_GROUP_SEPARATOR = AbstractUserIdentity::IDENTITY_SEPARATOR;

}
