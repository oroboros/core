<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\user;

/**
 * Abstract CLI User Identity
 * Provides abstraction for providing identity information for command line users
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
abstract class AbstractCliUserIdentity extends AbstractUserIdentity implements \Oroboros\core\interfaces\library\user\CliUserIdentityInterface
{

    /**
     * Scopes the identity type to user
     */
    const AUTH_IDENTITY_TYPE = 'user';
    
    /**
     * Match the identity type
     */
    const AUTH_SCOPE = self::AUTH_IDENTITY_TYPE;

    /**
     * Matches the auth identity group to the default user group setting.
     */
    const AUTH_IDENTITY_GROUP_TYPE = AbstractUserGroup::AUTH_IDENTITY_TYPE;

    /**
     * Defines the entity type as user
     */
    const ENTITY_TYPE = self::AUTH_IDENTITY_TYPE;
    
    /**
     * Defines the default username used if no username is passed
     * 
     * You may override this constant to return a legitimate default
     * user identifier to use as a default if none exists for the
     * specific identity.
     */
    const DEFAULT_USERNAME = null;
    
    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        if (is_null($command)) {
            // Use the default username if none was passed
            $command = static::DEFAULT_USERNAME;
        }
        parent::__construct($command, $arguments, $flags);
    }
    
    /**
     * CLI users can only authenticate from localhost connections over cli.
     * If both of the above are `true`, will always return `true`.
     * Otherwise returns `false`;
     * 
     * @param \Oroboros\core\interfaces\library\auth\CredentialContainerInterface $credentials
     * @return bool
     */
    public function authenticate(\Oroboros\core\interfaces\library\auth\CredentialContainerInterface $credentials): bool
    {
        if (!$this::user()->isLocalhost()) {
            return false;
        }
        if (!$this::IS_CLI) {
            return false;
        }
        return true;
    }
    
    /**
     * Override this method to perform evaluation of passed credentials.
     * 
     * This method must return `true` if credentials are sufficient
     * to access the identity, and `false` otherwise.
     * 
     * The default behavior always returns `true` if the environment is scoped to cli, and `false` otherwise.
     * 
     * @param \Oroboros\core\interfaces\library\auth\CredentialContainerInterface $credentials
     * @return bool
     */
    protected function evaluateAuthentication(\Oroboros\core\interfaces\library\auth\CredentialContainerInterface $credentials): bool
    {
        if ($this::IS_CLI) {
            // Environment permits authentication
            return true;
        }
        return false;
    }

}
