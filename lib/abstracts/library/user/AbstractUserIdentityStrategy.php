<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\user;

/**
 * Description of AbstractUserIdentityStrategy
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
abstract class AbstractUserIdentityStrategy extends \Oroboros\core\abstracts\library\auth\AbstractIdentityStrategy implements \Oroboros\core\interfaces\library\user\UserIdentityStrategyInterface
{

    use \Oroboros\core\traits\model\ModelAwareUtilityTrait;
    
    const AUTH_IDENTITY_TYPE = 'user';

    /**
     * Scopes the strategy to user identities
     */
    const STRATEGY_TARGET_INTERFACE = \Oroboros\core\interfaces\library\user\UserIdentityInterface::class;

    /**
     * Represents an immutable set of core user identities
     * 
     * @var array
     */
    private static $core_user_identities = [
        \Oroboros\core\library\user\RootUserIdentity::DEFAULT_USERNAME => \Oroboros\core\library\user\RootUserIdentity::class,
        \Oroboros\core\library\user\SystemUserIdentity::DEFAULT_USERNAME => \Oroboros\core\library\user\SystemUserIdentity::class,
        \Oroboros\core\library\user\CoreUserIdentity::DEFAULT_USERNAME => \Oroboros\core\library\user\CoreUserIdentity::class,
        \Oroboros\core\library\user\CliUserIdentity::DEFAULT_USERNAME => \Oroboros\core\library\user\CliUserIdentity::class,
        \Oroboros\core\library\user\ShellUserIdentity::DEFAULT_USERNAME => \Oroboros\core\library\user\ShellUserIdentity::class,
        \Oroboros\core\library\user\SocketServerUserIdentity::DEFAULT_USERNAME => \Oroboros\core\library\user\SocketServerUserIdentity::class,
        \Oroboros\core\library\user\JobQueueUserIdentity::DEFAULT_USERNAME => \Oroboros\core\library\user\JobQueueUserIdentity::class,
        \Oroboros\core\library\user\DefaultUserIdentity::DEFAULT_USERNAME => \Oroboros\core\library\user\DefaultUserIdentity::class,
        \Oroboros\core\library\user\BlacklistUserIdentity::DEFAULT_USERNAME => \Oroboros\core\library\user\BlacklistUserIdentity::class,
        \Oroboros\core\library\user\GhostlistUserIdentity::DEFAULT_USERNAME => \Oroboros\core\library\user\GhostlistUserIdentity::class,
        \Oroboros\core\library\user\RegisteredUserIdentity::DEFAULT_USERNAME => \Oroboros\core\library\user\RegisteredUserIdentity::class,
        \Oroboros\core\library\user\StaffUserIdentity::DEFAULT_USERNAME => \Oroboros\core\library\user\StaffUserIdentity::class,
        \Oroboros\core\library\user\ModeratorUserIdentity::DEFAULT_USERNAME => \Oroboros\core\library\user\ModeratorUserIdentity::class,
        \Oroboros\core\library\user\AdminUserIdentity::DEFAULT_USERNAME => \Oroboros\core\library\user\AdminUserIdentity::class,
    ];

    /**
     * Represents an immutable set of default user identities
     * 
     * @var array
     */
    private static $default_user_identities = [
        \Oroboros\core\library\user\RootUserIdentity::DEFAULT_USERNAME => \Oroboros\core\library\user\RootUserIdentity::class,
        \Oroboros\core\library\user\SystemUserIdentity::DEFAULT_USERNAME => \Oroboros\core\library\user\SystemUserIdentity::class,
        \Oroboros\core\library\user\CoreUserIdentity::DEFAULT_USERNAME => \Oroboros\core\library\user\CoreUserIdentity::class,
        \Oroboros\core\library\user\CliUserIdentity::DEFAULT_USERNAME => \Oroboros\core\library\user\CliUserIdentity::class,
        \Oroboros\core\library\user\ShellUserIdentity::DEFAULT_USERNAME => \Oroboros\core\library\user\ShellUserIdentity::class,
        \Oroboros\core\library\user\SocketServerUserIdentity::DEFAULT_USERNAME => \Oroboros\core\library\user\SocketServerUserIdentity::class,
        \Oroboros\core\library\user\JobQueueUserIdentity::DEFAULT_USERNAME => \Oroboros\core\library\user\JobQueueUserIdentity::class,
        \Oroboros\core\library\user\DefaultUserIdentity::DEFAULT_USERNAME => \Oroboros\core\library\user\DefaultUserIdentity::class,
        \Oroboros\core\library\user\BlacklistUserIdentity::DEFAULT_USERNAME => \Oroboros\core\library\user\BlacklistUserIdentity::class,
        \Oroboros\core\library\user\GhostlistUserIdentity::DEFAULT_USERNAME => \Oroboros\core\library\user\GhostlistUserIdentity::class,
        \Oroboros\core\library\user\RegisteredUserIdentity::DEFAULT_USERNAME => \Oroboros\core\library\user\RegisteredUserIdentity::class,
        \Oroboros\core\library\user\StaffUserIdentity::DEFAULT_USERNAME => \Oroboros\core\library\user\StaffUserIdentity::class,
        \Oroboros\core\library\user\ModeratorUserIdentity::DEFAULT_USERNAME => \Oroboros\core\library\user\ModeratorUserIdentity::class,
        \Oroboros\core\library\user\AdminUserIdentity::DEFAULT_USERNAME => \Oroboros\core\library\user\AdminUserIdentity::class,
    ];

    /**
     * Represents the registered valid user identities
     * 
     * @var array
     */
    private static $user_identities = null;

    /**
     * Returns an instance of the given identity type
     * 
     * @param string $type
     * @param string $command
     * @param array $arguments
     * @param array $flags
     * @return \Oroboros\core\interfaces\library\user\UserIdentityInterface
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    public function getIdentity(string $type, string $command = null, array $arguments = null, array $flags = null): \Oroboros\core\interfaces\library\user\UserIdentityInterface
    {
        $this->initializeUserIdentities();
        if (!array_key_exists($type, self::$user_identities)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Provided user identity key '
                        . '[%2$s] is not known. Valid keys are [%3$s]'
                        , get_class($this), $type, implode(', ', array_keys(self::$user_identities)))
            );
        }
        try {
            $identity = $this->getModel(static::AUTH_IDENTITY_TYPE, null, $this->getArguments(), $this->getFlags())->fetch()->get($command);
        } catch (\Psr\Container\NotFoundExceptionInterface $e) {
            $identity = $this->load('library', self::$user_identities[$type], $command, $arguments, $flags);
        }
        return $identity;
    }

    /**
     * Override this method to determine if the current user is blacklisted.
     * 
     * If this returns true, the user will be scoped to the blacklist user
     * and the application will refuse to serve their request.
     * 
     * The default behavior always returns false.
     * 
     * @return bool
     */
    protected function checkBlacklist(): bool
    {
        return false;
    }

    /**
     * Override this method to determine if the current user is flagged as a ghost.
     * 
     * If this returns true, the user will be scoped to the ghost identity
     * and none of their input will be trusted.
     * 
     * The default behavior always returns false.
     * 
     * @return bool
     */
    protected function checkGhostlist(): bool
    {
        return false;
    }

    /**
     * Override this method to determine if the current user is flagged as a root process.
     * 
     * If this returns true, the user will be scoped to the root user.
     * 
     * The default behavior always returns false.
     * 
     * @return bool
     */
    protected function checkRoot(): bool
    {
        return false;
    }

    /**
     * Override this method to determine if the current user is flagged as a core process.
     * 
     * If this returns true, the user will be scoped to the core user.
     * 
     * The default behavior always returns false.
     * 
     * @return bool
     */
    protected function checkCore(): bool
    {
        return false;
    }

    /**
     * Override this method to determine if the current user is flagged as a system process.
     * 
     * If this returns true, the user will be scoped to the system user.
     * 
     * The default behavior always returns false.
     * 
     * @return bool
     */
    protected function checkSystem(): bool
    {
        return false;
    }

    /**
     * Override this method to determine if the current user is flagged as a websocket server.
     * 
     * If this returns true, the user will be scoped to the websocket task runner user.
     * 
     * The default behavior always returns false.
     * 
     * @return bool
     */
    protected function checkWebsocket(): bool
    {
        return false;
    }

    /**
     * Override this method to determine if the current user is flagged as a job process.
     * 
     * If this returns true, the user will be scoped to the job queue task runner user.
     * 
     * The default behavior always returns false.
     * 
     * @return bool
     */
    protected function checkJob(): bool
    {
        return false;
    }

    /**
     * Override this method to determine if the current user is an admin user.
     * 
     * If this returns true, the user will be scoped to the admin user.
     * 
     * The default behavior always returns false.
     * 
     * @return bool
     */
    protected function checkAdmin(): bool
    {
        return false;
    }

    /**
     * Override this method to determine if the current user is a moderator user.
     * 
     * If this returns true, the user will be scoped to the moderator user.
     * 
     * The default behavior always returns false.
     * 
     * @return bool
     */
    protected function checkModerator(): bool
    {
        return false;
    }

    /**
     * Override this method to determine if the current user is a staff user.
     * 
     * If this returns true, the user will be scoped to the staff user.
     * 
     * The default behavior always returns false.
     * 
     * @return bool
     */
    protected function checkStaff(): bool
    {
        return false;
    }

    /**
     * Override this method to determine if the current user is a default user.
     * 
     * If this returns true, the user will be scoped to the default user.
     * 
     * The default behavior always returns false.
     * 
     * @return bool
     */
    protected function checkDefault(): bool
    {
        return false;
    }

    /**
     * Initializes the user identity list if not already done
     * 
     * @return void
     */
    private static function initializeUserIdentities(): void
    {
        if (!is_null(self::$user_identities)) {
            // Already done
            return;
        }
        self::$user_identities = [];
        foreach (self::$default_user_identities as $key => $value) {
            self::$user_identities[$key] = $value;
        }
        if (static::app()->getApplicationRoot() === OROBOROS_CORE_BASE_DIRECTORY) {
            foreach (self::$core_user_identities as $key => $value) {
                self::$user_identities[$key] = $value;
            }
        }
    }
}
