<?php
/*
 * The MIT License
 *
 * Copyright 2017 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\user;

/**
 * Abstract User
 * Provides abstraction for user objects
 *
 * @author Brian Dayhoff
 */
abstract class AbstractUser extends \Oroboros\core\abstracts\library\entity\AbstractPersonEntity implements \Oroboros\core\interfaces\library\user\UserInterface
{

    use \Oroboros\core\traits\library\resolver\ResolverUtilityTrait;

    /**
     * -------------------------------------------------------------------------
     * |                            Entity Declarations                        |
     * -------------------------------------------------------------------------
     */

    /**
     * Declares the class scope as user
     */
    const CLASS_SCOPE = AbstractUserIdentity::AUTH_IDENTITY_TYPE;

    /**
     * Declares the entity type as user
     */
    const ENTITY_TYPE = AbstractUserIdentity::AUTH_IDENTITY_TYPE;

    /**
     * Defines the entity scope as current user
     */
    const ENTITY_SCOPE = 'current-user';

    /**
     * -------------------------------------------------------------------------
     * |                            Auth Declarations                          |
     * -------------------------------------------------------------------------
     */

    /**
     * Declares the auth token class as the default auth token
     */
    const AUTH_TOKEN_CLASS = \Oroboros\core\library\auth\Token::class;

    /**
     * Declares the session class to use as the default user session
     */
    const SESSION_CLASS = \Oroboros\core\library\session\http\HttpSession::class;

    /**
     * -------------------------------------------------------------------------
     * |                            Flag Declarations                          |
     * -------------------------------------------------------------------------
     */

    /**
     * Defines the default user flag.
     * If this flag is passed as true and no username is provided,
     * the default user will be instantiated.
     */
    const FLAG_DEFAULT_USER = 'default-user';

    /**
     * Defines the default user flag.
     * If this flag is passed as true and no username is provided,
     * the default user will be instantiated.
     */
    const FLAG_GET_REQUEST_ORGANIZATION = 'use-request-org';

    /**
     * Defines the flag to scope the user as current.
     * If this flag is passed as true, the user details
     * will be persisted into the current session.
     * If the user is not valid, an error exception will be raised.
     */
    const FLAG_SCOPE_AS_CURRENT = 'scope-current';

    /**
     * -------------------------------------------------------------------------
     * |                            Identity Declarations                      |
     * -------------------------------------------------------------------------
     */

    /**
     * Defines the default username to use if no username is provided,
     * and the default user flag is passed.
     */
    const DEFAULT_IDENTITY = \Oroboros\core\library\user\DefaultUserIdentity::DEFAULT_USERNAME;

    /**
     * Defines the default username to use if no username is provided,
     * and the default user flag is passed in a cli environment.
     */
    const DEFAULT_CLI_IDENTITY = \Oroboros\core\library\user\CliUserIdentity::DEFAULT_USERNAME;

    /**
     * Defines the blacklist username to use if a blacklisted request is received,
     * and the default user flag is passed.
     */
    const BLACKLIST_IDENTITY = \Oroboros\core\library\user\BlacklistUserIdentity::DEFAULT_USERNAME;

    /**
     * Defines the ghost username to use if a ghost flagged request is received,
     * and the default user flag is passed.
     */
    const GHOSTLIST_IDENTITY = \Oroboros\core\library\user\GhostlistUserIdentity::DEFAULT_USERNAME;

    /**
     * Defines the default user group to use if no user group is found for the user,
     * and the default user flag is passed.
     */
    const DEFAULT_USERGROUP = 'public';

    /**
     * Defines the default organization to use if no organization is found for the user,
     * and the default user flag is passed.
     */
    const DEFAULT_ORGANIZATION = 'public';

    /**
     * -------------------------------------------------------------------------
     * |                            Old Garbage. Refactor                      |
     * -------------------------------------------------------------------------
     */

    /**
     * Defines the default details record for the user for inserting a new user row.
     * This will be merged with any credentials applied if a new row is generated.
     * @var array
     */
    private static $default_details = [
        'user' => self::DEFAULT_IDENTITY,
        'group' => self::DEFAULT_USERGROUP,
        'organization' => self::DEFAULT_ORGANIZATION,
        'password' => 'NO_PASSWORD_DEFINED', // This password will never validate. The user will need to reset their password, or an administrator will need to do it for them.
        'enabled' => 0,
        'verified' => 0,
        'internal' => 0
    ];

    /**
     * Defines the default identity record for inserting a new identity row.
     * This will be merged with any credentials applied if a new row is generated.
     * @var array
     */
    private static $default_identity = [
        'user' => self::DEFAULT_IDENTITY,
        'organization' => self::DEFAULT_ORGANIZATION,
        'email' => null,
        'salutation' => null,
        'first_name' => null,
        'middle_name' => null,
        'last_name' => null,
        'title' => null,
        'birthdate' => null,
        'phone_country_code' => null,
        'phone_area_code' => null,
        'phone_number' => null,
        'phone_extension' => null,
        'addr_1' => null,
        'addr_2' => null,
        'addr_city' => null,
        'addr_state' => null,
        'addr_country' => null,
        'addr_zip' => null,
        'addr_zip_ext' => null
    ];

    /**
     * -------------------------------------------------------------------------
     * |                            Internal Properties                        |
     * -------------------------------------------------------------------------
     */

    /**
     * The user identity strategy used to establish which user identity
     * class to scope to.
     * 
     * @var \Oroboros\core\interfaces\library\user\UserIdentityStrategyInterface
     */
    private static $identity_strategy = null;

    /**
     * -------------------------------------------------------------------------
     * |                            Scope Properties                           |
     * -------------------------------------------------------------------------
     */

    /**
     * Designates whether the currently defined user is a valid user.
     * @var bool
     */
    private $is_valid = false;

    /**
     * Designates whether the currently defined user is authenticated.
     * @var bool
     */
    private $is_authenticated = false;

    /**
     * Designates whether the currently defined user is scoped as the current user.
     * @var bool
     */
    private $is_scoped = false;

    /**
     * Designates whether the currently defined user is currently logged in.
     * @var bool
     */
    private $is_logged_in = false;

    /**
     * -------------------------------------------------------------------------
     * |                            Identity Properties                        |
     * -------------------------------------------------------------------------
     */

    /**
     * A list of all identity details associated with the scoped user,
     * or default values if the user does not exist.
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface 
     */
    private $identity = null;

    /**
     * -------------------------------------------------------------------------
     * |                            Session Properties                         |
     * -------------------------------------------------------------------------
     */

    /**
     * An array of the sessions currently allotted to the user.
     * Only one of these may be active, and only one may be primary.
     * @var array 
     */
    private $sessions = null;

    /**
     * The currently active session control object.
     * @var \Oroboros\core\interfaces\library\session\SessionInterface 
     */
    private $session = null;

    /**
     * The key that designates the users primary session.
     * @var string 
     */
    private $primary_session = null;

    /**
     * -------------------------------------------------------------------------
     * |                            Request Properties                         |
     * -------------------------------------------------------------------------
     */

    /**
     * The request object representing what the client sent
     * 
     * @var \Psr\Http\Message\ServerRequestInterface 
     */
    private static $request = null;

    /**
     * Determines if the current user request originated from localhost
     * 
     * @var bool
     */
    private static $is_localhost = false;

    /**
     * Determines if the current user request is on the whitelist
     * 
     * @var bool
     */
    private static $is_whitelisted = false;

    /**
     * Determines if the current user request is on the blacklist
     * 
     * @var bool
     */
    private static $is_blacklisted = false;

    /**
     * Determines if the current user request is on the ghostlist
     * 
     * @var bool
     */
    private static $is_ghostlisted = false;

    /**
     * Determines if the current user request is from an internal network
     * 
     * @var bool
     */
    private static $is_internal = false;

    /**
     * Determines if the current user request is from a trusted network
     * 
     * @var bool
     */
    private static $is_trusted = false;

    /**
     * Determines if the current user request is from a public network
     * 
     * @var bool
     */
    private static $is_public = false;

    /**
     * Designates whether the currently defined request is secure.
     * Secure requests have valid ssl, or originated from localhost
     * 
     * This can be configured to also allow specific
     * ip addresses or networks to be considered secure.
     * 
     * This alleviates some of the concern for ssl encryption
     * internally in test environments, and lets dev servers
     * and CI pipes work without having to integrate ssl into them.
     * 
     * On the cli side it allows the program to understand that users
     * with shell access always have secure access to the app because
     * they can really just edit source directly at that point anyways.
     * 
     * @var bool
     */
    private static $is_secure = self::IS_SSL;

    /**
     * -------------------------------------------------------------------------
     * |                            Constructor                                |
     * -------------------------------------------------------------------------
     */

    /**
     * Uses the standard constructor
     * @param string $command
     * @param array $arguments
     * @param array $flags
     */
    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        if (!is_null($flags) && array_key_exists(self::FLAG_DEFAULT_USER, $flags)) {
            $command = is_null($command) ? self::DEFAULT_IDENTITY : $command;
        }
        if (!is_null($flags) && array_key_exists(self::FLAG_GET_REQUEST_ORGANIZATION, $flags) && $flags[self::FLAG_GET_REQUEST_ORGANIZATION]) {
            $arguments['organization'] = array_key_exists('organization', $arguments) ? $arguments['organization'] : $this->getCurrentOrganization($arguments['request']);
        }
        parent::__construct($command, $arguments, $flags);
        $this->initializeUserIdentityStrategy();
    }
    /**
     * -------------------------------------------------------------------------
     * |                            Scope Api                                  |
     * -------------------------------------------------------------------------
     */

    /**
     * Returns a boolean determination as to whether
     * a valid identity is defined internally.
     * 
     * @note if no identity object is defined internally, this will always return `false`.
     * @return bool
     */
    public function isValid(): bool
    {
        if (is_null($this->identity)) {
            return false;
        }
        return true;
    }

    /**
     * Returns a boolean determination as to whether the
     * currently defined user is scoped to an identity.
     * 
     * If this returns `true`, internally defined user identity is
     * scoped to the current user the request originated from.
     * 
     * Always returns `false` if the current object is not valid to scope to.
     * 
     * If the current identity requires authentication,
     * it must be authenticated before it can be scoped.
     * 
     * @return bool
     */
    public function isScoped(): bool
    {
        if (!$this->isValid()) {
            // Always false when invalid
            return false;
        }
        return $this->is_scoped;
    }

    /**
     * Returns a boolean determination as to whether the user is currently enabled.
     * 
     * @note if no identity is scoped, this will always return `false`.
     * @return bool
     */
    public function isEnabled(): bool
    {
        if (!$this->isScoped()) {
            // Always false when unscoped
            return false;
        }
        return $this->identity()->isEnabled();
    }

    /**
     * Returns a boolean determination as to whether the user is
     * currently logged into a secure session.
     * 
     * @note if no identity is scoped, this will always return `false`.
     * @note if the scoped identity is not enabled, this will always return `false`.
     * @note if the scoped identity is not authenticated or can't authenticate,
     *       this will always return `false`.
     * @return bool
     */
    public function isLoggedIn(): bool
    {
        if (!$this->isEnabled()) {
            // Always false when unscoped or disabled
            return false;
        }
        return $this->is_logged_in;
    }

    /**
     * Returns a boolean determination as to whether the users account is authenticated.
     * Authenticated users have passed their credential check via their request
     * or session and are allowed to log in.
     * 
     * @note if no identity is scoped, this will always return `false`.
     * @note if the scoped identity is not enabled, this will always return `false`.
     * @note if the scoped identity has not provided valid credentials,
     *       this will always return `false`.
     *       Valid credentials are defined by the identity object.
     * @return bool
     */
    public function isAuthenticated(): bool
    {
        if (!$this->isLoggedIn()) {
            // Always false when unscoped or disabled
            return false;
        }
        return $this->is_authenticated;
    }

    /**
     * Returns a boolean determination as to whether the users account is verified.
     * If verification is enforced, the user must respond to the verification message
     * in order to be able to login fully.
     * 
     * @note if no identity is scoped, this will always return `false`.
     * @note if the scoped identity is not enabled, this will always return `false`.
     * @note if the scoped identity is not authenticated, this will always return `false`.
     * @note if the scoped identity is not logged in, this will always return `false`.
     * @return bool
     */
    public function isVerified(): bool
    {
        if (!$this->isLoggedIn()) {
            // Always false when unscoped, disabled, or logged out
            return false;
        }
        return $this->identity()->details()->security->verified->value();
    }
    /**
     * -------------------------------------------------------------------------
     * |                            Network Api                                |
     * -------------------------------------------------------------------------
     */

    /**
     * Returns a boolean determination as to whether the
     * users request originated from localhost.
     * 
     * This is internally used to flag requests as safe in lieu of ssl
     * or other typical required security measures.
     * 
     * @note if no request object has been parsed, this will always return `false`
     * @return bool
     */
    public function isLocalhost(): bool
    {
        return self::$is_localhost;
    }

    /**
     * Returns a boolean determination as to whether the
     * users request originated is blacklisted.
     * 
     * This is internally used to flag requests as invalid for any purpose.
     * 
     * @note if the user is root, system, core, cli, websocket, job, or shell, this will always return `false`
     * @note if the request originated from localhost, this will always return `false`
     * @note if the request originated from a whitelisted source, this will always return `false`
     * @note if no request object has been parsed, this will always return `false`
     * @return bool
     */
    public function isBlacklisted(): bool
    {
        return self::$is_blacklisted;
    }

    /**
     * Returns a boolean determination as to whether the
     * users request originated is whitelisted.
     * 
     * This is internally used to flag requests as having elevated access rights.
     * 
     * @note if the user is root, system, core, cli, websocket, job, or shell, this will always return `true`
     * @note if the request originated from localhost, this will always return `true`
     * @note if the request originated from a whitelisted source, this will always return `true`
     * @note `false` will be returned in all other cases.
     * @return bool
     */
    public function isWhitelisted(): bool
    {
        return self::$is_whitelisted;
    }

    /**
     * Returns a boolean determination as to whether the
     * users request is ghostlisted.
     * 
     * This is internally used to flag requests as having significantly reduced trust levels.
     * 
     * @note if the request originated from the command line, this will always return `false`
     * @note if the user is root, system, core, cli, websocket, job, or shell, this will always return `false`
     * @note if the request originated from localhost, this will always return `false`
     * @note if the request originated from an internal network, this will always return `false`
     * @note if the request originated from a whitelisted source, this will always return `false`
     * @note if the request is already blacklisted, this will always return `false`
     * @note This will only return `true` if a ghostlist is configured and none of the above apply.
     * @return bool
     */
    public function isGhost(): bool
    {
        return self::$is_ghostlisted;
    }

    /**
     * Returns a boolean determination as to whether the
     * users request originated from a channel that is safe to transmit unencrypted data.
     * 
     * This is internally used to flag requests as having elevated access rights.
     * 
     * @note if the user is root, system, core, cli, websocket, job, or shell, this will always return `true`
     * @note if the request originated from localhost, this will always return `true`
     * @note if the request originated from a whitelisted source, this will always return `true`
     * @note if the request originated from an internal network, this will always return `true`
     * @note if the request originated from cli, this will always return `true`.
     * @note if the request has a valid ssl signing, this will always return `true`.
     * @note `false` will be returned in all other cases.
     * @return bool
     */
    public function isSecure(): bool
    {
        return self::$is_secure;
    }

    /**
     * Returns a boolean determination as to whether the
     * users request originated from a channel that is safe to transmit unencrypted data.
     * 
     * This is internally used to flag requests as having elevated access rights.
     * 
     * @note if the user is root, system, core, cli, websocket, job, or shell, this will always return `true`
     * @note if the request originated from localhost, this will always return `true`
     * @note if the request originated from a whitelisted source, this will always return `true`
     * @note if the request originated from an internal network, this will always return `true`
     * @note if the request originated from cli, this will always return `true`.
     * @note if the request has a valid ssl signing, this will always return `true`.
     * @note `false` will be returned in all other cases.
     * @return bool
     */
    public function isInternal(): bool
    {
        return self::$is_internal;
    }

    /**
     * Returns a boolean determination as to whether the
     * users request originated from a channel that is safe to transmit unencrypted data.
     * 
     * This is internally used to flag requests as having elevated access rights.
     * 
     * @note if the user is root, system, core, cli, websocket, job, or shell, this will always return `true`
     * @note if the request originated from localhost, this will always return `true`
     * @note if the request originated from a whitelisted source, this will always return `true`
     * @note if the request originated from an internal network, this will always return `true`
     * @note if the request originated from cli, this will always return `true`.
     * @note if the request has a valid ssl signing, this will always return `true`.
     * @note `false` will be returned in all other cases.
     * @return bool
     */
    public function isTrusted(): bool
    {
        return self::$is_trusted;
    }

    /**
     * Returns a boolean determination as to whether the
     * users request originated from a channel that is safe to transmit unencrypted data.
     * 
     * This is internally used to flag requests as having elevated access rights.
     * 
     * @note if the user is root, system, core, cli, websocket, job, or shell, this will always return `true`
     * @note if the request originated from localhost, this will always return `true`
     * @note if the request originated from a whitelisted source, this will always return `true`
     * @note if the request originated from an internal network, this will always return `true`
     * @note if the request originated from cli, this will always return `true`.
     * @note if the request has a valid ssl signing, this will always return `true`.
     * @note `false` will be returned in all other cases.
     * @return bool
     */
    public function isPublic(): bool
    {
        return self::$is_public;
    }
    /**
     * -------------------------------------------------------------------------
     * |                            Request Api                                |
     * -------------------------------------------------------------------------
     */

    /**
     * Universal getter for the inbound request.
     * The request is set here in the post-initialization bootstrap step.
     * It may be obtained at any time thereafter via this method.
     * 
     * @return \Psr\Http\Message\ServerRequestInterface
     * @throws \Oroboros\core\exception\ErrorException
     *         If the request is not defined yet.
     */
    public function request(): \Psr\Http\Message\ServerRequestInterface
    {
        return $this->getRequestObject();
    }

    /**
     * Sets the Psr-7 request object representing the current request.
     * This can only be done one time.
     * 
     * The class may internally alter it's request but it
     * cannot externally modified if it is already set.
     * 
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @return void
     * @throws \Oroboros\core\exception\ErrorException
     *         If the request is already set.
     */
    public function setRequest(\Psr\Http\Message\ServerRequestInterface $request): void
    {
        if (!is_null(self::$request)) {
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. '
                        . 'Cannot set the request because '
                        . 'it is already defined.', get_class($this))
            );
        }
        $this->getLogger()->debug('[type][scope][class] User request object set.', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
        ]);
        self::$request = $request;
        $this->evaluateRequest($request);
    }
    /**
     * -------------------------------------------------------------------------
     * |                            Auth Api                                   |
     * -------------------------------------------------------------------------
     */

    /**
     * Returns a boolean determination as to whether the scoped identity
     * has access to a corresponding permission node.
     * 
     * If there is no current scoped identity, this will always return false.
     * 
     * @param string $permission
     * @return bool
     */
    public function can(string $permission): bool
    {
        if (!$this->isScoped()) {
            return false;
        }
        return $this->identity()->can($permission);
    }

    /**
     * Returns the identity groups associated with the scoped identity.
     * 
     * @return \Oroboros\core\interfaces\library\auth\IdentityGroupContainerInterface
     */
    public function groups(): \Oroboros\core\interfaces\library\auth\IdentityGroupContainerInterface
    {
        if (!$this->isValid()) {
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. Cannot obtain identity groups without a scoped identity.', get_class($this))
            );
        }
        return $this->identity()->groups();
    }

    /**
     * Returns the permissions assigned directly to the scoped identity.
     * 
     * @return \Oroboros\core\interfaces\library\auth\PermissionContainerInterface
     */
    public function permissions(): \Oroboros\core\interfaces\library\auth\PermissionContainerInterface
    {
        if (!$this->isValid()) {
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. Cannot obtain permissions without a scoped identity.', get_class($this))
            );
        }
        return $this->identity()->permissions();
    }

    /**
     * Attempts to authenticate against a given identity object.
     * If authentication is successful, the user will re-initialize the session
     * scoped to the given identity after authentication succeeds.
     * 
     * @param string $identity The id of the identity attempting to authenticate as.
     * @param \Oroboros\core\interfaces\library\auth\CredentialContainerInterface $credentials
     *        The credentials to authenticate the user identity against.
     *        This should contain, at minimum, an identity object mapping to
     *        the same id as the given identity, and an unhashed password to
     *        verify the stored password hash against.
     * 
     * @return void
     * @throws \Oroboros\core\exception\auth\AuthenticationException
     */
    public function authenticate(string $identity = null, \Oroboros\core\interfaces\library\auth\CredentialContainerInterface $credentials = null): void
    {
        $identity_object = $this->lookup($identity, $identity, null, $this->getArguments(), $this->getFlags());
        if (is_null($identity)) {
            throw new \Oroboros\core\exception\auth\AuthenticationException(
                    sprintf('Error encountered in [%1$s]. No user valid user '
                        . 'identity exists for user [%2$s].', get_class($this), $identity)
            );
        }
        if (!$identity_object->authenticate($credentials)) {
            throw new \Oroboros\core\exception\auth\AuthenticationException(
                    sprintf('Error encountered in [%1$s]. User identity [%2$s] '
                        . 'Failed to authenticate with the given credentials.'
                        , get_class($this), $identity)
            );
        }
        // Scope the user and re-initialize the session.
        $this->session()->destroy();
        $this->setIdentityObject($identity_object);
        $this->is_authenticated = true;
        $this->set('login-authenticated', true);
        $this->login();
    }
    /**
     * -------------------------------------------------------------------------
     * |                            Session Api                                |
     * -------------------------------------------------------------------------
     */

    /**
     * Scopes the current user to the user object. Does not update login timestamp.
     * If the current user is not valid, this will throw an error exception.
     * 
     * This method should be used when the user is already logged in.
     * 
     * @return $this (method chainable)
     * @throws \ErrorException if the current user reflected by the object is not a valid user.
     */
    public function scope()
    {
        $this->scopeSession();
        return $this;
    }

    /**
     * Respecs to a different current user.
     * 
     * This should generally only be done on login,
     * or on account switching functionality.
     * 
     * @param string $username
     * @param string $organization
     * @return $this (method chainable)
     */
    public function respec(string $username, string $organization)
    {
        $existing_username = $this->username;
        $existing_organization = $this->organization;
        $in_scope = $this->is_scoped;
        try {
            $this->setCommand($username);
            $this->initializeUser();
            if ($in_scope) {
                $this->scope();
            }
        } catch (\Exception $e) {
// Nope, put it back the way it was.
            $this->setCommand($existing_username);
            $this->initializeUser();
            if ($in_scope) {
                $this->scope();
            }
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. '
                        . 'Unable to respec user to provided user [%2$s] '
                        . 'and organization [%3$s]. Reason: [%4$s].'
                        , get_class($this), $username, $organization, $e->getMessage())
                    , $e->getCode(), $e);
        }
        return $this;
    }

    /**
     * Logs the currently scoped user in, updates the last login timestamp,
     * and scopes the session to reflect the scoped user.
     * 
     * This method should be used when the user is not already logged in,
     * but has authenticated successfully.
     * 
     * @return $this (method chainable)
     */
    public function login()
    {
        $this->is_authenticated = true;
        $this->set('login-authenticated', $this->is_logged_in);
        $this->is_logged_in = true;
        $this->set('login-status', $this->is_logged_in);
        return $this;
    }

    /**
     * Logs the currently scoped user out,
     * updates the last logout timestamp,
     * and scopes the session to reflect
     * their primary login session
     * (if currently logged in as a secondary user),
     * or the default user if the previous is not the case.
     * 
     * This method should be used when the user logs out of either
     * their primary or any secondary session.
     * 
     * @return $this (method chainable)
     */
    public function logout()
    {
        $this->respec(static::DEFAULT_IDENTITY, static::DEFAULT_ORGANIZATION);
        return $this;
    }
    /**
     * -------------------------------------------------------------------------
     * |                            Session Data Api                           |
     * -------------------------------------------------------------------------
     */

    /**
     * Gets a session value for the given user.
     * @param string $key
     */
    public function get(string $key)
    {
        return $this->session()->get($key);
    }

    /**
     * Returns a boolean determination as to whether a given session key exists for the user.
     * @param string $key
     * @return bool
     */
    public function has(string $key): bool
    {
        return $this->session()->has($key);
    }

    /**
     * Sets a session value for the given user.
     * @return $this (method chainable)
     */
    public function set(string $key, $value)
    {
        $this->session()->set($key, $value);
        return $this;
    }

    /**
     * Removes a session value for the given user.
     * @return $this (method chainable)
     */
    public function unset(string $key)
    {
        $this->session()->unset($key);
        return $this;
    }

    /**
     * Returns the scoped user session object
     * 
     * @return \Oroboros\core\interfaces\library\session\SessionInterface
     */
    public function session(): \Oroboros\core\interfaces\library\session\SessionInterface
    {
        return $this->getSessionObject();
    }

    /**
     * Returns the auth token issued upon session creation.
     * 
     * @return \Oroboros\core\interfaces\library\auth\TokenInterface
     */
    public function getAuthToken(): \Oroboros\core\interfaces\library\auth\TokenInterface
    {
        return $this->session()->getAuthToken();
    }
    /**
     * -------------------------------------------------------------------------
     * |                            Identity Api                               |
     * -------------------------------------------------------------------------
     */

    /**
     * Looks up a user and returns the identity object
     * for the user, or null if not found.
     * 
     * @param string $identity
     * @param string $organization
     * @param type $type
     * @return \Oroboros\core\interfaces\library\user\UserIdentityInterface|null
     */
    public function lookup(string $identity, string $organization = null, $type = null): ?\Oroboros\core\interfaces\library\user\UserIdentityInterface
    {
        if (is_null($type)) {
            
        }
        if (is_null($organization)) {
            
        }
        try {
            $identity = $this->lookupIdentity($identity, $identity, $this->getArguments(), $this->getFlags());
            return $identity;
        } catch (\Exception $e) {
            
        }
        return null;
    }

    /**
     * Returns the internal id of the scoped identity.
     * 
     * @return string|null
     * @throws \Oroboros\core\exception\ErrorException
     *         If no identity is currently scoped
     */
    public function id(): string
    {
        if (is_null($this->identity)) {
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. '
                        . 'Cannot return the identity name '
                        . 'because no identity is currently '
                        . 'scoped.', get_class($this))
            );
        }
        return $this->identity->id();
    }

    /**
     * The human readable name of the identity, if any
     * 
     * @return string|null
     * @throws \Oroboros\core\exception\ErrorException
     *         If no identity is currently scoped
     */
    public function name(): ?string
    {
        if (is_null($this->identity)) {
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. '
                        . 'Cannot return the identity name '
                        . 'because no identity is currently '
                        . 'scoped.', get_class($this))
            );
        }
        return $this->identity->name();
    }

    /**
     * The human readable description of the identity, if any
     * 
     * @return string|null
     * @throws \Oroboros\core\exception\ErrorException
     *         If no identity is currently scoped
     */
    public function description(): ?string
    {
        if (is_null($this->identity)) {
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. '
                        . 'Cannot return the identity description '
                        . 'because no identity is currently '
                        . 'scoped.', get_class($this))
            );
        }
        return $this->identity->description();
    }

    /**
     * Returns the currently scoped identity
     * 
     * @return \Oroboros\core\interfaces\library\user\UserIdentityInterface
     * @throws \Oroboros\core\exception\ErrorException
     *         If no identity is currently scoped
     */
    public function identity(): \Oroboros\core\interfaces\library\user\UserIdentityInterface
    {
        if (is_null($this->identity)) {
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. '
                        . 'Cannot return the identity '
                        . 'because no identity is currently '
                        . 'scoped.', get_class($this))
            );
        }
        return $this->identity;
    }

    /**
     * Returns the organization of the scoped identity
     * 
     * @return \Oroboros\core\interfaces\library\organization\OrganizationInterface
     * @throws \Oroboros\core\exception\ErrorException
     *         If no identity is currently scoped
     */
    public function organization(): \Oroboros\core\interfaces\library\organization\OrganizationInterface
    {
        if (!$this->isValid()) {
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. Cannot obtain organization without a scoped identity.', get_class($this))
            );
        }
        return $this->identity()->organization();
    }

    /**
     * Returns the account details for the currently scoped identity
     * 
     * @return \Oroboros\core\interfaces\library\entity\EntityDetailCollectionInterface|null
     * @throws \Oroboros\core\exception\ErrorException
     */
    public function details(): ?\Oroboros\core\interfaces\library\entity\EntityDetailCollectionInterface
    {
        if (!$this->isValid()) {
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. Cannot obtain organization without a scoped identity.', get_class($this))
            );
        }
        return $this->identity()->details();
    }

    /**
     * Returns the account preferences for the currently scoped identity
     * 
     * @return type
     */
    public function preferences()
    {
        if (!$this->isValid()) {
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. Cannot obtain organization without a scoped identity.', get_class($this))
            );
        }
        return $this->identity()->preferences();
    }
    /**
     * -------------------------------------------------------------------------
     * |                            Protected Api                              |
     * -------------------------------------------------------------------------
     */

    /**
     * Internal getter for the user request
     * 
     * @return \Psr\Http\Message\ServerRequestInterface
     * @throws \Oroboros\core\exception\ErrorException
     *         If the request is not defined yet.
     */
    protected function getRequestObject(): \Psr\Http\Message\ServerRequestInterface
    {
        if (is_null(self::$request)) {
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. '
                        . 'Cannot obtain the request because '
                        . 'it is not yet defined.', get_class($this))
            );
        }
        return self::$request;
    }

    /**
     * Internal setter for the request object.
     * 
     * Setting the request object will also evaluate the request.
     * 
     * This may result in session state change.
     * 
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @return void
     */
    protected function setRequestObject(\Psr\Http\Message\ServerRequestInterface $request): void
    {
        self::$request = null;
        $this->evaluateRequest($request);
    }

    /**
     * Internal getter for the user session
     * 
     * @return \Oroboros\core\interfaces\library\session\SessionInterface
     */
    protected function getSessionObject(): \Oroboros\core\interfaces\library\session\SessionInterface
    {
        return $this->session;
    }

    /**
     * Internal setter for the session object
     * 
     * @param \Oroboros\core\interfaces\library\session\SessionInterface $session
     * @return void
     */
    protected function setSessionObject(\Oroboros\core\interfaces\library\session\SessionInterface $session): void
    {
        $this->session = $session;
    }

    /**
     * Returns the current identity object
     * 
     * @return \Oroboros\core\interfaces\library\user\UserIdentityInterface
     */
    protected function getIdentityObject(): \Oroboros\core\interfaces\library\user\UserIdentityInterface
    {
        return $this->identity;
    }

    /**
     * Sets the current identity object
     * 
     * @param \Oroboros\core\interfaces\library\user\UserIdentityInterface $identity
     * @return void
     */
    protected function setIdentityObject(\Oroboros\core\interfaces\library\user\UserIdentityInterface $identity): void
    {
        $this->getLogger()->debug('[type][scope][class] Identity set with identity [user-identity] and organization [user-organization].', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'user-identity' => $identity->id(),
            'user-organization' => $identity->organization()->id(),
        ]);
        $this->identity = $identity;
        $this->is_valid = true;
        $this->scope();
    }

    /**
     * Scopes to the default identity or command line identity defined by the class.
     * 
     * Will use the default public identity if the request is http,
     * and the default cli identity if it is from the command line.
     * 
     * @return void
     */
    protected function scopeDefaultIdentity(): void
    {
        $id = static::DEFAULT_IDENTITY;
        if ($this::IS_CLI) {
            $id = static::DEFAULT_CLI_IDENTITY;
        }
        $identity = $this->lookupIdentity($id, $id, $this->getArguments(), $this->getFlags());
        $identity->setLogger($this->getLogger());
        $this->setIdentityObject($identity);
        $this->is_authenticated = false;
        $this->is_logged_in = false;
        $this->is_valid = true;
    }

    /**
     * Scopes to the blacklist identity defined by the class.
     * 
     * @return void
     */
    protected function scopeBlacklistIdentity(): void
    {
        $id = static::BLACKLIST_IDENTITY;
        $identity = $this->lookupIdentity($id, $id, $this->getArguments(), $this->getFlags());
        $identity->setLogger($this->getLogger());
        $this->setIdentityObject($identity);
        $this->is_authenticated = false;
        $this->is_logged_in = false;
        $this->is_valid = true;
    }

    /**
     * Scopes to the ghost identity defined by the class.
     * 
     * @return void
     */
    protected function scopeGhostIdentity(): void
    {
        $id = static::GHOSTLIST_IDENTITY;
        $identity = $this->lookupIdentity($id, $id, $this->getArguments(), $this->getFlags());
        $identity->setLogger($this->getLogger());
        $this->setIdentityObject($identity);
        $this->is_authenticated = false;
        $this->is_logged_in = false;
        $this->is_valid = true;
    }

    /**
     * Scopes to the root identity. This method cannot be overridden.
     * 
     * This method uses a fixed strategy, id, and setter for scoping
     * root so it cannot be interfered with.
     * 
     * The root identity class and details also cannot be overridden.
     * 
     * @return void
     */
    final protected function scopeRootIdentity(): void
    {
        $id = \Oroboros\core\library\user\RootUserIdentity::DEFAULT_USERNAME; // Not overrideable
        $identity = $this->load('library', \Oroboros\core\library\user\UserIdentityStrategy::class, $id, $id, $this->getArguments(), $this->getFlags());
        $identity->setLogger($this->getLogger());
        $this->setIdentityObject($identity);
        $this->is_authenticated = true;
        $this->is_logged_in = true;
        $this->is_valid = true;
    }

    /**
     * Scopes to the core identity. This method cannot be overridden.
     * 
     * This method uses a fixed strategy, id, and setter for scoping
     * core so it cannot be interfered with.
     * 
     * The core identity class and details also cannot be overridden.
     * 
     * @return void
     */
    final protected function scopeCoreIdentity(): void
    {
        $id = \Oroboros\core\library\user\CoreUserIdentity::DEFAULT_USERNAME; // Not overrideable
        $identity = $this->load('library', \Oroboros\core\library\user\UserIdentityStrategy::class, $id, $id, $this->getArguments(), $this->getFlags());
        $identity->setLogger($this->getLogger());
        $this->setIdentityObject($identity);
        $this->is_authenticated = true;
        $this->is_logged_in = true;
        $this->is_valid = true;
    }

    /**
     * Scopes to the system identity. This method cannot be overridden.
     * 
     * This method uses a fixed strategy, id, and setter for scoping
     * system so it cannot be interfered with.
     * 
     * The system identity class also cannot be overridden,
     * but it's details can be changed to reflect the
     * running application.
     * 
     * @return void
     */
    final protected function scopeSystemIdentity(): void
    {
        $id = \Oroboros\core\library\user\SystemUserIdentity::DEFAULT_USERNAME; // Not overrideable
        $identity = $this->load('library', \Oroboros\core\library\user\UserIdentityStrategy::class, $id, $id, $this->getArguments(), $this->getFlags());
        $identity->setLogger($this->getLogger());
        $this->setIdentityObject($identity);
        $this->is_authenticated = true;
        $this->is_logged_in = true;
        $this->is_valid = true;
    }

    /**
     * Fetches the given identity type scoped based on the passed arguments
     * 
     * @param string $type
     * @param string $command
     * @param array $arguments
     * @param array $flags
     * @return \Oroboros\core\interfaces\library\user\UserIdentityInterface
     */
    protected function lookupIdentity(string $type, string $identity, array $arguments = null, array $flags = null): \Oroboros\core\interfaces\library\user\UserIdentityInterface
    {
        if (is_null($arguments)) {
            $arguments = [];
        }
        if (is_null($flags)) {
            $flags = [];
        }
        $this->getLogger()->debug('[type][scope][class] Looking up identity [user-identity] of type [user-type].', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'user-identity' => $identity,
            'user-type' => $type,
        ]);
        $arguments = array_merge($this->getArguments(), $arguments);
        $flags = array_merge($this->getFlags(), $flags);
        $this->initializeUserIdentityStrategy();
        return self::$identity_strategy->getIdentity($type, $identity, $arguments, $flags);
    }

    /**
     * This method fires whenever a request object is set within the user object
     * from a remote http request.
     * 
     * This will evaluate the user identity against the details provided
     * within the request only.
     * 
     * If you have additional criteria to perform when an http request is sent
     * and associated with a user, you may override this method to do so.
     * You will need to call `parent::evaluateRequest($request)` to have
     * any user functionality whatsoever.
     * 
     * This method must only return `true` when in an http environment.
     * 
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @return void
     */
    protected function evaluateHttpRequest(\Psr\Http\Message\ServerRequestInterface $request): bool
    {
        $designation = false;
        if (!$this::IS_CLI) {
            $designation = true;
            self::$is_localhost = $this->evaluateLocalhost($request);
            self::$is_whitelisted = $this->evaluateWhitelist($request);
            self::$is_blacklisted = $this->evaluateBlacklist($request);
            self::$is_ghostlisted = $this->evaluateGhostlist($request);
            self::$is_internal = $this->evaluateInternalNetwork($request);
            self::$is_trusted = $this->evaluateTrustNetwork($request);
            self::$is_public = $this->evaluatePublicNetwork($request);
            self::$is_secure = $this->evaluateSecureNetwork($request);
            $this->setFlag('request-mode', 'http');
        }
        return $designation;
    }

    /**
     * This method fires whenever a request object is set within the user object
     * from a command line process.
     * 
     * This will evaluate the user identity against the details provided
     * within the request and environment.
     * 
     * If you have additional criteria to perform when a cli request is sent
     * and associated with a user, you may override this method to do so.
     * You will need to call `parent::evaluateRequest($request)` to have
     * any user functionality whatsoever.
     * 
     * This method must only return `true` in a command line environment.
     * 
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @return bool
     */
    protected function evaluateCliRequest(\Psr\Http\Message\ServerRequestInterface $request): bool
    {
        $designation = false;
        if ($this::IS_CLI) {
            $designation = true;
            self::$is_localhost = $this->evaluateLocalhost($request);
            self::$is_whitelisted = $this->evaluateWhitelist($request);
            self::$is_blacklisted = $this->evaluateBlacklist($request);
            self::$is_ghostlisted = $this->evaluateGhostlist($request);
            self::$is_internal = $this->evaluateInternalNetwork($request);
            self::$is_trusted = $this->evaluateTrustNetwork($request);
            self::$is_public = $this->evaluatePublicNetwork($request);
            self::$is_secure = $this->evaluateSecureNetwork($request);
            $this->setFlag('request-mode', 'cli');
        }
        return $designation;
    }

    /**
     * This method fires whenever a user or ip address is registered
     * into a blacklist.
     * 
     * This will flag the request not to be honored and trigger
     * a 403 response on all requests.
     * 
     * The user will automatically gain the blacklist identity group
     * and lose the ghostlist identity group if they have it.
     * 
     * If you have additional criteria to perform when a blacklisted request
     * is received, you may override this method to do so.
     * 
     * You will need to call `parent::evaluateRequest($request)` to have
     * any user functionality whatsoever.
     * 
     * This method cannot return `true` if the request is cli,
     * from localhost, is whitelisted, or is a root or system user.
     * 
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @return bool
     */
    protected function evaluateBlacklist(\Psr\Http\Message\ServerRequestInterface $request): bool
    {
        $designation = false;
        if ($this::IS_CLI) {
            // Cli requests are never blacklisted
            $designation = false;
            self::$is_blacklisted = false;
        }
        if (self::$is_localhost === true) {
            // Localhost requests are never blacklisted
            $designation = false;
            self::$is_blacklisted = false;
        }
        if (self::$is_whitelisted === true) {
            // Whitelisted requests are never blacklisted
            $designation = false;
            self::$is_blacklisted = false;
        }
        if ($designation === false) {
            // @todo Check actual blacklist
        }
        $this->setFlag('blacklist', $designation);
        self::$is_blacklisted = $designation;
        return $designation;
    }

    /**
     * This method fires whenever a user or ip address is registered
     * into a ghostlist.
     * 
     * This will flag the request not to trust any input and silently discard it,
     * but still honor regular unsecured navigation.
     * 
     * The user will automatically gain the ghostlist identity group.
     * 
     * If you have additional criteria to perform when a ghostlisted request
     * is received, you may override this method to do so.
     * 
     * You will need to call `parent::evaluateRequest($request)` to have
     * any user functionality whatsoever.
     * 
     * This method cannot return `true` if the request is cli,
     * from localhost, is whitelisted, or is a root or system user.
     * 
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @return bool
     */
    protected function evaluateGhostlist(\Psr\Http\Message\ServerRequestInterface $request): bool
    {
        $designation = false;
        if ($this::IS_CLI) {
            // Cli requests are never ghostlisted
            $designation = false;
        }
        if (self::$is_localhost === true) {
            // Localhost requests are never ghostlisted
            $designation = false;
        }
        if (self::$is_whitelisted === true) {
            // Whitelisted requests are never ghostlisted
            $designation = false;
        }
        if (self::$is_internal === true) {
            // Internal network requests are never ghostlisted
            $designation = false;
        }
        if (self::$is_blacklisted === true) {
            // Blacklisting supercedes ghostlist
            $designation = false;
        }
        if ($designation === false) {
            // @todo Check actual ghostlist
        }
        if ($designation === true) {
            // Apply ghostlist conditions
        }
        $this->setFlag('ghost', $designation);
        self::$is_ghostlisted = $designation;
        return $designation;
    }

    /**
     * This method fires whenever a user or ip address is registered
     * into a whitelist.
     * 
     * This will flag the request to have elevated privileges
     * reserved for internal networks. Whitelisted users cannot
     * be banned, blacklisted, ghostlisted or blocked.
     * Permissions and access rights otherwise still apply normally.
     * 
     * The user will automatically gain the whitelist identity group,
     * and lose the ghostlist and blacklist identity groups if they have them.
     * 
     * If you have additional criteria to perform when a whitelisted request
     * is received, you may override this method to do so.
     * 
     * You will need to call `parent::evaluateRequest($request)` to have
     * any user functionality whatsoever.
     * 
     * This method cannot return `true` if the request is cli,
     * from localhost, is whitelisted, or is a root or system user.
     * 
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @return bool
     */
    protected function evaluateWhitelist(\Psr\Http\Message\ServerRequestInterface $request): bool
    {
        $designation = false;
        if ($this::IS_CLI) {
            // Cli requests are always whitelisted
            $designation = true;
            self::$is_whitelisted = true;
        }
        if (self::$is_localhost === true) {
            // Localhost requests are always whitelisted
            $designation = true;
        }
        if (self::$is_internal === true) {
            // Internal network requests are always whitelisted
            $designation = true;
        }
        if ($designation === false) {
            // @todo Check actual whitelist
        }
        if ($designation === true) {
            // Apply true conditions
            self::$is_whitelisted = true;
            self::$is_internal = true;
            self::$is_trusted = true;
            self::$is_public = true;
            self::$is_blacklisted = false;
            self::$is_ghostlisted = false;
        }
        $this->setFlag('whitelist', $designation);
        self::$is_whitelisted = $designation;
        return $designation;
    }

    /**
     * Evaluates the given request to determine if a session
     * is included in the request
     * 
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @return void
     */
    protected function evaluateSession(\Psr\Http\Message\ServerRequestInterface $request): void
    {
        $this->session()->evaluate($request);
    }

    /**
     * returns true if this is a localhost http or any cli request.
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @return void
     */
    protected function evaluateLocalhost(\Psr\Http\Message\ServerRequestInterface $request): bool
    {
        if ($this::IS_CLI) {
            // Cli requests are always localhost requests
            $this->setFlag('localhost', true);
            return true;
        }
        $params = $request->getServerParams();
        if (!array_key_exists('SERVER_ADDR', $params) || !array_key_exists('REMOTE_ADDR', $params)) {
            $this->setFlag('localhost', false);
            return false;
        }
        //Evaluate host
        if ($params['SERVER_ADDR'] !== $params['REMOTE_ADDR']) {
            $this->setFlag('localhost', false);
            return false;
        }
        // This is an http localhost request
        $this->setFlag('localhost', true);
        self::$is_secure = true;
        self::$is_localhost = true;
        self::$is_trusted = true;
        self::$is_public = true;
        self::$is_whitelisted = true;
        self::$is_blacklisted = false;
        self::$is_ghostlisted = false;
        return true;
    }

    protected function evaluateInternalNetwork(\Psr\Http\Message\ServerRequestInterface $request): bool
    {
        $designation = false;
        if ($this::IS_CLI) {
            // Cli requests are always localhost requests
            $designation = true;
        }
        if ($this->isLocalhost()) {
            // Localhost requests are always internal network
            $designation = true;
        }
        $this->setFlag('localhost', $designation);
        return $designation;
    }

    protected function evaluateTrustNetwork(\Psr\Http\Message\ServerRequestInterface $request): bool
    {
        $designation = false;
        if ($this::IS_CLI) {
            // Cli requests are always localhost requests
            $designation = true;
        }
        if ($this->isLocalhost()) {
            // Localhost requests are always trusted network
            $designation = true;
        }
        if ($this->isBlacklisted()) {
            // Blacklist requests do not have trusted network status
            $designation = false;
        }
        if ($this->isGhost()) {
            // Ghostlist requests do not have trusted network status
            $designation = false;
        }
        $this->setFlag('trusted', $designation);
        return $designation;
    }

    protected function evaluatePublicNetwork(\Psr\Http\Message\ServerRequestInterface $request): bool
    {
        $designation = true;
        if ($this::IS_CLI) {
            // Cli requests are always localhost requests
            $designation = true;
        }
        if ($this->isLocalhost()) {
            // Localhost requests are always public
            $designation = true;
        }
        if ($this->isBlacklisted()) {
            // Blacklist requests do not have public visibility
            $designation = false;
        }
        $this->setFlag('public', $designation);
        return $designation;
    }

    protected function evaluateSecureNetwork(\Psr\Http\Message\ServerRequestInterface $request): bool
    {
        $designation = false;
        if ($this::IS_CLI) {
            // Cli requests are always localhost requests
            $designation = true;
        }
        if ($this->isLocalhost()) {
            // Localhost requests are always secure
            $designation = true;
        }
        if ($this::IS_SSL) {
            // SSL requests are always secure
            $designation = true;
        }
        if ($designation === true) {
            self::$is_secure = true;
        }
        $this->setFlag('secure', $designation);
        return $designation;
    }
    /**
     * -------------------------------------------------------------------------
     * |                            Internals                                  |
     * -------------------------------------------------------------------------
     */

    /**
     * This method is just the internal switch to designate whether
     * to call the http or cli setup.
     * 
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @return void
     */
    private function evaluateRequest(\Psr\Http\Message\ServerRequestInterface $request): void
    {
        $this->initializeSession();
        $this->session()->scope($request);
        if ($this::IS_CLI) {
            // CLI only functionality
            $this->evaluateCliRequest($request);
        } else {
            // HTTP only functionality
            $this->evaluateHttpRequest($request);
        }
        if ($this->isBlacklisted()) {
            $this->scopeBlacklistIdentity();
            return;
        }
        if ($this->isGhost()) {
            $this->scopeGhostIdentity();
            return;
        }
        $this->restoreSessionIdentity();
    }

    /**
     * Scopes the session to the current user
     * @return void
     */
    private function scopeSession(): void
    {
        $this->getLogger()->debug('[type][scope][class] Scoping current identity [user-identity] and organization [user-organization] into the user session.', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'user-identity' => $this->identity()->id(),
            'user-organization' => $this->identity()->organization()->id(),
        ]);
        $this->session()->start();
        $this->set('id', $this->identity()->id());
        $this->set('organization', $this->organization()->id());
        if ($this->has('login-status')) {
            $this->is_logged_in = $this->get('login-status');
        }
        if ($this->has('login-authenticated')) {
            $this->is_authenticated = $this->get('login-authenticated');
        }
        $this->set('login-status', $this->is_logged_in);
        $this->set('login-authenticated', $this->is_authenticated);
        $this->is_logged_in = $this->get('login-status');
        $this->is_authenticated = $this->get('login-authenticated');
        $this->initializeAuthToken();
        $this->is_scoped = true;
    }

    /**
     * Restores the session identity from the previous session,
     * or creates a default session if there was no previous session.
     * 
     * @return void
     */
    private function restoreSessionIdentity(): void
    {
        try {
            $this->getLogger()->debug('[type][scope][class] Looking up existing session identity.', [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
            ]);
            $this->initializeSessionIdentity();
        } catch (\Oroboros\core\exception\session\NotFoundException $e) {
            // evaluate session
            $this->getLogger()->debug('[type][scope][class] No session marker found for user, initializing default session identity definition.', [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
            ]);
            $this->initializeDefaultSessionIdentity();
        }
    }

    /**
     * Initializes the session
     * @return void
     */
    private function initializeSession(): void
    {
        $this->getLogger()->debug('[type][scope][class] Initializing user session object.', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
        ]);
        $session = $this->load('library', static::SESSION_CLASS, null, $this->getArguments(), $this->getFlags());
        $session->setLogger($this->getLogger());
        $this->setSessionObject($session);
    }

    /**
     * Restores the session if it exists,
     * or the default identity if it does not.
     * 
     * @return void
     */
    private function initializeSessionIdentity(): void
    {
        if (!$this->has('id') && !$this->has('organization')) {
            // No valid session id
            throw new \Oroboros\core\exception\session\NotFoundException('No valid session identifier found.');
        }
        $identity = $this->lookupIdentity($this->get('id'), $this->get('id'), $this->getArguments(), $this->getFlags());
        $identity->setLogger($this->getLogger());
        if ($this->has('login-status')) {
            $this->is_logged_in = $this->get('login-status');
        }
        if ($this->has('login-authenticated')) {
            $this->is_logged_in = $this->get('login-authenticated');
        }
        $this->setIdentityObject($identity);
        $this->getLogger()->debug('[type][scope][class] Loaded session identity [session-identity] with organization [session-organization]', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'session-identity' => $this->id(),
            'session-organization' => $this->organization()->id(),
        ]);
    }

    /**
     * Scopes the current user to the default public user
     * 
     * @return void
     */
    private function initializeDefaultSessionIdentity(): void
    {
        $identity = $this->lookupIdentity('default', 'default', $this->getArguments(), $this->getFlags());
        $identity->setLogger($this->getLogger());
        $this->setIdentityObject($identity);
    }

    /**
     * Scopes the current user to the blacklist
     * 
     * @return void
     */
    private function initializeBlacklistSessionIdentity(): void
    {
        $identity = $this->lookupIdentity('blacklist', 'blacklist', $this->getArguments(), $this->getFlags());
        $identity->setLogger($this->getLogger());
        $this->setIdentityObject($identity);
    }

    /**
     * Scopes the current user to the ghostlist
     * 
     * @return void
     */
    private function initializeGhostlistSessionIdentity(): void
    {
        $identity = $this->lookupIdentity('ghost', 'ghost', $this->getArguments(), $this->getFlags());
        $identity->setLogger($this->getLogger());
        $this->setIdentityObject($identity);
    }

    /**
     * Scopes the current user to root.
     * 
     * @return void
     */
    private function initializeRootSessionIdentity(): void
    {
        $identity = $this->lookupIdentity('root', 'root', $this->getArguments(), $this->getFlags());
        $identity->setLogger($this->getLogger());
        $this->setIdentityObject($identity);
    }

    /**
     * Scopes the current user to an internal system process user identity.
     * 
     * @return void
     */
    private function initializeCoreSessionIdentity(): void
    {
        $identity = $this->lookupIdentity('core', 'core', $this->getArguments(), $this->getFlags());
        $identity->setLogger($this->getLogger());
        $this->setIdentityObject($identity);
    }

    /**
     * Scopes the current user to a system process user identity.
     * 
     * @return void
     */
    private function initializeSystemSessionIdentity(): void
    {
        $identity = $this->lookupIdentity('system', 'system', $this->getArguments(), $this->getFlags());
        $identity->setLogger($this->getLogger());
        $this->setIdentityObject($identity);
    }

    /**
     * Scopes the current user to a command line user identity.
     * 
     * @return void
     */
    private function initializeCliSessionIdentity(): void
    {
        $identity = $this->lookupIdentity('cli', 'cli', $this->getArguments(), $this->getFlags());
        $identity->setLogger($this->getLogger());
        $this->setIdentityObject($identity);
    }

    /**
     * Scopes the current user to a shell user identity.
     * 
     * @return void
     */
    private function initializeShellSessionIdentity(): void
    {
        $identity = $this->lookupIdentity('shell', 'shell', $this->getArguments(), $this->getFlags());
        $identity->setLogger($this->getLogger());
        $this->setIdentityObject($identity);
    }

    /**
     * Scopes the current user to a job queue task runner.
     * 
     * @return void
     */
    private function initializeJobSessionIdentity(): void
    {
        $identity = $this->lookupIdentity('job', 'job', $this->getArguments(), $this->getFlags());
        $identity->setLogger($this->getLogger());
        $this->setIdentityObject($identity);
    }

    /**
     * Scopes the current user to a websocket process task runner.
     * 
     * @return void
     */
    private function initializeWebsocketSessionIdentity(): void
    {
        $identity = $this->lookupIdentity('websocket', 'websocket', $this->getArguments(), $this->getFlags());
        $identity->setLogger($this->getLogger());
        $this->setIdentityObject($identity);
    }

    /**
     * Sets the default values for request statistics
     * @return void
     */
    private function resetRequestStatistics(): void
    {
        self::$is_localhost = false;
        self::$is_secure = false;
        self::$is_blacklisted = false;
        self::$is_whitelisted = false;
        self::$is_ghostlisted = false;
        self::$is_internal = false;
        self::$is_trusted = false;
        self::$is_public = false;
    }

    /**
     * Scopes the current user to a websocket process task runner.
     * 
     * @return void
     */
    private function initializeUserIdentityStrategy(): void
    {
        if (!is_null(self::$identity_strategy)) {
            // already done
            return;
        }
        self::$identity_strategy = $this->load('library', \Oroboros\core\library\user\UserIdentityStrategy::class, null, $this->getArguments(), $this->getFlags());
        self::$identity_strategy->setLogger($this->getLogger());
    }

    /**
     * Initializes the session auth token if it is not already initialized
     * 
     * @return void
     */
    private function initializeAuthToken(): void
    {
        if (!$this->has('session-auth-token')) {
            $token = $this->load('library'
                , static::AUTH_TOKEN_CLASS
                , null
                , $this->getArguments()
                , $this->getFlags()
            );
            $this->set('session-auth-token', serialize($token));
        }
    }

    private function getCurrentOrganization(\Psr\Http\Message\ServerRequestInterface $request): string
    {
//        try {
//            // If successful, using the naked domain
//            $this->load('model', 'DnsDomainModel', 'dns-domain')->lookup([
//                'domain' => $request->getUri()->getHost()
//            ]);
//            $subdomain = '';
//        } catch (\InvalidArgumentException $e) {
//            // Failure should only indicate that there is a subdomain
//            $subdomain = substr($request->getUri()->getHost(), 0, strpos($request->getUri()->getHost(), '.'));
//            $subdomain = (($subdomain === 'www') ? '' : $subdomain);
//        }
// Check subdomain on dns table
//        $dns = $this->load('model', 'DnsModel', 'dns')->lookup(['subdomain' => $subdomain]);
//        return $dns->get('organization');
        return static::DEFAULT_ORGANIZATION;
    }
}
