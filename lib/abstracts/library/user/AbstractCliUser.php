<?php
/*
 * The MIT License
 *
 * Copyright 2017 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\user;

/**
 * Abstract CLI User
 * Provides abstraction for command line user objects
 *
 * @author Brian Dayhoff
 */
abstract class AbstractCliUser extends AbstractUser implements \Oroboros\core\interfaces\library\user\CliUserInterface
{

    use \Oroboros\core\traits\library\resolver\ResolverUtilityTrait;

    /**
     * Declares the entity type as user
     */
    const ENTITY_TYPE = 'user';

    /**
     * Defines the entity scope as cli user
     */
    const ENTITY_SCOPE = 'cli-user';

    /**
     * Declares the session class to use as the default user session
     */
    const SESSION_CLASS = \Oroboros\core\library\session\cli\CliSession::class;

    /**
     * Defines the default username to use if no username is provided,
     * and the default user flag is passed.
     */
    const DEFAULT_USERNAME = 'default';

    /**
     * Defines the default user group to use if no user group is found for the user,
     * and the default user flag is passed.
     */
    const DEFAULT_USERGROUP = 'cli';

    /**
     * Defines the default organization to use if no organization is found for the user,
     * and the default user flag is passed.
     */
    const DEFAULT_ORGANIZATION = 'cli';

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
    }
}
