<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\user;

/**
 * Abstract User Identity
 * Provides abstraction for providing identity information for users
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
abstract class AbstractUserIdentity extends \Oroboros\core\abstracts\library\auth\AbstractIdentity implements \Oroboros\core\interfaces\library\user\UserIdentityInterface
{

    /**
     * Scopes the identity type to user
     */
    const AUTH_IDENTITY_TYPE = 'user';

    /**
     * Match the identity type
     */
    const AUTH_SCOPE = self::AUTH_IDENTITY_TYPE;

    /**
     * Matches the auth identity group to the default user group setting.
     */
    const AUTH_IDENTITY_GROUP_TYPE = AbstractUserGroup::AUTH_IDENTITY_TYPE;

    /**
     * Defines the entity type as user
     */
    const ENTITY_TYPE = self::AUTH_IDENTITY_TYPE;

    /**
     * Designates the container class used to house a related set of entity details
     */
    const ENTITY_DETAILS_CONTAINER_CLASS = \Oroboros\core\library\user\UserDetailsCategory::class;

    /**
     * Designates the collection class used to house all entity details
     */
    const ENTITY_DETAILS_COLLECTION_CLASS = \Oroboros\core\library\user\UserDetails::class;

    /**
     * Defines the default username used if no username is passed
     * 
     * You may override this constant to return a legitimate default
     * user identifier to use as a default if none exists for the
     * specific identity.
     */
    const DEFAULT_USERNAME = null;
    
    /**
     * Defines the default password as `NO_PASSWORD_DEFINED`
     * 
     * This value will never authenticate,
     * but satisfies storage requirements for a non-null field.
     * 
     * This will be used for all new user accounts created that are not
     * assigned a password, which will prevent login until administration
     * assigns them an initial password.
     */
    const DEFAULT_PASSWORD = 'NO_PASSWORD_DEFINED';

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        if (is_null($command)) {
            // Use the default username if none was passed
            $command = static::DEFAULT_USERNAME;
        }
        parent::__construct($command, $arguments, $flags);
        $this->initializeUserIdentity();
    }

    /**
     * Override this method to perform evaluation of passed credentials.
     * 
     * This method must return `true` if credentials are sufficient
     * to access the identity, and `false` otherwise.
     * 
     * The default behavior always returns `true` if the environment is scoped to cli, and `false` otherwise.
     * 
     * @param \Oroboros\core\interfaces\library\auth\CredentialContainerInterface $credentials
     * @return bool
     */
    protected function evaluateAuthentication(\Oroboros\core\interfaces\library\auth\CredentialContainerInterface $credentials): bool
    {
        if ($this::IS_CLI) {
            // Environment permits authentication
            return true;
        }
        return parent::evaluateAuthentication($credentials);
    }

    private function initializeUserIdentity(): void
    {
        if ($this::index()->has($this->id())) {
//            d($this, $this::index()[$this->id()]); exit;
        }
        $this->initializeIdentityDetailCategories();
    }

    protected function initializeIdentityDetailCategories(): void
    {
        $this->initializeIdentityDetails();
        $this->initializeInformationDetails();
        $this->initializeProfileDetails();
        $this->initializeSecurityDetails();
        $this->initializeOrganizationDetails();
        $this->initializeRelationshipDetails();
        $this->initializePreferenceDetails();
        $this->initializeLegalDetails();
        $this->initializeAddressDetails();
        $this->initializeContentDetails();
        $this->initializeMediaDetails();
        $this->initializeNotificationsDetails();
//        d($this->details()); exit;
    }

    protected function initializeIdentityDetails(): void
    {
        $category = \Oroboros\core\library\user\details\categories\identity\UserIdentity::init();
        $category->setLogger($this->getLogger());
        $category->setEntity($this);
        $key = 'identity';
        if ($this->hasArgument('details')) {
            $details = $this->getArgument('details');
            if (array_key_exists($key, $details)) {
                $category->setEntityDetails($key, $details[$key]);
            }
        }
        $this->registerDetailSection($key, $category);
    }

    protected function initializeProfileDetails(): void
    {
        $category = \Oroboros\core\library\user\details\categories\profile\UserProfile::init();
        $category->setLogger($this->getLogger());
        $category->setEntity($this);
        $key = 'profile';
        if ($this->hasArgument('details')) {
            $details = $this->getArgument('details');
            if (array_key_exists($key, $details)) {
                $category->setEntityDetails($key, $details[$key]);
            }
        }
        $this->registerDetailSection($key, $category);
    }

    protected function initializeAddressDetails(): void
    {
        $category = \Oroboros\core\library\user\details\categories\address\UserContactInformation::init();
        $category->setLogger($this->getLogger());
        $category->setEntity($this);
        $key = 'address';
        if ($this->hasArgument('details')) {
            $details = $this->getArgument('details');
            if (array_key_exists($key, $details)) {
                $category->setEntityDetails($key, $details[$key]);
            }
        }
        $this->registerDetailSection($key, $category);
    }

    protected function initializeContentDetails(): void
    {
        $category = \Oroboros\core\library\user\details\categories\content\UserContent::init();
        $category->setLogger($this->getLogger());
        $category->setEntity($this);
        $key = 'content';
        if ($this->hasArgument('details')) {
            $details = $this->getArgument('details');
            if (array_key_exists($key, $details)) {
                $category->setEntityDetails($key, $details[$key]);
            }
        }
        $this->registerDetailSection($key, $category);
    }

    protected function initializeInformationDetails(): void
    {
        $category = \Oroboros\core\library\user\details\categories\information\UserInformation::init();
        $category->setLogger($this->getLogger());
        $category->setEntity($this);
        $key = 'information';
        if ($this->hasArgument('details')) {
            $details = $this->getArgument('details');
            if (array_key_exists($key, $details)) {
                $category->setEntityDetails($key, $details[$key]);
            }
        }
        $this->registerDetailSection($key, $category);
    }

    protected function initializeLegalDetails(): void
    {
        $category = \Oroboros\core\library\user\details\categories\legal\UserLegal::init();
        $category->setLogger($this->getLogger());
        $category->setEntity($this);
        $key = 'legal';
        if ($this->hasArgument('details')) {
            $details = $this->getArgument('details');
            if (array_key_exists($key, $details)) {
                $category->setEntityDetails($key, $details[$key]);
            }
        }
        $this->registerDetailSection($key, $category);
    }

    protected function initializeMediaDetails(): void
    {
        $category = \Oroboros\core\library\user\details\categories\media\UserMedia::init();
        $category->setLogger($this->getLogger());
        $category->setEntity($this);
        $key = 'media';
        if ($this->hasArgument('details')) {
            $details = $this->getArgument('details');
            if (array_key_exists($key, $details)) {
                $category->setEntityDetails($key, $details[$key]);
            }
        }
        $this->registerDetailSection($key, $category);
    }

    protected function initializeNotificationsDetails(): void
    {
        $category = \Oroboros\core\library\user\details\categories\notifications\UserNotifications::init();
        $category->setLogger($this->getLogger());
        $category->setEntity($this);
        $key = 'notifications';
        if ($this->hasArgument('details')) {
            $details = $this->getArgument('details');
            if (array_key_exists($key, $details)) {
                $category->setEntityDetails($key, $details[$key]);
            }
        }
        $this->registerDetailSection($key, $category);
    }

    protected function initializeOrganizationDetails(): void
    {
        $category = \Oroboros\core\library\user\details\categories\organizations\UserOrganizations::init();
        $category->setLogger($this->getLogger());
        $category->setEntity($this);
        $key = 'organizations';
        if ($this->hasArgument('details')) {
            $details = $this->getArgument('details');
            if (array_key_exists($key, $details)) {
                $category->setEntityDetails($key, $details[$key]);
            }
        }
        $this->registerDetailSection($key, $category);
    }

    protected function initializePreferenceDetails(): void
    {
        $category = \Oroboros\core\library\user\details\categories\preferences\UserPreferences::init();
        $category->setLogger($this->getLogger());
        $category->setEntity($this);
        $key = 'preferences';
        if ($this->hasArgument('details')) {
            $details = $this->getArgument('details');
            if (array_key_exists($key, $details)) {
                $category->setEntityDetails($key, $details[$key]);
            }
        }
        $this->registerDetailSection($key, $category);
    }

    protected function initializeRelationshipDetails(): void
    {
        $category = \Oroboros\core\library\user\details\categories\relationships\UserRelationshipDetails::init();
        $category->setLogger($this->getLogger());
        $category->setEntity($this);
        $key = 'relationships';
        if ($this->hasArgument('details')) {
            $details = $this->getArgument('details');
            if (array_key_exists($key, $details)) {
                $category->setEntityDetails($key, $details[$key]);
            }
        }
        $this->registerDetailSection($key, $category);
    }

    protected function initializeSecurityDetails(): void
    {
        $category = \Oroboros\core\library\user\details\categories\security\UserSecurityDetails::init();
        $category->setLogger($this->getLogger());
        $category->setEntity($this);
        $key = 'security';
        if ($this->hasArgument('details')) {
            $details = $this->getArgument('details');
            if (array_key_exists($key, $details)) {
                $category->setEntityDetails($key, $details[$key]);
            }
        }
        $this->registerDetailSection($key, $category);
    }
}
