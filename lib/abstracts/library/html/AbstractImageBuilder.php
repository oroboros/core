<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\html;

/**
 * Abstract Html Image Asset Builder
 * Provides abstraction for building html image assets
 *
 * @author Brian Dayhoff
 */
abstract class AbstractImageBuilder extends \Oroboros\core\abstracts\pattern\builder\AbstractBuilder implements \Oroboros\core\interfaces\library\LibraryInterface
{

    const CLASS_SCOPE = 'html';
    const WORKER_SCOPE = 'html';
    const WORKER_TASK = 'image';

    /**
     * Set of known image details
     * @var Oroboros\core\interfaces\library\container\ContainerInterface 
     */
    private static $images = null;

    /**
     * Use the default response container
     */
    const CONTAINER_CLASS = 'Oroboros\\core\\library\\container\\ResponseContainer';

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeImages();
    }

    public function executeTask(array $arguments = [], array $flags = [])
    {
        $images = $this->containerizeInto('Oroboros\\core\\library\\container\\ResponseCollection');
        foreach (array_keys($this->getCategories()->toArray()) as $category) {
            $this->packageImages($category, $images);
        }
        return $images;
    }

    /**
     * Images may be a string (identifier for known image),
     * or an existing image object.
     * @return array
     */
    protected function declareCategories(): array
    {
        return [
            'image' => ['string', 'Oroboros\\core\\interfaces\\library\\http\\ImageInterface'],
        ];
    }

    protected function packageImages(string $category, \Oroboros\core\interfaces\library\container\ResponseCollectionInterface $existing): void
    {
        if (!$existing->has($category)) {
            $existing[$category] = $this->containerize();
        }
        $output = $existing->get($category);
        $properties = $this->get($category);
        foreach ($properties as $name => $value) {
            if ($this->checkExisting($value, $existing)) {
                // Do not double-package images
                continue;
            }
            if ($value instanceof \Oroboros\core\interfaces\library\http\ImageInterface) {
                $this->appendImage($value);
                $value = $value->getKey();
            }
            if (!self::$images->has($value)) {
                throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. Provided image key [%2$s] is not known.', get_class($this), $value));
            }
            $image = self::$images->get($value);
            $this->loadDependencies($category, $image, $existing);
            $existing[$category][$image->getKey()] = $image;
        }
    }

    /**
     * Adds the image dependencies if they are not already added
     * @param string $section Dependencies will be added into this category if not already existing
     * @param \Oroboros\core\interfaces\library\http\ImageInterface $image
     * @param \Oroboros\core\interfaces\library\container\ResponseCollectionInterface $existing
     * @return void
     */
    private function loadDependencies(string $section, \Oroboros\core\interfaces\library\http\ImageInterface $image, \Oroboros\core\interfaces\library\container\ResponseCollectionInterface $existing): void
    {
        $dependencies = $image->getDependencies();
        foreach ($dependencies as $dependency) {
            $exists = false;
            foreach ($existing as $loaded) {
                if ($loaded->has($dependency)) {
                    $exists = true;
                    break;
                }
            }
            if (!$exists) {
                if (!self::$images->has($dependency)) {
                    throw new \Oroboros\core\exception\ErrorException('Error encountered in [%1$s]. '
                            . 'Provided dependency image key [%2$s] is not known. '
                            . 'Registered as a dependency of [%3$s].', get_class($this), $dependency, $image->getKey());
                }
                $dep = self::$images->get($dependency);
                $existing[$section][$dep->getKey()] = $dep;
                $this->loadDependencies($section, $dep, $existing);
            }
        }
    }

    /**
     * Returns whether a provided image was already added to the response collection
     * @param string $imagename
     * @param \Oroboros\core\interfaces\library\container\ResponseCollectionInterface $existing
     * @return bool
     */
    private function checkExisting(string $imagename, \Oroboros\core\interfaces\library\container\ResponseCollectionInterface $existing): bool
    {
        foreach ($existing as $section) {
            if ($section->has($imagename)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Add the image to the known image pool if it isn't already known.
     * 
     * @param \Oroboros\core\interfaces\library\http\ImageInterface $image
     * @return void
     */
    private function appendImage(\Oroboros\core\interfaces\library\http\ImageInterface $image): void
    {
        if (!self::$images->has($image->getKey())) {
            self::$images[$image->getKey()] = $image;
        }
    }

    /**
     * Establishes a set of image objects that handle image output,
     * so images can be referenced by keyword without further concern.
     * @return void
     */
    private function initializeImages()
    {
        if (is_null(self::$images)) {
            $images = [];
            foreach ($this->getModel('image')->fetch() as $key => $value) {
                $images[$key] = $this->load('library', 'http\\Image', $key, $value);
            }
            self::$images = $this->containerizeInto('Oroboros\\core\\library\\container\\Container', $images);
        }
    }
}
