<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\html;

/**
 * Abstract Html Font Builder
 * Provides abstraction for building html font dependency objects for responses
 *
 * @author Brian Dayhoff
 */
abstract class AbstractFontBuilder extends \Oroboros\core\abstracts\pattern\builder\AbstractBuilder implements \Oroboros\core\interfaces\library\LibraryInterface
{

    const CLASS_SCOPE = 'html';
    const WORKER_SCOPE = 'html';
    const WORKER_TASK = 'font';

    /**
     * Use the default response container
     */
    const CONTAINER_CLASS = 'Oroboros\\core\\library\\container\\ResponseContainer';

    /**
     * Defines the default font format used for the font [eot, ttf, woff, woff2]
     * This can be overridden by passing the `font-format` flag to the constructor.
     */
    const DEFAULT_FONT_FORMAT = 'woff2';

    /**
     * List of common font formats.
     * @var array
     */
    private static $font_formats = [
        'eot',
        'ttf',
        'woff',
        'woff2',
    ];

    /**
     * Set of known font details
     * @var Oroboros\core\interfaces\library\container\ContainerInterface 
     */
    private static $fonts = null;

    /**
     * Defines the localized font format used to define the font.
     * If the constructor flag `font-format` was not passed,
     * this will be the class constant `DEFAULT_FONT_FORMAT`.
     * @var string
     */
    private $font_format = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeFonts();
    }

    public function executeTask(array $arguments = [], array $flags = [])
    {
        $fonts = $this->containerizeInto('Oroboros\\core\\library\\container\\ResponseCollection', [
            'font' => $this->containerizeInto(self::CONTAINER_CLASS)
        ]);
        $this->packageFonts('font', $fonts);
        $this->packageLayoutFonts($fonts);
        $this->packageFonts('font', $fonts);
        return $fonts;
    }

    protected function declareCategories(): array
    {
        return [
            'font' => ['string', 'Oroboros\\core\\interfaces\\library\\http\\FontInterface'],
            'layout' => [
                'Oroboros\\core\\interfaces\\library\\layout\\LayoutInterface'
            ],
        ];
    }

    protected function packageFonts(string $category, \Oroboros\core\interfaces\library\container\ResponseCollectionInterface $existing): void
    {
        if (!$existing->has($category)) {
            $existing[$category] = $this->containerize();
        }
        $format = $this->font_format;
        $output = $existing->get($category);
        $properties = $this->get($category);
        foreach ($properties as $name => $value) {
            if ($this->checkExisting($value, $existing)) {
                // Do not double-package fonts
                continue;
            }
            if ($value instanceof \Oroboros\core\interfaces\library\http\FontInterface) {
                $this->appendFont($value);
                $value = $value->getKey();
            }
            if (!self::$fonts->get($format)->has($value)) {
                throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. Provided fontsheet key [%2$s] is not known.', get_class($this), $value));
            }
            $font = self::$fonts->get($format)->get($value);
            $this->loadDependencies($category, $font, $existing);
            $existing[$category][$font->getKey()] = $font;
        }
    }

    protected function packageLayoutFonts(\Oroboros\core\interfaces\library\container\ResponseCollectionInterface $existing): void
    {
        $layout = $this->get('layout')->toArray();
        if (empty($layout)) {
            return;
        }
        $layout = array_pop($layout);
        foreach ($layout->getFonts() as $key => $fonts) {
            foreach ($fonts as $font) {
                $this->appendFont($font);
                $this->loadDependencies('font', $font, $existing);
                $existing['font'][$font->getKey()] = $font;
            }
        }
    }

    /**
     * Adds the font dependencies if they are not already added
     * @param string $section Dependencies will be added into this category if not already existing
     * @param \Oroboros\core\interfaces\library\http\FontInterface $font
     * @param \Oroboros\core\interfaces\library\container\ResponseCollectionInterface $existing
     * @return void
     */
    private function loadDependencies(string $section, \Oroboros\core\interfaces\library\http\FontInterface $font, \Oroboros\core\interfaces\library\container\ResponseCollectionInterface $existing): void
    {
        $dependencies = $font->getDependencies();
        foreach ($dependencies as $dependency) {
            $exists = false;
            foreach ($existing as $loaded) {
                if ($loaded->has($dependency)) {
                    $exists = true;
                    break;
                }
            }
            if (!$exists) {
                if (!self::$fonts->has($dependency)) {
                    throw new \Oroboros\core\exception\ErrorException('Error encountered in [%1$s]. '
                            . 'Provided dependency fontsheet key [%2$s] is not known. '
                            . 'Registered as a dependency of [%3$s].', get_class($this), $dependency, $font->getKey());
                }
                $dep = self::$fonts->get($dependency);
                $existing[$section][$dep->getKey()] = $dep;
                $this->loadDependencies($section, $dep, $existing);
            }
        }
    }

    /**
     * Returns whether a provided font was already added to the response collection
     * @param string $fontname
     * @param \Oroboros\core\interfaces\library\container\ResponseCollectionInterface $existing
     * @return bool
     */
    private function checkExisting(string $fontname, \Oroboros\core\interfaces\library\container\ResponseCollectionInterface $existing): bool
    {
        foreach ($existing as $section) {
            if ($section->has($fontname)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Add the font to the known font pool if it isn't already known.
     * 
     * @param \Oroboros\core\interfaces\library\http\FontInterface $font
     * @return void
     */
    private function appendFont(\Oroboros\core\interfaces\library\http\FontInterface $font): void
    {
        if (!self::$fonts->has($font->getFormat())) {
            self::$fonts[$font->getFormat()] = $this::containerizeInto('Oroboros\\core\\library\\container\\Container');
        }
        if (!self::$fonts->get($font->getFormat())->has($font->getKey())) {
            self::$fonts[$font->getFormat()][$font->getKey()] = $font;
        }
    }

    /**
     * Establishes the font format that the builder
     * will be using to return built font objects.
     */
    private function initializeFontFormat()
    {
        if ($this->hasFlag('font-format')) {
            $this->font_format = $this->getFlag('font-format');
        } else {
            $this->font_format = static::DEFAULT_FONT_FORMAT;
        }
    }

    /**
     * Establishes a set of fontsheet objects that handle fontsheet output,
     * so fontsheets can be referenced by keyword without further concern.
     * @return void
     */
    private function initializeFonts()
    {
        $font_collection = $this->containerizeInto('Oroboros\\core\\library\\container\\Collection');
        if (is_null(self::$fonts)) {
            foreach (self::$font_formats as $format) {
                $fonts = [];
                foreach ($this->getModel('font')->fetch() as $key => $value) {
                    $fonts[$key] = $this->load('library', 'http\\Font', $key, $value, null, ['source' => $value['source'][$format]], ['font-format' => $format]);
                }
                $font_collection[$format] = $this->containerizeInto('Oroboros\\core\\library\\container\\Container', $fonts);
            }
            self::$fonts = $font_collection;
        }
    }
}
