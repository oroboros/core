<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\html;

/**
 * Abstract Html Meta Builder
 * Provides abstraction for building meta tag information for a html view
 *
 * @author Brian Dayhoff
 */
abstract class AbstractMetaBuilder extends \Oroboros\core\abstracts\pattern\builder\AbstractBuilder implements \Oroboros\core\interfaces\library\LibraryInterface
{

    const CLASS_SCOPE = 'html';
    const WORKER_SCOPE = 'html';
    const WORKER_TASK = 'meta';

    /**
     * Use the default response container
     */
    const CONTAINER_CLASS = 'Oroboros\\core\\library\\container\\ResponseContainer';

    public function executeTask(array $arguments = [], array $flags = [])
    {
        $meta = $this->containerizeInto('Oroboros\\core\\library\\container\\ResponseCollection');
        foreach (array_keys($this->getCategories()->toArray()) as $category) {
            $this->packageMeta($category, $this->get($category)->toArray(), $meta);
        }
        return $meta;
    }

    protected function declareCategories(): array
    {
        return [
            'charset' => ['string'],
            'viewport' => ['string'],
            'description' => ['string'],
            'author' => ['string'],
            'meta' => ['array', 'Oroboros\\core\\interfaces\\library\\container\\ContainerInterface'],
        ];
    }

    protected function declareDefaults(): array
    {
        return [
            'charset' => ['charset' => 'utf-8'],
            'viewport' => ['name' => 'viewport', 'content' => 'width=device-width, initial-scale=1, shrink-to-fit=no'],
            'description' => ['name' => 'description', 'content' => 'No description provided.'],
            'author' => ['name' => 'author', 'content' => 'Unknown'],
        ];
    }

    protected function packageMeta(string $category, array $meta, \Oroboros\core\interfaces\library\container\CollectionInterface $existing): void
    {
        if ($category === 'meta') {
            // Arbitrary tags will be filed on their own
            $this->packageArbitraryMetaTags($meta, $existing);
            return;
        }
        if (!$existing->has($category)) {
            $existing[$category] = $this::containerize();
        }
        // Remaining meta tags use explicit declarations.
        // They should be overwritten if they already exist.
        $existing[$category] = $this::containerize($meta);
    }

    private function packageArbitraryMetaTags(array $meta, \Oroboros\core\interfaces\library\container\ResponseCollectionInterface $existing): void
    {
        foreach ($meta as $name => $attributes) {
            if (!$existing->has($name)) {
                $existing[$name] = $this::containerize();
            }
            $existing[$name][] = $this::containerize($attributes);
        }
    }
}
