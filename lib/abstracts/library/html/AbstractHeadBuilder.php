<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\html;

/**
 * Abstract Html Document Head Builder
 * Provides abstraction for building a collection of all information
 * required for the document head to hand off to the view.
 *
 * @author Brian Dayhoff
 */
abstract class AbstractHeadBuilder extends \Oroboros\core\abstracts\pattern\builder\AbstractBuilder implements \Oroboros\core\interfaces\library\LibraryInterface
{

    const CLASS_SCOPE = 'html';
    const WORKER_SCOPE = 'html';
    const WORKER_TASK = 'head';

    /**
     * Use the default response container
     */
    const CONTAINER_CLASS = 'Oroboros\\core\\library\\container\\ResponseContainer';

    public function executeTask(array $arguments = [], array $flags = [])
    {
        $head = $this->containerize();
        foreach (array_keys($this->getCategories()->toArray()) as $category) {
            $this->packageHead($category, $this->get($category)->toArray(), $head);
        }
        return $head;
    }

    protected function declareCategories(): array
    {
        return [
            'title' => ['string'],
            'preconnect' => ['string'],
            'prefetch' => ['string'],
            'preload' => ['string'],
            'meta' => ['Oroboros\\core\\interfaces\\library\\container\\ContainerInterface'],
            'script' => ['Oroboros\\core\\interfaces\\library\\container\\ContainerInterface'],
            'stylesheet' => ['Oroboros\\core\\interfaces\\library\\container\\ContainerInterface'],
            'font' => ['Oroboros\\core\\interfaces\\library\\container\\ContainerInterface'],
            'data' => ['Oroboros\\core\\interfaces\\library\\container\\ContainerInterface'],
        ];
    }

    protected function declareDefaults(): array
    {
        return [
            'title' => ['page' => 'Untitled Page']
        ];
    }

    protected function packageHead(string $category, array $data, \Oroboros\core\interfaces\library\container\ContainerInterface $existing): void
    {
        switch ($category) {
            case 'title':
                $existing['title'] = $data['page'];
                break;
            case 'preconnect':
            case 'prefetch':
            case 'preload':
                $existing[$category] = $this->containerize($data);
                break;
            case 'data':
                $existing[$category] = json_encode($data);
                break;
            case 'script':
            case 'stylesheet':
            case 'font':
                $existing[$category] = $data['head'];
                break;
            case 'meta':
                $this->parseMeta($data, $existing);
                break;
        }
    }

    private function parseMeta(array $data, \Oroboros\core\interfaces\library\container\ContainerInterface $existing): void
    {
        foreach ($data as $key => $value) {
            switch ($key) {
                case 'charset':
                    $existing[$key] = $value->get('charset');
                    break;
                case 'viewport':
                case 'description':
                case 'author':
                    $existing[$key] = $value->get('content');
                    break;
                default:
                    $existing[$key] = $value;
                    break;
            }
        }
    }
}
