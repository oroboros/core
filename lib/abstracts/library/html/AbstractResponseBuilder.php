<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\html;

/**
 * Abstract Http Response Builder
 * Provides abstraction for building http responses
 *
 * @author Brian Dayhoff
 */
abstract class AbstractResponseBuilder extends \Oroboros\core\abstracts\library\http\AbstractResponseBuilder implements \Oroboros\core\interfaces\library\LibraryInterface
{

    const WORKER_SCOPE = 'html';

    public function build(array $flags = [])
    {
        $result = $this->containerizeInto('Oroboros\\core\\library\\container\\ResponseContainer');
        foreach (array_keys($this->getCategories()->toArray()) as $category) {
            $this->packageResponse($category, $this->get($category)->toArray(), $result);
        }
        $output = $this->containerize([
            'code' => $result->get('code'),
            'cookie' => $result->get('cookie'),
            'header' => $result->get('header'),
            'context' => $this::containerize([
                'doctype' => $result->get('doctype'),
                'lang' => $result->get('lang'),
                'head' => $result->get('head'),
                'body' => $result->get('body'),
                'component' => $result->get('component'),
            ])
        ]);
        return $output;
    }

    protected function declareCategories(): array
    {
        return array_replace_recursive(parent::declareCategories(), [
            'doctype' => 'string',
            'lang' => 'string',
            'header' => 'string',
            'cookie' => 'Oroboros\\core\\interfaces\\library\\container\\ResponseContainerInterface',
            'head' => [
                'string',
                'array',
                'Oroboros\\core\\interfaces\\library\\container\\ResponseCollectionInterface',
                'Oroboros\\core\\interfaces\\library\\container\\ResponseContainerInterface'
            ],
            'body' => [
                'string',
                'array',
                'Oroboros\\core\\interfaces\\library\\container\\ResponseCollectionInterface',
                'Oroboros\\core\\interfaces\\library\\container\\ResponseContainerInterface'
            ],
            'component' => 'Oroboros\\core\\interfaces\\library\\container\\ResponseContainerInterface',
        ]);
    }

    protected function declareDefaults(): array
    {
        return [
            'code' => ['status' => 200], // Default valid response
            'doctype' => ['page' => 'html'], // Default html5
            'lang' => ['page' => 'en'], // Default english
        ];
    }

    protected function packageResponse(string $category, array $data, \Oroboros\core\interfaces\library\container\ContainerInterface $existing): void
    {
        switch ($category) {
            case 'code':
                $existing[$category] = $data['status'];
                break;
            case 'doctype':
            case 'lang':
                $existing[$category] = $data['page'];
                break;
            case 'cookie':
            case 'header':
            case 'head':
            case 'body':
                $existing[$category] = $this->containerize($data);
                break;
            case 'component':
                $existing[$category] = $this->containerizeInto('Oroboros\\core\\library\\container\\ResponseCollection', $data);
                break;
            case 'content':
                // no-op - relic from abstraction, useful for raw http but not html
                break;
            default:
                d($category, $data, $existing->toArray());
                exit;
                exit;
                break;
        }
    }
}
