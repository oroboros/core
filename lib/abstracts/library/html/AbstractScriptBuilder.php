<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\html;

/**
 * Abstract Html Script Builder
 * Provides abstraction for building html script dependency objects for responses
 *
 * @author Brian Dayhoff
 */
abstract class AbstractScriptBuilder extends \Oroboros\core\abstracts\pattern\builder\AbstractBuilder implements \Oroboros\core\interfaces\library\LibraryInterface
{

    const CLASS_SCOPE = 'html';
    const WORKER_SCOPE = 'html';
    const WORKER_TASK = 'script';

    /**
     * Set of known script details
     * @var Oroboros\core\interfaces\library\container\ContainerInterface 
     */
    private static $scripts = null;

    /**
     * Use the default response container
     */
    const CONTAINER_CLASS = 'Oroboros\\core\\library\\container\\ResponseContainer';

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeScripts();
    }

    public function executeTask(array $arguments = [], array $flags = [])
    {
        $scripts = $this->containerizeInto('Oroboros\\core\\library\\container\\ResponseCollection', [
            'head' => $this->containerizeInto(self::CONTAINER_CLASS),
            'body' => $this->containerizeInto(self::CONTAINER_CLASS),
            'footer' => $this->containerizeInto(self::CONTAINER_CLASS),
        ]);
        $this->packageLayoutScripts($scripts);
        foreach (array_keys($this->getCategories()->toArray()) as $category) {
            if ($category === 'layout') {
                continue;
            }
            $this->packageScripts($category, $scripts);
        }
        return $scripts;
    }

    /**
     * Sets a value in the builder for the given category and key
     * @param string $category
     * @param string $key
     * @param type $value
     * @return $this (method chainable)
     * @throws \InvalidArgumentException If the given category does not exist,
     *     the given key is not valid for the given category,
     *     or the provided value is not a valid type for the given category and key.
     */
    public function set(string $category, string $key, $value): \Oroboros\core\interfaces\pattern\builder\BuilderInterface
    {
        if (is_string($value) && self::$scripts->has($value)) {
            $value = self::$scripts[$value];
        }
        parent::set($category, $key, $value);
        return $this;
    }

    /**
     * Scripts may be a string (identifier for known script),
     * or an existing script object.
     * @return array
     */
    protected function declareCategories(): array
    {
        return [
            'head' => ['string', 'Oroboros\\core\\interfaces\\library\\http\\ScriptInterface'],
            'body' => ['string', 'Oroboros\\core\\interfaces\\library\\http\\ScriptInterface'],
            'footer' => ['string', 'Oroboros\\core\\interfaces\\library\\http\\ScriptInterface'],
            'layout' => ['Oroboros\\core\\interfaces\\library\\layout\\LayoutInterface'],
        ];
    }

    /**
     * Declare the script resources that should be used as defaults.
     * These will override the defaults in the event of a collision.
     * @return array
     */
    protected function declareScripts(): array
    {
        $scripts = [];
        if ($this->hasArgument('extensions')) {
            foreach ($this->getArgument('extensions') as $name => $extension) {
                $set = $extension->scripts();
                if (!empty($set)) {
                    foreach ($set as $key => $script) {
                        $scripts[$key] = $script;
                    }
                }
            }
        }
        if ($this->hasArgument('modules')) {
            foreach ($this->getArgument('modules') as $name => $module) {
                $set = $module->scripts();
                if (!empty($set)) {
                    foreach ($set as $key => $script) {
                        $scripts[$key] = $script;
                    }
                }
            }
        }
        return $scripts;
    }

    protected function packageScripts(string $category, \Oroboros\core\interfaces\library\container\ResponseCollectionInterface $existing): void
    {
        $obj = null;
        if (!$existing->has($category)) {
            $existing[$category] = $this->containerize();
        }
        $output = $existing->get($category);
        $properties = $this->get($category);
        foreach ($properties as $name => $value) {
            if ($this->checkExisting($value, $existing)) {
                // Do not double-package scripts
                continue;
            }
            if ($value instanceof \Oroboros\core\interfaces\library\http\ScriptInterface) {
                $this->appendScript($value);
                $obj = $value;
                $value = $value->getKey();
            }
            if (!self::$scripts->has($value)) {
                throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. Provided script key [%2$s] is not known.', get_class($this), $value));
            }
            $script = self::$scripts->get($value);
            $this->loadDependencies($category, $script, $existing);
            $existing[$category][$script->getKey()] = $script;
        }
    }

    protected function packageLayoutScripts(\Oroboros\core\interfaces\library\container\ResponseCollectionInterface $existing): void
    {
        $layout = $this->get('layout')->toArray();
        if (empty($layout)) {
            return;
        }
        $layout = array_pop($layout);
        foreach ($layout->getScripts() as $category => $scripts) {
            foreach ($scripts as $script) {
                if (is_string($script) && self::$scripts->has($script)) {
                    $script = self::$scripts[$script];
                } elseif (is_string($script)) {
                    d($script);
                }
                $this->loadDependencies($category, $script, $existing);
                $existing[$category][$script->getKey()] = $script;
            }
        }
    }

    /**
     * Adds the script dependencies if they are not already added
     * @param string $section Dependencies will be added into this category if not already existing
     * @param \Oroboros\core\interfaces\library\http\ScriptInterface $script
     * @param \Oroboros\core\interfaces\library\container\ResponseCollectionInterface $existing
     * @return void
     */
    private function loadDependencies(string $section, \Oroboros\core\interfaces\library\http\ScriptInterface $script, \Oroboros\core\interfaces\library\container\ResponseCollectionInterface $existing): void
    {
        $dependencies = $script->getDependencies();
        foreach ($dependencies as $dependency) {
            $exists = false;
            foreach ($existing as $loaded) {
                if ($loaded->has($dependency)) {
                    $exists = true;
                    break;
                }
            }
            if (!$exists) {
                if (!self::$scripts->has($dependency)) {
                    throw new \Oroboros\core\exception\ErrorException('Error encountered in [%1$s]. '
                            . 'Provided dependency script key [%2$s] is not known. '
                            . 'Registered as a dependency of [%3$s].', get_class($this), $dependency, $script->getKey());
                }
                $dep = self::$scripts->get($dependency);
                $existing[$section][$dep->getKey()] = $dep;
                $this->loadDependencies($section, $dep, $existing);
            }
        }
    }

    /**
     * Returns whether a provided script was already added to the response collection
     * @param string $scriptname
     * @param \Oroboros\core\interfaces\library\container\ResponseCollectionInterface $existing
     * @return bool
     */
    private function checkExisting(string $scriptname, \Oroboros\core\interfaces\library\container\ResponseCollectionInterface $existing): bool
    {
        foreach ($existing as $section) {
            if ($section->has($scriptname)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Add the script to the known script pool if it isn't already known.
     * 
     * @param \Oroboros\core\interfaces\library\http\ScriptInterface $script
     * @return void
     */
    private function appendScript(\Oroboros\core\interfaces\library\http\ScriptInterface $script): void
    {
        if (!self::$scripts->has($script->getKey())) {
            self::$scripts[$script->getKey()] = $script;
        }
    }

    /**
     * Establishes a set of script objects that handle script output,
     * so scripts can be referenced by keyword without further concern.
     * @return void
     */
    private function initializeScripts()
    {
        if (is_null(self::$scripts)) {
            $scripts = [];
            // Load core scripts
            foreach ($this->getModel('script')->fetch() as $key => $value) {
                $scripts[$key] = $this->load('library', 'http\\Script', $key, $value);
            }
            // Load override scripts
            foreach ($this->declareScripts() as $key => $value) {
                $scripts[$key] = $this->load('library', 'http\\Script', $key, $value);
            }
            self::$scripts = $this->containerizeInto('Oroboros\\core\\library\\container\\Container', $scripts);
        }
    }
}
