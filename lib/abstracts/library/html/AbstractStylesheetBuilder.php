<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\html;

/**
 * Abstract Html Stylesheet Builder
 * Provides abstraction for building html stylesheet dependency objects for responses
 *
 * @author Brian Dayhoff
 */
abstract class AbstractStylesheetBuilder extends \Oroboros\core\abstracts\pattern\builder\AbstractBuilder implements \Oroboros\core\interfaces\library\LibraryInterface
{

    const CLASS_SCOPE = 'html';
    const WORKER_SCOPE = 'html';
    const WORKER_TASK = 'stylesheet';

    /**
     * Set of known stylesheet details
     * @var Oroboros\core\interfaces\library\container\ContainerInterface 
     */
    private static $styles = null;

    /**
     * Use the default response container
     */
    const CONTAINER_CLASS = 'Oroboros\\core\\library\\container\\ResponseContainer';

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeStylesheets();
    }

    public function executeTask(array $arguments = [], array $flags = [])
    {
        $styles = $this->containerizeInto('Oroboros\\core\\library\\container\\ResponseCollection', [
            'stylesheet' => $this->containerizeInto(self::CONTAINER_CLASS)
        ]);
        $this->packageLayoutStylesheets($styles);
        $this->packageStylesheets('stylesheet', $styles);
        return $styles;
    }

    /**
     * Stylesheets may be a string (identifier for known stylesheet),
     * or an existing stylesheet object.
     * @return array
     */
    protected function declareCategories(): array
    {
        return [
            'stylesheet' => ['string', 'Oroboros\\core\\interfaces\\library\\http\\StylesheetInterface'],
            'layout' => [
                'Oroboros\\core\\interfaces\\library\\layout\\LayoutInterface'
            ],
        ];
    }

    /**
     * Declare the stylesheet resources that should be used as defaults.
     * These will override the defaults in the event of a collision.
     * @return array
     */
    protected function declareStylesheets(): array
    {
        $sheets = [];
        if ($this->hasArgument('extensions')) {
            foreach ($this->getArgument('extensions') as $name => $extension) {
                $set = $extension->styles();
                if (!empty($set)) {
                    foreach ($set as $key => $style) {
                        $sheets[$key] = $style;
                    }
                }
            }
        }
        if ($this->hasArgument('modules')) {
            foreach ($this->getArgument('modules') as $name => $module) {
                $set = $module->styles();
                if (!empty($set)) {
                    foreach ($set as $key => $style) {
                        $sheets[$key] = $style;
                    }
                }
            }
        }
        return $sheets;
    }

    protected function packageStylesheets(string $category, \Oroboros\core\interfaces\library\container\ResponseCollectionInterface $existing): void
    {
        if (!$existing->has($category)) {
            $existing[$category] = $this->containerize();
        }
        $output = $existing->get($category);
        $properties = $this->get($category);
        foreach ($properties as $name => $value) {
            if ($this->checkExisting($value, $existing)) {
                // Do not double-package styles
                continue;
            }
            if ($value instanceof \Oroboros\core\interfaces\library\http\StylesheetInterface) {
                $this->appendStylesheet($value);
                $value = $value->getKey();
            }
            if (!self::$styles->has($value)) {
                throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. Provided stylesheet key [%2$s] is not known.', get_class($this), $value));
            }
            $style = self::$styles->get($value);
            $this->loadDependencies($category, $style, $existing);
            $existing[$category][$style->getKey()] = $style;
        }
    }

    protected function packageLayoutStylesheets(\Oroboros\core\interfaces\library\container\ResponseCollectionInterface $existing): void
    {
        $layout = $this->get('layout')->toArray();
        if (empty($layout)) {
            return;
        }
        $layout = array_pop($layout);
        foreach ($layout->getStyles() as $key => $style) {
            if (is_string($style) && self::$styles->has($style)) {
                $style = self::$styles[$style];
            }
            $this->appendStylesheet($style);
            $this->loadDependencies('stylesheet', $style, $existing);
            $existing['stylesheet'][$style->getKey()] = $style;
        }
    }

    /**
     * Adds the style dependencies if they are not already added
     * @param string $section Dependencies will be added into this category if not already existing
     * @param \Oroboros\core\interfaces\library\http\StylesheetInterface $style
     * @param \Oroboros\core\interfaces\library\container\ResponseCollectionInterface $existing
     * @return void
     */
    private function loadDependencies(string $section, \Oroboros\core\interfaces\library\http\StylesheetInterface $style, \Oroboros\core\interfaces\library\container\ResponseCollectionInterface $existing): void
    {
        $dependencies = $style->getDependencies();
        foreach ($dependencies as $dependency) {
            $exists = false;
            foreach ($existing as $loaded) {
                if ($loaded->has($dependency)) {
                    $exists = true;
                    break;
                }
            }
            if (!$exists) {
                if (!self::$styles->has($dependency)) {
                    throw new \Oroboros\core\exception\ErrorException('Error encountered in [%1$s]. '
                            . 'Provided dependency stylesheet key [%2$s] is not known. '
                            . 'Registered as a dependency of [%3$s].', get_class($this), $dependency, $style->getKey());
                }
                $dep = self::$styles->get($dependency);
                $existing[$section][$dep->getKey()] = $dep;
                $this->loadDependencies($section, $dep, $existing);
            }
        }
    }

    /**
     * Returns whether a provided style was already added to the response collection
     * @param string $stylename
     * @param \Oroboros\core\interfaces\library\container\ResponseCollectionInterface $existing
     * @return bool
     */
    private function checkExisting(string $stylename, \Oroboros\core\interfaces\library\container\ResponseCollectionInterface $existing): bool
    {
        foreach ($existing as $section) {
            if ($section->has($stylename)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Add the style to the known style pool if it isn't already known.
     * 
     * @param \Oroboros\core\interfaces\library\http\StylesheetInterface $style
     * @return void
     */
    private function appendStylesheet(\Oroboros\core\interfaces\library\http\StylesheetInterface $style): void
    {
        if (!self::$styles->has($style->getKey())) {
            self::$styles[$style->getKey()] = $style;
        }
    }

    /**
     * Establishes a set of stylesheet objects that handle stylesheet output,
     * so stylesheets can be referenced by keyword without further concern.
     * @return void
     */
    private function initializeStylesheets()
    {
        if (is_null(self::$styles)) {
            $styles = [];
            foreach ($this->getModel('style')->fetch() as $key => $value) {
                $styles[$key] = $this->load('library', 'http\\Stylesheet', $key, $value);
            }
            foreach ($this->declareStylesheets() as $key => $value) {
                $styles[$key] = $this->load('library', 'http\\Stylesheet', $key, $value);
            }
            self::$styles = $this->containerizeInto('Oroboros\\core\\library\\container\\Container', $styles);
        }
    }
}
