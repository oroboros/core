<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\html;

/**
 * Abstract Html Document Body Builder
 * Provides abstraction for building a collection of all information
 * required for the document body to hand off to the view.
 *
 * @author Brian Dayhoff
 */
abstract class AbstractBodyBuilder extends \Oroboros\core\abstracts\pattern\builder\AbstractBuilder implements \Oroboros\core\interfaces\library\LibraryInterface
{

    const CLASS_SCOPE = 'html';
    const WORKER_SCOPE = 'html';
    const WORKER_TASK = 'body';

    /**
     * Use the default response container
     */
    const CONTAINER_CLASS = 'Oroboros\\core\\library\\container\\ResponseContainer';

    public function executeTask(array $arguments = [], array $flags = [])
    {
        $body = $this->containerize();
        foreach (array_keys($this->getCategories()->toArray()) as $category) {
            $this->packageBody($category, $this->get($category)->toArray(), $body);
        }
        return $body;
    }

    protected function declareCategories(): array
    {
        return [
            'id' => ['string'],
            'class' => [
                'string',
                'array'
            ],
            'script' => [
                'Oroboros\\core\\interfaces\\library\\container\\ContainerInterface'
            ],
            'user' => [
                'string',
                'array',
                'Oroboros\\core\\interfaces\\library\\container\\ContainerInterface'
            ],
            'layout' => [
                'Oroboros\\core\\interfaces\\library\\layout\\LayoutInterface'
            ],
        ];
    }

    protected function packageBody(string $category, array $data, \Oroboros\core\interfaces\library\container\ContainerInterface $existing): void
    {
        switch ($category) {
            case 'id':
            case 'class':
                $existing[$category] = $data['page'];
                break;
            case 'user':
                $existing[$category] = $data;
                break;
            case 'script':
                $existing[$category] = $this->containerizeInto('Oroboros\\core\\library\\container\\ResponseCollection', $data);
                break;
            case 'layout':
                $existing[$category] = $this->packageLayout(array_pop($data));
                break;
            default:
                d('Invalid category', $category, $data, $existing);
                exit;
                break;
        }
    }

    private function packageLayout(\Oroboros\core\interfaces\library\layout\LayoutInterface $layout): string
    {
        return $layout->render();
    }
}
