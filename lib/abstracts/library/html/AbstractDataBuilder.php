<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\html;

/**
 * Abstract Html Data Builder
 * Provides abstraction for building embedded data for html pages
 *
 * @author Brian Dayhoff
 */
abstract class AbstractDataBuilder extends \Oroboros\core\abstracts\pattern\builder\AbstractBuilder implements \Oroboros\core\interfaces\library\LibraryInterface
{

    const CLASS_SCOPE = 'html';
    const WORKER_SCOPE = 'html';
    const WORKER_TASK = 'data';

    /**
     * Use the default response container
     */
    const CONTAINER_CLASS = 'Oroboros\\core\\library\\container\\ResponseContainer';

    /**
     * Set of known data details
     * @var Oroboros\core\interfaces\library\container\ContainerInterface 
     */
    private static $data = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeData();
    }

    public function executeTask(array $arguments = [], array $flags = [])
    {
        $data = $this->containerizeInto('Oroboros\\core\\library\\container\\ResponseCollection', []);
        foreach (array_keys(self::$data->toArray()) as $category) {
            if (in_array($category, ['layout', 'components'])) {
                continue;
            }
            $this->packageData($category, $data);
        }
        $this->packageLayoutData($data);
        $this->packageComponentData($data);
        return $data;
    }

    protected function declareCategories(): array
    {
        return [
            'fingerprint' => ['string'],
            'application' => ['array', 'Oroboros\\core\\interfaces\\library\\container\\ContainerInterface', 'Oroboros\\core\\interfaces\\library\\parser\\ParserInterface'],
            'page' => ['string', 'array', 'Oroboros\\core\\interfaces\\library\\container\\ContainerInterface', 'Oroboros\\core\\interfaces\\library\\parser\\ParserInterface'],
            'meta' => ['array', 'Oroboros\\core\\interfaces\\library\\container\\ContainerInterface', 'Oroboros\\core\\interfaces\\library\\parser\\ParserInterface'],
            'endpoints' => ['array', 'Oroboros\\core\\interfaces\\library\\container\\ContainerInterface', 'Oroboros\\core\\interfaces\\library\\parser\\ParserInterface'],
            'resources' => ['array', 'Oroboros\\core\\interfaces\\library\\container\\ContainerInterface', 'Oroboros\\core\\interfaces\\library\\parser\\ParserInterface'],
            'elements' => ['array', 'Oroboros\\core\\interfaces\\library\\container\\ContainerInterface', 'Oroboros\\core\\interfaces\\library\\parser\\ParserInterface'],
            'bindings' => ['array', 'Oroboros\\core\\interfaces\\library\\container\\ContainerInterface', 'Oroboros\\core\\interfaces\\library\\parser\\ParserInterface'],
            'auth' => ['array', 'Oroboros\\core\\interfaces\\library\\container\\ContainerInterface', 'Oroboros\\core\\interfaces\\library\\parser\\ParserInterface'],
            'user' => ['array', 'Oroboros\\core\\interfaces\\library\\container\\ContainerInterface', 'Oroboros\\core\\interfaces\\library\\parser\\ParserInterface'],
            'components' => ['array', 'Oroboros\\core\\interfaces\\library\\container\\ContainerInterface', 'Oroboros\\core\\interfaces\\library\\parser\\ParserInterface'],
            'redirects' => ['array', 'Oroboros\\core\\interfaces\\library\\container\\ContainerInterface', 'Oroboros\\core\\interfaces\\library\\parser\\ParserInterface'],
            'api' => ['array', 'Oroboros\\core\\interfaces\\library\\container\\ContainerInterface', 'Oroboros\\core\\interfaces\\library\\parser\\ParserInterface'],
            'debug' => ['array', 'Oroboros\\core\\interfaces\\library\\container\\ContainerInterface', 'Oroboros\\core\\interfaces\\library\\parser\\ParserInterface'],
            'layout' => [
                'Oroboros\\core\\interfaces\\library\\layout\\LayoutInterface'
            ],
        ];
    }

    protected function packageData(string $category, \Oroboros\core\interfaces\library\container\ResponseCollectionInterface $existing): void
    {
        if (!$existing->has($category)) {
            $existing[$category] = $this->containerize();
        }
        $output = $existing->get($category);
        $properties = $this->get($category);
        foreach ($properties as $name => $value) {
            if ($this->checkExisting($name, $existing)) {
                // Do not double-package data
                continue;
            }
            $existing[$category][$name] = $value;
        }
    }

    protected function packageLayoutData(\Oroboros\core\interfaces\library\container\ResponseCollectionInterface $existing): void
    {
        $layout = $this->get('layout')->toArray();
        if (empty($layout)) {
            return;
        }
        $layout = array_pop($layout);
        foreach ($layout->getData() as $key => $data) {
            foreach ($data as $data) {
                $this->appendData($data);
                $this->loadDependencies('data', $data, $existing);
                $existing['data'][$data->getKey()] = $data;
            }
        }
    }

    protected function packageComponentData(\Oroboros\core\interfaces\library\container\ResponseCollectionInterface $existing): void
    {
        $data = [];
        $components = $this->getDirector()->getWorker('component')->getComponents(static::WORKER_SCOPE);
        foreach ($components as $component_key => $instances) {
            foreach ($instances as $instance) {
                $identifier = $instance->getIdentifier();
                if (is_null($identifier)) {
                    // Do not package data for unnamed component instances
                    continue;
                }
                $dat = $instance->getData()->toArray();
                if (!empty($dat)) {
                    $data[$component_key][$identifier] = $dat;
                }
            }
        }
        $existing['component'] = $this::containerize($data);
    }

    /**
     * Adds the data dependencies if they are not already added
     * @param string $section Dependencies will be added into this category if not already existing
     * @param \Oroboros\core\interfaces\library\http\DataInterface $data
     * @param \Oroboros\core\interfaces\library\container\ResponseCollectionInterface $existing
     * @return void
     */
    private function loadDependencies(string $section, \Oroboros\core\interfaces\library\http\DataInterface $data, \Oroboros\core\interfaces\library\container\ResponseCollectionInterface $existing): void
    {
        $dependencies = $data->getDependencies();
        foreach ($dependencies as $dependency) {
            $exists = false;
            foreach ($existing as $loaded) {
                if ($loaded->has($dependency)) {
                    $exists = true;
                    break;
                }
            }
            if (!$exists) {
                if (!self::$data->has($dependency)) {
                    throw new \Oroboros\core\exception\ErrorException('Error encountered in [%1$s]. '
                            . 'Provided dependency dataheet key [%2$s] is not known. '
                            . 'Registered as a dependency of [%3$s].', get_class($this), $dependency, $data->getKey());
                }
                $dep = self::$data->get($dependency);
                $existing[$section][$dep->getKey()] = $dep;
                $this->loadDependencies($section, $dep, $existing);
            }
        }
    }

    /**
     * Returns whether a provided data was already added to the response collection
     * @param string $key
     * @param \Oroboros\core\interfaces\library\container\ResponseCollectionInterface $existing
     * @return bool
     */
    private function checkExisting(string $key, \Oroboros\core\interfaces\library\container\ResponseCollectionInterface $existing): bool
    {
        foreach ($existing as $section) {
            if ($section->has($key)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Add the data to the known data pool if it isn't already known.
     * 
     * @param \Oroboros\core\interfaces\library\http\DataInterface $data
     * @return void
     */
    private function appendData(\Oroboros\core\interfaces\library\http\DataInterface $data): void
    {
        if (!self::$data->has($data->getFormat())) {
            self::$data[$data->getFormat()] = $this::containerizeInto('Oroboros\\core\\library\\container\\Container');
        }
        if (!self::$data->get($data->getFormat())->has($data->getKey())) {
            self::$data[$data->getFormat()][$data->getKey()] = $data;
        }
    }

    /**
     * Establishes a container for page data.
     * @return void
     */
    private function initializeData()
    {
        $data_collection = $this->containerizeInto('Oroboros\\core\\library\\container\\Collection');
        if (is_null(self::$data)) {
            foreach ($this->declareCategories() as $format => $allow) {
                $data = [];
                $data_collection[$format] = $this->containerizeInto('Oroboros\\core\\library\\container\\Container', []);
            }
            self::$data = $data_collection;
        }
    }
}
