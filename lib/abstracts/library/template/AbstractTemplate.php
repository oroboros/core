<?php
namespace Oroboros\core\abstracts\library\template;

/**
 * Provides abstraction for wrapping templating systems,
 * so they can be used interchangeably (eg Twig, Laravel Blade, Smarty, etc)
 *
 * @author Brian Dayhoff
 */
abstract class AbstractTemplate extends \Oroboros\core\abstracts\library\AbstractLibrary implements \Oroboros\core\interfaces\library\template\TemplateInterface
{

    /**
     * Defines further child classes as templates
     */
    const CLASS_SCOPE = 'template';
    const TEMPLATE_ENGINE = null;

    private $template = null;
    private $data = [];

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        $this->verifyTemplateEngine();
        parent::__construct($command, $arguments, $flags);
        $this->loadTemplateEngineResources();
        $this->initializeTemplateEngine();
    }

    public function getTemplate()
    {
        return $this->template;
    }

    public function setTemplate(string $template)
    {
        $this->template = $template;
        return $this;
    }

    public function getData()
    {
        return $this->data;
    }

    public function setData(\Oroboros\core\interfaces\library\container\ContainerInterface $data)
    {
        $data['template'] = $this->template;
        $debug = $this->getDebugDetails();
        // Only package debug info if the app has designated that debug is enabled, 
        // and the provided data has a debug key
        if ($data->has('debug') && $debug['enabled'] === true && (is_object($data['debug']) && $data['debug'] instanceof \Oroboros\core\interfaces\library\component\ComponentInterface) ) {
//            d($this, $data['debug'], debug_backtrace(1)); exit;
            $data['debug']->addContext('template', $this->getDebugDetails());
            $debug['component'] = $data['debug'];
        }
        $data['debug'] = $debug;
        $this->data = $data;
        return $this;
    }

    abstract public function renderTemplate();

    /**
     * Override this method in the child class to load any resources
     * required for the template engine to be initialized.
     * @return void
     */
    abstract protected function loadTemplateEngineResources();

    /**
     * Override this method in the child class to initialize
     * the template engine itself.
     * @return void
     */
    abstract protected function initializeTemplateEngine();

    /**
     * Returns the baseline debug definitions
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    protected function getDebugDetails(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $enabled = $this::app()->debugEnabled();
        $data = [
            'enabled' => $enabled
        ];
        if ($enabled) {
            $data['template'] = $this->template;
            $data['engine'] = get_class($this);
        }
        return $this::containerize($data);
    }

    private function verifyTemplateEngine()
    {
        if (is_null(static::TEMPLATE_ENGINE)) {
            throw new \OutOfRangeException(sprintf('Class [%1$s] is invalid. '
                        . 'Class constant [%2$s] must be declared with the identifier of '
                        . 'the template engine represented by the class.', get_class($this), 'TEMPLATE_ENGINE'));
        }
    }
}
