<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\organization;

/**
 * Abstract Internal Organization
 * Provides baseline abstraction for official internal organizations
 * 
 * These always represent a confirmable entity under
 * the control of the same ownership of the application.
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
abstract class AbstractInternalOrganization extends AbstractOrganization implements \Oroboros\core\interfaces\library\organization\InternalOrganizationInterface
{

    /**
     * Define the class scope as organization
     */
    const CLASS_SCOPE = 'organization';

    /**
     * Scopes the identity type to organization
     */
    const AUTH_IDENTITY_TYPE = self::CLASS_SCOPE;

    /**
     * Match the identity type
     */
    const AUTH_SCOPE = self::AUTH_IDENTITY_TYPE;

    /**
     * Matches the auth identity group to the default organization group setting.
     */
    const AUTH_IDENTITY_GROUP_TYPE = AbstractOrganizationGroup::AUTH_IDENTITY_TYPE;

    /**
     * Defines the entity type as organization
     */
    const ENTITY_TYPE = self::AUTH_IDENTITY_TYPE;

}
