<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\organization;

/**
 * Abstract Organization Definer
 * Provides abstraction for defining organizations
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
abstract class AbstractOrganizationDefiner extends \Oroboros\core\abstracts\library\auth\AbstractIdentityDefiner implements \Oroboros\core\interfaces\library\organization\OrganizationDefinerInterface
{

    use \Oroboros\core\traits\library\entity\EntityCommonTrait;

    /**
     * Sets the class scope to identity definer
     */
    const CLASS_SCOPE = 'organization-definer';
    
    /**
     * Match the organization entity type
     */
    const ENTITY_TYPE = AbstractOrganization::ENTITY_TYPE;

    /**
     * Defines the entity scope as definer
     */
    const ENTITY_SCOPE = 'definer';
    
    /**
     * Match the organization entity category
     */
    const ENTITY_CATEGORY = AbstractOrganization::ENTITY_CATEGORY;

    /**
     * Match the organization entity subcategory
     */
    const ENTITY_SUBCATEGORY = AbstractOrganization::ENTITY_SUBCATEGORY;

    /**
     * Match the organization entity context
     */
    const ENTITY_CONTEXT = AbstractOrganization::ENTITY_CONTEXT;

    /**
     * Match the default organization auth scope
     */
    const AUTH_SCOPE = AbstractOrganization::AUTH_SCOPE;

    /**
     * Scope the default identity class to the default organization class
     */
    const DEFAULT_IDENTITY_CLASS = \Oroboros\core\library\organization\Organization::class;

}
