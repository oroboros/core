<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\organization;

/**
 * Abstract Organization
 * Provides baseline abstraction for officially known organizations
 * 
 * These always represent a confirmable entity.
 * For ambiguous organizations, use OrganizationEntity,
 * which does not enforce a known presence in the system
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
abstract class AbstractOrganization extends \Oroboros\core\abstracts\library\auth\AbstractOrganizationIdentity implements \Oroboros\core\interfaces\library\organization\OrganizationInterface
{

    use \Oroboros\core\traits\library\entity\EntityCommonTrait;

    /**
     * Define the class scope as organization
     */
    const CLASS_SCOPE = 'organization';

    /**
     * Scopes the identity type to organization
     */
    const AUTH_IDENTITY_TYPE = self::CLASS_SCOPE;

    /**
     * Match the identity type
     */
    const AUTH_SCOPE = self::AUTH_IDENTITY_TYPE;

    /**
     * Matches the auth identity group to the default organization group setting.
     */
    const AUTH_IDENTITY_GROUP_TYPE = AbstractOrganizationGroup::AUTH_IDENTITY_TYPE;

    /**
     * Defines the entity type as organization
     */
    const ENTITY_TYPE = self::AUTH_IDENTITY_TYPE;
    
    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeEntity();
        $this->initializeOrganization();
    }
    
    protected function handleArguments(): void
    {
        
    }
    
    private function initializeOrganization(): void
    {
        $this->handleArguments();
//        d($this->getArguments(), $this); exit;
    }

}
