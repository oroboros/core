<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Oroboros\core\abstracts\library\parser;

/**
 * Establishes standardized abstraction for parsing data formats
 *
 * @author Brian Dayhoff
 */
abstract class AbstractParser extends \Oroboros\core\abstracts\library\AbstractLibrary implements \Oroboros\core\interfaces\library\parser\ParserInterface
{

    /**
     * Defines further child classes as parsers
     */
    const CLASS_SCOPE = 'parser';

    /**
     * This must be overridden in the implementation to define the data format
     * the parser is associated with (eg: CSV, json, xml, etc).
     * Object construction will fail if this remains null.
     */
    const PARSER_FORMAT = null;

    /**
     * Determines the pointer for iteration
     * @var int
     */
    private $pointer = 0;

    /**
     * Contains the keys for iteration
     * @var array
     */
    private $keys = [];

    /**
     * Contains the values for iteration
     * @var array
     */
    private $values = [];

    /**
     * Stores the container used to house the full value set of the parser output.
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $container = null;

    /**
     * 
     * @param string $command The fully qualified path to the file to parse.
     * Must be a valid file
     * 
     * @param array $arguments Any arguments to pass to the parser,
     *  to interact with the underlying parser logic. Defined by extension.
     * @param array $flags Any arguments to pass to the parser to interact
     *  directly with the implementation. Defined by extension.
     * @throws OutOfRangeException If the parser is not a valid implementation.
     *  Do not catch this exception, it needs to be refactored by a developer.
     * @throws OutOfBoundsException If the given file is not valid.
     *  This exception should be handled upstream.
     */
    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        // Check that the parser honors its standardization contract
        $this->verifyParserFormat();
        parent::__construct($command, $arguments, $flags);
        $this->parse($command);
    }

    /**
     * Parses the file. If the file is not parsable into a valid data set, throws an \OutOfRange exception.
     * @return $this (method chainable)
     * @throws \OutOfBoundsException If the file does not exist or is not readable.
     */
    public function parse($file_location)
    {
        // Check that the given file exists
        $this->verifyFile($file_location);
        $data = $this->parseFile($file_location);
        $this->initializeContainer($data);
        return $this;
    }

    /**
     * Returns the finalized container that has all of the parsed values stored internally.
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    public function fetch()
    {
        return $this->container;
    }

    /**
     * Returns the value for the current iteration
     * @return mixed
     */
    public function current()
    {
        return $this->container->current();
    }

    /**
     * Returns the key for the current iteration
     * @return scalar
     */
    public function key()
    {
        return $this->container->key();
    }

    /**
     * Increments the iterator
     * @return void
     */
    public function next()
    {
        $this->container->next();
    }

    /**
     * Rewinds the iterator
     * @return void
     */
    public function rewind()
    {
        $this->container->rewind();
    }

    /**
     * Determines if any further iteration is possible
     * @return bool
     */
    public function valid()
    {
        return $this->container->valid();
    }

    /**
     * Checks whether a key exists
     * @param mixed $offset A scalar value of the specified offset (underlying implementation requires mixed, not scalar)
     * @return bool Returns whether the given key exists
     */
    public function offsetExists($offset)
    {
        return $this->container->offsetExists($offset);
    }

    /**
     * Gets a key
     * @param mixed $offset A scalar value of the specified offset (underlying implementation requires mixed, not scalar)
     * @return mixed Returns null if the key does not exist, otherwise returns the key.
     * @note To explicitly check for the existence of a key, use offsetExists
     */
    public function offsetGet($offset)
    {
        return $this->container->offsetGet($offset);
    }

    /**
     * Sets a key
     * @param mixed $offset A scalar value of the specified offset (underlying implementation requires mixed, not scalar)
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        $this->container->offsetSet($offset, $value);
    }

    /**
     * Unsets a key
     * @param mixed $offset A scalar value of the specified offset (underlying implementation requires mixed, not scalar)
     * @return void
     */
    public function offsetUnset($offset)
    {
        $this->container->offsetUnset($offset);
    }

    /**
     * Creates the data container for the parsed values
     * @param iterable $data
     */
    protected function initializeContainer(iterable $data)
    {
        $values = [];
        foreach ($data as $key => $value) {
            $values[$key] = $value;
        }
        $result = $this::containerize($values);
        $this->container = $result;
    }

    /**
     * Parses the file path given to the parser.
     * Should throw an \OutOfBoundsException if the file is missing
     * or incorrectly formatted for the given parser.
     * 
     * If the file can be parsed correctly,
     * this method should return an array of the parsed file data.
     * @return array
     * @throws OutOfBoundsException if the file cannot resolve to a valid data set for any reason
     */
    abstract protected function parseFile(string $file_location);

    /**
     * Throws an OutOfRangeException if the parser class is not correctly implemented.
     * @throws \OutOfRangeException
     */
    private function verifyParserFormat()
    {
        if (is_null(static::PARSER_FORMAT)) {
            throw new \OutOfRangeException(sprintf('Parser class [%1$s] is invalid. '
                        . 'Class constant [%2$s] must define the parser implementation scope.', get_class($this), 'PARSER_FORMAT'));
        }
    }

    /**
     * Verifies that the given data file location exists.
     * @throws \OutOfBoundsException if the file does not exist or is not readable.
     * @return void
     */
    private function verifyFile(string $file_location)
    {
        if (!file_exists($file_location)) {
            // File not found
            throw new \OutOfBoundsException(sprintf('Error encountered in [%1$s] parser class [%2$s]. '
                        . 'Provided file [%3$s] was not found.', static::PARSER_FORMAT, get_class($this), $file_location));
        } elseif (!is_readable($file_location)) {
            // File could not be read
            throw new \OutOfBoundsException(sprintf('Error encountered in [%1$s] parser class [%2$s]. '
                        . 'Provided file [%3$s] is not readable.', static::PARSER_FORMAT, get_class($this), $file_location));
        }
    }
}
