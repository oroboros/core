<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\php;

/**
 * Abstract PHP Response Director
 * Provides abstraction for building dynamically generated PHP responses
 *
 * @author Brian Dayhoff
 */
abstract class AbstractResponseDirector extends \Oroboros\core\abstracts\library\http\AbstractResponseDirector
{

    const CLASS_SCOPE = \Oroboros\core\abstracts\view\AbstractPhpView::CLASS_SCOPE;

    public function build(array $flags = [])
    {
        $output = $this->containerizeInto('Oroboros\\core\\library\\container\\ResponseCollection');
        $result = $this->containerizeInto('Oroboros\\core\\library\\container\\ResponseCollection');
        $keys = $this->getWorkerKeys();
        foreach ($keys as $worker) {
            if (in_array($worker, ['head', 'body', 'response'])) {
                // These will get packaged with the result set
                continue;
            }
            $output[$worker] = $this->getWorker($worker)->executeTask();
        }
        $this->packageResponseHead($output, $result);
        $this->packageResponseBody($output, $result);
        return $this->packageResponseObject($output, $result);
    }

    /**
     * Packages the data required for the document head
     * @param \Oroboros\core\interfaces\library\container\ResponseCollectionInterface $data
     * @param \Oroboros\core\interfaces\library\container\ResponseCollectionInterface $result
     */
    private function packageResponseHead(\Oroboros\core\interfaces\library\container\ResponseCollectionInterface $data, \Oroboros\core\interfaces\library\container\ResponseCollectionInterface $result)
    {
        $worker = $this->getWorker('head');
        foreach ($data as $key => $value) {
            switch ($key) {
                case 'meta':
                    foreach ($data->get('meta') as $key => $v) {
                        $worker->set('meta', $key, $v);
                    }
                    break;
                case 'stylesheet':
                    $worker->set('stylesheet', 'head', $value->get('stylesheet'));
                    break;
                case 'font':
                    $worker->set('font', 'head', $value->get('font'));
                    break;
                case 'script':
                    $worker->set('script', 'head', $value->get('head'));
                    break;
            }
        }
        $this->parsePreconnect($worker);
        $this->parsePrefetch($worker);
        $this->parsePreload($worker);
        $this->parseData($worker);
    }

    /**
     * Packages the data required for the document body
     * @param \Oroboros\core\interfaces\library\container\ResponseCollectionInterface $data
     * @param \Oroboros\core\interfaces\library\container\ResponseCollectionInterface $result
     */
    private function packageResponseBody(\Oroboros\core\interfaces\library\container\ResponseCollectionInterface $data, \Oroboros\core\interfaces\library\container\ResponseCollectionInterface $result)
    {
        $worker = $this->getWorker('body');
        foreach ($data as $key => $value) {
            switch ($key) {
                case 'script':
                    if ($value->has('body')) {
                        $worker->set('script', 'body', $value->get('body'));
                    }
                    if ($value->has('footer')) {
                        $worker->set('script', 'footer', $value->get('footer'));
                    }
                    break;
            }
        }
        $this->parsePreconnect($worker);
        $this->parsePrefetch($worker);
        $this->parsePreload($worker);
    }

    /**
     * Packages the final response collection
     * @param \Oroboros\core\interfaces\library\container\ResponseCollectionInterface $data
     */
    private function packageResponseObject(\Oroboros\core\interfaces\library\container\ResponseCollectionInterface $data)
    {
        $worker = $this->getWorker('response');
        if ($data->has('header')) {
            foreach ($data->get('header') as $key => $value) {
                $worker->set('header', $key, $value);
            }
        }
        if ($data->has('cookie')) {
            foreach ($data->get('cookie') as $key => $value) {
                $worker->set('cookie', $key, $value);
            }
        }
        if ($data->has('component')) {
            foreach ($data->get('component') as $key => $value) {
                $worker->set('component', $key, $value);
            }
        }
        foreach ($this->getWorker('head')->build() as $key => $value) {
            $worker->set('head', $key, $value);
        }
        foreach ($this->getWorker('body')->build() as $key => $value) {
            $worker->set('body', $key, $value);
        }
        $result = $worker->build();
        return $result;
    }

    private function parsePreconnect($worker)
    {
        $target = $this->getWorker('head');
        if ($worker->has('script')) {
            foreach ($worker->get('script') as $section) {
                foreach ($section as $script) {
                    $target->set('preconnect', $script->getHost()->getHost(), $script->getPreconnect());
                }
            }
        }
        if ($worker->has('stylesheet')) {
            foreach ($worker->get('stylesheet') as $section) {
                foreach ($section as $stylesheet) {
                    $target->set('preconnect', $stylesheet->getHost()->getHost(), $stylesheet->getPreconnect());
                }
            }
        }
        if ($worker->has('font')) {
            foreach ($worker->get('font') as $section) {
                foreach ($section as $font) {
                    $target->set('preconnect', $font->getHost()->getHost(), $font->getPreconnect());
                }
            }
        }
    }

    private function parsePrefetch($worker)
    {
        $target = $this->getWorker('head');
        if ($worker->has('script')) {
            foreach ($worker->get('script') as $section) {
                foreach ($section as $script) {
                    $target->set('prefetch', (string) $script->getHost(), $script->getPrefetch());
                }
            }
        }
        if ($worker->has('stylesheet')) {
            foreach ($worker->get('stylesheet') as $section) {
                foreach ($section as $stylesheet) {
                    $target->set('prefetch', (string) $stylesheet->getHost(), $stylesheet->getPrefetch());
                }
            }
        }
        if ($worker->has('font')) {
            foreach ($worker->get('script') as $section) {
                foreach ($section as $font) {
                    $target->set('prefetch', (string) $font->getHost(), $font->getPrefetch());
                }
            }
        }
    }

    private function parsePreload($worker)
    {
        $target = $this->getWorker('head');
        if ($worker->has('script')) {
            foreach ($worker->get('script') as $section) {
                foreach ($section as $script) {
                    $target->set('preload', (string) $script->getHost(), $script->getPreload());
                }
            }
        }
        if ($worker->has('stylesheet')) {
            foreach ($worker->get('stylesheet') as $section) {
                foreach ($section as $stylesheet) {
                    $target->set('preload', (string) $stylesheet->getHost(), $stylesheet->getPreload());
                }
            }
        }
        if ($worker->has('font')) {
            foreach ($worker->get('script') as $section) {
                foreach ($section as $font) {
                    $target->set('preload', (string) $font->getHost(), $font->getPreload());
                }
            }
        }
    }

    private function parseData($worker)
    {
        foreach ($this->getWorker('data')->build() as $key => $value) {
            $worker->set('data', $key, $value);
        }
    }
}
