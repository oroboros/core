<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\action;

/**
 * Abstract Action Manager
 * Provides abstraction for managing actions
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
abstract class AbstractActionManager extends \Oroboros\core\abstracts\pattern\director\AbstractDirector implements \Oroboros\core\interfaces\library\action\ActionManagerInterface
{

    use \Oroboros\core\traits\ContainerPackagerUtility;
    use \Oroboros\core\traits\LoaderTrait;
    use \Oroboros\core\traits\pattern\ObserverTrait;
    use \Oroboros\core\traits\pattern\ObservableTrait;

    /**
     * Designates the action manager as a library
     */
    const CLASS_TYPE = 'library';

    /**
     * Designates the class scope as an action manager.
     */
    const CLASS_SCOPE = 'action-manager';

    /**
     * Use the default action queue
     */
    const CONTAINER_CLASS = 'Oroboros\\core\\library\\action\\ActionQueue';

    /**
     * Use the default action queue index
     */
    const COLLECTION_CLASS = 'Oroboros\\core\\library\\action\\ActionQueueIndex';

    /**
     * Default action priority for the observer binding.
     * @type int
     */
    const DEFAULT_PRIORITY = 1024; // most urgent: 0, least urgent: 4294967295

    /**
     * Child classes must provide an action group.
     */
    const ACTION_GROUP = null;

    /**
     * Override in child classes to declare the action name for the given action.
     * 
     * For single-scope actions, set the class constant `ACTION_NAME` to a
     * fixed value, and any instances of that action will always use that name.
     * 
     * Otherwise actions may be arbitrarily named when constucted based on
     * their constructor parameter `$command`
     * 
     * It is not required to provide a name in either case unless tighter
     * scoping is required for the given action.
     */
    const ACTION_NAME = null;

    /**
     * Child classes must provide a worker class.
     */
    const WORKER_CLASS = null;

    /**
     * Scopes worker classes to actions.
     */
    const WORKER_SCOPE = AbstractAction::WORKER_SCOPE;

    /**
     * The default bindings for the spl observer that
     * makes the action system work correctly.
     * 
     * You may add additional bindings, but do not remove these.
     * 
     * @var array
     */
    private static $default_action_observer_bindings = [
        'bind' => 'onActionRegister',
        'unbind' => 'onActionUnregister',
        'run' => 'onActionRun',
        'error' => 'onActionError',
        'complete' => 'onActionComplete',
        'ready' => 'onActionReady',
        'run-callback' => 'onActionCallbackRun',
        'callback-error' => 'onActionCallbackError',
        'callback-complete' => 'onActionCallbackComplete',
        'bind-callback' => 'onActionCallbackBind',
        'unbind-callback' => 'onActionCallbackUnbind',
    ];

    /**
     * Designates which spl objects are declared valid to bypass scope lock
     * 
     * @var array
     */
    private static $allowed_action_spl_objects = [
        \Oroboros\core\interfaces\library\action\ActionInterface::class,
        \Oroboros\core\interfaces\library\action\ActionAwareInterface::class,
        \Oroboros\core\interfaces\library\action\ActionListenerInterface::class,
        \Oroboros\core\interfaces\library\action\ActionWatcherInterface::class,
    ];

    /**
     * Represents the subject used to relay with for actions that fire.
     * 
     * @var \Oroboros\core\interfaces\library\action\ActionAwareInterface
     */
    private $subject = null;

    /**
     * ------------------------------------------------------------------
     * Queue Reporting Properties
     * ------------------------------------------------------------------
     */

    /**
     * The identifier of queue currently being worked during an action run, or null
     * @var string
     */
    private $queued_queue = null;

    /**
     * The action being currently executed, or null
     * @var \Oroboros\core\interfaces\action\ActionInterface
     */
    private $queued_action = null;

    /**
     * The identifying key of the callback being currently executed, or null
     * @var type
     */
    private $queued_callback_id = null;

    /**
     * The actual callable being currently executed, or null
     * @var callable
     */
    private $queued_callback = null;

    /**
     * The id of the queued action currently being run, or null
     * @var string
     */
    private $queued_id = null;

    /**
     * The data passed to callback being currently run, or null
     * @var array
     */
    private $queued_data = null;

    /**
     * Any exceptions thrown by the callback, or null
     * @var \Exception
     */
    private $queued_error = null;

    /**
     * The result of the executed actions during an action callback chain, or null
     * @var mixed
     */
    private $queued_result = null;

    /**
     * The result of the executed callback during a callback execution, or null
     * @var mixed
     */
    private $queued_callback_result = null;

    /**
     * The collection used to manage individual action queue entries.
     * 
     * @var \Oroboros\core\interfaces\library\container\CollectionInterface
     */
    private $queue_collection = null;

    /**
     * If true, designates that the currently running action's
     * error state should be recoverable.
     * 
     * This value will always be marked false again after applied.
     * 
     * @var bool
     */
    private $queued_error_is_recoverable = false;

    /**
     * If true, designates that the currently running action was resolved,
     * and it can safely exit a pool without further callbacks running.
     * 
     * This value will always be marked false again after applied.
     * 
     * @var bool
     */
    private $queued_action_is_resolved = false;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        $this->verifyWorkerType();
        $this->verifyActionType();
        parent::__construct($command, $arguments, $flags);
        $this->initializeObserver();
        $this->initializeObservable();
        $this->initializeCollection(static::COLLECTION_CLASS);
        $this->initializeManager();
        $this->bindActionObserverEvents();
    }

    /**
     * Filter for handleble SplSubjects
     * 
     * This class understands bindings for the following subjects:
     * 
     * `\Oroboros\core\interfaces\action\ActionInterface`
     * For interfacing with firing actions
     * 
     * `\Oroboros\core\interfaces\action\ActionAwareInterface`
     * For relaying information with a controlling object
     * 
     * `\Oroboros\core\interfaces\action\ActionWatcherInterface`
     * For injecting callbacks into registered actions
     * 
     * `\Oroboros\core\interfaces\action\ActionListenerInterface`
     * For binding external listeners to expected actions
     * 
     * Other instances are not known, and will raise an exception
     * 
     * @param \SplSubject $subject
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    public function update(\SplSubject $subject): void
    {
        $valid = false;
        foreach (self::$allowed_action_spl_objects as $allowed) {
            if ($subject instanceof $allowed) {
                $valid = true;
                break;
            }
        }
        if (!$valid) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided object of type [%2$s] must be an instance of one of [%3$s].'
                        , get_class($this), get_class($subject), implode(', ', self::$allowed_action_spl_objects)));
        }
        $event = $subject->getObserverEvent();
        if ($this->observer_event_bindings->has($event)) {
            // Handle standard event bindings
            call_user_func($this->observer_event_bindings->get($event), $subject);
        }
        if ($subject instanceof \Oroboros\core\interfaces\library\action\ActionInterface) {
            // Handle event bindings for named actions
            $this->splObserverUpdateAction($subject);
        }
        if ($subject instanceof \Oroboros\core\interfaces\library\action\ActionAwareInterface) {
            // Handle action aware bindings
            $this->splObserverUpdateActionAware($subject);
        }
        if ($subject instanceof \Oroboros\core\interfaces\library\action\ActionWatcherInterface) {
            // Handle action watchers
            $this->splObserverUpdateWatcher($subject);
        }
        if ($subject instanceof \Oroboros\core\interfaces\library\action\ActionListenerInterface) {
            // Handle action listeners
            $this->splObserverUpdateListener($subject);
        }
    }

    /**
     * Dependency injection method for the action subject that controls the
     * actions supplied by the manager. This method may only be called one time
     * per instantiation.
     * 
     * If a subject was already set, it will raise an error exception.
     * 
     * @param \Oroboros\core\interfaces\library\action\ActionAwareInterface $subject
     * @return void
     * @throws \Oroboros\core\exception\ErrorException
     */
    public function setSubject(\Oroboros\core\interfaces\library\action\ActionAwareInterface $subject): void
    {
        if (!is_null($this->subject)) {
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. Action subject has already been declared.', get_class($this))
            );
        }
        $this->attach($subject);
        $this->subject = $subject;
        $this->setObserverEvent('bind');
    }

    /**
     * Returns the manager action group name.
     * 
     * @return string
     */
    public function getGroup(): string
    {
        return static::ACTION_GROUP;
    }

    /**
     * Returns the manager action name, if any.
     * 
     * @return string|null
     */
    public function getName(): ?string
    {
        if (!is_null(static::ACTION_NAME)) {
            return static::ACTION_NAME;
        }
        return $this->getCommand();
    }

    /**
     * Returns the factorizable classname or stubname of acceptable 
     * action classes to enter into a queue.
     * 
     * @return string
     */
    public function getActionClass(): string
    {
        return static::WORKER_CLASS;
    }

    /**
     * Returns the observer priority
     * 
     * @return int
     */
    public function getObserverPriority(): int
    {
        return static::DEFAULT_PRIORITY;
    }

    /**
     * Observer event binding for action registration
     * 
     * @param boolean $skip_reset If true, tells the action manager not to
     *        trigger an event state reset. This will be used when this method
     *        is called internally, and not used when it is called externally
     *        by the observer. This insures that the expected scope is not altered
     *        regardless of whether the action is fired from the action object itself,
     *        or from a call to the action manager to execute it.
     * @param \Oroboros\core\interfaces\library\action\ActionInterface $subject
     * @return void
     */
    final public function onActionRegister(\Oroboros\core\interfaces\library\action\ActionInterface $subject, bool $skip_reset = false): void
    {
        if (!$subject === $this->getCurrentAction()) {
            // Action managers only allow registration from authorized sources.
            // The owner of the action manager object can register and unregister actions,
            // external entities can only register or unregister callbacks.
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. Provided subject of '
                        . 'class [%2$s] did not originate from this manager '
                        . 'class.', get_class($this), get_class($subject))
            );
        }
        $this->setCurrentQueue($this->lookupActionQueue($subject));
        $this->setCurrentActionId($this->lookupActionId($this->getCurrentQueue(), $subject));
        $this->handleActionRegister(
            $this->getCurrentQueue(),
            $this->getCurrentActionId(),
            $subject
        );
        $this->setObserverEvent('register');
    }

    /**
     * Observer event binding for action unregistration
     * 
     * @param \Oroboros\core\interfaces\library\action\ActionInterface $subject
     * @param boolean $skip_reset If true, tells the action manager not to
     *        trigger an event state reset. This will be used when this method
     *        is called internally, and not used when it is called externally
     *        by the observer. This insures that the expected scope is not altered
     *        regardless of whether the action is fired from the action object itself,
     *        or from a call to the action manager to execute it.
     * @return void
     */
    final public function onActionUnregister(\Oroboros\core\interfaces\library\action\ActionInterface $subject, bool $skip_reset = false): void
    {
        if (!$subject === $this->getCurrentAction()) {
            // Action managers only allow registration from authorized sources.
            // The owner of the action manager object can register and unregister actions,
            // external entities can only register or unregister callbacks.
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. Provided subject of '
                        . 'class [%2$s] did not originate from this manager '
                        . 'class.', get_class($this), get_class($subject))
            );
        }
        $this->setCurrentQueue($this->lookupActionQueue($subject));
        $this->setCurrentActionId($this->lookupActionId($this->getCurrentQueue(), $subject));
        $this->handleActionUnregister(
            $this->getCurrentQueue(),
            $this->getCurrentActionId(),
            $subject
        );
        $this->setObserverEvent('unregister');
    }

    /**
     * Observer event binding for action execution
     * 
     * @param \Oroboros\core\interfaces\library\action\ActionInterface $subject
     * @param boolean $skip_reset If true, tells the action manager not to
     *        trigger an event state reset. This will be used when this method
     *        is called internally, and not used when it is called externally
     *        by the observer. This insures that the expected scope is not altered
     *        regardless of whether the action is fired from the action object itself,
     *        or from a call to the action manager to execute it.
     * @return void
     */
    final public function onActionRun(\Oroboros\core\interfaces\library\action\ActionInterface $subject, bool $skip_reset = false): void
    {
        $this->resetCurrent();
        $this->setCurrentQueue($this->lookupActionQueue($subject));
        $this->setCurrentActionId($this->lookupActionId($this->getCurrentQueue(), $subject));
        $this->setCurrentAction($subject);
        $this->setCurrentData($subject->getCurrentActionArguments());
        $this->setCurrentResult($subject->getCurrentActionResults());
        $this->handleActionRun(
            $this->getCurrentQueue(),
            $this->getCurrentActionId(),
            $subject->getCurrentActionResults(),
            $subject
        );
        $key = trim(sprintf('%1$s-%2$s', static::WORKER_SCOPE, 'run'), '.');
        $this->setObserverEvent($key);
    }

    /**
     * Observer event binding for action error status
     * 
     * @param \Oroboros\core\interfaces\library\action\ActionInterface $subject
     * @param boolean $skip_reset If true, tells the action manager not to
     *        trigger an event state reset. This will be used when this method
     *        is called internally, and not used when it is called externally
     *        by the observer. This insures that the expected scope is not altered
     *        regardless of whether the action is fired from the action object itself,
     *        or from a call to the action manager to execute it.
     * @return void
     */
    final public function onActionError(\Oroboros\core\interfaces\library\action\ActionInterface $subject, bool $skip_reset = false): void
    {
        $this->resetCurrentCallbackResult();
        $this->setCurrentQueue($this->lookupActionQueue($subject));
        $this->setCurrentActionId($this->lookupActionId($this->getCurrentQueue(), $subject));
        $this->setCurrentAction($subject);
        $this->setCurrentError($subject->getCurrentActionCallbackError());
        $this->handleActionError(
            $this->getCurrentQueue(),
            $this->getCurrentActionId(),
            $subject->getCurrentActionCallbackId(),
            $subject->getCurrentActionError(),
            $subject->getCurrentActionResults(),
            $subject->getCurrentActionCallback(),
            $subject
        );
        if (!$subject->isErrorRecoverable()) {
            $key = trim(sprintf('%1$s-%2$s', static::WORKER_SCOPE, 'error'), '.');
            $this->setObserverEvent($key);
        }
    }

    /**
     * Observer event binding for action that has completed running successfully.
     * This fires immediately prior to resettting.
     * 
     * @param \Oroboros\core\interfaces\library\action\ActionInterface $subject
     * @param boolean $skip_reset If true, tells the action manager not to
     *        trigger an event state reset. This will be used when this method
     *        is called internally, and not used when it is called externally
     *        by the observer. This insures that the expected scope is not altered
     *        regardless of whether the action is fired from the action object itself,
     *        or from a call to the action manager to execute it.
     * @return void
     */
    final public function onActionComplete(\Oroboros\core\interfaces\library\action\ActionInterface $subject, bool $skip_reset = false): void
    {
        if (is_null($this->queued_queue)) {
            $this->setCurrentQueue($this->lookupActionQueue($subject));
        }
        $this->setCurrentActionId($this->lookupActionId($this->getCurrentQueue(), $subject));
        $this->setCurrentAction($subject);
        $this->resetCurrentError();
        $details = array_replace_recursive([
            'queue' => $this->getCurrentQueue(),
            'action-id' => $this->getCurrentActionId(),
            ], $subject->getCurrentAction());
        $this->handleActionComplete(
            $details,
            $subject
        );
        $key = trim(sprintf('%1$s-%2$s', static::WORKER_SCOPE, 'complete'), '.');
        $this->setObserverEvent($key);
    }

    /**
     * Observer event binding for completion of an action action. This method
     * will only be called when the action manager returns to a clean state.
     * 
     * @param \Oroboros\core\interfaces\library\action\ActionInterface $subject
     * @param boolean $skip_reset If true, tells the action manager not to
     *        trigger an event state reset. This will be used when this method
     *        is called internally, and not used when it is called externally
     *        by the observer. This insures that the expected scope is not altered
     *        regardless of whether the action is fired from the action object itself,
     *        or from a call to the action manager to execute it.
     * @return void
     */
    final public function onActionReady(\Oroboros\core\interfaces\library\action\ActionInterface $subject, bool $skip_reset = false): void
    {
        $this->handleActionReady($subject);
        $key = trim(sprintf('%1$s-%2$s', static::WORKER_SCOPE, 'ready'), '.');
        $this->setObserverEvent($key);
    }

    /**
     * Observer event binding for action callback execution
     * 
     * @param \Oroboros\core\interfaces\library\action\ActionInterface $subject
     * @param boolean $skip_reset If true, tells the action manager not to
     *        trigger an event state reset. This will be used when this method
     *        is called internally, and not used when it is called externally
     *        by the observer. This insures that the expected scope is not altered
     *        regardless of whether the action is fired from the action object itself,
     *        or from a call to the action manager to execute it.
     * @return void
     */
    final public function onActionCallbackRun(\Oroboros\core\interfaces\library\action\ActionInterface $subject, bool $skip_reset = false): void
    {
        if (is_null($this->queued_queue)) {
            $this->setCurrentQueue($this->lookupActionQueue($subject));
        }
        $this->setCurrentActionId($this->lookupActionId($this->getCurrentQueue(), $subject));
        $this->setCurrentAction($subject);
        $this->setCurrentCallbackId($subject->getCurrentActionCallbackId());
        $this->setCurrentCallback($subject->getCurrentActionCallback());
        $this->resetCurrentError();
        $this->resetCurrentCallbackResult();
        $result = $subject->getCurrentActionResults();
        if (is_null($result) && is_null($this->queued_result)) {
            $this->setCurrentCallbackResult([]);
        }
        if (is_null($result)) {
            $result = $this->getCurrentCallbackResult();
        }
        $this->setCurrentResult($result);
        $this->handleActionCallbackRun(
            $this->getCurrentQueue(),
            $this->getCurrentActionId(),
            $subject->getCurrentActionCallbackId(),
            $subject->getCurrentActionCallbackArguments(),
            $subject->getCurrentActionCallback(),
            $subject
        );
        $key = trim(sprintf('%1$s-%2$s', static::WORKER_SCOPE, 'run-callback'), '.');
        $this->setObserverEvent($key);
    }

    /**
     * Observer event binding for action callback error status
     * 
     * @param \Oroboros\core\interfaces\library\action\ActionInterface $subject
     * @param boolean $skip_reset If true, tells the action manager not to
     *        trigger an event state reset. This will be used when this method
     *        is called internally, and not used when it is called externally
     *        by the observer. This insures that the expected scope is not altered
     *        regardless of whether the action is fired from the action object itself,
     *        or from a call to the action manager to execute it.
     * @return void
     */
    final public function onActionCallbackError(\Oroboros\core\interfaces\library\action\ActionInterface $subject, bool $skip_reset = false): void
    {
        if (is_null($this->queued_queue)) {
            $this->setCurrentQueue($this->lookupActionQueue($subject));
        }
        $this->setCurrentActionId($this->lookupActionId($this->getCurrentQueue(), $subject));
        $this->setCurrentAction($subject);
        $this->handleActionCallbackError(
            $this->getCurrentQueue(),
            $this->getCurrentActionId(),
            $subject->getCurrentActionCallbackId(),
            $subject->getCurrentActionCallbackError(),
            $subject->getCurrentActionCallbackArguments(),
            $subject->getCurrentActionCallback(),
            $subject
        );
        $key = trim(sprintf('%1$s-%2$s', static::WORKER_SCOPE, 'callback-error'), '.');
        $this->setObserverEvent($key);
    }

    /**
     * Observer event binding for action callback error status
     * 
     * @param \Oroboros\core\interfaces\library\action\ActionInterface $subject
     * @param boolean $skip_reset If true, tells the action manager not to
     *        trigger an event state reset. This will be used when this method
     *        is called internally, and not used when it is called externally
     *        by the observer. This insures that the expected scope is not altered
     *        regardless of whether the action is fired from the action object itself,
     *        or from a call to the action manager to execute it.
     * @return void
     */
    final public function onActionCallbackComplete(\Oroboros\core\interfaces\library\action\ActionInterface $subject, bool $skip_reset = false): void
    {
        if (is_null($this->queued_queue)) {
            $this->setCurrentQueue($this->lookupActionQueue($subject));
        }
        $this->setCurrentActionId($this->lookupActionId($this->getCurrentQueue(), $subject));
        $this->setCurrentAction($subject);
        $this->handleActionCallbackComplete(
            $this->getCurrentQueue(),
            $this->getCurrentActionId(),
            $subject->getCurrentActionCallbackId(),
            $subject->getCurrentActionResults(),
            $subject->getCurrentActionCallbackResults(),
            $subject->getCurrentActionCallback(),
            $subject
        );
        $key = trim(sprintf('%1$s-%2$s', static::WORKER_SCOPE, 'callback-complete'), '.');
        $this->setObserverEvent($key);
    }

    /**
     * Observer event binding for action callback binding
     * 
     * @param \Oroboros\core\interfaces\library\action\ActionInterface $subject
     * @param boolean $skip_reset If true, tells the action manager not to
     *        trigger an event state reset. This will be used when this method
     *        is called internally, and not used when it is called externally
     *        by the observer. This insures that the expected scope is not altered
     *        regardless of whether the action is fired from the action object itself,
     *        or from a call to the action manager to execute it.
     * @return void
     */
    final public function onActionCallbackBind(\Oroboros\core\interfaces\library\action\ActionInterface $subject, bool $skip_reset = false): void
    {
        if (is_null($this->queued_queue)) {
            $this->setCurrentQueue($this->lookupActionQueue($subject));
        }
        $this->setCurrentActionId($this->lookupActionId($this->getCurrentQueue(), $subject));
        $this->setCurrentAction($subject);
        $this->handleActionCallbackBind(
            $this->getCurrentQueue(),
            $this->getCurrentActionId(),
            $subject->getCurrentActionCallbackId(),
            $subject->getCurrentActionCallback(),
            $subject
        );
        $key = trim(sprintf('%1$s-%2$s', static::WORKER_SCOPE, 'bind-callback'), '.');
        $this->setObserverEvent($key);
    }

    /**
     * Observer event binding for action callback unbinding
     * 
     * @param \Oroboros\core\interfaces\library\action\ActionInterface $subject
     * @param boolean $skip_reset If true, tells the action manager not to
     *        trigger an event state reset. This will be used when this method
     *        is called internally, and not used when it is called externally
     *        by the observer. This insures that the expected scope is not altered
     *        regardless of whether the action is fired from the action object itself,
     *        or from a call to the action manager to execute it.
     * @return void
     */
    final public function onActionCallbackUnbind(\Oroboros\core\interfaces\library\action\ActionInterface $subject, bool $skip_reset = false): void
    {
        if (is_null($this->queued_queue)) {
            $this->setCurrentQueue($this->lookupActionQueue($subject));
        }
        $this->setCurrentActionId($this->lookupActionId($this->getCurrentQueue(), $subject));
        $this->setCurrentAction($subject);
        $this->handleActionCallbackUnbind(
            $this->getCurrentQueue(),
            $this->getCurrentActionId(),
            $subject->getCurrentActionCallbackId(),
            $subject->getCurrentActionCallback(),
            $subject
        );
        $key = trim(sprintf('%1$s-%2$s', static::WORKER_SCOPE, 'unbind-callback'), '.');
        $this->setObserverEvent($key);
    }

    /**
     * Queues a new action in the specified action queue.
     * @param string $queue
     * @param array $details
     * @return $this (method chainable)
     * @throws \Oroboros\core\exception\InvalidArgumentException If the given queue is not valid
     */
    public function registerAction(string $queue, string $key): \Oroboros\core\interfaces\library\action\ActionManagerInterface
    {
        if (!$this->hasQueue($queue)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided queue [%2$s] does not exist.', get_class($this), $queue));
        }
        if ($this->hasAction($queue, $key)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided action [%2$s] already exists in queue [%3$s].', get_class($this), $key, $queue));
        }
        $this->resetCurrent();
        $queue_object = $this->getQueue($queue);
        $action = $this->spawnAction($key);
        $this->setCurrentQueue($queue);
        $this->setCurrentActionId($key);
        $this->setCurrentAction($action);
        $queue_object[$key] = $action;
        $action->bind($this);
        $this->setObserverEvent('complete');
        $this->resetCurrent();
        $this->setObserverEvent('ready');
        return $this;
    }

    /**
     * Unregisters an action. All callbacks registered to run within the action
     * will be removed.
     * 
     * @param string $queue
     * @param string $key
     * @param boolean $skip_reset Used internally to avoid resetting event queue parameters
     * @return \Oroboros\core\interfaces\library\action\ActionManagerInterface
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    public function unregisterAction(string $queue, string $key, bool $skip_reset = false): \Oroboros\core\interfaces\library\action\ActionManagerInterface
    {
        if (!$this->hasQueue($queue)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. '
                        . 'Provided queue [%2$s] does not exist.'
                        , get_class($this), $queue));
        }
        if (!$this->hasAction($queue, $key)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. '
                        . 'Provided action [%2$s] does not '
                        . 'exist in queue [%3$s].'
                        , get_class($this), $key, $queue));
        }
        if (!$skip_reset) {
            $this->resetCurrent();
        }
        $action_queue = $this->getQueue($queue);
        $action = $action_queue[$key];
        $this->setCurrentQueue($queue);
        $this->setCurrentActionId($key);
        $this->setCurrentAction($action);
        $this->setObserverEvent('unregister');
        $action->unbind($this);
        $action_queue->offsetUnset($key);
        $this->setObserverEvent('complete');
        if (!$skip_reset) {
            $this->resetCurrent();
            $this->setObserverEvent('ready');
        }
        return $this;
    }

    /**
     * Lists the actions registered within a queue, if it is possible to do so
     * @param string $queue
     * @return array
     * @throws \Oroboros\core\exception\InvalidArgumentException If the given queue is not valid
     */
    public function listActions(string $queue): array
    {
        if (!$this->hasQueue($queue)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. '
                        . 'Provided queue [%2$s] does '
                        . 'not exist.', get_class($this), $queue));
        }
        return array_keys($this->getQueue($queue)->toArray());
    }

    /**
     * Returns a boolean designation as to whether a given queue has
     * a registered action for it of a specific key.
     * 
     * Returns false if the queue does not exist or if the action does not have
     * a bound callback within the queue for the specified key.
     * 
     * @param string $queue
     * @param string $key
     * @return bool
     */
    public function hasAction(string $queue, string $key): bool
    {
        if (!$this->hasQueue($queue)) {
            return false;
        }
        return $this->getQueue($queue)->has($key);
    }

    /**
     * Runs an entire action queue. All callbacks within the action
     * will be executed with the given arguments.
     * 
     * The return value of this method is intended to be determined by extension.
     * By default, an associative array of each callback return value will be
     * returned at the end of the execution. This may be further manipulated or
     * altered by child class implementation.
     * 
     * @param string $queue The queue containing the action to run
     * @param string $action The action to run
     * @param type $arguments Any arguments to pass to the action
     * @return type
     * @throws \Oroboros\core\exception\InvalidArgumentException If the specified
     *         queue does not exist, or the action does not exist in the specified
     *         queue
     */
    public function runAction(string $queue, string $action, $arguments = null)
    {
        if (!$this->hasQueue($queue)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. '
                        . 'Provided queue [%2$s] does not '
                        . 'exist.', get_class($this), $queue));
        }
        if (!$this->hasAction($queue, $action)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. '
                        . 'Provided queue [%2$s] does not have an action '
                        . 'registered to key [%3$s].', get_class($this), $queue, $action)
            );
        }
        $this->resetCurrent();
        $result = [];
        $action_object = $this->getQueue($queue)[$action];
        $this->setCurrentQueue($queue);
        $this->setCurrentData($arguments);
        $this->setCurrentActionId($action);
        $this->setCurrentAction($action_object);
        $this->setObserverEvent('run');
        try {
            $result = $action_object->run($arguments);
            $this->setCurrentResult($result);
        } catch (\Oroboros\core\exception\core\InvalidClassException $e) {
            // These only get caught by the bootstrap
            throw $e;
        } catch (\Oroboros\core\exception\ErrorException $e) {
            // These are already handled,
            if (!$this->isErrorRecoverable()) {
                $result = $action_object->getCurrentActionResults();
                $this->setCurrentResult($result);
                throw $e;
            }
        } catch (\Exception $e) {
            // Handle all other exceptions
            $this->resetCurrentCallbackResult();
            $this->setCurrentCallbackId($action_object->getCurrentActionCallbackId());
            $this->setCurrentCallback($action_object->getCurrentActionCallback());
            $this->setCurrentError($e);
            $this->setObserverEvent('error');
            if (!$this->isErrorRecoverable()) {
                // Error may be made recoverable during
                // the underlying error observer event
                throw new \Oroboros\core\exception\ErrorException(
                        sprintf('Error encountered in [%1$s]. Specified callback '
                            . '[%2$s] of action [%3$s] in queue [%4$s] generated '
                            . 'the following error: [%5$s].'
                            , get_class($this), $callback, $action, $queue, $e->getMessage()
                        ), $e->getCode(), E_ERROR, __FILE__, __LINE__, $e);
            }
            $result = $action_object->getCurrentActionResults();
            $this->setCurrentResult($result);
        }
        $this->setObserverEvent('complete');
        $this->resetCurrent();
        $this->setObserverEvent('ready');
        return $result;
    }

    /**
     * Runs a specific action callback with the given arguments.
     * 
     * @param string $queue The queue containing the action
     * @param string $action The action containing the callback
     * @param string $key The callback identifier
     * @param array $arguments Arguments to pass to the callback (uses `call_user_func_array`)
     * @param boolean $skip_reset Used internally to avoid resetting event queue parameters
     * @return mixed Child implementations will determine the output, if any
     * @throws \Oroboros\core\exception\InvalidArgumentException
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    public function runActionCallback(string $queue, string $action, string $key, array $arguments = [], bool $skip_reset = false)
    {
        if (!$this->hasQueue($queue)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. '
                        . 'Provided queue [%2$s] does not '
                        . 'exist.', get_class($this), $queue));
        }
        if (!$this->hasAction($queue, $action)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. '
                        . 'Provided queue [%2$s] does not have an action '
                        . 'registered to key [%3$s].', get_class($this), $queue, $action)
            );
        }
        if (!$this->hasActionCallback($queue, $action, $key)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. '
                        . 'Provided callback key [%2$s] does not exist in action '
                        . '[%3$s] of queue [%4$s].', get_class($this), $key, $action, $queue)
            );
        }
        $action_object = $this->getQueue($queue)[$action];
        if (!$skip_reset) {
            $this->resetCurrent();
            $this->setCurrentQueue($queue);
            $this->setCurrentData($arguments);
            $this->setCurrentActionId($action);
            $this->setCurrentAction($action_object);
        }
        $this->setCurrentCallbackId($key);
        $this->setCurrentCallback($action_object->getCallback($key));
        $this->setObserverEvent('run-callback');
        try {
            $callback_result = $action_object->runCallback($key, $arguments);
            $this->setCurrentCallbackResult($callback_result);
        } catch (\Oroboros\core\exception\core\InvalidClassException $e) {
            throw $e;
        } catch (\Exception $e) {
            $this->resetCurrentCallbackResult();
            $this->setCurrentError($e);
            $this->setObserverEvent('callback-error');
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. Specified callback '
                        . '[%2$s] of action [%3$s] in queue [%4$s] generated '
                        . 'the following error: [%5$s].'
                        , get_class($this), $key, $action, $queue, $e->getMessage()
                    ), $e->getCode(), E_ERROR, __FILE__, __LINE__, $e);
        }
        $this->setObserverEvent('callback-complete');
        if (!$skip_reset) {
            $this->resetCurrent();
            $this->setObserverEvent('ready');
        }
        return $callback_result;
    }

    /**
     * Binds a callback into the given action of the given queue,
     * which will run whenever the action fires.
     * 
     * This is the method external objects should use to hook into actions
     * defined by the subject.
     * 
     * If the given queue does not exist, or the given action does not exist,
     * an error exception will be raised.
     * 
     * An error exception will also be raised if the given key already has
     * an associated callback assigned for it.
     * 
     * @param string $queue The name of the queue
     * @param string $action The name of the action to bind to
     * @param string $key The identifier for the callback
     * @param callable $callback The operation to run when the action fires
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    public function bindActionCallback(string $queue, string $action, string $key, callable $callback): void
    {
        if (!$this->hasQueue($queue)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. '
                        . 'Provided queue [%2$s] does not exist '
                        . 'in this scope.', get_class($this), $queue)
            );
        }
        if (!$this->hasAction($queue, $action)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. '
                        . 'Provided queue [%2$s] does not have an action '
                        . 'registered to key [%3$s].', get_class($this), $queue, $action)
            );
        }
        if ($this->hasActionCallback($queue, $action, $key)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. '
                        . 'Provided callback key [%2$s] already exists in action '
                        . '[%3$s] of queue [%4$s].', get_class($this), $key, $action, $queue)
            );
        }
        $this->resetCurrent();
        $action_object = $this->getQueue($queue)[$action];
        $this->setCurrentQueue($queue);
        $this->setCurrentAction($action_object);
        $this->setCurrentActionId($action);
        $this->setCurrentCallbackId($key);
        $this->setCurrentCallback($callback);
        $this->setObserverEvent('bind-callback');
        $action_object->registerCallback($key, $callback);
        $this->setObserverEvent('complete');
        $this->resetCurrent();
        $this->setObserverEvent('ready');
    }

    /**
     * Unbinds an existing callback registered to the specified action queue
     * under the given keyname. This will cause that callback to no longer
     * fire when the queue is run.
     * 
     * This will raise an exception if the queue does not exist, or if there
     * is no callback registered within it to the given key.
     * 
     * @param string $queue The name of the queue
     * @param string $action The name of the action the callback is bound to
     * @param string $key The identifier of the callback to remove
     * @param boolean $skip_reset Used internally to avoid resetting event queue parameters
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    public function unbindActionCallback(string $queue, string $action, string $key, bool $skip_reset = false): void
    {
        if (!$this->hasQueue($queue)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. '
                        . 'Provided queue [%2$s] does not exist '
                        . 'in this scope.', get_class($this), $queue)
            );
        }
        if (!$this->hasAction($queue, $action)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. '
                        . 'Provided queue [%2$s] does not have an action '
                        . 'registered to key [%3$s] to remove.', get_class($this), $queue, $action)
            );
        }
        if (!$this->hasActionCallback($queue, $action, $key)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. '
                        . 'Provided callback key [%2$s] does not exist in action '
                        . '[%3$s] of queue [%4$s].', get_class($this), $key, $action, $queue)
            );
        }
        if (!$skip_reset) {
            $this->resetCurrent();
        }
        $action_object = $this->getQueue($queue)[$action];
        $this->setCurrentQueue($queue);
        $this->setCurrentActionId($action);
        $this->setCurrentAction($action_object);
        $this->setCurrentCallbackId($key);
        $this->setCurrentCallback($action_object->getCallback($key));
        $this->setObserverEvent('unbind-callback');
        $action_object->unregisterCallback($key);
        $this->setObserverEvent('complete');
        if (!$skip_reset) {
            $this->resetCurrent();
            $this->setObserverEvent('ready');
        }
    }

    /**
     * Returns a boolean designation as to whether a callback exists with
     * a given keyname in the specified action of the specified queue.
     * 
     * Returns false if the queue, $action, or callback key do not exist.
     * Returns true if all three exist.
     * 
     * @param string $queue
     * @param string $action
     * @param string $key
     * @return bool
     */
    public function hasActionCallback(string $queue, string $action, string $key): bool
    {
        if (!$this->hasQueue($queue)) {
            return false;
        }
        if (!$this->hasAction($queue, $action)) {
            return false;
        }
        $action_object = $this->getQueueCollection()[$queue][$action];
        return $this->getQueueCollection()[$queue][$action]->hasCallback($key);
    }

    /**
     * Returns an array of callback key names registered into the given action
     * and queue specified.
     * 
     * Will throw an invalid argument exception if either the queue
     * or action specified do not exist.
     * 
     * @param string $queue
     * @param string $action
     * @return array
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    public function listActionCallbacks(string $queue, string $action): array
    {
        if (!$this->hasQueue($queue)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. '
                        . 'Provided queue [%2$s] does not exist '
                        . 'in this scope.', get_class($this), $queue)
            );
        }
        if (!$this->hasAction($queue, $action)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. '
                        . 'Provided queue [%2$s] does not have an action '
                        . 'registered to key [%3$s] to remove.', get_class($this), $queue, $action)
            );
        }
        return $this->getQueue($queue)[$action]->listCallbacks();
    }

    /**
     * Returns a ActionQueue object corresponding to the specified queue identifier
     * @param string $queue
     * @return \Oroboros\core\interfaces\library\action\ActionQueueInterface
     * @throws \Oroboros\core\exception\InvalidArgumentException If the given queue is not valid
     */
    public function getQueue(string $queue): \Oroboros\core\interfaces\library\action\ActionQueueInterface
    {
        if (!$this->hasQueue($queue)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. '
                        . 'Provided queue [%2$s] does not '
                        . 'exist.', get_class($this), $queue));
        }
        return $this->getQueueCollection()[$queue];
    }

    /**
     * Returns a boolean determination as to whether a given queue is valid
     * 
     * @param string $queue
     * @return bool
     */
    public function hasQueue(string $queue): bool
    {
        return $this->getQueueCollection()->has($queue);
    }

    /**
     * Registers an action queue into the master queue index.
     * 
     * @param string $queue
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    public function registerQueue(string $queue): void
    {
        if ($this->hasQueue($queue)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. '
                        . 'Provided queue [%2$s] already '
                        . 'exists.', get_class($this), $queue));
        }
        $this->resetCurrent();
        $this->setCurrentQueue($queue);
        $this->setObserverEvent('register-queue');
        $this->createQueueIndex($queue);
        $this->setObserverEvent('complete');
        $this->resetCurrent();
        $this->setObserverEvent('ready');
    }

    /**
     * Removes an action queue from the master queue index
     * 
     * @param string $queue
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    public function unregisterQueue(string $queue): void
    {
        if (!$this->hasQueue($queue)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. '
                        . 'Provided queue [%2$s] does not '
                        . 'exist.', get_class($this), $queue));
        }
        $this->resetCurrent();
        $this->setCurrentQueue($queue);
        $this->setObserverEvent('unregister-queue');
        $this->removeQueueIndex($queue);
        $this->resetCurrent();
        $this->setCurrentQueue($queue);
        $this->setObserverEvent('complete');
        $this->resetCurrent();
        $this->setObserverEvent('ready');
    }

    /**
     * Returns a list of the queues currently registered.
     * 
     * @return array
     */
    public function listQueues(): array
    {
        return array_keys($this->getQueueCollection()->toArray());
    }

    /**
     * Returns all applicable current observer event data
     * 
     * @return array
     */
    public function getCurrentDetails(): array
    {
        $details = [
            'name' => $this->getName(),
            'group' => $this->getGroup(),
            'queue' => $this->getCurrentQueue(),
            'id' => $this->getCurrentActionId(),
            'action' => $this->getCurrentAction(),
            'callback-name' => $this->getCurrentCallbackId(),
            'callback' => $this->getCurrentCallback(),
            'callback-result' => $this->getCurrentCallbackResult(),
            'data' => $this->getCurrentData(),
            'error' => $this->getCurrentError(),
            'result' => $this->getCurrentResult(),
        ];
        return $details;
    }

    /**
     * Returns the queue currently being worked, if called during an action
     * or callback event, otherwise null.
     * 
     * @return string|null
     */
    public function getCurrentQueue(): ?string
    {
        return $this->queued_queue;
    }

    /**
     * Returns the keyname of the current action being run if called during an
     * action or callback event, otherwise null
     * @return string|null
     */
    public function getCurrentActionId(): ?string
    {
        return $this->queued_id;
    }

    /**
     * Returns the current action being executed if called during an action
     * or callback event, otherwise null
     * 
     * @return \Oroboros\core\interfaces\library\action\ActionInterface|null
     */
    public function getCurrentAction(): ?\Oroboros\core\interfaces\library\action\ActionInterface
    {
        return $this->queued_action;
    }

    /**
     * Returns the keyname of the callback being currently fired if called
     * during an action or callback event, otherwise null
     * 
     * @return callable|null
     */
    public function getCurrentCallbackId(): ?string
    {
        return $this->queued_callback_id;
    }

    /**
     * Returns the current callback being executed if called during an action
     * or callback event, otherwise null
     * 
     * @return callable|null
     */
    public function getCurrentCallback(): ?callable
    {
        return $this->queued_callback;
    }

    /**
     * Returns the data supplied to pass to the currently executing action or
     * callback if called during an action or callback event, otherwise null
     * 
     * This will always be an array during an event, even if no data was passed.
     * 
     * @return array|null
     */
    public function getCurrentData(): ?array
    {
        return $this->queued_data;
    }

    /**
     * Returns the exception thrown by a callback or action when they are
     * running and throw any exception, otherwise null
     * 
     * @note any instance of `\Oroboros\core\exception\core\InvalidClassException`
     *       will be allowed to bubble up to the bootstrap object.
     * @return \Exception|null
     */
    public function getCurrentError(): ?\Exception
    {
        return $this->queued_error;
    }

    /**
     * Returns an array of the resulting values returned from each callback
     * that has run during an action event, or null if no action is running
     * 
     * @return array|null
     */
    public function getCurrentResult(): ?array
    {
        return $this->queued_result;
    }

    /**
     * Returns the value of the last successfully run callback if called during
     * an action or callback event, otherwise null.
     * 
     * @note either of those may also cause this to be null if
     *       the specific action or callback returns null or void.
     * @return array|null
     */
    public function getCurrentCallbackResult()
    {
        return $this->queued_callback_result;
    }

    /**
     * Marks a currently running action or callback error as recoverable,
     * which will surpress it bubbling up to the main thread.
     * 
     * Recoverable status is always reset to false after it applies
     * 
     * @return void
     */
    public function markErrorRecoverable(): void
    {
        $this->queued_error_is_recoverable = true;
        if (!is_null($this->queued_action)) {
            $this->queued_action->markErrorRecoverable();
            $this->resetRecoverable();
        }
    }

    /**
     * Returns a boolean determination as to whether a currently scoped action
     * or callback error is marked as resolved.
     * 
     * @return bool
     */
    public function isErrorRecoverable(): bool
    {
        if (!is_null($this->queued_action)) {
            $this->queued_error_is_recoverable = $this->queued_action->isErrorRecoverable();
        }
        return $this->queued_error_is_recoverable;
    }

    /**
     * Marks a currently running action as resolved. No further callbacks will
     * fire and it will return it's existing results immediately.
     * 
     * Resolved status is always reset immediately after it is applied
     * 
     * @return void
     */
    public function markActionResolved(): void
    {
        $this->queued_action_is_resolved = true;
        if (!is_null($this->queued_action)) {
            $this->queued_action->markResolved();
            $this->resetResolved();
        }
    }

    /**
     * Checks whether a given running action has been marked as resolved.
     * 
     * @return bool
     */
    public function isActionResolved(): bool
    {
        if (!is_null($this->queued_action)) {
            $this->queued_action_is_resolved = $this->queued_action->isResolved();
        }
        return $this->queued_action_is_resolved;
    }

    /**
     * Verifies that an action can be called for the given queue.
     * This will throw an exception if the queue/action is not valid,
     * but will not call any heavy operations during validation.
     * @param string $queue
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException If the given queue or worker is not valid
     */
    public function verify(string $queue): void
    {
        $this->getStrategy()->verifyWorker(static::JOB_TYPE, $queue);
    }

    /**
     * Spawns an action
     * @param string $key the keyname for the new action
     */
    protected function spawnAction(string $key): \Oroboros\core\interfaces\library\action\ActionInterface
    {
        $classname = $this->getActionClass();
        $action = $this->load('library', $classname, $key, $this->getArguments(), $this->getFlags());
        return $action;
    }

    /**
     * Getter method for the subject owning the action pool.
     * 
     * If no subject has been set, this will raise an error exception.
     * 
     * @return \Oroboros\core\interfaces\library\action\ActionAwareInterface
     * @throws \Oroboros\core\exception\ErrorException
     */
    protected function getSubject(): \Oroboros\core\interfaces\library\action\ActionAwareInterface
    {
        if (is_null($this->subject)) {
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. No subject has been defined.', get_class($this))
            );
        }
        return $this->subject;
    }

    /**
     * Override this method to provide initialization logic for the specific manager.
     * Any additional setup operations that should occur on instantiation should
     * be called from within this method.
     * @return void
     */
    protected function initializeManager(): void
    {
        // no-op
    }

    /**
     * Registers the observer action bindings for monitoring actions
     * @return array
     */
    protected function declareObserverActionBindings(): array
    {
        return self::$default_action_observer_bindings;
    }

    /**
     * Resets all current queue data after a queue finishes to it's default status
     * 
     * @return void
     */
    protected function resetCurrent(): void
    {
        $this->resetCurrentQueue();
        $this->resetCurrentData();
        $this->resetCurrentError();
        $this->resetCurrentActionId();
        $this->resetCurrentAction();
        $this->resetCurrentCallbackId();
        $this->resetCurrentCallback();
        $this->resetCurrentCallbackResult();
        $this->resetCurrentResult();
    }

    /**
     * Resets the queue currently being worked, if called during an action
     * or callback event.
     * 
     * @return void
     */
    protected function resetCurrentQueue(): void
    {
        $this->queued_queue = null;
    }

    /**
     * Resets the keyname of the current action being run if called during an
     * action or callback event
     * 
     * @return void
     */
    protected function resetCurrentActionId(): void
    {
        $this->queued_id = null;
    }

    /**
     * Resets the current action being executed if called during an action
     * or callback event
     * 
     * @return void
     */
    protected function resetCurrentAction(): void
    {
        $this->queued_action = null;
    }

    /**
     * Resets the keyname of the callback being currently fired if called
     * during an action or callback event
     * 
     * @return void
     */
    protected function resetCurrentCallbackId(): void
    {
        $this->queued_callback_id = null;
    }

    /**
     * Resets the current callback being executed if called during an action
     * or callback event
     * 
     * @return void
     */
    protected function resetCurrentCallback(): void
    {
        $this->queued_callback = null;
    }

    /**
     * Resets the data supplied to pass to the currently executing action or
     * callback if called during an action or callback event
     * 
     * This will always be an array during an event, even if no data was passed.
     * 
     * @return void
     */
    protected function resetCurrentData(): void
    {
        $this->queued_data = null;
    }

    /**
     * Resets the exception thrown by a callback or action when they are
     * running and throw any exception
     * 
     * @return void
     */
    protected function resetCurrentError(): void
    {
        $this->queued_error = null;
    }

    /**
     * Returns an array of the resulting values returned from each callback
     * that has run during an action event, or null if no action is running
     * 
     * @return void
     */
    protected function resetCurrentResult(): void
    {
        $this->queued_result = null;
    }

    /**
     * Resets the value of the last successfully run callback if called during
     * an action or callback event.
     * 
     * @return void
     */
    protected function resetCurrentCallbackResult(): void
    {
        $this->queued_callback_result = null;
    }

    /**
     * Resets recoverable status after it skips an exception re-throw
     * due to parent object intervention
     * 
     * @return void
     */
    protected function resetRecoverable(): void
    {
        $this->queued_error_is_recoverable = false;
    }

    /**
     * Resets resolved status after breaking an action callback chain because
     * the parent object has marked the task as resolved.
     * 
     * @return void
     */
    protected function resetResolved(): void
    {
        $this->queued_action_is_resolved = false;
    }

    /**
     * Sets the identifier of the queue currently being worked
     * 
     * @param string $queue
     * @return void
     */
    protected function setCurrentQueue(string $queue): void
    {
        $this->queued_queue = $queue;
    }

    /**
     * Sets the keyname of the current action being run
     * 
     * @param string $id
     * @return void
     */
    protected function setCurrentActionId(string $id): void
    {
        $this->queued_id = $id;
    }

    /**
     * Sets the current action being executed
     * 
     * @param \Oroboros\core\interfaces\library\action\ActionInterface $action
     * @return void
     */
    protected function setCurrentAction(\Oroboros\core\interfaces\library\action\ActionInterface $action): void
    {
        $this->queued_action = $action;
    }

    /**
     * Sets the keyname of the callback being currently executed
     * 
     * @param string $id
     * @return void
     */
    protected function setCurrentCallbackId(string $id): void
    {
        $this->queued_callback_id = $id;
    }

    /**
     * Sets the current callback being executed
     * 
     * @param callable $callback
     * @return void
     */
    protected function setCurrentCallback(callable $callback): void
    {
        $this->queued_callback = $callback;
    }

    /**
     * Sets the data supplied to pass to the currently executing action or
     * callback
     * 
     * @param array $data
     * @return void
     */
    protected function setCurrentData(array $data): void
    {
        $this->queued_data = $data;
    }

    /**
     * Sets the exception thrown by a callback or action when they are
     * running and throw any exception
     * 
     * @param \Exception $error
     * @return void
     */
    protected function setCurrentError(\Exception $error): void
    {
        $this->queued_error = $error;
    }

    /**
     * Sets the current working results of an action callback chain
     * 
     * @param array $result
     * @return void
     */
    protected function setCurrentResult(array $result): void
    {
        $this->queued_result = $result;
    }

    /**
     * Sets the current result of the last successfully ran callback
     * 
     * @param array $result
     * @return void
     */
    protected function setCurrentCallbackResult($result): void
    {
        $this->queued_callback_result = $result;
    }

    /**
     * Designates the observable name for the action manager.
     * 
     * @return string
     */
    protected function declareObservableName(): string
    {
        $group = $this->getActionGroup();
        $type = $this->getActionName();
        return trim(sprintf('%1$s:%2$s.%3$s', static::CLASS_SCOPE, $group, $type), '.');
    }

    /**
     * Returns the scoped action group, if one exists.
     * @return string|null
     */
    protected function getActionGroup(): ?string
    {
        return static::ACTION_GROUP;
    }

    /**
     * Returns the scoped action name, if one exists
     * @return string|null
     */
    protected function getActionName(): ?string
    {
        $name = trim(sprintf('%1$s.%2$s', static::WORKER_SCOPE, $this->getCommand()), '.');
        if ($name === '') {
            return null;
        }
        return $name;
    }

    /**
     * Gets the default action priority, if none is supplied
     * @return int
     */
    protected function getActionPriority(): int
    {
        return static::DEFAULT_PRIORITY;
    }

    /**
     * Getter for the master queue collection.
     * 
     * @return \Oroboros\core\interfaces\library\container\CollectionInterface
     */
    protected function getQueueCollection(): \Oroboros\core\interfaces\library\container\CollectionInterface
    {
        return $this->queue_collection;
    }

    /**
     * Returns a boolean designation as to whether the master queue
     * has a given action queue key.
     * 
     * @param string $key
     * @return bool
     */
    protected function hasQueueIndex(string $key): bool
    {
        return $this->getQueueCollection()->has($key);
    }

    /**
     * Creates a new action queue index in the master queue.
     * 
     * @param string $key
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException if the given queue key already exists.
     */
    protected function createQueueIndex(string $key): void
    {
        if ($this->hasQueueIndex($key)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Supplied action queue key '
                        . '[%2$s] already exists.', get_class($this), $key)
            );
        }
        $queue = $this->containerizeInto(static::CONTAINER_CLASS);
        $master = $this->getQueueCollection();
        $master[$key] = $queue;
    }

    /**
     * Removes a given action queue from the master queue index.
     * 
     * @param string $key
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException if the given queue key does not exist.
     */
    protected function removeQueueIndex(string $key): void
    {
        if (!$this->hasQueueIndex($key)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Supplied action queue key '
                        . '[%2$s] does not exist.', get_class($this), $key)
            );
        }
        $master = $this->getQueueCollection();
        foreach ($master[$key] as $id => $action) {
            $this->unregisterAction($key, $id);
        }
        unset($master[$key]);
    }
    /**
     * -------------------------------------------------------------------------
     * Observer Internal Bindings
     * 
     * These methods will be called when various event manager observer events
     * fire. They have been structured to provide the relevant data to each
     * particular case, and can be overridden to handle specifics on a case by
     * case basis.
     * -------------------------------------------------------------------------
     */

    /**
     * Observer event binding for action registration
     * 
     * This can be overridden if you want to track action registration and
     * perform additional steps, such as importing callbacks into it.
     * 
     * Default behavior is to do nothing.
     * 
     * @param string $queue The name of the queue an action was entered into
     * @param string $id The name of the action entered into the queue
     * @param \Oroboros\core\interfaces\library\action\ActionInterface $action
     *        The action object that triggered the event
     * @return void
     */
    protected function handleActionRegister(string $queue, string $id, \Oroboros\core\interfaces\library\action\ActionInterface $action): void
    {
        // no-op
    }

    /**
     * Observer event binding for action unregistration
     * 
     * This can be overridden if you want to track when entire actions are
     * removed and perform additional steps.
     * 
     * Default behavior is to do nothing.
     * 
     * @param string $queue The name of the queue an action was removed from
     * @param string $id The name of the action removed from the queue
     * @param \Oroboros\core\interfaces\library\action\ActionInterface $action
     *        The action object that triggered the event
     * @return void
     */
    protected function handleActionUnregister(string $queue, string $id, \Oroboros\core\interfaces\library\action\ActionInterface $action): void
    {
        // no-op
    }

    /**
     * Observer event binding for action execution
     * 
     * This can be overridden if you want to track when specific actions fire
     * and perform additional steps prior to any callbacks running.
     * 
     * Default behavior is to do nothing.
     * 
     * @param string $queue The queue containing the currently firing action
     * @param string $action_id The identifier for the currently firing action
     * @param array $data The data passed to the action when it was called
     * @param \Oroboros\core\interfaces\library\action\ActionInterface $action
     *        The action object that triggered the event
     * @return void
     */
    protected function handleActionRun(string $queue, string $action_id, array $data, \Oroboros\core\interfaces\library\action\ActionInterface $action): void
    {
        // no-op
    }

    /**
     * Observer event binding for action error status
     * 
     * This can be overridden if you want to mitigate the failure of an
     * action run, and log errors, perform rollbacks, or any other
     * relevant cleanup.
     * 
     * Default behavior is to do nothing.
     * 
     * @param string $queue The queue containing the currently firing action
     * @param string $action_id The identifier for the currently firing action
     * @param string $callback_id The identifier for the currently firing callback
     * @param \Exception $error The exception raised during the action
     * @param array $data The data passed to the action when it was called
     * @param callable $callback The literal callback that is currently
     *        in scope at the time of the exception
     * @param \Oroboros\core\interfaces\library\action\ActionInterface $action
     *        The action object that triggered the event
     * @return void
     */
    protected function handleActionError(string $queue, string $action_id, string $callback_id, \Exception $error, array $data, callable $callback, \Oroboros\core\interfaces\library\action\ActionInterface $action): void
    {
        // no-op
    }

    /**
     * Fires when the action manager completes a clean execution.
     * 
     * This can be overridden if you want to analyze the final results of
     * a completed action.
     * 
     * Default behavior is to do nothing.
     * 
     * @param array $details A summary of the data collected from the last completed observer event
     * @param \Oroboros\core\interfaces\library\action\ActionInterface $action
     *        The action object that triggered the event
     * @return void
     */
    protected function handleActionComplete(array $details, \Oroboros\core\interfaces\library\action\ActionInterface $action): void
    {
        // no-op
    }

    /**
     * Fires when the action manager returns to ready state.
     * 
     * This can be overridden if you want a continuous action cycle, where the
     * next is kicked off immediately upon completion of the last.
     * 
     * Default behavior is to do nothing.
     * 
     * @param \Oroboros\core\interfaces\library\action\ActionInterface $action
     *        The action object that triggered the event
     * @return void
     */
    protected function handleActionReady(\Oroboros\core\interfaces\library\action\ActionInterface $action): void
    {
        // no-op
    }

    /**
     * Observer event binding for action callback execution
     * 
     * This can be overridden if you want to call some other process when
     * specific callback actions fire.
     * 
     * Default behavior is to do nothing.
     * 
     * @param string $queue The queue containing the currently firing action
     * @param string $action_id The identifier for the currently firing action
     * @param string $callback_id The identifier for the currently firing callback
     * @param array $data The data passed to the action when it was called
     * @param callable $callback The literal callback that is currently firing
     * @param \Oroboros\core\interfaces\library\action\ActionInterface $action
     *        The action object that triggered the event
     * @return void
     */
    protected function handleActionCallbackRun(string $queue, string $action_id, string $callback_id, array $data, callable $callback, \Oroboros\core\interfaces\library\action\ActionInterface $action): void
    {
        // no-op
    }

    /**
     * Observer event binding for action callback error status
     * 
     * This can be overridden if you want to log errors, break operation,
     * or perform rollback steps to mitigate problems.
     * 
     * Default behavior is to do nothing.
     * 
     * @param string $queue The queue containing the currently firing action
     * @param string $action_id The identifier for the currently firing action
     * @param string $callback_id The identifier for the currently firing callback
     * @param \Exception $error The exception raised during the callback
     * @param array $data The data passed to the action when it was called
     * @param callable $callback The literal callback that is currently firing
     * @param \Oroboros\core\interfaces\library\action\ActionInterface $action
     *        The action object that triggered the event
     * @return void
     */
    protected function handleActionCallbackError(string $queue, string $action_id, string $callback_id, \Exception $error, array $data, callable $callback, \Oroboros\core\interfaces\library\action\ActionInterface $action): void
    {
        // no-op
    }

    /**
     * Observer event binding for action callback error status
     * 
     * This can be overridden if you want to obtain a specific callback
     * operation result, or if you want to tell the action manager to
     * break operation during a run because the objective has already
     * been handled for the specific action.
     * 
     * Default behavior is to do nothing.
     * 
     * @param string $queue The queue that the last callback ran in
     * @param string $action_id The action id of the action the last callback ran in
     * @param string $callback_id The identifier for the action that just completed
     * @param mixed $data The data passed to the action callback
     * @param type $result The return result of the callback
     * @param callable $callback The literal callable callback
     * @param \Oroboros\core\interfaces\library\action\ActionInterface $action
     *        The action object that triggered the event
     * @return void
     */
    protected function handleActionCallbackComplete(string $queue, string $action_id, string $callback_id, $data, $result, callable $callback, \Oroboros\core\interfaces\library\action\ActionInterface $action): void
    {
        // no-op
    }

    /**
     * Observer event binding for action callback binding
     * 
     * This can be overridden if you want to track callback action binds,
     * which can be useful for debugging how pluggables and exterior objects
     * hook into the actions of the class
     * 
     * Default behavior is to do nothing.
     * 
     * @param string $queue The name of the queue that had a callback bound to one of it's actions
     * @param string $action_id The name of the action the callback was bound to
     * @param string $callback_id The name of the callback
     * @param callable $callback The callable callback reference
     * @param \Oroboros\core\interfaces\library\action\ActionInterface $action
     *        The action object that triggered the event
     * @return void
     */
    protected function handleActionCallbackBind(string $queue, string $action_id, string $callback_id, callable $callback, \Oroboros\core\interfaces\library\action\ActionInterface $action): void
    {
        // no-op
    }

    /**
     * Observer event binding for action callback unbinding
     * 
     * This can be overridden if you want to track callback action unbinds,
     * which can be useful for debugging how pluggables and exterior objects
     * hook into the actions of the class
     * 
     * Default behavior is to do nothing.
     * 
     * @param string $queue The name of the queue that had a callback unbound from one of its actions
     * @param string $action_id The name of the action the callback was unbound from
     * @param string $callback_id The name of the callback
     * @param callable $callback The callable callback reference
     * @param \Oroboros\core\interfaces\library\action\ActionInterface $action
     *        The action object that triggered the event
     * @return void
     */
    protected function handleActionCallbackUnbind(string $queue, string $action_id, string $callback_id, callable $callback, \Oroboros\core\interfaces\library\action\ActionInterface $action): void
    {
        // no-op
    }

    /**
     * You may override this method to add additional action observer bindings,
     * however you MUST also include the results of
     * `parent::declareActionManagerObserverEventBindings()` or you will break
     * your class action system.
     * 
     * @return array
     */
    protected function declareActionManagerObserverEventBindings(): array
    {
        return self::$default_action_observer_bindings;
    }

    /**
     * Returns the queue id of the currently registered queue from a given action object
     * 
     * @param \Oroboros\core\interfaces\library\action\ActionInterface $action
     * @return string
     * @throws \Oroboros\core\exception\ErrorException if the given action is not known.
     *         This should only ever happen if someone tries to jam one in without
     *         using the proper registration protocol. Events properly added by
     *         the subject of the action manager will not have this effect.
     */
    protected function lookupActionQueue(\Oroboros\core\interfaces\library\action\ActionInterface $action): string
    {
        $queue_id = null;
        foreach ($this->getQueueCollection() as $key => $queue) {
            if (!is_null($queue_id)) {
                break;
            }
            foreach ($queue as $name => $possible) {
                if ($action === $possible) {
                    $queue_id = $key;
                    break;
                }
            }
        }
        if (is_null($queue_id)) {
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. Provided action of type '
                        . '[%1$s] does not exist in any known queue.'
                        , get_class($this), get_class($action))
            );
        }
        return $queue_id;
    }

    /**
     * Returns the identifying key of a given action from the queue containing it
     * 
     * @param string $queue
     * @param \Oroboros\core\interfaces\library\action\ActionInterface $action
     * @return string
     * @throws \Oroboros\core\exception\InvalidArgumentException If the given queue is not known
     * @throws \Oroboros\core\exception\ErrorException if the given action is not known.
     *         This should only ever happen if someone tries to jam one in without
     *         using the proper registration protocol. Events properly added by
     *         the subject of the action manager will not have this effect.
     */
    protected function lookupActionId(string $queue, \Oroboros\core\interfaces\library\action\ActionInterface $action): string
    {
        if (!$this->hasQueue($queue)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided queue [%2$s] does not exist.', get_class($this), $queue));
        }
        foreach ($this->getQueueCollection()[$queue] as $key => $possible) {
            if ($action === $possible) {
                return $key;
            }
        }
        throw new \Oroboros\core\exception\ErrorException(
                sprintf('Error encountered in [%1$s]. Provided action of type [%2$s] '
                    . 'does not exist in queue [%3$s].'
                    , get_class($this), get_class($action), $queue)
        );
    }

    /**
     * Adds the spl observer bindings for the action manager
     * 
     * @return void
     */
    private function bindActionObserverEvents(): void
    {
        foreach ($this->declareActionManagerObserverEventBindings() as $key => $callback) {
            $key = trim(sprintf('%1$s.%2$s.%3$s', static::WORKER_SCOPE, static::ACTION_GROUP, trim($key, '.')), '.');
            $callback = [$this, $callback];
            $this->bindObserverEvent($key, $callback);
        }
    }

    /**
     * Removes the spl observer bindings for the event manager
     * 
     * @return void
     */
    private function unbindActionObserverEvents(): void
    {
        foreach ($this->declareEventManagerObserverEventBindings() as $key => $callback) {
            $key = trim(sprintf('%1$s.%2$s', static::WORKER_SCOPE, trim($key, '.')), '.');
            $this->unbindObserverEvent($key);
        }
    }

    /**
     * Formats spl observer updates for ActionInterface objects
     * 
     * @param \Oroboros\core\interfaces\library\action\ActionInterface $action
     * @return void
     */
    private function splObserverUpdateAction(\Oroboros\core\interfaces\library\action\ActionInterface $action): void
    {
        $prefix = trim(sprintf('%1$s.%2$s', trim(sprintf('%1$s', static::WORKER_SCOPE), ':.'), $action->getName()), ':.') . '.';
        if (strpos($action->getObserverEvent(), $prefix) === 0) {
            $key = trim(sprintf('%1$s.%2$s', trim(sprintf('%1$s.%2$s', static::WORKER_SCOPE, $action::ACTION_GROUP), ':.'), substr($action->getObserverEvent(), strlen($prefix))), ':.');
            if ($this->observer_event_bindings->has($key)) {
                call_user_func($this->observer_event_bindings->get($key), $action);
            }
        } else {
            $event = $action->getObserverEvent();
            $prefix = trim(sprintf('%1$s.%2$s', static::WORKER_SCOPE, static::ACTION_GROUP), '.');
            $event_key = substr($event, strpos($event, $action->getName()) + strlen($action->getName()) + 1);
            $event_name = trim(sprintf('%1$s.%2$s', $prefix, $event_key), '.');
            if ($this->observer_event_bindings->has($event_name)) {
                call_user_func($this->observer_event_bindings->get($event_name), $action);
            }
        }
    }

    /**
     * Formats spl observer updates for ActionAwareInterface objects
     * 
     * @param \Oroboros\core\interfaces\library\action\ActionAwareInterface $aware
     * @return void
     */
    private function splObserverUpdateActionAware(\Oroboros\core\interfaces\library\action\ActionAwareInterface $aware): void
    {
//        d(get_class($aware), $aware::CLASS_TYPE, $aware::CLASS_SCOPE, $aware->getObserverEvent());
    }

    /**
     * Formats spl observer updates for ActionListenerInterface objects
     * 
     * @param \Oroboros\core\interfaces\library\action\ActionListenerInterface $listener
     * @return void
     */
    private function splObserverUpdateListener(\Oroboros\core\interfaces\library\action\ActionListenerInterface $listener): void
    {
        
    }

    /**
     * Formats spl observer updates for ActionWatcherInterface objects
     * 
     * @param \Oroboros\core\interfaces\library\action\ActionWatcherInterface $watcher
     * @return void
     */
    private function splObserverUpdateWatcher(\Oroboros\core\interfaces\library\action\ActionWatcherInterface $watcher): void
    {
        
    }

    /**
     * Initializes the master queue.
     * Must be called in the constructor after `parent::__construct()`.
     * 
     * @param string $collection_class
     * @return void
     */
    private function initializeCollection(string $collection_class = null): void
    {
        if (is_null($collection_class)) {
            $collection_class = self::COLLECTION_CLASS;
        }
        if (is_null($this->queue_collection)) {
            $this->queue_collection = $this::containerizeInto($collection_class);
        }
    }

    /**
     * Verifies that the child class has declared the class constant `WORKER_CLASS`,
     * and that it resolves to a class implementing
     * `Oroboros\core\interfaces\library\action\ActionInterface`
     * 
     * If this method does not fire clean, the class is unusable.
     * 
     * @return void
     * @throws \Oroboros\core\exception\core\InvalidClassException
     */
    private function verifyWorkerType(): void
    {
        if (is_null(static::WORKER_CLASS)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Class [%2$s] is misconfigured. Expected class constant '
                        . '[%2$s] must define a corresponding worker class to handle '
                        . 'action group [%3$s].', get_class($this), 'WORKER_CLASS', static::ACTION_GROUP)
            );
        }
        $expected = 'Oroboros\\core\\interfaces\\library\\action\\ActionInterface';
        $resolver = $this->load('library', 'resolver\\ArgumentResolver');
        if (!$resolver->resolve(static::WORKER_CLASS, $expected)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Class [%2$s] is misconfigured. Expected class constant '
                        . '[%2$s] must define a corresponding worker class to handle '
                        . 'action group [%3$s] which implements interface [%4$s].'
                        , get_class($this), 'WORKER_CLASS', static::ACTION_GROUP, $expected)
            );
        }
    }

    /**
     * Verifies that the action group is declared
     * 
     * @return void
     * @throws \Oroboros\core\exception\core\InvalidClassException
     */
    private function verifyActionType(): void
    {
        if (is_null(static::ACTION_GROUP)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(sprintf('Class [%2$s] is misconfigured. '
                        . 'Expected class constant [%2$s] must define a corresponding action group.', get_class($this), 'ACTION_GROUP'));
        }
    }
}
