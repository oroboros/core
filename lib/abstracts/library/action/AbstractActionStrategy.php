<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\action;

/**
 * Abstract Action Strategy
 * Provides abstraction for action strategies
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
abstract class AbstractActionStrategy extends \Oroboros\core\abstracts\pattern\strategy\AbstractStrategy implements \Oroboros\core\interfaces\library\action\ActionStrategyInterface
{

    const CLASS_TYPE = 'library';
    const CLASS_SCOPE = 'action-strategy';
    const STRATEGY_TYPE = 'action';
    const STRATEGY_SCOPE = 'worker';
    const CONTAINER_CLASS = 'Oroboros\\core\\library\\action\\ActionQueue';
    const COLLECTION_CLASS = 'Oroboros\\core\\library\\action\\ActionQueueIndex';
    const STRATEGY_TARGET_INTERFACE = 'Oroboros\\core\\interfaces\\library\\action\\ActionInterface';
    const STRATEGY_MANAGER_INTERFACE = 'Oroboros\\core\\interfaces\\library\\action\\ActionManagerInterface';
    const ACTION_POOL_CLASS = null;

    /**
     * The action pool, which maps action classes to the corresponding action type.
     * Action types are defined by the action manager object.
     * @var \Oroboros\core\library\action\ActionPool
     */
    private static $action_targets = null;
    private static $action_managers = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeActionTargets();
        $this->initializeActionManagers();
    }

    /**
     * Obtains the correct manager and performs a two way binding with
     * the subject, then returns it.
     * 
     * Initial setup for a fully useable manager will have already occurred
     * when this method resolves.
     * 
     * @param \Oroboros\core\interfaces\library\action\ActionAwareInterface $subject
     * @return \Oroboros\core\interfaces\library\action\ActionManagerInterface
     */
    public function bind(\Oroboros\core\interfaces\library\action\ActionAwareInterface $subject): \Oroboros\core\interfaces\library\action\ActionManagerInterface
    {
        $type = $subject::CLASS_TYPE;
        if ($this::hasManager($type)) {
            $manager = $this::getManager($type, $this->getCommand(), $this->getArguments(), $this->getFlags());
        } else {
            $manager = $this::getManager('default', $this->getCommand(), $this->getArguments(), $this->getFlags());
        }
        // Give the subject to the manager to bind to internally
        $manager->setSubject($subject);
        // Perform any additional binding steps as per individual implementation.
        $this->bindSubject($subject, $manager);
        return $manager;
    }

    /**
     * Returns a boolean designation as to whether a given key has a
     * corresponding manager.
     * @param string $key
     * @return bool
     */
    public static function hasManager(string $key): bool
    {
        if (is_null(self::$action_managers)) {
            self::initializeActionManagers();
        }
        return self::$action_managers[static::STRATEGY_TYPE]->has($key);
    }

    /**
     * Returns the action manager keys that are currently registered in the current scope.
     * 
     * @return array
     */
    public static function listManagers(): array
    {
        if (is_null(self::$action_managers)) {
            self::initializeActionManagers();
        }
        return array_keys(self::$action_managers[static::STRATEGY_TYPE]->toArray());
    }

    /**
     * Returns a manager for a given key.
     * This method does not perform any bindings.
     * 
     * @param string $key
     * @param string|null $command Any command to pass to the manager constructor
     * @param array|null $args Any arguments to pass to the manager constructor
     * @param array|null $flags Any flags to pass to the manager constructor
     * @return \Oroboros\core\interfaces\library\action\ActionManagerInterface
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    public static function getManager(string $key, string $command = null, array $args = null, array $flags = null): \Oroboros\core\interfaces\library\action\ActionManagerInterface
    {
        if (!static::hasManager($key)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. No manager exists for provided key [%2$s]', static::class, $key)
            );
        }
        $proxy = new \Oroboros\core\pattern\factory\LoaderProxy();
        $manager = $proxy->load('library', self::$action_managers[static::STRATEGY_TYPE][$key], $command, $args, $flags);
        return $manager;
    }

    /**
     * Registers an action manager class
     * @param string $key
     * @param string $manager
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    public static function registerManager(string $key, string $manager): void
    {
        if (is_null(self::$action_managers)) {
            self::initializeActionManagers();
        }
        $expected = static::STRATEGY_MANAGER_INTERFACE;
        $proxy = new \Oroboros\core\pattern\factory\LoaderProxy();
        $resolver = $proxy->load('library', 'resolver\\ArgumentResolver');
        if (!$resolver->resolve($manager, $expected)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Provided manager [%2$s] '
                        . 'does not implement expected interface [%3$s].'
                        , static::class, $manager, $expected)
            );
        }
        self::$action_managers[static::STRATEGY_TYPE][$key] = $manager;
    }

    /**
     * Registers an action class as handling a specific action type
     * 
     * @param string $type
     * @param string $action
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException if the given
     *         action does not resolve to a class, or does not implement the
     *         action interface
     */
    public static function registerActionType(string $type, string $action): void
    {
        self::initializeActionTargets();
        $proxy = new \Oroboros\core\pattern\factory\LoaderProxy();
        $classname = $proxy->getFullClassName('library', $action);
        if (!$classname) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided action [%2$s] is not a valid class.', get_called_class(), $action));
        }
        $expected = static::STRATEGY_TARGET_INTERFACE;
        if (!in_array($expected, class_implements($classname))) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided worker [%2$s] is not a valid action. '
                        . 'Expected implementation of [%3$s].', get_called_class(), $action, $expected));
        }
        self::$action_targets[$type] = $action;
    }

    /**
     * Preflight method to insure that a given action/queue is valid.
     * @param string $type
     * @param string $queue
     * @return void
     * @throws \InvalidArgumentException If the given action or queue is not valid.
     */
    public function verifyAction(string $type, string $queue): void
    {
        if (!self::$action_targets->has($type)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'No action class exists of type [%2$s].', get_class($this), $type));
        }
    }

    /**
     * Extensions of this class must provide individual binding logic for
     * their own subject and manager pairs.
     */
    abstract protected function bindSubject(\Oroboros\core\interfaces\library\action\ActionAwareInterface $subject, \Oroboros\core\interfaces\library\action\ActionManagerInterface $manager): void;

    /**
     * Returns the action object designated
     * @param string $type
     * @param string $queue
     * @return \Oroboros\core\interfaces\library\action\ActionInterface
     */
    public function getAction(string $type, string $queue): \Oroboros\core\interfaces\library\action\ActionInterface
    {
        $this->verifyAction($type, $queue);
        $action = $this->load('library', self::$action_targets[$type], $queue);
        return $action;
    }

    /**
     * Override this method to declare default action targets
     * @return array
     */
    protected static function declareActionTargets(): array
    {
        return [];
    }

    /**
     * Override this method to declare valid action managers
     * @return array
     */
    protected static function declareActionManagers(): array
    {
        return [];
    }

    /**
     * 
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    protected static function generateActionTargets(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        return \Oroboros\core\library\container\Container::init();
    }

    /**
     * Initializes the action pool if it is not already.
     * @return void
     */
    private static function initializeActionTargets(): void
    {
        if (is_null(self::$action_targets)) {
            $action_pool_class = static::ACTION_POOL_CLASS;
            $action_pool = $action_pool_class::init();
            self::$action_targets = $action_pool;
        }
        if (!self::$action_targets->has(static::STRATEGY_TYPE)) {
            self::$action_targets[static::STRATEGY_TYPE] = static::generateActionTargets();
        }
    }

    /**
     * Initializes the index of known action managers
     * @return void
     */
    private static function initializeActionManagers(): void
    {
        if (is_null(self::$action_managers)) {
            self::$action_managers = \Oroboros\core\library\container\Container::init();
        }
        if (!self::$action_managers->has(static::STRATEGY_TYPE)) {
            self::$action_managers[static::STRATEGY_TYPE] = \Oroboros\core\library\container\Container::init();
            foreach (static::declareActionManagers() as $key => $class) {
                if (!self::$action_managers[static::STRATEGY_TYPE]->has($key)) {
                    static::registerManager($key, $class);
                }
            }
        }
    }
}
