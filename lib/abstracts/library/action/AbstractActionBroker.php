<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\action;

/**
 * Abstract Action Broker
 * Distributes default actions to initializing ActionAware objects.
 * 
 * Using a broker is optional. If a broker is not added as a dependency
 * in the constructor of the EventAware object, it will proceed normally.
 * 
 * If a broker is present, then it will analyze the EventAware object
 * and return the correct action queues and actions that it is aware of
 * for auto-registration.
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
abstract class AbstractActionBroker extends \Oroboros\core\abstracts\library\definer\AbstractDefiner implements \Oroboros\core\interfaces\library\action\ActionBrokerInterface
{

    use \Oroboros\core\traits\pattern\BrokerTrait {
        \Oroboros\core\traits\pattern\BrokerTrait::resolve as private broker_trait_resolve;
    }

    const CLASS_SCOPE = 'action-broker';

    /**
     * Designates the interface that an evaluation object
     * should have to return the dataset.
     */
    const BROKER_SUBJECT = 'Oroboros\\core\\interfaces\\library\\action\\ActionAwareInterface';

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeActionBroker();
    }

    /**
     * If method `analyze` returns `true`, this method MUST return
     * the relevant dataset for the subject.
     * 
     * If method `analyze` returns `false`, this method MUST return
     * a non-blocking empty container.
     * 
     * If a class defines a class container, that container will be used.
     * Otherwise the default generic container will be used.
     * 
     * @param object $subject
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    public function resolve(object $subject): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $data = $this->broker_trait_resolve($subject);
        $data = $this->resolveDefaults($data, $data);
        $const = 'CLASS_TYPE';
        if (defined(sprintf('%1$s::%2$s', get_class($subject), $const)) && $subject::CLASS_TYPE !== null) {
            $type = $subject::CLASS_TYPE;
            if ($data->has($type)) {
                $data = $data[$type];
            }
        }
        $const = 'CLASS_SCOPE';
        if (defined(sprintf('%1$s::%2$s', get_class($subject), $const)) && $subject::CLASS_SCOPE !== null) {
            $scope = $subject::CLASS_SCOPE;
            if ($data->has($scope)) {
                $data = $data[$scope];
            }
        }
        return $data;
    }

    /**
     * Provides a null definer data set by default.
     * This will always compile clean.
     * 
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    protected function declareDataSet(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $class = static::CONTAINER_CLASS;
        return $class::init(null, ['defaults' => []]);
    }

    /**
     * Returns the definer result by default
     * 
     * @return array
     */
    protected function declareBrokerDefaultData(): array
    {
        return $this->fetch()->toArray();
    }

    /**
     * Recursively resolve data defaults into the resulting container object.
     * 
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $defaults
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $scoped
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    protected function resolveDefaults(\Oroboros\core\interfaces\library\container\ContainerInterface $defaults, \Oroboros\core\interfaces\library\container\ContainerInterface $scoped): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $class = get_class($scoped);
        $result = [];
        $container_interface = \Oroboros\core\interfaces\library\container\ContainerInterface::class;
        foreach ($defaults as $key => $default) {
            if ($key === 'default') {
                // Import defaults
                $result = $this->resolveDefaults($default, $scoped)->toArray();
                continue;
            }
            if (!$scoped->has($key)) {
                // Import missing keys
                $result[$key] = $default;
                continue;
            }
            if (($default instanceof $container_interface) && ($scoped[$key] instanceof $container_interface)) {
                // Recurse if both containers
                $result[$key] = $this->resolveDefaults($default, $scoped[$key]);
            }
        }
        foreach ($scoped as $key => $value) {
            if (array_key_exists($key, $result) || $key === 'default') {
                continue;
            }
            $result[$key] = $value;
        }
        return $class::init(null, $result);
    }

    /**
     * Initializes the action broker
     * 
     * @return void
     * @throws \Oroboros\core\exception\ErrorException
     */
    private function initializeActionBroker(): void
    {
        foreach ($this->getArguments() as $key => $value) {
            if ($this->hasRequiredKey($key)) {
                $this->setRequiredKey($key, $value);
            }
            if ($this->hasOptionalKey($key)) {
                $this->setOptionalKey($key, $value);
            }
        }
        if (!$this->canCompile()) {
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. Cannot compile broker '
                        . 'dataset. Missing required arguments: [%2$s].'
                        , get_class($this), implode(', ', $this->getUnsatisfiedRequiredKeys()))
            );
        }
        try {
            $this->compile();
        } catch (\Oroboros\core\exception\ErrorException $e) {
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. Encountered an error '
                        . 'compiling broker dataset: [%2$s] at line [%3$s] of file [%4$s].'
                        , get_class($this), $e->getMessage(), $e->getLine(), $e->getFile())
                    , $e->getCode(), $e->getSeverity(), $e->getFile(), $e->getLine(), $e);
        }
        $this->initializeBroker();
    }
}
