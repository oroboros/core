<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\action;

/**
 * Abstract Action
 * 
 * Provides abstraction for defining and executing actions
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
abstract class AbstractAction extends \Oroboros\core\abstracts\pattern\director\AbstractWorker implements \Oroboros\core\interfaces\library\action\ActionInterface
{

    use \Oroboros\core\traits\ContainerPackagerUtility;
    use \Oroboros\core\traits\LoaderTrait;
    use \Oroboros\core\traits\pattern\ObservableTrait;

    /**
     * Use the default action pool
     */
    const CONTAINER_CLASS = 'Oroboros\\core\\library\\action\\ActionPool';

    /**
     * Designates the class type as an action
     */
    const CLASS_SCOPE = 'action';

    /**
     * The scope required of workers. Worker classes must define a WORKER_SCOPE,
     * which is used by directors to filter for valid workers in scope for their
     * subset of directives.
     */
    const WORKER_SCOPE = 'action';

    /**
     * The task accomplished by the worker. Worker classes must define a WORKER_TASK,
     * which is a keyword used by the director to isolate tasks appropriate for a given worker.
     */
    const WORKER_TASK = 'action';

    /**
     * Override in child classes to declare the action grouping for the given action.
     */
    const ACTION_GROUP = null;

    /**
     * Override in child classes to declare the action name for the given action.
     * 
     * For single-scope actions, set the class constant `ACTION_NAME` to a
     * fixed value, and any instances of that action will always use that name.
     * 
     * Otherwise actions may be arbitrarily named when constucted based on
     * their constructor parameter `$command`
     * 
     * It is not required to provide a name in either case unless tighter
     * scoping is required for the given action.
     */
    const ACTION_NAME = null;

    /**
     * Required keys for registering action queues
     * @var array
     */
    private static $required_queue_keys = [
    ];

    /**
     * The subject bound to the action.
     * 
     * @var \Oroboros\core\interfaces\library\action\ActionAwareInterface
     */
    private $subject = null;

    /**
     * The container containing all of the given callbacks registered to the action.
     * 
     * @var \Oroboros\core\interfaces\action\ActionPoolInterface
     */
    private $action_pool = null;

    /**
     * The ongoing results of a running action, or null if no action is running
     * 
     * @var array|null
     */
    private $action_results = null;

    /**
     * The arguments passed to the action if an action is running, or null if no action is running
     * 
     * @var array|null
     */
    private $action_arguments = null;

    /**
     * The exception thrown during an action, or null if no action is running and encounters an error
     * 
     * @var \Exception|null
     */
    private $action_error = null;

    /**
     * The name of the callback currently scoped, or null if no callback is currently scoped
     * 
     * @var string|null
     */
    private $action_callback_id = null;

    /**
     * The callable callback currently scoped, or null in all other cases
     * 
     * @var callable|null
     */
    private $action_callback = null;

    /**
     * The arguments passed to a currently scoped callback when a callback is executed, or null if no callback is currently executing
     * 
     * @var array|null
     */
    private $action_callback_arguments = null;

    /**
     * The results returned from the action callback immediately after
     * a callback resolves, or null in all other cases
     * 
     * @var mixed
     */
    private $action_callback_results = null;

    /**
     * Any exception raised by a callback when it is handled by the action system,
     * or null in all other cases
     * 
     * @var \Exception|null
     */
    private $action_callback_error = null;

    /**
     * Allows parent objects to mark errors as recoverable.
     * If an exception is marked as recoverable during an action event callback
     * or action, the exception will be supressed and the action or callback
     * will continue.
     * 
     * This value will always be set back to false after an error recovers.
     * 
     * @var boolean
     */
    private $action_error_is_recoverable = false;

    /**
     * Allows parent objects to mark an action run as resolved, returning
     * the existing results immediately without running further callbacks
     * 
     * This value will always be set back to false after an action chain is halted.
     * 
     * @var boolean
     */
    private $action_is_resolved = false;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeActionPool();
        $this->initializeObservable();
    }

    /**
     * Runs the action.
     * 
     * @param type $args
     */
    public function run($args = null)
    {
        $results = [];
        $this->setActionArguments($args);
        $this->setActionResults($results);
        $this->setObserverEvent('run');
        foreach ($this->getActionPool() as $key => $callback) {
            if ($this->isResolved()) {
                $this->resetResolved();
                break;
            }
            try {
                $this->setActionCallbackId($key);
                $result = $this->runCallback($key, $this->getCurrentActionArguments(), true);
                $results[] = $result;
                $this->setActionResults($results);
            } catch (\Oroboros\core\exception\core\InvalidClassException $e) {
                // Only the bootstrap handles these
                throw $e;
            } catch (\Exception $e) {

                $this->resetActionCallbackResults();
                $this->setActionError($e);
                $this->setObserverEvent('error');
                if (!$this->isErrorRecoverable()) {
                    throw $e;
                }
                $this->resetRecoverable();
            }
            $this->resetActionCallbackResults();
        }
        $this->setObserverEvent('complete');
        $this->resetAction();
        $this->setObserverEvent('ready');
        return $results;
    }

    /**
     * This marks an error caught as recoverable, which allows a parent object
     * to surpress the exception from bubbling up further in the main runtime.
     * 
     * This value will always be reset to false after each recoverred error.
     * 
     * @return void
     */
    public function markErrorRecoverable(): void
    {
        $this->action_error_is_recoverable = true;
    }

    /**
     * Returns whether anything has marked the current error recoverable.
     * 
     * @return bool
     */
    public function isErrorRecoverable(): bool
    {
        return $this->action_error_is_recoverable;
    }

    /**
     * This marks an action as resolved, which will break further callbacks
     * within the action from running. The existing results
     * will be returned as-is.
     * 
     * This value will always be reset to false after each recovered error.
     * 
     * @return void
     */
    public function markActionResolved(): void
    {
        $this->action_is_resolved = true;
    }

    /**
     * Returns whether a running action has been marked as resolved
     * 
     * @return bool
     */
    public function isResolved(): bool
    {
        return $this->action_is_resolved;
    }

    /**
     * Returns the name of the action
     * @return string
     */
    public function getName(): string
    {
        $group = $this->getActionGroup();
        $name = $this->getActionName();
        return trim(sprintf('%1$s.%2$s', $group, $name), '.');
    }

    /**
     * Binds the given subject to the action. There must not be a subject
     * currently bound, and the subject must pass any criteria from
     * `verifySubject` prior to binding.
     * 
     * If any of these are not the case, an error exception will be raised.
     * 
     * @param \Oroboros\core\interfaces\library\action\ActionManagerInterface $subject
     * @return void
     * @throws \Oroboros\core\exception\ErrorException If there is already a
     *         bound subject, or if the given subject does not pass any criteria
     *         from method `verifySubject`
     */
    public function bind(\Oroboros\core\interfaces\library\action\ActionManagerInterface $subject): void
    {
        if (!is_null($this->subject)) {
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. Cannot bind a subject '
                        . 'because a subject is already bound.', get_class($this))
            );
        }
        $this->resetAction();
        $this->verifySubject($subject);
        $this->bindSubject($subject);
        $this->setObserverEvent('bind');
        $this->setObserverEvent('complete');
        $this->resetAction();
        $this->setObserverEvent('ready');
    }

    /**
     * Unbinds the subject from the action and restores the action to it's unbound state.
     * The `$subject` argument must **exactly match** the object internally held
     * in the subject binding.
     * 
     * If there is no currently bound subject or if the given subject
     * does not match, an error exception will be raised.
     * 
     * @param \Oroboros\core\interfaces\library\action\ActionManagerInterface $subject
     * @return void
     * @throws \Oroboros\core\exception\ErrorException if the given subject
     *         does not exactly match the bound subject, or if there is no
     *         currently bound subject.
     */
    public function unbind(\Oroboros\core\interfaces\library\action\ActionManagerInterface $subject): void
    {
        if (is_null($this->subject)) {
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. Cannot unbind because no '
                        . 'subject is currently bound.', get_class($this))
            );
        }
        if (!$subject === $this->getSubject()) {
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. Provided subject of class '
                        . '[%2$] is not the currently bound subject of class [%3$s].'
                        , get_class($this), get_class($subject), get_class($this->getSubject()))
            );
        }
        $this->resetAction();
        $this->setObserverEvent('unbind');
        $this->unbindSubject($subject);
        $this->setObserverEvent('complete');
        $this->resetAction();
        $this->setObserverEvent('unbound');
    }

    /**
     * Runs a specific action callback.
     * 
     * @param string $key the identifier of the callback
     * @param array $args Any arguments to pass to the callback
     * @param boolean $skip_reset Used internally to designate that this call
     *        is part of a broader running event and the call should not
     *        explicitly reset arguments.
     * @return mixed
     * @throws \Oroboros\core\exception\InvalidArgumentException
     *         if the specified callback does not exist.
     */
    public function runCallback(string $key, array $args = [], bool $skip_reset = false)
    {
        if (!$this->hasCallback($key)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Provided callback key '
                        . '[%2$s] does not exist.', get_class($this), $key)
            );
        }
        if (!$skip_reset) {
            $this->resetAction();
        }
        $callback = $this->getActionPool()[$key];
        $this->setActionCallbackId($key);
        $this->setActionCallbackCallable($this->getCallback($key));
        $this->setActionCallbackArguments($args);
        try {
            $this->setObserverEvent('run-callback');
            $result = call_user_func_array($this->getCallback($key), $args);
            $this->setActionCallbackResults($result);
        } catch (\Oroboros\core\exception\core\InvalidClassException $e) {
            // Only the bootstrap handles these
            throw $e;
        } catch (\Exception $e) {
            $this->setActionCallbackError($e);
            $this->setObserverEvent('callback-error');
            if (!$this->isErrorRecoverable()) {
                throw new \Oroboros\core\exception\ErrorException(
                        sprintf('Error encountered in [%1$s]. Callback [%2$s] '
                            . 'encountered an error of type [%3$s] with message [%4$s].'
                            , get_class($this), $key, get_class($e), $e->getMessage())
                        , $e->getCode(), E_ERROR, __FILE__, __LINE__, $e)
                ;
            }
            $this->resetRecoverable();
            $result = null;
        }
        $this->setObserverEvent('callback-complete');
        if (!$skip_reset) {
            $this->resetAction();
            $this->setObserverEvent('ready');
        }
        return $result;
    }

    /**
     * Returns a boolean designation as to whether a given callback key exists
     * 
     * @param string $key
     * @return bool
     */
    public function hasCallback(string $key): bool
    {
        return $this->getActionPool()->has($key);
    }

    /**
     * Returns the callable registered for the specified key
     * 
     * @param string $key
     * @return callable
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    public function getCallback(string $key): callable
    {
        if (!$this->hasCallback($key)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Provided callback key '
                        . '[%2$s] does not exist.', get_class($this), $key)
            );
        }
        $callback = $this->getActionPool()[$key];
        return $callback;
    }

    /**
     * Returns an array of registered callbacks for the action.
     * 
     * @return array
     */
    public function listCallbacks(): array
    {
        return array_keys($this->getActionPool()->toArray());
    }

    /**
     * Registers a callback against the action with the given key.
     * 
     * Throws an exception if the key is already in use.
     * 
     * @param string $key
     * @param callable $callback
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    public function registerCallback(string $key, callable $callback): void
    {
        if ($this->hasCallback($key)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Provided callback key '
                        . '[%2$s] already exists.', get_class($this), $key)
            );
        }
        $this->resetAction();
        $this->setActionCallbackId($key);
        $this->setActionCallbackCallable($callback);
        $this->setObserverEvent('register-callback');
        $this->getActionPool()[$key] = $callback;
        $this->setObserverEvent('complete');
        $this->resetAction();
        $this->setObserverEvent('ready');
    }

    /**
     * Removes a callback registered by the given key.
     * 
     * Throws an exception if the key does not exist.
     * 
     * @param string $key
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    public function unregisterCallback(string $key): void
    {
        if (!$this->hasCallback($key)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Provided callback key '
                        . '[%2$s] does not exist.', get_class($this), $key)
            );
        }
        $this->resetAction();
        $this->setActionCallbackId($key);
        $this->setActionCallbackCallable($this->getCallback($key));
        $this->setObserverEvent('unregister-callback');
        $pool = $this->getActionPool();
        unset($pool[$key]);
        $this->setObserverEvent('complete');
        $this->resetAction();
        $this->setObserverEvent('ready');
    }

    /**
     * Director pattern  compatibility
     * 
     * @param array $arguments
     * @param array $flags
     * @return mixed
     */
    public function executeTask(array $arguments = [], array $flags = [])
    {
        return $this->run([
                'args' => $arguments,
                'flags' => $flags
        ]);
    }

    /**
     * Returns an array of all current observer event reporting keys
     * 
     * @return array
     */
    public function getCurrentAction(): array
    {
        return [
            'arguments' => $this->getCurrentActionArguments(),
            'error' => $this->getCurrentActionError(),
            'results' => $this->getCurrentActionResults(),
            'callback-id' => $this->getCurrentActionCallbackId(),
            'callback' => $this->getCurrentActionCallback(),
            'callback-arguments' => $this->getCurrentActionCallbackArguments(),
            'callback-results' => $this->getCurrentActionCallbackResults(),
            'callback-error' => $this->getCurrentActionCallbackError(),
        ];
    }

    /**
     * Getter for the current action results during a "run" or "error" event,
     * which will be an array of each subsequent return value of the callbacks
     * referenced during the action up to that point. It will be null in all
     * other cases.
     * 
     * @return array|null
     */
    public function getCurrentActionResults(): ?array
    {
        return $this->action_results;
    }

    /**
     * Getter for the current action error during the "error" observer event.
     * It will be null in all other cases.
     * 
     * @return \Exception|null
     */
    public function getCurrentActionError(): ?\Exception
    {
        return $this->action_error;
    }

    /**
     * Getter for the currently scoped action arguments. This will have an array
     * value during the observer event "run", during "error", and during "complete"
     * if "complete" was fired at the end of an action run event. It will be null
     * in all other cases.
     * 
     * @return array|null
     */
    public function getCurrentActionArguments(): ?array
    {
        return $this->action_arguments;
    }

    /**
     * Getter for the currently scoped callback id. This occurs when bound,
     * unbound, an action is run, or a single callback is run.
     * Null in all other cases.
     * 
     * @return string|null
     */
    public function getCurrentActionCallbackId(): ?string
    {
        return $this->action_callback_id;
    }

    /**
     * Getter for the currently scoped callback. This occurs when bound,
     * unbound, an action is run, or a single callback is run.
     * Null in all other cases.
     * 
     * @return callable|null
     */
    public function getCurrentActionCallback(): ?callable
    {
        return $this->action_callback;
    }

    /**
     * Getter for the arguments passed to the last successfully run callback
     * when the "callback-complete" or "run-callback" event is active.
     * Null in all other cases.
     * 
     * @return type
     */
    public function getCurrentActionCallbackArguments(): ?array
    {
        return $this->action_callback_arguments;
    }

    /**
     * Getter for the results of the last successfully run callback when the "callback-complete"
     * event is active. Null in all other cases.
     * 
     * @note individual callbacks may explicitly return `null`
     *       or not return anything, in which case it will
     *       still be `null`.
     * @return type
     */
    public function getCurrentActionCallbackResults()
    {
        return $this->action_callback_results;
    }

    /**
     * Getter for the current action callback error when the "callback-error"
     * event is active. Null in all other cases.
     * 
     * @return \Exception|null
     */
    public function getCurrentActionCallbackError(): ?\Exception
    {
        return $this->action_callback_error;
    }

    /**
     * Internal getter for the subject controlling the action.
     * 
     * If no subject has been set yet, this will raise an error exception.
     * 
     * @return \Oroboros\core\interfaces\library\action\ActionManagerInterface
     * @throws \Oroboros\core\exception\ErrorException
     */
    protected function getSubject(): \Oroboros\core\interfaces\library\action\ActionManagerInterface
    {
        if (is_null($this->subject)) {
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. No subject is currently bound.', get_class($this))
            );
        }
        return $this->subject;
    }

    /**
     * Internal getter for the action pool object.
     * @return \Oroboros\core\interfaces\library\action\ActionPoolInterface
     */
    protected function getActionPool(): \Oroboros\core\interfaces\library\action\ActionPoolInterface
    {
        return $this->action_pool;
    }

    /**
     * Override this method to perform additional verifications on the
     * given subject that has requested a binding.
     * 
     * @param \Oroboros\core\interfaces\library\action\ActionAwareInterface $subject
     * @return void
     * @throws \Oroboros\core\exception\ErrorException if the given subject is
     * invalid for the current scope.
     */
    protected function verifySubject(\Oroboros\core\interfaces\library\action\ActionManagerInterface $subject): void
    {
        // no-op
    }

    /**
     * Performs any additional binding steps on a given subject
     * as per implementation details.
     * 
     * Subjects passed to this method have already passed validation via `verifySubject`.
     * 
     * You may override this method to perform additional binding steps,
     * but you should call `parent::bindSubject` **before** doing so.
     * 
     * @param \Oroboros\core\interfaces\library\action\ActionManagerInterface $subject
     * @return void
     */
    protected function bindSubject(\Oroboros\core\interfaces\library\action\ActionManagerInterface $subject): void
    {
        $this->attach($subject);
        $this->subject = $subject;
    }

    /**
     * Removes the internal binding of the subject and returns the action
     * to it's unbound state.
     * 
     * You may override this method to perform additional unbinding steps,
     * but you must call `parent::unbindSubject` **after** doing so.
     * 
     * @param \Oroboros\core\interfaces\library\action\ActionManagerInterface $subject
     * @return void
     */
    protected function unbindSubject(\Oroboros\core\interfaces\library\action\ActionManagerInterface $subject): void
    {
        $this->detach($subject);
        $this->subject = null;
    }

    /**
     * Returns the keys required for registering an action queue
     * 
     * @see \Oroboros\core\interfaces\library\resolver\ResolverInterface
     * @return array
     */
    protected function declareRequiredQueueDetailKeys(): array
    {
        return self::$required_queue_keys;
    }

    /**
     * Sets the observable name for the action as per the observable pattern.
     * 
     * @return string
     */
    protected function declareObservableName(): string
    {
        return trim(sprintf('%1$s.%2$s', static::CLASS_SCOPE, $this->getName()), '.');
    }

    /**
     * Returns the action group, if one exists.
     * @return string|null
     */
    protected function getActionGroup(): ?string
    {
        return static::ACTION_GROUP;
    }

    /**
     * Returns the action name, if one exists.
     * If class constant `ACTION_NAME` is not null, that will be returned.
     * If class constant `ACTION_NAME` is null, the command string/null will
     * be returned instead.
     * 
     * For single-scope actions, set the class constant `ACTION_NAME` to a
     * fixed value, and any instances of that action will always use that name.
     * 
     * Otherwise actions may be arbitrarily named when constucted based on
     * their constructor parameter `$command`
     * 
     * @return string|null
     */
    protected function getActionName(): ?string
    {
        $name = trim(sprintf('%1$s.%2$s', static::ACTION_NAME, $this->getCommand()), '.');
        if ($name === '') {
            return null;
        }
        return $name;
    }

    /**
     * Defines the currently scoped action arguments during an observer event
     * This is used for event reporting to observers.
     * 
     * @param array $arguments
     * @return void
     */
    protected function setActionArguments(array $arguments): void
    {
        $this->action_arguments = $arguments;
    }

    /**
     * Defines the currently scoped action error during an observer event
     * This is used for event reporting to observers.
     * 
     * @param \Exception $error
     * @return void
     */
    protected function setActionError(\Exception $error): void
    {
        $this->action_error = $error;
    }

    /**
     * Defines the currently scoped action results during an observer event
     * This is used for event reporting to observers.
     * 
     * @param array $results
     * @return void
     */
    protected function setActionResults(array $results): void
    {
        $this->action_results = $results;
    }

    /**
     * Defines the currently scoped action callback id during an observer event
     * This is used for event reporting to observers.
     * 
     * @param string $id
     * @return void
     */
    protected function setActionCallbackId(string $id): void
    {
        $this->action_callback_id = $id;
    }

    /**
     * Defines the currently scoped action callback callable during an observer event
     * This is used for event reporting to observers.
     * 
     * @param callable $callback
     * @return void
     */
    protected function setActionCallbackCallable(callable $callback): void
    {
        $this->action_callback = $callback;
    }

    /**
     * Defines the currently scoped action callback arguments during an observer event
     * This is used for event reporting to observers.
     * 
     * @param array $arguments
     * @return void
     */
    protected function setActionCallbackArguments(array $arguments): void
    {
        $this->action_callback_arguments = $arguments;
    }

    /**
     * Defines the currently scoped action callback result during an observer event
     * This is used for event reporting to observers.
     * 
     * @param mixed $results
     * @return void
     */
    protected function setActionCallbackResults($results): void
    {
        $this->action_callback_results = $results;
    }

    /**
     * Defines the currently scoped action callback error during an observer event
     * This is used for event reporting to observers.
     * 
     * @param \Exception $error
     * @return void
     */
    protected function setActionCallbackError(\Exception $error): void
    {
        $this->action_callback_error = $error;
    }

    /**
     * Returns all event monitoring action properties to their default state
     * 
     * @return void
     */
    protected function resetAction(): void
    {
        $this->resetActionResults();
        $this->resetActionArguments();
        $this->resetActionError();
        $this->resetActionCallback();
        $this->resetRecoverable();
        $this->resetResolved();
    }

    /**
     * Returns the action results property to it's default state
     * 
     * @return void
     */
    protected function resetActionResults(): void
    {
        $this->action_results = null;
    }

    /**
     * Returns the action error property to it's default state
     * 
     * @return void
     */
    protected function resetActionError(): void
    {
        $this->action_error = null;
    }

    /**
     * Returns the action arguments property to it's default state
     * 
     * @return void
     */
    protected function resetActionArguments(): void
    {
        $this->action_arguments = null;
    }

    /**
     * Returns the all action callback properties to their default state
     * 
     * @return void
     */
    protected function resetActionCallback(): void
    {
        $this->resetActionCallbackId();
        $this->resetActionCallbackArguments();
        $this->resetActionCallbackCallable();
        $this->resetActionCallbackError();
        $this->resetActionCallbackResults();
    }

    /**
     * Returns the action callback id property to it's default state
     * 
     * @return void
     */
    protected function resetActionCallbackId(): void
    {
        $this->action_callback_id = null;
    }

    /**
     * Returns the action callback callable property to it's default state
     * 
     * @return void
     */
    protected function resetActionCallbackCallable(): void
    {
        $this->action_callback = null;
    }

    /**
     * Returns the action callback arguments property to it's default state
     * 
     * @return void
     */
    protected function resetActionCallbackArguments(): void
    {
        $this->action_callback_arguments = null;
    }

    /**
     * Returns the action callback results property to it's default state
     * 
     * @return void
     */
    protected function resetActionCallbackResults(): void
    {
        $this->action_callback_results = null;
    }

    /**
     * Returns the action callback error property to it's default state
     * 
     * @return void
     */
    protected function resetActionCallbackError(): void
    {
        $this->action_callback_error = null;
    }

    /**
     * Resets recoverable status after it skips an exception re-throw
     * due to parent object intervention
     * 
     * @return void
     */
    protected function resetRecoverable(): void
    {
        $this->action_error_is_recoverable = false;
    }

    /**
     * Resets resolved status after breaking an action callback chain because
     * the parent object has marked the task as resolved.
     * 
     * @return void
     */
    protected function resetResolved(): void
    {
        $this->action_is_resolved = false;
    }

    /**
     * Initializes the action pool object.
     */
    private function initializeActionPool(): void
    {
        if (!is_null($this->action_pool)) {
            return;
        }
        $expected = 'Oroboros\\core\\interfaces\\library\\action\\ActionPoolInterface';
        $class = static::CONTAINER_CLASS;
        $resolver = $this->load('library', 'resolver\\ArgumentResolver');
        if (!$resolver->resolve($class, $expected)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. Provided class constant by '
                        . 'name [%2$s] must resolve to a classname implementing interface '
                        . '[%3$s]. This class is not useable in it\'s currrent state.'
                        , get_class($this), 'CONTAINER_CLASS', $expected)
            );
        }
        $this->action_pool = $class::init();
    }
}
