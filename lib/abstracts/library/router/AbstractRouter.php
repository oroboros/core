<?php
/*
 * The MIT License
 *
 * Copyright 2015 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\router;

/**
 * Provides standardized baseline router abstraction
 *
 * @author Brian Dayhoff
 */
abstract class AbstractRouter extends \Oroboros\core\abstracts\library\AbstractLibrary implements \Oroboros\core\interfaces\library\router\RouterInterface
{

    /**
     * Defines further child classes as routers
     */
    const CLASS_SCOPE = 'router';
    const ROUTER_SCOPE = null;
    const ROUTE_CLASS = null;

    private static $valid_scopes = [
        'http',
        'cli'
    ];

    /**
     * A shared container of all known current routes
     * @var Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private static $routes = null;

    /**
     * A PSR-7 ServerRequestInterface (or an emulated one that still honors the interface for cli requests)
     * @var Psr\Http\Message\ServerRequestInterface
     */
    private static $request = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        $this->verifyRouterScope();
        $this->verifyRouteClass();
        $this->initializeRoutes();
        parent::__construct($command, $arguments, $flags);
        $this->parseRoutes();
        self::$request = $this->initializeRequestObject();
    }

    /**
     * Returns the matching route, or the corresponding error route if no matching route is found.
     * @return Oroboros\core\interfaces\library\router\RouteInterface
     */
    public function match(\Psr\Http\Message\ServerRequestInterface $request)
    {
        $route = null;
        foreach (self::$routes[static::ROUTER_SCOPE] as $key => $candidate_route) {
            if ($candidate_route->match($request)) {
                $route = $candidate_route;
                break;
            }
        }
        if (is_null($route)) {
            // Load the error route, as no valid route was found
            $route = $this->loadErrorRoute(self::$request);
        }
        return $route;
    }

    /**
     * Returns the request object
     * @return Psr\Http\Message\ServerRequestInterface
     */
    public function getRequest()
    {
        return self::$request;
    }

    /**
     * Returns a container of the routes scoped to the current router.
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    public function getRoutes(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        return self::$routes[static::ROUTER_SCOPE];
    }

    /**
     * Must be overridden in the extension class to provide a default route
     * that handles errors when no route exists for the given request.
     * 
     * This route is always returned when no route exists for the given request.
     * 
     * @return Oroboros\core\interfaces\library\router\RouteInterface
     */
    abstract public function loadErrorRoute(\Psr\Http\Message\ServerRequestInterface $request);

    /**
     * Must be overridden in the extension class to provide valid route.
     * @return Oroboros\core\interfaces\library\router\RouteInterface
     */
    abstract protected function initializeRequestObject();

    /**
     * Override this method to provide default arguments
     * to the route objects that are generated.
     * 
     * If you are using a custom route object, this allows you
     * to perform custom dependency injection on instantiation.
     * 
     * @return array
     */
    protected function getRouteArguments(): array
    {
        return [
            'request' => self::$request,
            'modules' => $this->getArgument('modules'),
            'extensions' => $this->getArgument('extensions'),
            'services' => $this->getArgument('services'),
        ];
    }

    /**
     * Formats the routes into injectable route objects
     * Can be overridden to pass additional parameters,
     * or alter formatting as required.
     * @param array $routes
     * @return array
     */
    protected function formatRoutes(array $routes): array
    {
        $class = $this->getFullClassName('library', static::ROUTE_CLASS);
        $results = [];
        foreach ($routes as $type => $domain) {
            foreach ($domain as $host => $details) {
                foreach ($details as $uri => $opts) {
                    foreach ($opts as $protocol => $mapping) {
                        $key = sprintf('%1$s:%2$s:%3$s%4$s', $type, $protocol, $host, $uri);
                        $route = new $class($uri, $mapping);
                        $route->setProtocol($protocol)
                            ->setController($mapping['controller'])
                            ->setControllerMethod($mapping['method']);
                        $results[$key] = $route;
                    }
                }
            }
        }
        return $results;
    }

    /**
     * Verify that the extension class has declared its scope correctly
     * @throws \OutOfRangeException
     */
    private function verifyRouterScope()
    {
        if (is_null(static::ROUTER_SCOPE) || !in_array(static::ROUTER_SCOPE, self::$valid_scopes)) {
            throw new \OutOfRangeException(sprintf('Router class [%1$s] is invalid. It must define class constant [%2$s] as one of [%3$s].', get_class($this), 'ROUTER_SCOPE', implode(', ', self::$valid_scopes)));
        }
    }

    /**
     * Verify that the extension class has declared its route class correctly
     * @throws \OutOfRangeException
     */
    private function verifyRouteClass()
    {
        $expected = 'Oroboros\\core\\interfaces\\library\\router\\RouteInterface';
        $existing = $this->getFullClassName('library', static::ROUTE_CLASS);
        if (!($existing == $expected || is_subclass_of($existing, $expected))) {
            throw new \OutOfRangeException(sprintf('Router class [%1$s] is invalid. '
                        . 'It must define class constant [%2$s]  a valid implementation of [%3$s].', get_class($this), 'ROUTE_CLASS', $expected));
        }
    }

    private function parseRoutes()
    {
        $routeset = $this->parseModuleRoutes();
        if ($this->hasArgument('routes')) {
            $routes = $this->getArgument('routes');
        }
        $routes = $this::containerize($routes);
        if (empty(self::$routes[static::ROUTER_SCOPE]->toArray())) {
            if (!$routes->has(static::ROUTER_SCOPE)) {
                // No routes exist for this scope. Do nothing.
                return;
            }
            foreach ($this->formatRoutes($routes->get(static::ROUTER_SCOPE)) as $key => $route) {
                $routeset[$key] = $route;
            }
            self::$routes[static::ROUTER_SCOPE] = $routeset;
        }
    }

    private function parseModuleRoutes(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $routes = [];
        foreach ($this->getArgument('modules') as $module) {
            $formatted_routes = [];
            $module_routes = $module->routes();
            if (array_key_exists(static::ROUTER_SCOPE, $module_routes)) {
                $formatted_routes = $this->formatRoutes($module_routes[static::ROUTER_SCOPE]);
            }
            foreach ($formatted_routes as $key => $route) {
                if (!array_key_exists($key, $routes)) {
                    $routes[$key] = $route;
                }
            }
        }
        return $this::containerize($routes);
    }

    private function initializeRoutes(): void
    {
        if (is_null(self::$routes)) {
            self::$routes = $this::containerize();
        }
        if (!self::$routes->has(static::ROUTER_SCOPE)) {
            self::$routes[static::ROUTER_SCOPE] = $this::containerize();
        }
    }
}
