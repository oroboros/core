<?php
/*
 * The MIT License
 *
 * Copyright 2015 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\router;

/**
 * Provides standardized baseline abstraction for single valid route objects
 *
 * @author Brian Dayhoff
 */
abstract class AbstractRoute extends \Oroboros\core\abstracts\library\AbstractLibrary implements \Oroboros\core\interfaces\library\router\RouteInterface
{

    use \Oroboros\core\traits\router\RouteUtilityTrait;

    /**
     * Defines further child classes as routes
     */
    const CLASS_SCOPE = 'route';
    const ROUTE_SCOPE = null;
    const ROUTE_DYNAMIC_PATTERN_MATCH = '/{([a-zA-Z0-9\_\-]{1,})}/';

    private static $valid_scopes = [
        'http',
        'cli'
    ];
    private static $valid_protocols = [
        'get',
        'post',
        'put',
        'delete',
        'patch',
        'cli',
    ];
    private $uri = null;
    private $protocol = null;
    private $controller = null;
    private $method = null;
    private $expected_keys = [];
    private $has_expected_keys = false;
    private $expected_flags = [];
    private $has_expected_flags = false;
    private $request_arguments = null;
    private $request_flags = null;
    private $request = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        $this->verifyRouteScope();
        $this->uri = $command;
        parent::__construct($command, $arguments, $flags);
        $this->initializeArguments();
        $this->expected_keys = $this->getDynamicRouteMatchKeys();
        if (!is_null($this->expected_keys)) {
            $this->has_expected_keys = true;
        }
    }

    public function setUri(string $uri)
    {
        $this->uri = $uri;
        return $this;
    }

    public function setProtocol(string $protocol)
    {
        $this->verifyProtocol($protocol);
        $this->protocol = $protocol;
        return $this;
    }

    public function setController(string $controller)
    {
        $this->verifyController($controller);
        $this->controller = $this->getFullClassName('controller', $controller);
        return $this;
    }

    public function setControllerMethod(string $method)
    {
        $this->verifyControllerMethod($method);
        $this->method = $method;
        return $this;
    }

    public function match(\Psr\Http\Message\ServerRequestInterface $request)
    {
        $this->request = $request;
        if ($this->isDynamicRoute() && $this->checkDynamicRouteMatch($request)) {
            // Route is dynamic, a match, and has wildcard values.
            $this->request_arguments = $this->getDynamicRouteMatchArguments($request);
        } else {
            // Route is a static uri, or mismatched dynamic route
            //  (which will also correctly return false here)
            if ($request->getUri()->getPath() !== $this->uri) {
                // Uri mismatch.
                if ($request->getRequestTarget() === '/' && $this->uri === '/') {
                    d('mismatch', $this->uri, $request->getRequestTarget(), $request->getUri()->getPath());
                }
                return false;
            }
        }
        $this->request_flags = $this->getRouteFlags($request);
        if (strtolower($request->getMethod()) !== strtolower($this->protocol)) {
            // HTTP request method mismatch.
            return false;
        }
//        d('match', $this, $this->uri, $request->getRequestTarget());
        return true;
    }

    /**
     * Returns a container of any arguments extracted from the request uri, or null if none were matched
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface|null
     */
    public function getRequestArguments(): ?\Oroboros\core\interfaces\library\container\ContainerInterface
    {
        return $this->request_arguments;
    }

    /**
     * Returns a container of any flags passed into the request, or null if none were given
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface|null
     */
    public function getRequestFlags(): ?\Oroboros\core\interfaces\library\container\ContainerInterface
    {
        return $this->request_flags;
    }

    /**
     * Returns the route uri pattern
     * @return string
     */
    public function getUri(): string
    {
        return $this->uri;
    }

    /**
     * Returns the name of the controller method associated with the route.
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    private function isDynamicRoute()
    {
        return preg_match(static::ROUTE_DYNAMIC_PATTERN_MATCH, $this->uri) ? true : false;
    }

    private function checkDynamicRouteMatch(\Psr\Http\Message\ServerRequestInterface $request)
    {
        if (!$this->isDynamicRoute()) {
            return false;
        }
        $expected_uri = $this->uri;
        $uri_literal = $request->getUri()->getPath();
        $keynames = [];
        $args = [];
        preg_match_all(static::ROUTE_DYNAMIC_PATTERN_MATCH, $this->uri, $keynames);
        if (!array_key_exists(1, $keynames)) {
            // No keys recovered
            return false;
        }
        $keynames = $keynames[1];
        foreach ($keynames as $key) {
            $prefix = $this->getDynamicRouteUriPrefix($key, $expected_uri);
            $suffix = $this->getDynamicRouteUriSuffix($key, $expected_uri);
            $argument = substr($uri_literal, strlen($prefix));
            $argument = substr($argument, 0, strpos($argument, '/'));
            $args[$key] = $argument;
            $expected_uri = sprintf('%1$s%2$s%3$s', $prefix, $argument, $suffix);
        }
        return $expected_uri === $request->getUri()->getPath();
    }

    private function getDynamicRouteMatchKeys(): array
    {
        if (!$this->isDynamicRoute()) {
            return [];
        }
        $keynames = [];
        preg_match_all(static::ROUTE_DYNAMIC_PATTERN_MATCH, $this->uri, $keynames);
        if (!array_key_exists(1, $keynames)) {
            // No keys recovered
            return [];
        }
        return $keynames[1];
    }

    private function getDynamicRouteMatchArguments(\Psr\Http\Message\ServerRequestInterface $request): ?\Oroboros\core\interfaces\library\container\ContainerInterface
    {
        if (!$this->isDynamicRoute()) {
            return null;
        }
        $expected_uri = $this->uri;
        $uri_literal = $request->getUri()->getPath();
        $args = [];
        $keynames = $this->getDynamicRouteMatchKeys();
        foreach ($keynames as $key) {
            $prefix = $this->getDynamicRouteUriPrefix($key, $expected_uri);
            $suffix = $this->getDynamicRouteUriSuffix($key, $expected_uri);
            $argument = substr($uri_literal, strlen($prefix));
            $argument = substr($argument, 0, strpos($argument, '/'));
            $args[$key] = $argument;
            $expected_uri = sprintf('%1$s%2$s%3$s', $prefix, $argument, $suffix);
        }
        return $this::containerize($args);
    }

    private function getRouteFlags(\Psr\Http\Message\ServerRequestInterface $request): ?\Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $flags = [];
        $query = $request->getUri()->getQuery();
        foreach (explode('&', $query) as $parameter) {
            if (strpos($parameter, '=') === false) {
                $flags[$parameter] = true;
            } else {
                $flags[substr($parameter, 0, strpos($parameter, '='))] = urldecode(substr($parameter, strpos($parameter, '=') + 1));
            }
        }
        return $this::containerize($flags);
    }

    private function getDynamicRouteUriPrefix(string $key, string $uri)
    {
        $prefix = substr($uri, 0, strpos($uri, sprintf('{%1$s}', $key)));
        return $prefix;
    }

    private function getDynamicRouteUriSuffix(string $key, string $uri)
    {
        $suffix = substr($uri, strpos($uri, sprintf('{%1$s}', $key)) + strlen(sprintf('{%1$s}', $key)));
        return $suffix;
    }

    /**
     * @param string $command (optional)
     * @param array $arguments (optional)
     * @param array $flags (optional)
     * @return \Oroboros\core\interfaces\controller\ControllerInterface
     */
    public function getController(string $command = null, array $arguments = null, array $flags = null): \Oroboros\core\interfaces\controller\ControllerInterface
    {
        if (is_null($arguments)) {
            $arguments = [];
        }
        $arguments['request'] = $this->getRequest();
        $arguments['route'] = $this;
        $instance = $this->load('controller', $this->controller, $command, $arguments, $flags);
        return $instance;
    }

    protected function getRequest(): \Psr\Http\Message\ServerRequestInterface
    {
        if (is_null($this->request)) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. Request object has not been defined.', get_class($this)));
        }
        return $this->request;
    }

    /**
     * Verify that the extension class has declared its scope correctly
     * @throws \OutOfRangeException
     */
    private function verifyRouteScope()
    {
        if (is_null(static::ROUTE_SCOPE) || !in_array(static::ROUTE_SCOPE, self::$valid_scopes)) {
            throw new \OutOfRangeException(sprintf('Router class [%1$s] is invalid. '
                        . 'It must define class constant [%2$s] as one of [%3$s].', get_class($this), 'ROUTE_SCOPE', implode(', ', self::$valid_scopes)));
        }
    }

    /**
     * Verify that the declared controller adheres to the controller interface
     * @param string $controller
     * @throws \OutOfRangeException
     */
    private function verifyController(string $controller)
    {
        $expected = 'Oroboros\\core\\interfaces\\controller\\ControllerInterface';
        $provided = $this->getFullClassName('controller', $controller);
        if (!($provided == $expected || is_subclass_of($provided, $expected))) {
            d($provided, $expected, $controller, \Oroboros\core\library\autoload\Autoloader::getNamespaces());
            throw new \OutOfRangeException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided controller class [%2$s] is invalid. It must be a valid implementation of [%3$s].', get_class($this), $provided, $expected));
        }
    }

    /**
     * Verify that the declared method exists in the declared controller.
     * 
     * You must call `setController` prior to calling this method.
     * 
     * @param string $method
     * @throws \OutOfBoundsException
     */
    private function verifyControllerMethod(string $method)
    {
        if (is_null($this->controller)) {
            throw new \OutOfBoundsException(sprintf('Error encountered in [%1$s]. '
                        . 'Cannot verify controller method [%2$s] because the controller has not been declared.', get_class($this), $method));
        }
        if (!method_exists($this->controller, $method)) {
            throw new \OutOfBoundsException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided method [%2$s] does not exist in declared controller [%3$s].', get_class($this), $method, $this->controller));
        }
    }

    private function verifyProtocol(string $protocol)
    {
        if (!in_array($protocol, self::$valid_protocols)) {
            throw new \OutOfBoundsException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided route protocol [%2$s] is invalid. Valid protocols are [%3$s].', get_class($this), $method, implode(', ', self::$valid_protocols)));
        }
    }
}
