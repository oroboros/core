<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\definer;

/**
 * Abstract Definer
 * Provides abstraction for definer objects.
 * 
 * Definers manage the required keys needed to construct a complex set
 * of dependencies from a single object, and manage distribution of
 * repeat values to any number of uses through recursive depths.
 * Definers also understand and relay the distinction between optional
 * and required keys supported throughout their entire scope, and will prevent
 * an expensive operation from occurring if it lacks required input to prevent
 * a half-done task or unnecessary processing overhead.
 * 
 * An example would be when a given sequence of several classes need to
 *  be constructed, and all of them require the same base level namespace root,
 * or when several files need to be packaged and all require the
 * same author/publishing date/license and need to be organized hierarchically
 * in relation to each other. In these cases, the definer will manage
 * distribution of the common data, relaying all required keys for the full
 * depth back to the calling object, and sequencing them correctly for
 * distribution, as well as establishing hierarchical correlation so the
 * finished package works as expected with minimal input.
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
abstract class AbstractDefiner extends \Oroboros\core\abstracts\library\AbstractLibrary implements \Oroboros\core\interfaces\library\definer\DefinerInterface
{

    use \Oroboros\core\traits\library\resolver\ResolverUtilityTrait;

    /**
     * Use the default resolver
     */
    const RESOLVER_CLASS = 'resolver\\ArgumentResolver';

    /**
     * Stub name of the hookset class used to resolve
     * value injection into the defined content set.
     * 
     * Must be a fully qualified classname, and must resolve to a class
     * implementing `\Oroboros\core\interfaces\library\hook\HookSetInterface`
     */
    const HOOKSET_CLASS = \Oroboros\core\library\hook\HookSet::class;
    
    /**
     * The hook extractor class used by the definer to fetch hooks from the raw dataset.
     */
    const HOOK_EXTRACTOR_CLASS = 'hook\\HookExtractor';

    /**
     * The dataset returned by the child class. This represents
     * the uncompiled dataset.
     * 
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $data = null;

    /**
     * This represents the compiled dataset.
     * 
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $compiled = null;

    /**
     * The declared hookset class object
     * 
     * @var \Oroboros\core\interfaces\library\hook\HookSetInterface
     */
    private $hookset = null;

    /**
     * The given optional keys.
     * 
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $optional_keys = null;

    /**
     * The declared hook extractor object
     * @var \Oroboros\core\interfaces\library\hook\HookExtractorInterface
     */
    private $extractor = null;

    /**
     * Designates whether the data has been prepared correctly to return for use.
     * @var bool
     */
    private $data_prepared = false;

    /**
     * The default keys required for the given dataset.
     * These will be set directly into the returned dataset if they are absent.
     * @var array
     */
    private static $definer_defaults = [
        'require' => [],
        'optional' => [],
        'provide' => [],
        'defaults' => []
    ];

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeResolver();
        $this->initializeHookSet();
        $this->initializeExtractor();
        $this->initializeOptionalKeys();
        $this->loadDataSet();
        $this->prepareDataSet();
    }

    /**
     * Returns the compiled dataset if it has been compiled.
     * 
     * If the dataset is not ready for use, will raise an exception.
     * 
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     * @throws \Oroboros\core\exception\ErrorException if the dataset has not yet been compiled.
     */
    public function fetch(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        if (!$this->isCompiled()) {
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. The requested data has not yet been compiled.', get_class($this))
            );
        }
        return $this->compiled;
    }

    /**
     * Returns a boolean designation as to whether the dataset
     * is prepared for use correctly.
     * 
     * If all required keys have been satisfied and the data has been compiled,
     * this will return true. Otherwise it will return false.
     * 
     * If this returns false, an exception will be raised when attempting
     * to fetch the finished data.
     * 
     * @return bool
     */
    public function isCompiled(): bool
    {
        return $this->data_prepared;
    }

    /**
     * Returns a boolean determination as to whether the final data set
     * can compile with the current set of satisfied keys.
     * 
     * @return bool
     */
    public function canCompile(): bool
    {
        foreach ($this->data['require'] as $key => $type) {
            if (!$this->hookset->has($key)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Compiles the final dataset output. All required keys must be satisfied.
     * An ErrorException will be raised if there are missing required keys.
     * 
     * @return \Oroboros\core\interfaces\library\definer\DefinerInterface returns $this (method chainable)
     * @throws \Oroboros\core\exception\ErrorException if any required keys have not yet been passed definitions.
     */
    public function compile(): \Oroboros\core\interfaces\library\definer\DefinerInterface
    {
        if (!$this->canCompile()) {
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. Cannot compile dataset '
                        . 'because required keys [%2$s] do not have definitions.'
                        , get_class($this), implode(', ', array_keys($this->getUnsatisfiedRequiredKeys())))
            );
        }
        // Repackage data so compiled is not a reference to 
        // the same object as the data property.
        $data = $this->containerize($this->data['defaults']->toArray());
        $data = $this->injectRequiredKeys($data, $this->hookset);
        $data = $this->resolveRequiredKeys($data, $this->hookset);
        $data = $this->resolveOptionalKeys($data, $this->optional_keys);
        $this->compiled = $this->formatCompiled($data);
        $this->data_prepared = true;
        return $this;
    }

    /**
     * Returns a container with all of the required keys,
     * where the key is their key name and the value is
     * the required format.
     * 
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    public function getRequired(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        return $this->data['require'];
    }

    /**
     * Returns an associative array of any currently unsatisfied required keys,
     * where the key is the keyname and the value is the expected format
     * 
     * @return array
     */
    public function getUnsatisfiedRequiredKeys(): array
    {
        $unsatisfied = [];
        foreach ($this->getRequired() as $key => $format) {
            if (!$this->hookset->has($key)) {
                $unsatisfied[$key] = $format;
            }
        }
        return $unsatisfied;
    }

    /**
     * Returns a boolean designation as to whether any required keys
     * are currently unsatisfied.
     * 
     * @return bool
     */
    public function hasUnsatisfiedRequiredKeys(): bool
    {
        return !empty($this->getUnsatisfiedRequiredKeys());
    }

    /**
     * Returns a boolean designation as to whether a supplied
     * key name is required for the dataset to compile.
     * 
     * @param string $key The name of the key to evaluate
     * @return bool
     */
    public function hasRequiredKey(string $key): bool
    {
        return $this->data['require']->has($key);
    }

    /**
     * Returns a boolean determination as to whether a given required key is satisfied.
     * 
     * Returns true if it has a value supplied and false if not.
     * 
     * If a non-existent key is passed, raises an exception.
     * 
     * @param string $key
     * @return bool
     * @throws \Oroboros\core\exception\InvalidArgumentException if the given key is not a required key
     */
    public function isRequiredKeySatisfied(string $key): bool
    {
        if (!$this->hasRequiredKey($key)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Supplied key [%2$s] is not a required key.', get_class($this), $key)
            );
        }
        return $this->hookset->has($key);
    }

    /**
     * Sets the insert value for the given required key.
     * 
     * This will raise an exception if the value is the wrong type,
     * or if the key is not a required key.
     * 
     * This can be checked safely with `hasRequiredKey`
     * and `isValidRequiredValue` respectively.
     * 
     * @param string $key
     * @param type $value
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException if the designated key
     *         is not a required key, or if the type of the value is not valid
     *         for the required type.
     */
    public function setRequiredKey(string $key, $value): void
    {
        if (!$this->hasRequiredKey($key)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Provided key [%2$s] is not a required key.', get_class($this), $key)
            );
        }
        if (!$this->getResolver()->resolve($value, $this->data['require'][$key])) {
            // @todo Refactor this to use the resolver to obtain acceptable 
            // and given types when it has appropriate abstraction to accomplish this.
            // For now it's check functionality is solid, so the important bit 
            // is already in place.
            $valid = $this->data['require'][$key];
            if (is_iterable($this->data['require'][$key])) {
                $valid = [];
                foreach ($this->data['require'][$key] as $allow) {
                    if (is_string($allow)) {
                        $valid[] = $allow;
                    } else {
                        $valid[] = gettype($allow);
                    }
                }
                $valid = implode(', ', $valid);
            }
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Provided required key [%2$s] '
                        . 'requires type [%3$s], but the supplied data is of '
                        . 'type [%4$s].', get_class($this), $key, $valid, gettype($value))
            );
        }
        try {
            $this->hookset[$key] = $value;
        } catch (\Oroboros\core\exception\container\ContainerException $e) {
            d($key, $value, $e->getMessage(), $e->getTrace());
            exit;
        }
    }

    /**
     * Returns the current value for the given required key.
     * @param string $key
     * @return mixed
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    public function getRequiredValue(string $key)
    {
        if (!$this->hookset->has($key)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Requested key [%2$s] is not defined.', get_class($this), $key)
            );
        }
        return $this->hookset[$key];
    }

    /**
     * Returns a boolean designation as to whether a supplied
     * value satisfies the type requirement of a required key.
     * 
     * If the keyname exists and the value is valid, returns true.
     * Otherwise returns false.
     * 
     * @param string $key The name of the key to evaluate
     * @return bool
     */
    public function isValidRequiredValue(string $key, $value): bool
    {
        if (!$this->hasRequiredKey($key)) {
            return false;
        }
        return $this->getResolver()->resolve($value, $this->data['required'][$key]);
    }

    /**
     * Returns a container with all of the optional keys,
     * where the key is their key name and the value is
     * their required format.
     * 
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    public function getOptional(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        return $this->data['optional'];
    }

    /**
     * Returns an associative array of any currently unsatisfied optional keys,
     * where the key is the keyname and the value is the expected format
     * 
     * @return array
     */
    public function getUnsatisfiedOptionalKeys(): array
    {
        $unsatisfied = [];
        foreach ($this->getOptional() as $key => $format) {
            if (!$this->optional_keys->has($key)) {
                $unsatisfied[$key] = $format;
            }
        }
        return $unsatisfied;
    }

    /**
     * Returns a boolean designation as to whether any optional keys
     * are currently unsatisfied.
     * 
     * @return bool
     */
    public function hasUnsatisfiedOptionalKeys(): bool
    {
        return !empty($this->getUnsatisfiedOptionalKeys());
    }

    /**
     * Returns a boolean designation as to whether a supplied
     * key name is within the set of optional keys acceptable
     * for the dataset to compile with.
     * 
     * @param string $key The name of the key to evaluate
     * @return bool
     */
    public function hasOptionalKey(string $key): bool
    {
        return $this->data['optional']->has($key);
    }

    /**
     * Returns a boolean determination as to whether a given required key is satisfied.
     * 
     * Returns true if it has a value supplied and false if not.
     * 
     * If a non-existent key is passed, raises an exception.
     * 
     * @param string $key
     * @return bool
     * @throws \Oroboros\core\exception\InvalidArgumentException if the given key is not an optional key
     */
    public function isOptionalKeySatisfied(string $key): bool
    {
        if (!$this->hasOptionalKey($key)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Supplied key [%2$s] is not an optional key.', get_class($this), $key)
            );
        }
        return $this->optional_keys->has($key);
    }

    /**
     * Sets the insert value for the given optional key.
     * 
     * This will raise an exception if the value is the wrong type,
     * or if the key is not an optional key.
     * 
     * This can be checked safely with `hasOptionalKey`
     * and `isValidOptionalValue` respectively.
     * 
     * @param string $key
     * @param type $value
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException if the designated key
     *         is not an optional key, or if the type of the value is not valid
     *         for the required type.
     */
    public function setOptionalKey(string $key, $value): void
    {
        if (!$this->hasOptionalKey($key)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Provided key [%2$s] is not an optional key.', get_class($this), $key)
            );
        }
        if (!$this->getResolver()->resolve($value, $this->data['optional'][$key])) {
            // @todo Refactor this to use the resolver to obtain acceptable 
            // and given types when it has appropriate abstraction to accomplish this.
            // For now it's check functionality is solid, so the important bit 
            // is already in place.
            $valid = $this->data['optional'][$key];
            if (is_iterable($this->data['optional'][$key])) {
                $valid = [];
                foreach ($this->data['optional'][$key] as $allow) {
                    if (is_string($allow)) {
                        $valid[] = $allow;
                    } else {
                        $valid[] = gettype($allow);
                    }
                }
                $valid = implode(', ', $valid);
            }
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Provided required key [%2$s] '
                        . 'requires type [%3$s], but the supplied data is of '
                        . 'type [%4$s].', get_class($this), $key, $valid, gettype($value))
            );
        }
        $this->optional_keys[$key] = $value;
    }

    /**
     * Returns the current value for the given optional key.
     * @param string $key
     * @return mixed
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    public function getOptionalValue(string $key)
    {
        if (!$this->optional_keys->has($key)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Requested key [%2$s] is not defined.', get_class($this), $key)
            );
        }
        return $this->optional_keys[$key];
    }

    /**
     * Returns a boolean designation as to whether a supplied
     * value satisfies the type requirement of a required key.
     * 
     * If the keyname exists and the value is valid, returns true.
     * Otherwise returns false.
     * 
     * @param string $key The name of the key to evaluate
     * @return bool
     */
    public function isValidOptionalValue(string $key, $value): bool
    {
        if (!$this->hasOptionalKey($key)) {
            return false;
        }
        return $this->getResolver()->resolve($value, $this->data['optional'][$key]);
    }

    /**
     * This method must return a container of the data values
     * used to establish how the definer operates.
     * 
     * may contain the following keys:
     * - required: an associative array of keys where the key is the required key
     *             and the format is the variable type that is acceptable
     * - optional: an associative array of keys where the key is the optional key
     *             and the format is the variable type that is acceptable
     * - provide:  an associative array of keys where the key is the provided identifier,
     *             and the value is the injectable value.
     * - defaults: an associative array of keys to use as default data. Values
     *             may be any primitive value, null, any iterable object, or a
     *             multidimensional array. Objects resolving to strings will be
     *             string cast prior to searching, and arrays will be
     *             containerized recursively.
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    abstract protected function declareDataSet(): \Oroboros\core\interfaces\library\container\ContainerInterface;

    /**
     * Validates the dataset to check for the presence of all required keys for the current scope.
     * This method may be overridden to check for the presence of custom keys.
     * If additional keys are required and are not present,
     * an `\Oroboros\core\exception\ErrorException` should be thrown.
     * 
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $data
     * @return void
     * @throws \Oroboros\core\exception\ErrorException if any required keys are missing from the dataset
     */
    protected function validateDataSet(\Oroboros\core\interfaces\library\container\ContainerInterface $data): void
    {
        // no-op
    }

    /**
     * Returns the full dataset to child classes for further transformation as needed.
     * 
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     * @throws \Oroboros\core\exception\core\InvalidClassException
     *         if an internal request for the data was made before it had a
     *         chance to resolve, indicating the class is broken
     */
    protected function getDataSet(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        if (is_null($this->data)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. Internal request for the '
                        . 'data set was made before it has been prepared. '
                        . 'This class cannot be used in it\'s current state.', get_class($this))
            );
        }
        return $this->data;
    }

    /**
     * Override this method to perform any required key operations that need to be resolved.
     * Parameter `$data` will contain the dataset with all required keys already resolved.
     * Parameter `$required` will contain any registered required keys passed to the definer.
     * This should return the `$data` object (or equivalent container) for final transformation.
     * 
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $data
     * @param \Oroboros\core\interfaces\library\hook\HookSetInterface $required
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    protected function resolveRequiredKeys(\Oroboros\core\interfaces\library\container\ContainerInterface $data, \Oroboros\core\interfaces\library\hook\HookSetInterface $required): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        return $data;
    }

    /**
     * Override this method to perform any optional key operations that need to be resolved.
     * Parameter `$data` will contain the dataset with all required keys already resolved.
     * Parameter `$optional` will contain any registered optional keys passed to the definer.
     * This should return the `$data` object (or equivalent container) for final transformation.
     * 
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $data
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $optional
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    protected function resolveOptionalKeys(\Oroboros\core\interfaces\library\container\ContainerInterface $data, \Oroboros\core\interfaces\library\container\ContainerInterface $optional): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        return $data;
    }

    /**
     * Recursively iterates over the dataset and performs replacements
     * for required keys on the existing hooks anywhere within the data definition.
     * 
     * @param mixed $data
     * @param \Oroboros\core\interfaces\library\hook\HookSetInterface $hooks
     * @return mixed
     */
    protected function injectRequiredKeys($data, \Oroboros\core\interfaces\library\hook\HookSetInterface $hooks)
    {
        if (is_string($data)) {
            $data = $hooks->replace($data);
        }
        if (is_iterable($data)) {
            foreach ($data as $key => $value) {
                $data[$key] = $this->injectRequiredKeys($value, $hooks);
            }
        }
        return $data;
    }

    /**
     * Prepares the initial dataset for external operations.
     * Sets defaults if they do not exist, formats the dataset,
     * and fetches existing required keys from anywhere in
     * the dataset tree recursively.
     * 
     * @return void
     */
    protected function prepareDataSet(): void
    {
        $formatted = [];
        $keys = $this->extractRequiredDataKeys($this->data['defaults']);
        foreach ($keys as $key) {
            if (array_key_exists($key, $this->data['require'])) {
                $existing = $this->data['require'][$key];
                if (is_string($existing)) {
                    $existing = [$existing];
                }
                $formatted[$key] = $existing;
                continue;
            }
            // Default resolution
            $formatted[$key] = [null, false, 'string'];
        }
        $existing = $this->data['require'];
        $this->data['require'] = $this->containerize(array_replace_recursive($existing, $formatted));
        $this->data = $this->formatDataSet($this->data);
    }

    /**
     * Overrride this method if you wish to declare a
     * predefined set of default values for required keys.
     * 
     * These may be overridden externally before the object compiles.
     * 
     * @return array
     */
    protected function declareDefaultRequiredKeys(): array
    {
        return [];
    }

    /**
     * Override this method if you wish to declare a
     * predefined set of values for optional keys.
     * 
     * These may be overridden externally before the object compiles.
     * 
     * @return array
     */
    protected function declareDefaultOptionalKeys(): array
    {
        return [];
    }

    /**
     * Override this method if you wish to declare a
     * predefined set of values for provided keys.
     * 
     * These will be merged with any that exist within the
     * supplied data source provided via `declareDataSet`
     * 
     * Provided keys may **not** be externally edited after compiling,
     * however they may be overridden if the object defines a key as
     * both required/optional as well as provided.
     * 
     * @return array
     */
    protected function declareDefaultProvidedKeys(): array
    {
        return [];
    }

    /**
     * Recursively analyzes the dataset and containerizes
     * arrays for ease of value checking.
     * 
     * @param mixed $data dataset to aggregate
     * @param string|null $keyname the name of the key for the current depth level, if any.
     * @return mixed
     */
    protected function formatDataSet($data, string $keyname = null)
    {
        if (!is_iterable($data)) {
            return $data;
        }
        foreach ($data as $key => $value) {
            $data[$key] = $this->formatDataSet($value, $key);
        }
        if (!(is_object($data) && $data instanceof \Oroboros\core\interfaces\library\container\ContainerInterface)) {
            $data = $this->containerize($data);
        }
        return $data;
    }

    /**
     * You may override this method to perform any final transformations on the
     * resulting output dataset.
     * 
     * Data will be handed to this immediately before being stored internally.
     * 
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $data
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    protected function formatCompiled(\Oroboros\core\interfaces\library\container\ContainerInterface $data): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        return $data;
    }

    /**
     * Recursively analyzes the dataset and extracts any hook definitions,
     * returning the full set of found hooks at the end to be declared
     * as required keys.
     * 
     * @param mixed $data the data currently being analyzed
     * @param array $keys existing required keys found during recursion
     * @return array any keys extracted from the data
     */
    private function extractRequiredDataKeys($data, array $keys = []): array
    {
        $new_keys = [];
        if (is_object($data) && $data instanceof \Oroboros\core\interfaces\library\container\ContainerInterface) {
            $data = $data->toArray();
        }
        if (is_array($data)) {
            foreach ($data as $key => $value) {
                $new_keys = array_merge($new_keys, $this->extractRequiredDataKeys($value));
            }
        }
        if (is_string($data) && $this->extractor->hasKeys($data)) {
            foreach ($this->extractor->getKeys($data) as $key) {
                if (!in_array($key, $new_keys)) {
                    $new_keys[] = $key;
                }
            }
        }
        foreach ($new_keys as $key) {
            if (!in_array($key, $keys)) {
                $keys[] = $key;
            }
        }
        return $keys;
    }

    /**
     * Loads the initial dataset and sets it into the class property.
     * 
     * @return void
     * @throws \Oroboros\core\exception\core\InvalidClassException
     *         Bubbles these up if they occur to be handled by the bootstrap.
     *         These mean the child class is broken and needs refactoring.
     * @throws \Oroboros\core\exception\ErrorException
     *         If data does not validate (as per child class determination)
     */
    private function loadDataSet(): void
    {
        try {
            $data = $this->containerize(array_replace_recursive(self::$definer_defaults, $this->declareDataSet()->toArray()));
            $this->validateDataSet($data);
            $this->data = $data;
            if (!$data->has('provide')) {
                $data['provide'] = [];
            }
            if (is_array($data['provide'])) {
                $data['provide'] = $this->containerize($data['provide']);
            }
            foreach ($this->declareDefaultProvidedKeys() as $key => $value) {
                if (!$data['provide']->has($key)) {
                    $data['provide'][$key] = $value;
                }
            }
            if ($data->has('provide')) {
                foreach ($data['provide'] as $key => $value) {
                    if (!$this->hookset->has($key)) {
                        $this->hookset[$key] = $value;
                    }
                }
            }
        } catch (\Exception $e) {
            if ($e instanceof \Oroboros\core\exception\core\InvalidClassException) {
                // We don't catch these, they must bubble directly up to the bootstrap.
                throw $e;
            }
            // All other exception types will be handled as error exceptions.
            // They may be caught and handled as appropriate.
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. An error was encountered '
                        . 'attempting to obtain the specified data set. An exception '
                        . 'of type [%2$s] was raised with message [%3$s], '
                        . 'at line [%4$s] of file [%5$s].'
                        , get_class($this), get_class($e), $e->getMessage(), $e->getLine(), $e->getFile())
            );
        }
    }

    /**
     * Initializes the hookset object.
     * @return void
     * @throws \Oroboros\core\exception\core\InvalidClassException if the hookset
     *         class constant is not valid, or if it can't be initialized as
     *         a container at all.
     */
    private function initializeHookSet(): void
    {
        if (is_null($this->hookset)) {
            $hookset = $this->containerizeInto(static::HOOKSET_CLASS, []);
            $expected_interface = 'Oroboros\\core\\interfaces\\library\\hook\\HookSetInterface';
            $interfaces = class_implements($hookset);
            if (!in_array($expected_interface, $interfaces)) {
                throw new \Oroboros\core\exception\core\InvalidClassException(
                        sprintf('Error encountered in [%1$s]. Class constant [%2$s] '
                            . 'declared an invalid value. Expected a classname '
                            . 'implementing [%3$s].', get_class($this), 'HOOKSET_CLASS', $expected_interface)
                );
            }
            $this->hookset = $hookset;
        }
    }

    /**
     * Initializes the hook extractor object.
     * @return void
     * @throws \Oroboros\core\exception\core\InvalidClassException if the extractor
     *         class constant is not valid
     */
    private function initializeExtractor(): void
    {
        if (is_null($this->extractor)) {
            $class = static::HOOK_EXTRACTOR_CLASS;
            $classname = $this->getFullClassName('library', $class);
            if ($classname === false) {
                throw new \Oroboros\core\exception\core\InvalidClassException(
                        sprintf('Error encountered in [%1$s]. Class constant [%2$s] '
                            . 'declared an invalid value. Expected a classname '
                            . 'or stub classname, received [%3$s].'
                            . 'This class is not useable in it\'s current state.'
                            , get_class($this), 'HOOK_EXTRACTOR_CLASS'
                            , ((is_string(static::HOOK_EXTRACTOR_CLASS)) ? static::HOOK_EXTRACTOR_CLASS : gettype(static::HOOK_EXTRACTOR_CLASS)))
                );
            }
            $expected_interface = 'Oroboros\\core\\interfaces\\library\\hook\\HookExtractorInterface';
            $interfaces = class_implements($classname);
            if (!in_array($expected_interface, $interfaces)) {
                throw new \Oroboros\core\exception\core\InvalidClassException(
                        sprintf('Error encountered in [%1$s]. Class constant [%2$s] '
                            . 'declared an invalid value. Expected a classname '
                            . 'implementing [%3$s], declared class resolving to '
                            . '[%4$s] does not. This class is not useable in it\'s current state.'
                            , get_class($this), 'HOOKSET_CLASS', $expected_interface, $classname)
                );
            }
            $this->extractor = $this->load('library', static::HOOK_EXTRACTOR_CLASS);
        }
    }

    /**
     * Initializes the optional key set
     * @return void
     */
    private function initializeOptionalKeys(): void
    {
        if (is_null($this->optional_keys)) {
            $this->optional_keys = $this->containerize();
        }
    }
}
