<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\library\form;

/**
 * Abstract Form Data Parser
 * Provides abstraction for parsing form data returned from clients
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
abstract class AbstractFormDataParser extends \Oroboros\core\abstracts\library\parser\AbstractParser implements \Oroboros\core\interfaces\library\form\FormParserInterface
{

    /**
     * For multipart forms
     */
    const COLLECTION_CLASS = \Oroboros\core\library\form\FormDataCollection::class;

    /**
     * For single forms or sections of multipart forms
     */
    const CONTAINER_CLASS = \Oroboros\core\library\form\FormData::class;

    /**
     * Indicates that the parser parses form data
     */
    const PARSER_FORMAT = 'form';

    /**
     * Indicates the type of form parsed by the parser
     */
    const FORM_TYPE = 'application/x-www-form-urlencoded';

    /**
     * This constant may be overridden to declare
     * a key prefix to remove from form keys.
     */
    const KEY_PREFIX = null;

    /**
     * Parses the file of form values or psr7 ServerRequest data provided as a
     * constructor arg. If neither of these are present or the form does not
     * follow the declared format for the form parsser, throws an \OutOfBoundsException.
     * 
     * @return $this (method chainable)
     * @throws \OutOfBoundsException If no request argument was passed into the 
     *         constructor, or the form type does not match the format the
     *         parser handles.
     */
    public function parse($file_location)
    {
        $results = [];
        // Handle file-based cases
        if (is_string($file_location) && is_readable($file_location)) {
            return parent::parse($file_location);
        }
        if ($this->hasArgument('request')) {
            foreach($this->parseServerRequest($this->getArgument('request')) as $key => $value) {
                $data[$this->formatKey($key)] = $value;
            }
        }
        $this->initializeContainer($data);
        return $this;
    }

    protected function parseFile(string $file_location): array
    {
        d($file_location, $this);
        exit;
    }

    protected function parseServerRequest(\Psr\Http\Message\ServerRequestInterface $request)
    {
        if (!$request->hasHeader('content-type') || !in_array(static::FORM_TYPE, $request->getHeader('content-type'))) {
            throw new \Oroboros\core\exception\OutOfBoundsException(
                    'Error encountered in [%1$s]. Expected form type is [%2$s], '
                    . 'provided data does not satisfy this format.'
                    , get_class($this), static::FORM_TYPE
            );
        }
        $results = $request->getParsedBody();
        return $results;
    }

    private function formatKey(string $key): string
    {
        if (is_null(static::KEY_PREFIX)) {
            return $key;
        }
        if (strpos($key, static::KEY_PREFIX) === 0 && $key !== static::KEY_PREFIX) {
            $key = ltrim(substr($key, strlen(static::KEY_PREFIX)), '-_.');
        }
        return $key;
    }
}
