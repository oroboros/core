<?php
namespace Oroboros\core\abstracts\controller;

/**
 * Abstract Controller
 * 
 * Sets the abstract foundation for how
 * controllers enforce the logical flow of operation.
 *
 * @author Brian Dayhoff
 */
class AbstractController extends \Oroboros\core\abstracts\AbstractBase implements \Oroboros\core\interfaces\controller\ControllerInterface
{

    use \Oroboros\core\traits\LoaderTrait;
    use \Oroboros\core\traits\ContainerPackagerUtility;
    use \Oroboros\core\traits\StringUtilityTrait;
    use \Oroboros\core\traits\pattern\ObservableTrait;
    use \Oroboros\core\traits\pattern\ObserverTrait;
    use \Oroboros\core\traits\pattern\ResponseDirectorUtilityTrait;
    use \Oroboros\core\traits\job\JobQueueUtilityTrait;
    use \Oroboros\core\traits\pattern\EventAwareTrait;
    use \Oroboros\core\traits\pattern\FilterAwareTrait;
    use \Oroboros\core\traits\model\ModelAwareUtilityTrait;

    /**
     * Designates further child classes as controllers.
     */
    const CLASS_TYPE = 'controller';

    /**
     * Abstract controller does not define the class scope.
     * This must be done by the individual controller.
     * 
     * Controllers may alternately be further sub-classed by
     * intended protocol if desired (eg html, cli, cgi, etc),
     * however this will almost certainly require code duplication,
     * and is suggested to be avoided for ongoing maintenance purposes.
     */
    const CLASS_SCOPE = null;

    /**
     * Child classes must define an error controller that will be used in
     * the event that an error occurs during controller logic.
     */
    const ERROR_CONTROLLER = null;

    /**
     * Child classes must define a response builder that will be used to collect
     * output data and package it for the view correctly.
     */
    const RESPONSE_DIRECTOR_CLASS = null;

    /**
     * Child classes that belong to a module must define their module key.
     * This should match the name in the manifest.
     */
    const MODULE_NAME = null;

    /**
     * Response data from the view.
     * This is returned to the bootstrap object to output,
     * which keeps the bootstrap object and the view decoupled
     * @var 
     */
    private $response = null;

    /**
     * Use the default container
     */
    const CONTAINER_CLASS = 'Oroboros\\core\\library\\container\\Container';

    private static $valid_class_scopes = [
        'frontend',
        'dashboard',
        'login',
        'maintenance',
        'error',
        'http',
        'xml',
        'plaintext',
        'html',
        'rest',
        'ajax',
        'socket',
        'cli',
        'job',
    ];
    private static $view_types = [
        'cli' => 'cli\\CliView',
        'cli-interactive' => 'cli\\InteractiveCliView',
        'html' => 'html\\PageView',
        'xml' => 'xml\\XmlView',
        'css' => 'css\\CssView',
        'javascript' => 'javascript\\JavascriptView',
        'json' => 'json\\JsonView',
    ];

    /**
     * The error controller to use when an exception is raised
     * or a method cannot resolve.
     * @var \Oroboros\core\interfaces\controller\ErrorControllerInterface
     */
    private $error_controller = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        $this->verifyClassScope();
        $this->verifyResponseDirector();
        parent::__construct($command, $arguments, $flags);
        $this->initializeModelAware();
        $this->verifyErrorController(); // Needs to be done after base instantiation because it needs the getters for args/flags/command
        $this->initializeObservable(); // Controllers are observable to trigger response info being sent by the bootstrap.
        $this->initializeObserver();
        $this->initializeEvents();
        $this->initializeFilters();
        if ($this->hasEventQueue('init')) {
            $this->scopeEventQueue('init');
        }
        if ($this->hasFilterQueue('init')) {
            $this->scopeFilterQueue('init');
        }
        if ($this->hasEvent('pre-initialize')) {
            $this->fireEvent('pre-initialize');
        }
        $this->initializeResponseBuilder(); // Needs to be done after base instantiation in the event that dependency injection occurred
        $this->initializeJobQueues();
        if ($this->hasEvent('post-initialize')) {
            $this->fireEvent('post-initialize', $this->getEventArguments());
        }
        if ($this->hasEventQueue('ready')) {
            $this->scopeEventQueue('ready');
        }
        if ($this->hasFilterQueue('ready')) {
            $this->scopeFilterQueue('ready');
        }
        $this->setObserverEvent('ready');
    }

    /**
     * Only fires the teardown event
     */
    public function __destruct()
    {
        if ($this->hasEventQueue('teardown')) {
            $this->scopeEventQueue('teardown');
        }
        if ($this->hasFilterQueue('teardown')) {
            $this->scopeFilterQueue('teardown');
        }
        $this->setObserverEvent('teardown');
        if ($this->hasEvent('cleanup')) {
            $this->fireEvent('cleanup', $this->getEventArguments());
        }
        $this->getLogger()->debug('[type][scope][class] Fired controller destructor.', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
        ]);
    }

    /**
     * Handles cli errors that occur within the controller scope.
     * 
     * @param int $code The status code for the error
     * @param string $message Optional message body
     */
    public function error(int $code = null, string $message = null, \Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\ErrorControllerInterface
    {
        if (is_null($arguments)) {
            // Empty container if no arguments passed
            $arguments = $this->containerize();
        }
        if (is_null($flags)) {
            // Controller flags if no flags passed
            $flags = $this->containerize($this->getFlags());
        }
        if ($this::app()->debugEnabled()) {
            $arguments['debug'] = $this::app()->debugEnabled();
            $arguments['trace'] = debug_backtrace(1);
        }
        if ($this->hasEventQueue('error')) {
            $this->scopeEventQueue('error');
        }
        if ($this->hasFilterQueue('error')) {
            $this->scopeFilterQueue('error');
        }
        if ($this->hasEvent('error')) {
            $this->fireEvent('error', $this->getEventArguments([
                    'error-code' => $code,
                    'error-message' => $message,
                    'error-arguments' => $arguments,
                    'error-flags' => $flags
            ]));
        }
        $this->setObserverEvent('error');
        return $this->getErrorController()->error($code, $message, $arguments, $flags);
    }

    /**
     * Registers a given view type as valid for the controller scope being referenced.
     * 
     * This method is used to allow modules and extensions to register new views
     * to handle more granular purposes than the underlying system ships with.
     * 
     * @param string $key A canonicalized lower case string representing
     *        the identifier for the view type.
     * @param string $class The name of a concrete class implementing
     *        `\Oroboros\core\interfaces\view\ViewInterface`
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException if the key is
     *         malformed, or the given class name does not
     *         resolve to a concrete class implementing
     *         `\Oroboros\core\interfaces\view\ViewInterface`
     */
    public static function addViewType(string $key, string $class): void
    {
        $proxy = new \Oroboros\core\pattern\factory\LoaderProxy();
        if (!$proxy->getFullClassName('view', $class)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Provided class [%2$s] for key [%3$s] is not a valid view class.', get_called_class(), $class, $key));
        }
        self::$view_types[$key] = $class;
    }

    /**
     * Makes an added class scope known.
     * If this method *has not* been called on the underlying abstract to
     * declare a scope valid, then it must raise a
     * `\Oroboros\core\exception\OutOfBoundsException` in it's constructor if a
     * child class with the unknown scope attempts to call `parent::__construct`
     * 
     * Child classes are responsible for registering their own scopes prior
     * to calling `parent::__construct`
     * 
     * Class scopes are used extensively to relationally group objects that
     * work within a similar scope but are otherwise unrelated.
     * 
     * @param string $scope
     * @return void
     */
    public static function addClassScope(string $scope): void
    {
        if (!in_array($scope, self::$valid_class_scopes)) {
            self::$valid_class_scopes[] = $scope;
        }
    }

    /**
     * Performs the render step after any controller route
     * method has been executed.
     * 
     * This method is internally responsible for dispatch, handling views,
     * and sending the response body to whatever client it is interacting with.
     * 
     * @param string $format
     * @param string $template
     * @param array $flags
     * @return \Oroboros\core\interfaces\controller\ControllerInterface
     * @throws \Oroboros\core\exception\OutOfBoundsException if the `$format`
     *         does not correspond to a known view type.
     */
    public function render(string $format, string $template, array $flags = null): \Oroboros\core\interfaces\controller\ControllerInterface
    {
        $current_event_queue = $this->getCurrentEventQueue();
        if ($this->hasEventQueue('render')) {
            $this->scopeEventQueue('render');
        }
        $this->getLogger()->debug('[type][scope][class] Firing pre-render event.', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
        ]);
        if ($this->hasEvent('pre-render')) {
            $this->fireEvent('pre-render', $this->getEventArguments([
                    'render-format' => $format,
                    'render-template' => $template,
                    'render-flags' => $flags
            ]));
        }
        if (!array_key_exists($format, self::$view_types)) {
            $this->getLogger()->warning('[type][scope][class] Unknown view format [format] referenced in controller.', [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
                'format' => $format,
            ]);
            throw new \Oroboros\core\exception\OutOfBoundsException(sprintf('Error encountered in [%1$s]. No known view for format [%2$s].', get_class($this), $format));
        }
        $view = $this->load('view', self::$view_types[$format], null, ['data' => $this->buildOutput($this->getFlags())], $flags);
        $this->response = $view->render($this->declareTemplateName($format, $template));
        $this->setObserverEvent('render');
        $this->scopeEventQueue($current_event_queue);
        $this->getLogger()->debug('[type][scope][class] Firing post-render event.', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
        ]);
        if ($this->hasEvent('post-render')) {
            $this->fireEvent('pre-render', $this->getEventArguments([
                    'render-format' => $format,
                    'render-template' => $template,
                    'render-flags' => $flags,
                    'view' => $view,
                    'response' => $this->getResponse()
            ]));
        }
        return $this;
    }

    /**
     * This method will receive the user object and the currently selected route
     * to determine if the given user is properly authenticated to call the
     * selected controller method.
     * 
     * This will always be asked by the bootstrap object prior to calling
     * a controller method.
     * 
     * Controllers subrouting should also execute this method prior to calling
     * any method on another controller.
     * 
     * You may override this method to add any relevant authentication constraints
     * to the method being referenced appropriate to the controller scope.
     * 
     * Additional authentication may be required for partial content, which may
     * be performed within the body of the method referenced. However, if this
     * method returns `false`, calling the referenced method with the same user
     * should *always* generate a not authorized or forbidden error, and if it
     * returns `true`, then calling the same method *should not* generate a
     * not authorized or forbidden error.
     * 
     * This method **must not** prevent execution of the method named `error`.
     * The `error` method will be called if this method returns `false`.
     * 
     * @note The default implementation only returns `false` if the given method
     *       does not exist within this controller.
     * 
     * @note you may supply permissions directly to controller methods
     *       by supplying them from method `declareMethodPermissions`
     *       without any need to alter this method.
     * 
     * @param string $method The name of the method to call on the controller
     * @param \Oroboros\core\interfaces\library\user\UserInterface $user
     *        The object representing the currently scoped user.
     * @return bool
     * @throws \Oroboros\core\exception\auth\AuthenticationException
     *         If any authentication criteria are invalid
     */
    public function authenticate(string $method, \Oroboros\core\interfaces\library\user\UserInterface $user): bool
    {
        if ($this::user()->isBlacklisted()) {
            throw new \Oroboros\core\exception\auth\AuthenticationException('You are banned from this site.', 403);
        }
        if (!method_exists($this, $method)) {
            $this->getLogger()->warning('[type][scope][class] No known authentication method [method]', [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
                'method' => $method,
            ]);
            return false;
        }
        foreach ($this->declareMethodPermissions() as $key => $permission) {
            if ($key !== $method && $key !== '*') {
                continue;
            }
            if (!$this::user()->can($permission)) {
                throw new \Oroboros\core\exception\auth\AuthenticationException('You are not authorized to view this content.', 403);
            }
            if ($key !== '*') {
                break;
            }
        }
        return true;
    }

    /**
     * Provides the response data to the bootstrap object,
     * so it can be sent to the client.
     * @return type
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * Returns the error controller for the given controller scope
     * @return \Oroboros\core\interfaces\controller\ErrorControllerInterface
     */
    public function getErrorController(): \Oroboros\core\interfaces\controller\ErrorControllerInterface
    {
        $this->setObserverEvent('get-error-controller');
        return $this->error_controller;
    }

    protected function getView(string $type, $data = null, array $flags = null): \Oroboros\core\interfaces\view\ViewInterface
    {
        if (!array_key_exists($type, self::$view_types)) {
            $this->getLogger()->warning('[type][scope][class] Unknown view type [view-type] referenced in controller.', [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
                'view-type' => $type,
            ]);
            throw new \OutOfBoundsException(sprintf('Error encountered in [%1$s]. No known view for format [%2$s].', get_class($this), $type));
        }
        $args = null;
        if (!is_null($data)) {
            $args = ['data' => $data];
        }
        if (is_null($flags)) {
            $flags = $this->getFlags();
        }
        $this->setObserverEvent('get-view');
        $this->getLogger()->debug('[type][scope][class] Loaded view of type [view-type] with class [view-class]', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'view-type' => $type,
            'view-class' => self::$view_types[$type],
        ]);
        return $this->load('view', self::$view_types[$type], null, $args, $flags);
    }

    /**
     * Declares the template name used to render the response.
     * Can be overridden for alternate behavior.
     * @param string $format
     * @param string $template
     * @return string
     */
    protected function declareTemplateName(string $format, string $template): string
    {
        return $format . DIRECTORY_SEPARATOR . 'layout' . DIRECTORY_SEPARATOR . $template;
    }

    /**
     * Sets the observable name for the view
     * @return string
     */
    protected function declareObservableName(): string
    {
        return trim(sprintf('%1$s:%2$s', static::CLASS_TYPE, static::CLASS_SCOPE), ':');
    }

    /**
     * You may override this method to return a
     * list of permissions for calling controller methods.
     * 
     * If the requested user does not have sufficient privileges,
     * the controller method will never be called and they will
     * simply be served a frontend 404 page.
     * 
     * This method should return an associative array where the key
     * is the method name, and the value is the authentication
     * permission for the method.
     * 
     * More complex permission matching can also be done within
     * the controller method itself.
     * 
     * @return array
     */
    protected function declareMethodPermissions(): array
    {
        return [];
    }

    /**
     * Builds the output to pass to the view.
     * This method may be overridden to add additional contextual data for the view to parse.
     * @param array $flags
     * @return Oroboros\core\interfaces\library\container\ResponseContainerInterface
     */
    protected function buildOutput(array $flags = []): \Oroboros\core\interfaces\library\container\ResponseContainerInterface
    {
        $this->getLogger()->debug('[type][scope][class] Building view output.', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
        ]);
        $output = $this->getResponseDirector()->build($flags);
        $this->setObserverEvent('build-output');
        return $output;
    }

    /**
     * Takes a definer and a set of keys to inject, and returns the fully compiled dataset.
     * 
     * @param string $definer_name
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $args
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     * @throws \Oroboros\core\exception\InvalidArgumentException If the given keys
     *         do not resolve the supplied definer requirements.
     */
    protected function resolveDefiner(string $definer_name, \Oroboros\core\interfaces\library\container\ContainerInterface $args): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $definer = $this->load('library', $definer_name);
        foreach ($args as $key => $value) {
            if ($definer->hasRequiredKey($key)) {
                $definer->setRequiredKey($key, $value);
            }
            if ($definer->hasOptionalKey($key)) {
                $definer->setOptionalKey($key, $value);
            }
        }
        try {
            return $definer->compile()->fetch();
        } catch (\Exception $e) {
            $this->setObserverEvent('definer-error');
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Provided definer [%2$s] '
                        . 'could not resolve with the given argument set [%3$s].'
                        , get_class($this), $definer_name
                        , implode(', ', array_keys($args->toArray())))
                    , 0, $e);
        }
    }

    protected function getEventArguments(array $arguments = null): array
    {
        if (is_null($arguments)) {
            $arguments = [];
        }
        $arguments['controller'] = $this;
        $arguments['scope'] = $this::CLASS_SCOPE;
        $arguments['error-controller'] = $this->error_controller;
        $args = [
            'command' => $this->getCommand(),
            'args' => $arguments,
            'flags' => $this->getFlags()
        ];
        return $args;
    }

    private function loadErrorController(string $class, \Oroboros\core\interfaces\controller\ErrorControllerInterface $object = null): void
    {
        if (!is_null($object)) {
            // Error controller is already loaded.
            $this->error_controller = $object;
        } else {
            // Load the class manually
            $this->getLogger()->debug('[type][scope][class] Loading error controller [error-controller-class].', [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
                'error-controller-class' => $class,
            ]);
            $this->error_controller = $this->load('controller', $class, $this->getCommand(), $this->getArguments(), $this->getFlags());
        }
    }

    private function verifyClassScope()
    {
        if (is_null(static::CLASS_SCOPE) || !in_array(static::CLASS_SCOPE, self::$valid_class_scopes)) {
            throw new \Oroboros\core\exception\OutOfBoundsException(sprintf('Class [%1$s] is invalid. '
                        . 'Controller must declare class constant [%2$s] with a valid known value [%3$s].', get_class($this), 'CLASS_SCOPE', implode(', ', self::$valid_class_scopes)));
        }
    }

    /**
     * Verifies that a valid error controller was declared, and loads it if it is valid.
     * @return void
     * @throws \Oroboros\core\exception\core\core\InvalidClassException
     */
    private function verifyErrorController()
    {
        $class = null;
        $error_controller = null;
        $expected = 'Oroboros\\core\\interfaces\\controller\\ErrorControllerInterface';
        if (is_null(static::ERROR_CONTROLLER)) {
            $this->setObserverEvent('error-controller-invalid-definition');
            throw new \Oroboros\core\exception\core\InvalidClassException(sprintf('Class [%1$s] is invalid. '
                        . 'Class constant [%2$s] must be defined with a valid fully '
                        . 'qualified or error controller class name, or a stub class name '
                        . 'that can be resolved correctly by the loader.', get_class($this), 'ERROR_CONTROLLER'));
        }
        if (in_array($expected, class_implements($this))) {
            // This class is already an error controller
            $error_controller = $this;
            $class = get_class($this);
            $this->loadErrorController($class, $error_controller);
            return;
        } elseif (!class_exists(static::ERROR_CONTROLLER)) {
            // Class does not exist, but it may be a stub declaration. Try the factory.
            $class = $this->getFullClassName('controller', static::ERROR_CONTROLLER);
        } else {
            // Class exists, and is not the current class.
            $class = static::ERROR_CONTROLLER;
        }
        if (!class_exists($class) || !in_array($expected, class_implements($class))) {
            $this->setObserverEvent('error-controller-invalid-definition');
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Controller class [%1$s] is invalid. '
                        . 'Class constant [%2$s] must be defined with an existing class that is a valid implementation '
                        . 'of [%3$s]. Current value [%4$s].',
                        get_class($this),
                        'ERROR_CONTROLLER',
                        $expected,
                        $class . (class_exists($class) ? null : '(non-existent class)')
            ));
        }
        try {
            $this->loadErrorController($class, $error_controller);
        } catch (\Exception $e) {
            $this->setObserverEvent('error-controller-invalid');
            // Failed to load the error controller
            throw new \Oroboros\core\exception\core\InvalidClassException(sprintf('Controller class [%1$s] is invalid. '
                        . 'Class declares an invalid error controller [%2$s] which could '
                        . 'not be instantiated correctly. ' . PHP_EOL
                        . 'Provided exception message: [%3$s].' . PHP_EOL
                        . 'Provided exception code: [%4$s].', get_class($this), (is_string($class) ? $class : get_class($class)), $e->getMessage(), $e->getCode()), $e->getCode());
        }
    }
}
