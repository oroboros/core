<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\controller;

/**
 * Abstract Error Dashboard Page
 * Provides abstraction for handling errors on login dashboard error pages
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
abstract class AbstractErrorDashboardPage extends AbstractErrorPageController implements \Oroboros\core\interfaces\controller\DashboardErrorPageInterface
{
    use \Oroboros\core\traits\html\HtmlDashboardPageUtilityTrait;
    
    /**
     * Designates which error controller is used in cases in which the
     * user is not logged in.
     * 
     * It is not appropriate to show the rest of the dashboard UI to non-logged
     * in users, so instead they will get the default frontend error if they
     * have not logged in and attempt to view secure pages.
     */
    const FRONTEND_ERROR_CONTROLLER = 'html\\ErrorController';
}
