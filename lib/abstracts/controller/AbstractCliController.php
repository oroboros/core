<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\controller;

/**
 * Abstract Cli Controller
 * Provides abstraction for command line controllers
 *
 * @author Brian Dayhoff
 */
abstract class AbstractCliController extends AbstractController implements \Oroboros\core\interfaces\controller\CliControllerInterface
{

    use \Oroboros\core\traits\cli\CliControllerCommonUtilityTrait;
    use \Oroboros\core\traits\cli\CliProcessUtilityTrait;
    use \Oroboros\core\traits\cli\CliFilesystemUtilityTrait;
    use \Oroboros\core\traits\cli\CliOutputUtilityTrait;
    use \Oroboros\core\traits\cli\CliSystemUtilityTrait;

    const RESPONSE_DIRECTOR_CLASS = 'cli\\ResponseDirector';
    const CLASS_SCOPE = 'cli';

    private static $cli_initialized = false;
    private static $cli_default_setup_methods = [
        'initializePid',
        'initializeUid',
        'initializeGid',
        'initializeInode',
        'initializeOs',
        'initializeOsVersion',
        'initializeCwd',
        'initializeCurrentCliUser',
        'initializeIOStreams',
        'initializeComponentBuilder',
    ];
    private $cli_setup_methods = null;
    private $builder = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        $this::addClassScope(self::CLASS_SCOPE);
        parent::__construct($command, $arguments, $flags);
        $this->initializeCliControllerSetup();
    }

    public function getResponseBuilders(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $container = parent::getResponseBuilders();

        return $container;
    }

    protected function getComponentBuilder()
    {
        return $this->builder;
    }

    protected function declareCliSetupMethods(): array
    {
        return self::$cli_default_setup_methods;
    }

    private function initializeCliControllerSetup(): void
    {
        if (!self::$cli_initialized) {
            self::$cli_initialized = true;
        }
        $this->cli_setup_methods = $this->declareCliSetupMethods();
        foreach ($this->cli_setup_methods as $method) {
            $this->$method();
        }
        $this->syncPath();
        $this->setObserverEvent('cli-initialized');
    }

    private function initializeComponentBuilder(): void
    {
        if (is_null($this->builder)) {
            $this->builder = $this->load('library', 'component\\cli\\ComponentBuilder', null, $this->getArguments());
        }
    }
}
