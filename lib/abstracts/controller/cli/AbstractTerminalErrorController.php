<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\controller\cli;

/**
 * Abstract Cli Error Controller
 * Provides abstraction for command line error controllers
 *
 * @author Brian Dayhoff
 */
abstract class AbstractTerminalErrorController extends \Oroboros\core\abstracts\controller\AbstractCliErrorController implements \Oroboros\core\interfaces\controller\cli\TerminalErrorControllerInterface
{

    use \Oroboros\core\traits\pattern\ObserverTrait;
    
    const CLASS_SCOPE = 'console';

    /**
     * The default cli console, if not supplied
     */
    const DEFAULT_CONSOLE = 'cli\\Console';

    /**
     * This is already an error controller. Give its own class.
     */
    const ERROR_CONTROLLER = self::class;

    /**
     * The default error status code to send when none is provided.
     * Indicates that the requested method was not found
     */
    const DEFAULT_ERROR_CODE = 127;

    /**
     * The default error status code to send when the program must terminate and none is provided.
     */
    const DEFAULT_FATAL_ERROR_CODE = 2;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        $this::addClassScope(self::CLASS_SCOPE);
        parent::__construct($command, $arguments, $flags);
        $this->initializeObserver();
        $this->bindObserverEvent('error', [$this, 'handleError']);
    }

    protected function getErrorMessageByCode(int $code)
    {
        global $argv;
        $args = $argv;
        array_shift($args);
        switch ($code) {
            case static::DEFAULT_ERROR_CODE:
                $this->error_message(sprintf('Provided command [%1$s] is not known.', implode(' ', $args)));
                break;
            case static::DEFAULT_FATAL_ERROR_CODE:
                $this->error_message('A fatal error occurred.');
                break;
            default:
                $this->error_message('An unknown error occurred.');
                break;
        }
    }

    public function handleError(\Oroboros\core\interfaces\library\cli\ShellConsoleInterface $console)
    {
        $input = $console->getInput();
        if ($input === 'exit') {
            $console->close();
        } else {
//            $this->error_message('Unknown command.');
        }
    }

    protected function declareObserverEventBindings(): array
    {
        return [
            'error' => [$this, 'handleError']
        ];
    }
}
