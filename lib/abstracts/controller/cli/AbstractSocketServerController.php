<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\controller\cli;

/**
 * Controls socket server instances
 *
 * @author Brian Dayhoff
 */
abstract class AbstractSocketServerController extends \Oroboros\core\abstracts\controller\AbstractCliController implements \Oroboros\core\interfaces\controller\cli\SocketServerControllerInterface
{

    use \Oroboros\core\traits\pattern\ObserverTrait;
    
    const CLASS_SCOPE = 'socket';

    /**
     * Use the standard cli error controller
     */
    const ERROR_CONTROLLER = 'cli\\SocketServerErrorController';

    /**
     * The list of all available socket applications
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private static $socket_applications = null;

    /**
     * Strategy obect that returns instantiated websocket applications
     * @var \Oroboros\core\interfaces\library\socket\SocketStrategyInterface
     */
    private static $socket_strategy = null;
    private $server_up = false;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        $this::addClassScope(self::CLASS_SCOPE);
        parent::__construct($command, $arguments, $flags);
        $this->initializeStrategy();
        $this->initializeObserver();
    }

    public function index()
    {
        $this->success_message('You have successfully loaded [the socket server controller].');
        exit(0);
    }

    /**
     * Starts a socket application instance
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $arguments
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $flags
     */
    public function start(\Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null)
    {
        if (is_null($arguments) || !$arguments->has('application')) {
            $this->error_message('No socket application identifier provided.');
            exit(1);
        }
        try {
            $server = $this->getStrategy()->evaluate($arguments->get('application'), $this->getArguments(), $this->getFlags());
        } catch (\InvalidArgumentException $e) {
            $this->error_message(sprintf('Socket application [%1$s] is not valid.', $arguments->get('application')));
            exit(1);
        } catch (\ErrorException $e) {
            $this->error_message(sprintf('Unable to start socket server for [%1$s] due to an error [%2$s]', $arguments->get('application'), $e->getMessage()));
            $this->error_message($e->getTraceAsString());
            exit(1);
        }
        $this->server_up = true;
        $this->info_message(sprintf('Initializing socket server for [%1$s].', $arguments->get('application')));
        // Automatically respawn failed socket applications 
        // until explicitly terminated.
        while ($this->server_up === true) {
            try {
                $this->runServer($server, $arguments);
            } catch (Exception $e) {
                $this->error_message(sprintf('Socket application [%1$s] has terminated with an exception. '
                        . 'Connections have been dropped!', $arguments->get('application')));
                $this->error_message(sprintf('%1$s%3$s%2$s', $e->getMessage(), $e->getTraceAsString(), PHP_EOL));
                if ($this->server_up === true) {
                    $this->info_message(sprintf('Respawning socket application [%1$s].', $arguments->get('application')));
                }
            }
        }
        $this->info_message(sprintf('Socket server for application [%1$s] running as host [%2$s] on port [%3$s] has been closed.', $arguments->get('application'), $arguments->get('host'), $arguments->get('port')));
        exit(0);
    }

    /**
     * Stops a socket application instance
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $arguments
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $flags
     */
    public function stop(\Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null)
    {
        if (is_null($arguments) || !$arguments->has('application')) {
            $this->error_message('No socket application identifier provided.');
            exit(1);
        }
        $this->error_message('This functionality is not implemented.');
        exit(1);
    }

    /**
     * Lists available socket applications
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $arguments
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $flags
     */
    public function info(\Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null)
    {
        if (is_null($arguments) || !$arguments->has('application')) {
            // List info for all applications
            $this->print_message('Showing socket application status for all registered applications.');
            $apps = $this->getStrategy()->list();
            foreach ($apps as $app) {
                $info = $this->getStrategy()->info($app);
                $this->print_message(sprintf('Socket application [%1$s]:', $app));
                $this->print_message(sprintf(' - Host: [%1$s]', $info['host']));
                $this->print_message(sprintf(' - Port: [%1$s]', $info['port']));
                $this->print_message(sprintf(' - Class: [%1$s]', $info['application']));
            }
        } else {
            // List info for only the specified application
            try {
                $info = $this->getStrategy()->info($arguments->get('application'));
                $this->print_message(sprintf('Socket application [%1$s]:', $arguments->get('application')));
                $this->print_message(sprintf(' - Host: [%1$s]', $info['host']));
                $this->print_message(sprintf(' - Port: [%1$s]', $info['port']));
                $this->print_message(sprintf(' - Class: [%1$s]', $info['application']));
            } catch (\InvalidArgumentException $e) {
                $this->error_message(sprintf('Unknown socket application [%1$s].', $arguments->get('application')));
                exit(1);
            }
        }
        exit(0);
    }

    /**
     * Lists available socket applications
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $arguments
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $flags
     */
    public function list(\Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null)
    {
        $apps = $this->getStrategy()->list();
        foreach ($apps as $app) {
            $info = $this->getStrategy()->info($app);
            $this->print_message(sprintf('%1$s', $app));
        }
        exit(0);
    }

    /**
     * Displays the status of a socket application instance
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $arguments
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $flags
     */
    public function status(\Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null)
    {
        if (is_null($arguments) || !$arguments->has('application')) {
            // List status for all applications
            $this->print_message('Showing socket application status for all registered applications.');
            $apps = $this->getStrategy()->list();
            foreach ($apps as $app) {
                
            }
        } else {
            try {
                
            } catch (\InvalidArgumentException $e) {
                $this->error_message(sprintf('Unknown socket application [%1$s].', $arguments->get('application')));
                exit(1);
            }
        }
    }

    protected function getStrategy(): \Oroboros\core\interfaces\library\socket\SocketStrategyInterface
    {
        return self::$socket_strategy;
    }

    protected function runServer(\Ratchet\Server\IoServer $server, \Oroboros\core\interfaces\library\container\ContainerInterface $arguments): void
    {
        $this->info_message(sprintf('Starting socket socket application [%1$s] on host [%2$s], port [%3$s].', $arguments->get('application'), $arguments->get('host'), $arguments->get('port')));
        try {
            $server->run();
        } catch (\Oroboros\core\exception\socket\SocketApplicationException $e) {
            throw $e;
        } catch (\Exception $e) {
            // All uncaught exceptions at this level 
            // get transmuted to a socket server exception.
            throw new \Oroboros\core\exception\socket\SocketApplicationException($e->getMessage(), $e->getCode());
        }
    }

    private function verifyQueueType(string $key): void
    {
        if (!property_exists(self::$registered_queues, $key)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided queue type [%2$s] is not registered to any valid job queue.', get_class($this), $key));
        }
    }

    private function verifyQueue(string $type, string $queue): void
    {
        $this->verifyQueueType($type);
    }

    private function initializeStrategy(): void
    {
        if (is_null(self::$socket_strategy)) {
            self::$socket_strategy = $this->load('library', 'socket\\SocketStrategy', 'socket-application-controller', $this->getArguments(), $this->getFlags());
        }
    }
}
