<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\controller\cli;

/**
 * Controls job worker instances
 *
 * @author Brian Dayhoff
 */
abstract class AbstractJobController extends \Oroboros\core\abstracts\controller\AbstractCliController implements \Oroboros\core\interfaces\controller\cli\JobControllerInterface
{

    use \Oroboros\core\traits\pattern\ObserverTrait;
    
    const CLASS_SCOPE = 'job';

    /**
     * Use the standard cli error controller
     */
    const ERROR_CONTROLLER = 'cli\\JobErrorController';

    /**
     * The default job queue, if not supplied
     */
    const DEFAULT_QUEUE = 'job\\BeanstalkQueue';

    private static $job_manager_strategy = null;

    /**
     * Registered job queue handlers.
     * @var array
     */
    private static $registered_queues = [];
    private static $default_consoles_initialized = false;

    /**
     * Base initialization methods required for controller setup, if any.
     * @var array
     */
    private $setup = [];

    /**
     * The job queue manager instance scoped to the current controller instance
     * @var \Oroboros\core\interfaces\library\job\JobManagerInterface
     */
    private $queue = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        $this::addClassScope(self::CLASS_SCOPE);
        parent::__construct($command, $arguments, $flags);
        $this->initializeStrategy();
        $this->initializeObserver();
    }

    /**
     * Registers a new job queue
     * @param string $key
     * @param string $class
     * @return void
     * @throws \InvalidArgumentException If the given class does not exist,
     *     is not a job queue class, or provides an identification key
     *     that is already registered.
     */
    public static function registerQueue(string $key, string $class): void
    {
        $proxy = new \Oroboros\core\pattern\factory\LoaderProxy();
        if (!$proxy->getFullClassName('library', $class)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Provided class [%2$s] for key [%3$s] is not a valid job queue manager class.', get_called_class(), $class, $key));
        }
        if (property_exists(self::$registered_queues, $key) && $class !== self::$registered_queues[$key]) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Provided job queue manager key [%2$s] is already registered.', get_class($this), $key));
        }
        $expected = 'Oroboros\\core\\interfaces\\library\\job\\JobManagerInterface';
        if (!in_array($expected, class_implements($proxy->getFullClassName('library', $class)))) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided job queue manager class [%2$s] for provided key [%3$s] is not '
                        . 'a valid job queue manager object. '
                        . 'Expected classname implementing [%4$s].', get_called_class(), $class, $key, $expected));
        }
        self::$registered_queues[$key] = $class;
    }

    public function index()
    {
        $this->success_message('You have successfully loaded [the job controller].');
        exit(0);
    }

    /**
     * Opens a queue and begins working the tasks in it.
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $arguments
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $flags
     * @return void
     */
    public function watch(\Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null)
    {
        $job_arguments = is_null($arguments) ? null : $arguments->toArray();
        $job_flags = is_null($flags) ? null : $flags->toArray();
        $type = null;
        $queue = null;
        if (is_null($arguments) || !$arguments->has('type')) {
            $this->error_message(sprintf('No job type or queue specified to watch.'));
            exit(1);
        } elseif ($arguments->has('type') && !$arguments->has('queue')) {
            $this->error_message(sprintf('No queue specified to watch for job type [%1$s].', $arguments->get('type')));
            exit(1);
        } else {
            try {
                $manager = $this->loadManager($arguments->get('type'), $arguments->get('queue'), $job_arguments, $job_flags);
                $manager->verify($arguments->get('queue'));
                $this->print_message(sprintf('Spawning worker of type [%1$s], watching job queue [%2$s].', $arguments->get('type'), $arguments->get('queue')));
                if (!$manager->watch($arguments->get('queue'))) {
                    $this->print_message(sprintf('There are currently no jobs of type [%1$s] in job queue [%2$s], closing thread.', $arguments->get('type'), $arguments->get('queue')));
                } else {
                    $this->print_message(sprintf('No more jobs exist of type [%1$s] in job queue [%2$s], closing thread.', $arguments->get('type'), $arguments->get('queue')));
                }
            } catch (\InvalidArgumentException $e) {
//                print_r($e->getMessage()); // Debugging only
                $this->error_message(sprintf('Invalid queue [%1$s] of type [%2$s].', $arguments->get('queue'), $arguments->get('type')));
                exit(1);
            }
        }
        exit(0);
    }

    public function list(\Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null)
    {
        $job_arguments = is_null($arguments) ? null : $arguments->toArray();
        $job_flags = is_null($flags) ? null : $flags->toArray();
        if (!is_null($arguments)) {
            // Verify type/queue
            if ($arguments->has('type') && $arguments->has('queue')) {
                try {
                    $manager = $this->loadManager($arguments->get('type'), $arguments->get('queue'), $job_arguments, $job_flags);
                    $manager->verify($arguments->get('queue'));
                    $this->print_message(sprintf('Checking queue [%1$s] of type [%2$s]', $arguments->get('queue'), $arguments->get('type')));
                    $jobs = $manager->listJobs($arguments->get('queue'));
                    $this->print_message(sprintf('Jobs registered for queue [%1$s] of type [%2$s]:', $arguments->get('queue'), $arguments->get('type')));
                    if (empty($jobs)) {
                        $this->print_message(sprintf('Job queue [%1$s] is empty.', $arguments->get('queue')));
                    } else {
                        $this->print_message(sprintf('Queue [%1$s] has [%2$s] jobs waiting.', $arguments->get('queue'), count($jobs)));
                    }
                } catch (\InvalidArgumentException $e) {
                    $this->error_message(sprintf('Invalid queue [%1$s] of type [%2$s].', $arguments->get('queue'), $arguments->get('type')));
                    exit(1);
                }
            } elseif ($arguments->has('type')) {
                try {
                    $manager = $this->loadManager($arguments->get('type'), null, $job_arguments, $job_flags);
                    $this->print_message(sprintf('Checking queue type [%1$s]', $arguments->get('type')));
                    $queues = $manager->listQueues();
                    if (empty($queues)) {
                        $this->print_message(sprintf('There are no queues in type [%1$s].', $arguments->get('type')));
                    } else {
                        $this->print_message(sprintf('The following queues exist in in type [%1$s]:', $arguments->get('type')));
                        foreach ($queues as $queue) {
                            $this->print_message(sprintf('[%1$s]', $queue));
                        }
                    }
                } catch (\InvalidArgumentException $e) {
//                    print_r($e->getMessage()); // Debugging only
                    $this->error_message(sprintf('Invalid queue type [%1$s].', $arguments->get('type')));
                    exit(1);
                }
            } else {
                // Invalid command
                $this->error_message(sprintf('Invalid job list command.'));
                exit(1);
            }
        } else {
            // List all queues
            $this->print_message(sprintf('Checking all job queues'));
        }
        exit(0);
    }

    public function queue(\Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null)
    {
        $job_arguments = is_null($arguments) ? null : $arguments->toArray();
        $job_flags = is_null($flags) ? null : $flags->toArray();
        if (!is_null($arguments) && $arguments->has('type') && $arguments->has('queue')) {
            try {
                $manager = $this->loadManager($arguments->get('type'), $arguments->get('queue'), $job_arguments, $job_flags);
                $manager->verify($arguments->get('queue'));
                try {
                    $manager->queueJob($arguments->get('queue'), $this->getQueueDetails($job_arguments, $job_flags));
                } catch (\InvalidArgumentException $e) {
                    $this->error_message(sprintf('Unable to queue job in queue [%1$s] of type [%2$s]. Reason: [%3$s].', $arguments->get('queue'), $arguments->get('type'), $e->getMessage()));
                    exit(1);
                }
            } catch (\InvalidArgumentException $e) {
                $this->error_message(sprintf('Invalid queue [%1$s] of type [%2$s].', $arguments->get('queue'), $arguments->get('type')));
                exit(1);
            }
        } elseif (!is_null($arguments) && $arguments->has('type')) {
            // Invalid command
            $this->error_message(sprintf('Invalid job queue command. No queue supplied for type [%1$s]', $arguments->get('type')));
            exit(1);
        } else {
            // Invalid command
            $this->error_message(sprintf('Invalid job queue command. [type] and [queue] must be supplied.'));
            exit(1);
        }
        exit(0);
    }

    public function status(\Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null)
    {
        
    }

    public function pause(\Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null)
    {
        
    }

    public function resume(\Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null)
    {
        
    }

    public function restart(\Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null)
    {
        
    }

    public function kill(\Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null)
    {
        
    }

    public function forceKill(\Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null)
    {
        
    }

    public function handleJobStart(\Oroboros\core\interfaces\library\job\JobManagerInterface $manager): void
    {
        $this->evaluateJobStart($manager);
    }

    public function handleJobRestart(\Oroboros\core\interfaces\library\job\JobManagerInterface $manager): void
    {
        $this->evaluateJobRestart($manager);
    }

    public function handleJobPause(\Oroboros\core\interfaces\library\job\JobManagerInterface $manager): void
    {
        $this->evaluateJobPause($manager);
    }

    public function handleJobResume(\Oroboros\core\interfaces\library\job\JobManagerInterface $manager): void
    {
        $this->evaluateJobRestart($manager);
    }

    public function handleJobKill(\Oroboros\core\interfaces\library\job\JobManagerInterface $manager): void
    {
        $this->evaluateJobKill($manager);
    }

    public function handleJobForceKill(\Oroboros\core\interfaces\library\job\JobManagerInterface $manager): void
    {
        $this->evaluateJobForceKill($manager);
    }

    public function handleJobError(\Oroboros\core\interfaces\library\job\JobManagerInterface $manager): void
    {
        $this->evaluateJobError($manager);
    }

    public function handleJobFailure(\Oroboros\core\interfaces\library\job\JobManagerInterface $manager): void
    {
        $this->evaluateJobFailure($manager);
    }

    public function handleJobComplete(\Oroboros\core\interfaces\library\job\JobManagerInterface $manager): void
    {
        $this->evaluateJobComplete($manager);
    }

    public function handleQueuedJob(\Oroboros\core\interfaces\library\job\JobManagerInterface $manager): void
    {
        $queue = $manager->getJobQueue();
        $id = $manager->getJobId();
        $data = $manager->getJobData();
        $this->registerQueuedJob($manager::JOB_TYPE, $queue, $id, $data);
        $this->success_message(sprintf('Job successfully queued of type [%1$s] '
                . 'in queue [%2$s] with id [%3$s].'
                , $manager::JOB_TYPE, $queue, $id));
    }

    /**
     * Override this method to register a newly queued job into the database
     * or some other external job tracking mechanism.
     * 
     * The default behavior does not perform any such registration,
     * but this method is hooked on queue events to provide a
     * convenient place to perform such actions.
     * 
     * @param string $type
     * @param string $queue
     * @param int $id
     * @param string $data
     * @return void
     */
    protected function registerQueuedJob(string $type, string $queue, int $id, string $data): void
    {
        // no-op. This method is just a hook for overrides.
    }

    /**
     * Loads and correctly initializes a job manager for a specific type/queue
     * @param string $type
     * @param string $queue
     * @param array $arguments
     * @param array $flags
     * @return \Oroboros\core\interfaces\library\job\JobManagerInterface
     */
    protected function loadManager(string $type, string $queue = null, array $arguments = [], array $flags = []): \Oroboros\core\interfaces\library\job\JobManagerInterface
    {
        if (is_null($type)) {
            $type = static::DEFAULT_QUEUE;
        }
        $strategy = $this->getStrategy();
        $manager = $strategy->getManager($type, $queue, $arguments, $flags);
        $manager->attach($this);
        return $manager;
    }

    protected function getComponentArguments(array $arguments = []): array
    {
        $args = [
            'builder' => $this->getComponentBuilder()
        ];
        return array_replace_recursive($args, $arguments);
    }

    protected function getConsoleArguments(array $arguments = null): ?array
    {
        if (is_null($arguments)) {
            $arguments = [];
        }
        return $this->getComponentArguments($arguments);
    }

    protected function getConsoleFlags(array $flags = null): ?array
    {
        return $flags;
    }

    protected function evaluateJobStart(\Oroboros\core\interfaces\library\job\JobManagerInterface $manager): void
    {
        $queue = $manager->getJobQueue();
        $id = $manager->getJobId();
        $this->info_message(sprintf('A job was started of type [%1$s] in queue [%2$s] with id [%3$s]', $manager::JOB_TYPE, $queue, $id));
    }

    protected function evaluateJobRestart(\Oroboros\core\interfaces\library\job\JobManagerInterface $manager): void
    {
        $queue = $manager->getJobQueue();
        $id = $manager->getJobId();
        $this->info_message(sprintf('A job was restarted of type [%1$s] in queue [%2$s] with id [%3$s]', $manager::JOB_TYPE, $queue, $id));
    }

    protected function evaluateJobKill(\Oroboros\core\interfaces\library\job\JobManagerInterface $manager): void
    {
        $queue = $manager->getJobQueue();
        $id = $manager->getJobId();
        $this->info_message(sprintf('A job was stopped of type [%1$s] in queue [%2$s] with id [%3$s]', $manager::JOB_TYPE, $queue, $id));
    }

    protected function evaluateJobForceKill(\Oroboros\core\interfaces\library\job\JobManagerInterface $manager): void
    {
        $queue = $manager->getJobQueue();
        $id = $manager->getJobId();
        $this->warning_message(sprintf('A job was forcibly killed of type [%1$s] in queue [%2$s] with id [%3$s]', $manager::JOB_TYPE, $queue, $id));
    }

    protected function evaluateJobPause(\Oroboros\core\interfaces\library\job\JobManagerInterface $manager): void
    {
        $queue = $manager->getJobQueue();
        $id = $manager->getJobId();
        $this->info_message(sprintf('A job was paused of type [%1$s] in queue [%2$s] with id [%3$s]', $manager::JOB_TYPE, $queue, $id));
    }

    protected function evaluateJobResume(\Oroboros\core\interfaces\library\job\JobManagerInterface $manager): void
    {
        $queue = $manager->getJobQueue();
        $id = $manager->getJobId();
        $this->info_message(sprintf('A job was resumed of type [%1$s] in queue [%2$s] with id [%3$s]', $manager::JOB_TYPE, $queue, $id));
    }

    protected function evaluateJobFailure(\Oroboros\core\interfaces\library\job\JobManagerInterface $manager): void
    {
        $queue = $manager->getJobQueue();
        $id = $manager->getJobId();
        $this->error_message(sprintf('A job failed of type [%1$s] in queue [%2$s] with id [%3$s]', $manager::JOB_TYPE, $queue, $id));
    }

    protected function evaluateJobComplete(\Oroboros\core\interfaces\library\job\JobManagerInterface $manager): void
    {
        $queue = $manager->getJobQueue();
        $id = $manager->getJobId();
        $this->info_message(sprintf('A job was completed of type [%1$s] in queue [%2$s] with id [%3$s]', $manager::JOB_TYPE, $queue, $id));
    }

    protected function evaluateJobError(\Oroboros\core\interfaces\library\job\JobManagerInterface $manager): void
    {
        $queue = $manager->getJobQueue();
        $id = $manager->getJobId();
        $error = $manager->getJobError();
        $this->warning_message(sprintf('A job error occurred of type [%1$s] in queue [%2$s] with id [%3$s]', $manager::JOB_TYPE, $queue, $id));
        $this->error_message(sprintf('%1$s%3$s%2$s', $error->getMessage(), $error->getTraceAsString(), PHP_EOL));
    }

    protected function declareObserverEventBindings(): array
    {
        return [
            'start' => [$this, 'handleJobStart'],
            'restart' => [$this, 'handleJobRestart'],
            'pause' => [$this, 'handleJobPause'],
            'resume' => [$this, 'handleJobResume'],
            'kill' => [$this, 'handleJobKill'],
            'force-kill' => [$this, 'handleJobForceKill'],
            'error' => [$this, 'handleJobError'],
            'fail' => [$this, 'handleJobFailure'],
            'complete' => [$this, 'handleJobComplete'],
            'queue' => [$this, 'handleQueuedJob'],
        ];
    }

    protected function getStrategy(): \Oroboros\core\interfaces\library\job\JobManagerStrategyInterface
    {
        return self::$job_manager_strategy;
    }

    private function verifyQueueType(string $key): void
    {
        if (!property_exists(self::$registered_queues, $key)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided queue type [%2$s] is not registered to any valid job queue.', get_class($this), $key));
        }
    }

    private function verifyQueue(string $type, string $queue): void
    {
        $this->verifyQueueType($type);
    }

    private function initializeStrategy(): void
    {
        if (is_null(self::$job_manager_strategy)) {
            self::$job_manager_strategy = $this->load('library', 'job\\JobManagerStrategy', 'job-controller', $this->getArguments(), $this->getFlags());
        }
    }

    private function getQueueDetails(array $arguments, array $flags): array
    {
        $details = [];
        $manager = $this->getStrategy()->getManager($arguments['type'], $arguments['queue'], $arguments, $flags);
        if (array_key_exists('action', $flags)) {
            $details['action'] = $flags['action'];
        }
        if (array_key_exists('data', $flags)) {
            $details['data'] = $flags['data'];
        }
        if (!array_key_exists('delay', $flags)) {
            $details['delay'] = $manager::DEFAULT_DELAY;
        } else {
            $details['delay'] = $flags['delay'];
        }
        if (!array_key_exists('priority', $flags)) {
            $details['priority'] = $manager::DEFAULT_PRIORITY;
        } else {
            $details['priority'] = $flags['priority'];
        }
        if (!array_key_exists('ttr', $flags)) {
            $details['ttr'] = $manager::DEFAULT_TTR;
        } else {
            $details['ttr'] = $flags['ttr'];
        }
        return $details;
    }
}
