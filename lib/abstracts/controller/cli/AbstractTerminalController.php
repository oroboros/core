<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\controller\cli;

/**
 * Controls the interactive application terminal
 *
 * @author Brian Dayhoff
 */
abstract class AbstractTerminalController extends \Oroboros\core\abstracts\controller\AbstractCliController implements \Oroboros\core\interfaces\controller\cli\TerminalControllerInterface
{

    use \Oroboros\core\traits\pattern\ObserverTrait;
    
    const CLASS_SCOPE = 'console';

    /**
     * Use the standard cli error controller
     */
    const ERROR_CONTROLLER = 'cli\\TerminalErrorController';

    /**
     * The default cli console, if not supplied
     */
    const DEFAULT_CONSOLE = 'cli\\Console';

    /**
     * Registered console instances.
     * @var array
     */
    private static $registered_consoles = [];
    private static $default_consoles_initialized = false;

    /**
     * Base initialization methods required for controller setup, if any.
     * @var array
     */
    private $setup = [];

    /**
     * The console instance currently scoped to the terminal session
     * @var \Oroboros\core\interfaces\library\cli\ShellConsoleInterface
     */
    private $console = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        $this::addClassScope(self::CLASS_SCOPE);
        $this->initializeDefaultConsoles();
        parent::__construct($command, $arguments, $flags);
        $this->initializeObserver();
    }

    /**
     * Registers a new console component type
     * @param string $key
     * @param string $class
     * @return void
     * @throws \InvalidArgumentException If the given class does not exist,
     *     is not a console component, or provides an identification key
     *     that is already registered.
     */
    public static function registerConsole(string $key, string $class): void
    {
        $proxy = new \Oroboros\core\pattern\factory\LoaderProxy();
        if (!$proxy->getFullClassName('component', $class)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Provided class [%2$s] for key [%3$s] is not a valid component console class.', get_called_class(), $class, $key));
        }
        if (array_key_exists($key, self::$registered_consoles) && $class !== self::$registered_consoles[$key]) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Provided console key [%2$s] is already registered.', get_class($this), $key));
        }
        $expected = 'Oroboros\\core\\interfaces\\library\\cli\\ShellConsoleInterface';
        if (!in_array($expected, class_implements($proxy->getFullClassName('component', $class)))) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided console class [%2$s] for provided key [%3$s] is not '
                        . 'a console component. '
                        . 'Expected classname implementing [%4$s].', get_class($this), $class, $key, $expected));
        }
        self::$registered_consoles[$key] = $class;
    }

    public function console(\Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null)
    {
        $console_arguments = null;
        $console_flags = null;
        $type = null;
        if (!is_null($arguments)) {
            $console_arguments = $arguments->toArray();
        }
        if (!is_null($flags)) {
            $console_flags = $flags->toArray();
        }
        if (!is_null($arguments)) {
            try {
                $this->verifyConsoleType($arguments->get('console'));
                $type = self::$registered_consoles[$arguments->get('console')];
            } catch (\InvalidArgumentException $e) {
                $this->error_message(sprintf('No console of type [%1$s] exists.', $type));
                exit(1);
            }
        }
        $this->console = $this->loadConsole('console', $type, $console_arguments, $console_flags);
        $this->console->setPrefix(sprintf('%1$s:%2$s > ', $this::app()->getApplicationDefinition()->get('namespace'), $this->console::CLASS_SCOPE));
        $this->console->setPrefixFormat('blue');
        $this->console->setHeader();
        $this->console->open();
        exit(0);
    }

    public function handleCommand(\Oroboros\core\interfaces\library\cli\ShellConsoleInterface $console): void
    {
        $this->evaluateCommand($console);
    }

    public function handleError(\Oroboros\core\interfaces\library\cli\ShellConsoleInterface $console): void
    {
        $this->evaluateError($console);
    }

    protected function loadConsole(string $id, string $console = null, $arguments = [], $flags = []): \Oroboros\core\interfaces\library\cli\ShellConsoleInterface
    {
        if (is_null($console)) {
            $console = static::DEFAULT_CONSOLE;
        }
        $object = $this->load('component', $console, $id, $this->getConsoleArguments($arguments), $this->getConsoleFlags($flags));
        $object->attach($this);
        return $object;
    }

    protected function closeConsole(\Oroboros\core\interfaces\library\cli\ShellConsoleInterface $console): void
    {
        $this->console->close();
    }

    protected function getComponentArguments(array $arguments = []): array
    {
        $args = [
            'builder' => $this->getComponentBuilder()
        ];
        return array_replace_recursive($args, $arguments);
    }

    protected function getConsoleArguments(array $arguments = null): ?array
    {
        if (is_null($arguments)) {
            $arguments = [];
        }
        return $this->getComponentArguments($arguments);
    }

    protected function getConsoleFlags(array $flags = null): ?array
    {
        return $flags;
    }

    protected function evaluateCommand(\Oroboros\core\interfaces\library\cli\ShellConsoleInterface $console): void
    {
        $command = $console->getCommand();
        $command->execute();
        $command->releaseSubject();
    }

    protected function evaluateError(\Oroboros\core\interfaces\library\cli\ShellConsoleInterface $console): void
    {
        $error = $console->getError();
        $this->error_message($error);
    }

    protected function declareObserverEventBindings(): array
    {
        return [
            'command' => [$this, 'handleCommand'],
            'error' => [$this, 'handleError'],
        ];
    }

    private function verifyConsoleType(string $key): void
    {
        if (!array_key_exists($key, self::$registered_consoles)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided console key [%2$s] is not registered to any valid console.', get_class($this), $key));
        }
    }

    private function initializeDefaultConsoles(): void
    {
        if (!self::$default_consoles_initialized) {
            $consoles = $config = $this->load('library', 'parser\\JsonParser', $this::CONFIG_ROOT . 'consoles.json')
                ->fetch();
            foreach ($consoles as $key => $class) {
                if (!array_key_exists($key, self::$registered_consoles) && $class !== false) {
                    $this::registerConsole($key, $class);
                }
            }
            self::$default_consoles_initialized = true;
        }
    }
}
