<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\controller;

/**
 * Abstract Http Controller
 * Provides abstraction for controllers that handle http requests.
 * 
 * @author Brian Dayhoff
 */
abstract class AbstractHttpController extends AbstractController implements \Oroboros\core\interfaces\controller\HttpControllerInterface
{

    use \Oroboros\core\traits\http\HttpControllerCommonUtility;

    const RESPONSE_DIRECTOR_CLASS = 'http\\ResponseDirector';
    const LAYOUT_CLASS = 'RawLayout';
    const CLASS_SCOPE = 'http';

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        $this::addClassScope(self::CLASS_SCOPE);
        $this->clearServerExposeHeaders();
        parent::__construct($command, $arguments, $flags);
    }

    /**
     * Remove the php "X-Powered-By" header, which is basically useless
     * and allows people to sniff for php-driven sites.
     * 
     * This is not insecure in and of itself, but it does reveal
     * platform details that may allow analysis for other
     * attack vectors.
     * 
     * If you were running a vulnerable php installation version
     * with a confirmed attack vector, this would tell anyone
     * looking at client headers from your site that you are
     * vulnerable if it were left enabled, allowing them to
     * freely proceed with attacking such a vulnerability.
     * You do not want that.
     * 
     * @note If you are actually running an insecure php version,
     *       this is *NOT* going to stop it from being a vulnerability.
     *       All it is going to do is prevent your site from announcing
     *       to the world with a megaphone that you are.
     * @return void
     */
    private function clearServerExposeHeaders(): void
    {
        header_remove('X-Powered-By');
    }
}
