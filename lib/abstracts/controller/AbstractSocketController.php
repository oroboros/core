<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\controller;

/**
 * Abstract Socket Controller
 * Provides abstraction for socket requests
 *
 * @author Brian Dayhoff
 */
abstract class AbstractSocketController extends AbstractHttpController implements \Oroboros\core\interfaces\controller\socket\SocketControllerInterface
{

    use \Oroboros\core\traits\http\SocketResponseTrait;

    const CLASS_SCOPE = 'socket';
    const RESPONSE_DIRECTOR_CLASS = 'html\\ResponseDirector';
    const LAYOUT_CLASS = 'DefaultLayout';

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        $this::addClassScope(self::CLASS_SCOPE);
        parent::__construct($command, $arguments, $flags);
        $this->setObserverEvent('socket-ready');
//        $this->initializeScripts();
//        $this->initializeStyles();
//        $this->initializeFonts();
//        $this->initializeLayout();
    }

    public function getResponseBuilders(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $container = parent::getResponseBuilders();
//       $container['script'] = $this->load('library', 'html\\ScriptBuilder');
//       $container['stylesheet'] = $this->load('library', 'html\\StylesheetBuilder');
//       $container['font'] = $this->load('library', 'html\\FontBuilder');
//       $container['data'] = $this->load('library', 'html\\DataBuilder');
////       $container['image'] = $this->load('library', 'html\\ImageBuilder');
//       $container['component'] = $this->load('library', 'component\\html\\ComponentBuilder');
//       $container['meta'] = $this->load('library', 'html\\MetaBuilder');
//       $container['head'] = $this->load('library', 'html\\HeadBuilder');
//       $container['body'] = $this->load('library', 'html\\BodyBuilder');
//       $container['response'] = $this->load('library', 'html\\ResponseBuilder');
        return $container;
    }
}
