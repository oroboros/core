<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\pluggable;

/**
 * Abstract Pluggable Importer
 * Baseline abstraction for importing pluggables
 * (extensions, modules, services, etc)
 *
 * @author Brian Dayhoff
 */
abstract class AbstractPluggableImporter extends \Oroboros\core\abstracts\library\AbstractLibrary implements \Oroboros\core\interfaces\pluggable\PluggableImporterInterface
{

    const CLASS_SCOPE = 'importer';
    const PLUGGABLE_TYPE = null;

    /**
     * The default baseline expected keys that all manifest files must have.
     * @var array
     */
    private static $default_manifest_required_keys = [
        'name',
        'title',
        'description',
        'version',
        'type',
        'namespace',
        'definer'
    ];

    /**
     * The index of all listed pluggables
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private static $pluggables = null;

    /**
     * The autoloader used to register the Psr-4 pluggable package when imported
     * @var \Oroboros\core\library\autoload\Autoloader
     */
    private static $autoloader = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        $this->verifyPluggableType();
        parent::__construct($command, $arguments, $flags);
        $this->initializePluggables();
        $this->initializeAutoloader();
    }

    /**
     * Determines if a given directory is importable.
     * @return string
     */
    public function evaluate(string $directory): bool
    {
        return $this->verifyPluggable($directory);
    }

    /**
     * Imports a pluggable directory, and registers it with any relevant
     * resources that make it available to the system.
     * 
     * If the method `evaluate` returns true,
     * this method should not throw an exception.
     * 
     * @return $this (method chainable)
     * @throws \InvalidArgumentException If the provided directory is not a
     *     valid pluggable of the correct type defined by the importer.
     */
    public function import(string $directory): \Oroboros\core\interfaces\pluggable\PluggableImporterInterface
    {
        $this->importPluggable($directory);
        return $this;
    }

    /**
     * Lists all of the imported pluggables
     * of the type handled by this importer.
     * @return string
     */
    public function list(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        return self::$pluggables[static::PLUGGABLE_TYPE];
    }

    /**
     * Returns a boolean determination as to whether the given pluggable name is registered.
     * If this method returns true, then the `get` method will not throw an exception.
     * If it returns false, the `get` method will always throw an exception.
     * @param string $pluggable
     * @return bool
     */
    public function has(string $pluggable): bool
    {
        return self::$pluggables[static::PLUGGABLE_TYPE]->has($pluggable);
    }

    /**
     * Returns the definer for the given pluggable.
     * All other assets provided by the pluggable can be retrieved from the definer.
     * @param string $pluggable
     * @return \Oroboros\core\interfaces\pluggable\PluggableInterface
     * @throws \InvalidArgumentException If there is no pluggable of the given name registered.
     */
    public function get(string $pluggable): \Oroboros\core\interfaces\pluggable\PluggableInterface
    {
        if (!$this->has($pluggable)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided pluggable [%2$s] does not exist.', get_class($this), $pluggable));
        }
        return self::$pluggables[static::PLUGGABLE_TYPE]->get($pluggable);
    }

    /**
     * Returns basic information about a specific pluggable.
     * Specific pluggable importers should extend on this method and add
     * any additional keys to the info as per their pluggable type.
     * 
     * All pluggables should have the keys provided by
     * the baseline definition of this method.
     * 
     * @return string
     * @throws \InvalidArgumentException If there is no pluggable of the given name registered.
     */
    public function info(string $pluggable): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $info = [];
        $definer = $this->get($pluggable);
        $info['name'] = $definer->name();
        $info['type'] = static::PLUGGABLE_TYPE;
        $info['title'] = $definer->title();
        $info['description'] = $definer->description();
        $info['path'] = $definer->path();
        $info['namespace'] = $definer->baseNamespace();
        $info['version'] = $definer->version();
        $info['manifest'] = $this::containerize($definer->manifest());
        return $this::containerize($info);
    }

    /**
     * Fetches a container of all defined pluggables of the current type
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    public function fetch(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        return self::$pluggables[static::PLUGGABLE_TYPE];
    }

    /**
     * Internal getter for the manifest of the pluggable being evaluated
     * @return array
     */
    protected function getManifest(string $path): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        if (!is_dir($path)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Provided path [%2$s] is not a directory.', get_class($this), $path));
        }
        $manifest = $path . 'manifest.json';
        if (!is_readable($manifest)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Expected manifest [%2$s] does not exist or is not readable.', get_class($this), $manifest));
        }
        return $this->load('library', 'parser\\JsonParser', $manifest)->fetch();
    }

    /**
     * Returns a boolean determination as to whether the pluggable provided is valid.
     * @param string $path
     * @return bool
     */
    protected function verifyPluggable(string $path): bool
    {
        try {
            $manifest = $this->getManifest($path);
            $this->verifyManifest($manifest);
            $definer_file = sprintf('%1$s%3$s%2$s.php', rtrim($path, DIRECTORY_SEPARATOR), $manifest->get('definer'), DIRECTORY_SEPARATOR);
            if (!is_readable($definer_file)) {
                // Invalid definer
                return false;
            }
            if (!in_array($definer_file, get_included_files())) {
                require_once $definer_file;
            }
            $definer = sprintf('%1$s\\%2$s', $manifest->get('namespace'), $manifest->get('definer'));
            $this->getLogger()->debug('[type][scope][class] Importing [type] [name] with definer class: [definer]', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'type' => static::PLUGGABLE_TYPE,
            'name' => $manifest->get('name'),
            'definer' => $definer,
        ]);
            $pluggable = new $definer();
            $pluggable->setLogger($this->getLogger());
            $this->verifyPluggableDefiner($pluggable);
        } catch (\InvalidArgumentException $e) {
            return false;
        }
        return true;
    }

    /**
     * Imports a pluggable. All assets will be lazy loaded on demand,
     * but will load correctly after the pluggable base folder has
     * been passed to this method.
     * @param string $path
     * @return void
     * @throws \InvalidArgumentException If the pluggable is not valid,
     *     or is the wrong type for the current importer.
     */
    protected function importPluggable(string $path): void
    {
        if (!$this->verifyPluggable($path)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided path [%2$s] does not resolve to a valid pluggable of type [%3$s],'
                        , get_class($this), $path, static::PLUGGABLE_TYPE));
        }
        $manifest = $this->getManifest($path);
        $this->verifyManifest($manifest);
        $definer_file = sprintf('%1$s%3$s%2$s.php', rtrim($path, DIRECTORY_SEPARATOR), $manifest->get('definer'), DIRECTORY_SEPARATOR);
        $definer = sprintf('%1$s\\%2$s', $manifest->get('namespace'), $manifest->get('definer'));
        $pluggable = new $definer();
        self::$pluggables[static::PLUGGABLE_TYPE][$pluggable->name()] = $pluggable;
        $this->getAutoloader()->addNamespace($pluggable->baseNamespace(), $pluggable->source(), true);
    }

    /**
     * 
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $manifest
     * @return void
     * @throws \InvalidArgumentException If the given manifest is missing required keys,
     *     or if the type declaration in the given manifest does not match the
     *     type declaration of the current importer.
     */
    protected function verifyManifest(\Oroboros\core\interfaces\library\container\ContainerInterface $manifest): void
    {
        $bad_key_msg = 'Error encountered in [%1$s]. Provided manifest does not include expected key [%2$s].';
        $expected_keys = $this->declareExpectedManifestKeys();
        foreach ($expected_keys as $key) {
            if (!$manifest->has($key)) {
                // No namespace provided. Not valid.
                throw new \Oroboros\core\exception\InvalidArgumentException(sprintf($bad_key_msg, get_class($this), $key));
            }
        }
        if ($manifest->get('type') !== static::PLUGGABLE_TYPE) {
            // Incorrect pluggable type. Not valid.
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided manifest is not the correct pluggable type. '
                        . 'Expected [%3$s] but received [%2$s].'
                        , get_class($this), $manifest->get('type'), static::PLUGGABLE_TYPE));
        }
    }

    /**
     * Must be overridden to perform the import process from the pluggable.
     * @return void
     */
    abstract protected function importAssets(\Oroboros\core\interfaces\pluggable\PluggableInterface $pluggable): void;

    /**
     * Must be overridden to declare the interface the pluggable definer is expected to adhere to.
     * @return void
     */
    abstract protected function declareExpectedPluggableType(): string;

    /**
     * Returns the keys that must be present in a valid manifest of this type.
     * May be overridden to provide additional expected keys.
     * All pluggables must contain the baseline keys provided by the default here.
     * These values should be propogated forward if this method is overridden.
     * @return array
     */
    protected function declareExpectedManifestKeys(): array
    {
        return self::$default_manifest_required_keys;
    }

    /**
     * Returns the autoloader, for registering the namespace of the pluggable asset.
     * @return \Oroboros\core\library\autoload\Autoloader
     */
    protected function getAutoloader(): \Oroboros\core\library\autoload\Autoloader
    {
        return self::$autoloader;
    }

    /**
     * Verifies that the given pluggable is correct for the current importer to process.
     * @param object $definer
     * @return void
     * @throws \InvalidArgumentException If the provided definer does not adhere to the expected interface
     */
    protected function verifyPluggableDefiner(object $definer): void
    {
        $expected = $this->declareExpectedPluggableType();
        if (!($definer instanceof $expected)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided class [%2$s] does not implement expected interface [%3$s].'
                        , get_class($this), get_class($definer), $expected));
        }
    }

    private function initializePluggables(): void
    {
        if (is_null(self::$pluggables)) {
            self::$pluggables = $this::containerize();
        }
        if (!self::$pluggables->has(static::PLUGGABLE_TYPE)) {
            self::$pluggables[static::PLUGGABLE_TYPE] = $this::containerize();
        }
    }

    private function initializeAutoloader(): void
    {
        if (is_null(self::$autoloader)) {
            self::$autoloader = new \Oroboros\core\library\autoload\Autoloader();
        }
    }

    private function verifyPluggableType(): void
    {
        if (is_null(static::PLUGGABLE_TYPE)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(sprintf('Class [%1$s] is invalid. '
                        . 'Expected class constant [%2$s] must have a non-null value.'
                        , get_class($this), 'PLUGGABLE_TYPE'));
        }
    }
}
