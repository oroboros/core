<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\pluggable;

/**
 * Abstract Pluggable
 * Baseline abstraction for pluggable features
 * (extensions, modules, services, etc)
 *
 * @author Brian Dayhoff
 */
abstract class AbstractPluggable extends \Oroboros\core\abstracts\AbstractBase implements \Oroboros\core\interfaces\pluggable\PluggableInterface
{

    use \Oroboros\core\traits\LoaderTrait;
    use \Oroboros\core\traits\ContainerPackagerUtility;
    use \Oroboros\core\traits\pattern\ObservableTrait;
    use \Oroboros\core\traits\pattern\ObserverTrait;
    use \Oroboros\core\traits\pattern\EventAwareTrait;
    use \Oroboros\core\traits\pattern\FilterAwareTrait;
    use \Oroboros\core\traits\model\ModelAwareUtilityTrait;
    
    const CLASS_TYPE = 'pluggable';
    const CLASS_SCOPE = null;
    
    const CONTAINER_CLASS = 'Oroboros\\core\\library\\container\\Container';
    const COLLECTION_CLASS = 'Oroboros\\core\\library\\container\\Collection';

    const MANIFEST_FILENAME = 'manifest.json';

    /**
     * Reflection class used to get info about the child class
     * @var \ReflectionClass
     */
    private $reflector = null;

    /**
     * Array of details about the pluggable
     * @var array
     */
    private $manifest = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeReflector();
        $this->initializeManifest();
        $this->initializeModelAware();
        $this->initializeObservable();
        $this->initializeObserver();
        $this->initializeEvents();
        $this->initializeFilters();
    }

    /**
     * Returns the identifier slug name of the pluggable
     * @return string
     */
    public function name(): string
    {
        return $this->manifest()['name'];
    }

    /**
     * Returns the human readable name of the pluggable
     * @return string
     */
    public function title(): string
    {
        return $this->manifest()['title'];
    }

    /**
     * Returns the description of the pluggable
     * @return string
     */
    public function description(): string
    {
        return $this->manifest()['description'];
    }

    /**
     * Returns the absolute directory path of the pluggable asset
     * @return string
     */
    public function path(): string
    {
        $dirname = dirname($this->reflector->getFileName()) . DIRECTORY_SEPARATOR;
        return $dirname;
    }

    /**
     * Returns the absolute directory path of the pluggable asset code source
     * 
     * The default is a directory called "lib" inside the pluggable base folder.
     * Override this if you use an alternate directory structure.
     * 
     * @return string
     */
    public function source(): string
    {
        $dirname = $this->path() . 'lib' . DIRECTORY_SEPARATOR;
        return $dirname;
    }

    /**
     * Returns the absolute directory path of the pluggable asset configuration
     * 
     * The default is a directory called "config" inside the pluggable base folder.
     * Override this if you use an alternate directory structure.
     * 
     * @return string
     */
    public function config(): string
    {
        $dirname = $this->path() . 'config' . DIRECTORY_SEPARATOR;
        return $dirname;
    }

    /**
     * Returns the vendor/package name of the pluggable
     * @return string
     */
    public function baseNamespace(): string
    {
        return $this->manifest()['namespace'];
    }

    /**
     * Returns the semver compatible version of the pluggable
     * @return string
     */
    public function version(): string
    {
        return $this->manifest()['version'];
    }

    /**
     * Returns the manifest as an associative array
     * @return array
     */
    public function manifest(): array
    {
        return $this->getManifest();
    }

    /**
     * Internal getter for the reflector, if needed by extending classes
     * @return \ReflectionClass
     */
    protected function getReflector(): \ReflectionClass
    {
        return $this->reflector;
    }

    /**
     * Internal getter for the manifest, if needed by extending classes
     * @return array
     */
    protected function getManifest(): array
    {
        return $this->manifest;
    }
    
    /**
     * Sets the observable name for the pluggable
     * @return string
     */
    protected function declareObservableName(): string
    {
        return trim(sprintf('%1$s:%2$s', static::CLASS_TYPE, static::CLASS_SCOPE), ':');
    }

    private function initializeReflector(): void
    {
        $reflector = new \ReflectionClass($this);
        $this->reflector = $reflector;
    }

    private function initializeManifest(): void
    {
        $proxy = new \Oroboros\core\pattern\factory\LoaderProxy();
        $file = $this->path() . static::MANIFEST_FILENAME;
        $this->manifest = $proxy->load('library', 'parser\\JsonParser', $file)->fetch()->toArray();
    }
}
