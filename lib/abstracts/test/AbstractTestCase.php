<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\test;

/**
 * Provides abstraction for running test cases with PHPUnit
 * @coversDefaultClass \Oroboros\core\Oroboros
 * @coversDefaultClass \Oroboros\core\abstracts\AbstractBase
 * @coversDefaultClass \Oroboros\core\abstracts\test\AbstractTestCase
 * @author Brian Dayhoff
 */
abstract class AbstractTestCase extends \PHPUnit\Framework\TestCase implements \Oroboros\core\interfaces\test\TestInterface
{

    use \Oroboros\core\traits\LoaderTrait;
    use \Oroboros\core\traits\ContainerPackagerUtility;
    use \Oroboros\core\traits\model\ModelAwareUtilityTrait;

    /**
     * Designates further child classes as unit tests.
     */
    const CLASS_TYPE = 'unit-test';

    /**
     * Class scope must be provided by abstraction, if applicable.
     * This should be a loader type (eg library, controller, model, view, component, etc)
     */
    const CLASS_SCOPE = \Oroboros\core\abstracts\AbstractBase::CLASS_TYPE;

    /**
     * Test subject must be provided by abstraction.
     */
    const TEST_SUBJECT = null;

    /**
     * Expected interface must be provided by abstraction.
     */
    const EXPECTED_INTERFACE = \Oroboros\core\interfaces\BaseInterface::class;

    /**
     * The object currently being tested.
     * 
     * @var \Oroboros\core\abstracts\AbstractBase
     */
    private $current_test_subject = null;

    /**
     * Test command to use for constructor testing.
     * 
     * @var string|null
     */
    private $test_constructor_command = null;

    /**
     * Test arguments to use for constructor testing.
     * 
     * @var string|null
     */
    private $test_constructor_arguments = null;

    /**
     * Test flags to use for constructor testing.
     * 
     * @var string|null
     */
    private $test_constructor_flags = null;

    /**
     * Represents the instantiated test subject.
     * @var type
     */
    private $subject = null;

    /**
     * The valid class scopes that can be tested
     * @var array
     */
    private static $valid_scopes = [
        \Oroboros\core\abstracts\adapter\AbstractAdapter::CLASS_TYPE,
        \Oroboros\core\abstracts\bootstrap\AbstractBootstrap::CLASS_TYPE,
        \Oroboros\core\abstracts\controller\AbstractController::CLASS_TYPE,
        \Oroboros\core\abstracts\extension\AbstractExtension::CLASS_TYPE,
        \Oroboros\core\abstracts\pattern\factory\AbstractFactory::CLASS_TYPE,
        \Oroboros\core\abstracts\library\AbstractLibrary::CLASS_TYPE,
        \Oroboros\core\abstracts\library\component\AbstractComponent::CLASS_TYPE,
        \Oroboros\core\abstracts\library\container\AbstractContainer::CLASS_TYPE,
        \Oroboros\core\abstracts\model\AbstractModel::CLASS_TYPE,
        \Oroboros\core\abstracts\module\AbstractModule::CLASS_TYPE,
        \Oroboros\core\abstracts\pluggable\AbstractPluggable::CLASS_TYPE,
        \Oroboros\core\abstracts\service\AbstractService::CLASS_TYPE,
        \Oroboros\core\abstracts\view\AbstractView::CLASS_TYPE,
    ];

    /**
     * Use the default container
     */
    const CONTAINER_CLASS = 'Oroboros\\core\\library\\container\\Container';

    public function __construct($name = null, $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->validateTestCase();
        $this->validateExpectedInterface();
        $this->initializeModelAware();
        $this->setupTestParameters();
        $this->instantiateTestSubject();
    }

    /**
     * Tests whether the defined subject implements the expected interface definition.
     * 
     * @group interfaces
     * @return void
     */
    public function testInterface(): void
    {
        $this->instantiateTestSubject();
        $this->assertTrue($this->hasInterface(static::EXPECTED_INTERFACE, $this->getCurrentTestSubject()),
            sprintf('Provided class [%1$s] does not implement expected interface [%2$s].',
                get_class($this->getCurrentTestSubject()), static::EXPECTED_INTERFACE)
        );
    }

    /**
     * Returns the declared test type
     * 
     * @return string|null
     */
    protected function getTestType(): string
    {
        return static::CLASS_TYPE;
    }

    /**
     * Returns the scope of the unit test.
     * 
     * This will correspond to the class type of the class being tested,
     * or null if the test subject does not declare a class type
     * 
     * @return string|null
     */
    protected function getTestScope(): ?string
    {
        return static::CLASS_SCOPE;
    }

    /**
     * Test command getter
     * @return string|null
     */
    protected function getTestCommand(): ?string
    {
        return $this->test_constructor_command;
    }

    /**
     * Test argument getter
     * @return array|null
     */
    protected function getTestArguments(): ?array
    {
        return $this->test_constructor_arguments;
    }

    /**
     * Test flag getter
     * @return array|null
     */
    protected function getTestFlags(): ?array
    {
        return $this->test_constructor_flags;
    }

    /**
     * Getter for the current test subject
     * 
     * @return \Oroboros\core\abstracts\AbstractBase|null
     */
    protected function getCurrentTestSubject(): ?\Oroboros\core\abstracts\AbstractBase
    {
        return $this->current_test_subject;
    }

    /**
     * Resets all constructor test parameters to null
     * @return void
     */
    protected function resetTestParameters(): void
    {
        $this->setTestCommand();
        $this->setTestArguments();
        $this->setTestFlags();
    }

    /**
     * Resets the current test subject, firing it's destructor
     * 
     * @return void
     */
    protected function resetCurrentTestSubject(): void
    {
        $this->current_test_subject = null;
    }

    /**
     * Test command setter
     * @param string $command
     * @return void
     */
    protected function setTestCommand(string $command = null): void
    {
        $this->test_constructor_command = $command;
    }

    /**
     * Test argument setter
     * @param array $arguments
     * @return void
     */
    protected function setTestArguments(array $arguments = null): void
    {
        $this->test_constructor_arguments = $arguments;
    }

    /**
     * Test flag setter
     * @param array $flags
     * @return void
     */
    protected function setTestFlags(array $flags = null): void
    {
        $this->test_constructor_flags = $flags;
    }

    /**
     * Setter for the current test subject
     * 
     * @param \Oroboros\core\abstracts\AbstractBase $subject
     * @return void
     */
    protected function setCurrentTestSubject(\Oroboros\core\abstracts\AbstractBase $subject): void
    {
        $this->current_test_subject = $subject;
    }

    /**
     * Returns the test subject class name
     * 
     * Override this method if you need to declare a test subject
     * some other way than using the class constant.
     * 
     * @return string
     */
    protected function declareTestSubjectClass(): string
    {
        return static::TEST_SUBJECT;
    }

    /**
     * Returns the interface expected to be implemented by the test subject
     * 
     * @return string
     */
    protected function declareExpectedInterface(): string
    {
        return static::EXPECTED_INTERFACE;
    }

    /**
     * Returns a boolean designation as to whether a given object implements
     * the given interface.
     * @param string $interface
     * @param object $subject
     * @return bool
     */
    protected function hasInterface(string $interface, object $subject): bool
    {
        $implemented = class_implements($subject);
        return in_array($interface, $implemented);
    }

    /**
     * Instantiates the test subject.
     * @covers ::__construct
     * @return void
     */
    protected function instantiateTestSubject(): void
    {
        if (!is_null(static::TEST_SUBJECT) && is_null($this->getCurrentTestSubject())) {
            if (in_array($this->getTestScope(), ['bootstrap'])) {
                $class = $this->declareTestSubjectClass();
                $subject = new $class(
                    $this->getTestCommand(),
                    $this->getTestArguments(),
                    $this->getTestFlags()
                );
            } else {
                $subject = $this->load(
                    $this->getTestScope(),
                    $this->declareTestSubjectClass(),
                    $this->getTestCommand(),
                    $this->getTestArguments(),
                    $this->getTestFlags()
                );
            }
            $this->setCurrentTestSubject($subject);
        }
    }

    /**
     * Override this method to setup any preliminary arguments,
     * construct dependencies, or otherwise prepare parameters
     * for test subject instantiation.
     * 
     * @return void
     */
    protected function setupTestParameters(): void
    {
        // no-op
    }

    /**
     * Verifies that a class scope is declared
     * 
     * Class scope must exist in the array of valid scopes,
     * which correspond to core class types
     * 
     * @return void
     * @throws \Oroboros\core\exception\ErrorException
     */
    private function validateTestCase(): void
    {
        if (is_null(static::CLASS_SCOPE)) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. No CLASS_SCOPE was provided. Valid scopes are [%2$s]', get_class($this), implode(', ', self::$valid_scopes)));
        }
        if (!in_array(static::CLASS_SCOPE, self::$valid_scopes)) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. Provided CLASS_SCOPE [%2$s] is not valid. Valid scopes are [%3$s]', get_class($this), static::CLASS_SCOPE, implode(', ', self::$valid_scopes)));
        }
    }

    /**
     * Verifies that the expected interface exists.
     * 
     * All classes will always be checked to insure they implement
     * the correct interface for the given test set.
     * 
     * @return void
     * @throws \Oroboros\core\exception\ErrorException
     */
    private function validateExpectedInterface(): void
    {
        if (is_null(static::EXPECTED_INTERFACE)) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. No EXPECTED_INTERFACE class constant was provided.', get_class($this)));
        }
        if (!interface_exists(static::EXPECTED_INTERFACE)) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. Provided EXPECTED_INTERFACE [%2$s] is not a valid interface.', get_class($this), static::EXPECTED_INTERFACE));
        }
    }
}
