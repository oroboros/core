<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\adapter\cli;

/**
 * Abstract Shell Adapter
 * Provides baseline methods for interacting with the shell
 *
 * @author Brian Dayhoff
 */
abstract class AbstractShellAdapter extends \Oroboros\core\abstracts\adapter\AbstractAdapter implements \Oroboros\core\interfaces\adapter\cli\ShellAdapterInterface
{

    use \Oroboros\core\traits\pattern\ObservableTrait;

    const CLASS_SCOPE = 'cli';
    const ADAPTER_TYPE = 'cli';
    const ADAPTER_SCOPE = 'shell';
    const DEFAULT_PROCESS_TYPE = '/bin/bash';

    private static $exec_function = null;
    private static $call_function = null;
    private $command = null;
    private $is_open = false;
    private $stdin = null;
    private $stdout = null;
    private $stderr = null;
    private $process = null;
    private $cwd = null;
    private $env = null;
    private $output = null;
    private $error = null;
    private $code = 0;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeObservable();
        $this->verifyCli();
        $this->verifyShellPath();
        $this->verifyShellEnvironment();
    }

    public function __destruct()
    {
        if ($this->isOpen()) {
            $this->close();
        }
    }

    public function execute(string $command): \Oroboros\core\interfaces\adapter\cli\ShellAdapterInterface
    {
        if (!$this->isOpen()) {
            $this->open();
        }
        $this->handleCommand($command);
        $this->close();
        return $this;
    }

    public function getOutput(): ?string
    {
        return $this->output;
    }

    public function getError(): ?string
    {
        return $this->error;
    }

    public function getCode(): int
    {
        return $this->code;
    }

    public function open(): \Oroboros\core\interfaces\adapter\cli\ShellAdapterInterface
    {
        if ($this->isOpen()) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. Process is already open.', get_class($this)));
        }
        $process = proc_open(static::DEFAULT_PROCESS_TYPE, $this->getSpec(), $pipes, $this->cwd, $this->env);
        if (!is_resource($process)) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. '
                        . 'Return response of provided command [%2$s] did not return '
                        . 'a bound process.', get_class($this), $this->command));
        }
        $this->process = &$process;
        $this->stdin = &$pipes[0];
        $this->stdout = &$pipes[1];
        $this->stderr = &$pipes[2];
        $this->is_open = true;
        return $this;
    }

    public function isOpen(): bool
    {
        return $this->is_open;
    }

    public function close(): \Oroboros\core\interfaces\adapter\cli\ShellAdapterInterface
    {
        if (!$this->isOpen()) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. Process is already closed.', get_class($this)));
        }
        if (is_resource($this->stdin)) {
            fclose($this->stdin);
        }
        if (is_resource($this->stdout)) {
            fclose($this->stdout);
        }
        if (is_resource($this->stderr)) {
            fclose($this->stderr);
        }
        $this->code = proc_close($this->process);
        $this->is_open = false;
        $this->handleResponseCode($this->code);
        $this->stdin = null;
        $this->stdout = null;
        $this->stderr = null;
        $this->process = null;
        $this->command = null;
        return $this;
    }

    public function reset(): \Oroboros\core\interfaces\adapter\cli\ShellAdapterInterface
    {
        $this->code = 0;
        $this->output = null;
        $this->error = null;
        if ($this->isOpen()) {
            $this->close;
        }
        return $this;
    }

    protected function sanitizeInput(string $input): string
    {
        $sanitized = escapeshellcmd($input);
        return $sanitized;
    }

    protected function sanitizeArgument(string $input): string
    {
        $sanitized = escapeshellarg($input);
        return $sanitized;
    }

    protected function handleCommand(string $input): void
    {
        $command = $this->sanitizeInput($input);
        fwrite($this->stdin, $command);
        if (is_resource($this->stdin)) {
            fclose($this->stdin);
        }
        $output = stream_get_contents($this->stdout);
        $this->handleResponse($output);
        $error = stream_get_contents($this->stderr);
        $this->handleError($error);
    }

    protected function handleResponse(string $output): void
    {
        if ($output !== '') {
            $this->output = $output;
            $this->setObserverEvent('output');
        } else {
            $this->output = null;
        }
    }

    protected function handleError(string $error): void
    {
        if ($error !== '') {
            $this->error = $error;
            $this->setObserverEvent('error');
        } else {
            $this->error = null;
        }
    }

    protected function handleResponseCode(int $code): void
    {
        $this->code = $code;
        if ($code === 0) {
            $this->setObserverEvent('success');
        } else {
            $this->setObserverEvent('failure');
        }
    }

    /**
     * Returns the spec template for `proc_open`.
     * By default this uses pipes for all three I/O streams.
     * This method may be overridden to provide alternate behavior.
     * @return array
     */
    protected function getSpec(): array
    {
        return array(
            0 => array("pipe", "r"), // stdin is a pipe that the child will read from
            1 => array("pipe", "w"), // stdout is a pipe that the child will write to
            2 => array("pipe", "w") // stderr is a pipe that the child will write to
        );
    }

    /**
     * Standard observer definition
     * @return string
     */
    protected function declareObservableName(): string
    {
        return trim(sprintf('%1$s:%2$s', static::CLASS_TYPE, static::CLASS_SCOPE), ':');
    }

    /**
     * Verifies that the underlying PHP functionality
     * for usage of the adapter is enabled.
     * @return void
     * @throws \ErrorException If the required `proc_open` method used
     *     to call the command line is not available.
     */
    private function verifyShellEnvironment(): void
    {
        if (!function_exists('proc_open')) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. '
                        . 'Required adapter function [%2$s] is disabled.', get_class($this), 'proc_open'));
        }
    }

    /**
     * Verifies that the given shell path is readable.
     * If no shell path is supplied, the current working directory will be used.
     * @return void
     * @throws \ErrorException If the given shell path is not readable.
     */
    private function verifyShellPath(): void
    {
        if (is_null($this->cwd) && $this->hasArgument('cwd')) {
            $this->cwd = rtrim($this->getArgument('cwd'), DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;
        } elseif (is_null($this->cwd)) {
            $this->cwd = rtrim(getcwd(), DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;
        }
        if (!is_dir($this->cwd)) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. Provided path [%2$s] is not a directory.', get_class($this), $this->cwd));
        } elseif (!is_readable($this->cwd)) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. Provided directory [%2$s] is not readable.', get_class($this), $this->cwd));
        }
    }

    /**
     * Verifies that the environment is an array.
     * If no environment is provided, the current runtime environment will be used.
     * @return void
     * @throws \InvalidArgumentException If the provided environment is not an array
     */
    private function verifyEnv(): void
    {
        if (is_null($this->env) && $this->hasArgument('environment')) {
            $this->env = $this->getArgument('environment');
        } elseif (is_null($this->cwd)) {
            $this->env = $_ENV;
        }
        if (!is_array($this->env)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Provided environment must be an array.'));
        }
    }

    /**
     * Verifies that the shell adapter only runs from CLI mode.
     * @return void
     * @throws \ErrorException If the current runtime is not a command line operation
     */
    private function verifyCli(): void
    {
        if (!$this::IS_CLI) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. '
                        . 'Adapter [%1$s] can only be run in a cli environment.', get_class($this)));
        }
    }
}
