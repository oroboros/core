<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\adapter\supervisor;

/**
 * Abstract Supervisor Adapter
 * Provides abstraction for interacting with supervisor
 *
 * @author Brian Dayhoff
 */
abstract class AbstractSupervisorAdapter extends \Oroboros\core\abstracts\adapter\AbstractAdapter implements \Oroboros\core\interfaces\adapter\supervisor\SupervisorAdapterInterface
{

    const CLASS_SCOPE = 'supervisor';
    const ADAPTER_TYPE = 'supervisor';

    /**
     * The registered credentials for supervisor connections
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private static $connections = null;

    /**
     * The default supervisor connection, if applicable.
     * Null if no default connection exists.
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface|null
     */
    private static $default_connection = null;

    /**
     * The currently scoped supervisor connection.
     * This adapter will not operate without a valid connection.
     * 
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface|null
     */
    private $connection = null;

    /**
     * The XML-RPC connector used to connect to supervisor
     * @var \fXmlRpc\Client
     */
    private $connector = null;

    /**
     * The underlying supervisor control object
     * @var \Supervisor\Supervisor 
     */
    private $supervisor = null;

    /**
     * The currently running processes.
     * 
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface|null
     */
    private $processes = null;

    /**
     * Instantiates the supervisor adapter
     * @param string $command (optional) The connection key to use.
     *     If not provided, the default connection will be used.
     * @param array $arguments (optional) Any arguments to pass into the adapter
     * @param array $flags (optional) Any flags to pass into the adapter
     * 
     */
    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeConnections();
        $this->initializeCurrentConnection();
        $this->initializeConnector();
        $this->initializeSupervisor();
        $this->initializeProcesses();
    }

    /**
     * Returns an object representation of a given process
     * @param string $process
     * @return \Supervisor\Process
     * @throws \InvalidArgumentException If the given process name does not exist
     */
    public function process(string $process): \Supervisor\Process
    {
        $this->checkProcess($process);
        return $this->processes->get($process);
    }

    /**
     * Returns a boolean determination as to whether a given process exists
     * @param string $process
     * @return bool
     */
    public function hasProcess(string $process): bool
    {
        return $this->processes->has($process);
    }

    /**
     * Returns an indexed array of valid process names
     * @return array
     */
    public function list(): array
    {
        return array_keys($this->processes->toArray());
    }

    /**
     * Returns a container of payload details for a given process
     * @param string $process
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     * @throws \InvalidArgumentException If the given process name does not exist
     */
    public function payload(string $process): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        return $this::containerize($this->process($process)->getPayload());
    }

    /**
     * Starts a given process
     * @param string $process
     * @return $this (method chainable)
     * @throws \InvalidArgumentException If the given process name does not exist
     */
    public function start(string $process, bool $wait = false): \Oroboros\core\interfaces\adapter\supervisor\SupervisorAdapterInterface
    {
        $this->checkProcess($process);
        return $this;
    }

    /**
     * Stops a given process
     * @param string $process
     * @return $this (method chainable)
     * @throws \InvalidArgumentException If the given process name does not exist
     */
    public function stop(string $process): \Oroboros\core\interfaces\adapter\supervisor\SupervisorAdapterInterface
    {
        $this->checkProcess($process);
        return $this;
    }

    /**
     * Returns a container of details outlining the status of a given process
     * @param string $process
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     * @throws \InvalidArgumentException If the given process name does not exist
     */
    public function status(string $process): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $this->checkProcess($process);
        return $this::containerize($this->supervisor->getProcessInfo($process));
    }

    /**
     * Returns a boolean determination as to whether
     * the supervisor connection is currently running
     * @return bool
     */
    public function isRunning(): bool
    {
        return $this->supervisor->isRunning();
    }

    /**
     * Returns a boolean determination as to whether
     * a valid connection to supervisor was made
     * @return bool
     */
    public function isConnected(): bool
    {
        return $this->supervisor->isConnected();
    }

    protected function checkProcess(string $process): void
    {
        if (!$this->processes->has($process)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'No process exists for provided identifier [%2$s].'
                        , get_class($this), $process));
        }
    }

    private function initializeProcesses(): void
    {
        $this->processes = $this::containerize();
        foreach ($this->supervisor->getAllProcesses() as $process) {
            $this->processes[$process->getName()] = $process;
        }
    }

    private function initializeConnector(): void
    {
        $httpClient = new \GuzzleHttp\Client();
        $client = new \fXmlRpc\Client(
            $this->getXmlRpcConnectionString(),
            new \fXmlRpc\Transport\HttpAdapterTransport(
                new \Http\Message\MessageFactory\DiactorosMessageFactory(),
                new \Http\Adapter\Guzzle7\Client($httpClient)
            )
        );
        $this->connector = $client;
    }

    private function initializeSupervisor(): void
    {
        $this->supervisor = new \Supervisor\Supervisor($this->connector);
    }

    private function getXmlRpcConnectionString(): string
    {
        if (is_null($this->connection)) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. '
                        . 'Cannot connect to supervisor without a defined connection. '
                        . 'Please provide a valid connection key on instantiation or '
                        . 'configure a default connection.', get_class($this)));
        }
        $con = sprintf('http://%1$s:%2$s/RPC2', $this->connection->get('host'), $this->connection->get('port'));
        return $con;
    }

    private function initializeCurrentConnection(): void
    {
        if (!is_null($this->getCommand())) {
            if (self::$connections->has($this->getCommand())) {
                $this->connection = self::$connections->get($this->getCommand());
            } else {
                throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                            . 'Requested supervisor connection [%2$s] does not exist.'
                            , get_class($this), $this->getCommand()));
            }
        } elseif (!is_null(self::$default_connection)) {
            $this->connection = self::$default_connection;
        }
    }

    private function initializeConnections(): void
    {
        if (is_null(self::$connections)) {
            self::$connections = $this::containerize();
            if ($this::app()->environment()->has('application') && $this::app()->environment()->get('application')->has('connections') && array_key_exists('supervisord', $this::app()->environment()->get('application')->get('connections'))
            ) {
                foreach ($this::app()->environment()->get('application')->get('connections')['supervisord'] as $key => $value) {
                    self::$connections[$key] = $this::containerize($value);
                }
            }
            $this->initializeDefaultConnection();
        }
    }

    private function initializeDefaultConnection(): void
    {
        if (is_null(self::$default_connection)) {
            if ($this::app()->environment()->has('application') && $this::app()->environment()->get('application')->has('connections') && array_key_exists('defaults', $this::app()->environment()->get('application')->get('connections')) && array_key_exists('supervisord', $this::app()->environment()->get('application')->get('connections')['defaults'])
            ) {
                $con = $this::app()->environment()->get('application')->get('connections')['defaults']['supervisord'];
                if (self::$connections->has($con)) {
                    self::$default_connection = self::$connections->get($con);
                } else {
                    // Error, misconfigured default connection.
                    throw new \LogicException(sprintf('Error encountered in [%1$s]. '
                                . 'Specified default supervisor connection [%2$s] is not defined.'
                                , get_class($this), $con));
                }
            }
        }
    }
}
