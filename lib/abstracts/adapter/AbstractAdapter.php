<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\adapter;

/**
 * Abstract Adapter
 * Defines a set of methods common to all adapter classes
 *
 * @author Brian Dayhoff
 */
abstract class AbstractAdapter extends \Oroboros\core\abstracts\library\AbstractLibrary implements \Oroboros\core\interfaces\adapter\AdapterInterface
{
    use \Oroboros\core\traits\pattern\ObservableTrait;
    use \Oroboros\core\traits\pattern\ObserverTrait;
    use \Oroboros\core\traits\pattern\EventAwareTrait;
    use \Oroboros\core\traits\pattern\FilterAwareTrait;

    /**
     * Designates further child classes as adaptera.
     */
    const CLASS_TYPE = 'adapter';

    /**
     * Class scope must be provided by abstraction, if applicable.
     */
    const CLASS_SCOPE = null;

    /**
     * Adapter type must be provided by abstraction, if applicable.
     */
    const ADAPTER_TYPE = null;

    /**
     * Adapter scope must be provided by abstraction, if applicable.
     */
    const ADAPTER_SCOPE = null;

    /**
     * Use the default container
     */
    const CONTAINER_CLASS = 'Oroboros\\core\\library\\container\\Container';

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeObservable();
        $this->initializeObserver();
        $this->initializeEvents();
        $this->initializeFilters();
        $this->setObserverEvent('ready');
    }
    
    /**
     * Sets the observable name for the bootstrap
     * @return string
     */
    protected function declareObservableName(): string
    {
        return trim(sprintf('%1$s:%2$s', static::CLASS_TYPE, static::CLASS_SCOPE), ':');
    }
}
