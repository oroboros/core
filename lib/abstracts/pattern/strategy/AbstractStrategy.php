<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\pattern\strategy;

/**
 * Abstract Strategy
 * Provides abstraction for implementing a strategy pattern.
 *
 * @author Brian Dayhoff
 */
abstract class AbstractStrategy extends \Oroboros\core\abstracts\AbstractBase implements \Oroboros\core\interfaces\pattern\strategy\StrategyInterface
{

    use \Oroboros\core\traits\LoaderTrait;
    use \Oroboros\core\traits\ContainerPackagerUtility;

    /**
     * Designates further child classes as libraries.
     */
    const CLASS_TYPE = 'pattern';

    /**
     * Class scope must be provided by abstraction, if applicable.
     */
    const CLASS_SCOPE = 'strategy';

    /**
     * Strategy type must be provided by abstraction, if applicable.
     */
    const STRATEGY_TYPE = null;

    /**
     * Strategy scope must be provided by abstraction, if applicable.
     */
    const STRATEGY_SCOPE = null;

    /**
     * Use the default container
     */
    const CONTAINER_CLASS = 'Oroboros\\core\\library\\container\\Container';

    /**
     * Must be overridden by the extending class with the name of a valid interface that extends the base interface.
     */
    const STRATEGY_TARGET_INTERFACE = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        $this->verifyStrategyTargetInterface();
        parent::__construct($command, $arguments, $flags);
    }

    /**
     * Call this method to verify that a provided subject parameter
     * conforms to the expected target interface.
     * If this does not apply, omit this step.
     * @param type $subject
     * @return void
     * @throws \InvalidArgumentException
     */
    protected function verifySubjectInterface($subject): void
    {
        if (!is_object($subject) || !in_array(static::STRATEGY_TARGET_INTERFACE, class_implements($subject))) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided subject is invalid. Expected instance of [%2$s], '
                        . 'received [%3$s].', get_class($this), static::STRATEGY_TARGET_INTERFACE, gettype($subject)));
        }
    }

    /**
     * Verifies that the current strategy instance correctly defines its scope of encapsulation.
     * @throws \OutOfRangeException
     */
    private function verifyStrategyTargetInterface()
    {
        $interface = static::STRATEGY_TARGET_INTERFACE;
        $expected = 'Oroboros\\core\\interfaces\\BaseInterface';
        if (is_null($interface) || !($interface == $expected || is_subclass_of($interface, $expected))) {
            throw new \Oroboros\core\exception\core\InvalidClassException(sprintf('Strategy class [%1$s] is invalid. '
                        . 'It must be declare class constant [%2$s] as a valid interface extending [%3$s].', get_class($this), 'STRATEGY_TARGET_INTERFACE', $expected));
        }
    }
}
