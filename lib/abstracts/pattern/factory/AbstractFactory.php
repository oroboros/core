<?php
namespace Oroboros\core\abstracts\pattern\factory;

/**
 * Defines standardized abstraction for factorization of classes
 *
 * @author Brian Dayhoff
 */
abstract class AbstractFactory extends \Oroboros\core\abstracts\AbstractBase implements \Oroboros\core\interfaces\pattern\factory\FactoryInterface
{

    /**
     * Designates further child classes as design patterns.
     */
    const CLASS_TYPE = 'pattern';

    /**
     * Defines further child classes as factories.
     */
    const CLASS_SCOPE = 'factory';

    /**
     * Must be overridden by the extending class with the name of a valid interface that extends the base interface.
     */
    const FACTORY_TARGET_INTERFACE = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        $this->verifyFactoryTargetInterface();
        parent::__construct($command, $arguments, $flags);
    }

    /**
     * 
     * @param string $class The name of the class to generate an object instance of.
     * @param string $command (optional) Not required unless the factorized class requires this parameter in its constructor.
     * @param array $arguments (optional) Not required unless the factorized class requires this parameter in its constructor.
     * @param array $flags (optional) Not required unless the factorized class requires this parameter in its constructor.
     * @return \Oroboros\core\interfaces\BaseInterface The returned class will always be an instance of the base interface,
     *  and additionally will be an instance of the interface defined by the class constant `FACTORY_TARGET_INTERFACE`.
     */
    public function load(string $class, string $command = null, array $arguments = null, array $flags = null)
    {
        $this->verifyClass($class);
        $object = new $class($command, $arguments, $flags);
        return $object;
    }

    /**
     * Verifies that the given class name implements the expected interface for the current factory instance.
     * @param string $class
     * @throws \OutOfBoundsException
     */
    protected function verifyClass(string $class)
    {
        $expected = static::FACTORY_TARGET_INTERFACE;
        if (!in_array($expected, class_implements($class))) {
            throw new \OutOfBoundsException(sprintf('Error encountered in factory class [%1$s]. '
                        . 'Provided class [%2$s] must be a valid implementation of [%3$s].', get_class($this), $class, $expected));
        }
    }

    /**
     * Verifies that the current factory instance correctly defines its scope of factorization.
     * @throws \OutOfRangeException
     */
    private function verifyFactoryTargetInterface()
    {
        $interface = static::FACTORY_TARGET_INTERFACE;
        $expected = 'Oroboros\\core\\interfaces\\BaseInterface';
        if (is_null($interface) || !($interface == $expected || is_subclass_of($interface, $expected))) {
            throw new \Oroboros\core\exception\core\InvalidClassException(sprintf('Factory class [%1$s] is invalid. '
                        . 'It must be declare class constant [%2$s] as a valid interface extending [%3$s].', get_class($this), 'FACTORY_TARGET_INTERFACE', $expected));
        }
    }
}
