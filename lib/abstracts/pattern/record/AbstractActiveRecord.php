<?php
namespace Oroboros\core\abstracts\pattern\record;

/**
 * Defines base level abstraction for Active Record classes.
 * Simplifies direct interaction with the database.
 *
 * @author Brian Dayhoff
 */
abstract class AbstractActiveRecord extends \Oroboros\core\abstracts\AbstractBase implements \Oroboros\core\interfaces\pattern\record\ActiveRecordInterface
{

    use \Oroboros\core\traits\LoaderTrait;

    /**
     * Designates further child classes as design patterns.
     */
    const CLASS_TYPE = 'pattern';

    /**
     * Defines further child classes as records.
     */
    const CLASS_SCOPE = 'record';

    /**
     * The default connection to use if none is otherwise defined.
     */
    const DEFAULT_CONNECTION = 'local';

    /**
     * Must be defined by extension to scope the record class to a given database.
     * 
     */
    const DATABASE_NAME = null;

    /**
     * Must be defined by extension to scope the record class to a given table.
     */
    const TABLE_NAME = null;

    private static $result_set = [];

    /**
     * Active database connection
     * @var \Oroboros\core\library\connection\MySQL
     */
    private static $database = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        $this->verifyDatabaseName();
        $this->verifyTableName();
        $this->initializeResultSet();
        parent::__construct($command, $arguments, $flags);
    }

    /**
     * Performs a select statement, and returns an array of ActiveRecordRow objects associated with the result set.
     * @param array $where (optional) If defined, an array of where clauses
     * @param int $limit (optional) If defined, the return limit for selection
     * @param int $paginate (optional) If defined, the pagination limit for selection
     * @return array
     */
    public function get(array $where = null, int $limit = null, int $paginate = null)
    {
        
    }

    /**
     * Performs an update statement
     * @param array $where (optional) If defined, an array of where clauses
     * @param int $limit (optional) If defined, the maximum row count to update
     * @return $this (method chainable)
     */
    public function set(array $where = null, int $limit = null)
    {
        
    }

    /**
     * Performs an insert statement
     * @param array $values The values to insert. Should be an array of arrays,
     *  where each nested array is an associative set where the key is the column name,
     *  and the value is the insert value.
     * @return $this (method chainable)
     */
    public function add(array $values)
    {
        
    }

    /**
     * Performs a delete statement
     * @param array $where (optional) If defined, an array of where clauses
     * @param int $limit (optional) If defined, the max count of rows to delete
     * @return $this (method chainable)
     */
    public function delete(array $where = null, int $limit = null)
    {
        
    }

    /**
     * Returns the active database object
     * @param string $instance The key name of the database connection to return.
     *  If not provided, the default will be used.
     * @return \Oroboros\core\library\connection\MySQL
     */
    protected function database(string $instance = null)
    {
        if (is_null($instance)) {
            $instance = static::DEFAULT_CONNECTION;
        }
        $this->loadDatabaseConnection($instance);
        return self::$database;
    }

    /**
     * Loads the database connection instance if it is not already loaded.
     * All active record instances share this connection, so it doesn't
     * congest the available connections in the database.
     * @return void
     */
    private function loadDatabaseConnection()
    {
        if (is_null(self::$database)) {
            $credentials = $this->getConnectionCredentials();
            $db = new \Oroboros\core\library\connection\MySQL(null, $credentials);
            self::$database = $db;
        }
    }

    /**
     * Returns the database credentials for the currently associated key name.
     * @return array
     */
    private function getConnectionCredentials(string $instance = null)
    {
        if (is_null($instance)) {
            $instance = static::DEFAULT_CONNECTION;
        }
        $keyname = ($instance !== static::DEFAULT_CONNECTION && $instance !== $this->getArgument('connection')) ? $instance : $this->getConnectionCredentialKey();
        if (!$this->getArgument('connections')->offsetExists($keyname)) {
            throw new \OutOfBoundsException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided database credential key [%2$s] does not exist.', get_class($this), $keyname));
        }
        $credentials = $this->getArgument('connections')[$keyname];
        if (is_null())
            return $credentials;
    }

    /**
     * Returns the array key used to isolate the database connection to be used.
     * @return string
     */
    private function getConnectionCredentialKey()
    {
        $key = ($this->hasArgument('connection') ? $this->getArgument('connection') : static::DEFAULT_CONNECTION);
        return $key;
    }

    /**
     * Verifies that the extension class has properly defined their database scope.
     * @throws \OutOfRangeException
     */
    private function verifyDatabaseName()
    {
        if (is_null(static::DATABASE_NAME)) {
            throw new \OutOfRangeException(sprintf('Active Record class [%1$s] is invalid. '
                        . 'Class constant [%2$s] must be defined with a valid database name.', get_class($this), 'DATABASE_NAME'));
        }
    }

    /**
     * Verifies that the extension class has properly defined their table scope.
     * @throws \OutOfRangeException
     */
    private function verifyTableName()
    {
        if (is_null(static::TABLE_NAME)) {
            throw new \OutOfRangeException(sprintf('Active Record class [%1$s] is invalid. '
                        . 'Class constant [%2$s] must be defined with a valid table name.', get_class($this), 'TABLE_NAME'));
        }
    }

    /**
     * Create the result set array for this table if it does not already exist
     * @return void
     */
    private function initializeResultSet()
    {
        if (!(property_exists(self::$result_set, static::DATABASE_NAME) && property_exists(self::$result_set[static::DATABASE_NAME], static::TABLE_NAME) )) {
            self::$result_set[static::DATABASE_NAME][static::TABLE_NAME] = [];
        }
    }
}
