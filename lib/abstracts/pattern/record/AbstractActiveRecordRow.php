<?php
namespace Oroboros\core\abstracts\pattern\record;

/**
 * A data object associated with a specific row in a table.
 *
 * @author Brian Dayhoff
 */
abstract class AbstractActiveRecordRow extends \Oroboros\core\abstracts\AbstractBase implements \Oroboros\core\interfaces\pattern\record\ActiveRecordRowInterface
{

    /**
     * Designates further child classes as design patterns.
     */
    const CLASS_TYPE = 'pattern';

    /**
     * Defines further child classes as record rows.
     */
    const CLASS_SCOPE = 'record-row';

}
