<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\pattern\builder;

/**
 * Abstract Builder
 * Provides baseline abstraction for builders
 *
 * @author Brian Dayhoff
 */
abstract class AbstractBuilder extends \Oroboros\core\abstracts\pattern\director\AbstractWorker implements \Oroboros\core\interfaces\pattern\builder\BuilderInterface
{

    use \Oroboros\core\traits\ContainerPackagerUtility;
    use \Oroboros\core\traits\LoaderTrait;
    use \Oroboros\core\traits\model\ModelAwareUtilityTrait;
    use \Oroboros\core\traits\StringUtilityTrait;

    /**
     * Designates further child classes as builders.
     */
    const CLASS_TYPE = 'builder';

    /**
     * Override the class scope in a child class to define the scope of the builder
     */
    const CLASS_SCOPE = null;

    /**
     * If the director that should be associated with the builder has
     * a defined worker scope, the builder child class must define
     * the same scope as the value of this class constant.
     * 
     * This is not required by default, but may be enforced by the director class
     * (using the same class constant name on that class).
     * 
     * A director with a WORKER_SCOPE value of null does not enforce this constraint.
     */
    const WORKER_SCOPE = null;

    /**
     * Contains the values passed to the build process
     * @var Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $properties = null;

    /**
     * Contains the categories of values that can be passed
     * to the builder through the setter.
     * 
     * Each key of this set will correspond to a container of the values for the given category key.
     * 
     * The entire set may be recursively cast to array, json, or serialized for caching purposes if needed.
     * 
     * @var Oroboros\core\interfaces\library\container\CollectionInterface
     */
    private $categories = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->reset();
    }

    /**
     * Returns a list of categories of the builder
     * 
     * @return array
     */
    public function categories(): array
    {
        return array_keys($this->categories->toArray());
    }

    /**
     * Returns a boolean designation as to whether the builder has the specified
     * category
     * 
     * @param string $category
     * @return bool
     */
    public function hasCategory(string $category): bool
    {
        return $this->categories->has($category);
    }

    /**
     * Returns the keys registered within a given category
     * 
     * @param string $category
     * @return type
     * @throws \Oroboros\core\exception\InvalidArgumentException
     *         If the given category does not exist
     */
    public function keys(string $category)
    {
        if (!$this->hasCategory($category)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Provided category '
                        . '[%2$s] does not exist.', get_class($this), $category)
            );
        }
        return array_keys($this->categories[$category]->toArray());
    }

    /**
     * Sets a value in the builder for the given category and key
     * @param string $category
     * @param string $key
     * @param type $value
     * @return $this (method chainable)
     * @throws \InvalidArgumentException If the given category does not exist,
     *     the given key is not valid for the given category,
     *     or the provided value is not a valid type for the given category and key.
     */
    public function set(string $category, string $key, $value): \Oroboros\core\interfaces\pattern\builder\BuilderInterface
    {
        $this->validateKey($category, $key, $value);
        $this->properties[$category][$key] = $value;
        return $this;
    }

    /**
     * Removes a value from the specified category key
     * @param string $category
     * @param string $key
     * @return $this (method chainable)
     * @throws \InvalidArgumentException If the supplied category does not exist
     */
    public function remove(string $category, string $key): \Oroboros\core\interfaces\pattern\builder\BuilderInterface
    {
        if (!$this->validateCategory($category)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Given category [%2$s] does not exist.', get_class($this), $category));
        }
        if ($this->properties->has($category)) {
            if ($this->properties->get($category)->has($key)) {
                unset($this->properties[$category][$key]);
            }
        }
        return $this;
    }

    /**
     * Returns either the entire specified category,
     * or the given key of the specified category if
     * an optional key name is supplied.
     * 
     * @param string $category
     * @param string $key
     * @return mixed
     * @throws \InvalidArgumentException if the given category does not exist,
     *     or if a key is supplied and it does not exist in the given category.
     */
    public function get(string $category, string $key = null)
    {
        if (!$this->validateCategory($category)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Given category [%2$s] does not exist.', get_class($this), $category));
        }
        $cat = $this->properties->get($category);
        if (is_null($key)) {
            return $cat;
        }
        if (!$cat->has($key)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Given key [%2$s] does not exist in category [%3$s].', get_class($this), $key, $category));
        }
        return $cat->get($key);
    }

    /**
     * Returns whether or not a given category exists,
     * or if a key exists in a given category if the optional keyname is also supplied.
     * This method must not throw an exception.
     * 
     * @param string $category
     * @param string $key (optional)
     * @return bool
     */
    public function has(string $category, string $key = null): bool
    {
        if (!$this->categories->has($category)) {
            return false;
        }
        if (is_null($key)) {
            return true;
        }
        $cat = $this->properties->get($category);
        return ($cat->has($key));
    }

    /**
     * Runs the underlying executeTask method with the collection of
     * category keys as its arguments, and any flags manually forwarded from the director.
     * @return mixed
     */
    public function build(array $flags = [])
    {
        return $this->executeTask($this->properties->toArray(), $flags);
    }

    /**
     * Resets the builder to its default state.
     * This can be used to recycle a builder for multiple related purposes
     * with the same output types but different values.
     * @return $this (method chainable)
     */
    public function reset(): \Oroboros\core\interfaces\pattern\builder\BuilderInterface
    {
        if (!is_null($this->categories)) {
            // Only perform this action if already initialized,
            // otherwise a null pointer exception will occur.
            foreach ($this->getCategories() as $category => $details) {
                $this->removeCategory($category);
            }
        }
        $this->initializeCategories();
        $this->initializeProperties();
        $this->initializeDefaults();
        return $this;
    }

    /**
     * Returns the categories, with their validation rules
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    public function getCategories(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        return $this->categories;
    }

    /**
     * Returns whether or not a given value is valid for the given category and key.
     * Will always return false if the category does not exist,
     * or if the key is not valid for the given category.
     * 
     * This method should be used to check if a proposed value will throw a validation exception for the given category
     * 
     * Will return true if the given value matches the validation rules for the given category and key
     * 
     * @param string $category
     * @param string $key
     * @param type $value
     * @return bool
     */
    public function isValid(string $category, string $key, $value): bool
    {
        try {
            return $this->validateKey($category, $key, $value);
        } catch (\InvalidArgumentException $e) {
            // Category does not exist
            return false;
        }
    }

    /**
     * Override this method in a child class to declare
     * valid properties that can be passed to the builder setter.
     * 
     * This should be an associative array where the key is the category,
     * and the value is the variable type, class, or interface that is
     * acceptable as an argument.
     * 
     * If a category can take multiple value types, the value returned
     * should be an array of the acceptable types, classes, or interfaces.
     * If any value is acceptable, pass "mixed".
     * @return array
     */
    abstract protected function declareCategories(): array;

    /**
     * Override this method in a child class to declare
     * default properties to be used if none are supplied for the given categories.
     * 
     * This should be an associative array, matching categories and keys
     * expected from the `declareCategories` method.
     * 
     * Defaults do not need to be set for all categories,
     * but defaults cannot declare categories that are
     * not declared within `declareCategories`.
     * 
     * @return array
     */
    protected function declareDefaults(): array
    {
        return [];
    }

    /**
     * Adds a new category if it does not already exist.
     * This method is accessible to child classes, but remains immutable.
     * @param string $category
     * @param type $valid (optional) May pass 
     *     a variable type [string, bool, int, float, array, object, null],
     *     class or interface,
     *     or explicit value.
     *     Passing "mixed" means any value is acceptable (default)
     * @return bool Returns true if a new category was created, false if it already existed
     * @final
     */
    final protected function addCategory(string $category, $valid = 'mixed'): bool
    {
        $generated = false;
        // Do not regenerate if it already exists or it will wipe existing values.
        // removal should be explicitly called if that is desired.
        if (!$this->categories->has($category)) {
            $this->categories[$category] = $valid;
            $this->properties[$category] = $this::containerize();
            $generated = true;
        }
        return $generated;
    }

    /**
     * Removes a category.
     * This method is accessible to child classes, but remains immutable.
     * @param string $category
     * @return bool If the category was removed, returns true. If it did not exist, returns false.
     * @final
     */
    final protected function removeCategory(string $category): bool
    {
        $deleted = false;
        if ($this->categories->has($category)) {
            unset($this->categories[$category]);
            unset($this->properties[$category]);
            $deleted = true;
        }
        return $deleted;
    }

    private function validateCategory(string $category)
    {
        return $this->categories->has($category);
    }

    private function validateKey(string $category, string $key, $value)
    {
        $is_valid = false;
        if (!$this->validateCategory($category)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Could not create key [%2$s] because given category [%3$s] does not exist.', get_class($this), $key, $category));
        }
        $valid = $this->categories->get($category);
        if (is_array($valid)) {
            // Validate where multiple values are acceptable
            foreach ($valid as $rule) {
                if ($this->validateExpected($value, $rule)) {
                    $is_valid = true;
                    break;
                }
            }
        } else {
            // Validate single expected type/class/interface/value
            $is_valid = $this->validateExpected($value, $valid);
        }
        if (!$is_valid) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided category [%2$s] with key [%3$s] is not valid.'
                        . 'Valid keys are [%4$s]', get_class($this), $category, $key, implode(', ', array_keys($this->categories->toArray()))));
        }
    }

    private function validateExpected($given, $expected)
    {
        if (is_string($expected) && $expected === 'mixed') {
            // Any value is sufficient.
            return true;
        } elseif (is_string($expected) && $expected === strtolower(gettype($given))) {
            // Typecheck validation passed.
            return true;
        } elseif (is_string($expected) && class_exists($expected) || interface_exists($expected)) {
            return $this->validateExpectedClass($given, $expected);
        }
        return false;
    }

    private function validateExpectedClass($given, string $expected)
    {
        $classname = $given;
        if (!is_object($given) && !is_string($given)) {
            // Given is not a class
            return false;
        }
        if (is_object($given)) {
            // Extract the class name from the object
            $classname = get_class($given);
        }
        if (!class_exists($classname)) {
            // Given is not a class
            return false;
        }
        if (trim($expected, '\\') === trim($classname, '\\')) {
            // Given class equals expected
            return true;
        }
        if (in_array(trim($expected, '\\'), class_implements($classname))) {
            // Given class implements expected
            return true;
        }
        // Given class is not valid
        return false;
    }

    private function initializeCategories(): void
    {
        $categories = $this->declareCategories();
        $this->categories = $this::containerize($categories);
    }

    private function initializeProperties(): void
    {
        $this->properties = $this->containerizeInto('Oroboros\\core\\library\\container\\Collection', []);
        $category_keys = array_keys($this->categories->toArray());
        foreach ($category_keys as $key) {
            $this->properties[$key] = $this::containerize();
        }
    }

    private function initializeDefaults(): void
    {
        $defaults = $this->declareDefaults();
        if (empty($defaults)) {
            // Do nothing if no defaults are supplied.
            return;
        }
        foreach ($defaults as $category => $details) {
            foreach ($details as $key => $value) {
                $this->set($category, $key, $value);
            }
        }
    }
}
