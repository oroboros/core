<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\pattern\command;

/**
 * Description of AbstractCommand
 *
 * @author Brian Dayhoff
 */
abstract class AbstractCommand extends \Oroboros\core\abstracts\AbstractBase implements \Oroboros\core\interfaces\pattern\command\CommandInterface
{

    const CLASS_TYPE = 'pattern';
    const CLASS_SCOPE = 'command';

    private $command = null;
    private $subject = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->verifyDeclaredInterface();
        $this->initializeCommand();
    }

    /**
     * Returns the command string, or an empty string if no command was passed.
     * @return string
     */
    public function __toString(): string
    {
        $command = $this->getCommand();
        if (is_null($command)) {
            $command = '';
        }
        return $command;
    }

    /**
     * Returns the command string passed when the object was instantiated,
     * or null if no command was passed.
     * @return string|null
     */
    public function getCommand(): ?string
    {
        return parent::getCommand();
    }

    /**
     * Runs the command operation on the subject.
     * The return value will be whatever the final outcome from the subject is.
     * @return mixed
     * @throws \BadMethodCallException If no subject is currently defined.
     * @throws \ErrorException If the subject responds with any exception during execution,
     *     It will be recast to an ErrorException,
     *     and will retain the original message and code.
     */
    public function execute()
    {
        if (is_null($this->subject)) {
            throw new \BadMethodCallException(sprintf('Error encountered in [%1$s]. No command subject is defined.', get_class($this)));
        }
        try {
            return $this->runCommand();
        } catch (\Exception $e) {
            throw new \Oroboros\core\exception\ErrorException($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Sets the subject that the command object operates upon.
     * @param \Oroboros\core\interfaces\pattern\command\CommandSubjectInterface $subject
     * @return $this (method chainable)
     */
    public function setSubject(\Oroboros\core\interfaces\pattern\command\CommandSubjectInterface $subject): \Oroboros\core\interfaces\pattern\command\CommandInterface
    {
        $expected = $this->declareSubjectType();
        if (!in_array($expected, class_implements($subject))) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided subject [%2$s] does not implement '
                        . 'expected interface [%3$s], '
                        . 'and is not compatible with this command.'
                        , get_class($this), get_class($subject), $expected));
        }
        $this->subject = $subject;
        return $this;
    }

    /**
     * Releases the subject the command object operates upon.
     * @return $this (method chainable)
     */
    public function releaseSubject(): \Oroboros\core\interfaces\pattern\command\CommandInterface
    {
        if (!is_null($this->subject)) {
            $this->subject = null;
        }
        return $this;
    }

    /**
     * Returns the subject that the command object operates upon,
     * or null if the subject is not currently defined.
     * @return \Oroboros\core\interfaces\pattern\command\CommandSubjectInterface|null
     */
    public function getSubject(): ?\Oroboros\core\interfaces\pattern\command\CommandSubjectInterface
    {
        return $this->subject;
    }

    /**
     * Returns the interface name of the expected subject.
     * Provided subjects must implement the interface declared
     * within this method in addition to the command subject interface.
     * @return string
     */
    public function declareSubjectType(): string
    {
        return 'Oroboros\\core\\interfaces\\pattern\\command\\CommandSubjectInterface';
    }

    /**
     * This method must be overridden to provide
     * the means of executing the given command.
     * @return mixed
     */
    abstract protected function runCommand();

    /**
     * Performs the initial command setup on instantiation.
     * @return void
     */
    private function initializeCommand(): void
    {
        if ($this->hasArgument('subject')) {
            $this->setSubject($this->getArgument('subject'));
        }
    }

    private function verifyDeclaredInterface(): void
    {
        if (!interface_exists($this->declareSubjectType())) {
            throw new \Oroboros\core\exception\core\InvalidClassException(sprintf('Class [%1$s] is misconfigured. '
                        . 'Command class method [%2$s] must return '
                        . 'a valid interface name.', get_class($this), 'declareSubjectType'));
        }
    }
}
