<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\pattern\director;

/**
 * Abstract Worker Pattern
 * Provides underlying abstraction for worker pattern objects
 *
 * @author Brian Dayhoff
 */
abstract class AbstractWorker extends \Oroboros\core\abstracts\AbstractBase implements \Oroboros\core\interfaces\pattern\director\WorkerInterface
{

    use \Oroboros\core\traits\pattern\WorkerTrait;

    /**
     * Designates further child classes as workers.
     */
    const CLASS_TYPE = 'worker';

    /**
     * Class scope must be defined through extension.
     */
    const CLASS_SCOPE = null;

    /**
     * The scope required of workers. Worker classes must define a WORKER_SCOPE,
     * which is used by directors to filter for valid workers in scope for their
     * subset of directives.
     */
    const WORKER_SCOPE = null;

    /**
     * The task accomplished by the worker. Worker classes must define a WORKER_TASK,
     * which is a keyword used by the director to isolate tasks appropriate for a given worker.
     */
    const WORKER_TASK = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeWorker();
    }
}
