<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\pattern\director;

/**
 * Abstract Director
 * Provides underlying abstraction for director pattern objects.
 * 
 * This class is a wrapper for the DirectorTrait,
 * and correctly implements the required interface
 * and defines the required class constants
 * (as those cannot be accomplished in a trait).
 * 
 * All of the actual logic to operate as a Director is encapsulated
 * in the DirectorTrait, which may be implemented on any class
 * by using that trait, implementing \Oroboros\core\interfaces\pattern\director\DirectorInterface,
 * and defining the class constants[CLASS_SCOPE].
 * 
 * Valid workers may additionally be constrained by overriding
 * class constant [WORKER_SCOPE] with a keyword designating their grouping,
 * or by overriding class constant [EXPECTED_WORKER_INTERFACE] with the name
 * of an interface extending \Oroboros\core\interfaces\pattern\director\DirectorInterface
 * (or both).
 *
 * @author Brian Dayhoff
 */
abstract class AbstractDirector extends \Oroboros\core\abstracts\AbstractBase implements \Oroboros\core\interfaces\pattern\director\DirectorInterface
{

    use \Oroboros\core\traits\pattern\DirectorTrait;

    /**
     * Designates further child classes as directors.
     */
    const CLASS_TYPE = 'director';

    /**
     * Class scope must be defined through extension.
     */
    const CLASS_SCOPE = null;

    /**
     * The scope required of workers. If supplied, workers that do not also
     * define their own WORKER_SCOPE class constant with an equal value will
     * be rejected.
     */
    const WORKER_SCOPE = null;

    /**
     * The interface expected to be honored by workers packaged into the director.
     * At the base abstract level, it only requires the base level worker interface.
     * 
     * Further extensions of this class may define more granular interfaces, but they
     * must be extended implementations of this base interface.
     */
    const EXPECTED_WORKER_INTERFACE = 'Oroboros\\core\\interfaces\\pattern\\director\\WorkerInterface';

    private $workers = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeDirector();
    }
}
