<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\model;

/**
 * Abstract Model Strategy
 * Provides abstraction for selecting the
 * correct model type for the current environment.
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
abstract class AbstractModelStrategy extends \Oroboros\core\abstracts\pattern\strategy\AbstractStrategy implements \Oroboros\core\interfaces\model\ModelStrategyInterface
{

    use \Oroboros\core\traits\LoaderTrait;
    use \Oroboros\core\traits\ContainerPackagerUtility;

    /**
     * Use the default container
     */
    const CONTAINER_CLASS = \Oroboros\core\library\container\Container::class;

    /**
     * Use the default collection
     */
    const COLLECTION_CLASS = \Oroboros\core\library\container\Collection::class;

    /**
     * Defines the target interface of the strategy to models
     */
    const STRATEGY_TARGET_INTERFACE = \Oroboros\core\interfaces\model\ModelInterface::class;

    /**
     * An immutable list of core-specific models
     * 
     * @var array
     */
    private static $core_models = [
        \Oroboros\core\model\file\config\json\core\CoreAppDefinitionModel::MODEL_SCOPE => \Oroboros\core\model\file\config\json\core\CoreAppDefinitionModel::class,
        \Oroboros\core\model\file\config\json\core\CoreComponentModel::MODEL_SCOPE => \Oroboros\core\model\file\config\json\core\CoreComponentModel::class,
        \Oroboros\core\model\file\config\json\core\CoreConnectionModel::MODEL_SCOPE => \Oroboros\core\model\file\config\json\core\CoreConnectionModel::class,
        \Oroboros\core\model\file\config\json\core\CoreConsoleModel::MODEL_SCOPE => \Oroboros\core\model\file\config\json\core\CoreConsoleModel::class,
        \Oroboros\core\model\file\config\json\core\CoreEnvironmentModel::MODEL_SCOPE => \Oroboros\core\model\file\config\json\core\CoreEnvironmentModel::class,
        \Oroboros\core\model\file\config\json\core\CoreEventModel::MODEL_SCOPE => \Oroboros\core\model\file\config\json\core\CoreEventModel::class,
        \Oroboros\core\model\file\config\json\core\CoreExtensionModel::MODEL_SCOPE => \Oroboros\core\model\file\config\json\core\CoreExtensionModel::class,
        \Oroboros\core\model\file\config\json\core\CoreFilterModel::MODEL_SCOPE => \Oroboros\core\model\file\config\json\core\CoreFilterModel::class,
        \Oroboros\core\model\file\config\json\core\CoreFontModel::MODEL_SCOPE => \Oroboros\core\model\file\config\json\core\CoreFontModel::class,
        \Oroboros\core\model\file\config\json\core\CoreGruntModel::MODEL_SCOPE => \Oroboros\core\model\file\config\json\core\CoreGruntModel::class,
        \Oroboros\core\model\file\config\json\core\CoreImageModel::MODEL_SCOPE => \Oroboros\core\model\file\config\json\core\CoreImageModel::class,
        \Oroboros\core\model\file\config\json\core\CoreJobActionModel::MODEL_SCOPE => \Oroboros\core\model\file\config\json\core\CoreJobActionModel::class,
        \Oroboros\core\model\file\config\json\core\CoreJobModel::MODEL_SCOPE => \Oroboros\core\model\file\config\json\core\CoreJobModel::class,
        \Oroboros\core\model\file\config\json\core\CoreLayoutModel::MODEL_SCOPE => \Oroboros\core\model\file\config\json\core\CoreLayoutModel::class,
        \Oroboros\core\model\file\config\json\core\CoreLogModel::MODEL_SCOPE => \Oroboros\core\model\file\config\json\core\CoreLogModel::class,
        \Oroboros\core\model\file\config\json\core\CoreModuleModel::MODEL_SCOPE => \Oroboros\core\model\file\config\json\core\CoreModuleModel::class,
        \Oroboros\core\model\file\config\json\core\CoreRouteModel::MODEL_SCOPE => \Oroboros\core\model\file\config\json\core\CoreRouteModel::class,
        \Oroboros\core\model\file\config\json\core\CoreSchemaModel::MODEL_SCOPE => \Oroboros\core\model\file\config\json\core\CoreSchemaModel::class,
        \Oroboros\core\model\file\config\json\core\CoreScriptModel::MODEL_SCOPE => \Oroboros\core\model\file\config\json\core\CoreScriptModel::class,
        \Oroboros\core\model\file\config\json\core\CoreServiceModel::MODEL_SCOPE => \Oroboros\core\model\file\config\json\core\CoreServiceModel::class,
        \Oroboros\core\model\file\config\json\core\CoreSocketModel::MODEL_SCOPE => \Oroboros\core\model\file\config\json\core\CoreSocketModel::class,
        \Oroboros\core\model\file\config\json\core\CoreStylesheetModel::MODEL_SCOPE => \Oroboros\core\model\file\config\json\core\CoreStylesheetModel::class,
        \Oroboros\core\model\file\config\json\core\CoreWorkerModel::MODEL_SCOPE => \Oroboros\core\model\file\config\json\core\CoreWorkerModel::class,
        \Oroboros\core\model\definer\permission\PermissionModel::MODEL_SCOPE => \Oroboros\core\model\definer\permission\PermissionModel::class,
        \Oroboros\core\model\definer\permission\PermissionGroupModel::MODEL_SCOPE => \Oroboros\core\model\definer\permission\PermissionGroupModel::class,
        \Oroboros\core\model\definer\user\UserModel::MODEL_SCOPE => \Oroboros\core\model\definer\user\UserModel::class,
        \Oroboros\core\model\definer\user\UserGroupModel::MODEL_SCOPE => \Oroboros\core\model\definer\user\UserGroupModel::class,
        \Oroboros\core\model\definer\organization\OrganizationModel::MODEL_SCOPE => \Oroboros\core\model\definer\organization\OrganizationModel::class,
    ];

    /**
     * An immutable list of default models
     * 
     * @var array
     */
    private static $default_models = [
        \Oroboros\core\model\file\config\json\AppDefinitionModel::MODEL_SCOPE => \Oroboros\core\model\file\config\json\AppDefinitionModel::class,
        \Oroboros\core\model\file\config\json\ComponentModel::MODEL_SCOPE => \Oroboros\core\model\file\config\json\ComponentModel::class,
        \Oroboros\core\model\file\config\json\ConnectionModel::MODEL_SCOPE => \Oroboros\core\model\file\config\json\ConnectionModel::class,
        \Oroboros\core\model\file\config\json\ConsoleModel::MODEL_SCOPE => \Oroboros\core\model\file\config\json\ConsoleModel::class,
        \Oroboros\core\model\file\config\json\EnvironmentModel::MODEL_SCOPE => \Oroboros\core\model\file\config\json\EnvironmentModel::class,
        \Oroboros\core\model\file\config\json\EventModel::MODEL_SCOPE => \Oroboros\core\model\file\config\json\EventModel::class,
        \Oroboros\core\model\file\config\json\ExtensionModel::MODEL_SCOPE => \Oroboros\core\model\file\config\json\ExtensionModel::class,
        \Oroboros\core\model\file\config\json\FilterModel::MODEL_SCOPE => \Oroboros\core\model\file\config\json\FilterModel::class,
        \Oroboros\core\model\file\config\json\FontModel::MODEL_SCOPE => \Oroboros\core\model\file\config\json\FontModel::class,
        \Oroboros\core\model\file\config\json\GruntModel::MODEL_SCOPE => \Oroboros\core\model\file\config\json\GruntModel::class,
        \Oroboros\core\model\file\config\json\ImageModel::MODEL_SCOPE => \Oroboros\core\model\file\config\json\ImageModel::class,
        \Oroboros\core\model\file\config\json\JobActionModel::MODEL_SCOPE => \Oroboros\core\model\file\config\json\JobActionModel::class,
        \Oroboros\core\model\file\config\json\JobModel::MODEL_SCOPE => \Oroboros\core\model\file\config\json\JobModel::class,
        \Oroboros\core\model\file\config\json\LayoutModel::MODEL_SCOPE => \Oroboros\core\model\file\config\json\LayoutModel::class,
        \Oroboros\core\model\file\config\json\LogModel::MODEL_SCOPE => \Oroboros\core\model\file\config\json\LogModel::class,
        \Oroboros\core\model\file\config\json\ModuleModel::MODEL_SCOPE => \Oroboros\core\model\file\config\json\ModuleModel::class,
        \Oroboros\core\model\file\config\json\RouteModel::MODEL_SCOPE => \Oroboros\core\model\file\config\json\RouteModel::class,
        \Oroboros\core\model\file\config\json\SchemaModel::MODEL_SCOPE => \Oroboros\core\model\file\config\json\SchemaModel::class,
        \Oroboros\core\model\file\config\json\ScriptModel::MODEL_SCOPE => \Oroboros\core\model\file\config\json\ScriptModel::class,
        \Oroboros\core\model\file\config\json\ServiceModel::MODEL_SCOPE => \Oroboros\core\model\file\config\json\ServiceModel::class,
        \Oroboros\core\model\file\config\json\SocketModel::MODEL_SCOPE => \Oroboros\core\model\file\config\json\SocketModel::class,
        \Oroboros\core\model\file\config\json\StylesheetModel::MODEL_SCOPE => \Oroboros\core\model\file\config\json\StylesheetModel::class,
        \Oroboros\core\model\file\config\json\WorkerModel::MODEL_SCOPE => \Oroboros\core\model\file\config\json\WorkerModel::class,
        \Oroboros\core\model\definer\permission\PermissionModel::MODEL_SCOPE => \Oroboros\core\model\definer\permission\PermissionModel::class,
        \Oroboros\core\model\definer\permission\PermissionGroupModel::MODEL_SCOPE => \Oroboros\core\model\definer\permission\PermissionGroupModel::class,
        \Oroboros\core\model\definer\user\UserModel::MODEL_SCOPE => \Oroboros\core\model\definer\user\UserModel::class,
        \Oroboros\core\model\definer\user\UserGroupModel::MODEL_SCOPE => \Oroboros\core\model\definer\user\UserGroupModel::class,
        \Oroboros\core\model\definer\organization\OrganizationModel::MODEL_SCOPE => \Oroboros\core\model\definer\organization\OrganizationModel::class,
    ];

    /**
     * An updatable list of known models
     * 
     * @var array
     */
    private static $model_index = [];

    /**
     * Prevents double bootstrapping
     * 
     * @var bool
     */
    private static $is_bootstrapped = false;

    /**
     * Prevents registration if additional models
     * This will be performed by the bootstrap after allowing all pluggables to
     * register their models and checking the ModelModel for any additional app
     * models that are not registered.
     * 
     * @var bool
     */
    private static $is_locked = false;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this::bootstrap();
    }

    /**
     * Loads a model corresponding to the given key and returns it
     * 
     * @param string $key The key identifier for the model
     * @param string $command The command to pass to the model
     * @param array $arguments Any arguments required by the model
     * @param array $flags Any flags to pass to the model
     * @return \Oroboros\core\interfaces\model\ModelInterface
     * @throws \Oroboros\core\exception\InvalidArgumentException
     *         If no model is known for the corresponding key
     * @throws \Oroboros\core\exception\core\InvalidClassException
     *         If a model is broken, or the strategy loading it is broken
     * @throws \Oroboros\core\exception\ErrorException
     *         If a model cannot load with the supplied arguments
     */
    public function get(string $key, string $command = null, array $arguments = null, array $flags = null): \Oroboros\core\interfaces\model\ModelInterface
    {
        if (!$this->has($key)) {
            d($key, $command, $arguments, $flags, debug_backtrace(1)); exit;
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. '
                        . 'No model exists for given key [%2$s].'
                        , get_class($this), $key)
            );
        }
        try {
            $model = $this->loadModel($key, $command, $arguments, $flags);
            return $model;
        } catch (\Oroboros\core\exception\core\InvalidClassException $e) {
            // Do not catch these
            throw $e;
        } catch (\Oroboros\core\exception\ErrorException $e) {
            // Problem loading model
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. Unable to load model '
                        . 'with key [%2$s] because instantiation raised an '
                        . 'exception of type [%3$s] with message [%4$s].'
                        , get_class($this), $key, get_class($e), $e->getMessage())
                    , 0, E_ERROR, __FILE__, __LINE__, $e);
        } catch (\Exception $e) {
            // Unhandled error 
            // This indicates a child class is not correctly handling
            // its instantiation errors. This class is unsafe in this status.
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. Failed to mitigate '
                        . 'an exception during model loading for key '
                        . '[%2$s] because instantiation raised an '
                        . 'unhandled exception of type [%3$s] with message [%4$s]. '
                        . 'This class is not useable in it\'s current state.'
                        , get_class($this), $key, get_class($e), $e->getMessage()));
        }
    }

    /**
     * Returns a boolean designation as to whether
     * the strategy has a model for a given key.
     * 
     * @param string $key
     * @return bool
     */
    public function has(string $key): bool
    {
        return array_key_exists($key, self::$model_index);
    }

    /**
     * Returns a list of key identifiers of valid registered models
     * 
     * @return array
     */
    public function keys(): array
    {
        return array_keys(self::$model_index);
    }

    /**
     * Returns an associative array of all registered models where the key is
     * the identifier and the value is the fully qualified class name of the
     * model corresponding to that key.
     * 
     * @return array
     */
    public function index(): array
    {
        $results = [];
        foreach ($this->keys() as $key) {
            $results[$key] = $this->type($key);
        }
        return $results;
    }

    /**
     * Returns the literal class name of a given model key.
     * If it was registered as a stub class, the fully qualified
     * concrete class will be returned.
     * 
     * @param string $key
     * @return string
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    public function type(string $key): string
    {
        if (!$this->has($key)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. '
                        . 'No model exists for given key [%2$s].'
                        , get_class($this), $key)
            );
        }
        $class = self::$model_index[$key];
        if (!class_exists($class)) {
            $class = $this->getFullClassName('model', $class);
        }
        return $class;
    }

    /**
     * Sets up the initial definition of models if not already done.
     * All other public methods as well as the constructor call this
     * to insure that setup is done correctly prior to operation.
     * 
     * Bootstrap process must not repeat once it has been done once.
     * 
     * @return void
     */
    public static function bootstrap(): void
    {
        if (!self::$is_bootstrapped) {
            self::initializeDefaultModelDefinitions();
            self::$is_bootstrapped = true;
        }
    }

    /**
     * Locks the strategy so no additional models can be registered.
     * Once locked, the strategy cannot be unlocked.
     * This will be called typically by the bootstrap class after
     * it finishes initialization and has fired it's events to allow
     * pluggables and the application to register their own models.
     * 
     * This will carry over through all other child classes.
     * 
     * This prevents expected model definitions from being
     * altered during runtime and causing unexpected results
     * or data pollution.
     * 
     * @return void
     */
    final public static function lock(): void
    {
        self::$is_locked = true;
    }

    /**
     * Returns a boolean designation as to whether the strategy is locked.
     * If this returns `true`, any further attempts to `register` a model
     * will throw an error exception.
     * 
     * @return bool
     */
    final public static function isLocked(): bool
    {
        return self::$is_locked;
    }

    /**
     * Registers a model with the strategy.
     * Existing keys can be overridden.
     * 
     * @param string $key string identifier for the model
     * @param string $model classname or stub classname of a model class
     * @return void
     * @throws \Oroboros\core\exception\ErrorException
     *         If the strategy is locked. If you are encountering this exception,
     *         you need to register your models within the bootstrap initialize event.
     *         The strategy will be locked after initialization is completed.
     * @throws \Oroboros\core\exception\InvalidArgumentException
     *         If the given model name does not resolve to a class implementing
     *         `Oroboros\core\interfaces\model\ModelInterface`
     */
    final public static function register(string $key, string $model): void
    {
        self::bootstrap();
        if ($this->isLocked()) {
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. Strategy is locked '
                        . 'and cannot receive additional model registrations.'
                        , static::class)
            );
        }
        $expected = self::STRATEGY_TARGET_INTERFACE;
        $proxy = new \Oroboros\core\pattern\factory\LoaderProxy();
        $resolver = $proxy->load('library', 'resolver\\ArgumentResolver');
        if (!$resolver->resolve($model, $expected)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Provided model with key '
                        . '[%2$s] and definition [%3$s] does not resolve to a class '
                        . 'implementing expected interface [%4$s]. '
                        . 'This model cannot be registered.', static::class)
            );
        }
        self::$model_index[$key] = $model;
    }

    /**
     * Loads the requested model
     * 
     * @param string $key
     * @param string|null $command
     * @param array|null $arguments
     * @param array|null $flags
     * @return \Oroboros\core\interfaces\model\ModelInterface
     * @throws \Oroboros\core\exception\core\InvalidClassException
     *         If these are encountered they are bubbled up unhandled
     * @throws \Oroboros\core\exception\ErrorException
     *         If classloading fails for any reason.
     */
    protected function loadModel(string $key, string $command = null, array $arguments = null, array $flags = null): \Oroboros\core\interfaces\model\ModelInterface
    {
        try {
            $class = $this->type($key);
            $model = $this->load('model', $class, $command, $arguments, $flags);
            return $model;
        } catch (\Oroboros\core\exception\core\InvalidClassException $e) {
            // Do not catch these
            throw $e;
        } catch (\Exception $e) {
            // cast to error exception
            throw new \Oroboros\core\exception\ErrorException(
                    $e->getMessage(),
                    $e->getCode(),
                    E_ERROR,
                    $e->getFile(),
                    $e->getLine(),
                    $e
            );
        }
    }

    /**
     * Imports the initial model definitions one time only per runtime.
     * 
     * @return void
     */
    private static function initializeDefaultModelDefinitions(): void
    {
        if (empty(self::$model_index)) {
            if (self::app()->getApplicationRoot() === OROBOROS_CORE_BASE_DIRECTORY) {
                // this is a core app
                self::$model_index = self::$core_models;
            } else {
                // this is a normal app
                self::$model_index = self::$default_models;
            }
        }
    }
}
