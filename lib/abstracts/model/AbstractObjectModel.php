<?php
namespace Oroboros\core\abstracts\model;

/**
 * Abstract Service Model
 * 
 * Sets the abstract foundation for models
 * associated with a remote service api.
 * 
 * This model allows api calls to be represented as models.
 *
 * @author Brian Dayhoff
 */
abstract class AbstractObjectModel extends AbstractModel implements \Oroboros\core\interfaces\model\ObjectModelInterface
{

    /**
     * Defines child classes as object models.
     */
    const CLASS_SCOPE = 'object-model';

}
