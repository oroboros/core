<?php
namespace Oroboros\core\abstracts\model;

/**
 * Abstract Service Model
 * 
 * Sets the abstract foundation for models
 * associated with a remote service api.
 * 
 * This model allows api calls to be represented as models.
 *
 * @author Brian Dayhoff
 */
abstract class AbstractServiceModel extends AbstractModel implements \Oroboros\core\interfaces\model\ServiceModelInterface
{

    /**
     * Defines child classes as service models.
     */
    const CLASS_SCOPE = 'service-model';

}
