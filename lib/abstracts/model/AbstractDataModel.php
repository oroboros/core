<?php
namespace Oroboros\core\abstracts\model;

/**
 * Abstract Data Model
 * 
 * Sets the abstract foundation for models
 * directly associated with a data layer.
 * 
 * This is the most common type of model implementation.
 *
 * @author Brian Dayhoff
 */
class AbstractDataModel extends AbstractModel implements \Oroboros\core\interfaces\model\DataModelInterface
{

    /**
     * Defines child classes as data models.
     */
    const CLASS_SCOPE = 'data-model';

    /**
     * Defines the database type associated with the model
     */
    const DATA_LAYER = 'mysql';

    /**
     * Represents the database schema bound to the data model.
     * This constant must be overridden in child classes.
     */
    const SCHEMA_BINDING = null;

    /**
     * Represents the database table the model is bound to.
     * This constant must be overridden in child classes.
     */
    const TABLE_BINDING = null;

    /**
     * Contains the table, index, and relationship metadata
     * for associated tables models are bound to.
     * 
     * These are statically cached here so additional model instances do not
     * incur the weight of table analysis that has already been done
     * within the same runtime.
     * 
     * @var Oroboros\core\interfaces\library\container\CollectionInterface
     */
    private static $metadata = null;

    /**
     * Represents the database credentials object.
     * @var Oroboros\core\interfaces\library\credentials\CredentialsInterface
     */
    private $credentials = null;

    /**
     * Represents the database connection object.
     * @var Oroboros\core\interfaces\library\connection\ConnectionInterface
     */
    private $connection = null;

    /**
     * The bound schema
     * @var Oroboros\core\interfaces\library\database\schema\SchemaInterface
     */
    private $schema = null;

    /**
     * The bound table
     * @var Oroboros\core\interfaces\library\database\table\TableInterface
     */
    private $table = null;

    /**
     * The table definition for the bound table
     * @var Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $definition = null;

    /**
     * The column definitions for the bound table
     * @var Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $columns = null;

    /**
     * The index definitions for the bound table
     * @var Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $indexes = null;

    /**
     * The index definition for the bound primary key, if one exists
     * @var Oroboros\core\interfaces\library\database\index\IndexInterface
     */
    private $primary_key = null;

    /**
     * The foreign key definition for the bound table
     * @var Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $relationships = null;

    /**
     * The associated record, if any has been loaded
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $record = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        if (is_null($command)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided argument [%2$s] is required for the model to load.', get_class($this), '$command'));
        }
        $this->verifySchemaBinding();
        $this->verifyTableBinding();
        parent::__construct($command, $arguments, $flags);
        $this->loadConnection();
        $this->loadMetadata();
        $this->setObserverEvent('data-ready');
    }
    
    /**
     * Returns a boolean designation as to whether
     * the underlying source data is readonly
     * 
     * If this method returns `false`, all create/update/delete
     * operations will throw an exception.
     * 
     * @return bool
     */
    public function isReadonly(): bool
    {
        return false;
    }

    /**
     * Scopes the model object to the first row corresponding to the given column values.
     * For best results, distinctly identifying column values should be supplied.
     * Only the first row of a multiple set will be considered.
     * @param array $columns Associative array corresponding to key: column name, value: column value.
     *     These details must isolate the expected row.
     *     Primary keys and unique constraint columns are preferable for isolation.
     * @return $this (method chainable)
     * @throws \InvalidArgumentException If no keys were provided,
     *     or no rows were returned with the given provided keys
     * @throws \ErrorException If a query error occurred due to malformed syntax from
     *     improper parameters or if the database cannot be reached.
     */
    public function lookup(array $columns): \Oroboros\core\interfaces\model\ModelInterface
    {
        if (empty($columns)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'No lookup keys provided.', get_class($this)));
        }
        try {
            $query = $this->connection->select(static::SCHEMA_BINDING, static::TABLE_BINDING);
            $is_first = true;
            foreach ($columns as $column => $value) {
                $operand = $this->getOperandByType($value);
                if ($is_first) {
                    $query->where($column, $operand, $value);
                    $is_first = false;
                } else {
                    $query->and($column, $operand, $value);
                }
            }
            $query->limit(null, 1);
            $result = $query->execute()->fetch();
        } catch (\Exception $e) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. '
                        . 'Could not execute model record lookup for table [%2$s.%3$s]. '
                        . 'Reason: [%4$s].', get_class($this), static::SCHEMA_BINDING, static::TABLE_BINDING, $e->getMessage()), $e->getCode());
        }
        if (count($result) === 0) {
            // No record found
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'No record found with provided credentials in [%2$s.%3$s]', get_class($this), static::SCHEMA_BINDING, static::TABLE_BINDING));
        }
        $result = $result->toArray();
        $details = array_shift($result);
        $column_names = array_keys($details);
        $this->record = $this::containerize($details);
        return $this;
    }

    /**
     * Returns all data within the column scope given, or all data if no column is given
     * 
     * @param array $columns
     * @param array $order_by
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     * @throws \Oroboros\core\exception\ErrorException
     */
    public function pull(array $columns = null, array $order_by = null): \Oroboros\core\interfaces\library\data\DataContainerInterface
    {
        if (is_null($columns)) {
            $columns = [];
        }
        try {
            $query = $this->connection->select(static::SCHEMA_BINDING, static::TABLE_BINDING);
            $is_first = true;
            foreach ($columns as $column => $value) {
                $operand = $this->getOperandByType($value);
                if ($is_first) {
                    $query->where($column, $operand, $value);
                    $is_first = false;
                } else {
                    $query->and($column, $operand, $value);
                }
            }
            if (!is_null($order_by)) {
                foreach ($order_by as $column => $sort) {
                    $query->order($column, $sort);
                }
            }
            $result = $query->execute()->fetch();
        } catch (\Exception $e) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. '
                        . 'Could not execute model record lookup for table [%2$s.%3$s]. '
                        . 'Reason: [%4$s].', get_class($this), static::SCHEMA_BINDING, static::TABLE_BINDING, $e->getMessage()), $e->getCode());
        }
        return $result;
    }

    /**
     * Releases the binding to the underlying database record,
     * restoring the model to its default unscoped state.
     * The scoped record is not modified or deleted.
     * The model object should be either expired or rescoped
     * to a new record after calling this method.
     * Has no effect if the record is not currently scoped.
     * @return $this (method chainable)
     */
    public function flush(): \Oroboros\core\interfaces\model\ModelInterface
    {
        $this->record = null;
        return $this;
    }

    /**
     * Returns the value associated with the given column name for the currently scoped record.
     * @param string $key
     * @return mixed
     * @throws \InvalidArgumentException if the given key does not correspond to a known column.
     * @throws \ErrorException If the model is not currently scoped to a record.
     *     This may occur if it was not initially scoped, or if it has previously been deleted.
     */
    public function get(string $key)
    {
        $this->verifyRecord();
        try {
            return $this->record->get($key);
        } catch (\Exception $e) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided key [%2$s] does not exist as '
                        . 'a valid column in [%3$s.%4$s].', get_class($this), $key, static::SCHEMA_BINDING, static::TABLE_BINDING));
        }
    }

    /**
     * Sets the value of the given column name to the given value for the currently scoped record.
     * @param string $key
     * @param type $value
     * @return $this (method chainable)
     * @throws \InvalidArgumentException If the given value is not
     *     appropriate for the specified column and cannot be updated.
     *     This indicates either a nonexistent column, a value type mismatch,
     *     or a failing foreign key constraint.
     * @throws \ErrorException If the model is not currently scoped to a record.
     *     This may occur if it was not initially scoped, or if it has previously been deleted.
     * @todo Cascade updates to corresponding models with foreign keys set to cascade
     *     that would be affected by the update.
     */
    public function set(string $key, $value): \Oroboros\core\interfaces\model\ModelInterface
    {
        $this->verifyRecord();
        if (!$this->record->has($key)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided key [%2$s] does not exist as '
                        . 'a valid column in [%3$s.%4$s].', get_class($this), $key, static::SCHEMA_BINDING, static::TABLE_BINDING));
        }
        $query = $this->connection->update(static::SCHEMA_BINDING, static::TABLE_BINDING)->set($key, $value);
        $this->scopeQueryToRecord($query);
        try {
            $query->execute();
        } catch (\Exception $e) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Could not set value [%2$s] for record in table [%3$s.%4$s]. '
                        . 'Reason: [%5$s].', get_class($this), $key, static::SCHEMA_BINDING, static::TABLE_BINDING, $e->getMessage()), $e->getCode());
        }
        $this->record[$key] = $value;
        return $this;
    }

    /**
     * Inserts a new row into the database, and scopes the model object to it.
     * Will return a clone of the model scoped to the new record, leaving the
     * original object still usable in its original context.
     * @param array $details Associative array with key: column name, value: column insert value
     * @return \Oroboros\core\interfaces\model\ModelInterface
     *     Will return a clone of the current object scoped to the new record.
     * @throws \ErrorException If a new record could not be created with the given details.
     */
    public function create(array $details): \Oroboros\core\interfaces\model\ModelInterface
    {
        $query = $this->connection->insert(static::SCHEMA_BINDING, static::TABLE_BINDING)
            ->columns(array_keys($details))
            ->values($details);
        try {
            $query->execute();
        } catch (\Exception $e) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. '
                        . 'Unable to create new row in table [%2$s.%3$s] with provided details. '
                        . 'Reason: [%4$s].', get_class($this), static::SCHEMA_BINDING, static::TABLE_BINDING, $e->getMessage()), $e->getCode());
        }
        $class = get_class($this);
        $object = new $class($this->getCommand(), $this->getArguments(), $this->getFlags());
        $object->lookup($details);
        return $object;
    }

    /**
     * Batches several new rows into the corresponding table,
     * and returns a container of model objects scoped to each new row.
     * @param array $rows a multidimensional array. Values should be
     *     associative arrays where the key is the column name and the value
     *     is the new value for that column. Top level array may be either
     *     indexed or associative. Return keys of the container will correspond
     *     to string cast equivalent of the top level array keys provided.
     * @param bool $break_on_error (optional). If true will halt batch inserts
     *     if an error is encountered. If false, will skip errors and continue
     *     the batch for any subsequent rows. Default true.
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     * @throws \ErrorException If a new record could not be created with the given details
     *     for any given sub-array. Note that this may result in incomplete inserts.
     */
    public function batch(array $rows, bool $break_on_error = true): \Oroboros\core\interfaces\library\data\DataContainerInterface
    {
        $models = $this::containerize();
        foreach ($rows as $key => $details) {
            try {
                $models[(string) $key] = $this->create($details);
            } catch (\ErrorException $e) {
                if (!$break_on_error) {
                    continue;
                }
                throw $e;
            }
        }
        return $models;
    }

    /**
     * Deletes the associated record corresponding to the model object, and purges its values from memory.
     * The model object should then be either expired or re-initialized to a new record.
     * @return $this (method chainable)
     * @throws \ErrorException If the model is not currently scoped to a record.
     *     This may occur if it was not initially scoped,
     *     if it has previously been deleted,
     *     if a database connection could not be established,
     *     or if a key constraint prevents deleting the row.
     */
    public function delete(string $key = null): \Oroboros\core\interfaces\model\ModelInterface
    {
        $this->verifyRecord();
        $query = $this->connection->delete(static::SCHEMA_BINDING, static::TABLE_BINDING);
        $this->scopeQueryToRecord($query);
        try {
            $query->execute();
        } catch (\Exception $e) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. '
                        . 'Unable to delete associated record for table [%2$s.%3$s]. '
                        . 'Reason: [%4$s].', get_class($this), static::SCHEMA_BINDING, static::TABLE_BINDING, $e->getMessage()), $e->getCode());
        }
        return $this->flush();
    }

    /**
     * Returns a container of values keyed by column name,
     * with values corresponding to the data type of the associated column.
     * 
     * This can be used for type verification of insert values.
     * 
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    public function getKeys(): \Oroboros\core\interfaces\library\data\DataContainerInterface
    {
        $keys = [];
        foreach ($this->columns as $column) {
            $keys[$column['COLUMN_NAME']] = $column['COLUMN_TYPE'];
        }
        return $this::containerize($keys);
    }

    /**
     * Returns a container of column objects corresponding to
     * all columns of the underlying table represented by the model.
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    protected function getColumns(): \Oroboros\core\interfaces\library\data\DataContainerInterface
    {
        return $this->columns;
    }

    /**
     * Returns a column object for a specified column
     * @param string $key The column name
     * @return \Oroboros\core\interfaces\library\database\column\ColumnInterface
     * @throws \InvalidArgumentException If the given column is not known
     */
    protected function getColumn(string $key): \Oroboros\core\interfaces\library\database\column\ColumnInterface
    {
        if (!$this->columns->has($key)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Column [%2$s] does not exist in this scope.', get_class($this), $key));
        }
        return $this->columns->get($key);
    }

    /**
     * Returns the table object associated with the model
     * @return \Oroboros\core\interfaces\library\database\table\TableInterface
     */
    protected function getTable(): \Oroboros\core\interfaces\library\database\table\TableInterface
    {
        return $this->table;
    }

    /**
     * Returns the schema object associated with the model
     * @return \Oroboros\core\interfaces\library\database\schema\SchemaInterface
     */
    protected function getSchema(): \Oroboros\core\interfaces\library\database\schema\SchemaInterface
    {
        return $this->schema;
    }

    /**
     * Verifies that an existing record is currently scoped to the model.
     * This method should be called immediately within all methods that directly
     * interact with the scoped record to prevent errors.
     * @return void
     * @throws \ErrorException If no record is currently scoped to the model
     */
    protected function verifyRecord(): void
    {
        if (is_null($this->record)) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. '
                        . 'No record initialized. '
                        . 'Please initialize a record with [%2$s] '
                        . 'before proceeding.', get_class($this), get_class($this) . '::lookup'));
        }
    }

    /**
     * Provides the WHERE clauses to a query that identify the underlying record associated with the model.
     * @param \Oroboros\core\interfaces\library\database\query\QueryInterface $query
     * @return void
     * @throws \ErrorException If the model is not currently scoped to a record.
     *     This may occur if it was not initially scoped, or if it has previously been deleted.
     * @throws \DomainException If the table structure of the database lacks 
     *     any primary key or unique constraint appropriate to effectively
     *     isolate the record. This indicates that the SQL structure needs
     *     to be refactored outside of the application codebase.
     */
    protected function scopeQueryToRecord(\Oroboros\core\interfaces\library\database\query\QueryInterface $query): void
    {
        $is_first = true;
        $columns = $this::containerize();
        $this->verifyRecord();
        if (!is_null($this->primary_key)) {
            // Use the primary key to isolate the record
            foreach ($this->primary_key->getColumns() as $key => $column) {
                $columns[$key] = $column;
            }
        } elseif (!is_null($this->indexes)) {
            // Use known indexed columns to isolate the record
            // This is less efficient, and will only occur on 
            // a table that does not have a primary key
            foreach ($this->indexes as $index) {
                foreach ($index->getColumns() as $key => $column) {
                    if (!$columns->has($key)) {
                        $columns[$key] = $column;
                    }
                }
            }
        } else {
            // The record cannot be appropriately scoped because no 
            // unique constraints exist on the underlying row. 
            // This indicates that the database table needs to be 
            // reworked on the SQL side.
            throw new \DomainException(sprintf('Error encountered in [%1$s]. '
                        . 'No unique index or primary key found for table [%2$s.%3$s]. '
                        . 'Please set a primary key or unique constraint on the table '
                        . 'structure in the database to prevent ambiguous references.', get_class($this), static::SCHEMA_BINDING, static::TABLE_BINDING));
        }
        foreach ($columns as $key => $column) {
            // Refactor to account for aliasing on joins
//            $colname = sprintf('%1$s%4$s%2$s%4$s%3$s', $column->getSchema()->getName(), $column->getTable()->getName(), $column->getName());
            $colname = sprintf('%1$s', $column->getName());
            if ($is_first) {
                $query->where($colname, '=', $this->get($column->getName()));
            } else {
                $query->and($colname, '=', $this->get($column->getName()));
            }
        }
    }

    private function loadConnection(): void
    {
        $this->loadCredentials();
        $this->connection = $this->credentials->getConnection();
    }

    private function loadCredentials(): void
    {
        $env = $this::app()->environment()->get('application');
        if (!$env->has('connections')) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. '
                        . 'Expected environment key [%2$s] is not defined.', get_class($this), 'connections'));
        }
        $connections = $env->get('connections');
        if (!array_key_exists(static::DATA_LAYER, $connections)) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. '
                        . 'Expected connection key [%2$s] is not defined.', get_class($this), static::DATA_LAYER));
        }
        if (!$this->hasArgument('connection')) {
            $connection = $this->loadDefaultConnection();
        } else {
            $connection = $this->getArgument('connection');
        }
        $connections = $connections[static::DATA_LAYER];
        if (!array_key_exists($connection, $connections)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Connection key [%2$s] does not exist amongst any known connections '
                        . 'for type [%3$s].', get_class($this), $connection, static::DATA_LAYER));
        }
        $this->credentials = $this->load('library', 'database\\MysqlCredentials', $connection, $connections[$connection]);
    }

    private function loadDefaultConnection(): string
    {
        $connections = $this::app()->environment()->get('application')->get('connections');
        if (!array_key_exists('defaults', $connections)) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. '
                        . 'No connection key provided, and no default connections defined in the environment.', get_class($this)));
        }
        if (!array_key_exists(static::DATA_LAYER, $connections['defaults'])) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. '
                        . 'No connection key provided, and no default connections '
                        . 'defined for data layer [%2$s] in the environment.', get_class($this), static::DATA_LAYER));
        }
        return $connections['defaults'][static::DATA_LAYER];
    }

    private function loadMetadata(): void
    {
        $this->definition = $this::containerize();
        $this->columns = $this::containerize();
        $this->indexes = $this::containerize();
        $this->relationships = $this::containerize();
        if (is_null(self::$metadata)) {
            self::$metadata = $this->containerizeInto('Oroboros\\core\\library\\container\\Collection');
        }
        if (!self::$metadata->has(get_class($this))) {
            self::$metadata[get_class($this)] = $this->buildModelMetadata();
        }
        $def = self::$metadata->get(get_class($this));
        $this->schema = $def['schema'];
        $this->table = $def['table'];
        try {
            $this->primary_key = $this->table->getPrimaryKey();
        } catch (\Exception $e) {
            // No primary key
            $this->primary_key = null;
        }
    }

    private function buildModelMetadata(): \Oroboros\core\interfaces\library\data\DataContainerInterface
    {
        $meta = $this->containerizeInto('Oroboros\\core\\library\\container\\Container');
        $meta['schema'] = $this->connection->schema();
        $meta['table'] = $meta['schema']->getTable(static::TABLE_BINDING);
//        $meta['columns'] = $meta['table']->getColumns();
//        $meta['indexes'] = $meta['table']->getIndexes();
//        $meta['primary'] = $meta['table']->getPrimaryKey();
//        $meta['relationships'] = $meta['table']->getRelationships();
        return $meta;
    }

    private function getOperandByType($subject)
    {
        $operand = '=';
        if (is_null($subject)) {
            $operand = 'IS NULL';
        } elseif (is_iterable($subject)) {
            $operand = 'IN';
        } elseif (is_string($subject) && ((strpos($subject, '%') === 0 || strrpos($subject, '%') === strlen($subject)))) {
            $operand = 'LIKE';
        }
        return $operand;
    }

    private function verifySchemaBinding(): void
    {
        if (is_null(static::SCHEMA_BINDING)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(sprintf('Error encountered in [%1$s]. '
                        . 'Expected class constant [%2$s] must be defined.', get_class($this), 'SCHEMA_BINDING'));
        }
    }

    private function verifyTableBinding(): void
    {
        if (is_null(static::TABLE_BINDING)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(sprintf('Error encountered in [%1$s]. '
                        . 'Expected class constant [%2$s] must be defined.', get_class($this), 'TABLE_BINDING'));
        }
    }
}
