<?php
namespace Oroboros\core\abstracts\model;

/**
 * Abstract File Model
 * 
 * Sets the abstract foundation for models
 * that operate against a flat file or configuration file.
 * 
 * This model type is useful for low level operations that are not particularly complex,
 * and handling changes to files that need to be read externally,
 * but where the external consuming process requires a specific format
 * or lacks a database connector implementation.
 *
 * @author Brian Dayhoff
 */
abstract class AbstractFileModel extends AbstractModel implements \Oroboros\core\interfaces\model\FileModelInterface
{

    /**
     * Defines child classes as file models.
     */
    const CLASS_SCOPE = 'file-model';

    /**
     * The fully qualified source file path
     * @var string
     */
    private static $source_file = null;

    /**
     * The data contained in the source file.
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private static $data = null;

    /**
     * Determines if the underlying source file is writeable
     * 
     * This will assume readonly unless provable otherwise
     * 
     * @var bool
     */
    private $is_readonly = true;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->verifyDataFormat();
        $this->verifyDataSource();
        $this->verifyParser();
        $this->verifyReadableFile();
        $this->verifyWriteableFile();
        $this->loadData();
        $this->setObserverEvent('file-ready');
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    /**
     * Returns a boolean designation as to whether
     * the underlying source file is readonly
     * 
     * If this method returns `false`, all update/insert/delete
     * operations will throw an exception.
     * 
     * @return bool
     */
    public function isReadonly(): bool
    {
        return is_writable($this->getSourceFile());
    }

    /**
     * Returns the fully qualified path
     * to the source file represented by the model
     * 
     * @return string
     */
    public function getSourceFile(): string
    {
        return $this->defineSourceFile();
    }

    /**
     * Returns the data format of the model
     * eg json, xml, ini, etc
     * 
     * @return string
     */
    public function getFormat(): string
    {
        return $this->getModelType();
    }

    public function getKeys(): \Oroboros\core\interfaces\library\data\DataContainerInterface
    {
        ;
    }

    /**
     * Returns the full dataset represented by the model
     * 
     * @return \Oroboros\core\interfaces\library\data\DataContainerInterface
     */
    public function fetch(): \Oroboros\core\interfaces\library\data\DataContainerInterface
    {
        return $this->containerize(self::$data[get_class($this)]->toArray());
    }

    /**
     * Returns the value for the specified key
     * 
     * @param string $key
     */
    public function get(string $key)
    {
        return $this->getData($key);
    }

    /**
     * Sets a value for a specified key.
     * If the key already exists, it will be updated.
     * If it does not exist, it will be created.
     * 
     * If the model is read-only, this will raise an 
     * 
     * @param string $key
     * @param type $value
     * @return \Oroboros\core\interfaces\model\ModelInterface
     */
    public function set(string $key, $value): \Oroboros\core\interfaces\model\ModelInterface
    {
        if ($this->isReadonly()) {
            throw new \Oroboros\core\exception\model\ReadonlyException(
                    sprintf('Error encountered in [%1$s]. Cannot set data '
                        . 'because the model is in readonly status.', get_class($this))
            );
        }
        $this->flagAsModified();
        return $this;
    }

    /**
     * Returns a boolean designation as to whether a given
     * key exists in the data set represented by the model.
     * 
     * @param string $key
     * @return bool
     */
    public function hasKey(string $key): bool
    {
        return self::$data[get_class($this)]->has($key);
    }

    /**
     * Creates new data within the record(s) represented by the model.
     * 
     * If the model is in readonly status, a `\Oroboros\core\exception\model\ReadonlyException`
     * will be raised preventing the operation.
     * 
     * @param array $details
     * @return \Oroboros\core\interfaces\model\ModelInterface
     * @throws \Oroboros\core\exception\model\ReadonlyException
     */
    public function create(array $details): \Oroboros\core\interfaces\model\ModelInterface
    {
        if ($this->isReadonly()) {
            throw new \Oroboros\core\exception\model\ReadonlyException(
                    sprintf('Error encountered in [%1$s]. Cannot create data '
                        . 'because the model is in readonly status.', get_class($this))
            );
        }
        $this->flagAsModified();
        return $this;
    }

    /**
     * Deletes a given key, if specified. If not specified,
     * deletes the entire record.
     * 
     * If method `isReadonly` returns true, this will ALWAYS
     * raise an `\Oroboros\core\exception\model\ReadonlyException`
     * 
     * It MUST also raise such an exception if any other conditions,
     * logic, or availability conditions interfere with deletion.
     * 
     * @param string $key (required)
     * @return \Oroboros\core\interfaces\model\ModelInterface
     * @throws \Oroboros\core\exception\model\ReadonlyException
     *         if the model is readonly, if a file is not writeable,
     *         if an api has no edit methods, if an underlying object
     *         can't be deleted, or doesn't have an unsetter method
     *         for the given key, or if a database constraint fails
     *         or the connection does not have sufficient privileges
     *         to delete. Any criteria that forcibly or programatically
     *         block deletion will raise this exception.
     * @param string $key
     * @return \Oroboros\core\interfaces\model\ModelInterface
     * @throws \Oroboros\core\exception\model\ReadonlyException
     *         if the model is in readonly status, or if no key is passed.
     *         File models cannot delete their entire file. A key must
     *         be provided to call this method.
     * @throws \Oroboros\core\exception\model\MissingKeyException
     *         If the specified key does not exist.
     */
    public function delete(string $key = null): \Oroboros\core\interfaces\model\ModelInterface
    {
        if ($this->isReadonly()) {
            throw new \Oroboros\core\exception\model\ReadonlyException(
                    sprintf('Error encountered in [%1$s]. Cannot delete data '
                        . 'because the model is in readonly status.', get_class($this))
            );
        }
        if (!is_null($key)) {
            throw new \Oroboros\core\exception\model\ReadonlyException(
                    sprintf('Error encountered in [%1$s]. '
                        . 'File models cannot delete their entire file. '
                        . 'A key must be supplied for a delete operation.'
                        , get_class($this), $key)
            );
        }
        if (!$this->hasKey($key)) {
            throw new \Oroboros\core\exception\model\MissingKeyException(
                    sprintf('Error encountered in [%1$s]. Supplied key [%2$s] '
                        . 'does not exist in this scope.', get_class($this), $key)
            );
        }
        unset(self::$data[get_class($this)][$key]);
        $this->flagAsModified();
        return $this;
    }

    /**
     * This method will be called when data is committed
     * to apply the changes back to the original file.
     * 
     * Individual implementations must override this to
     * provide logic for saving the data set back to it's
     * original output format.
     * 
     * @return void
     */
    protected function saveDataChanges(): void
    {
        // no-op
        parent::saveDataChanges();
    }

    /**
     * Defines the base path that the subject file exists in
     * 
     * @return string
     */
    protected function defineSourceFilePath(): string
    {
        $path = $this::app()->getApplicationRoot();
        return $path;
    }

    /**
     * Returns a data key if it exists.
     * 
     * If it does not exist, a `\Oroboros\core\exception\model\MissingKeyException`
     * will be raised.
     * 
     * @param string $key
     * @return mixed
     * @throws \Oroboros\core\exception\model\MissingKeyException
     *         if the specified key does not exist.
     */
    protected function getData(string $key = null)
    {
        if (is_null($key)) {
            return self::$data[get_class($this)];
        }
        if (!$this->hasKey($key)) {
            throw new \Oroboros\core\exception\model\MissingKeyException(
                    sprintf('Error encountered in [%1$s]. Provided data key [%2$s] '
                        . 'does not exist in this scope.', get_class($this) . $key)
            );
        }
        return self::$data[get_class($this)][$key];
    }

    /**
     * Defines the source file.
     * This method should only return a filename, not any path parameters.
     * 
     * Path parameters should only be returned by `defineSourceFilePath`
     * 
     * @return string
     */
    protected function defineSourceFile()
    {
        $source = $this->defineSourceFilePath() . static::DATA_SOURCE;
        return $source;
    }

    /**
     * Loads the initial data set if it is not already loaded.
     * 
     * @return void
     */
    private function loadData(): void
    {
        if (is_null(self::$data)) {
            $this->initializeDataContainer();
            $data = $this->load('library', static::PARSER_CLASS, self::$source_file)->fetch();
            self::$data[get_class($this)] = $data;
        }
        if (!self::$data->has(get_class($this))) {
            $data = $this->parseDataSet();
            self::$data[get_class($this)] = $data;
        }
    }

    /**
     * Parses the specified file into a data container object and returns it
     * 
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     * @throws \Oroboros\core\exception\model\MissingResourceException
     */
    private function parseDataSet(): \Oroboros\core\interfaces\library\data\DataContainerInterface
    {
        if (!is_readable($this->getSourceFile())) {
            throw new \Oroboros\core\exception\model\MissingResourceException(
                    sprintf('Error encountered in [%1$s]. Expected data resource '
                        . 'file [%2$s] of type [%3$s] does not exist or is not readable.', get_class($this), $this->getFormat(), $this->getSourceFile())
            );
        }
        $data = $this->containerize($this->load('library', static::PARSER_CLASS, $this->getSourceFile())->fetch()->toArray());
        return $data;
    }

    /**
     * Creates the master container if it does not already exist
     * 
     * @return void
     */
    private function initializeDataContainer(): void
    {
        if (is_null(self::$data)) {
            self::$data = $this->containerizeInto(static::COLLECTION_CLASS);
        }
    }

    /**
     * Verifies that the source file is readable as declared.
     * 
     * Throws a `\Oroboros\core\exception\model\MissingResourceException`
     * if the source cannot be read
     * 
     * @return void
     * @throws \Oroboros\core\exception\model\MissingResourceException
     */
    private function verifyReadableFile(): void
    {
        $file = $this->getSourceFile();
        if (!is_readable($file)) {
            throw new \Oroboros\core\exception\model\MissingResourceException(
                    sprintf('Error encountered in [%1$s]. Declared data endpoint '
                        . '[%2$s] does not exist or is not readable.', get_class($this), $file)
            );
        }
    }

    /**
     * Verifies that the data source file is writeable
     * and removes readonly status if it is.
     * 
     * @return void
     */
    private function verifyWriteableFile(): void
    {
        $file = $this->getSourceFile();
        if (is_writable($file)) {
            $this->is_readonly = false;
        }
    }

    /**
     * Verifies that the data format class constant is properly declared.
     * 
     * @return void
     * @throws \Oroboros\core\exception\core\InvalidClassException
     */
    private function verifyDataFormat(): void
    {
        $class = get_class($this);
        $expected = 'DATA_FORMAT';
        if (!defined($class . '::' . $expected)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(sprintf('Error encountered in [%1$s]. Expected class constant [%2$s] is not defined.', $class, $expected));
        }
    }

    /**
     * Verifies that the data parser class is properly declared.
     * 
     * @return void
     * @throws \Oroboros\core\exception\core\InvalidClassException
     */
    private function verifyParser(): void
    {
        $class = get_class($this);
        $expected = 'PARSER_CLASS';
        if (!defined($class . '::' . $expected)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(sprintf('Error encountered in [%1$s]. Expected class constant [%2$s] is not defined.', $class, $expected));
        }
        $classname = $this->getFullClassName('library', static::PARSER_CLASS);
        if ($classname === false) {
            throw new \Oroboros\core\exception\core\InvalidClassException(sprintf('Error encountered in [%1$s]. Expected class constant [%2$s] was not found.', $class, static::PARSER_CLASS));
        }
        $interface = 'Oroboros\\core\\interfaces\\library\\parser\\ParserInterface';
        if (!in_array($interface, class_implements($classname))) {
            throw new \Oroboros\core\exception\core\InvalidClassException(sprintf('Error encountered in [%1$s]. Provided parser class [%2$s] must implement expected interface [%3$s].', $class, static::PARSER_CLASS, $interface));
        }
    }

    /**
     * Verifies that the data source is properly declared.
     * 
     * @return void
     * @throws \Oroboros\core\exception\core\InvalidClassException
     * @throws \Oroboros\core\exception\ErrorException
     */
    private function verifyDataSource(): void
    {
        $class = get_class($this);
        $expected = 'DATA_SOURCE';
        if (!defined($class . '::' . $expected)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(sprintf('Error encountered in [%1$s]. Expected class constant [%2$s] is not defined.', $class, $expected));
        }
        $file = $this->defineSourceFile();
        if (!is_readable($file)) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. Data source file [%2$s] does not exist or is not readable.', $class, $file));
        }
        if (is_null(self::$source_file)) {
            self::$source_file = $file;
        }
    }
}
