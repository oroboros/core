<?php
namespace Oroboros\core\abstracts\model;

/**
 * Abstract Service Model
 * 
 * Sets the abstract foundation for models
 * associated with a remote service api.
 * 
 * This model allows api calls to be represented as models.
 *
 * @author Brian Dayhoff
 */
abstract class AbstractAdapterModel extends AbstractModel implements \Oroboros\core\interfaces\model\AdapterModelInterface
{

    /**
     * Defines child classes as adapter models.
     */
    const CLASS_SCOPE = 'adapter-model';

}
