<?php
namespace Oroboros\core\abstracts\model;

/**
 * Abstract Model
 * 
 * Sets the abstract foundation for how
 * models interact with the data layer.
 *
 * @author Brian Dayhoff
 */
abstract class AbstractModel extends \Oroboros\core\abstracts\AbstractBase implements \Oroboros\core\interfaces\model\ModelInterface
{

    use \Oroboros\core\traits\LoaderTrait;
    use \Oroboros\core\traits\ContainerPackagerUtility;
    use \Oroboros\core\traits\StringUtilityTrait;
    use \Oroboros\core\traits\pattern\ObservableTrait;
    use \Oroboros\core\traits\pattern\ObserverTrait;
    use \Oroboros\core\traits\pattern\EventAwareTrait;
    use \Oroboros\core\traits\pattern\FilterAwareTrait;

    /**
     * Designates further child classes as models.
     */
    const CLASS_TYPE = 'model';

    /**
     * Class scope should be defined by subclasses.
     */
    const CLASS_SCOPE = null;

    /**
     * This constant must be overridden to
     * declare the type of endpoint the model interacts with.
     * 
     * For a data model, this would be the schema type.
     * For a file model, this would be the file format.
     * For an object model, this would be the target class type.
     * For a service model, this would be the service name.
     * For an adapter model, this would be the adapter scope.
     */
    const MODEL_TYPE = null;

    /**
     * This constant must be overridden to
     * declare the scope the model interacts with.
     * 
     * For a data model, this would be the schema name.
     * For a file model, this would be the file name.
     * For an object model, this would be the target class name.
     * For a service model, this would be the service endpoint identifier.
     * For an adapter model, this would be the adapter connection protocol (if any).
     */
    const MODEL_SCOPE = null;

    /**
     * Use the standard data container
     */
    const CONTAINER_CLASS = \Oroboros\core\library\data\DataContainer::class;

    /**
     * Use the standard data collection
     */
    const COLLECTION_CLASS = \Oroboros\core\library\data\DataCollection::class;

    /**
     * If any changes are made at all to the underlying data, this will be `true`.
     * Otherwise always `false`.
     * 
     * This is used as a flag to determine if data should be committed
     * or not to prevent redundant expensive write operations
     * 
     * @var bool
     */
    private $is_changed = false;

    /**
     * Oroboros models always use the standard constructor
     * 
     * @param string $command per implementation
     * @param array $arguments per implementation
     * @param array $flags per implementation
     * 
     * @throws \Oroboros\core\exception\model\MissingCredentialsException
     *         If credentials are required but have not been supplied
     * @throws \Oroboros\core\exception\model\InvalidCredentialsException
     *         If the supplied credentials are not valid for the
     *         given connection requested
     * @throws \Oroboros\core\exception\model\MissingResourceException
     *         If the underlying data source is not reachable.
     *         This could be a database that is not enabled or blocked by
     *         network rules, a missing file, an unloadable object,
     *         or an unreachable api
     */
    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        $this->verifyModelType();
        parent::__construct($command, $arguments, $flags);
        $this->initializeObservable();
        $this->initializeObserver();
        $this->initializeEvents();
        $this->initializeFilters();
        $this->setObserverEvent('ready');
        $this->getLogger()->debug('[type][scope][class] Initialized model of type [model-type] in scope [model-scope].', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'model-type' => static::CLASS_TYPE,
            'model-scope' => static::CLASS_SCOPE,
        ]);
    }

    /**
     * Commits any data changes on object destruction
     */
    public function __destruct()
    {
        $this->setObserverEvent('teardown');
        $this->commit();
    }

    /**
     * Returns the model type.
     * 
     * @return string|null
     */
    public static function getModelType(): string
    {
        self::verifyModelType();
        return static::MODEL_TYPE;
    }

    /**
     * Returns the model scope, if one is declared.
     * 
     * @return string|null
     */
    public static function getModelScope(): ?string
    {
        return static::MODEL_SCOPE;
    }
    
    /**
     * Returns a boolean designation as to whether
     * the underlying source file is readonly
     * 
     * If this method returns `false`, all update/insert/delete
     * operations will throw an exception.
     * 
     * @return bool
     */
    public function isReadonly(): bool
    {
        return false;
    }

    /**
     * Commits any changed data back to the original data source
     * if any changes were made and the model is not flagged
     * as readonly.
     * 
     * If the model is flagged as readonly, calling this
     * method will do nothing but is otherwise non-blocking.
     * 
     * @return \Oroboros\core\interfaces\model\ModelInterface
     */
    public function commit(): \Oroboros\core\interfaces\model\ModelInterface
    {
        if (!$this->isReadonly() && $this->isModified()) {
            $this->saveDataChanges();
        }
        return $this;
    }

    /**
     * Returns the full dataset represented by the model
     * 
     * @return \Oroboros\core\interfaces\library\data\DataContainerInterface
     */
    public function fetch(): \Oroboros\core\interfaces\library\data\DataContainerInterface
    {
        return $this->containerize();
    }

    /**
     * Returns an arbitrary set of keys that exist within the dataset,
     * ordered by the given criteria, if any.
     * 
     * This works similarly to fetch, except it
     * allows filtering keys and sorting.
     * 
     * @param array $columns
     * @param array $order_by
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    public function pull(array $columns = null, array $order_by = null): \Oroboros\core\interfaces\library\data\DataContainerInterface
    {
        $results = [];
        $missing = [];
        $data = $this->fetch();
        if (is_null($columns)) {
            return $data;
        }
        foreach ($columns as $key) {
            if (!$data->has($key)) {
                $missing[] = $key;
            }
        }
        if (!empty($missing)) {
            throw new \Oroboros\core\exception\model\MissingKeyException(
                    sprintf('Error encountered in [%1$s]. Specified keys [%2$s] '
                        . 'do not exist in this scope.'
                        , get_class($this), implode(', ', $missing))
            );
        }
        foreach ($columns as $key => $value) {
            if (array_key_exists($key, $columns)) {
                $results[$key] = $value;
            }
        }
        return $this;
    }
    
    public function get(string $key)
    {
        return null;
    }
    
    public function set(string $key, $value): \Oroboros\core\interfaces\model\ModelInterface
    {
        return $this;
    }
    
    public function create(array $details): \Oroboros\core\interfaces\model\ModelInterface
    {
        return $this;
    }
    
    public function delete(string $key = null): \Oroboros\core\interfaces\model\ModelInterface
    {
        return $this;
    }
    
    /**
     * Returns a boolean determination as to whether a given
     * column name exists within the scope of the current model.
     * @param string $key
     * @return bool
     */
    public function hasKey(string $key): bool
    {
        return $this->getKeys()->has($key);
    }
    
    public function getKeys(): \Oroboros\core\interfaces\library\data\DataContainerInterface
    {
        return $this->containerize();
    }

    /**
     * This method will be called when data is committed
     * to apply the changes back to the original file.
     * 
     * Individual implementations must override this to
     * provide logic for saving the data set back to it's
     * original output format.
     * 
     * @return void
     */
    protected function saveDataChanges(): void
    {
        // no-op
        $this->is_changed = false;
    }

    /**
     * Returns a boolean designation as to whether or not
     * the data set has been changed since instantiation.
     * 
     * Individual instances of the model track this
     * separately even if they reference the same data,
     * so that only the ones that applied changes will
     * call update methods against the permanent source.
     * 
     * @return bool
     */
    protected function isModified(): bool
    {
        return $this->is_changed;
    }

    /**
     * Calling this method at any time will flag the dataset as changed,
     * so commit operations will proceed when called.
     * 
     * @return void
     */
    protected function flagAsModified(): void
    {
        $this->is_changed = true;
    }

    /**
     * Sets the observable name for the model
     * @return string
     */
    protected function declareObservableName(): string
    {
        return trim(sprintf('%1$s:%2$s', static::CLASS_TYPE, static::CLASS_SCOPE), ':');
    }

    /**
     * Verifies that the model has properly declared it's type
     * 
     * @return void
     * @throws \Oroboros\core\exception\core\InvalidClassException
     */
    private static function verifyModelType(): void
    {
        if (is_null(static::MODEL_TYPE)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. Expected class constant '
                        . '[%2$s] is not defined. This class is not useable in '
                        . 'it\'s current state.', static::class, 'MODEL_TYPE')
            );
        }
    }
}
