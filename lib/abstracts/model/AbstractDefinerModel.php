<?php
namespace Oroboros\core\abstracts\model;

/**
 * Abstract Definer Model
 * 
 * Sets the abstract foundation for models
 * that operate against a definer object.
 * 
 * This model type is used similarly to the file model,
 * but works against definer objects.
 * 
 * It will use the base dataset of the definer as it's internal data,
 * and restricts write operations to whether the definer is writable.
 *
 * @author Brian Dayhoff
 */
abstract class AbstractDefinerModel extends AbstractModel implements \Oroboros\core\interfaces\model\DefinerModelInterface
{

    /**
     * Defines child classes as definer models.
     */
    const CLASS_SCOPE = 'definer-model';

    /**
     * Must be overridden by abstraction.
     * This constant must declare a valid class name or stub class name
     * resolving to a class implementing
     * `\Oroboros\core\interfaces\library\definer\DefinerInterface`
     * 
     * This will be used as the data source for the model
     */
    const DEFINER_CLASS = null;

    /**
     * The compiled definer data.
     * 
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private static $data = null;

    /**
     * The definer object the model is working against for its data set
     * 
     * @var \Oroboros\core\interfaces\library\definer\DefinerInterface
     */
    private $definer = null;

    /**
     * Determines if the underlying source file is writeable
     * 
     * This will assume readonly unless provable otherwise
     * 
     * @var bool
     */
    private $is_readonly = true;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->verifyDefiner();
        $this->loadData();
        $this->setObserverEvent('definer-ready');
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    /**
     * Returns a boolean designation as to whether
     * the underlying source file is readonly
     * 
     * If this method returns `false`, all update/insert/delete
     * operations will throw an exception.
     * 
     * @return bool
     */
    public function isReadonly(): bool
    {
        return true;
    }

    /**
     * Returns the data format of the model
     * eg json, xml, ini, etc
     * 
     * @return string
     */
    public function getFormat(): string
    {
        return $this->getModelType();
    }

    public function getKeys(): \Oroboros\core\interfaces\library\data\DataContainerInterface
    {
        ;
    }

    /**
     * Returns the full dataset represented by the model
     * 
     * @return \Oroboros\core\interfaces\library\data\DataContainerInterface
     */
    public function fetch(): \Oroboros\core\interfaces\library\data\DataContainerInterface
    {
        return $this->containerize(self::$data[get_class($this)]->toArray());
    }

    /**
     * Returns the value for the specified key
     * 
     * @param string $key
     */
    public function get(string $key)
    {
        return $this->getData($key);
    }

    /**
     * Sets a value for a specified key.
     * If the key already exists, it will be updated.
     * If it does not exist, it will be created.
     * 
     * If the model is read-only, this will raise an 
     * 
     * @param string $key
     * @param type $value
     * @return \Oroboros\core\interfaces\model\ModelInterface
     */
    public function set(string $key, $value): \Oroboros\core\interfaces\model\ModelInterface
    {
        if ($this->isReadonly()) {
            throw new \Oroboros\core\exception\model\ReadonlyException(
                    sprintf('Error encountered in [%1$s]. Cannot set data '
                        . 'because the model is in readonly status.', get_class($this))
            );
        }
        $this->flagAsModified();
        return $this;
    }

    /**
     * Returns a boolean designation as to whether a given
     * key exists in the data set represented by the model.
     * 
     * @param string $key
     * @return bool
     */
    public function hasKey(string $key): bool
    {
        return self::$data[get_class($this)]->has($key);
    }

    /**
     * Creates new data within the record(s) represented by the model.
     * 
     * If the model is in readonly status, a `\Oroboros\core\exception\model\ReadonlyException`
     * will be raised preventing the operation.
     * 
     * @param array $details
     * @return \Oroboros\core\interfaces\model\ModelInterface
     * @throws \Oroboros\core\exception\model\ReadonlyException
     */
    public function create(array $details): \Oroboros\core\interfaces\model\ModelInterface
    {
        if ($this->isReadonly()) {
            throw new \Oroboros\core\exception\model\ReadonlyException(
                    sprintf('Error encountered in [%1$s]. Cannot create data '
                        . 'because the model is in readonly status.', get_class($this))
            );
        }
        $this->flagAsModified();
        return $this;
    }

    /**
     * Deletes a given key, if specified. If not specified,
     * deletes the entire record.
     * 
     * If method `isReadonly` returns true, this will ALWAYS
     * raise an `\Oroboros\core\exception\model\ReadonlyException`
     * 
     * It MUST also raise such an exception if any other conditions,
     * logic, or availability conditions interfere with deletion.
     * 
     * @param string $key (required)
     * @return \Oroboros\core\interfaces\model\ModelInterface
     * @throws \Oroboros\core\exception\model\ReadonlyException
     *         if the model is readonly, if a file is not writeable,
     *         if an api has no edit methods, if an underlying object
     *         can't be deleted, or doesn't have an unsetter method
     *         for the given key, or if a database constraint fails
     *         or the connection does not have sufficient privileges
     *         to delete. Any criteria that forcibly or programatically
     *         block deletion will raise this exception.
     * @param string $key
     * @return \Oroboros\core\interfaces\model\ModelInterface
     * @throws \Oroboros\core\exception\model\ReadonlyException
     *         if the model is in readonly status, or if no key is passed.
     *         File models cannot delete their entire file. A key must
     *         be provided to call this method.
     * @throws \Oroboros\core\exception\model\MissingKeyException
     *         If the specified key does not exist.
     */
    public function delete(string $key = null): \Oroboros\core\interfaces\model\ModelInterface
    {
        if ($this->isReadonly()) {
            throw new \Oroboros\core\exception\model\ReadonlyException(
                    sprintf('Error encountered in [%1$s]. Cannot delete data '
                        . 'because the model is in readonly status.', get_class($this))
            );
        }
        if (!is_null($key)) {
            throw new \Oroboros\core\exception\model\ReadonlyException(
                    sprintf('Error encountered in [%1$s]. '
                        . 'Definer models cannot delete their entire definition. '
                        . 'A key must be supplied for a delete operation.'
                        , get_class($this), $key)
            );
        }
        if (!$this->hasKey($key)) {
            throw new \Oroboros\core\exception\model\MissingKeyException(
                    sprintf('Error encountered in [%1$s]. Supplied key [%2$s] '
                        . 'does not exist in this scope.', get_class($this), $key)
            );
        }
        unset(self::$data[get_class($this)][$key]);
        $this->flagAsModified();
        return $this;
    }

    /**
     * This method will be called when data is committed
     * to apply the changes back to the original file.
     * 
     * Individual implementations must override this to
     * provide logic for saving the data set back to it's
     * original output format.
     * 
     * @return void
     */
    protected function saveDataChanges(): void
    {
        // no-op
        parent::saveDataChanges();
    }

    /**
     * Returns a data key if it exists.
     * 
     * If it does not exist, a `\Oroboros\core\exception\model\MissingKeyException`
     * will be raised.
     * 
     * @param string $key
     * @return mixed
     * @throws \Oroboros\core\exception\model\MissingKeyException
     *         if the specified key does not exist.
     */
    protected function getData(string $key = null)
    {
        if (is_null($key)) {
            return self::$data[get_class($this)];
        }
        if (!$this->hasKey($key)) {
            throw new \Oroboros\core\exception\model\MissingKeyException(
                    sprintf('Error encountered in [%1$s]. Provided data key [%2$s] '
                        . 'does not exist in this scope.', get_class($this) . $key)
            );
        }
        return self::$data[get_class($this)][$key];
    }

    protected function declareDefinerCommand(): ?string
    {
        return $this->getCommand();
    }

    protected function declareDefinerArguments(): ?array
    {
        return $this->getArguments();
    }

    protected function declareDefinerFlags(): ?array
    {
        return $this->getFlags();
    }

    /**
     * Override this method to resolve optional keys.
     * This method will receive an associative array of required keys,
     * where the key is the keyname and the value is acceptable values
     * that can resolve against a resolver object.
     * 
     * This method must return an array that contains
     * all corresponding keys with the appropriate values
     * 
     * Required keys are blocking if not supplied,
     * and will result in an exception from the definer
     * if not defined.
     * 
     * @param array $keys
     * @return array
     */
    protected function resolveRequiredKeys(array $keys): array
    {
        return [];
    }

    /**
     * Override this method to resolve optional keys.
     * This method will receive an associative array of optional keys,
     * where the key is the keyname and the value is acceptable values
     * that can resolve against a resolver object.
     * 
     * This method must return an array that may contain
     * any corresponding key with the appropriate value
     * 
     * Optional keys are non-blocking if not supplied.
     * 
     * @param array $keys
     * @return array
     */
    protected function resolveOptionalKeys(array $keys): array
    {
        return [];
    }

    /**
     * Initializes the definer object.
     * 
     * You may override this method to perform
     * any additional initialization steps
     * 
     * @return void
     */
    protected function initializeDefiner(): void
    {
        if (!is_null($this->definer)) {
            // Already been done
            return;
        }
        $this->definer = $this->load('library', static::DEFINER_CLASS, $this->declareDefinerCommand(), $this->declareDefinerArguments(), $this->declareDefinerFlags());
        foreach ($this->definer->getUnsatisfiedRequiredKeys() as $key => $value) {
            $this->definer->setRequiredKey($key, $value);
        }
        foreach ($this->definer->getUnsatisfiedOptionalKeys() as $key => $value) {
            $this->definer->setOptionalKey($key, $value);
        }
    }

    /**
     * Returns the definer the model internally uses to obtain it's data set
     * 
     * @return \Oroboros\core\interfaces\library\definer\DefinerInterface
     */
    protected function getDefiner(): \Oroboros\core\interfaces\library\definer\DefinerInterface
    {
        return $this->definer;
    }

    /**
     * Compiles the definer data
     * This can be called again later if need be
     * 
     * @return void
     */
    protected function compileData(): void
    {
        $this->initializeDefiner();
        $this->getDefiner()->compile();
        self::$data[get_class($this)] = $this->containerize($this->getDefiner()->fetch()->toArray());
    }

    /**
     * Loads the initial data set if it is not already loaded.
     * 
     * @return void
     */
    private function loadData(): void
    {
        if (is_null(self::$data)) {
            $this->initializeDataContainer();
        }
        if (!self::$data->has(get_class($this))) {
            $data = $this->parseDataSet();
            self::$data[get_class($this)] = $data;
        }
    }

    /**
     * Compiles the definer data and enters it into the model dataset
     * 
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     * @throws \Oroboros\core\exception\model\MissingResourceException
     */
    private function parseDataSet(): \Oroboros\core\interfaces\library\data\DataContainerInterface
    {
        $this->initializeDefiner();
        if (!$this->getDefiner()->canCompile()) {
            throw new \Oroboros\core\exception\model\MissingResourceException(
                    sprintf('Error encountered in [%1$s]. Definer missing required keys  '
                        . '[%2$s].', get_class($this), implode(
                            ', ', array_keys($this->getDefiner()->getUnsatisfiedRequiredKeys())
                        )
                    )
            );
        }
        $this->compileData();
        $data = $this->containerize($this->getDefiner()->fetch()->toArray());
        return $data;
    }

    /**
     * Creates the master container if it does not already exist
     * 
     * @return void
     */
    private function initializeDataContainer(): void
    {
        if (is_null(self::$data)) {
            self::$data = $this->containerizeInto(static::COLLECTION_CLASS);
        }
    }

    /**
     * Verifies that the definer class is properly declared.
     * 
     * @return void
     * @throws \Oroboros\core\exception\core\InvalidClassException
     */
    private function verifyDefiner(): void
    {
        $class = get_class($this);
        $expected = \Oroboros\core\interfaces\library\definer\DefinerInterface::class;
        $classname = $this->getFullClassName('library', static::DEFINER_CLASS);
        if ($classname === false) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. Expected class constant [%2$s] '
                        . 'with value [%3$s] must define a valid class or stub class name '
                        . 'implementing expected interface [%4$s].'
                        , $class, 'DEFINER_CLASS', static::DEFINER_CLASS, $expected));
        }
    }
}
