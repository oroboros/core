<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\module;

/**
 * Abstract Module
 * Provides abstraction for pluggable modules
 *
 * @author Brian Dayhoff
 */
abstract class AbstractModule extends \Oroboros\core\abstracts\pluggable\AbstractPluggable implements \Oroboros\core\interfaces\module\ModuleInterface
{

    use \Oroboros\core\traits\pluggable\JsonConfigUtilityTrait;
    use \Oroboros\core\traits\pluggable\TemplateExtensionSupportTrait;
    use \Oroboros\core\traits\pattern\EventWatcherTrait;
    use \Oroboros\core\traits\pattern\FilterWatcherTrait;

    const CLASS_TYPE = 'module';
    const CLASS_SCOPE = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeFilterWatchList();
        $this->initializeEventWatchList();
        $this->setObserverEvent('ready');
    }
    
    
    public function __destruct()
    {
        $this->setObserverEvent('teardown');
    }

    /**
     * Any additional supporting libraries provided by the module
     * @return array
     */
    public function libraries(): array
    {
        return $this->parseJsonData('libraries');
    }

    /**
     * Any additional models provided by the module
     * @return array
     */
    public function models(): array
    {
        return $this->parseJsonData('models');
    }

    /**
     * Any additional views provided by the module
     * @return array
     */
    public function views(): array
    {
        return $this->parseJsonData('views');
    }

    /**
     * Any additional routes provided by the module
     * @return array
     */
    public function routes(): array
    {
        return $this->parseJsonData('routes');
    }

    /**
     * Any additional services provided by the module
     * @return array
     */
    public function services(): array
    {
        return $this->parseJsonData('services');
    }

    /**
     * Any additional job actions provided by the module
     * @return array
     */
    public function actions(): array
    {
        return $this->parseJsonData('actions');
    }

    /**
     * Any additional script resources provided by the module
     * @return array
     */
    public function scripts(): array
    {
        return $this->parseJsonData('scripts');
    }

    /**
     * Any additional stylesheet resources provided by the module
     * @return array
     */
    public function styles(): array
    {
        return $this->parseJsonData('styles');
    }

    /**
     * Any additional font resources provided by the module
     * @return array
     */
    public function fonts(): array
    {
        return $this->parseJsonData('fonts');
    }

    /**
     * Any additional media assets provided by the module
     * @return array
     */
    public function media(): array
    {
        return $this->parseJsonData('media');
    }

    /**
     * Any additional components provided by the module
     * @return array
     */
    public function components(): array
    {
        return $this->parseJsonData('components');
    }

    /**
     * Any additional templates provided by the module
     * @return array
     */
    public function templates(): array
    {
        $paths = $this->parseJsonData('templates');
        $default_tpl_dir = $this->path() . 'tpl' . DIRECTORY_SEPARATOR;
        if (is_dir($default_tpl_dir)) {
            $paths['default'] = $default_tpl_dir;
        }
        return $paths;
    }

    /**
     * Returns a boolean designation as to whether the module provides
     * resources for the given route.
     * Resources may include scripts, stylesheets, media, fonts, data, or any
     * other relevant resources that the controller may need to obtain to
     * facilitate the module functionality.
     * If this method returns true, the function `getModuleResources` should
     * return a collection of appropriate resources.
     * If this method returns false, then `getModuleResources` should throw
     * an InvalidArgumentException when given the same route.
     * 
     * @param \Oroboros\core\interfaces\library\router\RouteInterface $route
     * @return bool
     */
    public function hasRouteResources(\Oroboros\core\interfaces\library\router\RouteInterface $route): bool
    {
        return false;
    }

    /**
     * Returns a collection of resources to use for the given route.
     * If ANY resources are obtainable for this route, the method `hasRouteResources`
     * should return `true` when given the same route. If no resources exist,
     * `hasRouteResources` should return `false` for the same route, and this
     * method should throw an \InvalidArgumentException.
     * 
     * @param \Oroboros\core\interfaces\library\router\RouteInterface $route
     * @return \Oroboros\core\interfaces\library\container\CollectionInterface
     * @throws \InvalidArgumentException If no resources exist for the given route
     */
    public function getRouteResources(\Oroboros\core\interfaces\library\router\RouteInterface $route): \Oroboros\core\interfaces\library\container\CollectionInterface
    {
        throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. No resources exist for given route in module [%2$s]', get_class($this), $this->name()));
    }
    
    /**
     * Returns a boolean determination as to whether the module has additional
     * menu entries for the given menu id.
     * 
     * @param string $menu
     * @return bool
     */
    public function hasMenuData(string $menu): bool
    {
        return false;
    }
    
    /**
     * Updates the given menu to include the links designated by the module.
     * if `hasMenuSection` returns `false` for the same identifier, this method
     * MUST NOT alter the menu.
     * 
     * @param string $name
     * @param \Oroboros\core\interfaces\library\menu\MenuContainerInterface $menu
     * @return \Oroboros\core\interfaces\library\menu\MenuContainerInterface
     */
    public function updateMenu(string $name, \Oroboros\core\interfaces\library\menu\MenuContainerInterface $menu): \Oroboros\core\interfaces\library\menu\MenuContainerInterface
    {
        if (!$this->hasMenuSection($name)) {
            return $menu;
        }
        return $this->formatMenu($name, $menu);
    }
    
    /**
     * Override this method to perform alterations to menus passed to the module.
     * If any links or sections need to be added to menus, it should be done
     * within this method.
     * 
     * @param string $id
     * @param \Oroboros\core\interfaces\library\menu\MenuContainerInterface $menu
     * @return \Oroboros\core\interfaces\library\menu\MenuContainerInterface
     */
    protected function formatMenu(string $id, \Oroboros\core\interfaces\library\menu\MenuContainerInterface $menu): \Oroboros\core\interfaces\library\menu\MenuContainerInterface
    {
        return $menu;
    }
    
    /**
     * Default module observable name.
     * Default format: `[class type]:[class scope]:[module name]`
     * @return string
     */
    protected function declareObservableName(): string
    {
        return trim(sprintf('%1$s:%2$s', trim(sprintf('%1$s:%2$s', static::CLASS_TYPE, static::CLASS_SCOPE), ':'), $this->name()), ':.');
    }
}
