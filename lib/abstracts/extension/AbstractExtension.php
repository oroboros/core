<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\extension;

/**
 * Abstract Extension
 * Provides abstraction for pluggable extensions
 *
 * @author Brian Dayhoff
 */
abstract class AbstractExtension extends \Oroboros\core\abstracts\pluggable\AbstractPluggable implements \Oroboros\core\interfaces\extension\ExtensionInterface
{

    use \Oroboros\core\traits\pluggable\JsonConfigUtilityTrait;
    use \Oroboros\core\traits\pluggable\TemplateExtensionSupportTrait;
    use \Oroboros\core\traits\pattern\EventWatcherTrait;
    use \Oroboros\core\traits\pattern\FilterWatcherTrait;

    const CLASS_TYPE = 'extension';
    const CLASS_SCOPE = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeFilterWatchList();
        $this->initializeEventWatchList();
        $this->setObserverEvent('ready');
    }
    
    public function __destruct()
    {
        $this->setObserverEvent('teardown');
    }

    /**
     * Any additional supporting libraries provided by the module
     * @return array
     */
    public function libraries(): array
    {
        return $this->parseJsonData('libraries');
    }

    /**
     * Any additional views provided by the module
     * @return array
     */
    public function views(): array
    {
        return $this->parseJsonData('views');
    }

    /**
     * Any additional services provided by the module
     * @return array
     */
    public function services(): array
    {
        return $this->parseJsonData('services');
    }

    /**
     * Any additional job actions provided by the module
     * @return array
     */
    public function actions(): array
    {
        return $this->parseJsonData('actions');
    }

    /**
     * Any additional script resources provided by the module
     * @return array
     */
    public function scripts(): array
    {
        return $this->parseJsonData('scripts');
    }

    /**
     * Any additional stylesheet resources provided by the module
     * @return array
     */
    public function styles(): array
    {
        return $this->parseJsonData('styles');
    }

    /**
     * Any additional font resources provided by the module
     * @return array
     */
    public function fonts(): array
    {
        return $this->parseJsonData('fonts');
    }

    /**
     * Any additional media assets provided by the module
     * @return array
     */
    public function media(): array
    {
        return $this->parseJsonData('media');
    }

    /**
     * Any additional components provided by the module
     * @return array
     */
    public function components(): array
    {
        return $this->parseJsonData('components');
    }

    /**
     * Any additional templates provided by the module
     * @return array
     */
    public function templates(): array
    {
        $paths = $this->parseJsonData('templates');
        $default_tpl_dir = $this->path() . 'tpl' . DIRECTORY_SEPARATOR;
        if (is_dir($default_tpl_dir)) {
            $paths['default'] = $default_tpl_dir;
        }
        return $paths;
    }
    
    protected function declareObservableName(): string
    {
        return trim(sprintf('%1$s:%2$s', trim(sprintf('%1$s:%2$s', static::CLASS_TYPE, static::CLASS_SCOPE), ':'), $this->name()), ':.');
    }
}
