<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\service;

/**
 * Abstract Service
 * Provides abstraction for pluggable services
 *
 * @author Brian Dayhoff
 */
abstract class AbstractService extends \Oroboros\core\abstracts\pluggable\AbstractPluggable implements \Oroboros\core\interfaces\service\ServiceInterface
{

    use \Oroboros\core\traits\pluggable\JsonConfigUtilityTrait;
    use \Oroboros\core\traits\pattern\EventWatcherTrait;
    use \Oroboros\core\traits\pattern\FilterWatcherTrait;

    const CLASS_TYPE = 'service';
    const CLASS_SCOPE = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeFilterWatchList();
        $this->initializeEventWatchList();
        $this->setObserverEvent('ready');
    }
    
    
    public function __destruct()
    {
        $this->setObserverEvent('teardown');
    }

    /**
     * Any additional supporting libraries provided by the service
     * @return array
     */
    public function libraries(): array
    {
        return $this->parseJsonData('libraries');
    }

    /**
     * Any additional models provided by the module
     * @return array
     */
    public function models(): array
    {
        return $this->parseJsonData('models');
    }

    /**
     * Any additional routes provided by the module
     * @return array
     */
    public function routes(): array
    {
        return $this->parseJsonData('routes');
    }

    /**
     * Any filters that should be run against output,
     * and the data definitions that trigger them
     * @return array
     */
    public function filters(): array
    {
        return $this->parseJsonData('filters');
    }

    /**
     * Any hooks that should fire on events,
     * and the event definitions that trigger them
     * @return array
     */
    public function hooks(): array
    {
        return $this->parseJsonData('hooks');
    }

    /**
     * Any drivers that enable accessing additional external resources
     * @return array
     */
    public function drivers(): array
    {
        return $this->parseJsonData('drivers');
    }

    /**
     * Any additional job actions provided by the module
     * @return array
     */
    public function actions(): array
    {
        return $this->parseJsonData('actions');
    }

    /**
     * Any additional components provided by the module
     * @return array
     */
    public function components(): array
    {
        return $this->parseJsonData('components');
    }
    
    protected function declareObservableName(): string
    {
        return trim(sprintf('%1$s:%2$s', trim(sprintf('%1$s:%2$s', static::CLASS_TYPE, static::CLASS_SCOPE), ':'), $this->name()), ':.');
    }
}
