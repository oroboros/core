<?php
namespace Oroboros\core\abstracts\view;

/**
 * Abstract PHP View
 * 
 * Provides abstraction for rendering PHP source code
 *
 * @author Brian Dayhoff
 */
abstract class AbstractPhpView extends AbstractView
{

    /**
     * Defines subclasses as php views.
     * 
     * This is only used to output php source code.
     */
    const CLASS_SCOPE = 'php';

    /**
     * Cli output does not use a mime type, because they are not accessed via http.
     */
    const MIME_TYPE = null;

}
