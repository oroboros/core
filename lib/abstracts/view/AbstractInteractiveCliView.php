<?php
namespace Oroboros\core\abstracts\view;

/**
 * Abstract Interactive Cli View
 * 
 * Provides abstraction for interactive command line shells
 *
 * @author Brian Dayhoff
 */
abstract class AbstractInteractiveCliView extends AbstractCliView implements \Oroboros\core\interfaces\view\InteractiveCliViewInterface
{

    use \Oroboros\core\traits\library\resolver\ResolverUtilityTrait;

    /**
     * Use the default resolver
     */
    const RESOLVER_CLASS = 'resolver\\ArgumentResolver';

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeResolver();
    }

    /**
     * Takes a set of setup parameters represented in a container and creates
     * an interactive shell from them to prompt user input, validate it within
     * a specific acceptable scope, and return the result when it validates.
     * 
     * The given container MUST contain the following keys:
     * 
     * - "valid": designates what qualifies as a valid response. This uses the
     *            resolver api, any format passed that works for resolvers is valid.
     * - "keyname": designates what key the given input applies to. This is
     *              required to pass to the filter method so it can do contextual
     *              lookups based on the relevant key being processed.
     * - "method": designates what prompt method to use to obtain input.
     *             valid values are ["text", "key", and "code"].
     *             "text" will return a line of text entered by the user.
     *             "key" will return a single key pressed by the user as a
     *                   resolved character (only available on unix shells)
     *             "code" will return a single key pressed by the user as
     *                    the keycode of the keypress (only available on unix shells)
     * 
     * The given container MAY contain the following optional keys:
     * 
     * - "title": (string) A title to use for the option displayed.
     * - "content": (string|iterable) Any descriptive text used to designate
     *              what the option being requested is for.
     * - "default": (string) The default value to use if the user returns an
     *              empty string. If this is present and no input is entered,
     *              this value will be used in place of the empty user response.
     * - "options": (\Oroboros\core\interfaces\library\container\ContainerInterface)
     *              A list of predefined options, where the key is the option to
     *              match response text to, and the value is the descriptive text
     *              of the option. These options will not be enforced internally,
     *              they simply indicate any special options that may be handled
     *              by program logic internally a bit differently than a literal
     *              text string. Only the "exit" option will be handled within
     *              the view, all others are expected to be handled by the controller
     *              or component referencing this object.
     * "typecast": (string|false) designates whether or not to resolve the
     *             returned user input into the variable type it represents.
     *             If false, it will be left as a string. If a string, it must
     *             be a valid php variable type. By default, 'string', 'integer',
     *             'boolean', 'array' will be handled (arrays split on commas and
     *             trim whitespace from results)
     *             Implementations may extend upon this to accept additional types
     *             that they provide logic to resolve to (eg: directory, interface,
     *             classname, etc).
     * 
     * Other keynames may be present but will be ignored if they are
     * not required by implementations. Implementations should not fail
     * due to the presence of keys they do not use, which allows the option
     * definition to remain portable between the view and other use cases.
     * 
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $opt
     * @return mixed
     * @throws \Oroboros\core\exception\ExitException
     *         If the user enters "exit" as their response, indicating they wish
     *         to terminate the interactive shell.
     * @throws \Oroboros\core\exception\ErrorException
     *         If the option passed is malformed. This indicates the passed
     *         configuration needs to be adjusted.
     * @throws \Oroboros\core\exception\LogicException
     *         If the flags passed to the view indicate that there should not
     *         be visible output. Interactive shells require visible output,
     *         and unit testing requires extension classes honor their
     *         underlying methods cleanly. If the "quiet" or "silent" flag was
     *         passed, this method cannot resolve.
     */
    public function getOpt(\Oroboros\core\interfaces\library\container\ContainerInterface $opt)
    {
        if (($this->hasFlag('quiet') && $this->getFlag('quiet') === true) || ($this->hasFlag('silent') && $this->getFlag('silent') === true)) {
            throw new \Oroboros\core\exception\LogicException(
                    sprintf('Error encountered in [%1$s]. Interactive shell requires '
                        . 'output, but flags passed indicate output should be silenced. '
                        . 'Method [%2$s] cannot resolve.', get_class($this), __METHOD__)
            );
        }
        $this->validateOption($opt);
        $valid = false;
        $error_msg = false;
        while ($valid === false) {
            if (!($this->hasFlag('debug') && $this->getFlag('debug') === true)) {
                // Only clear the screen if debug is not enabled.
                $this->clear();
            }
            if ($opt->has('title')) {
                $this->renderOptionTitle($opt['title']);
            }
            if ($opt->has('content')) {
                $this->renderOptionContent($opt['content']);
            }
            if ($opt->has('options')) {
                $this->renderOptionOptions($opt['options']);
            }
            if ($error_msg !== false) {
                $this->renderOptionError($error_msg);
                $error_msg = false;
            }
            try {
                $value = $this->promptOptionValue($opt['method']);
                $value = $this->filterOptValue($opt['keyname'], $value, $opt);
                $this->validateOptionValue($opt['keyname'], $value, $opt['valid']);
                // Break the loop
                $valid = true;
            } catch (\Oroboros\core\interfaces\exception\BreakpointExceptionInterface $e) {
                // Recoverable error in the prompt method
                $error_msg = $e->getMessage();
            } catch (\Oroboros\core\exception\InvalidArgumentException $e) {
                // Format did not pass validation.
                $error_msg = $e->getMessage();
            }
        }
        return $value;
    }

    /**
     * Prints a title for an interactive response option.
     * 
     * @param string $title
     * @return void
     */
    protected function renderOptionTitle(string $title): void
    {
        $start = $this->getFormatCode('red') . $this->getFormatCode('bold');
        $end = $this->getFormatCode('reset');
        $this->raw(sprintf('%1$s%2$s%3$s%4$s', $start, $title, $end, PHP_EOL));
    }

    /**
     * Prints a content line for an interactive response option.
     * 
     * @param string $content
     * @return void
     */
    protected function renderOptionContent($content): void
    {
        if (is_iterable($content)) {
            foreach ($content as $line) {
                $this->print($line);
            }
        } else {
            $this->print($content);
        }
        $this->raw('');
    }

    /**
     * Prints a list of pre-existing valid options for an interactive response option.
     * 
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $options
     * @return void
     */
    protected function renderOptionOptions(\Oroboros\core\interfaces\library\container\ContainerInterface $options): void
    {
        $len = 0;
        $this->print(sprintf('    Options:%1$s%2$s', PHP_EOL, '    ----------------------------------'));
        $keys = array_keys($options->toArray());
        foreach ($keys as $key) {
            if ((strlen($key) + 2 - $len) > $len) {
                $len = strlen($key) + 2 - $len;
            }
        }
        foreach ($options as $key => $value) {
            $this->print($this->leftPad(sprintf('[%1$s]%2$s| %3$s', $key, $this->rightPad('', $len - strlen($key)), $value), 4));
        }
        $this->raw('');
    }

    /**
     * Renders an option error if the user input was malformed.
     * 
     * @param string $error
     * @return void
     */
    protected function renderOptionError(string $error): void
    {
        $this->error($error);
    }

    /**
     * Prompts the user for input and returns the corresponding string
     * This can return a typed line of text if 
     * 
     * @param string $method Must be one of `["text", "key", "code"]`
     * @return string
     * @throws \Oroboros\core\exception\core\InvalidClassException
     *         If the `$method` is not one of `["text", "key", "code"]`.
     *         This should never occur, because the opt preflight should pick up
     *         this problem before it gets to here. If it gets this far, that
     *         means some child class broke the option validation, which introduces
     *         dangerous and unexpected consequences.
     * @throws \Oroboros\core\exception\cli\ExitException If the user indicates
     *         they wish to cancel the interactive shell. This occurs when the
     *         string "exit" is typed in text mode, or when ESC is pressed in key
     *         or code mode.
     */
    protected function promptOptionValue(string $method): string
    {
        switch ($method) {
            case 'text':
                $value = $this->read('Please enter your selection, or [exit] to quit.');
                break;
            case 'code':
            case 'key':
                $value = $this->getKeyCode('Please enter your selection, or [ESC] to quit.');
                $this->raw('');
                if ($value === '27') {
                    $value = 'exit';
                    break;
                }
                if ($method === 'key') {
                    $value = $this->resolveKeyCode($value);
                }
                break;
            default:
                throw new \Oroboros\core\exception\core\InvalidClassException(
                        sprintf('Error encountered in [%1$s]. Invalid user prompt '
                            . 'option [%2$] was not handled by option preflight. '
                            . 'This class is not useable in it\'s current state.'
                            , get_class($this), $method)
                );
        }
        if ($value === 'exit') {
            // User aborted interactive shell
            throw new \Oroboros\core\exception\cli\ExitException();
        }
        return $value;
    }

    /**
     * Prompts the user for a string of text.
     * 
     * @param string $message an optional replacement for the default message.
     * @return string
     */
    protected function promptTextInput(string $message = null): string
    {
        if (is_null($message)) {
            $message = 'Please enter your selection, or [exit] to quit.';
        }
        return $this->read($message);
    }

    /**
     * Prompts the user for a keypress.
     * 
     * @param string $message
     * @return string
     */
    protected function promptCodeInput(string $message = null): string
    {
        if (is_null($message)) {
            $message = 'Please enter your selection, or [ESC] to quit.';
        }
        return $this->getKeyCode($message);
    }

    protected function declareRequiredOptions(): array
    {
        return [
            // What formats should be valid as per resolver api for the user input
            'valid' => ['string', 'array', 'Oroboros\\core\\interfaces\\library\\container\\ContainerInterface'],
            // identifying key of the option
            'keyname' => 'string',
            // what response format to use to prompt user interaction
            'method' => ['text', 'code', 'key'],
        ];
    }

    protected function declareOptionalOptions(): array
    {
        return [
            // Title of the option
            'title' => 'string',
            // Content fields describing the option
            'content' => ['string', 'array', 'Oroboros\\core\\interfaces\\library\\container\\ContainerInterface'],
            // Default value of an empty response
            'default' => [null, 'string', 'boolean', 'array', 'integer'],
            // Preset options to show the user
            'options' => ['Oroboros\\core\\interfaces\\library\\container\\ContainerInterface'],
            // Whether to typecast the user input into an expected format
            'typecast' => [false, 'string', 'integer', 'boolean', 'array'],
        ];
    }

    protected function validateOption(\Oroboros\core\interfaces\library\container\ContainerInterface $opt): void
    {
        foreach ($this->declareRequiredOptions() as $key => $format) {
            if (!$opt->has($key)) {
                throw new \Oroboros\core\exception\ErrorException(
                        sprintf('Error encountered in [%1$s]. Required option key '
                            . '[%2$s] was not detected in the given option '
                            . 'definition.', get_class($this), $key)
                );
            }
            if (!$this->getResolver()->resolve($opt[$key], $format)) {
                throw new \Oroboros\core\exception\ErrorException(
                        sprintf('Error encountered in [%1$s]. Required option definition key '
                            . '[%2$s] does not follow the correct format.', get_class($this), $key)
                );
            }
        }
        foreach ($this->declareOptionalOptions() as $key => $format) {
            if (!$opt->has($key)) {
                continue;
            }
            if (!$this->getResolver()->resolve($opt[$key], $format)) {
                throw new \Oroboros\core\exception\ErrorException(
                        sprintf('Error encountered in [%1$s]. Optional option definition key '
                            . '[%2$s] does not follow the correct format.', get_class($this), $key)
                );
            }
        }
    }

    /**
     * Validates a user returned argument.
     * Throws an `\Oroboros\core\exception\InvalidArgumentException`
     * if it does not pass validation.
     * 
     * Default behavior defers to the declared resolver class
     * defined by class constant `RESOLVER_CLASS`
     * 
     * If you need more robust error checking methods,
     * you should override this method for custom checking.
     * 
     * @note This method fires AFTER `filterOptionValue`,
     *       so it will still validate transformed arguments.
     *       Transformation filters should not operate with the assumption
     *       that the value is correct. If it is not, return a non-validating
     *       value.
     * 
     * @param mixed $value
     * @param mixed $valid
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException if the argument is not valid
     */
    protected function validateOptionValue(string $key, $value = null, $valid = null): void
    {
        if (!$this->getResolver()->resolve($value, $valid)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('[%1$s] is not a valid value.', $value));
        }
    }

    protected function filterTypecasting(string $typecast, $value)
    {
        $result = $value;
        switch ($typecast) {
            case 'string':
                $result = (string) $value;
                break;
            case 'boolean':
                if (in_array($value, ["0", "false", 0, false])) {
                    return false;
                    break;
                }
                $result = true;
                break;
            case 'integer':
                $result = intval($value);
                break;
            case 'array':
                $result = explode(',', $value);
                foreach ($result as $k => $v) {
                    $result[$k] = trim($v, ' ');
                }
                break;
            default:
                throw new \Oroboros\core\exception\ErrorException(
                        sprintf('Error encountered in [%1$s]. Provided typecast definition [%2$s] is not recognized.', get_class($this), $typecast)
                );
        }
        return $result;
    }

    /**
     * Performs any transformations on the user input value to convert it into
     * expected values to hand back to the controller or component referencing this view.
     * 
     * By default, this will resolve empty strings into the default if one is declared,
     * and resolve typecasting if the typecasting option is declared.
     * 
     * Child classes may override this method to perform additional transformations
     * based on the specified key or output type.
     * 
     * @param string $key the keyname of the given value option
     * @param type $value the value supplied by user input
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $definition
     *        The full set of options used to define the option requested from the user,
     *        in the event that additional logic needs to perform custom lookups.
     *        Also used to check the default value and typecasting settings.
     * @return mixed
     */
    protected function filterOptValue(string $key, $value, \Oroboros\core\interfaces\library\container\ContainerInterface $definition)
    {
        if ($definition->has('default') && $value === "") {
            $value = $definition['default'];
        }
        if ($definition->has('typecast') && $definition['typecast'] !== false) {
            $value = $this->filterTypecasting($definition['typecast'], $value);
        }
        return $value;
    }
}
