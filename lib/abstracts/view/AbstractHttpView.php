<?php
namespace Oroboros\core\abstracts\view;

/**
 * Abstract Http View
 * 
 * Provides abstraction for all http response rendering
 *
 * @author Brian Dayhoff
 */
abstract class AbstractHttpView extends AbstractView
{

    /**
     * Defines subclasses as http-type views.
     */
    const CLASS_SCOPE = 'http';

    /**
     * Http views require a mime type, but it must be defined by further abstraction.
     */
    const MIME_TYPE = null;

    /**
     * The response object used to package the output to return to the client.
     * @var \Psr\Http\Message\ResponseInterface
     */
    private $response = null;

    /**
     * The HTTP Status code to emit with the response
     * @var int
     */
    private $status_code = 200;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        $this->verifyMimeType();
        parent::__construct($command, $arguments, $flags);
        $this->initializeResponseObject();
    }

    public function render(string $template, array $data = null)
    {
        if (is_null($data)) {
            $data = $this->getArgument('data');
        } else {
            $data = $this::containerize($data);
        }
        $page_template = static::CLASS_SCOPE . DIRECTORY_SEPARATOR . 'page/html';
        if ($data['context'] instanceof \Oroboros\core\interfaces\library\container\ContainerInterface) {
            $context = $data['context']->toArray();
        } elseif (!is_array($data['context'])) {
            $context = ['content' => $data['context']];
        } else {
            $context = $data['context'];
        }
        $context['template'] = $template;
        $data['output'] = parent::render($page_template, $context);
        return $this->packageResponseObject($data);
    }

    /**
     * Tells the view to issue the response as a download instead of rendering in the browser.
     * A filename must be supplied when a download is initialized.
     * Otherwise functionally equivalent to `render`.
     * @param string $filename
     * @param string $template
     * @param array $data
     * @return $this (method chainable)
     */
    public function download(string $filename, string $template, array $data = array())
    {
        $this->setDownloadHeaders($filename);
        return $this->render($template, $data);
    }

    protected function withStatusCode(int $code = 200)
    {
        $this->status_code = $code;
        $this->response = $this->response->withStatus($code);
        return $this;
    }

    protected function withHeader(string $name, string $value)
    {
        $this->response = $this->response->withAddedHeader($name, $value);
        return $this;
    }

    protected function withoutHeader(string $name)
    {
        $this->response = $this->response->withoutHeader($name);
        return $this;
    }

    protected function withBody(string $body)
    {
        $stream = fopen('php://memory', 'w+b');
        fwrite($stream, $body);
        $output = new \Zend\Diactoros\Stream($stream);
        $this->response = $this->response->withBody($output);
        return $this;
    }

    protected function withProtocol(string $protocol = '1.1')
    {
        $this->response->validateProtocolVersion($protocol);
        $this->response = $this->response->withProtocolVersion($protocol);
        return $this;
    }

    /**
     * Sets all of the appropriate download headers for a stream download of the body.
     * The body stream will be utilized to get the output file size,
     * so it does not need to be provided.
     * @param string $filename The name of the file to send
     */
    private function setDownloadHeaders(string $filename)
    {
        $quoted = sprintf('"%s"', addcslashes($filename, '"\\'));
        $size = $this->response->getBody()->getSize();
        $this->withHeader('Content-Description', 'File Transfer')
            ->withHeader('Content-Type', 'application/octet-stream')
            ->withHeader('Content-Disposition', 'attachment; filename=' . $quoted)
            ->withHeader('Content-Transfer-Encoding', 'binary')
            ->withHeader('Connection', 'Keep-Alive')
            ->withHeader('Expires', '0')
            ->withHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0')
            ->withHeader('Pragma', 'public')
            ->withHeader('Content-Length', $size);
    }

    private function initializeResponseObject()
    {
        $response = new \Zend\Diactoros\Response();
        $this->response = $response->withStatus($this->status_code);
        $this->withHeader('Content-Type', static::MIME_TYPE);
        $this->withHeader('X-Content-Type-Options', 'nosniff');
        $this->withHeader('X-Frame-Options', 'SAMEORIGIN');
        $this->withHeader('Access-Control-Allow-Origin', $this::HOSTNAME);
        $this->withHeader('Access-Control-Allow-Methods', 'GET');
        $this->withHeader('Access-Control-Max-Age', '86400');
        $this->withHeader('Access-Control-Allow-Headers', '*');
        $this->withHeader('Vary', 'Accept-Encoding, Origin');
    }

    private function verifyMimeType()
    {
        if (is_null(static::MIME_TYPE)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(sprintf('Class [%1$s] is invalid. '
                        . 'Http views must define class constant [%2$s] with a valid mime type.'), get_class($this), 'MIME_TYPE');
        }
    }

    private function packageResponseObject(\Oroboros\core\interfaces\library\container\ResponseContainerInterface $output)
    {
        $this->withBody($output->get('output'));
        $this->withStatusCode($output->get('code'));
        foreach ($output->get('header') as $key => $value) {
            $this->withHeader($key, $value);
        }
        return $this->response;
    }
}
