<?php
namespace Oroboros\core\abstracts\view;

/**
 * Abstract Json View
 * 
 * Provides abstraction for json response rendering over http
 *
 * @author Brian Dayhoff
 */
abstract class AbstractJsonView extends AbstractPlaintextView
{

    /**
     * Defines subclasses as json views.
     * 
     * This is only used to output json data.
     */
    const CLASS_SCOPE = 'json';

    /**
     * The output header for all content returned by this view
     */
    const MIME_TYPE = 'application/json; charset=utf-8';

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
    }

    public function render(string $template, array $data = null)
    {
        if (is_null($data)) {
            $data = $this->getArgument('data');
        } else {
            $data = $this::containerize($data);
        }
        if ($data->has('context')) {
            $data['context'] = json_encode($data['context']);
        }
        return parent::render($template, $data->toArray());
    }
}
