<?php
namespace Oroboros\core\abstracts\view;

/**
 * Abstract View
 * 
 * Sets the abstract foundation for how
 * views render relevant details.
 *
 * @author Brian Dayhoff
 */
abstract class AbstractView extends \Oroboros\core\abstracts\AbstractBase implements \Oroboros\core\interfaces\view\ViewInterface
{

    use \Oroboros\core\traits\LoaderTrait;
    use \Oroboros\core\traits\ContainerPackagerUtility;
    use \Oroboros\core\traits\StringUtilityTrait;
    use \Oroboros\core\traits\pattern\ObservableTrait;
    use \Oroboros\core\traits\pattern\ObserverTrait;
    use \Oroboros\core\traits\pattern\EventAwareTrait;
    use \Oroboros\core\traits\pattern\FilterAwareTrait;

    /**
     * Designates further child classes as views.
     */
    const CLASS_TYPE = 'view';

    /**
     * Class scope should be defined by subclasses.
     */
    const CLASS_SCOPE = null;

    /**
     * Use the default container
     */
    const CONTAINER_CLASS = 'Oroboros\\core\\library\\container\\ResponseContainer';

    /**
     * Defines the valid view scopes known to the system
     * @var array
     */
    private static $valid_class_scopes = [
        \Oroboros\core\abstracts\view\AbstractPlaintextView::CLASS_SCOPE,
        \Oroboros\core\abstracts\view\AbstractHttpView::CLASS_SCOPE,
        \Oroboros\core\abstracts\view\AbstractHtmlView::CLASS_SCOPE,
        \Oroboros\core\abstracts\view\AbstractCliView::CLASS_SCOPE,
        \Oroboros\core\abstracts\view\AbstractCssView::CLASS_SCOPE,
        \Oroboros\core\abstracts\view\AbstractJavascriptView::CLASS_SCOPE,
        \Oroboros\core\abstracts\view\AbstractPhpView::CLASS_SCOPE,
        \Oroboros\core\abstracts\view\AbstractJsonView::CLASS_SCOPE,
        \Oroboros\core\abstracts\view\AbstractXmlView::CLASS_SCOPE,
    ];

    /**
     * The templating engine used to render output
     * @var Oroboros\core\interfaces\library\template\TemplateInterface
     */
    private $template_engine = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        $this->verifyViewScope();
        parent::__construct($command, $arguments, $flags);
        $this->initializeObservable();
        $this->initializeObserver();
        $this->initializeEvents();
        $this->initializeFilters();
        $expected = 'Oroboros\core\interfaces\library\template\TemplateInterface';
        if (!( $this->hasArgument('template-engine') && is_object($this->getArgument('template-engine')) && in_array($expected, class_implements($this->getArgument('template-engine')))
            )
        ) {
            throw new \Oroboros\core\exception\ErrorException('Error encountered in [%1$s]. '
                    . 'View must be provided with a template engine of '
                    . 'type [%2$s] on instantiation.', get_class($this), $expected);
        }
        $this->setTemplateEngine($this->getArgument('template-engine'));
        $this->setObserverEvent('ready');
        $this->getLogger()->debug('[type][scope][class] Initialized view.', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
        ]);
    }

    public function __destruct()
    {
        $this->getLogger()->debug('[type][scope][class] Fired view destructor.', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
        ]);
        parent::__destruct();
    }

    public function render(string $template, array $data = null)
    {
        if (is_null($data)) {
            $data = $this->getArgument('data');
        } else {
            $data = $this::containerize($data);
        }
        try {
            $renderer = $this->getTemplate($template);
            $renderer->setData($data);
            $output = $renderer->renderTemplate();
            $this->setObserverEvent('render');
            $this->getLogger()->debug('[type][scope][class] Rendering view output.', [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
            ]);
            return $output;
        } catch (\Exception $e) {
            $this->getLogger()->warning(
                sprintf('[type][scope][class] Render error of type [error-type] '
                    . 'at line [error-line] of file [error-file]'
                    . '%1$s%2$s- With message [error-message]'
                    . '%1$s%2$s- Trace: [error-trace].', PHP_EOL, '    '), [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
                'error-type' => get_class($e),
                'error-file' => $e->getFile(),
                'error-line' => $e->getLine(),
                'error-message' => $e->getMessage(),
                'error-trace' => $e->getTraceAsString(),
            ]);
            $this->setObserverEvent('render-error');
            throw $e;
        }
    }

    /**
     * Dependency Injection method for the template engine.
     * @param \Oroboros\core\interfaces\library\template\TemplateInterface $template_engine
     */
    protected function setTemplateEngine(\Oroboros\core\interfaces\library\template\TemplateInterface $template_engine)
    {
        $this->template_engine = $template_engine;
        $this->getLogger()->debug('[type][scope][class] Template engine registered as [engine].', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'engine' => get_class($template_engine),
        ]);
        $this->setObserverEvent('set-template-engine');
    }

    /**
     * Returns an instance of the template object that will handle the 
     * parameter injection and output of the specified content.
     * 
     * The returned template object should be pre-initialized
     * to the desired template to output.
     * 
     * @return \Oroboros\core\interfaces\library\template\TemplateInterface
     */
    protected function getTemplate(string $template)
    {
        return $this->initializeTemplateEngine($template);
    }

    /**
     * Sets the observable name for the view
     * @return string
     */
    protected function declareObservableName(): string
    {
        return trim(sprintf('%1$s:%2$s', static::CLASS_TYPE, static::CLASS_SCOPE), ':');
    }

    private function initializeTemplateEngine(string $template)
    {
        if (is_null($this->template_engine)) {
            $expected = 'Oroboros\core\interfaces\library\template\TemplateInterface';
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. '
                        . 'No template engine has been declared. Expected instance of [%2$s]', get_class($this), $expected));
        }
        $this->getLogger()->debug('[type][scope][class] Initializing template engine [engine] with template [template]', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'engine' => get_class($this->template_engine),
            'template' => $template,
        ]);
        $this->template_engine->setTemplate($template);
        return $this->template_engine;
    }

    private function verifyViewScope()
    {
        if (is_null(static::CLASS_SCOPE)) {
            throw new \OutOfRangeException(sprintf('Class [%1$s] is invalid. '
                        . 'Class constant [%2$s] must be defined as a known valid '
                        . 'view scope [%3$s].', get_class($this), 'CLASS_SCOPE', implode(', ', self::$valid_class_scopes)));
        }
    }
}
