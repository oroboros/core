<?php
namespace Oroboros\core\abstracts\view;

/**
 * Abstract Html View
 * 
 * Provides abstraction for HTML page rendering
 *
 * @author Brian Dayhoff
 */
abstract class AbstractHtmlView extends AbstractHttpView
{

    /**
     * Defines subclasses as html-type views.
     */
    const CLASS_SCOPE = 'html';

    /**
     * The output header for all content returned by this view
     */
    const MIME_TYPE = 'text/html; charset=utf-8';

}
