<?php
namespace Oroboros\core\abstracts\view;

/**
 * Abstract Css View
 * 
 * Provides abstraction for dynamically generated css responses over http
 *
 * @author Brian Dayhoff
 */
abstract class AbstractCssView extends AbstractPlaintextView
{

    /**
     * Defines subclasses as html-type views.
     */
    const CLASS_SCOPE = 'css';

    /**
     * The output header for all content returned by this view
     */
    const MIME_TYPE = 'text/css; charset=utf-8';

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
    }
}
