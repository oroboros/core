<?php
namespace Oroboros\core\abstracts\view;

/**
 * Abstract Plaintext View
 * 
 * Provides abstraction for plaintext rendering
 *
 * @author Brian Dayhoff
 */
abstract class AbstractPlaintextView extends AbstractHttpView
{

    /**
     * Defines subclasses as plaintext-type views.
     */
    const CLASS_SCOPE = 'plaintext';

    /**
     * The output header for all content returned by this view
     */
    const MIME_TYPE = 'text/plain; charset=utf-8';

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->setTemplateEngine($this->load('library', 'template\\raw\\Raw'));
    }
}
