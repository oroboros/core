<?php
namespace Oroboros\core\abstracts\view;

/**
 * Abstract Html View
 * 
 * Provides abstraction for HTML page rendering
 *
 * @author Brian Dayhoff
 */
abstract class AbstractCliView extends AbstractView implements \Oroboros\core\interfaces\view\CliViewInterface
{

    use \Oroboros\core\traits\cli\CliOutputUtilityTrait;
    use \Oroboros\core\traits\cli\CliProcessUtilityTrait;
    use \Oroboros\core\traits\cli\CliSystemUtilityTrait;

    /**
     * Defines subclasses as cli-type views.
     * 
     * This is similar to plaintext, but also has support for console color commands,
     * and other utilites common on the command line which are not relevant
     * in standard plaintext format.
     */
    const CLASS_SCOPE = 'cli';

    /**
     * Cli output does not use a mime type, because they are not accessed via http.
     */
    const MIME_TYPE = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializePid();
        $this->initializeOsVersion();
        $this->initializeIOStreams();
    }
    
    /**
     * Clears the console.
     * Returns self for method chaining.
     * 
     * @return \Oroboros\core\interfaces\view\CliViewInterface
     */
    public function clear(): \Oroboros\core\interfaces\view\CliViewInterface
    {
        $this->clear_screen();
        return $this;
    }
    
    /**
     * Prints a message in raw format, with the option to write to either stdout or stderr
     * If the quiet flag or silent flag were passed on view instantiation, these will not be displayed.
     * Returns self for method chaining.
     * 
     * @param string $message
     * @param string $stream
     * @return \Oroboros\core\interfaces\view\CliViewInterface
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    public function raw(string $message, string $stream = 'stdout'): \Oroboros\core\interfaces\view\CliViewInterface
    {
        $valid_streams = ['stderr', 'stdout'];
        if (!in_array($stream, $valid_streams)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                sprintf('Error encountered in [%1$s]. Invalid stream argument passed. Valid streams are [%2$s].', get_class($this), implode(', ', $valid_streams))
            );
        }
        if (($this->hasFlag('quiet') && $this->getFlag('quiet') === true) || ($this->hasFlag('silent') && $this->getFlag('silent') === true)) {
            return $this;
        }
        $this->print_raw($message, $stream);
        return $this;
    }
    
    /**
     * Prints a message in standard console format to stdout
     * If the quiet flag or silent flag were passed on view instantiation, these will not be displayed.
     * Returns self for method chaining.
     * 
     * @param string $message
     * @return \Oroboros\core\interfaces\view\CliViewInterface
     */
    public function print(string $message): \Oroboros\core\interfaces\view\CliViewInterface
    {
        if (($this->hasFlag('quiet') && $this->getFlag('quiet') === true) || ($this->hasFlag('silent') && $this->getFlag('silent') === true)) {
            return $this;
        }
        $this->print_message($message);
        return $this;
    }
    
    /**
     * Prints a message indicating a successful operation in console format to stdout
     * If the quiet flag or silent flag were passed on view instantiation, these will not be displayed.
     * Returns self for method chaining.
     * 
     * @param string $message
     * @return \Oroboros\core\interfaces\view\CliViewInterface
     */
    public function success(string $message): \Oroboros\core\interfaces\view\CliViewInterface
    {
        if (($this->hasFlag('quiet') && $this->getFlag('quiet') === true) || ($this->hasFlag('silent') && $this->getFlag('silent') === true)) {
            return $this;
        }
        $this->success_message($message);
        return $this;
    }
    
    /**
     * Prints a message in debug console format to stdout
     * If a debug flag was not passed on view instantiation, or if the quiet flag
     * or silent flag were passed on view instantiation, these will not be displayed.
     * Returns self for method chaining.
     * 
     * @param string $message
     * @return \Oroboros\core\interfaces\view\CliViewInterface
     */
    public function debug(string $message): \Oroboros\core\interfaces\view\CliViewInterface
    {
        if (!($this::app()->debugEnabled()) || ($this->hasFlag('quiet') && $this->getFlag('quiet') === true) || ($this->hasFlag('silent') && $this->getFlag('silent') === true)) {
            return $this;
        }
        $this->debug_message($message);
        return $this;
    }
    
    /**
     * Prints a message in info console format to stdout
     * If the quiet flag or silent flag were passed on view instantiation, these will not be displayed.
     * Returns self for method chaining.
     * 
     * @param string $message
     * @return \Oroboros\core\interfaces\view\CliViewInterface
     */
    public function info(string $message): \Oroboros\core\interfaces\view\CliViewInterface
    {
        if (($this->hasFlag('quiet') && $this->getFlag('quiet') === true) || ($this->hasFlag('silent') && $this->getFlag('silent') === true)) {
            return $this;
        }
        $this->info_message($message);
        return $this;
    }
    
    /**
     * Prints a message in notice console format to stdout
     * If the quiet flag or silent flag were passed on view instantiation, these will not be displayed.
     * Returns self for method chaining.
     * 
     * @param string $message
     * @return \Oroboros\core\interfaces\view\CliViewInterface
     */
    public function notice(string $message): \Oroboros\core\interfaces\view\CliViewInterface
    {
        if (($this->hasFlag('quiet') && $this->getFlag('quiet') === true) || ($this->hasFlag('silent') && $this->getFlag('silent') === true)) {
            return $this;
        }
        $this->notice_message($message);
        return $this;
    }
    
    /**
     * Prints a message in warning message in low priority error format to stderr
     * If the silent flag was passed on view instantiation, these will not be displayed.
     * They will still display if the quiet flag is passed.
     * Returns self for method chaining.
     * 
     * @param string $message
     * @return \Oroboros\core\interfaces\view\CliViewInterface
     */
    public function warning(string $message): \Oroboros\core\interfaces\view\CliViewInterface
    {
        if (($this->hasFlag('silent') && $this->getFlag('silent') === true)) {
            return $this;
        }
        $this->warning_message($message);
        return $this;
    }
    
    /**
     * Prints a message in error console format to stderr
     * If the silent flag was passed on view instantiation, these will not be displayed.
     * They will still display if the quiet flag is passed.
     * Returns self for method chaining.
     * 
     * @param string $message
     * @return \Oroboros\core\interfaces\view\CliViewInterface
     */
    public function error(string $message): \Oroboros\core\interfaces\view\CliViewInterface
    {
        if (($this->hasFlag('silent') && $this->getFlag('silent') === true)) {
            return $this;
        }
        $this->error_message($message);
        return $this;
    }
    
    /**
     * Prints a message in elevated priority error console format to stderr
     * If the silent flag was passed on view instantiation, these will not be displayed.
     * They will still display if the quiet flag is passed.
     * Returns self for method chaining.
     * 
     * @param string $message
     * @return \Oroboros\core\interfaces\view\CliViewInterface
     */
    public function critical(string $message): \Oroboros\core\interfaces\view\CliViewInterface
    {
        if (($this->hasFlag('silent') && $this->getFlag('silent') === true)) {
            return $this;
        }
        $this->critical_message($message);
        return $this;
    }
    
    /**
     * Prints a message in high priority error console format to stderr
     * If the silent flag was passed on view instantiation, these will not be displayed.
     * They will still display if the quiet flag is passed.
     * Returns self for method chaining.
     * 
     * @param string $message
     * @return \Oroboros\core\interfaces\view\CliViewInterface
     */
    public function alert(string $message): \Oroboros\core\interfaces\view\CliViewInterface
    {
        if (($this->hasFlag('silent') && $this->getFlag('silent') === true)) {
            return $this;
        }
        $this->alert_message($message);
        return $this;
    }
    
    /**
     * Prints a message in emergency maximum priority error console format to stderr
     * If the silent flag was passed on view instantiation, these will not be displayed.
     * They will still display if the quiet flag is passed.
     * Returns self for method chaining.
     * 
     * @param string $message
     * @return \Oroboros\core\interfaces\view\CliViewInterface
     */
    public function emergency(string $message): \Oroboros\core\interfaces\view\CliViewInterface
    {
        if (($this->hasFlag('silent') && $this->getFlag('silent') === true)) {
            return $this;
        }
        $this->emergency_message($message);
        return $this;
    }

    /**
     * Prompts a shell user for input and returns a string representing their reply.
     * 
     * @param string $message If supplied, will print a message on the line preceding the prompt.
     * @param string $prefix Prints a prefix for the input field.
     * @param bool $secure
     * @return string
     */
    public function read(string $message = null, string $prefix = ' > ', bool $secure = false): string
    {
        if (!is_null($message)) {
            $this->print_message($message);
        }
        return $this->read_input($prefix, $secure);
    }

    /**
     * Returns a single keypress character.
     * Only works on unix systems.
     * 
     * @param string $message
     * @param string $prefix
     * @param bool $secure
     * @return string
     */
    public function getKey(string $message = null, string $prefix = ' > ', bool $secure = false): string
    {
        return $this->resolveKeyCode($this->getKeyCode($message, $prefix, $secure));
    }

    /**
     * Returns the keycode of a single keypress only.
     * Only works on unix systems.
     * 
     * @param string $message
     * @param string $prefix
     * @param bool $secure
     * @return string
     */
    public function getKeyCode(string $message = null, string $prefix = ' > ', bool $secure = false): string
    {
        if (!is_null($message)) {
            $this->print_message($message);
        }
        return $this->read_keypress($prefix, $secure);
    }

    /**
     * Resolves a given keycode into the character it represents
     * 
     * @param string $code
     * @return string
     */
    public function resolveKeyCode(string $code): ?string
    {
        return chr(intval($code));
    }
    
    /**
     * Gets the literal value of a given format code key.
     * 
     * @param string $code
     * @return string
     */
    protected function getFormatCode(string $code = null): string
    {
        
        if ($this->hasFlag('no-colors')) {
            return "";
        }
        if (is_null($code)) {
            return "";
        }
        $this->verifyFormatKey($code);
        return self::$format_codes[$code];
    }
}
