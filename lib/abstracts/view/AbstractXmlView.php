<?php
namespace Oroboros\core\abstracts\view;

/**
 * Abstract Plaintext View
 * 
 * Provides abstraction for xml rendering
 *
 * @author Brian Dayhoff
 */
abstract class AbstractXmlView extends AbstractPlaintextView
{

    /**
     * Defines subclasses as plaintext-type views.
     */
    const CLASS_SCOPE = 'xml';

    /**
     * The output header for all content returned by this view
     */
    const MIME_TYPE = 'application/xml; charset=utf-8';

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
    }
}
