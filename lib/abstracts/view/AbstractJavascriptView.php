<?php
namespace Oroboros\core\abstracts\view;

/**
 * Abstract Json View
 * 
 * Provides abstraction for dynamically generated javascript response rendering over http
 *
 * @author Brian Dayhoff
 */
abstract class AbstractJavascriptView extends AbstractPlaintextView
{

    /**
     * Defines subclasses as html-type views.
     */
    const CLASS_SCOPE = 'javascript';

    /**
     * The output header for all content returned by this view
     */
    const MIME_TYPE = 'application/javascript; charset=utf-8';

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
    }
}
