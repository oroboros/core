<?php
namespace Oroboros\core\abstracts;

/**
 * Oroboros Base Abstract
 * 
 * Serves as a base level abstraction for further Oroboros classes.
 * 
 * Provides simple abstraction for setting commands, arguments and flags,
 * and complementary getters, setters, and check methods for these so
 * object construction can be standardized. Otherwise has no opinion
 * about extension.
 *
 * @author Brian Dayhoff
 */
abstract class AbstractBase implements \Oroboros\core\interfaces\BaseInterface
{

    use \Oroboros\core\traits\library\log\LoggerAwareTrait;

    /**
     * This should be overridden by other abstract classes
     * that inherit from this class to define their scope.
     * The constructor will not allow instantiation if this is left null.
     * 
     * This constant is used to categorize related groups of classes (eg model, controller, view, library, etc).
     */
    const CLASS_TYPE = null;

    /**
     * Designates the command (if any) provided to the constructor.
     * @var string
     */
    private $command = null;

    /**
     * Designates arguments (if any) provided to the constructor.
     * @var array
     */
    private $arguments = null;

    /**
     * Designates any optional flags (if any) provided to the constructor.
     * @var array 
     */
    private $flags = null;

    /**
     * The currently running application.
     * @var \Oroboros\core\interfaces\application\ApplicationInterface
     */
    private static $application = null;

    /**
     * The currently scoped user object.
     * All child classes can obtain this object via `self::user()`
     * 
     * @var \Oroboros\core\interfaces\library\user\UserInterface
     */
    private static $user = null;

    /**
     * Default constructor. Standardizes object construction to make
     * implementation of factories and prototype patterns more approachable.
     * @param string $command (optional)
     * @param array $arguments (optional)
     * @param array $flags (optional)
     * @throws \OutOfRangeException If the implementing class has not declared a class type.
     */
    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        $this->verifyClassType();
        $this->command = $command;
        $this->arguments = (is_null($arguments)) ? [] : $arguments;
        $this->flags = (is_null($flags)) ? [] : $flags;
        $this->initializeDefaultLogger();
    }

    /**
     * Placeholder Destructor
     */
    public function __destruct()
    {
        // no-op
    }

    /**
     * Sets the current application, so it is accessible to all extensions of the base abstract.
     * @param \Oroboros\core\interfaces\application\ApplicationInterface $application
     * @throws \ErrorException If an attempt to override the application definition is made.
     */
    final public static function setApplication(\Oroboros\core\interfaces\application\ApplicationInterface $application)
    {
        if (!is_null(self::$application)) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. '
                        . 'Application cannot be set because it is already defined.', static::class));
        }
        self::$application = $application;
    }
    
    final public static function setUser(\Oroboros\core\interfaces\library\user\UserInterface $user): void
    {
        if (!is_null(self::$user)) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. '
                        . 'User cannot be set because it is already defined.', static::class));
        }
        self::$user = $user;
    }

    final public static function getApplication(): \Oroboros\core\interfaces\application\ApplicationInterface
    {
        if (is_null(self::$application)) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. '
                        . 'No application is defined.', static::class));
        }
        return self::$application;
    }

    /**
     * Returns the class type designated by child abstraction,
     * which defines the scope of the class.
     * @return string
     */
    public function getType()
    {
        return static::CLASS_TYPE;
    }

    /**
     * Returns the currently running application definer class.
     * This method is internally accessible to every class extending
     * anything in this package.
     * @return Oroboros\core\interfaces\application\ApplicationInterface
     * @throws \Oroboros\core\exception\ErrorException
     *         If the application is requested before it is set by the Core
     */
    protected static function app(): \Oroboros\core\interfaces\application\ApplicationInterface
    {
        if (is_null(self::$application)) {
            // App not loaded yet
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. Request for the '
                        . 'application was passed before it has been defined.',
                        static::class)
            );
        }
        return self::$application;
    }

    /**
     * Returns the currently scoped user object.
     * This method is internally accessible to every class extending
     * anything in this package.
     * @return \Oroboros\core\interfaces\library\user\UserInterface
     * @throws \Oroboros\core\exception\ErrorException
     *         If the user is requested before it is set by the Core
     */
    protected static function user(): \Oroboros\core\interfaces\library\user\UserInterface
    {
        if (is_null(self::$user)) {
            // User not loaded yet
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. Request for the '
                        . 'user was passed before it has been defined.',
                        static::class)
            );
        }
        return self::$user;
    }

    /**
     * Command getter.
     * 
     * @return string|null the passed command, or null if no command was provided.
     */
    protected function getCommand()
    {
        return $this->command;
    }

    /**
     * Command setter.
     * 
     * @return void
     */
    protected function setCommand(string $command = null)
    {
        $this->command = $command;
    }

    /**
     * Arguments getter.
     * @return array Returns an array of provided arguments.
     *  If none were passed, it will return an empty array,
     *  making it iterable safe.
     */
    protected function getArguments(): array
    {
        return (is_null($this->arguments) ? [] : $this->arguments);
    }

    /**
     * Single argument getter.
     * @param string $key
     * @return null|mixed Returns a specific argument by key.
     *  If the given argument key was not passed, returns null.
     */
    protected function getArgument(string $key)
    {
        if (!array_key_exists($key, $this->arguments)) {
            return null;
        }
        return $this->arguments[$key];
    }

    /**
     * Returns a boolean determination as to whether or not an argument exists.
     * @param string $key
     * @return bool
     */
    protected function hasArgument(string $key)
    {
        return array_key_exists($key, $this->arguments);
    }

    /**
     * Single argument setter.
     * @param string $key
     * @param mixed $value
     * @return $this (method chainable)
     */
    protected function setArgument(string $key, $value)
    {
        $this->arguments[$key] = $value;
        return $this;
    }

    /**
     * Flags getter.
     * @return array Returns an array of provided flags.
     *  If none were passed, it will return an empty array,
     *  making it iterable safe.
     */
    protected function getFlags(): array
    {
        return (is_null($this->flags) ? [] : $this->flags);
    }

    /**
     * Returns a boolean determination as to whether or not a flag exists.
     * @param string $key
     * @return bool
     */
    protected function hasFlag(string $key): bool
    {
        if (is_null($key) || is_null($this->flags)) {
            print_r(debug_backtrace());
            exit;
        }
        return array_key_exists($key, $this->flags);
    }

    /**
     * Single flag getter.
     * @param string $key
     * @return null|mixed Returns a specific argument by key.
     *  If the given argument key was not passed, returns null.
     */
    protected function getFlag(string $key)
    {
        if (!array_key_exists($key, $this->flags)) {
            return null;
        }
        return $this->flags[$key];
    }

    /**
     * Single flag setter.
     * @param string $key
     * @param scalar $value
     * @return $this (method chainable)
     */
    protected function setFlag(string $key, $value)
    {
        $this->arguments[$key] = $value;
        return $this;
    }

    /**
     * Returns the currently loaded modules
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    protected function getModules(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        return \Oroboros\core\Oroboros::modules();
    }

    /**
     * Returns the currently loaded extensions
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    protected function getExtensions(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        return \Oroboros\core\Oroboros::extensions();
    }

    /**
     * Returns the currently loaded services
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    protected function getServices(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        return \Oroboros\core\Oroboros::services();
    }

    /**
     * This method insures that a proper class type for the child class is defined.
     * This should be defined in any abstract class inheriting this base abstract.
     * 
     * The referenced constant is used to define the logical scope of the class inheriting from this abstract.
     * @return void
     * @throws \OutOfRangeException
     * @see \Oroboros\core\BaseAbstract::CLASS_TYPE
     */
    private function verifyClassType()
    {
        if (is_null(static::CLASS_TYPE)) {
            throw new \OutOfRangeException(sprintf('Expected class constant [CLASS_TYPE] is not defined in [%1$s].', get_class($this)));
        }
    }
}
