<?php
/*
 * The MIT License
 *
 * Copyright 2015 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\bootstrap;

/**
 * Provides abstraction for standardization of the bootstrap process.
 * This allows a standardized front controller to load a bootstrap specific to the environment (http/cli/etc).
 * 
 * The bootstrap object will insure that routing, controllers,
 * and views are correctly handled in an orderly fashion,
 * and that uncaught errors are handled at a baseline level.
 * 
 * It also executes the teardown script, which releases any registered resources
 * or streams not otherwise cleaned up that may persist in memory
 * after program termination, if any exist.
 * 
 * Only one bootstrap object may be initialized.
 *
 * @author Brian Dayhoff
 */
abstract class AbstractBootstrap extends \Oroboros\core\abstracts\AbstractBase implements \Oroboros\core\interfaces\bootstrap\BootstrapInterface
{

    use \Oroboros\core\traits\LoaderTrait;
    use \Oroboros\core\traits\ContainerPackagerUtility;
    use \Oroboros\core\traits\StringUtilityTrait;
    use \Oroboros\core\traits\pattern\ObservableTrait;
    use \Oroboros\core\traits\pattern\ObserverTrait;
    use \Oroboros\core\traits\pattern\EventAwareTrait;
    use \Oroboros\core\traits\pattern\FilterAwareTrait;
    use \Oroboros\core\traits\model\ModelAwareUtilityTrait;

    /**
     * Designates further child classes as bootstrap objects.
     */
    const CLASS_TYPE = 'bootstrap';

    /**
     * Class scope should be defined by subclasses.
     */
    const CLASS_SCOPE = null;

    /**
     * Use the default container
     */
    const CONTAINER_CLASS = 'Oroboros\\core\\library\\container\\Container';

    /**
     * Determines the scope of the bootstrap. Should be http, cli, or cgi.
     * Must be overridden by extension.
     */
    const BOOTSTRAP_SCOPE = null;

    /**
     * Determines if the bootstrap extension handles command line interaction (bool).
     * Must be overridden by extension.
     */
    const BOOTSTRAP_CLI = null;

    /**
     * Determines the router type to use to resolve the request.
     * Must be overridden by extension.
     */
    const ROUTER_CLASS = null;

    /**
     * Determines the error handler to use for the runtime.
     * May be overridden by extension.
     */
    const ERROR_HANDLER_CLASS = \Oroboros\core\library\error\NullErrorHandler::class;

    /**
     * Determines if the bootstrap extension is headless (bool).
     * Must be overridden by extension.
     * 
     * If headless, return values can only be exit codes.
     * Must be zero if no error occurred. Otherwise view output is ignored for render.
     */
    const BOOTSTRAP_HEADLESS = null;

    private static $valid_scopes = [
        'http',
        'cli',
        'cgi',
        'unit-test'
    ];

    /**
     * Designates that only one bootstrap object may be initialized.
     * This is not a singleton, and is not obtainable after the initial constructor call.
     * The bootstrap object must be passed off via dependency injection
     * if it is needed outside the global scope. Further instances will refuse to instantiate entirely,
     * which enforces that heavy bootstrap operations only occur in one location.
     * @var bool
     */
    private static $is_loaded = false;

    /**
     * The active bootstrap instance. If this is not null, no further bootstrap objects may be created.
     * @var \Oroboros\core\interfaces\bootstrap\BootstrapInterface
     */
    private static $loaded_instance = null;

    /**
     * Any registered modules.
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private static $modules = null;

    /**
     * Any registered extensions.
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private static $extensions = null;

    /**
     * Any registered services.
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private static $services = null;

    /**
     * Represents database connection credentials available.
     * @var \Oroboros\core\library\parser\JsonParser
     */
    private static $connections = null;

    /**
     * @todo
     * @var \Oroboros\core\interfaces\library\request\RequestInterface 
     */
    private static $request = null;

    /**
     * The router instance
     * @var \Oroboros\core\interfaces\library\router\RouterInterface 
     */
    private static $router = null;

    /**
     * The route selected to match the request
     * @var \Oroboros\core\interfaces\library\router\RouteInterface
     */
    private static $route = null;

    /**
     * The controller object returned from the route
     * @var \Oroboros\core\interfaces\controller\ControllerInterface 
     */
    private static $controller = null;

    /**
     * The method to call on the controller selected to handle the request
     * @var string
     */
    private static $method = null;

    /**
     * The command to pass to the controller, if any
     * @var string
     */
    private static $controller_command = null;

    /**
     * The arguments to pass to the controller, if any
     * @var array
     */
    private static $controller_arguments = null;

    /**
     * The flags to pass to the controller, if any
     * @var array
     */
    private static $controller_flags = null;

    /**
     * @todo
     * @var \Oroboros\core\interfaces\view\ViewInterface 
     */
    private static $view = null;

    /**
     * The currently active runtime error handler
     * 
     * @var \Oroboros\core\interfaces\library\error\ErrorHandlerInterface
     */
    private static $error_handler = null;

    /**
     * Observer event bindings for the bootstrap object
     * 
     * @var array
     */
    private static $bootstrap_observer_event_bindings = [
        'render' => 'serveResponse',
    ];

    /**
     * The user scoped for the current request
     * @var \Oroboros\core\interfaces\library\user\UserInterface
     */
    private $user = null;

    /**
     * General bootstrap constructor
     * 
     * @param string $command
     * @param array $arguments
     * @param array $flags
     * @throws \Oroboros\core\exception\core\InvalidClassException If any
     *         required class constant is not correctly defined by extension.
     */
    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        $this->constructorVerification();
        $this->initializeModelAware();
        parent::__construct($command, $arguments, $flags);
        $this::app()->setLogger($this->getLogger());
        $this::app()->setErrorHandler($this->getErrorHandler());
        $this->constructorPreInitialize();
        $this->constructorPostInitialize();
    }

    /**
     * General bootstrap destructor
     */
    public function __destruct()
    {
        $this->getLogger()->debug('[type][scope][class] Fired bootstrap destructor.', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
        ]);
        $this->teardown();
    }

    /**
     * Runs on all shutdown operations after execution.
     * 
     * This will defer errors to the method `onShutdownError`,
     * and fatal errors to the method `onShutdownFatalError`.
     * Only one of these two will run if an error has occurred,
     * depending on the severity of the error.
     * 
     * `E_ERROR` and `E_USER_ERROR` will trigger `onShutdownFatalError`.
     * All other errors will trigger `onShutdownError`
     * 
     * By default, there is no other operation aside from running shutdown
     * observer events and action events. External objects may bind to those
     * if they wish to perform shutdown operations.
     * 
     * It is generally preferable to only run logging tasks post-shutdown,
     * and only if they are required.
     * 
     * @return void
     */
    public function onShutdown(): void
    {
        $this->getLogger()->debug('[type][scope][class] Running bootstrap shutdown operations.', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
        ]);
        if ($this->hasEventQueue('shutdown')) {
            $this->scopeEventQueue('shutdown');
        }
        if ($this->hasFilterQueue('shutdown')) {
            $this->scopeFilterQueue('shutdown');
        }
        $this->setObserverEvent('shutdown');
        $last = error_get_last();
        if (!is_null($last)) {
            if ($last['type'] === E_ERROR || $last['type'] === E_USER_ERROR) {
                // fatal error has occured
                $this->onShutdownFatalError();
            } else {
                // Some non-fatal error occurred
                $this->onShutdownError();
            }
        } else {
            $this->getLogger()->debug('[type][scope][class] Executing clean shutdown events.', [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE
            ]);
            if ($this->hasEvent('shutdown')) {
                $this->fireEvent('shutdown', $this->getBootstrapEventArguments());
            }
        }
    }

    /**
     * Executes the current request.
     * 
     * @return void
     */
    public function bootstrap()
    {
        $exit_code = 1;
        $error_controller = null;
        // Execute the program
        $this->getLogger()->debug('[type][scope][class] Executing program.', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE
        ]);
        try {
            $this->bootstrapPreInitialize();
            $error_controller = self::$controller->getErrorController();
            $exit_code = $error_controller::DEFAULT_FATAL_ERROR_CODE;
            $this->bootstrapPostInitialize($error_controller);
            $exit_code = 0;
        } catch (\Oroboros\core\exception\core\InvalidClassException $e) {
            // A broken class was loaded. This needs to be logged. Show a 500 error and close up.
            // This does not need further handling.
            $this->handleInvalidClassException($e, $error_controller);
        } catch (\Oroboros\core\interfaces\exception\ErrorExceptionInterface $e) {
            // An irrecoverable error occurred, but it was handled by an error controller already.
            $exit_code = $e->getCode();
            $this->handleControllerHandledException($e, $error_controller);
        } catch (\Exception $error) {
            $this->handleUncaughtException($error, $error_controller);
        }
        if (ob_get_level()) {
            ob_end_flush();
        }
        $this->teardown();
        if ($exit_code !== 0) {
            $this->exitWithError($exit_code);
        }
    }

    /**
     * Cleans up after runtime.
     * 
     * @return void
     */
    public function teardown(): void
    {
        $this->getLogger()->debug('[type][scope][class] Executing bootstrap teardown.', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE
        ]);
        if ($this->hasEventQueue('teardown')) {
            $this->scopeEventQueue('teardown');
        }
        if ($this->hasFilterQueue('teardown')) {
            $this->scopeFilterQueue('teardown');
        }
        if ($this->hasEvent('cleanup')) {
            $this->fireEvent('cleanup', $this->getBootstrapEventArguments());
        }
        $this->setObserverEvent('teardown');
    }

    /**
     * Designates that the bootstrap should exit with an error.
     * This method will fire the "exit-error" event if it exists,
     * and exit runtime with the given code.
     * 
     * @param int $code
     */
    public function exitWithError(int $code)
    {
        if ($this->hasEventQueue('error')) {
            $this->scopeEventQueue('error');
        }
        if ($this->hasFilterQueue('error')) {
            $this->scopeFilterQueue('error');
        }
        if ($this->hasEvent('exit-error')) {
            $this->fireEvent('exit-error', $this->getBootstrapEventArguments([
                    'error' => error_get_last()
            ]));
        }
        $this->setObserverEvent('exit-with-error');
        exit($code);
    }
    
    /**
     * Returns the current runtime error handler
     * 
     * @return \Oroboros\core\interfaces\library\error\ErrorHandlerInterface
     */
    public function getErrorHandler(): \Oroboros\core\interfaces\library\error\ErrorHandlerInterface
    {
        $this->initializeErrorHandler();
        return self::$error_handler;
    }

    /**
     * Returns the currently loaded modules
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    public function getLoadedModules(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        return self::$modules;
    }

    /**
     * Returns the currently loaded extensions
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    public function getLoadedExtensions(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        return self::$extensions;
    }

    /**
     * Returns the currently loaded services
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    public function getLoadedServices(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        return self::$services;
    }

    /**
     * Serves a response from a controller that has completed content to emit back to the client.
     * @param \Oroboros\core\interfaces\controller\ControllerInterface $controller
     */
    public function serveResponse(\Oroboros\core\interfaces\controller\ControllerInterface $controller)
    {
        $strategy = $this->load('library', 'dispatch\\DispatchStrategy');
        $dispatcher = $strategy->evaluate($controller);
        $this->setObserverEvent('response');
        $dispatcher->dispatch();
    }

    /**
     * Handles any exception not handled by the controller.
     * 
     * @param \Exception $error
     */
    public function handleUncaughtError(\Exception $error)
    {
        try {
            $this->handleUncaughtException($error);
        } catch (\Exception $e) {
            // Failed to handle error
            $this->handleUncaughtUnhandledException($e);
            $error = $e;
        } finally {
            $this->teardown();
            $this->exitWithError($error->getCode());
        }
    }

    /**
     * Perform any additional steps prior to loading the routes.
     */
    protected function initialize(): void
    {
        $this->setObserverEvent('initialized');
    }

    /**
     * Sets up the default bootstrap object observer event bindings.
     * 
     * @return array
     */
    protected function declareObserverEventBindings(): array
    {
        $bindings = self::$bootstrap_observer_event_bindings;
        foreach ($bindings as $key => $value) {
            if (!is_callable($value)) {
                $newval = [$this, $value];
                $bindings[$key] = $newval;
            }
        }
        return $bindings;
    }

    /**
     * Sets the observable name for the bootstrap
     * @return string
     */
    protected function declareObservableName(): string
    {
        return trim(sprintf('%1$s:%2$s', static::CLASS_TYPE, static::CLASS_SCOPE), ':');
    }

    /**
     * Override this method to pass additional default arguments
     * to controllers upon instantiation.
     * 
     * If your application specific controllers require
     * additional dependency injection, this method should
     * be overridden to provide the required dependencies.
     * @return array
     */
    protected function getControllerArguments(): array
    {
        $this->setObserverEvent('before-controller-arguments');
        $arguments = [
            'connections' => self::$connections,
            'extensions' => self::$extensions,
            'modules' => self::$modules,
            'services' => self::$services,
            'router' => $this->getRouter(),
            'logger' => $this->getLogger(),
        ];
        return array_replace($this->getArguments(), $arguments);
    }

    /**
     * Override this method to pass additional default arguments
     * to the router upon instantiation.
     * 
     * If your application specific router requires
     * additional dependency injection, this method should
     * be overridden to provide the required dependencies.
     * @return array
     */
    protected function getRouterArguments(): array
    {
        $this->setObserverEvent('before-router-arguments');
        $arguments = [
            'routes' => $this::app()->getRoutes(),
            'extensions' => self::$extensions,
            'modules' => self::$modules,
            'services' => self::$services,
            'event-broker' => $this->load('library', 'event\\EventBroker'),
            'filter-broker' => $this->load('library', 'filter\\FilterBroker'),
            'logger' => $this->getLogger(),
        ];
        return array_replace($this->getArguments(), $arguments);
    }

    /**
     * Returns the currently scoped controller
     * 
     * @return \Oroboros\core\interfaces\controller\ControllerInterface
     * @throws \Oroboros\core\exception\core\BootstrapException
     */
    protected function getController(): \Oroboros\core\interfaces\controller\ControllerInterface
    {
        if (is_null(self::$controller)) {
            throw new \Oroboros\core\exception\core\BootstrapException(sprintf('Error encountered in [%1$s]. Bootstrap failed to define a valid controller class.', get_class($this)));
        }
        return self::$controller;
    }

    /**
     * Getter method to allow child classes access to the server request object.
     * @return \Psr\Http\Message\ServerRequestInterface
     */
    protected function getRequest(): \Psr\Http\Message\ServerRequestInterface
    {
        return self::$request;
    }

    /**
     * Getter method to allow child classes access to the router object.
     * @return \Oroboros\core\interfaces\library\router\RouterInterface
     */
    protected function getRouter(): \Oroboros\core\interfaces\library\router\RouterInterface
    {
        return self::$router;
    }

    /**
     * Declares the flags to pass to the logger
     * that is automatically instantiated.
     * 
     * By default this passes the same flags passed
     * into the costructor of the object itself.
     * 
     * You may override this method to pass specialized flags or
     * append extra flags to the logger constructor
     * 
     * @return array|null
     */
    protected function declareLoggerFlags(): ?array
    {
        return $this->getFlags();
    }

    /**
     * Returns the arguments the bootstrap object passes in all events fired
     * 
     * @param array $arguments Any additional arguments to add to the defaults
     * @return array
     */
    protected function getBootstrapEventArguments(array $arguments = null): array
    {
        $args = $this->getArguments();
        $args['bootstrap'] = $this;
        $args['loaded'] = self::$is_loaded;
        $args['scope'] = static::CLASS_SCOPE;
        $args['modules'] = self::$modules;
        $args['services'] = self::$services;
        $args['extensions'] = self::$extensions;
        $args['router'] = self::$router;
        $args['route'] = self::$route;
        $args['controller'] = self::$controller;
        $args['connections'] = self::$connections;
        if (!is_null($arguments)) {
            $args = array_replace_recursive($args, $arguments);
        }
        $arguments = [
            'command' => $this->getCommand(),
            'args' => $args,
            'flags' => $this->getFlags(),
        ];
        return $arguments;
    }

    /**
     * Loads the router.
     * Override if you want custom routing logic.
     */
    protected function loadRouter()
    {
        $this->setObserverEvent('load-router');
        if ($this->hasEvent('before-load-router')) {
            $this->fireEvent('before-load-router', $this->getBootstrapEventArguments());
        }
        $class = $this->getFullClassName('library', static::ROUTER_CLASS);
        $instance = $this->load('library', static::ROUTER_CLASS, $this->getCommand(), $this->getRouterArguments(), $this->getFlags());
        $instance->setLogger($this->getLogger());
        self::$request = $instance->getRequest();
        self::$router = $this->updateRouter($instance);
        if ($this->hasEvent('after-load-router')) {
            $this->fireEvent('after-load-router', $this->getBootstrapEventArguments([
                    'router' => self::$router,
                    'request' => self::$request
            ]));
        }
    }

    /**
     * Override to update or replace the router
     * @param type $instance
     */
    protected function updateRouter(\Oroboros\core\interfaces\library\router\RouterInterface $instance): \Oroboros\core\interfaces\library\router\RouterInterface
    {
        $this->setObserverEvent('router-updated');
        return $instance;
    }

    /**
     * Returns the currently scoped user object
     * 
     * @return \Oroboros\core\interfaces\library\user\UserInterface
     * @throws \Oroboros\core\exception\core\InvalidClassException if the user
     *         is requested internally before it has loaded. This means the
     *         bootstrap extending this abstract is broken and does not follow
     *         the correct order of operations. As long as `parent::__construct`
     *         was called before requesting a user object, this should never happen.
     */
    protected function getUser(): \Oroboros\core\interfaces\library\user\UserInterface
    {
        if (is_null($this->user)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. User object was '
                        . 'requested before it has been initialized.', get_class($this))
            );
        }
        return $this->user;
    }

    /**
     * Loads the user object.
     * If a child class has already loaded an override user object,
     * it should be handed back to `parent::initializeUser($user)`
     * to set it correctly.
     * 
     * This may only be done one time.
     * 
     * @param \Oroboros\core\interfaces\library\user\UserInterface $user
     * @return void
     * @throws \ErrorException
     */
    protected function initializeUser(\Oroboros\core\interfaces\library\user\UserInterface $user = null): void
    {
        if (!is_null($this->user)) {
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. '
                        . 'Provided user cannot be scoped because '
                        . 'a user is already defined.', get_class($this)));
        }
        $this->setObserverEvent('initialize-user');
        if ($this->hasEvent('before-load-user')) {
            $this->fireEvent('before-load-user', $this->getBootstrapEventArguments());
        }
        if (!is_null($user)) {
            $this->user = $user;
        } else {
            $this->user = $this->load('library', 'user\\NullUser', null, array_replace([
                'request' => $this->getRequest()
                    ], $this->getArguments()), $this->getFlags());
        }
        try {
            \Oroboros\core\abstracts\AbstractBase::setUser($this->user);
        } catch (\Oroboros\core\exception\ErrorException $e) {
            // Users already defined. 
            // This is totally fine.
            // It just means that a custom user
            // was set prior to runtime.
        }
        $this->user = $this::user();
        $this->user->setLogger($this->getLogger());
        $this->user->setRequest(self::$request);
        if ($this->hasEvent('after-load-user')) {
            $this->fireEvent('after-load-user', $this->getBootstrapEventArguments([
                    'user' => $this->user
            ]));
        }
    }

    /**
     * Updates the action bindings for objects that
     * loaded prior to pluggables being imported fully.
     * 
     * @return void
     */
    protected function reinitializeActionBindings(): void
    {
        // Update Self
        $this->reinitializeEventWatchers();
        $this->reinitializeFilterWatchers();
        // Update the App
        $this::app()->reinitializeEventWatchers();
        $this::app()->reinitializeFilterWatchers();
        // Update Modules
        foreach (self::$modules as $module) {
            $module->reinitializeEventWatchers();
            $module->reinitializeFilterWatchers();
        }
        // Update Services
        foreach (self::$services as $service) {
            $service->reinitializeEventWatchers();
            $service->reinitializeFilterWatchers();
        }
        // Update Extensions
        foreach (self::$extensions as $extension) {
            $extension->reinitializeEventWatchers();
            $extension->reinitializeFilterWatchers();
        }
        $this->setObserverEvent('watchers-updated');
    }

    /**
     * Runs if any unhandled error occurs during runtime
     * 
     * @return void
     */
    protected function onShutdownError(): void
    {
        $error = error_get_last();
        $this->getLogger()->alert(sprintf('[type][scope][class] Executing shutdown with errors: [error]'
                . '%1$s%2$s- Generated at line [errorline] of file [errorfile]', PHP_EOL, '    '), [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'error' => $error['message'],
            'errorline' => $error['line'],
            'errorfile' => $error['file'],
        ]);
        if ($this->hasEvent('error-report')) {
            $this->fireEvent('error-report', $this->getBootstrapEventArguments([
                    'error' => error_get_last()
            ]));
        }
        $this->setObserverEvent('shutdown-error');
    }

    /**
     * Runs if a fatal error occured during shutdown
     * @return void
     */
    protected function onShutdownFatalError(): void
    {
        $error = error_get_last();
        $this->getLogger()->alert(sprintf('[type][scope][class] A fatal error has occurred: [error]'
                . '%1$s%2$s- Generated at line [errorline] of file [errorfile]', PHP_EOL, '    '), [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'error' => $error['message'],
            'errorline' => $error['line'],
            'errorfile' => $error['file'],
        ]);
        if ($this->hasEventQueue('fatal')) {
            $this->scopeEventQueue('fatal');
        }
        if ($this->hasFilterQueue('fatal')) {
            $this->scopeFilterQueue('fatal');
        }
        if ($this->hasEvent('error-report')) {
            $this->fireEvent('error-report', $this->getBootstrapEventArguments([
                    'error' => error_get_last()
            ]));
        }
        $this->setObserverEvent('fatal');
    }

    /**
     * Verify that the extension class has declared its scope correctly
     * @throws \OutOfRangeException
     */
    private function verifyBootstrapScope()
    {
        if (is_null(static::BOOTSTRAP_SCOPE) || !in_array(static::BOOTSTRAP_SCOPE, self::$valid_scopes)) {
            throw new \OutOfRangeException(sprintf('Bootstrap class [%1$s] is invalid. '
                        . 'It must define class constant [%2$s] as one of [%3$s].', get_class($this), 'BOOTSTRAP_SCOPE', implode(', ', self::$valid_scopes)));
        }
    }

    /**
     * Verify that the extension class has declared whether or not it is in cli mode
     * @throws \OutOfRangeException
     */
    private function verifyBootstrapCli()
    {
        if (is_null(static::BOOTSTRAP_CLI) || !is_bool(static::BOOTSTRAP_CLI)) {
            throw new \OutOfRangeException(sprintf('Bootstrap class [%1$s] is invalid. '
                        . 'It must define class constant [%2$s] as one of [%3$s] (strict boolean only).', get_class($this), 'BOOTSTRAP_CLI', implode(', ', ['true', 'false'])));
        }
    }

    /**
     * Verify that the extension class has declared whether or not it is headless
     * @throws \OutOfRangeException
     */
    private function verifyBootstrapHeadless()
    {
        if (is_null(static::BOOTSTRAP_HEADLESS) || !is_bool(static::BOOTSTRAP_HEADLESS)) {
            throw new \OutOfRangeException(sprintf('Bootstrap class [%1$s] is invalid. '
                        . 'It must define class constant [%2$s] as one of [%3$s] (strict boolean only).', get_class($this), 'BOOTSTRAP_HEADLESS', implode(', ', ['true', 'false'])));
        }
    }

    /**
     * Verify that no duplicate call to the `bootstrap` method has occurred
     * @throws \OutOfRangeException
     */
    private function verifyCleanLoad()
    {
        if (self::$is_loaded) {
            throw new \OutOfRangeException(sprintf('Error encountered in [%1$s]. '
                        . 'Bootstrap may only occur one time.'), get_class($this));
        }
    }

    /**
     * Verify that a bootstrap object has not already been instantiated
     * @throws \OutOfRangeException
     */
    private function verifyNonDuplicateLoad()
    {
        if (!is_null(self::$loaded_instance) && static::BOOTSTRAP_SCOPE !== 'unit-test') {
            throw new \OutOfRangeException(sprintf('Error encountered in [%1$s]. '
                        . 'Only one bootstrap instance may be instantiated. '
                        . 'Instantiation already occurred for bootstrap class [%2$s].', get_class($this), get_class(self::$loaded_instance)));
        }
    }

    /**
     * Verify that the declared router implements the router interface
     * @throws \OutOfRangeException
     */
    private function verifyRouterClass()
    {
        $expected = 'Oroboros\\core\\interfaces\\library\\router\\RouterInterface';
        if (is_null(static::ROUTER_CLASS) || !($this->getFullClassName('library', static::ROUTER_CLASS) == $expected || is_subclass_of($this->getFullClassName('library', static::ROUTER_CLASS), $expected))) {
            throw new \OutOfRangeException(sprintf('Bootstrap class [%1$s] is invalid. '
                        . 'It must define class constant [%2$s] as a class name that is a valid implementation of [%3$s].', get_class($this), 'ROUTER_CLASS', $expected));
        }
    }

    /**
     * Loads the known connections from the scoped connection model.
     * You may modify this behavior by overriding the connection model
     * and registering the replacement prior to this method call.
     * 
     * You can listen on the bootstrap observer event 'load-credentials'
     * and register a replacement model within that event callback.
     * 
     * @return void
     */
    private function loadDatabaseCredentials()
    {
        $this->setObserverEvent('load-credentials');
        $connections = $this->getModel('connection')->fetch();
        self::$connections = $connections;
    }

    /**
     * @todo
     */
    private function returnResponse()
    {
        $this->setObserverEvent('dispatch-response');
        if (!static::BOOTSTRAP_HEADLESS) {
            // $view = self::$controller->getView();
            // $view->render();
        } else {
            // $code = self::$controller->getExitCode();
            // echo $code;
        }
    }

    /**
     * Calls the selected controller method designated to handle the request.
     * @return void
     */
    private function executeControllerMethod()
    {
        $this->setObserverEvent('controller-execute');
        $method = self::$method;
        $controller = self::$controller;
        if ($this->hasEvent('before-controller-execute')) {
            $this->fireEvent('before-controller-execute', $this->getBootstrapEventArguments([
                    'controller-method' => $method,
                    'controller' => $controller,
                    'route' => self::$route
            ]));
        }
        try {
            $this->authenticateControllerMethod();
            $this->getLogger()->debug('[type][scope][class] Executing controller method: [controller]->[method]', [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
                'controller' => get_class($controller),
                'method' => $method,
            ]);
            if (is_null($method)) {
                $method = 'error';
            }
            if ($method === 'error') {
                $code = $controller::DEFAULT_ERROR_CODE;
                $controller->$method($code, null, self::$route->getRequestArguments(), self::$route->getRequestFlags());
            } else {
                $controller->$method(self::$route->getRequestArguments(), self::$route->getRequestFlags());
            }
        } catch (\Oroboros\core\exception\auth\AuthenticationException $e) {
            // Unauthorized page section as referenced 
            // within the controller method instead of before it.
            $this->handleUnauthorizedAccess($e);
        }
        $this->returnResponse();
        if ($this->hasEvent('after-controller-execute')) {
            $this->fireEvent('after-controller-execute', $this->getBootstrapEventArguments([
                    'controller-method' => $method,
                    'controller' => $controller,
                    'route' => $route
            ]));
        }
    }

    /**
     * This method fires if a controller reports an authentication error.
     * 
     * The default behavior is to simply return the front page error 404 page.
     * 
     * @param \Oroboros\core\exception\auth\AuthenticationException $error
     * @return void
     */
    private function handleUnauthorizedAccess(\Oroboros\core\exception\auth\AuthenticationException $error): void
    {
        $route = self::$router->match(self::$request->withUri(self::$request->getUri()->withPath('/')));
        $method = 'error';
        $controller = $route->getController()->getErrorController();
        $controller->attach($this);
        self::$controller = $controller;
        self::$method = $method;
        self::$controller->setLogger($this->getLogger());
        self::$controller->attach($this);
        $args = $this->getControllerArguments();
        $args['error'] = $error;
        $controller = $controller->getErrorController();
        if ($this->hasEvent('before-authentication-error')) {
            $this->fireEvent('before-authentication-error', $this->getBootstrapEventArguments([
                    'controller-method' => $method,
                    'controller' => $controller,
                    'route' => self::$route
            ]));
        }
        $controller->$method($error->getCode(), $error->getMessage(), $this->containerize($args));
        $this->serveResponse($controller);
        $this->exitWithError($error->getCode());
    }

    /**
     * Authenticates the controller method prior to calling it against
     * the currently scoped user.
     * 
     * This allows controllers to check permissions against the
     * intended method before it is called and reject it.
     * 
     * This will force the bootstrap to use the public error controller,
     * masking the private controller completely to the unauthenticated
     * end user.
     * 
     * @return void
     * @throws \Oroboros\core\exception\ErrorException
     *         If a controller fails to authenticate properly.
     */
    private function authenticateControllerMethod(): void
    {
        $method = self::$method;
        $controller = self::$controller;
        if (is_null($method)) {
            $method = 'error';
        }
        try {
            if (!$controller->authenticate($method, $this->user)) {
                throw new \Oroboros\core\exception\ErrorException(
                        sprintf('Error encountered in [%1$s]. '
                            . 'Controller [%2$s] failed to authenticate '
                            . 'method [%3$s].'
                            , get_class($this), get_class($controller), $method)
                );
            }
        } catch (\Oroboros\core\exception\auth\AuthenticationException $e) {
            // This is expected when controllers reject authentication
            $this->handleUnauthorizedAccess($e);
            return;
        }
        self::$controller = $controller;
        self::$controller->setLogger($this->getLogger());
        self::$method = $method;
    }

    /**
     * Injects direct dependencies into classes that will need
     * to be made aware of them.
     * 
     * @return void
     */
    private function injectDependencyObjects(): void
    {
        // Provide the template engine to the component abstract.
        // @todo set a configuration for which template engine to load. For now twig is sufficient.
        $template_engine = $this->load('library', 'template\\twig\\Twig', null, ['modules' => self::$modules, 'extensions' => self::$extensions]);
        \Oroboros\core\abstracts\library\component\AbstractComponent::setTemplateEngine($template_engine);
    }

    /**
     * Imports any extensions the application has declared are active.
     * 
     * @return void
     */
    private function importExtensions(): void
    {
        $importer = $this->load('library', 'extension\\ExtensionImporter');
        $importer->setLogger($this->getLogger());
        foreach ($this::app()->getApplicationExtensions() as $extension => $path) {
            if (!$importer->evaluate($path)) {
                continue;
            }
            $importer->import($path);
        }
        self::$extensions = $importer->fetch();
    }

    /**
     * Imports any modules the application has declared are active.
     * 
     * @return void
     */
    private function importModules(): void
    {
        $importer = $this->load('library', 'module\\ModuleImporter');
        $importer->setLogger($this->getLogger());
        foreach ($this::app()->getApplicationModules() as $module => $path) {
            if (!$importer->evaluate($path)) {
                continue;
            }
            $importer->import($path);
        }
        self::$modules = $importer->fetch();
    }

    /**
     * Imports any services the application has declared are active.
     * 
     * @return void
     */
    private function importServices(): void
    {
        $importer = $this->load('library', 'service\\ServiceImporter');
        $importer->setLogger($this->getLogger());
        foreach ($this::app()->getApplicationServices() as $service => $path) {
            if (!$importer->evaluate($path)) {
                continue;
            }
            $importer->import($path);
        }
        foreach (self::$modules as $key => $module) {
            foreach ($module->services() as $service => $path) {
                if (!$importer->evaluate($path)) {
                    continue;
                }
                $importer->import($path);
            }
        }
        self::$services = $importer->fetch();
    }

    /**
     * Runs validation tasks prior to calling `parent::__construct`
     * 
     * @return void
     */
    private function constructorVerification(): void
    {
        // Validate the extension class defaults
        $this->verifyBootstrapScope();
        // Check if this is a cli or http request
        $this->verifyBootstrapCli();
        // Check if a view object should be obtained from the controller
        $this->verifyBootstrapHeadless();
        // Insure the declared router is valid
        $this->verifyRouterClass();
        // Don't re-instantiate a bootstrap object
        $this->verifyNonDuplicateLoad();
    }

    /**
     * Runs pre-initialization tasks for the bootstrap, before any
     * external functionality is available.
     * 
     * @return void
     */
    private function constructorPreInitialize(): void
    {

        register_shutdown_function([$this, 'onShutdown']);
        $this->importExtensions();
        $this->importModules();
        $this->importServices();
        $this->injectDependencyObjects();
        $this->initializeObservable();
        $this->initializeObserver();
        $this->initializeEvents();
        $this->initializeFilters();
        if ($this->hasEventQueue('init')) {
            $this->scopeEventQueue('init');
        }
        if ($this->hasFilterQueue('init')) {
            $this->scopeFilterQueue('init');
        }
        if ($this->hasEvent('pre-initialize')) {
            $this->fireEvent('pre-initialize', $this->getBootstrapEventArguments());
        }
        self::$loaded_instance = $this;
        $this->getLogger()->debug('[type][scope][class] Completed constructor pre-initialization', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
        ]);
    }

    /**
     * Runs post-initialization tasks for the bootstrap, after pluggables
     * have loaded completely and can begin interaction with each other.
     * 
     * @return void
     */
    private function constructorPostInitialize(): void
    {
        // Verify we are not duplicating the bootstrap
        $this->verifyCleanLoad();
        $this->reinitializeActionBindings();
        if ($this->hasEvent('pluggables-ready')) {
            $this->fireEvent('pluggables-ready', $this->getBootstrapEventArguments());
        }
        // Initialize
        $this->loadDatabaseCredentials();
        $this->initialize();
        $this->loadRouter();
        $user = null;
        if ($this->hasArgument('user')) {
            $user = $this->getArgument('user');
        }
        $this->initializeUser($user);
        if ($this->hasEvent('post-initialize')) {
            $this->fireEvent('post-initialize', $this->getBootstrapEventArguments());
        }
        if ($this->hasEventQueue('ready')) {
            $this->scopeEventQueue('ready');
        }
        if ($this->hasFilterQueue('ready')) {
            $this->scopeFilterQueue('ready');
        }
        // No more models may be registered after this time
        $this->getModelStrategy()->lock();
        $this->getLogger()->debug('[type][scope][class] Completed constructor post-initialization', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
        ]);
    }

    /**
     * Performs the initial bootstrap method call setup.
     * 
     * @return void
     */
    private function bootstrapPreInitialize(): void
    {
        if ($this->hasEventQueue('bootstrap')) {
            $this->scopeEventQueue('bootstrap');
        }
        if ($this->hasFilterQueue('bootstrap')) {
            $this->scopeFilterQueue('bootstrap');
        }
        $this->setObserverEvent('bootstrap');
        self::$is_loaded = true;
        self::$route = self::$router->match(self::$request);
        if ($this->hasEvent('before-load-controller')) {
            $this->fireEvent('before-load-controller', $this->getBootstrapEventArguments([
                    'route' => self::$route
            ]));
        }
        self::$controller = self::$route->getController(null, $this->getControllerArguments(), $this->getFlags());
        self::$controller->attach($this);
        if ($this->hasEvent('before-load-error-controller')) {
            $this->fireEvent('before-load-error-controller', $this->getBootstrapEventArguments([
                    'controller' => self::$controller,
                    'route' => self::$route
            ]));
        }
    }

    /**
     * Executes the controller operation.
     * Must be handed the existing error controller for
     * reporting and issue mitigation.
     * 
     * @param \Oroboros\core\interfaces\controller\ErrorControllerInterface $error_controller
     * @return void
     */
    private function bootstrapPostInitialize(\Oroboros\core\interfaces\controller\ErrorControllerInterface $error_controller): void
    {
        if (self::$controller !== $error_controller) {
            $error_controller->attach($this);
        }
        if ($this->hasEvent('after-load-error-controller')) {
            $this->fireEvent('after-load-error-controller', $this->getBootstrapEventArguments([
                    'error-controller' => $error_controller,
                    'route' => self::$route
            ]));
        }
        self::$method = self::$route->getMethod();
        if ($this->hasEvent('after-load-controller')) {
            $this->fireEvent('after-load-controller', $this->getBootstrapEventArguments([
                    'controller' => self::$controller,
                    'method' => self::$method,
                    'error-controller' => $error_controller,
                    'route' => self::$route
            ]));
        }
        if ($this->hasEvent('bootstrap-initialized')) {
            $this->fireEvent('bootstrap-initialized', $this->getBootstrapEventArguments());
        }
        $this->executeControllerMethod();
        if ($this->hasEvent('controller-clean-run')) {
            $this->fireEvent('controller-clean-run', $this->getBootstrapEventArguments());
        }
    }

    /**
     * When a controller handles an exception and throws their own indicating
     * a problem was encountered but it was handled within the controller,
     * those exceptions will arrive at this method.
     * 
     * @param \Oroboros\core\interfaces\exception\ErrorExceptionInterface $e
     * @param \Oroboros\core\interfaces\controller\ErrorControllerInterface|null $error_controller
     * @return void
     */
    private function handleControllerHandledException(\Oroboros\core\interfaces\exception\ErrorExceptionInterface $e, \Oroboros\core\interfaces\controller\ErrorControllerInterface $error_controller = null): void
    {
        if ($this->hasEventQueue('error')) {
            $this->scopeEventQueue('error');
        }
        if ($this->hasFilterQueue('error')) {
            $this->scopeFilterQueue('error');
        }
        if ($this->hasEvent('exception-handled')) {
            $this->fireEvent('exception-handled', $this->getBootstrapEventArguments([
                    'exception' => $e
            ]));
        }
        $this->setObserverEvent('error-handled');
        if (ob_get_level()) {
            ob_end_flush();
        }
    }

    /**
     * Exceptions thrown during controller execution will arrive at this method.
     * 
     * This will trigger loading the error controller and passing the
     * given exception to it.
     * 
     * @param \Exception $error
     * @param \Oroboros\core\interfaces\controller\ErrorControllerInterface|null $error_controller
     * @return void
     */
    private function handleUncaughtException(\Exception $error, \Oroboros\core\interfaces\controller\ErrorControllerInterface $error_controller = null): void
    {
        d(get_class($error), $error->getMessage(), $error->getTrace());
        $this->getLogger()->error(sprintf('[type][scope][class] An uncaught '
                . 'exception of type [exception] has occurred.'
                . '%1$s%2$s- Message: [message]'
                . '%1$s%2$s- File: [file], line [line]'
                . '%1$s%2$s- Trace: [trace]'
                , PHP_EOL, '    '), [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'exception' => get_class($error),
            'message' => $error->getMessage(),
            'line' => $error->getLine(),
            'file' => $error->getFile(),
            'trace' => $error->getTraceAsString(),
        ]);
        // Delete any partial output if it exists, so the error message can be rendered cleanly.
        if (ob_get_level()) {
            ob_end_clean();
        }
        ob_start();
        if ($this->hasEventQueue('error')) {
            $this->scopeEventQueue('error');
        }
        if ($this->hasFilterQueue('error')) {
            $this->scopeFilterQueue('error');
        }
        if ($this->hasEvent('exception-unhandled')) {
            $this->fireEvent('exception-unhandled', $this->getBootstrapEventArguments([
                    'exception' => $error
            ]));
        }
        $this->setObserverEvent('error-uncaught');
        $error_route = self::$router->loadErrorRoute(self::$request);
        $method = 'error';
        if (is_null($error_controller)) {
            $args = $this->getControllerArguments();
            $args['error'] = $error;
            if ($this->hasEvent('before-load-error-controller')) {
                $this->fireEvent('before-load-error-controller', $this->getBootstrapEventArguments([
                        'controller' => self::$controller,
                        'route' => $error_route
                ]));
            }
            $error_controller = $error_route->getController(null, $args);
        }
        if (self::$controller !== $error_controller) {
            $error_controller->attach($this);
        }
        if ($this->hasEvent('after-load-error-controller')) {
            $this->fireEvent('after-load-error-controller', $this->getBootstrapEventArguments([
                    'error-controller' => $error_controller,
                    'route' => $error_route
            ]));
        }
        $code = $error_controller::DEFAULT_FATAL_ERROR_CODE;
        $error_controller->$method($code, $error->getMessage(), $error_route->getRequestArguments(), $error_route->getRequestFlags());
        $this->returnResponse();
    }

    /**
     * Exceptions generated during error controller execution
     * will arrive at this method.
     * 
     * This indicates the runtime is terminally broken.
     * 
     * @param \Exception $error
     * @param \Oroboros\core\interfaces\controller\ErrorControllerInterface|null $error_controller
     * @return void
     */
    private function handleUncaughtUnhandledException(\Exception $error, \Oroboros\core\interfaces\controller\ErrorControllerInterface $error_controller = null): void
    {
        $this->getLogger()->critical(sprintf('[type][scope][class] An uncaught '
                . 'exception of type [exception] was unhandled by the bootstrap.'
                . '%1$s%2$s- Message: [message]'
                . '%1$s%2$s- File: [file], line [line]'
                . '%1$s%2$s- Trace: [trace]'
                , PHP_EOL, '    '), [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'exception' => get_class($error),
            'message' => $error->getMessage(),
            'line' => $error->getLine(),
            'file' => $error->getFile(),
            'trace' => $error->getTraceAsString(),
        ]);
        if ($this->hasEventQueue('error')) {
            $this->scopeEventQueue('error');
        }
        if ($this->hasFilterQueue('error')) {
            $this->scopeFilterQueue('error');
        }
        if ($this->hasEvent('exception-uncaught-unhandled')) {
            $this->fireEvent('exception-uncaught-unhandled', $this->getBootstrapEventArguments([
                    'exception' => $error,
                    'error-controller' => $error_controller
            ]));
        }
        $this->setObserverEvent('error-uncaught-unhandled');
        if ($this::app()->debugEnabled()) {
            d('Failed to handle an uncaught error. Program must terminate.', get_class($error), $error->getMessage(), $error->getCode(), $error->getTrace());
        }
    }

    /**
     * This will handle an invalid class exception, if any occur.
     * 
     * It functions identically to handling an uncaught unhandled exception,
     * except it will also fire an invalid class error event if the bootstrap has one.
     * 
     * @param \Oroboros\core\exception\core\InvalidClassException $e
     * @return void
     */
    private function handleInvalidClassException(\Oroboros\core\exception\core\InvalidClassException $e, \Oroboros\core\interfaces\controller\ErrorControllerInterface $error_controller = null): void
    {
        $this->getLogger()->alert(sprintf('[type][scope][class] An invalid class '
                . 'has been detected via exception [exception].'
                . '%1$s%2$s- Message: [message]'
                . '%1$s%2$s- File: [file], line [line]'
                . '%1$s%2$s- Trace: [trace]'
                , PHP_EOL, '    '), [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'exception' => get_class($e),
            'message' => $e->getMessage(),
            'line' => $e->getLine(),
            'file' => $e->getFile(),
            'trace' => $e->getTraceAsString(),
        ]);
        if ($this->hasEventQueue('error')) {
            $this->scopeEventQueue('error');
        }
        if ($this->hasFilterQueue('error')) {
            $this->scopeFilterQueue('error');
        }
        if ($this->hasEvent('invalid-class')) {
            $this->fireEvent('invalid-class', $this->getBootstrapEventArguments([
                    'exception' => $e,
                    'error-controller' => $error_controller
            ]));
        }
        $this->setObserverEvent('invalid-class');
        $this->handleUncaughtUnhandledException($e, $error_controller);
    }

    /**
     * Instantiates, registers and activates the error handler
     * 
     * @return void
     */
    private function initializeErrorHandler(): void
    {
        if (!is_null(self::$error_handler)) {
            // already done
            return;
        }
        $class = static::ERROR_HANDLER_CLASS;
        $object = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $object->setLogger($this->getLogger());
        self::$error_handler = $object;
    }
}
