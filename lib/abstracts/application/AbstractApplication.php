<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\abstracts\application;

/**
 * Abstract Application
 * Provides base level abstraction for declaring a
 * standalone application using this system.
 *
 * @author Brian Dayhoff
 */
abstract class AbstractApplication extends \Oroboros\core\abstracts\library\AbstractLibrary implements \Oroboros\core\interfaces\application\ApplicationInterface
{

    use \Oroboros\core\traits\pattern\ObservableTrait;
    use \Oroboros\core\traits\pattern\ObserverTrait;
    use \Oroboros\core\traits\pattern\EventAwareTrait;
    use \Oroboros\core\traits\pattern\FilterAwareTrait;
    use \Oroboros\core\traits\model\ModelAwareUtilityTrait;

    const CLASS_TYPE = 'application';

    /**
     * The bootstrap class used to bootstrap http requests.
     * Must be a class implementing Oroboros\core\interfaces\bootstrap\BootstrapInterface
     */
    const HTTP_BOOTSTRAP = 'Oroboros\\core\\bootstrap\\HttpBootstrap';

    /**
     * The bootstrap class used to bootstrap command line requests
     * Must be a class implementing Oroboros\core\interfaces\bootstrap\BootstrapInterface
     */
    const CLI_BOOTSTRAP = 'Oroboros\\core\\bootstrap\\CliBootstrap';

    /**
     * The environment variables defined at application bootstrap.
     * @var Oroboros\core\interfaces\library\container\EnvironmentContainerInterface
     */
    private static $environment = null;

    /**
     * The error handler defined at application bootstrap.
     * @var Oroboros\core\interfaces\library\error\ErrorHandlerInterface
     */
    private static $error_handler = null;

    /**
     *
     * @var Oroboros\core\interfaces\library\container\ConfigCollectionInterface
     */
    private static $config = null;

    /**
     *
     * @var Oroboros\core\interfaces\library\container\ConfigCollectionInterface
     */
    private static $definition = null;

    /**
     * Returns the compiled raw environment object
     * All classes can reach this via `static::app()->environment()`
     * 
     * @return \Oroboros\core\interfaces\library\container\EnvironmentContainerInterface
     */
    final public static function environment(): \Oroboros\core\interfaces\library\container\EnvironmentContainerInterface
    {
        return clone self::$environment;
    }

    /**
     * Returns a copy of the application config.
     * This does not return the original object, which is preserved as immutable.
     * This contains all of the application definer data
     * All classes can reach this via `static::app()->config()`
     * 
     * @return \Oroboros\core\interfaces\library\container\ConfigCollectionInterface
     */
    final public static function config(): \Oroboros\core\interfaces\library\container\ConfigCollectionInterface
    {
        return clone self::$config;
    }

    /**
     * Default bootstrap constructor.
     * Bootstrap objects use the standard constructor.
     * 
     * @param string $command
     * @param array $arguments
     * @param array $flags
     */
    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeApplication();
        $this->setObserverEvent('ready');
    }

    /**
     * Default bootstrap destructor.
     * Fires teardown and cleanup events throughout the system
     */
    public function __destruct()
    {
        $this->setObserverEvent('teardown');
        if ($this->hasFilterQueue('teardown')) {
            $this->scopeFilterQueue('teardown');
        }
        if ($this->hasEventQueue('teardown')) {
            $this->scopeEventQueue('teardown');
            if ($this->hasEvent('cleanup')) {
                $this->fireEvent('cleanup');
            }
        }
    }

    /**
     * Returns base namespace of the application.
     * This is used for Psr-4 registration.
     * @return string
     */
    abstract public function getBaseNamespace(): string;

    /**
     * Returns the root directory of the application.
     * @return string
     */
    abstract public function getApplicationRoot(): string;

    /**
     * Returns the source directory of the application.
     * This is used for Psr-4 autoload registration.
     * @return string
     */
    abstract public function getApplicationSourceDirectory(): string;

    /**
     * Dependency injection method for the environment variables defined at application bootstrap.
     * @param \Oroboros\core\interfaces\library\container\EnvironmentContainerInterface $environment
     * @return void
     * @throws \ErrorException If attempting to set the environment container a second time.
     */
    final public function setEnvironment(\Oroboros\core\interfaces\library\container\EnvironmentContainerInterface $environment): void
    {
        if (!is_null(self::$environment)) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. '
                        . 'Cannot set the application environment again after initial definition.', get_class($this)));
        }
        self::$environment = $environment;
    }

    /**
     * Used only by the Core to bootstrap
     * Cannot be called twice
     * 
     * @param \Oroboros\core\interfaces\library\container\ConfigCollectionInterface $config
     * @return void
     * @throws \Oroboros\core\exception\ErrorException
     */
    final public function setConfiguration(\Oroboros\core\interfaces\library\container\ConfigCollectionInterface $config): void
    {
        if (!is_null(self::$config)) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. '
                        . 'Cannot set the application configuration again after initial definition.', get_class($this)));
        }
        self::$config = $config;
    }
    
    /**
     * Sets the currently scoped error handler for the runtime
     * 
     * @param \Oroboros\core\interfaces\library\error\ErrorHandlerInterface $error_handler
     * @return void
     * @throws \Oroboros\core\exception\ErrorException
     */
    final public function setErrorHandler(\Oroboros\core\interfaces\library\error\ErrorHandlerInterface $error_handler): void
    {
        if (!is_null(self::$error_handler)) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. '
                        . 'Cannot replace the application error handler.', get_class($this)));
        }
        self::$error_handler = $error_handler;
        // Uncomment when the error handler works correctly
//        self::$error_handler->activate();
//        self::$error_handler->register();
    }
    
    /**
     * Returns the currently scoped error handler object.
     * 
     * @return \Oroboros\core\interfaces\library\error\ErrorHandlerInterface
     */
    public function getErrorHandler(): \Oroboros\core\interfaces\library\error\ErrorHandlerInterface
    {
        return self::$error_handler;
    }

    public function getApplicationDefinition()
    {
        $definition = $this->getModel('app')->fetch();
        return $definition;
    }

    public function getApplicationEnvironment()
    {
        $default = $this->environment()->get('baseline')->get('environment');
        if ($this->environment()->get('application')->has('environment')) {
            return $this->environment()->get('application')->get('environment');
        }
        return $default;
    }

    /**
     * Returns the currently scoped domain name,
     * or "localhost" if it could not resolve.
     * CLI will always be "localhost"
     * All classes can reach this via `static::app()->getApplicationDomain()`
     * 
     * @return string
     */
    public function getApplicationDomain()
    {
        $default = 'localhost';
        $environment = $this->getApplicationEnvironment();
        $definition = $this->getApplicationDefinition();
        if ($definition->has('domain')) {
            if (is_array($definition->get('domain')) && property_exists($definition->get('domain'), $environment)) {
                return $definition->get('domain')[$environment];
            } elseif (is_string($definition->get('domain'))) {
                return $definition->get('domain');
            }
        }
        return $default;
    }

    /**
     * Returns any additional controller scopes that should be considered valid.
     * This is used for enforcing domain logic when routing.
     * Duplicate scopes will not be ignored.
     * @return array
     */
    public function getApplicationControllerScopes(): array
    {
        return [];
    }

    /**
     * Returns the source directory of application templates.
     * This is used for template overrides.
     * Return null if not applicable.
     * @return string|null
     */
    public function getApplicationTemplateDirectory(): ?string
    {
        return OROBOROS_CORE_TEMPLATES;
    }

    /**
     * Returns the config folder of the application.
     * This is used for parsing static configurations.
     * Return null if not applicable.
     * @return string|null
     */
    public function getApplicationConfigDirectory(): ?string
    {
        $path = $this->getApplicationRoot() . 'config' . DIRECTORY_SEPARATOR;
        if (is_dir($path)) {
            return $path;
        } elseif (is_dir(OROBOROS_CORE_BASE_DIRECTORY . 'config' . DIRECTORY_SEPARATOR)) {
            return OROBOROS_CORE_BASE_DIRECTORY . 'config' . DIRECTORY_SEPARATOR;
        }
        return null;
    }

    /**
     * Returns the public folder of the application.
     * This is used for generating links to frontend content.
     * Return null if not applicable.
     * @return string|null
     */
    public function getApplicationPublicDirectory(): ?string
    {
        return null;
    }

    /**
     * Returns an associative array of extensions.
     * The key is the extension slug, and the value is
     * the path to the extension folder.
     * 
     * A valid extension must include a manifest.json file
     * within the supplied directory.
     * 
     * @return array
     */
    public function getApplicationExtensions(): array
    {
        $extensions = [];
        $file = $this->getApplicationConfigDirectory() . 'extensions.json';
        if (is_readable($file)) {
            $parsed = $this->load('library', 'parser\\JsonParser', $file)->fetch();
            foreach ($parsed as $key => $path) {
                if (is_dir($this->getApplicationRoot() . $path)) {
                    $extensions[$key] = $this->getApplicationRoot() . $path;
                } elseif (is_dir(OROBOROS_CORE_BASE_DIRECTORY . $path)) {
                    $extensions[$key] = OROBOROS_CORE_BASE_DIRECTORY . $path;
                }
            }
        }
        return $extensions;
    }

    /**
     * Returns an associative array of modules.
     * The key is the module slug, and the value is
     * the path to the module folder.
     * 
     * A valid module must include a manifest.json file
     * within the supplied directory, or be registered
     * via an extension.
     * 
     * @return array
     */
    public function getApplicationModules(): array
    {
        $modules = [];
        $file = $this->getApplicationConfigDirectory() . 'modules.json';
        if (is_readable($file)) {
            $parsed = $this->load('library', 'parser\\JsonParser', $file)->fetch();
            foreach ($parsed as $key => $path) {
                if (is_dir($this->getApplicationRoot() . $path)) {
                    $modules[$key] = $this->getApplicationRoot() . $path;
                } elseif (is_dir(OROBOROS_CORE_BASE_DIRECTORY . $path)) {
                    $modules[$key] = OROBOROS_CORE_BASE_DIRECTORY . $path;
                }
            }
        }
        return $modules;
    }

    /**
     * Returns an associative array of services.
     * The key is the service slug, and the value is
     * the path to the service folder.
     * 
     * A valid service must include a manifest.json file
     * within the supplied directory, or be registered
     * by an extension or module.
     * 
     * @return array
     */
    public function getApplicationServices(): array
    {
        $services = [];
        $file = $this->getApplicationConfigDirectory() . 'services.json';
        if (is_readable($file)) {
            $parsed = $this->load('library', 'parser\\JsonParser', $file)->fetch();
            foreach ($parsed as $key => $path) {
                if (is_dir($this->getApplicationRoot() . $path)) {
                    $services[$key] = $this->getApplicationRoot() . $path;
                } elseif (is_dir(OROBOROS_CORE_BASE_DIRECTORY . $path)) {
                    $services[$key] = OROBOROS_CORE_BASE_DIRECTORY . $path;
                }
            }
        }
        return $services;
    }

    /**
     * Returns the source folder for frontend scripts and stylesheet source.
     * This is used for running build tasks for the application.
     * Return null if not applicable.
     * @return string|null
     */
    public function getApplicationEtcDirectory(): ?string
    {
        return null;
    }

    /**
     * Returns the unit test directory.
     * This is used for running automated unit tests.
     * Return null if not applicable.
     * @return string|null
     */
    public function getApplicationTestDirectory(): ?string
    {
        return null;
    }

    /**
     * Returns the full classname of the bootstrap class used to boot the application.
     * Must be a class implementing Oroboros\core\interfaces\bootstrap\BootstrapInterface
     * @return string
     */
    public function getBootstrapClass(): string
    {
        if (\Oroboros\core\interfaces\environment\EnvironmentInterface::IS_CLI) {
            return static::CLI_BOOTSTRAP;
        }
        return static::HTTP_BOOTSTRAP;
    }

    /**
     * Return an array of routes to populate the router.
     * @param array|null $routes If supplied from a child class,
     *     the routes passed back will take precedence over the base routes.
     *     If an application does not want to include default routes,
     *     return the routes directly. If including the default routes
     *     is desireable, return `parent::getRoutes($your_new_routes);`
     * @return array
     */
    public function getRoutes(array $routes = null): array
    {
        $new_routes = [];
        if (is_null($routes)) {
            $routes = [];
        }
        $file = $this->getApplicationConfigDirectory() . 'routes.json';
        if (is_readable($file)) {
            $new_routes = json_decode(file_get_contents($file), 1);
        }
        return array_replace_recursive($new_routes, $routes);
    }

    /**
     * Return an array of component declarations.
     * @param array|null $components If supplied from a child class,
     *     the components passed back will take precedence over the
     *     base components on duplicate keys, and details not supplied
     *     for components will use default values. If an application
     *     does not want to utilize the underlying components, then it
     *     should just return its own set. If it wants to include
     *     default components also or inherit defaults,
     *     return `parent::getComponents($your_new_components);`
     * @param array $components
     * @return array
     */
    public function getComponents(array $components = null): array
    {
        $new_components = [];
        if (is_array($components)) {
            $new_components = $components;
        }
        $file = $this::getApplicationConfigDirectory() . 'components.json';
        $components = json_decode(file_get_contents($file), 1);
        return array_replace_recursive($components, $new_components);
    }

    /**
     * Return a command, if any, that should be passed to the
     * bootstrap object on application instantiation.
     * Must explicitly return a string or null.
     * May not return void.
     * @return string|null
     */
    public function getBootstrapCommand(): ?string
    {
        return null;
    }

    /**
     * Return commands, if any, that should be passed to the
     * bootstrap object on application instantiation.
     * Must explicitly return an array or null.
     * May not return void.
     * May not return a container.
     * @return array|null
     */
    public function getBootstrapArguments(): ?array
    {
        return null;
    }

    /**
     * Return flags, if any, that should be passed to the
     * bootstrap object on application instantiation.
     * Must explicitly return an array or null.
     * May not return void.
     * May not return a container.
     * @return array|null
     */
    public function getBootstrapFlags(): ?array
    {
        return null;
    }

    /**
     * Returns the filename of the application config file.
     * @return string
     */
    public function getApplicationConfig(): string
    {
        return $this->getApplicationConfigDirectory() . 'app-default.json';
    }

    /**
     * Returns a boolean designation as to whether debug mode is enabled or not.
     * 
     * By default this returns `true` for CLI runtimes if the `--debug` flag
     * was passed, and `false` otherwise for all cli sessions.
     * 
     * For http, this returns `true` if any of the following apply:
     * - the constant `OROBOROS_CORE_DEBUG` is defined and is `true`
     * - the environment variable `OROBOROS_CORE_DEBUG` exists and is `true`
     * - either the application `.env` or `.env.defaults` has defined key
     *   `OROBOROS_CORE_DEBUG` as `true`, and it is not overridden by
     *   an explicitly `false` definition from one of the previous.
     * 
     * The entire system defers to this method to determine whether
     * debug context applies or not.
     * 
     * You may override this in your application if you want to use
     * an alternate debug definition system.
     * 
     * @return bool
     */
    public function debugEnabled(): bool
    {
        if ($this::IS_CLI) {
            // Cli debug is based on the actual debug flag,
            // so it doesn't get constantly spammy from
            // unwanted debug output if the .env value is set.
            global $argv;
            global $argc;
            if (is_array($argv) && in_array('--debug', $argv, true)) {
                return true;
            }
            return false;
        }
        // All other cases use the environment or constant definition
        return $this::DEBUG;
    }

    /**
     * Sets the observable name for the view
     * @return string
     */
    protected function declareObservableName(): string
    {
        return trim(sprintf('%1$s:%2$s', static::CLASS_TYPE, static::CLASS_SCOPE), ':');
    }

    /**
     * Initializes the application
     * @return void
     */
    private function initializeApplication(): void
    {
        $this->initializeObservable();
        $this->initializeObserver();
        $this->initializeEvents();
        $this->initializeFilters();
    }
}
