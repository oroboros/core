/* 
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
'use strict';
(function (core) {
    const oroboros = core();
    const component = 'vertical-navbar';
    const abstract = oroboros.abstract('component');
    const toggle = '[data-toggle="collapse-left"]';
    const toggle_class = 'collapse-left';
    const lib = {
        is_initialized: false,
        init: function () {
            if (!this.is_initialized)
            {
                this.collapse.init();
                this.is_initialized = true;
            }
        },
        collapse: {
            selector: null,
            is_initialized: false,
            init: function () {
                if (!this.is_initialized)
                {
                    let selector;
                    this.selector = selector = jQuery(toggle);
                    this.selector.on('click', function (e) {
                        let target;
                        let transitioning = false;
                        let target_id;
                        let transition_event = function (event) {
                            if (transitioning === true)
                            {
                                event.stopPropagation();
                                return false;
                            }
                            transitioning = true;
                            target.off('animationend');
                            target.removeClass(toggle_class);
                            if (target.hasClass('slide-out'))
                            {
                                target.addClass('hide');
                                target.removeClass('show');
                            } else {
                                target.addClass('show');
                                target.removeClass('hide');
                            }
                            transitioning = false;
                            target.addClass(toggle_class);
                            target.removeClass('slide-in');
                            target.removeClass('slide-out');
                            event.stopPropagation();
                            return false;
                        };
                        target_id = jQuery(this).attr('data-target');
                        if (!target_id instanceof String)
                        {
                            return false;
                        }
                        target = jQuery('#' + target_id);
                        target.on('animationend', transition_event);
                        if (target.hasClass('show'))
                        {
                            // close the drawer
                            target.addClass('slide-out');
                            target.removeClass('show');
                            jQuery('#' + target_id + ' .dropdown-menu').removeClass('show');
                        } else if (target.hasClass('hide')) {
                            // open the drawer
                            target.addClass('slide-in');
                            target.removeClass('hide');
                        }
                        e.stopPropagation();
                        return false;
                    });
                    this.is_initialized = true;
                }
            }
        },

    };
    const VerticalNavbar = class VerticalNavbar extends abstract {

        static classKey()
        {
            return component;
        }

        constructor(id, args, flags) {
            super(id, args, flags);
        }
    }
    lib.init();
    oroboros.register('component', component, VerticalNavbar);
})(Oroboros);

