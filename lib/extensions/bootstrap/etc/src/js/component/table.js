/* 
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
'use strict';
(function (core) {
    const oroboros = core();
    const component = 'table';
    const component_row = 'table-row';
    const component_column = 'table-column';
    const component_cell = 'table-cell';
    const abstract = oroboros.abstract('component');
    const lib = {
        is_initialized: false,
        init: function () {
            if (!this.is_initialized)
            {
                this.ui.init();
                this.datatables.init();
                this.is_initialized = true;
            }
        },
        ui: {
            is_initialized: false,
            init: function () {
                if (!this.is_initialized)
                {
                    this.search.init();
                    this.pagination.init();
                    this.is_initialized = true;
                }
            },
            search: {
                registered: null,
                is_initialized: false,
                init: function () {
                    if (!this.is_initialized)
                    {
                        this.registered = {};
                        this.is_initialized = true;
                    }
                },
                register: function (id, search) {
                    if (!this.registered.hasOwnProperty(id))
                    {
                        this.registered[id] = search;
                    }
                },
                has: function (id) {
                    return this.registered.hasOwnProperty(id);
                }
            },
            pagination: {
                registered: null,
                is_initialized: false,
                init: function () {
                    if (!this.is_initialized)
                    {
                        this.registered = {};
                        this.is_initialized = true;
                    }
                },
                register: function (id, search) {
                    if (!this.registered.hasOwnProperty(id))
                    {
                        this.registered[id] = search;
                    }
                },
                has: function (id) {
                    return this.registered.hasOwnProperty(id);
                }
            }
        },
        datatables: {
            registered: null,
            is_initialized: false,
            init: function () {
                if (!this.is_initialized)
                {
                    this.registered = {};
                    this.is_initialized = true;
                }
            },
            register: function (id, datatable) {
                if (!this.registered.hasOwnProperty(id))
                {
                    this.registered[id] = datatable;
                }
            },
            has: function (id) {
                return this.registered.hasOwnProperty(id);
            }
        }
    };
    const Table = class Table extends abstract {

        static classKey()
        {
            return component;
        }

        constructor(id, args, flags) {
            super(id, args, flags);
            this.is_datatable = false;
            this.datatable_driver = null;
            this.columns = [];
            this.rows = [];
        }

        setElement(element, preserve) {
            let is_datatable;
            if (!(element instanceof HTMLTableElement))
            {
                throw new TypeError('Provided argument [element] must be an instance of [HTMLTableElement]');
            }
            super.setElement(element, preserve);
            this.initializeRows();
            this.initializeColumns();
            is_datatable = this.class.includes('datatable');
            this.is_datatable = is_datatable;
            if (this.is_ready)
            {
                // Do not perform this action if the dom isn't loaded yet.
                this.releaseDataTableDriver();
                this.datatable_driver = this.getDataTableDriver();
            }
            return this;
        }

        onReady(subject) {
            super.onReady(subject);
            subject.is_ready = true;
            if (subject.class instanceof DOMTokenList)
            {
                subject.is_datatable = subject.class.contains('datatable');
            } else {
                subject.is_datatable = subject.class.includes('datatable');
            }
            subject.datatable_driver = subject.getDataTableDriver();
            if (subject.datatable_driver != null)
            {
                subject.initializeDataTable();
            }
            let selector = (function () {

            })();
        }

        getDataTableDriver()
        {
            let datatable_driver;
            if (this.is_datatable === false)
            {
                return null;
            }
            if (lib.datatables.has(this.id))
            {
                return lib.datatables.registered[this.id];
            }
            datatable_driver = jQuery(this.element).DataTable(this.getDataTableAttributes());
            lib.datatables.register(this.id, datatable_driver);
            return datatable_driver;
        }

        releaseDataTableDriver()
        {
            if (!this.is_datatable || this.datatable_driver === null)
            {
                return;
            }
            this.datatable_driver.destroy();
            this.datatable_driver = null;
        }

        getDataTableAttributes() {
            let attributes = {
                retrieve: true,
                responsive: true
            };
            return attributes;
        }

        initializeRows() {
            let rows = this.document.querySelectorAll('#' + this.element.id + ' tr');
            for (let i of rows) {
                this.rows.push(new TableRow(this.rows.length, {
                    'table': this.element,
                    'element': rows[i]
                }, this.flags));
            }
        }

        initializeColumns() {
            let columns = this.document.querySelectorAll('#' + this.element.id + ' tr:first-child th');
            let column_count = columns.length;
            for (let i of columns)
            {
                this.columns[column_count] = new TableColumn(column_count, {
                    'table': this.element,
                    'rows': this.rows,
                    'element': columns[i],
                    'offset': columns.length - column_count
                }, this.flags);
            }
        }

        initializeDataTable() {
            jQuery('#' + this.element.id + '_wrapper.dataTables_wrapper > div.row:first-child').hide();
            this.initializeSearch();
        }

        initializeSearch() {
            if (this.is_datatable && !lib.ui.search.has(this.id))
            {
                let subject = this;
                let table = this.datatable_driver;
                let ui_target = jQuery('#' + this.element.id + '-header');
                let searchbar = document.createElement('div');
                let searchbar_wrapper = document.createElement('span');
                let searchbar_count_wrapper = document.createElement('span');
                let searchbar_icon_wrapper = document.createElement('span');
                let searchbar_icon = document.createElement('i');
                let searchbar_count_icon_wrapper = document.createElement('span');
                let searchbar_count_icon = document.createElement('i');
                let searchbar_input_wrapper = document.createElement('span');
                let searchbar_input = document.createElement('input');
                let searchbar_count_input_wrapper = document.createElement('span');
                let searchbar_count_input = document.createElement('input');
                searchbar_input.setAttribute('type', 'text');
                searchbar_input.setAttribute('id', this.element.id + '-searchbar-input');
                searchbar_input.setAttribute('class', 'form-control collapse py-0 my-0');
                searchbar_input.setAttribute('placeholder', 'Search');
                searchbar_input.setAttribute('aria-label', 'Search');
                searchbar_input.setAttribute('data-context', 'permissions-searchbar');
                searchbar_input.setAttribute('data-component', 'searchbar');
                searchbar_count_input.setAttribute('type', 'number');
                searchbar_count_input.setAttribute('step', '1');
                searchbar_count_input.setAttribute('min', '1');
                searchbar_count_input.setAttribute('max', '50');
                searchbar_count_input.setAttribute('id', this.element.id + '-searchbar-page-count');
                searchbar_count_input.setAttribute('class', 'form-control collapse py-0 my-0');
                searchbar_count_input.setAttribute('placeholder', 'Pagination');
                searchbar_count_input.setAttribute('aria-label', 'Pagination');
                searchbar_count_input.setAttribute('data-context', 'permissions-searchbar');
                searchbar_count_input.setAttribute('data-component', 'searchbar');
//                searchbar_input.setAttribute('data-parent', this.element.id);
                searchbar_icon_wrapper.setAttribute('class', 'px-2 waves-effect waves-light');
                searchbar_icon_wrapper.setAttribute('role', 'button');
                searchbar_icon.setAttribute('class', 'fas fa-search text-primary');
                searchbar_count_icon_wrapper.setAttribute('class', 'px-2 waves-effect waves-light');
                searchbar_count_icon_wrapper.setAttribute('role', 'button');
                searchbar_count_icon.setAttribute('class', 'fas fa-sort-numeric-down text-primary');
                searchbar_wrapper.setAttribute('class', 'd-flex justify-content-start align-items-center md-form');
                searchbar_count_wrapper.setAttribute('class', 'd-flex justify-content-start align-items-center md-form');
                searchbar.setAttribute('id', this.element.id + '-searchbar');
                searchbar.setAttribute('class', 'col-sm-12 col-md-6 d-flex flex-row justify-content-end ml-auto');
                searchbar_input_wrapper.appendChild(searchbar_input);
                searchbar_count_input_wrapper.appendChild(searchbar_count_input);
                searchbar_icon_wrapper.appendChild(searchbar_icon);
                searchbar_count_icon_wrapper.appendChild(searchbar_count_icon);
                searchbar_wrapper.appendChild(searchbar_icon_wrapper);
                searchbar_wrapper.appendChild(searchbar_input_wrapper);
                searchbar_count_wrapper.appendChild(searchbar_count_icon_wrapper);
                searchbar_count_wrapper.appendChild(searchbar_count_input_wrapper);
                searchbar.appendChild(searchbar_wrapper);
                searchbar.appendChild(searchbar_count_wrapper);
                searchbar_icon_wrapper.setAttribute('data-toggle', 'collapse');
                searchbar_icon_wrapper.setAttribute('data-target', '#' + this.element.id + '-searchbar-input');
                searchbar_icon_wrapper.setAttribute('aria-controls', this.element.id + '-searchbar-input');
                searchbar_count_icon_wrapper.setAttribute('data-toggle', 'collapse');
                searchbar_count_icon_wrapper.setAttribute('data-target', '#' + this.element.id + '-searchbar-page-count');
                searchbar_count_icon_wrapper.setAttribute('aria-controls', this.element.id + '-searchbar-page-count');
                jQuery(searchbar_icon_wrapper).unbind('click').on('click', function (event) {
                    let searchfield = jQuery(searchbar_input);
                    event.preventDefault();
                    searchfield.collapse('show');
                    searchfield.focus();

                });
                jQuery(searchbar_input).on('blur', function () {
                    jQuery(this).collapse('hide');
                });
                jQuery(searchbar_count_icon_wrapper).on('click', function (event) {
                    let searchfield = jQuery(searchbar_count_input);
                    event.preventDefault();
                    searchfield.collapse('show');
                    searchfield.focus();

                });
                jQuery(searchbar_count_input).on('blur', function () {
                    jQuery(this).collapse('hide');
                });
                jQuery(searchbar_count_input).on('change', function () {
                    subject.onPageSize(jQuery(this).val());
                });
                ui_target.append(searchbar);
                jQuery(searchbar_input).on('keyup', function () {
                    subject.onSearch(jQuery(this).val());
                });
                lib.ui.search.register(this.id, searchbar);
            }
        }

        onSearch(term)
        {
            if (this.is_datatable)
            {
                this.library.debounce(this.datatable_driver.search(term).draw(), 250);
            }
        }

        onPageSize(size)
        {
            if (this.is_datatable)
            {
                this.library.debounce(this.datatable_driver.page.len(size).draw(), 250);
            }
        }
    }

    const AbstractTableElement = class AbstractTableElement extends abstract {

        static classKey()
        {
            return 'abstract-table-element';
        }

        constructor(id, args, flags) {
            super(id, args, flags);
            this.is_datatable = false;
            this.datatable_driver = null;
            this.table_element = null;
            if (this.args.hasOwnProperty('driver'))
            {
                this.setDataTableDriver(this.args.driver);
            }
            if (this.args.hasOwnProperty('table'))
            {
                this.setDataTableElement(this.args.table);
            }
        }

        setTableElement(element) {
            if (!(element instanceof HTMLTableElement))
            {
                throw new TypeError('Provided element must be an instance of [HTMLTableElement]');
            }
            this.table_element = element;
        }

        setDataTableDriver(driver) {
            this.is_datatable = true;
            this.datatable_driver = driver;
        }
    }

    const TableRow = class TableRow extends AbstractTableElement {

        static classKey()
        {
            return component_row;
        }

        constructor(id, args, flags) {
            super(id, args, flags);
            this.row = null;
            this.cells = [];
        }

        setElement(element, preserve) {
//            if (!(element instanceof HTMLTableElement))
//            {
//                throw new TypeError('Provided argument [element] must be an instance of [HTMLTableElement]');
//            }
            super.setElement(element, preserve);
            this.initializeCells();
            return this;
        }

        initializeCells()
        {
            let cells = [], cell, cellargs;
            for (let i in this.element.children)
            {
                cellargs = {
                    'element': this.element.children[i]
                };
                cell = new TableCell(cells.length, {
                    'element': this.element.children[i]
                }, this.flags)
                cells.push(cell);
            }
            this.cells = cells;
        }
    }

    const TableColumn = class TableColumn extends AbstractTableElement {

        static classKey()
        {
            return component_column;
        }

        constructor(id, args, flags) {
            super(id, args, flags);
            this.rows = [];
            this.cells = [];
            this.offset = NaN;
            if (this.args.hasOwnProperty('offset'))
            {
                this.offset = this.args.offset;
            }
            if (this.args.hasOwnProperty('rows'))
            {
                this.rows = this.args.rows;
                this.initializeCells();
            }
        }

        setElement(element, preserve) {
//            if (!(element instanceof HTMLTableElement))
//            {
//                throw new TypeError('Provided argument [element] must be an instance of [HTMLTableElement]');
//            }
            super.setElement(element, preserve);
            this.initializeCells();
            return this;
        }

        initializeCells()
        {
            let cells = [];

        }
    }

    const TableCell = class TableCell extends AbstractTableElement {

        static classKey()
        {
            return component_cell;
        }

        constructor(id, args, flags) {
            super(id, args, flags);
        }

        setElement(element, preserve) {
//            if (!(element instanceof HTMLTableElement))
//            {
//                throw new TypeError('Provided argument [element] must be an instance of [HTMLTableElement]');
//            }
            super.setElement(element, preserve);
            return this;
        }
    }

    lib.init();
    oroboros.register('component', component, Table);
    oroboros.register('component', component_row, TableRow);
})(Oroboros);
