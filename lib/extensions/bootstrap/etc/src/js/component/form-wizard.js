/* 
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
'use strict';
(function (core) {
    const oroboros = core();
    const component = 'form-wizard';
    const component_wizard_section = 'form-wizard-section';
    const component_wizard_ui = 'form-wizard-ui';
    const component_wizard_form = 'form-wizard-form';
    const abstract = oroboros.abstract('component');
    const form_abstract = oroboros.extend('component', 'form');
    const lib = {
        init: function () {

        }
    };

    const FormWizard = class FormWizard extends abstract {

        static classKey()
        {
            return component;
        }

        constructor(id, args, flags) {
            super(id, args, flags);
            this.active = null;
            this.ui = null;
            this.sections = {};
            this.form = null;
            this.validation = {};
            this.selection_order = {};
            this.setElement(this.args.element);
        }

        activate(section) {
            let current = this.active;
            let id = section.id;
            if (current != null)
            {
                current.deactivate();
            }
            this.active = section;
            this.form.setCurrent(section);
            this.ui.setCurrent(section);
        }

        next(active) {
            let key = null;
            if (this.active == active || this.active == null) {
                this.active = active;
                key = jQuery(this.element).find('a[data-toggle="tab"][href="#' + this.active.element.id + '"]').parent().next().children('a[data-toggle="tab"]').attr('href').replace('#', '');
                this.active = this.sections[key];
                this.active.activate();
                this.form.setCurrent(this.active);
                this.ui.setCurrent(this.active);
            }
            return this;
        }

        previous(active) {
            let key = null;
            if (this.active == active || this.active == null) {
                this.active = active;
                key = jQuery(this.element).find('a[data-toggle="tab"][href="#' + this.active.element.id + '"]').parent().prev().children('a[data-toggle="tab"]').attr('href').replace('#', '');
                this.active = this.sections[key];
                this.active.activate();
                this.form.setCurrent(this.active);
                this.ui.setCurrent(this.active);
            }
            return this;
        }

        getCurrent() {
            return this.active;
        }

        setElement(element, preserve) {
            const subject = this;
            super.setElement(element, preserve);
            let ui_element = document.querySelector('#' + this.element.id + ' .wizard > .wizard-inner ul[data-component="' + component_wizard_ui + '"]');
            let form_element = document.querySelector('#' + this.element.id + ' form[data-component="' + component_wizard_form + '"]');
            let section_elements = document.querySelectorAll('#' + this.element.id + ' form[data-component="' + component_wizard_form + '"] [data-component="' + component_wizard_section + '"]');
            let sections = this.sections = {};
            for (let section of section_elements)
            {
                sections[section.id] = oroboros.create('component', component_wizard_section, section.id, {"element": section});
            }
            this.sections = sections;
            this.ui = oroboros.create('component', component_wizard_ui, this.element.id + '-ui', {"element": ui_element}).setWizard(this);
            this.form = oroboros.create('component', component_wizard_form, this.element.id + '-form', {"element": form_element}).setWizard(this).setUi(this.ui);
            this.ui.setForm(this.form);
            for (let section_key in this.sections)
            {
                this.sections[section_key].setWizard(this);
                this.sections[section_key].setForm(this.form);
                this.sections[section_key].setUi(this.ui);
            }
            this.ui.setSections(this.sections);
            this.form.setSections(this.sections);
            return this;
        }

        onReady(subject) {
            super.onReady(subject);
        }
    }

    const FormWizardUi = class FormWizardUi extends abstract {
        static classKey()
        {
            return component_wizard_ui;
        }

        constructor(id, args, flags) {
            super(id, args, flags);
            this.wizard = null;
            this.sections = null;
            this.form = null;
            this.current_section = null;
        }

        setElement(element, preserve) {
            super.setElement(element, preserve);
            return this;
        }

        setWizard(wizard) {
            if (!(wizard instanceof FormWizard))
            {
                throw new TypeError('Provided wizard element is not an instance of [' + FormWizard.name + ']');
            }
            this.wizard = wizard;
            return this;
        }

        setSections(sections) {
            const wizard = this.wizard;
            const subject = this;
            let selection_order = {};
            this.sections = sections;
            jQuery(this.element).children('li[role="presentation"]').children('a[data-toggle="tab"]').each(function (tab) {
                let target = jQuery(this).attr('href').replace('#', '');
                if (sections.hasOwnProperty(target))
                {
                    selection_order[target] = sections[target];
                }
            });
            this.wizard.selection_order = selection_order;
            return this;
        }

        setCurrent(section) {
            if (section != this.current_section)
            {
                if (this.current_section != null)
                {
                    jQuery(this.current_section.element).removeClass('active');
                    jQuery(this.element).find('a[data-toggle="tab"][href="#' + this.current_section.element.id + '"]').removeClass('active');
                    jQuery(this.element).find('a[data-toggle="tab"][href="#' + this.current_section.element.id + '"]').parent().removeClass('active');
                }
                this.current_section = section;
                jQuery(this.element).find('a[data-toggle="tab"][href="#' + this.current_section.element.id + '"]').addClass('active');
                jQuery(this.element).find('a[data-toggle="tab"][href="#' + this.current_section.element.id + '"]').parent().addClass('active');
                jQuery(this.current_section.element).addClass('active');
            }
        }

        setForm(form) {
            if (!(form instanceof FormWizardForm))
            {
                throw new TypeError('Provided wizard form element is not an instance of [' + FormWizardForm.name + ']');
            }
            this.form = form;
            return this;
        }

        onReady(subject) {
            super.onReady(subject);
            jQuery(subject.element).children('li[role="presentation"]').children('a[data-toggle="tab"]').on('show.bs.tab', function (event) {
                let target = jQuery(event.target);
                let value = event.target.getAttribute('href').replace('#', '');
                let target_object = subject.sections[value];
                subject.wizard.activate(target_object);
//                if (subject.sections.hasOwnProperty(value))
//                {
//                    target_object = subject.sections[value];
//                    if (target_object.active)
//                    {
//                        return false;
//                    }
//                    target_object.activate();
//                }
            });
        }

        onReset(event) {
            return true;
        }

        validateSection()
        {
            return true;
        }
    }

    const FormWizardSection = class FormWizardSection extends abstract {
        static classKey()
        {
            return component_wizard_section;
        }

        constructor(id, args, flags) {
            super(id, args, flags);
            this.wizard = null;
            this.ui = null;
            this.form = null;
            this.active = false;
            this.completed = false;
            this.fields = {};
        }

        setElement(element, preserve) {
            super.setElement(element, preserve);
            let inputs = document.querySelectorAll('#' + element.id + ' input');
            let selects = document.querySelectorAll('#' + element.id + ' select');
            let textareas = document.querySelectorAll('#' + element.id + ' textarea');
            let buttons = document.querySelectorAll('#' + element.id + ' button');
            let submits = document.querySelectorAll('#' + element.id + ' submit');
            this.fields = {};
            for (let field of inputs)
            {
                if (field.hasAttribute('name'))
                {
                    this.fields[field.getAttribute('name')] = field;
                }
            }
            for (let field of selects)
            {
                if (field.hasAttribute('name'))
                {
                    this.fields[field.getAttribute('name')] = field;
                }
            }
            for (let field of textareas)
            {
                if (field.hasAttribute('name'))
                {
                    this.fields[field.getAttribute('name')] = field;
                }
            }
            for (let field of buttons)
            {
                if (field.hasAttribute('name'))
                {
                    this.fields[field.getAttribute('name')] = field;
                }
            }
            for (let field of submits)
            {
                if (field.hasAttribute('name'))
                {
                    this.fields[field.getAttribute('name')] = field;
                }
            }
            return this;
        }

        setWizard(wizard) {
            const subject = this;
            if (!(wizard instanceof FormWizard))
            {
                throw new TypeError('Provided wizard element is not an instance of [' + FormWizard.name + ']');
            }
            this.wizard = wizard;
            this.setUi(this.wizard.ui).setForm(this.wizard.form);
            jQuery('#' + this.element.id + ' .tab-nav > .tab-nav-ui button.next-step').click(function (event) {
                subject.wizard.next(subject);
                return false;
            });
            jQuery('#' + this.element.id + ' .tab-nav > .tab-nav-ui button.prev-step').click(function (event) {
                subject.wizard.previous(subject);
                return false;
            });
            if (this.element.classList.contains('active'))
            {
                this.activate();
            }
            return this;
        }

        setUi(ui) {
            if (!(ui instanceof FormWizardUi))
            {
                throw new TypeError('Provided wizard ui element is not an instance of [' + FormWizardUi.name + ']');
            }
            this.ui = ui;
            return this;
        }

        setForm(form) {
            if (!(form instanceof FormWizardForm))
            {
                throw new TypeError('Provided wizard form element is not an instance of [' + FormWizardForm.name + ']');
            }
            this.form = form;
            return this;
        }

        activate() {
            this.active = true;
            this.wizard.activate(this);
            return this;
        }

        deactivate() {
            this.active = false;
            return this;
        }

        onReady(subject) {
            super.onReady(subject);
        }

        onReset(event) {
            return true;
        }

        validateSection()
        {
            return true;
        }
    }

    const FormWizardForm = class FormWizardForm extends form_abstract {
        static classKey()
        {
            return component_wizard_form;
        }

        constructor(id, args, flags) {
            super(id, args, flags);
            this.wizard = null;
            this.ui = null;
            this.sections = null;
            this.current = null;
        }

        setElement(element, preserve) {
            super.setElement(element, preserve);
            return this;
        }

        setWizard(wizard) {
            if (!(wizard instanceof FormWizard))
            {
                throw new TypeError('Provided wizard element is not an instance of [' + FormWizard.name + ']');
            }
            this.wizard = wizard;
            return this;
        }

        setUi(ui) {
            if (!(ui instanceof FormWizardUi))
            {
                throw new TypeError('Provided wizard ui element is not an instance of [' + FormWizardUi.name + ']');
            }
            this.ui = ui;
            return this;
        }

        setSections(sections) {
            this.sections = sections;
            return this;
        }

        setCurrent(section) {
            if (this.current != null)
            {
                this.current.deactivate();
            }
            this.current = section;
            return this;
        }

        onReady(subject) {
            super.onReady(subject);
        }

        validateForm()
        {
            designation = true;
            for (let section of this.sections)
            {
                if (!section.validateSection())
                {
                    designation = false;
                }
            }
            return designation;
        }

        onSubmit(event) {
            let designation = this.validateForm();
            if (designation == false)
            {
                event.stopPropagation();
                event.preventDefault();
                return false;
            }
            return true;
        }

        onReset(event) {
            return true;
        }
    }

    lib.init();
    oroboros.register('component', component, FormWizard);
    oroboros.register('component', component_wizard_ui, FormWizardUi);
    oroboros.register('component', component_wizard_section, FormWizardSection);
    oroboros.register('component', component_wizard_form, FormWizardForm);
})(Oroboros);
