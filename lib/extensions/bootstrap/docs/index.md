# Bootstrap 5 Extension

- [Overview](#overview)

## Overview {#overview}

This extension provides templating, scripting, sass integration, and components that serve standardized [Bootstrap 5][1] html and styling. By default, templates are implemented in [Twig][2].

[1]: https://getbootstrap.com/
[2]: https://twig.symfony.com/