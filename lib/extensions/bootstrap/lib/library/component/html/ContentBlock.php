<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\bootstrap\library\component\html;

/**
 * Bootstrap Content Block
 * Represents a component wrapper for a Bootstrap full width content block
 * @author Brian Dayhoff
 */
class ContentBlock extends \Oroboros\bootstrap\abstracts\library\component\html\AbstractBootstrapComponent
{

    const COMPONENT_TEMPLATE = 'component/bootstrap/content/block';
    const COMPONENT_KEY = 'content-block';

    private $context_keys = [
        'banner',
        'header',
        'nav',
        'image',
        'title',
        'subtitle',
        'row',
        'column',
        'footer',
    ];
    private $context_component_filters = [
        'banner' => 'filterBanner',
        'header' => 'filterHeader',
        'nav' => 'filterNav',
        'title' => 'filterTitle',
        'subtitle' => 'filterTitle',
        'row' => 'filterRow',
        'column' => 'filterColumn',
        'footer' => 'filterFooter',
    ];

    protected function declareValidContextKeys(): array
    {
        return array_merge(parent::declareValidContextKeys(), $this->context_keys);
    }

    protected function declareContextFilters(): array
    {
        return array_merge(parent::declareContextFilters(), $this->context_component_filters);
    }

    protected function getDefaultScripts(): array
    {
        return array_merge_recursive(parent::getDefaultScripts(), [
            'footer' => [sprintf('%1$s-%2$s', static::CLASS_SCOPE, self::COMPONENT_KEY)]
        ]);
    }

    protected function filterBanner(array $details)
    {
        return $details;
    }

    protected function filterHeader(array $details)
    {
        return $details;
    }

    protected function filterNav(array $details)
    {
        return $details;
    }

    protected function filterTitle(array $details)
    {
        if (!array_key_exists('title-type', $details)) {
            $details['title-type'] = 'p';
        }
        if (!array_key_exists('title-class', $details)) {
            $details['title-class'] = 'h1-responsive';
        }
        return $details;
    }

    protected function filterSubtitle(array $details)
    {
        return $details;
    }

    protected function filterRow(array $details)
    {
        return $details;
    }

    protected function filterColumn(array $details)
    {
        return $details;
    }

    protected function filterFooter(array $details)
    {
        return $details;
    }
}
