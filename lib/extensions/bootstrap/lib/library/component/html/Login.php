<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\bootstrap\library\component\html;

/**
 * Bootstrap Login
 * 
 * Represents a component wrapper for a Bootstrap responsive form
 * @author Brian Dayhoff
 */
class Login extends \Oroboros\bootstrap\abstracts\library\component\html\AbstractBootstrapFormComponent
{
    
    const FORM_PARSER_CLASS = \Oroboros\core\library\form\login\FormParser::class;

    const COMPONENT_TEMPLATE = 'component/bootstrap/login/login';
    const COMPONENT_KEY = 'login';

    private $context_keys = [
        // Layout
        'title',
        'header',
        'footer',
        'validation',
        // form sections
        'main',
        'social',
        'registration',
        'csrf',
        // form functionality
        'action',
        'method',
        // form field details
        'credentials',
        'identity',
        'password',
        'errors',
    ];
    private $context_component_filters = [
        'nonce' => 'filterNonce',
        'identity' => 'filterIdentity',
        'password' => 'filterPassword',
        'social' => 'filterSocial',
        'credentials' => 'filterCredentials',
    ];

    protected function declareValidContextKeys(): array
    {
        return array_merge(parent::declareValidContextKeys(), $this->context_keys);
    }

    protected function declareContextFilters(): array
    {
        return array_merge(parent::declareContextFilters(), $this->context_component_filters);
    }

    protected function getDefaultScripts(): array
    {
        return array_merge_recursive(parent::getDefaultScripts(), [
            'footer' => [sprintf('%1$s-%2$s', static::CLASS_SCOPE, self::COMPONENT_KEY)]
        ]);
    }

    protected function filterNonce(\Oroboros\core\interfaces\library\auth\NonceInterface $nonce = null)
    {
        if (is_null($nonce)) {
            return $nonce;
        }
        if (!$this->hasContext('csrf')) {
            $this->addContext('csrf', $this->containerize());
        }
        $csrf = $this->getContext('csrf');
        $csrf['nonce'] = $nonce->get();
        $this->addContext('csrf', $csrf);
        return $nonce;
    }

    protected function filterIdentity(\Oroboros\core\interfaces\library\auth\IdentityInterface $identity = null)
    {
        if (is_null($identity)) {
            return $identity;
        }
        return $identity;
    }

    protected function filterPassword(\Oroboros\core\interfaces\library\auth\PasswordInterface $password = null)
    {
        if (is_null($password)) {
            return $password;
        }
        return $password;
    }

    protected function filterSocial($social)
    {
        if (is_null($social)) {
            return $social;
        }
        return $this->containerize($social);
    }

    protected function filterCredentials(\Oroboros\core\interfaces\library\auth\CredentialContainerInterface $credentials = null)
    {
        if (is_null($credentials)) {
            $this->addContext('identity', null);
            $this->addContext('password', null);
            $this->addContext('nonce', null);
            return $credentials;
        }
        if ($credentials->has('identity')) {
            $this->addContext('identity', $credentials['identity']);
        }
        if ($credentials->has('password')) {
            $this->addContext('password', $credentials['password']);
        }
        return null;
    }

    protected function initializeContext(): void
    {
        parent::initializeContext();
        // Default tagname is "form"
        $this->addContext('tagname', 'form');
        // Default form action is http?s://{hostname}/login/
        $this->addContext('action', sprintf('%1$s/%2$s/', $this::HOSTNAME, 'login'));
        // Default id is "login"
        $this->addContext('id', 'login');
        // Default name is "login"
        $this->addContext('name', 'login');
        // Default method is post
        $this->addContext('method', 'post');
        // No assumption of a title
        $this->addContext('title', null);
        // No assumption of csrf
        $this->addContext('csrf', null);
        // No assumption of social login
        $this->addContext('social', null);
        // No assumption of a registration section
        $this->addContext('registration', null);
    }
}
