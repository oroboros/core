<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\bootstrap\library\component\html\user;

/**
 * Bootstrap User Settings
 * Represents a component wrapper for a Bootstrap 5 user settings page,
 * which represents the personal control panel for a logged in user to
 * manage their account.
 * 
 * @author Brian Dayhoff
 */
class UserSettings extends \Oroboros\bootstrap\library\component\html\user\profile\UserProfile
{

    const COMPONENT_TEMPLATE = 'component/bootstrap/user/settings';
    const COMPONENT_KEY = 'user-settings';

    private $context_keys = [
        'header',
        'footer',
        'title',
        'menu',
        'sections',
    ];
    private $context_component_filters = [
        'header' => 'filterHeader',
        'footer' => 'filterFooter',
        'title' => 'filterTitle',
        'menu' => 'filterMenu',
        'sections' => 'filterSections',
    ];

    protected function declareValidContextKeys(): array
    {
        return array_merge(parent::declareValidContextKeys(), $this->context_keys);
    }

    protected function declareContextFilters(): array
    {
        return array_merge(parent::declareContextFilters(), $this->context_component_filters);
    }

    protected function filterHeader($header, $component_class = null)
    {
        if (is_null($header)) {
            return null;
        }
        if (is_null($component_class)) {
            $component_class = 'user-settings-header';
        }
        return parent::filterHeader($header, $component_class);
    }

    protected function filterFooter($footer)
    {
        if (is_null($footer)) {
            return null;
        }
        return $this->containerize($footer);
    }

    protected function filterTitle($title): ?\Oroboros\core\interfaces\library\component\ComponentInterface
    {
        if (is_null($title)) {
            return null;
        }
        $id = '';
        if (is_string($title)) {
            $title = ['title' => $title];
        }
        if (is_array($title)) {
            $title = $this->containerize($title);
        }
        if ($title->has('id')) {
            $id = $title->id;
            unset($title['id']);
        }
        $component = $this->getComponent(sprintf('%1$s-section-title', static::COMPONENT_KEY), $id, $title->toArray());
        $component->setLogger($this->getLogger());
        $component->setParent($this);
        return $component;
    }

    protected function filterMenu($menu): ?\Oroboros\core\interfaces\library\component\ComponentInterface
    {
        if (is_null($menu)) {
            return null;
        }
        $id = '';
        if (is_string($menu)) {
            $menu = ['id' => $menu];
        }
        if (is_array($menu)) {
            $menu = $this->containerize($menu);
        }
        if ($menu->has('id')) {
            $id = $menu->id;
            unset($menu['id']);
        }
        $component = $this->getComponent(sprintf('%1$s-menu', static::COMPONENT_KEY), $id, $menu->toArray());
        $component->setLogger($this->getLogger());
        $component->setParent($this);
//        d($component, $menu->toArray());
        return $component;
    }

    protected function filterSections($sections)
    {
        if (is_null($sections)) {
            return null;
        }
        $results = [];
        d($sections, $results); exit;
        return $this->containerize($results);
    }
//
//    protected function getDefaultScripts(): array
//    {
//        return array_merge_recursive(parent::getDefaultScripts(), [
//            'footer' => [sprintf('%1$s-%2$s', static::CLASS_SCOPE, self::COMPONENT_KEY)]
//        ]);
//    }

    /**
     * Initializes the icon baseline context
     * 
     * @return void
     */
    protected function initializeContext(): void
    {
        parent::initializeContext();
//        // Default tagname is "li"
//        $this->addContext('tagname', 'span');
//        // Default id is the passed command
//        $this->addContext('id', $this->getCommand());
//        // Default icon class
//        $this->addContext('class', 'icon text-muted d-flex font-weight-lighter h-100');
    }

    protected function initializeIdentityContext(\Oroboros\core\interfaces\library\auth\IdentityInterface $identity)
    {
        parent::initializeIdentityContext($identity);
        $this->initializeIdentitySectionContext($identity);
    }
    
    protected function initializeIdentitySectionContext(\Oroboros\core\interfaces\library\auth\IdentityInterface $identity)
    {
        
    }
}
