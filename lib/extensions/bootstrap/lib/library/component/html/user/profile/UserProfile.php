<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\bootstrap\library\component\html\user\profile;

/**
 * Bootstrap User Profile
 * Represents a component wrapper for a Bootstrap user profile element
 * @author Brian Dayhoff
 */
class UserProfile extends \Oroboros\bootstrap\library\component\html\Card
{

    const COMPONENT_TEMPLATE = 'component/bootstrap/user/profile/user';
    const COMPONENT_KEY = 'user';

    private $context_keys = [
        'identity',
        'header',
        'footer',
        'title',
        'menu',
        'content',
    ];
    private $context_component_filters = [
        'identity' => 'filterIdentity',
        'header' => 'filterHeader',
        'footer' => 'filterFooter',
        'title' => 'filterTitle',
        'menu' => 'filterMenu',
        'content' => 'filterContent',
    ];

    protected function declareValidContextKeys(): array
    {
        return array_merge(parent::declareValidContextKeys(), $this->context_keys);
    }

    protected function declareContextFilters(): array
    {
        return array_merge(parent::declareContextFilters(), $this->context_component_filters);
    }

    protected function filterHeader($header, $component_class = null)
    {
        if (is_null($header)) {
            return null;
        }
        $id = null;
        if (is_null($component_class)) {
            $component_class = 'user-profile-header';
        }
        if (is_array($header)) {
            $header = $this->containerize($header);
        }
        if ($this->hasContext('id')) {
            $id = sprintf('%1$s-header', $this->getContext('id'));
        }
        if (!$header->has('id')) {
            $header['id'] = $id;
        }
        $id = $header->id;
        $component = $this->getComponent($component_class, $id, $header->toArray());
        $component->setLogger($this->getLogger());
        foreach ($header->toArray() as $key => $context) {
            if ($key === 'content') {
                continue;
            }
            $component->addContext($key, $context);
        }
        $component->setParent($this);
        return $component;
    }

    protected function filterIdentity(\Oroboros\core\interfaces\library\auth\IdentityInterface $identity = null)
    {
        if (is_null($identity)) {
            return null;
        }
        $this->initializeIdentityContext($identity);
        return null;
    }

    protected function getDefaultScripts(): array
    {
        return array_merge_recursive(parent::getDefaultScripts(), [
            'footer' => [sprintf('%1$s-%2$s', static::CLASS_SCOPE, self::COMPONENT_KEY)]
        ]);
    }

    protected function initializeIdentityContext(\Oroboros\core\interfaces\library\auth\IdentityInterface $identity)
    {
        $this->initializeHeaderIdentityContext($identity);
        $this->initializeMenuIdentityContext($identity);
        $this->initializeTitleIdentityContext($identity);
        $this->initializeContentIdentityContext($identity);
        $this->initializeFooterIdentityContext($identity);
    }

    protected function initializeHeaderIdentityContext(\Oroboros\core\interfaces\library\auth\IdentityInterface $identity)
    {
        $context = [];
        $this->addContext('header', $context);
        if ($this->getContext('header')->isValidContext('identity')) {
            $this->getContext('header')->addContext('identity', $identity);
        }
    }

    protected function initializeMenuIdentityContext(\Oroboros\core\interfaces\library\auth\IdentityInterface $identity)
    {
        
    }

    protected function initializeTitleIdentityContext(\Oroboros\core\interfaces\library\auth\IdentityInterface $identity)
    {
        
    }

    protected function initializeContentIdentityContext(\Oroboros\core\interfaces\library\auth\IdentityInterface $identity)
    {
        
    }

    protected function initializeFooterIdentityContext(\Oroboros\core\interfaces\library\auth\IdentityInterface $identity)
    {
        
    }
}
