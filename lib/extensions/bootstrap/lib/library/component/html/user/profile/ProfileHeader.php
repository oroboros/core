<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\bootstrap\library\component\html\user\profile;

/**
 * Bootstrap User Settings
 * Represents a component wrapper for a Bootstrap 5 user settings page,
 * which represents the personal control panel for a logged in user to
 * manage their account.
 * 
 * @author Brian Dayhoff
 */
class ProfileHeader extends \Oroboros\bootstrap\abstracts\library\component\html\AbstractBootstrapComponent
{

    const COMPONENT_TEMPLATE = 'component/bootstrap/user/profile/header';
    const COMPONENT_KEY = 'user-profile-header';

    private $context_keys = [
        'identity',
        'avatar',
        'banner',
        'details',
    ];
    private $context_component_filters = [
        'identity' => 'filterIdentity',
    ];

    protected function declareValidContextKeys(): array
    {
        return array_merge(parent::declareValidContextKeys(), $this->context_keys);
    }

    protected function declareContextFilters(): array
    {
        return array_merge(parent::declareContextFilters(), $this->context_component_filters);
    }

    protected function filterIdentity(\Oroboros\core\interfaces\library\auth\IdentityInterface $identity = null)
    {
        if (is_null($identity)) {
            return null;
        }
        $this->initializeIdentityContext($identity);
        return null;
    }

    /**
     * Initializes the icon baseline context
     * 
     * @return void
     */
    protected function initializeContext(): void
    {
        parent::initializeContext();
        // Default tagname is "div"
        $this->addContext('tagname', 'div');
        // Default id is the passed command
        $this->addContext('id', $this->getCommand());
        // Default icon class
        $this->addContext('class', 'profile-header card-header position-relative min-vh-25 p-0');
    }

    protected function initializeIdentityContext(\Oroboros\core\interfaces\library\auth\IdentityInterface $identity)
    {
        $this->initializeIdentityBannerContext($identity);
        $this->initializeIdentityAvatarContext($identity);
        $this->initializeIdentityDetailsContext($identity);
        foreach ($this->getChildComponents() as $key => $component) {
            if ($component->isValidContext('identity')) {
                $component->addContext('identity', $identity);
            }
        }
    }

    protected function initializeIdentityBannerContext(\Oroboros\core\interfaces\library\auth\IdentityInterface $identity, string $component_class = null)
    {
        if (is_null($component_class)) {
            $component_class = 'user-profile-banner';
        }
        $id = sprintf('%1$s-banner', $this->getContext('id'));
        $component = $this->getComponent($component_class, $id);
        $component->setLogger($this->getLogger());
        $component->setParent($this);
        if ($component->isValidContext('identity')) {
            $component->addContext('identity', $identity);
        }
        $this->addContext('banner', $component);
    }

    protected function initializeIdentityAvatarContext(\Oroboros\core\interfaces\library\auth\IdentityInterface $identity, string $component_class = null)
    {
        if (is_null($component_class)) {
            $component_class = 'user-profile-avatar';
        }
        $id = sprintf('%1$s-avatar', $this->getContext('id'));
        $component = $this->getComponent($component_class, $id);
        $component->setLogger($this->getLogger());
        $component->setParent($this);
        if ($component->isValidContext('identity')) {
            $component->addContext('identity', $identity);
        }
        $this->addContext('avatar', $component);
    }

    protected function initializeIdentityDetailsContext(\Oroboros\core\interfaces\library\auth\IdentityInterface $identity, string $component_class = null)
    {
        if (is_null($component_class)) {
            $component_class = 'user-profile-details';
        }
        $id = sprintf('%1$s-details', $this->getContext('id'));
        $component = $this->getComponent($component_class, $id);
        $component->setLogger($this->getLogger());
        $component->setParent($this);
        if ($component->isValidContext('identity')) {
            $component->addContext('identity', $identity);
        }
        $this->addContext('details', $component);
    }
}
