<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\bootstrap\library\component\html\user\profile;

/**
 * Bootstrap User Profile Banner
 * Represents a component wrapper for a user profile banner image.
 * 
 * @author Brian Dayhoff
 */
class ProfileBanner extends \Oroboros\bootstrap\library\component\html\Card
{

    const COMPONENT_TEMPLATE = 'component/bootstrap/user/profile/avatar';
    const COMPONENT_KEY = 'user-profile-banner';

    private $context_keys = [
        'identity',
        'icon',
    ];
    private $context_component_filters = [
        'identity' => 'filterIdentity',
    ];

    protected function declareValidContextKeys(): array
    {
        return array_merge(parent::declareValidContextKeys(), $this->context_keys);
    }

    protected function declareContextFilters(): array
    {
        return array_merge(parent::declareContextFilters(), $this->context_component_filters);
    }

    protected function filterHeader($header)
    {
        if (is_null($header)) {
            return null;
        }
        return $this->containerize($header);
    }

    protected function filterFooter($footer)
    {
        if (is_null($footer)) {
            return null;
        }
        return $this->containerize($footer);
    }

    protected function filterTitle($title): ?\Oroboros\core\interfaces\library\component\ComponentInterface
    {
        if (is_null($title)) {
            return null;
        }
        $id = '';
        if (is_string($title)) {
            $title = ['title' => $title];
        }
        if (is_array($title)) {
            $title = $this->containerize($title);
        }
        if ($title->has('id')) {
            $id = $title->id;
            unset($title['id']);
        }
        $component = $this->getComponent(sprintf('%1$s-title', static::COMPONENT_KEY), $id, $title->toArray());
        $component->setLogger($this->getLogger());
        $component->setParent($this);
        return $component;
    }

    protected function filterMenu($menu): ?\Oroboros\core\interfaces\library\component\ComponentInterface
    {
        if (is_null($menu)) {
            return null;
        }
        $id = '';
        if (is_string($menu)) {
            $menu = ['id' => $menu];
        }
        if (is_array($menu)) {
            $menu = $this->containerize($menu);
        }
        if ($menu->has('id')) {
            $id = $menu->id;
            unset($menu['id']);
        }
        $component = $this->getComponent(sprintf('%1$s-menu', static::COMPONENT_KEY), $id, $menu->toArray());
        $component->setLogger($this->getLogger());
        $component->setParent($this);
//        d($component, $menu->toArray());
        return $component;
    }

    protected function filterContent($content)
    {
        if (is_null($content)) {
            return null;
        }
        return $this->containerize($content);
    }

    protected function filterIdentity(\Oroboros\core\interfaces\library\auth\IdentityInterface $identity = null)
    {
        if (is_null($identity)) {
            return null;
        }
        $this->initializeIdentityContext($identity);
        return null;
    }
//
//    protected function getDefaultScripts(): array
//    {
//        return array_merge_recursive(parent::getDefaultScripts(), [
//            'footer' => [sprintf('%1$s-%2$s', static::CLASS_SCOPE, self::COMPONENT_KEY)]
//        ]);
//    }

    /**
     * Initializes the icon baseline context
     * 
     * @return void
     */
    protected function initializeContext(): void
    {
        parent::initializeContext();
        // Default tagname is "div"
        $this->addContext('tagname', 'div');
        // Default id is the passed command
        $this->addContext('id', $this->getCommand());
        // Default icon class
        $this->addContext('class', 'profile-banner rounded-3 rounded-bottom-0');
    }

    protected function initializeIdentityContext(\Oroboros\core\interfaces\library\auth\IdentityInterface $identity)
    {
        foreach ($this->getChildComponents() as $key => $component) {
            if ($component->isValidContext('identity')) {
                $component->addContext('identity', $identity);
            }
        }
    }
}
