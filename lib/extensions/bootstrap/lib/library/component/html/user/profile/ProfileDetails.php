<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\bootstrap\library\component\html\user\profile;

/**
 * Bootstrap User Profile Details
 * Represents a component wrapper for a profile detail summary.
 * 
 * @author Brian Dayhoff
 */
class ProfileDetails extends \Oroboros\bootstrap\abstracts\library\component\html\AbstractBootstrapComponent
{

    const COMPONENT_TEMPLATE = 'component/bootstrap/user/profile/details';
    const COMPONENT_KEY = 'user-profile-details';

    private $context_keys = [
        'identity',
        'name',
        'verified',
    ];
    private $context_component_filters = [
        'identity' => 'filterIdentity',
    ];

    protected function declareValidContextKeys(): array
    {
        return array_merge(parent::declareValidContextKeys(), $this->context_keys);
    }

    protected function declareContextFilters(): array
    {
        return array_merge(parent::declareContextFilters(), $this->context_component_filters);
    }

    protected function filterTitle($title): ?\Oroboros\core\interfaces\library\component\ComponentInterface
    {
        if (is_null($title)) {
            return null;
        }
        $id = '';
        if (is_string($title)) {
            $title = ['title' => $title];
        }
        if (is_array($title)) {
            $title = $this->containerize($title);
        }
        if ($title->has('id')) {
            $id = $title->id;
            unset($title['id']);
        }
        $component = $this->getComponent(sprintf('%1$s-title', static::COMPONENT_KEY), $id, $title->toArray());
        $component->setLogger($this->getLogger());
        $component->setParent($this);
        return $component;
    }

    protected function filterIdentity(\Oroboros\core\interfaces\library\auth\IdentityInterface $identity = null)
    {
        if (is_null($identity)) {
            return null;
        }
        $this->initializeIdentityContext($identity);
        return null;
    }
//
//    protected function getDefaultScripts(): array
//    {
//        return array_merge_recursive(parent::getDefaultScripts(), [
//            'footer' => [sprintf('%1$s-%2$s', static::CLASS_SCOPE, self::COMPONENT_KEY)]
//        ]);
//    }

    /**
     * Initializes the icon baseline context
     * 
     * @return void
     */
    protected function initializeContext(): void
    {
        parent::initializeContext();
        // Default tagname is "div"
        $this->addContext('tagname', 'div');
        // Default id is the passed command
        $this->addContext('id', $this->getCommand());
        // Default icon class
        $this->addContext('class', 'profile-details col-12 col-md-9');
    }

    protected function initializeIdentityContext(\Oroboros\core\interfaces\library\auth\IdentityInterface $identity)
    {
//        d(
//            $identity->details(),
//            $identity->details()->profile,
//            $identity->details()->identity,
//            $identity->details()->identity['display-name']->value(),
//        );
//        exit;
        $this->addContext('name', $identity->details()->identity['display-name']->value());
        $this->addContext('verified', $identity->details()->security->verified->value());
        foreach ($this->getChildComponents() as $key => $component) {
            if ($component->isValidContext('identity')) {
                $component->addContext('identity', $identity);
            }
        }
    }
}
