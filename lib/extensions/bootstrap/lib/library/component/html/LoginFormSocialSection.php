<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\bootstrap\library\component\html;

/**
 * Bootstrap Login Form Social Section
 * 
 * Represents a component wrapper for the social login section of a bootstrap form
 * @author Brian Dayhoff
 */
class LoginFormSocialSection extends \Oroboros\bootstrap\abstracts\library\component\html\AbstractBootstrapFormFieldsetComponent
{

    const COMPONENT_TEMPLATE = 'component/bootstrap/login/login-social';
    const COMPONENT_KEY = 'login-form-social-section';

    private $context_keys = [
        'title',
        'name',
        'fields',
        'buttons',
    ];
    private $context_component_filters = [
        'fields' => 'filterFields',
        'buttons' => 'filterButtons',
    ];

    protected function declareValidContextKeys(): array
    {
        return array_merge(parent::declareValidContextKeys(), $this->context_keys);
    }

    protected function declareContextFilters(): array
    {
        return array_merge(parent::declareContextFilters(), $this->context_component_filters);
    }

//    protected function getDefaultScripts(): array
//    {
//        return array_merge_recursive(parent::getDefaultScripts(), [
//            'footer' => [sprintf('%1$s-%2$s', static::CLASS_SCOPE, self::COMPONENT_KEY)]
//        ]);
//    }

    protected function filterFields(iterable $fields = null): ?\Oroboros\core\interfaces\library\container\ContainerInterface
    {
        parent::filterFields($fields);
        $results = [];
        if (is_null($fields)) {
            return $fields;
        }
        if (is_array($fields)) {
            $fields = $this->containerize($fields);
        }
        return $this->containerize($results);
    }

    protected function filterButtons(iterable $buttons = null): ?\Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $results = [];
        if (is_null($buttons)) {
            return $buttons;
        }
        if (is_array($buttons)) {
            $buttons = $this->containerize($buttons);
        }
        return $this->containerize($results);
    }

    protected function initializeContext(): void
    {
        parent::initializeContext();
        // Default tagname is "fieldset"
        $this->addContext('tagname', 'fieldset');
        // Default id is the passed command, if any
        $this->addContext('id', $this->getCommand());
        // Default id is the passed command, if any
        $this->addContext('name', $this->getCommand());
        // No assumption of a title
        $this->addContext('title', null);
        // No assumption of fields
        $this->addContext('fields', null);
        // No assumption of buttons
        $this->addContext('buttons', null);
    }
}
