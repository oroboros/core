<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\bootstrap\library\component\html;

/**
 * Dashboard Primary Navbar
 * Represents a component wrapper for the Oroboros dashboard navigation
 * @author Brian Dayhoff
 */
class PrimaryNavbar extends Navbar
{

    const COMPONENT_TEMPLATE = 'component/bootstrap/navbar/primary-navbar';
    const COMPONENT_KEY = 'primary-navbar';

    private $context_keys = [
        'brand',
        'menu',
        'ui',
    ];
    private $context_component_filters = [
//        'brand' => 'filterBrand',
        'menu' => 'filterMenu',
        'ui' => 'filterUi',
    ];

    protected function declareValidContextKeys(): array
    {
        return array_merge(parent::declareValidContextKeys(), $this->context_keys);
    }

    protected function declareContextFilters(): array
    {
        return array_merge(parent::declareContextFilters(), $this->context_component_filters);
    }

    protected function getDefaultScripts(): array
    {
        return array_merge_recursive(parent::getDefaultScripts(), [
            'footer' => [sprintf('%1$s-%2$s', static::CLASS_SCOPE, self::COMPONENT_KEY)]
        ]);
    }
    
    protected function filterBrand($brand)
    {
        return $brand;
    }
    
    protected function filterMenu(iterable $menu): ?\Oroboros\core\interfaces\library\component\HtmlComponentInterface
    {
        if (is_array($menu)) {
            $menu = $this->containerize($menu);
        }
        if ($menu->has('permission') && !$this::user()->can($menu->permission)) {
            // not authorized
            return null;
        }
        $menu['component'] = 'primary-navigation';
        $id = null;
        if ($menu->has('id')) {
            $id = $menu['id'];
        } elseif ($this->hasContext('id')) {
            $id = sprintf('%1$s-menu', $this->getContext('id'));
        }
        $component = $this->getComponent($menu['component'], $id, $menu->toArray(), $this->getFlags());
        $component->setLogger($this->getLogger());
        $component->setParent($this);
        return $component;
    }
    
    protected function filterUi(iterable $ui)
    {
        if (is_array($ui)) {
            $ui = $this->containerize($ui);
        }
        if (!$this::user()->isBlacklisted() && $this::user()->isLoggedIn()) {
            // Add login menu contexts
            if ($ui->has('links') && $ui['links']->has('login')) {
                unset($ui['links']['login']);
            }
            return $this->getComponent('ui-user', $ui['id'], $ui->toArray(), $this->getFlags());
        } elseif ($this::user()->can('http.login.submit')) {
            // Add the short form login ui
            return $this->getComponent('ui-login', $ui['id'], $ui->toArray(), $this->getFlags());
        }
        // do nothing
        return $this->containerize($ui);
    }
}
