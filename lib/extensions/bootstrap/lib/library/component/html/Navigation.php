<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\bootstrap\library\component\html;

/**
 * Bootstrap Navigation
 * Represents a component wrapper for a Bootstrap 5 navigation element
 * @author Brian Dayhoff
 */
class Navigation extends \Oroboros\bootstrap\abstracts\library\component\html\AbstractBootstrapComponent
{

    const COMPONENT_TEMPLATE = 'component/bootstrap/navbar/navbar-menu';
    const COMPONENT_KEY = 'navigation';

    private $navigation_context_keys = [
        'links'
    ];
    private $context_component_filters = [
        'links' => 'filterLinks',
    ];

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
    }

    protected function declareValidContextKeys(): array
    {
        return array_merge(parent::declareValidContextKeys(), $this->navigation_context_keys);
    }

    protected function declareContextFilters(): array
    {
        return array_merge(parent::declareContextFilters(), $this->context_component_filters);
    }
//
//    protected function getDefaultScripts(): array
//    {
//        return array_merge_recursive(parent::getDefaultScripts(), [
//            'footer' => [sprintf('%1$s-%2$s', static::CLASS_SCOPE, self::COMPONENT_KEY)]
//        ]);
//    }

    /**
     * Generates a container of links
     * 
     * @param iterable $links
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    protected function filterLinks(iterable $links = null): ?\Oroboros\core\interfaces\library\container\ContainerInterface
    {
        if (is_array($links)) {
            $links = $this->containerize($links);
        }
        if ($links->has('id')) {
            $this->addContext('id', $links['id']);
            unset($links['id']);
        }
        if ($links->has('context')) {
            $this->addContext('context', $links['context']);
            unset($links['context']);
        }
        if ($links->has('catagory')) {
            $this->addContext('category', $links['category']);
            unset($links['category']);
        }
        if ($links->has('parent')) {
            unset($links['parent']);
        }
        foreach ($links as $key => $link) {
            if (!is_iterable($link)) {
                continue;
            }
            if (is_array($link)) {
                $link = $this->containerize($link);
            }
            if ($link->has('permission') && !$this::user()->can($link->permission)) {
                // Not authorized
                unset($links[$key]);
                continue;
            }
            if ($this->hasContext('id')) {
                $link['parent'] = $this->getContext('id');
            }
            if ($this->hasContext('category')) {
                $link['category'] = $this->getContext('category');
            }
            if ($link->has('children')) {
                
            } else {
                
            }
            $links[$key] = $link;
        }
        return $links;
    }

    /**
     * Sets up the default navigation component context
     * 
     * @return void
     */
    protected function initializeContext(): void
    {
        parent::initializeContext();
        // Default tagname is "ul"
        $this->addContext('tagname', 'ul');
        // Default class is the bootstrap responsive navbar class
        $this->addContext('class', 'navbar-nav me-auto order-5 order-md-3"');
    }
}
