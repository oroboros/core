<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\bootstrap\library\component\html;

/**
 * Bootstrap Card Grid
 * Represents a component wrapper for a set of Bootstrap cards aligned evenly to a grid
 * @author Brian Dayhoff
 */
class CardGrid extends \Oroboros\bootstrap\abstracts\library\component\html\AbstractBootstrapComponent
{

    const COMPONENT_TEMPLATE = 'component/bootstrap/card/card-grid';
    const COMPONENT_KEY = 'card-grid';

    private $context_keys = [
        'cards',
        'columns',
        'columns-sm',
        'columns-md',
        'columns-lg',
        'columns-xl',
        'columns-xxl',
    ];
    private $context_component_filters = [
        'cards' => 'filterCards',
    ];

    protected function declareValidContextKeys(): array
    {
        return array_merge(parent::declareValidContextKeys(), $this->context_keys);
    }

    protected function declareContextFilters(): array
    {
        return array_merge(parent::declareContextFilters(), $this->context_component_filters);
    }

    protected function getDefaultScripts(): array
    {
        return array_merge_recursive(parent::getDefaultScripts(), [
            'footer' => [sprintf('%1$s-%2$s', static::CLASS_SCOPE, self::COMPONENT_KEY)]
        ]);
    }

    protected function filterCards($value)
    {
        $skip = ['parent', 'id', 'context', 'category'];
        $baseline_details = [
            'parent' => $value['parent'],
            'id' => $value['id'],
            'context' => $value['context'],
            'category' => $value['category'],
            'component' => 'card',
        ];
        $card = [];
        foreach ($value as $key => $card) {
            // Do not process pre-existing components
            if (is_object($card) && $card instanceof \Oroboros\core\interfaces\library\component\ComponentInterface) {
                $cards[$key] = $card;
                continue;
            }
            // Do not process details set by the underlying abstraction
            if (in_array($key, $skip)) {
                continue;
            }
            if (!is_iterable($card)) {
                throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Provided card details for key [%2$s] must be iterable or a component.', get_class($this), $key));
            }
            if (is_object($card) && $card instanceof \Oroboros\core\interfaces\library\container\ContainerInterface) {
                $card = $card->toArray();
            }
            foreach ($baseline_details as $subkey => $detail) {
                if (!array_key_exists($subkey, $card)) {
                    $card[$subkey] = $detail;
                }
            }
            $component = $this->getComponent($card['component'], $card['component'], $card);
            $cards[$key] = $component;
        }
        return $this->containerize($cards);
    }
}
