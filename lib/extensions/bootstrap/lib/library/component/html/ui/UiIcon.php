<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\bootstrap\library\component\html\ui;

/**
 * UI Icon
 * Represents a component wrapper for a ui icon
 * with optional control parameters
 * @author Brian Dayhoff
 */
class UiIcon extends \Oroboros\bootstrap\library\component\html\Icon
{

    const COMPONENT_TEMPLATE = 'component/bootstrap/ui/ui-icon';
    const COMPONENT_KEY = 'ui-icon';

    private $context_keys = [
        'badge',
    ];
    private $context_component_filters = [
        'badge' => 'filterBadge',
    ];

    protected function filterIcon($icon)
    {
        $icon = parent::filterIcon($icon);
        return $icon;
    }

    protected function declareValidContextKeys(): array
    {
        return array_merge(parent::declareValidContextKeys(), $this->context_keys);
    }

    protected function declareContextFilters(): array
    {
        return array_merge(parent::declareContextFilters(), $this->context_component_filters);
    }

    protected function filterBadge($badge)
    {
        if (is_null($badge)) {
            return null;
        }
        $id = null;
        if ($this->hasContext('id')) {
            $id = sprintf('%1$s-badge', $this->getContext('id'));
        }
        if (is_scalar($badge)) {
            $badge = ['badge' => $badge];
        }
        if (is_array($badge)) {
            $badge = $this->containerize($badge);
        }
        if (!$badge->has('id')) {
            $badge['id'] = $id;
        }
        $id = $badge['id'];
        $badge['class'] = 'rounded-pill position-absolute top-0 left-100 translate-middle bg-danger text-white font-weight-lighter';
        $badge['context'] = 'count';
        if ($badge->has('badge')) {
            $badge['content'] = $badge['badge'];
            unset($badge['badge']);
        }
        $component = $this->getComponent('badge', $id, $badge->toArray());
        $component->setLogger($this->getLogger());
        foreach ($badge->toArray() as $key => $context) {
            if ($key === 'content') {
                continue;
            }
            $component->addContext($key, $context);
        }
        $component->setParent($this);
        return $component;
    }

    /**
     * Initializes the icon baseline context
     * 
     * @return void
     */
    protected function initializeContext(): void
    {
        parent::initializeContext();
        // Default tagname is "span"
        $this->addContext('tagname', 'span');
        // Default id is the passed command
        $this->addContext('id', $this->getCommand());
        // Default icon class
        $this->addContext('class', 'icon text-muted d-flex font-weight-lighter h-100');
        // Default behavior is no badge
        $this->addContext('badge', null);
    }
}
