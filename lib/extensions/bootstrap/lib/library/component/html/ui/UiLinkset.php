<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\bootstrap\library\component\html\ui;

/**
 * Bootstrap Ui Linkset
 * 
 * Provides an individual ui link that controls page behavior.
 * 
 * @author Brian Dayhoff
 */
class UiLinkset extends \Oroboros\bootstrap\abstracts\library\component\html\AbstractBootstrapUiMenuLinksetComponent
{

//    const FORM_PARSER_CLASS = \Oroboros\core\library\form\login\FormParser::class;

    const COMPONENT_TEMPLATE = 'component/bootstrap/navbar/navbar-ui';
    const COMPONENT_KEY = 'ui-linkset';

    private $context_keys = [
//        'links',
    ];
    private $context_component_filters = [
//        'links' => 'filterLinks',
    ];

    protected function declareValidContextKeys(): array
    {
        return array_merge(parent::declareValidContextKeys(), $this->context_keys);
    }

    protected function declareContextFilters(): array
    {
        return array_merge(parent::declareContextFilters(), $this->context_component_filters);
    }
//    
//    protected function filterLinks($links = null): ?\Oroboros\core\interfaces\library\component\ComponentInterface
//    {
//        d($links); exit;
//        return parent::filterLinks($links);
//    }
//
//    protected function getDefaultScripts(): array
//    {
//        return array_merge_recursive(parent::getDefaultScripts(), [
//            'footer' => [sprintf('%1$s-%2$s', static::CLASS_SCOPE, self::COMPONENT_KEY)]
//        ]);
//    }

    /**
     * Initializes the ui link default context
     * 
     * @return void
     */
//    protected function initializeContext(): void
//    {
//        parent::initializeContext();
//        // Default tagname is "li"
//        $this->addContext('tagname', 'li');
//        // Default id is the passed command
//        $this->addContext('id', $this->getCommand());
//        // No assumption of a passed link
//        $this->addContext('link', null);
//    }
}
