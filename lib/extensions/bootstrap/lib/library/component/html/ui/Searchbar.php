<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\bootstrap\library\component\html\ui;

/**
 * Bootstrap Search Bar
 * Represents a component wrapper for a Bootstrap search bar element
 * @author Brian Dayhoff
 */
class Searchbar extends \Oroboros\bootstrap\abstracts\library\component\html\AbstractBootstrapUiComponent
{

    const COMPONENT_TEMPLATE = 'component/bootstrap/userbar/ui/ui-searchbar';
    const COMPONENT_KEY = 'ui-searchbar';

    /**
     * The class used to generate the search form.
     * 
     * This may be overridden to use an alternate search form component
     */
    const SEARCHBAR_DEFAULT_FORM_CLASS = \Oroboros\bootstrap\library\component\html\ui\form\SearchbarForm::class;

    private $context_keys = [
        'form'
    ];
    private $context_component_filters = [
        'form' => 'filterForm',
    ];

    protected function declareValidContextKeys(): array
    {
        return array_merge(parent::declareValidContextKeys(), $this->context_keys);
    }

    protected function declareContextFilters(): array
    {
        return array_merge(parent::declareContextFilters(), $this->context_component_filters);
    }
//
//    protected function getDefaultScripts(): array
//    {
//        return array_merge_recursive(parent::getDefaultScripts(), [
//            'footer' => [sprintf('%1$s-%2$s', static::CLASS_SCOPE, self::COMPONENT_KEY)]
//        ]);
//    }

    /**
     * 
     * @param mixed $form
     * @return \Oroboros\core\interfaces\library\component\ComponentInterface
     */
    protected function filterForm($form = null): ?\Oroboros\core\interfaces\library\component\ComponentInterface
    {
        if (is_null($form)) {
            return null;
        }
        if (is_object($form) && ($form instanceof \Oroboros\core\interfaces\library\component\ComponentInterface)) {
            $this->setUiTarget($form);
            return $form;
        }
        if (is_string($form)) {
            $form = ['id' => $form];
        }
        if (is_array($form)) {
            $form = $this->containerize($form);
        }
        if (!$form->has('id')) {
            $form['id'] = trim(sprintf('%1$s-search', $this->getContext('id')), '-');
        }
        $component = $this->getComponent(static::SEARCHBAR_DEFAULT_FORM_CLASS, $form['id'], $form->toArray());
        $component->setLogger($this->getLogger());
        $component->setParent($this);
        return $component;
    }
    
    protected function filterUiTarget($target): ?\Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $target = parent::filterUiTarget($target);
        if (is_null($target)) {
            return null;
        }
        $target['target'] = '.ui-searchbar-collapse';
        $target['class'] = 'ui-searchbar-collapse collapse show';
        $target['toggle'] = 'collapse';
        return $target;
    }

    /**
     * Initializes the searchbar ui baseline context
     * 
     * @return void
     */
    protected function initializeContext(): void
    {
        parent::initializeContext();
        // Default tagname is "li"
        $this->addContext('icon', 'fas fa-search fa-fw text-gray-400');
        // Enable button
        $this->addContext('button', $this->containerize([
                'id' => $this->getContext('id') . '-search'
        ]));
        $this->initializeForm();
    }

    /**
     * Initializes the search form
     * 
     * @return void
     */
    protected function initializeForm(): void
    {
        $details = [
            'id' => sprintf('%1$s-', $this->getContext('id')),
            'method' => 'GET',
            'action' => '/search/',
        ];
        $this->addContext('form', $this->containerize($details));
        $this->setUiTarget($this->getContext('form'));
    }
}
