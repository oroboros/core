<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\bootstrap\library\component\html\ui\form;

/**
 * Bootstrap Search Bar Form
 * Represents a component wrapper for a Bootstrap 5 searchbar form
 * @author Brian Dayhoff
 */
class SearchbarForm extends \Oroboros\bootstrap\abstracts\library\component\html\AbstractBootstrapComponent
{

    const COMPONENT_TEMPLATE = 'component/bootstrap/ui/search/ui-searchbar-form';
    const COMPONENT_KEY = 'ui-searchbar-form';
    
    /**
     * Corresponds to the search query key passed back
     * when the search form is submitted.
     */
    const SEARCH_TYPE = 'search-default';

    private $context_keys = [
        'method',
        'name',
        'action',
        'button'
    ];
    private $context_component_filters = [
//        'button' => 'filterButton',
    ];

    protected function declareValidContextKeys(): array
    {
        return array_merge(parent::declareValidContextKeys(), $this->context_keys);
    }

    protected function declareContextFilters(): array
    {
        return array_merge(parent::declareContextFilters(), $this->context_component_filters);
    }
//
//    protected function getDefaultScripts(): array
//    {
//        return array_merge_recursive(parent::getDefaultScripts(), [
//            'footer' => [sprintf('%1$s-%2$s', static::CLASS_SCOPE, self::COMPONENT_KEY)]
//        ]);
//    }
    
    /**
     * Initializes the searchbar ui baseline context
     * 
     * @return void
     */
    protected function initializeContext(): void
    {
        parent::initializeContext();
        // Default id is the passed command, or passed id
        $this->addContext('id', $this->getCommand());
        if ($this->hasArgument('id')) {
            $this->addContext('id', $this->getArgument('id'));
        }
        // Default searchbar name
        $this->addContext('class', 'ui-searchbar-form collapse collapse-horizontal ui-searchbar-collapse');
        // Default searchbar name
        $this->addContext('name', static::SEARCH_TYPE);
        // Enable button
        $this->addContext('button', $this->containerize([
            'id' => $this->getContext('id') . '-search'
            ]));
        $this->initializeForm();
    }
    
    /**
     * Initializes the search form
     * 
     * @return void
     */
    protected function initializeForm(): void
    {
        
    }
}
