<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\bootstrap\library\component\html\ui;

/**
 * Bootstrap Notifications
 * Represents a component wrapper for a Bootstrap-based user notification dropdown
 * @author Brian Dayhoff
 */
class Notifications extends \Oroboros\bootstrap\abstracts\library\component\html\AbstractBootstrapUiComponent
{

    const COMPONENT_TEMPLATE = 'component/bootstrap/userbar/ui/ui-notifications';
    const COMPONENT_KEY = 'ui-notifications';

    private $context_keys = [];
    private $context_component_filters = [];

    protected function declareValidContextKeys(): array
    {
        return array_merge(parent::declareValidContextKeys(), $this->context_keys);
    }

    protected function declareContextFilters(): array
    {
        return array_merge(parent::declareContextFilters(), $this->context_component_filters);
    }
//
//    protected function getDefaultScripts(): array
//    {
//        return array_merge_recursive(parent::getDefaultScripts(), [
//            'footer' => [sprintf('%1$s-%2$s', static::CLASS_SCOPE, self::COMPONENT_KEY)]
//        ]);
//    }

    /**
     * Initializes the notifications ui baseline context
     * 
     * @return void
     */
    protected function initializeContext(): void
    {
        parent::initializeContext();
        // Default icon for the notifications
        $this->addContext('icon', 'far fa-bell fa-fw text-gray-400');
        $this->addContext('button', $this->containerize([
                'id' => $this->getContext('id') . '-notifications'
        ]));
        $this->initializeNotificationMenu();
    }

    private function initializeNotificationMenu(): void
    {
        $definer = $this->load('library', 'menu\\AccountNotificationsMenuDefiner', null, $this->getArguments(), $this->getFlags());
        $menu = $definer->compile()->fetch();
        $menu['id'] = sprintf('%1$s-menu', $this->getContext('id'));
        $badge = 0;
        if ($menu->has('links')) {
            foreach ($menu->links as $key => $link) {
                if (is_array($link)) {
                    $link = $this->containerize($link);
                }
                if ($link->has('permission') && !$this::user()->can($link->permission)) {
                    unset($menu['links'][$key]);
                    continue;
                }
            }
            $badge = count($menu['links']);
        }
        $this->addContext('menu', $menu);
        if ($badge === 0) {
            return;
        }
        if ($badge > 99) {
            $badge = '99+';
        }
        $badge = (string) $badge;
        $this->getContext('icon')->addContext('badge', $badge);
    }
}
