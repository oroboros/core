<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\bootstrap\library\component\html\ui;

/**
 * Bootstrap Navbar Ui
 * Represents a component wrapper for a ui section in the primary navbar
 * @author Brian Dayhoff
 */
class NavbarUi extends \Oroboros\bootstrap\abstracts\library\component\html\AbstractBootstrapUiMenuLinksetComponent
{

    const COMPONENT_TEMPLATE = 'component/bootstrap/navbar/navbar-ui';
    const COMPONENT_KEY = 'navbar-ui';

    private $context_keys = [
        'icons',
        'debugger',
        'identity',
        'spillover',
    ];
    private $context_component_filters = [
        'icons' => 'filterIcons',
        'identity' => 'filterUserIdentity',
        'spillover' => 'filterSpillover',
    ];

    public function render(): string
    {
        return parent::render();
    }

    protected function declareValidContextKeys(): array
    {
        return array_merge(parent::declareValidContextKeys(), $this->context_keys);
    }

    protected function declareContextFilters(): array
    {
        return array_merge(parent::declareContextFilters(), $this->context_component_filters);
    }

//    protected function getDefaultScripts(): array
//    {
//        return array_merge_recursive(parent::getDefaultScripts(), [
//            'footer' => [sprintf('%1$s-%2$s', static::CLASS_SCOPE, self::COMPONENT_KEY)]
//        ]);
//    }

    protected function filterIcons($icons)
    {
        return $icons;
    }

    protected function filterUserIdentity(\Oroboros\core\interfaces\library\user\UserIdentityInterface $identity = null): ?\Oroboros\core\interfaces\library\entity\EntityDetailContainerInterface
    {
        if (is_null($identity)) {
            return null;
        }
        return $identity->details();
    }

    protected function filterSpillover($spillover)
    {
        return $spillover;
    }

    protected function initializeContext(): void
    {
        parent::initializeContext();
        // Default tagname is "li"
        $this->addContext('tagname', 'li');
//        // Default navbar ui class
        $this->addContext('class', 'nav-item navbar-ui d-flex flex-row h-100');
        // UI everyone has who is not blacklisted
        $this->initializePublicUi();
        // UI logged in users have
        $this->initializeRegisteredUi();
        // UI internal logged in users have
        $this->initializeStaffUi();
        // UI internal logged in users that are moderator level or higher have
        $this->initializeModeratorUi();
        // UI internal logged in users that are admin level or higher have
        $this->initializeAdminUi();
        // UI only the root user has
        $this->initializeRootUi();
        // UI that displays if debug is enabled
        $this->initializeDebuggerUi();
        // UI that displays if an identity is passed to the ui
        $this->initializeIdentityUi();
        // UI that keeps the individual ui icons from exceeding the size of their container
        $this->initializeSpilloverUi();
    }

    /**
     * Initializes the public user ui if it applies
     * 
     * @return void
     */
    protected function initializePublicUi(): void
    {
        if (!$this::user()->can('http.frontend.view')) {
            // no-op
            return;
        }
        $ui = $this->containerize();
        $search = $this->getComponent('ui-searchbar', trim(sprintf('%1$s-search-default', $this->getContext('id')), '-'));
        $search->setLogger($this->getLogger());
        $search->setParent($this);
        $ui['search'] = $search;
        $this->addContext('public', $ui);
    }

    /**
     * Initializes the registered user ui if it applies
     * 
     * @return void
     */
    protected function initializeRegisteredUi(): void
    {
        if (!$this::user()->isLoggedIn() || !$this::user()->can('http.registered.view')) {
            // no-op
            return;
        }
        $ui = $this->containerize();
        $search = $this->getComponent('ui-notifications', trim(sprintf('%1$s-notifications', $this->getContext('id')), '-'));
        $search->setLogger($this->getLogger());
        $search->setParent($this);
        $ui['notifications'] = $search;
        $this->addContext('registered', $ui);
    }

    /**
     * Initializes the staff user ui if it applies
     * 
     * @return void
     */
    protected function initializeStaffUi(): void
    {
        if (!$this::user()->isLoggedIn() || !$this::user()->can('http.staff.view')) {
            // no-op
            return;
        }
    }

    /**
     * Initializes the moderator user ui if it applies
     * 
     * @return void
     */
    protected function initializeModeratorUi(): void
    {
        if (!$this::user()->isLoggedIn() || !$this::user()->can('http.moderator.view')) {
            // no-op
            return;
        }
    }

    /**
     * Initializes the admin user ui if it applies
     * 
     * @return void
     */
    protected function initializeAdminUi(): void
    {
        if (!$this::user()->isLoggedIn() || !$this::user()->can('http.admin.view')) {
            // no-op
            return;
        }
    }

    /**
     * Initializes the root user ui if it applies
     * 
     * @return void
     */
    protected function initializeRootUi(): void
    {
        if (!$this::user()->isLoggedIn() || !$this::user()->can('*')) {
            // no-op
            return;
        }
    }

    /**
     * Initializes the ui debugger button if it applies
     * 
     * @return void
     */
    protected function initializeDebuggerUi(): void
    {
        if (!$this::app()->debugEnabled()) {
            // no-op
            return;
        }
        $debugger = $this->containerize();
        $component = $this->getComponent('ui-debugger', 'page-debug');
        $component->setLogger($this->getLogger());
        $component->setParent($this);
        $debugger['page-debugger'] = $component;
        $this->addContext('debugger', $debugger);
    }

    /**
     * Initializes the ui icon spillover menu
     * 
     * Default behavior is no identity ui
     * Override this method to inject an identity component
     * 
     * User driven ui components can override this to map to an identity automatically.
     * 
     * @return void
     */
    protected function initializeIdentityUi(): void
    {
        $this->addContext('identity', $this::user()->identity());
    }

    /**
     * Initializes the ui icon spillover menu
     * 
     * Ui icons that are not tagged as sticky will be moved into the
     * spillover container when there is not enough space in their ui row
     * 
     * If you do not want a spillover menu, you may override this method and skip it.
     * 
     * @return void
     */
    protected function initializeSpilloverUi(): void
    {
        $component = $this->getComponent(\Oroboros\bootstrap\library\component\html\ui\Spillover::class, trim(sprintf('%1$s-spillover', $this->getContext('id')), '-'));
        $component->setLogger($this->getLogger());
        $component->setParent($this);
        $this->addContext('spillover', $component);
    }
}
