<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\bootstrap\library\component\html\ui;

/**
 * UI Dropdown Menu
 * Represents a component wrapper for a ui dropdown menu
 * @author Brian Dayhoff
 */
class UiDropdownMenu extends \Oroboros\bootstrap\library\component\html\Navigation
{

    const COMPONENT_TEMPLATE = 'component/bootstrap/ui/menu/ui-dropdown-menu';
    const COMPONENT_KEY = 'ui-dropdown-menu';

    private $context_keys = [
        'header',
        'page-link',
    ];
    private $context_component_filters = [
//        'icon' => 'filterIcon',
    ];

    protected function filterIcon($icon)
    {
        $icon = parent::filterIcon($icon);
        return $icon;
    }

    protected function declareValidContextKeys(): array
    {
        return array_merge(parent::declareValidContextKeys(), $this->context_keys);
    }

    protected function declareContextFilters(): array
    {
        return array_merge(parent::declareContextFilters(), $this->context_component_filters);
    }

    /**
     * Initializes the icon baseline context
     * 
     * @return void
     */
    protected function initializeContext(): void
    {
        parent::initializeContext();
        // Default tagname is "li"
        $this->addContext('tagname', 'div');
        // Default id is the passed command
        $this->addContext('id', $this->getCommand());
        // Default icon class
        $this->addContext('class', 'ui-dropdown-menu dropdown-menu dropdown-menu-end shadow animated--grow-in');
    }
}
