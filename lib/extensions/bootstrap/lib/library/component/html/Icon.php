<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\bootstrap\library\component\html;

/**
 * Standard Icon
 * Represents a component wrapper for a standard icon graphic
 * @author Brian Dayhoff
 */
class Icon extends \Oroboros\bootstrap\abstracts\library\component\html\AbstractBootstrapComponent
{

    const COMPONENT_TEMPLATE = 'component/icon/icon';
    const COMPONENT_KEY = 'icon';

    private $context_keys = [
        'icon',
    ];
    private $context_component_filters = [
        'icon' => 'filterIcon',
    ];

    protected function filterIcon($icon)
    {
        if (is_null($icon)) {
            return null;
        }
        if (is_string($icon)) {
            $icon = ['icon' => $icon];
        }
        if (is_object($icon) && ($icon instanceof \Oroboros\core\interfaces\library\container\ContainerInterface)) {
            $icon = $icon->toArray();
        }
        if (array_key_exists('icon', $icon)) {
            $icon['name'] = $icon['icon'];
            unset($icon['icon']);
        }
        return $this->containerize($icon);
    }

    protected function declareValidContextKeys(): array
    {
        return array_merge(parent::declareValidContextKeys(), $this->context_keys);
    }

    protected function declareContextFilters(): array
    {
        return array_merge(parent::declareContextFilters(), $this->context_component_filters);
    }

    /**
     * Initializes the icon baseline context
     * 
     * @return void
     */
    protected function initializeContext(): void
    {
        parent::initializeContext();
        // Default tagname is "li"
        $this->addContext('tagname', 'span');
        // Default id is the passed command
        $this->addContext('id', $this->getCommand());
        // Default icon class
        $this->addContext('class', 'icon text-muted d-flex font-weight-lighter');
    }
}
