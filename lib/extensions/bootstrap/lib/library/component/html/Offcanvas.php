<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\bootstrap\library\component\html;

/**
 * Bootstrap Offcanvas Component
 * Represents a component wrapper for a Bootstrap offcanvas component
 * @author Brian Dayhoff
 */
class Offcanvas extends \Oroboros\bootstrap\abstracts\library\component\html\AbstractBootstrapComponent
{

    const COMPONENT_TEMPLATE = 'component/bootstrap/offcanvas/offcanvas';
    const COMPONENT_KEY = 'offcanvas';

    private $context_keys = [
        'orientation',
        'backdrop',
        'scroll',
        'escapable',
        'dismissable',
        'header',
        'title',
        'brand',
        'ui',
        'footer',
    ];
    private $context_component_filters = [
        'orientation' => 'filterOrientation',
        'backdrop' => 'filterBackdrop',
        'scroll' => 'filterScrollable',
        'escapable' => 'filterEscapable',
        'dismissable' => 'filterDismissable',
    ];

    protected function declareValidContextKeys(): array
    {
        return array_merge(parent::declareValidContextKeys(), $this->context_keys);
    }

    protected function declareContextFilters(): array
    {
        return array_merge(parent::declareContextFilters(), $this->context_component_filters);
    }

    protected function getDefaultScripts(): array
    {
        return array_merge_recursive(parent::getDefaultScripts(), [
            'footer' => [sprintf('%1$s-%2$s', static::CLASS_SCOPE, self::COMPONENT_KEY)]
        ]);
    }

    /**
     * Registers the correct orienttation for the offcanvas element.
     * Valid options are `['start', 'end', 'top', 'bottom']`
     * 
     * @param string $orientation
     * @return string
     * @throws \Oroboros\core\exception\InvalidArgumentException if the orientation provided is not valid
     */
    protected function filterOrientation(string $orientation): string
    {
        $valid = ['start', 'end', 'top', 'bottom'];
        switch ($orientation) {
            case in_array($orientation, $valid, true):
                return sprintf('offcanvas-%1$s', $orientation);
            default:
                throw new \Oroboros\core\exception\InvalidArgumentException(
                        sprintf('Error encountered in [%1$s]. Provided orientation mode [%2$s] is not valid. Valid orientations are [%3$s].', get_class($this), $orientation, implode(', ', $valid))
                );
        }
    }

    /**
     * Forces boolean for backdrop argument
     * 
     * @param bool $backdrop
     * @return bool
     */
    protected function filterBackdrop(bool $backdrop): bool
    {
        return $backdrop;
    }

    /**
     * Forces boolean for scrollable argument
     * 
     * @param bool $scrollable
     * @return bool
     */
    protected function filterScrollable(bool $scrollable): bool
    {
        return $scrollable;
    }

    /**
     * Forces boolean for escapable argument
     * 
     * @param bool $escapable
     * @return bool
     */
    protected function filterEscapable(bool $escapable): bool
    {
        return $escapable;
    }

    /**
     * Forces boolean for dismissable argument
     * 
     * @param bool $dismissable
     * @return bool
     */
    protected function filterDismissable(bool $dismissable): bool
    {
        return $dismissable;
    }

    /**
     * Sets default template context
     * 
     * @return void
     */
    protected function initializeContext(): void
    {
        parent::initializeContext();
        // Default behavior is left side orientation
        $this->addContext('orientation', 'start');
        // Default behavior has a backdrop
        $this->addContext('backdrop', true);
        // Default behavior is not scrollable
        $this->addContext('scrollable', false);
        // Default behavior is to close on escape key press
        $this->addContext('escapable', true);
        // Default behavior is to provide a dismiss button if a header or title exist
        $this->addContext('dismissable', true);
        // no assumption that a title exists
        $this->addContext('title', null);
        // no assumption that a sidebar header exists
        $this->addContext('header', null);
        // no assumption that a sidebar brand exists
        $this->addContext('brand', null);
        // no assumption that a sidebar ui exists
        $this->addContext('ui', null);
        // no assumption that a sidebar footer exists
        $this->addContext('footer', null);
    }
}
