<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\bootstrap\abstracts\library\component\html;

/**
 * Abstract Bootstrap UI Component
 * Provides abstraction for standard Bootstrap 5 components
 * that provide end user page controls
 * 
 * @author Brian Dayhoff
 */
abstract class AbstractBootstrapUiComponent extends AbstractBootstrapComponent
{

    use \Oroboros\bootstrap\traits\library\component\BootstrapUiComponentAbstractionCommonTrait;
    use \Oroboros\bootstrap\traits\library\component\BootstrapUiComponentAbstractionTrait;

    /**
     * Declares the permission node checked for root user context
     * 
     * You may override this to loosen this permission if the situation warrants
     * on a per-component basis.
     */
    const PERMISSION_ROOT_CONTENT = '*';

    /**
     * Declares the permission node checked for admin user context
     * 
     * You may override this to loosen or tighten this permission if the situation warrants
     * on a per-component basis.
     */
    const PERMISSION_ADMIN_CONTENT = 'http.admin.view';

    /**
     * Declares the permission node checked for moderator user context
     * 
     * You may override this to loosen or tighten this permission if the situation warrants
     * on a per-component basis.
     */
    const PERMISSION_MODERATOR_CONTENT = 'http.moderator.view';

    /**
     * Declares the permission node checked for staff user context
     * 
     * You may override this to loosen or tighten this permission if the situation warrants
     * on a per-component basis.
     */
    const PERMISSION_STAFF_CONTENT = 'http.staff.view';

    /**
     * Declares the permission node checked for registered user context
     * 
     * You may override this to loosen or tighten this permission if the situation warrants
     * on a per-component basis.
     */
    const PERMISSION_REGISTERED_CONTENT = 'http.registered.view';

    /**
     * Declares the permission node checked for public user context
     * 
     * You may override this to loosen or tighten this permission if the situation warrants
     * on a per-component basis.
     */
    const PERMISSION_PUBLIC_CONTENT = 'http.frontend.view';

    /**
     * Declares the baseline class used to generate a ui linkset
     */
    const UI_DEFAULT_LINKSET_CLASS = 'html\\ui\\UiLinkset';

    /**
     * Declares the baseline class used to generate a ui link
     */
    const UI_DEFAULT_LINK_CLASS = 'html\\ui\\UiLink';

    /**
     * Declares the baseline class used to generate a ui icon
     */
    const UI_DEFAULT_ICON_CLASS = \Oroboros\bootstrap\library\component\html\ui\UiIcon::class;

    /**
     * Declares the baseline class used to generate a ui icon
     */
    const UI_DEFAULT_BADGE_CLASS = \Oroboros\bootstrap\library\component\html\Badge::class;

    /**
     * Declares the baseline class used to generate a ui icon
     */
    const UI_DEFAULT_MENU_CLASS = \Oroboros\bootstrap\library\component\html\ui\UiDropdownMenu::class;

    /**
     * Baseline context keys for ui components
     * 
     * @var array
     */
    private $context_keys = [
        // UI for public users and above
        'public',
        // UI for registered users and above
        'registered',
        // UI for staff users and above
        'staff',
        // UI for moderator users and above
        'moderator',
        // UI for admin users and above
        'admin',
        // UI for debugging
        'debugger',
        // General UI links
        'links',
        // Single UI link
        'link',
        // UI Icons
        'icon',
        // UI Dropdown Menu
        'menu',
        // UI Button
        'button',
        // UI Button Target
        'ui-target',
    ];

    /**
     * Baseline context filters for ui components
     * 
     * @var array
     */
    private $context_component_filters = [
        'icon' => 'filterIcon',
        'button' => 'filterButton',
        'ui-target' => 'filterUiTarget',
        'link' => 'filterLink',
        'links' => 'filterLinks',
        'menu' => 'filterMenu',
        'public' => 'filterPublicUserContent',
        'registered' => 'filterRegisteredUserContent',
        'staff' => 'filterStaffUserContent',
        'moderator' => 'filterModeratorUserContent',
        'admin' => 'filterAdminUserContent',
        'root' => 'filterRootUserContent',
        'debugger' => 'filterDebugger',
    ];

    /**
     * Sets the given component as the target for the ui component
     * 
     * @param \Oroboros\core\interfaces\library\component\HtmlComponentInterface $target
     * @return void
     */
    public function setUiTarget(\Oroboros\core\interfaces\library\component\HtmlComponentInterface $target): void
    {
        $result = [];
        $result['target'] = sprintf('#%1$s', $target->getContext('id'));
        $this->addContext('ui-target', $this->containerize($result));
    }

    /**
     * Declares the valid baseline context keys for ui components
     * 
     * @return array
     */
    protected function declareValidContextKeys(): array
    {
        return array_merge(parent::declareValidContextKeys(), $this->context_keys);
    }

    /**
     * Declares the context filter methods for baseline ui component context keys
     * 
     * @return array
     */
    protected function declareContextFilters(): array
    {
        return array_merge(parent::declareContextFilters(), $this->context_component_filters);
    }

    /**
     * Filters the target details and returns a container of details,
     * or null if none apply
     * 
     * @param mixed $target
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface|null
     */
    protected function filterUiTarget($target): ?\Oroboros\core\interfaces\library\container\ContainerInterface
    {
        if (is_null($target)) {
            return null;
        }
        $result = [];
        if (is_string($target)) {
            $result['target'] = $target;
        }
        if (is_array($target)) {
            $target = $this->containerize($target);
        }
        if (is_object($target) && ($target instanceof \Oroboros\core\interfaces\library\container\ContainerInterface)) {
            if ($target->has('id')) {
                $result['target'] = sprintf('#%1$s', ltrim($target['id'], '#'));
            }
            if ($target->has('target')) {
                $result['target'] = $target['target'];
            }
        }
        if (array_key_exists('class', $result)) {
            unset($result['class']);
        }
        if (array_key_exists('id', $result)) {
            unset($result['id']);
        }
        if (!array_key_exists('role', $result)) {
            $result['role'] = 'button';
        }
        if (!array_key_exists('toggle', $result)) {
            $result['toggle'] = 'dropdown';
        }
        if (!array_key_exists('parent', $result)) {
            $result['parent'] = $this->getContext('id');
        }
        if (!array_key_exists('category', $result) && $this->hasContext('category')) {
            $result['category'] = $this->getContext('category');
        }
        if (!array_key_exists('context', $result) && $this->hasContext('context')) {
            $result['context'] = $this->getContext('context');
        }
        return $this->containerize($result);
    }

    protected function filterButton($button): ?\Oroboros\core\interfaces\library\container\ContainerInterface
    {
        if (is_null($button)) {
            return null;
        }
        return $this->containerize($button);
    }

    protected function filterMenu($menu): ?\Oroboros\core\interfaces\library\component\ComponentInterface
    {
        if (is_null($menu)) {
            return null;
        }
        $id = null;
        $class = static::UI_DEFAULT_MENU_CLASS;
        if (is_object($menu) && ($menu instanceof \Oroboros\core\interfaces\library\component\ComponentInterface)) {
            return $menu;
        }
        if (is_string($menu)) {
            $menu = ['id' => $menu];
        }
        if (is_array($menu)) {
            $menu = $this->containerize($menu);
        }
        if ($menu->has('id')) {
            $id = $menu->id;
        }
        if ($menu->has('component')) {
            $class = $menu->component;
        }
        $component = $this->getComponent($class, $id, $menu->toArray());
        $component->setLogger($this->getLogger());
        $component->setParent($this);
        $this->setUiTarget($component);
        $target = $this->getContext('ui-target');
        $target['toggle'] = 'dropdown';
        $this->addContext('ui-target', $target);
        return $component;
    }

    protected function filterBadge($badge): ?\Oroboros\core\interfaces\library\component\ComponentInterface
    {
        if (is_null($menu)) {
            return null;
        }
    }

    /**
     * Initializes the ui baseline context
     * 
     * @return void
     */
    protected function initializeContext(): void
    {
        parent::initializeContext();
        // No assumption of a ui target
        $this->addContext('ui-target', null);
        // Default tagname is "li"
        $this->addContext('tagname', 'li');
        // Default id is the passed command
        $this->addContext('id', $this->getCommand());
        // Default ui class
        $this->addContext('class', 'nav-item dropdown no-arrow d-flex flex-row align-items-center h-100 my-auto');
    }
}
