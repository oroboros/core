<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\bootstrap\abstracts\library\component\html;

/**
 * Abstract Bootstrap Component
 * Provides abstraction for Bootstrap components
 * @author Brian Dayhoff
 */
abstract class AbstractBootstrapFormComponent extends \Oroboros\core\abstracts\library\component\html\AbstractHtmlFormComponent implements \Oroboros\bootstrap\interfaces\library\component\html\BootstrapComponentInterface
{

    use \Oroboros\core\traits\pluggable\ExtensionLibraryTrait;
    use \Oroboros\bootstrap\traits\library\component\BootstrapComponentAbstractionCommonTrait;

    /**
     * Declares the extension name as bootstrap
     */
    const EXTENSION_NAME = 'bootstrap';

    /**
     * Default fieldset is bootstrap form-groups
     */
    const FORM_FIELDSET_CLASS = \Oroboros\bootstrap\library\component\html\FormGroup::class;

}
