<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\bootstrap\abstracts\library\component\html;

/**
 * Abstract Bootstrap UI Menu Component
 * Provides abstraction for standard Bootstrap 5 components
 * that provide a set if ui controls
 * 
 * @author Brian Dayhoff
 */
abstract class AbstractBootstrapUiMenuComponent extends AbstractBootstrapComponent
{

    use \Oroboros\bootstrap\traits\library\component\BootstrapUiComponentAbstractionCommonTrait;
    use \Oroboros\bootstrap\traits\library\component\BootstrapUiComponentAbstractionTrait;

    /**
     * Declares the permission node checked for root user context
     * 
     * You may override this to loosen this permission if the situation warrants
     * on a per-component basis.
     */
    const PERMISSION_ROOT_CONTENT = '*';

    /**
     * Declares the permission node checked for admin user context
     * 
     * You may override this to loosen or tighten this permission if the situation warrants
     * on a per-component basis.
     */
    const PERMISSION_ADMIN_CONTENT = 'http.admin.view';

    /**
     * Declares the permission node checked for moderator user context
     * 
     * You may override this to loosen or tighten this permission if the situation warrants
     * on a per-component basis.
     */
    const PERMISSION_MODERATOR_CONTENT = 'http.moderator.view';

    /**
     * Declares the permission node checked for staff user context
     * 
     * You may override this to loosen or tighten this permission if the situation warrants
     * on a per-component basis.
     */
    const PERMISSION_STAFF_CONTENT = 'http.staff.view';

    /**
     * Declares the permission node checked for registered user context
     * 
     * You may override this to loosen or tighten this permission if the situation warrants
     * on a per-component basis.
     */
    const PERMISSION_REGISTERED_CONTENT = 'http.registered.view';

    /**
     * Declares the permission node checked for public user context
     * 
     * You may override this to loosen or tighten this permission if the situation warrants
     * on a per-component basis.
     */
    const PERMISSION_PUBLIC_CONTENT = 'http.frontend.view';

    /**
     * Declares the baseline class used to generate a ui linkset
     */
    const UI_DEFAULT_LINKSET_CLASS = 'html\\ui\\UiLinkset';

    /**
     * Declares the baseline class used to generate a ui link
     */
    const UI_DEFAULT_LINK_CLASS = 'html\\ui\\UiLink';
    
    /**
     * Declares the baseline class used to generate a ui icon
     */
    const UI_DEFAULT_ICON_CLASS = \Oroboros\bootstrap\library\component\html\ui\UiIcon::class;
    
    /**
     * Baseline context keys for ui components
     * 
     * @var array
     */
    private $context_keys = [
        // UI for public users and above
        'public',
        // UI for registered users and above
        'registered',
        // UI for staff users and above
        'staff',
        // UI for moderator users and above
        'moderator',
        // UI for admin users and above
        'admin',
        // UI for debugging
        'debugger',
        // General UI links
        'links',
        // Single UI link
        'link',
        // UI Icons
        'icon',
    ];

    /**
     * Baseline context filters for ui components
     * 
     * @var array
     */
    private $context_component_filters = [
        'icon' => 'filterIcon',
        'link' => 'filterLink',
        'links' => 'filterLinks',
        'public' => 'filterPublicUserContent',
        'registered' => 'filterRegisteredUserContent',
        'staff' => 'filterStaffUserContent',
        'moderator' => 'filterModeratorUserContent',
        'admin' => 'filterAdminUserContent',
        'root' => 'filterRootUserContent',
        'debugger' => 'filterDebugger',
    ];

    /**
     * Declares the valid baseline context keys for ui components
     * 
     * @return array
     */
    protected function declareValidContextKeys(): array
    {
        return array_merge(parent::declareValidContextKeys(), $this->context_keys);
    }

    /**
     * Declares the context filter methods for baseline ui component context keys
     * 
     * @return array
     */
    protected function declareContextFilters(): array
    {
        return array_merge(parent::declareContextFilters(), $this->context_component_filters);
    }

    /**
     * Initializes the ui menu baseline context
     * 
     * @return void
     */
    protected function initializeContext(): void
    {
        parent::initializeContext();
        // Default tagname is "li"
        $this->addContext('tagname', 'ul');
        // Default id is the passed command
        $this->addContext('id', $this->getCommand());
        // Default ui class
        $this->addContext('class', 'nav-item dropdown my-auto no-arrow d-flex flex-row align-items-center');
    }
}
