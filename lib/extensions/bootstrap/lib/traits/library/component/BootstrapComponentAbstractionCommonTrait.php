<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\bootstrap\traits\library\component;

/**
 * Bootstrap Component Abstraction Common Trait
 * Provides common abstraction for bootstrap abstract components
 * 
 * This is only intended to be used by abstract
 * components for the bootstrap extension.
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
trait BootstrapComponentAbstractionCommonTrait
{

    /**
     * Declares the valid bootstrap context keys
     * 
     * @var array
     */
    private $bootstrap_context_keys = [
        'toggle',
        'target',
        'expanded',
    ];
    
    /**
     * Declares the bootstrap context filters
     * 
     * @var array
     */
    private $bootstrap_context_component_filters = [];

    /**
     * Returns the bootstrap context keys, and underlying context keys
     * 
     * @return array
     */
    protected function declareValidContextKeys(): array
    {
        return array_merge(parent::declareValidContextKeys(), $this->bootstrap_context_keys);
    }

    /**
     * Returns the boostrap context filters, and underlying context filters
     * 
     * @return array
     */
    protected function declareContextFilters(): array
    {
        return array_merge(parent::declareContextFilters(), $this->bootstrap_context_component_filters);
    }

    /**
     * Registers the bootstrap script as a dependency
     * @return array
     */
    protected function getDefaultScripts(): array
    {
        return array_replace_recursive(parent::getDefaultScripts(), [
            'footer' => ['bootstrap']
        ]);
    }

    /**
     * Registers the bootstrap styling
     * 
     * @return array
     */
    protected function getDefaultStyles(): array
    {
        return array_replace_recursive(parent::getDefaultStyles(), [
            'bootstrap'
        ]);
    }

    /**
     * Returns default bootstrap fonts
     * 
     * @return array
     */
    protected function getDefaultFonts(): array
    {
        return array_replace_recursive(parent::getDefaultFonts(), []);
    }
}
