<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\bootstrap\traits\library\component;

/**
 * Bootstrap Ui Component Abstraction Linkset Trait
 * Provides common methods for the ui menu linkset component abstraction
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
trait BootstrapUiComponentAbstractionLinksetTrait
{

    /**
     * Filters link declarations for the linkset
     * 
     * @param iterable $debug
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface|null
     */
    protected function filterLinks(iterable $links = null): ?\Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $linkset = $this->containerize();
        if (is_null($links)) {
            return $links;
        }
        if (is_array($links)) {
            $links = $this->containerize($links);
        }
        if ($links->has('permission') && !$this::user()->can($links->permission)) {
            // Not authorized
            return null;
        }
        foreach ($links as $key => $value) {
            if (is_object($value) && ($value instanceof \Oroboros\core\interfaces\library\component\ComponentInterface)) {
                $linkset[$key] = $value;
                continue;
            }
            $this->addContext($key, $value);
        }
        return $linkset;
    }
}
