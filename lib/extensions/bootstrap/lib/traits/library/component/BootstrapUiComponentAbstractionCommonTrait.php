<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\bootstrap\traits\library\component;

/**
 * Bootstrap Ui Component Abstraction Common Trait
 * Provides common methods for the bootstrap ui components
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
trait BootstrapUiComponentAbstractionCommonTrait
{

    /**
     * Filters an individual link and generates a link component,
     * or `null` if the current user does not have sufficient
     * access rights for the ui link.
     * 
     * @param iterable $link
     * @return \Oroboros\core\interfaces\library\component\ComponentInterface|null
     */
    protected function filterIdentity($identity = null): ?\Oroboros\core\interfaces\library\component\ComponentInterface
    {
        return null;
    }

    /**
     * Filters an individual link and generates a link component,
     * or `null` if the current user does not have sufficient
     * access rights for the ui link.
     * 
     * @param iterable $link
     * @return \Oroboros\core\interfaces\library\component\ComponentInterface|null
     */
    protected function filterLink(iterable $link = null): ?\Oroboros\core\interfaces\library\component\ComponentInterface
    {
        if (is_null($link)) {
            return null;
        }
        if (is_array($link)) {
            $link = $this::containerize($link);
        }
        $id = null;
        if ($link->has('permission') && !$this::user()->can($link->permission)) {
            return null;
        }
        if ($link->has('id')) {
            $id = $link->id;
        }
        return $this->generateLink($id, $link);
    }

    /**
     * Returns an icon component from the provided details.
     * 
     * @param array $icon
     * @return \Oroboros\core\interfaces\library\component\ComponentInterface
     */
    protected function filterIcon(array $icon): \Oroboros\core\interfaces\library\component\ComponentInterface
    {
        $id = null;
        $icon = $this::containerize($icon);
        if ($icon->has('id')) {
            $id = $icon->id;
        }
        return $this->generateIcon($id, $icon);
    }

    /**
     * Filters content that should only appear for accounts with
     * public user level access or higher.
     * 
     * This will always return `null` if the user does not have
     * sufficient access rights or is blacklisted.
     * 
     * @param iterable|null $links
     * @return \Oroboros\core\interfaces\library\component\ComponentInterface|null
     */
    protected function filterPublicUserContent(iterable $links = null): ?\Oroboros\core\interfaces\library\component\ComponentInterface
    {
        if (is_null($links) || $this::user()->isBlacklisted() || !$this::user()->can(static::PERMISSION_PUBLIC_CONTENT)) {
            return null;
        }
        $id = trim(sprintf('%1$s-%2$s', $this->getContext('id'), 'public-linkset'), '-');
        return $this->generateLinkset($id, $links);
    }

    /**
     * Filters content that should only appear for accounts with
     * registered user level access or higher.
     * 
     * This will always return `null` if the user does not have
     * sufficient access rights or is not logged in.
     * 
     * @param iterable|null $links
     * @return \Oroboros\core\interfaces\library\component\ComponentInterface|null
     */
    protected function filterRegisteredUserContent(iterable $links = null): ?\Oroboros\core\interfaces\library\component\ComponentInterface
    {
        if (is_null($links) || !$this::user()->isLoggedIn() || !$this::user()->can(static::PERMISSION_REGISTERED_CONTENT)) {
            return null;
        }
        $id = trim(sprintf('%1$s-%2$s', $this->getContext('id'), 'login-linkset'), '-');
        return $this->generateLinkset($id, $links);
    }

    /**
     * Filters content that should only appear for accounts with
     * staff level access or higher.
     * 
     * This will always return `null` if the user does not have
     * sufficient access rights or is not logged in.
     * 
     * @param iterable|null $links
     * @return \Oroboros\core\interfaces\library\component\ComponentInterface|null
     */
    protected function filterStaffUserContent(iterable $links = null): ?\Oroboros\core\interfaces\library\component\ComponentInterface
    {
        if (is_null($links) || !$this::user()->isLoggedIn() || !$this::user()->can(static::PERMISSION_STAFF_CONTENT)) {
            return null;
        }
        $id = trim(sprintf('%1$s-%2$s', $this->getContext('id'), 'staff-linkset'), '-');
        return $this->generateLinkset($id, $links);
    }

    /**
     * Filters content that should only appear for accounts with
     * moderator level access or higher.
     * 
     * This will always return `null` if the user does not have
     * sufficient access rights or is not logged in.
     * 
     * @param iterable|null $links
     * @return \Oroboros\core\interfaces\library\component\ComponentInterface|null
     */
    protected function filterModeratorUserContent(iterable $links = null): ?\Oroboros\core\interfaces\library\component\ComponentInterface
    {
        if (is_null($links) || !$this::user()->isLoggedIn() || !$this::user()->can(static::PERMISSION_MODERATOR_CONTENT)) {
            return null;
        }
        $id = trim(sprintf('%1$s-%2$s', $this->getContext('id'), 'moderator-linkset'), '-');
        return $this->generateLinkset($id, $links);
    }

    /**
     * Filters content that should only appear for accounts with
     * admin level access or higher.
     * 
     * This will always return `null` if the user does not have
     * sufficient access rights or is not logged in.
     * 
     * @param iterable|null $links
     * @return \Oroboros\core\interfaces\library\component\ComponentInterface|null
     */
    protected function filterAdminUserContent(iterable $links = null): ?\Oroboros\core\interfaces\library\component\ComponentInterface
    {
        if (is_null($links) || !$this::user()->isLoggedIn() || !$this::user()->can(static::PERMISSION_ADMIN_CONTENT)) {
            return null;
        }
        $id = trim(sprintf('%1$s-%2$s', $this->getContext('id'), 'admin-linkset'), '-');
        return $this->generateLinkset($id, $links);
    }

    /**
     * Filters content that should only appear for the root user.
     * 
     * This will always return `null` if the user is not root or is not logged in.
     * 
     * @param iterable $debug
     * @return \Oroboros\core\interfaces\library\component\ComponentInterface|null
     */
    protected function filterRootUserContent(iterable $links = null): ?\Oroboros\core\interfaces\library\component\ComponentInterface
    {
        if (is_null($links) || !$this::user()->isLoggedIn() || !$this::user()->can(static::PERMISSION_ROOT_CONTENT)) {
            return null;
        }
        $id = trim(sprintf('%1$s-%2$s', $this->getContext('id'), 'root-linkset'), '-');
        return $this->generateLinkset($id, $links);
    }

    /**
     * Filters content that should only appear if debug is enabled.
     * 
     * This will always return `null` if debug mode is disabled.
     * 
     * @param iterable $debug
     * @return \Oroboros\core\interfaces\library\component\ComponentInterface|null
     */
    protected function filterDebugger(iterable $links = null): ?\Oroboros\core\interfaces\library\component\ComponentInterface
    {
        if (is_null($links) || !$this::app()->debugEnabled()) {
            return null;
        }
        $id = trim(sprintf('%1$s-%2$s', $this->getContext('id'), 'debug-linkset'), '-');
        return $this->generateLinkset($id, $links);
    }

    /**
     * Generates a linkset containing any passed links,
     * or an empty one if no links are passed or the user
     * does not have access rights for any of them.
     * 
     * @param iterable $links
     * @return \Oroboros\core\interfaces\library\component\ComponentInterface
     */
    protected function generateLinkset(string $id = null, iterable $links = null): \Oroboros\core\interfaces\library\component\ComponentInterface
    {
        $link_container = $this::containerize();
        if (is_iterable($links)) {
            if (is_array($links)) {
                $links = $this->containerize($links);
            }
            foreach ($links as $key => $link) {
                if (is_object($link) && ($link instanceof \Oroboros\core\interfaces\library\component\ComponentInterface)) {
                    $link_container[$key] = $link;
                    continue;
                }
                if (!is_iterable($link)) {
                    continue;
                }
                $link_component = $this->generateLink($key, $link);
                if (!is_null($link_component)) {
                    $link_container[$key] = $link_component;
                }
            }
        }
        $component = $this->getComponent(static::UI_DEFAULT_LINKSET_CLASS, $id);
        $component->setLogger($this->getLogger());
        $component->setParent($this);
        if ($links->has('id')) {
            $component->addContext('id', $links->id);
        }
        if ($links->has('permission')) {
            $component->addContext('permission', $links->permission);
        }
        if ($links->has('category')) {
            $component->addContext('category', $links->category);
        }
        if ($links->has('context')) {
            $component->addContext('context', $links->context);
        }
        $component->addContext('links', $link_container);
        return $component;
    }

    /**
     * Generates a link component from the provided details,
     * or `null` if no link is passed or the user
     * does not have access rights for the link.
     * 
     * @param iterable $link
     * @return \Oroboros\core\interfaces\library\component\ComponentInterface
     */
    protected function generateLink(string $id = null, iterable $link = null): ?\Oroboros\core\interfaces\library\component\ComponentInterface
    {
        if (is_null($link)) {
            return null;
        }
        if (is_array($link)) {
            $link = $this::containerize($link);
        }
        if ($link->has('permission') && !$this::user()->can('permission')) {
            return null;
        }
        $class = static::UI_DEFAULT_LINK_CLASS;
        if ($link->has('classname')) {
            $class = $link->classname;
            unset($link->classname);
        }
        if ($link->has('id')) {
            $id = $link->id;
            unset($link->id);
        }
        if ($link->has('component')) {
            $class = $link->component;
            unset($link->component);
        }
        $component = $this->getComponent($class, $id, $link->toArray());
        $component->setLogger($this->getLogger());
        $component->setParent($this);
        if ($link->has('id')) {
            $component->addContext('id', $link->id);
        }
        if ($link->has('class')) {
            $component->addContext('class', $link->class);
        }
        if ($link->has('permission')) {
            $component->addContext('permission', $link->permission);
        }
        if ($link->has('category')) {
            $component->addContext('category', $link->category);
        }
        if ($link->has('context')) {
            $component->addContext('context', $link->context);
        }
        if ($link->has('icon')) {
            $component->addContext('icon', $link->icon);
        }
//        d($this, $id, $link, $link->toArray(), $this, $class, $component);
        return $component;
    }

    /**
     * Generates an icon component from the provided details
     * 
     * @param string $id
     * @param iterable $icon
     * @return \Oroboros\core\interfaces\library\component\ComponentInterface
     */
    protected function generateIcon(string $id = null, \Oroboros\core\interfaces\library\container\ContainerInterface $icon = null): \Oroboros\core\interfaces\library\component\ComponentInterface
    {
        $class = static::UI_DEFAULT_ICON_CLASS;
        if ($icon->has('id')) {
            $id = $icon->id;
            unset($icon->id);
        }
        if ($icon->has('classname')) {
            $class = $icon->classname;
            unset($icon->classname);
        }
        if ($icon->has('component')) {
            $class = $icon->component;
            unset($icon->component);
        }
        if ($icon->has('parent')) {
            unset($icon->parent);
        }
//        d($id, $icon, $class);
        $component = $this->getComponent($class, $id);
        $component->setLogger($this->getLogger());
        if ($icon->has('icon')) {
            $component->addContext('icon', $icon->icon);
        }
        if ($icon->has('link')) {
            $component->addContext('link', $icon->link);
        }
//        foreach ($icon as $key => $context) {
////            d($key, $context);
//            $component->addContext($key, $context);
//        }
        $component->setParent($this);
//        d($this, $id, $icon, $component); exit;
        return $component;
    }
}
