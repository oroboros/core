<?php
namespace Oroboros\core;

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Oroboros Core Base Control Class
 * Provides direct system control outwardly to other packages.
 * This class is a generalized facade for common internals,
 * and wraps complex chains of functionality into a straightforward,
 * simplified api, deferring logical chains and complex tasks to objects
 * tasked with more granular efforts.
 *
 * @author Brian Dayhoff
 * @final
 */
final class Oroboros
{

    /**
     * Primary initializiation occurs during setup,
     * and may not occur more than one time.
     * 
     * Further calls to initialize are safely non-blocking.
     * @var bool
     */
    private static $is_initialized = false;

    /**
     * Imported configuration files
     * @var Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private static $configs = null;

    /**
     * Imported extensions
     * @var Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private static $extensions = null;

    /**
     * Imported modules
     * @var Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private static $modules = null;

    /**
     * Imported packages
     * @var Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private static $packages = null;

    /**
     * Environment build director
     * @var Oroboros\core\interfaces\library\environment\EnvironmentDirectorInterface
     */
    private static $environment = null;

    /**
     * Imported Psr-4 namespaces
     * @var Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private static $namespaces = null;

    /**
     * The package Psr-4 autoloader.
     * Injected by the setup file prior to initialization.
     * 
     * @var Oroboros\core\library\autoload\Autoloader
     */
    private static $autoloader = null;
    
    /**
     * The currently running bootstrap object.
     * @var \Oroboros\core\interfaces\bootstrap\BootstrapInterface
     */
    private static $bootstrap = null;

    /**
     * Initialize Method
     * Performs the original setup initialization.
     * This occurs one time when the setup is included, and does not occur again.
     * This method will do nothing if called a second time.
     * @return void
     */
    public static function initialize(): void
    {
        if (!self::$is_initialized) {
            self::initializeNamespaceContainer();
            self::initializePackageContainer();
            self::initializeModuleContainer();
            self::initializeExtensionContainer();
            self::initializeConfigurationContainer();
            self::initializeEnvironmentBuilder();
            self::$is_initialized = true;
        }
    }

    /**
     * Import Method
     * This method is used to import extensions, modules, packages,
     * and other dependencies compatible with this system.
     * 
     * This method outwardly is duck-typed to make it versatile,
     * but references statically typed methods under the hood.
     * A thin type validation layer is applied to insure that errors
     * can be handled when calling this method rather than crashing the build.
     * This behavior may be overridden in the environment file to allow a
     * full crash for unit testing and development purposes, so that faulty
     * applications do not propagate to production.
     * 
     * @param string $type [environment|module|extension|template|package|connection]
     * @param string $package_class
     */
    public static function import(string $type, string $subject): void
    {
        switch ($package_type) {
            case 'environment':
                break;
            case 'module':
                break;
            case 'extension':
                break;
            case 'template':
                break;
            case 'package':
                break;
            case 'connection':
                break;
            default:
                throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Unknown import method [%2$s]', self::class, $package_type));
        }
    }

    /**
     * Config Method
     * Used to provide a custom configuration to the base class,
     * which will propogate through the bootstrap process.
     * 
     * Config files must be in json format.
     * 
     * @param string $file
     */
    public static function config(string $file): void
    {
        if (!is_readable($file)) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided configuration file [%2$s] does not exist or is not readable.', self::class, $file));
        }
        $config = json_decode(file_get_contents($file), 1);
    }

    /**
     * Bootstrap Method
     * @param \Oroboros\core\interfaces\application\ApplicationInterface $application (optional)
     *     If called without this parameter, the application will bootstrap
     *     the default build, which is a stateless middleware package.
     * @return void
     */
    public static function bootstrap(\Oroboros\core\interfaces\application\ApplicationInterface $application = null): void
    {
        if (is_null($application)) {
            $application = self::getDefaultMiddlewareApplication();
        }
        
        self::verifyApplicationConfiguration($application);
        self::$configs['application'] = \Oroboros\core\library\container\Container::init(null, json_decode(file_get_contents($application->getApplicationConfig()), 1));
        self::$autoloader->addNamespace($application->getBaseNamespace(), $application->getApplicationSourceDirectory(), true);
        self::setApplicationEnvironment($application);
        \Oroboros\core\abstracts\AbstractBase::setApplication($application);
        $proxy = new \Oroboros\core\pattern\factory\LoaderProxy();
        $application->importEventBroker($proxy->load('library', 'event\\EventBroker'));
        $application->importFilterBroker($proxy->load('library', 'filter\\FilterBroker'));
        $command = self::getApplicationBootstrapCommand($application);
        $arguments = self::getApplicationBootstrapArguments($application);
        $flags = self::getApplicationBootstrapFlags($application);
        $bootstrap_class = $application->getBootstrapClass();
        $bootstrap = new $bootstrap_class($command, $arguments, $flags);
        self::verifyBootstrap($bootstrap);
        self::$bootstrap = $bootstrap;
        $bootstrap->bootstrap();
    }

    public static function app(): \Oroboros\core\interfaces\application\ApplicationInterface
    {
        try {
            $application = \Oroboros\core\abstracts\AbstractBase::getApplication();
        } catch (\ErrorException $e) {
            // Application not defined
            throw $e;
        }
        return $application;
    }
    
    /**
     * Returns a container of all currently loaded modules.
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     * @throws \Oroboros\core\exception\ErrorException if no application is currently running
     */
    public static function modules(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        if (is_null(self::$bootstrap)) {
            throw new \Oroboros\core\exception\ErrorException(
                sprintf('Error encountered in [%1$s]. Cannot return modules because no application is currently running.', __CLASS__)
            );
        }
        return self::$bootstrap->getLoadedModules();
    }
    
    /**
     * Returns a container of all currently loaded extensions.
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     * @throws \Oroboros\core\exception\ErrorException if no application is currently running
     */
    public static function extensions(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        if (is_null(self::$bootstrap)) {
            throw new \Oroboros\core\exception\ErrorException(
                sprintf('Error encountered in [%1$s]. Cannot return extensions because no application is currently running.', __CLASS__)
            );
        }
        return self::$bootstrap->getLoadedExtensions();
    }
    
    /**
     * Returns a container of all currently loaded services.
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     * @throws \Oroboros\core\exception\ErrorException if no application is currently running
     */
    public static function services(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        if (is_null(self::$bootstrap)) {
            throw new \Oroboros\core\exception\ErrorException(
                sprintf('Error encountered in [%1$s]. Cannot return services because no application is currently running.', __CLASS__)
            );
        }
        return self::$bootstrap->getLoadedServices();
    }

    /**
     * Dependency injection method for the autoloader.
     * Only accepts the known functional autoloader class.
     * 
     * @param \Oroboros\core\library\autoload\Autoloader $autoloader
     * @return void
     */
    public static function setAutoloader(\Oroboros\core\library\autoload\Autoloader $autoloader): void
    {
        self::$autoloader = $autoloader;
    }

    private static function getDefaultMiddlewareApplication(): \Oroboros\core\application\DefaultMiddlewareApplication
    {
        $application = new \Oroboros\core\application\DefaultMiddlewareApplication();
        return $application;
    }

    private static function getApplicationBootstrapCommand(\Oroboros\core\interfaces\application\ApplicationInterface $application): ?string
    {
        return $application->getBootstrapCommand();
    }

    private static function getApplicationBootstrapArguments(\Oroboros\core\interfaces\application\ApplicationInterface $application): ?array
    {
        $arguments = $application->getBootstrapArguments();
        if (is_null($arguments)) {
            $arguments = [];
        }
        $proxy = new \Oroboros\core\pattern\factory\LoaderProxy();
        $arguments['application'] = $application;
        $arguments['routes'] = $application->getRoutes();
        $arguments['components'] = $application->getComponents();
        $arguments['event-broker'] = $proxy->load('library', 'event\\EventBroker');
        $arguments['filter-broker'] = $proxy->load('library', 'filter\\FilterBroker');
        return $arguments;
    }

    private static function getApplicationBootstrapFlags(\Oroboros\core\interfaces\application\ApplicationInterface $application): ?array
    {
        return $application->getBootstrapFlags();
    }

    private static function setApplicationEnvironment(\Oroboros\core\interfaces\application\ApplicationInterface $application): void
    {
        $env_loader = new \Symfony\Component\Dotenv\Dotenv();
        if (is_readable($application->getApplicationRoot() . '.env.defaults')) {
            $env_loader->load($application->getApplicationRoot() . '.env.defaults');
        }
        if (is_readable($application->getApplicationRoot() . '.env')) {
            $env_loader->overload($application->getApplicationRoot() . '.env');
        }
        $worker = self::$environment->getWorker('environment');
        foreach (array_merge(getenv(), $_ENV) as $key => $value) {
            $worker->set('environment', $key, $value);
        }
        $application->setEnvironment(self::$environment->build());
        self::updateApplicationScopes($application);
        self::updateApplicationConfigs($application);
    }

    private static function updateApplicationConfigs(\Oroboros\core\interfaces\application\ApplicationInterface $application): void
    {
        $file = $application->getApplicationConfig();
        $configs = self::$configs;
        if (!is_null($file)) {
            $data = json_decode(file_get_contents($file), 1);
            $app = $configs->get('app')->fromArray(array_replace_recursive($configs->get('app')->toArray(), $data));
            $configs['app'] = $app;
        }
        $application->setConfiguration($configs);
    }

    private static function updateApplicationScopes(\Oroboros\core\interfaces\application\ApplicationInterface $application): void
    {
        foreach ($application->getApplicationControllerScopes() as $scope) {
            \Oroboros\core\abstracts\controller\AbstractController::addClassScope($scope);
        }
    }

    private static function initializeEnvironmentBuilder(): void
    {
        $director = new \Oroboros\core\library\environment\EnvironmentDirector();
        $director->addWorker(new \Oroboros\core\library\environment\EnvironmentBuilder());
        $worker = $director->getWorker('environment');
        self::$environment = $director;
    }

    private static function initializeNamespaceContainer(): void
    {
        $container = new \Oroboros\core\library\container\Container();
        self::$namespaces = $container;
    }

    private static function initializePackageContainer(): void
    {
        $container = new \Oroboros\core\library\container\Container();
        self::$packages = $container;
    }

    private static function initializeModuleContainer(): void
    {
        $container = new \Oroboros\core\library\container\Container();
        self::$modules = $container;
    }

    private static function initializeExtensionContainer(): void
    {
        $container = new \Oroboros\core\library\container\Container();
        self::$extensions = $container;
    }

    private static function initializeConfigurationContainer(): void
    {
        $collection = new \Oroboros\core\library\container\ConfigCollection();
        foreach (glob(OROBOROS_CORE_CONFIG . '*.json') as $file) {
            $key = substr(basename($file), 0, strpos(basename($file), '.json'));
            if ($key === 'app-default') {
                $key = 'app';
            }
            $data = json_decode(file_get_contents($file), 1);
            $collection[$key] = \Oroboros\core\library\container\ConfigContainer::init(null, $data);
        }
        self::$configs = $collection;
    }

    private static function verifyBootstrap(\Oroboros\core\interfaces\bootstrap\BootstrapInterface $bootstrap): void
    {
        $expected = 'Oroboros\\core\\interfaces\\bootstrap\\BootstrapInterface';
        if (!in_array($expected, class_implements($bootstrap))) {
            throw new \Oroboros\core\exception\core\InvalidClassException(sprintf('Class [%1$s] is invalid. '
                        . 'Expected instance of [%2$s].', get_class($bootstrap), $expected));
        }
    }

    private static function verifyApplicationConfiguration(\Oroboros\core\interfaces\application\ApplicationInterface $application): void
    {
        $file = $application->getApplicationConfig();
        if (!is_readable($file)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(sprintf('Class [%1$s] is invalid. '
                        . 'Provided configuration [%2$s] does not exist or is not readable.', get_class($application), $file));
        }
    }
}
