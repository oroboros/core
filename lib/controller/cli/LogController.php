<?php
namespace Oroboros\core\controller\cli;

/**
 * Controls command line management interaction with application log and log management
 *
 * @author Brian Dayhoff
 */
final class LogController extends \Oroboros\core\abstracts\controller\AbstractCliController implements \Oroboros\core\interfaces\CoreInterface
{

    /**
     * Use the standard cli error controller
     */
    const ERROR_CONTROLLER = 'cli\\ErrorController';

    /**
     * Base initialization methods required for controller setup, if any.
     * @var array
     */
    private $setup = [];

    public function index()
    {
        $this->success_message('You have successfully loaded [the module controller].');
        exit(0);
    }
}
