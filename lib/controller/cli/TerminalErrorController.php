<?php
namespace Oroboros\core\controller\cli;

/**
 * Default Terminal Error Controller
 * Provides error handling for command line terminal consoles
 *
 * @author Brian Dayhoff
 */
final class TerminalErrorController extends \Oroboros\core\abstracts\controller\cli\AbstractTerminalErrorController implements \Oroboros\core\interfaces\CoreInterface
{
    // no-op
}
