<?php
namespace Oroboros\core\controller\cli;

/**
 * Controls the application from the command line
 *
 * @author Brian Dayhoff
 */
final class AppController extends \Oroboros\core\abstracts\controller\AbstractCliController implements \Oroboros\core\interfaces\CoreInterface
{

    /**
     * Use the standard cli error controller
     */
    const ERROR_CONTROLLER = 'cli\\ErrorController';

    /**
     * Base initialization methods required for controller setup, if any.
     * @var array
     */
    private $setup = [];

    public function index()
    {
        $this->success_message('You have successfully loaded [the app controller]. Booyah.');
        exit(0);
    }

    public function info()
    {
        $details = $this::app()->getApplicationDefinition();
        if ($details->has('name')) {
            $this->success_message($details->get('name'));
        }
        if ($details->has('version')) {
            $this->info_message(sprintf('Version: [%1$s]', $details->get('version')));
        }
        if ($details->has('namespace')) {
            $this->info_message(sprintf('Base Namespace: [%1$s]', $details->get('namespace')));
        }
        if ($details->has('domain')) {
            if (is_iterable($details->get('domain'))) {
                $this->info_message('Application Domains');
                foreach ($details->get('domain') as $name => $domain) {
                    $this->info_message(sprintf('[%1$s]: %2$s', $name, $domain));
                }
            } else {
                $this->info_message(sprintf('Application Domain: %2$s', $domain));
            }
        }
        exit(0);
    }

    public function name()
    {
        $details = $this::app()->getApplicationDefinition();
        if ($details->has('name')) {
            $this->print_message($details->get('name'));
        }
        exit(0);
    }

    public function domain()
    {
        $this->print_message($this::app()->getApplicationDomain());
        exit(0);
    }

    public function environment()
    {
        $this->print_message($this::app()->getApplicationEnvironment());
        exit(0);
    }

    public function path()
    {
        $this->print_message($this::app()->getApplicationRoot());
        exit(0);
    }

    public function configPath()
    {
        $this->print_message($this::app()->getApplicationRoot());
        exit(0);
    }

    public function publicPath()
    {
        $this->print_message($this::app()->getApplicationRoot());
        exit(0);
    }

    public function templatePath()
    {
        $this->print_message($this::app()->getApplicationRoot());
        exit(0);
    }

    public function sourcePath()
    {
        $this->print_message($this::app()->getApplicationSourceDirectory());
        exit(0);
    }

    public function testPath()
    {
        $this->print_message($this::app()->getApplicationRoot());
        exit(0);
    }

    public function namespace()
    {
        $this->print_message($this::app()->getBaseNamespace());
        exit(0);
    }

    public function version()
    {
        $details = $this::app()->getApplicationDefinition();
        if ($details->has('version')) {
            $this->print_message($details->get('version'));
        }
        exit(0);
    }

    public function register()
    {
        $pwd = $this->getPath();
        $this->print_message($pwd);
        exit(0);
    }

    protected function declareCliSetupMethods(): array
    {
        $methods = parent::declareCliSetupMethods();
        foreach ($this->setup as $method) {
            if (!in_array($method, $methods)) {
                $methods[] = $method;
            }
        }
        return $methods;
    }
}
