<?php
namespace Oroboros\core\controller\cli;

/**
 * Default Http Error Controller
 * Provides error handling for cli requests
 *
 * This is the default fallback error controller for all cli requests
 * if higher level cli error controllers fail.
 * 
 * @author Brian Dayhoff
 */
final class ErrorController extends \Oroboros\core\abstracts\controller\AbstractCliErrorController implements \Oroboros\core\interfaces\CoreInterface
{

    const CLASS_SCOPE = 'cli';

}
