<?php
namespace Oroboros\core\controller\cli;

/**
 * Controls command line interaction with the application
 *
 * @author Brian Dayhoff
 */
final class CliIndexController extends \Oroboros\core\abstracts\controller\AbstractCliController implements \Oroboros\core\interfaces\CoreInterface
{

    /**
     * Use the standard cli error controller
     */
    const ERROR_CONTROLLER = 'cli\\ErrorController';

    /**
     * The currently scoped application.
     * @var $app \Oroboros\core\interfaces\application\ApplicationInterface
     */
    private $app = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->app = \Oroboros\core\Oroboros::app();
    }

    public function index()
    {
        $path = OROBOROS_CORE_BASE_DIRECTORY . 'composer.json';
        $definition = $this->load('library', 'parser\\JsonParser', $path)->fetch();
        $this->print_message(sprintf('Oroboros CLI, version [%1$2s]', $definition['version']));
        $this->print_message(sprintf('For help, enter [%1$2s] or [%1$s %2$s]', 'oroboros help', '<command>'));
    }

    private function fetchCliRouteInfo()
    {
        $routes = [];
        foreach ($this->getArgument('router')->getRoutes() as $key => $value) {
            $formatted = trim($value->getUri(), '/');
            if ($formatted === '') {
                continue;
            }
            if (count(explode('/', $formatted)) === 1) {
                $routes[$formatted] = [];
            }
        }
        return $routes;
    }
}
