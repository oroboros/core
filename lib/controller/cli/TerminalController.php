<?php
namespace Oroboros\core\controller\cli;

/**
 * Controls the interactive application terminal
 *
 * @author Brian Dayhoff
 */
final class TerminalController extends \Oroboros\core\abstracts\controller\cli\AbstractTerminalController implements \Oroboros\core\interfaces\pattern\observer\ObserverInterface, \Oroboros\core\interfaces\CoreInterface
{
    // no-op
}
