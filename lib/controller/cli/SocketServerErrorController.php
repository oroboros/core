<?php
namespace Oroboros\core\controller\cli;

/**
 * Default Web Socket Server Error Controller
 * Provides error handling for socket server instances
 *
 * @author Brian Dayhoff
 */
final class SocketServerErrorController extends \Oroboros\core\abstracts\controller\cli\AbstractSocketServerErrorController implements \Oroboros\core\interfaces\CoreInterface
{
    // no-op
}
