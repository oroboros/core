<?php
namespace Oroboros\core\controller\cli;

/**
 * Provides system information for the command line bash library to consume
 *
 * @author Brian Dayhoff
 */
final class SystemController extends \Oroboros\core\abstracts\controller\AbstractCliController implements \Oroboros\core\interfaces\CoreInterface
{

    /**
     * Use the standard cli error controller
     */
    const ERROR_CONTROLLER = 'cli\\ErrorController';

    /**
     * Base initialization methods required for controller setup, if any.
     * @var array
     */
    private $setup = [];

    public function index()
    {
        $this->success_message('You have successfully loaded [the system controller].');
        exit(0);
    }
}
