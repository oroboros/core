<?php
namespace Oroboros\core\controller\cli;

/**
 * Controls the websocket server
 *
 * @author Brian Dayhoff
 */
final class SocketServerController extends \Oroboros\core\abstracts\controller\cli\AbstractSocketServerController implements \Oroboros\core\interfaces\CoreInterface
{
    // no-op
}
