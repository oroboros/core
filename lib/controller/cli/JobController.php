<?php
namespace Oroboros\core\controller\cli;

/**
 * Controls command line management interaction with job queues
 *
 * @author Brian Dayhoff
 */
final class JobController extends \Oroboros\core\abstracts\controller\cli\AbstractJobController implements \Oroboros\core\interfaces\CoreInterface
{
    // no-op
}
