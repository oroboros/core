<?php
namespace Oroboros\core\controller\cli;

/**
 * Controls help menus for the command line
 *
 * @author Brian Dayhoff
 */
final class HelpController extends \Oroboros\core\abstracts\controller\AbstractCliController implements \Oroboros\core\interfaces\CoreInterface
{

    /**
     * Use the standard cli error controller
     */
    const ERROR_CONTROLLER = 'cli\\ErrorController';

    /**
     * Base initialization methods required for controller setup, if any.
     * @var array
     */
    private $setup = [];

    /**
     * 
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $docs = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
    }

    public function index(\Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\CliControllerInterface
    {
        if (is_null($arguments)) {
            $arguments = $this->containerize();
        }
        if (!$this->getModules()->has('docs')) {
            return $this->error(404, 'No help page found because the documentation module is not installed.');
        }
        $controller = $this->load('controller', sprintf('%1$s\\DocumentationController', static::CLASS_SCOPE), $this->getCommand(), $this->getArguments(), $this->getFlags());
        $topic = [
            'category' => 'core',
            'subcategory' => 'usage',
            'article' => 'cli',
        ];
        $arguments = $arguments->toArray();
        $key = array_search('help', $arguments, true);
        if ($key !== false) {
            unset($arguments[$key]);
        }
        $iter = 1;
        foreach ($arguments as $key => $arg) {
            $keyname = sprintf('%1$s%2$s', 'sub', $iter);
            $topic[$keyname] = $arg;
            $iter++;
        }
        return $controller->categoryIndex($this->containerize($topic), $flags);
    }

    public function commandHelp(\Oroboros\core\interfaces\library\container\ContainerInterface $command)
    {
        $this->print_message(sprintf('You have successfully loaded [the help controller] for command [%1$s].', $command['command']));
        try {
            $help = $this->lookupCommand($command['command']);
            $this->print_message(PHP_EOL . sprintf($this->getHelpTitle($command['command'])));
            $this->print_message(str_pad('', strlen($this->getHelpTitle($command['command'])), '-'));
            $this->print_message(sprintf('Description: %1$s', $this->getHelpDescription($command['command'])));
            $this->print_message(sprintf('Usage: %1$s', $this->getHelpUsage($command['command'])));
            $this->print_message(sprintf('Added in version: %1$s', $this->getHelpAdded($command['command'])));
            $this->print_message(sprintf('Additional Documentation: %1$s', $this->getHelpDocs($command['command'])));
        } catch (\InvalidArgumentException $e) {
            $this->error_message(sprintf('Command [%1$s] is not known. See [%2$s] for a list of valid commands.', $command['command'], 'oroboros help'));
        }
        exit(0);
    }

    private function lookupCommand(string $command): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $help = $this->docs;
        if (!$this->docs->has($command)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Command [%2$s] is not valid.', get_class($this), $command));
        }
        return $this->containerize($help[$command]);
    }

    private function fetchCliRouteInfo()
    {
        $routes = [];
        foreach ($this->getArgument('router')->getRoutes() as $key => $value) {
            $formatted = trim($value->getUri(), '/');
            if ($formatted === '') {
                continue;
            }
            if (count(explode('/', $formatted)) === 1) {
                $routes[$formatted] = [];
            }
        }
        return $routes;
    }

    private function getHelpTitle(string $command)
    {
        $help = $this->docs[$command];
        if (!array_key_exists('title', $help) || $help['title'] === '') {
            return $command;
        }
        return $help['title'];
    }

    private function getHelpDescription(string $command)
    {
        $help = $this->docs[$command];
        if (!array_key_exists('description', $help) || $help['description'] === '') {
            return 'No description provided.';
        }
        return $help['description'];
    }

    private function getHelpUsage(string $command, bool $concise = false)
    {
        $help = $this->docs[$command];
        if (!array_key_exists('usage', $help) || $help['usage'] === '') {
            return 'no usage information provided.';
        }
        if (is_string($help['usage'])) {
            return $help['usage'];
        }
        if ($concise) {
            return sprintf('See: [oroboros help %1$s]', $command);
        }
        $message = PHP_EOL;
        foreach ($help['usage'] as $usage) {
            $message .= PHP_EOL;
            if (array_key_exists('title', $usage)) {
                $message .= $usage['title'] . PHP_EOL;
            }
            if (array_key_exists('description', $usage)) {
                $message .= ((is_array($usage['description'])) ? implode(PHP_EOL, $usage['description']) : $usage['description']) . PHP_EOL;
            }
            if (array_key_exists('usage', $usage)) {
                $message .= '[' . $usage['usage'] . ']' . PHP_EOL;
            }
        }
        return $message;
    }

    private function getHelpAdded(string $command)
    {
        $help = $this->docs[$command];
        if (!array_key_exists('added', $help) || $help['added'] === '') {
            return 'unknown';
        }
        return $help['added'];
    }

    private function getHelpDocs(string $command)
    {
        $help = $this->docs[$command];
        if (!array_key_exists('docs', $help) || $help['docs'] === '') {
            return 'No additional documentation provided.';
        }
        return $help['docs'];
    }
}
