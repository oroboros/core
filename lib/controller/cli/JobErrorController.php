<?php
namespace Oroboros\core\controller\cli;

/**
 * Default Cli Job Error Controller
 * Provides error handling for job worker instances
 *
 * @author Brian Dayhoff
 */
final class JobErrorController extends \Oroboros\core\abstracts\controller\cli\AbstractJobErrorController implements \Oroboros\core\interfaces\CoreInterface
{
    // no-op
}
