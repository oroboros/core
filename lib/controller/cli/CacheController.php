<?php
namespace Oroboros\core\controller\cli;

/**
 * Controls caching layers from the command line
 *
 * @author Brian Dayhoff
 */
final class CacheController extends \Oroboros\core\abstracts\controller\AbstractCliController implements \Oroboros\core\interfaces\CoreInterface
{

    /**
     * Use the standard cli error controller
     */
    const ERROR_CONTROLLER = 'cli\\ErrorController';

    /**
     * Base initialization methods required for controller setup, if any.
     * @var array
     */
    private $setup = [];

    public function index()
    {
        $this->success_message('You have successfully loaded [the cache controller].');
        exit(0);
    }

    public function flush(\Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null)
    {
        if (is_null($arguments)) {
            $this->clear_screen();
            $this->info_message('Flushing all caches.');
            $this->print_message('What is the capital of Kathmandu?');
            $input = $this->read_input(' answer > ', true);
            $this->print_message(sprintf('Your answer is [%1$s]', $input));
            // Flush all caches
        } else {
            $key = $arguments->get('cache');
            $this->info_message(sprintf('Flushing cache [%1$s].', $key));
            // Flush the specified cache
        }
        exit(0);
    }
}
