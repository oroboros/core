<?php
namespace Oroboros\core\controller\html\frontend;

/**
 * Default Frontend Html Error Controller
 * Provides error handling for frontend html requests
 * 
 * @author Brian Dayhoff
 */
final class ErrorController extends \Oroboros\core\abstracts\controller\AbstractErrorPageController implements \Oroboros\core\interfaces\CoreInterface
{

    const CLASS_SCOPE = 'frontend';

    /**
     * Use the standard html error controller as a fallback resource if this controller fails
     */
    const ERROR_CONTROLLER = 'html\\ErrorController';

}
