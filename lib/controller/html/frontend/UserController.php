<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\controller\html\frontend;

/**
 * Controls public account access and self-management for logged in users
 *
 * @author Brian Dayhoff
 */
final class UserController extends \Oroboros\core\abstracts\controller\AbstractPageController implements \Oroboros\core\interfaces\CoreInterface
{

    use \Oroboros\core\traits\html\DefinerPageContentUtility;

    const CLASS_SCOPE = 'frontend';

    /**
     * Use the html default frontend error controller
     */
    const ERROR_CONTROLLER = 'html\\frontend\\ErrorController';

    public function index(\Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\HtmlControllerInterface
    {
        if ($this::user()->isBlacklisted() || !$this::user()->isLoggedIn()) {
            return $this->error(404);
        }
        try {
            $this->setupPage('user');
            $this->queuePageAssets();
            $this->injectUserIdentity();
            return $this->render('html', 'frontend/index');
        } catch (\Exception $e) {
            return $this->error(500, $e->getMessage());
        }
    }

    public function activity(\Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\HtmlControllerInterface
    {
        if ($this::user()->isBlacklisted() || !$this::user()->isLoggedIn()) {
            return $this->error(404);
        }
        try {
            $this->setupPage('user');
            $this->queuePageAssets();
            $this->injectUserIdentity();
            return $this->render('html', 'frontend/index');
        } catch (\Exception $e) {
            return $this->error(500, $e->getMessage());
        }
    }

    public function address(\Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\HtmlControllerInterface
    {
        if ($this::user()->isBlacklisted() || !$this::user()->isLoggedIn()) {
            return $this->error(404);
        }
        try {
            $this->setupPage('user');
            $this->queuePageAssets();
            $this->injectUserIdentity();
            return $this->render('html', 'frontend/index');
        } catch (\Exception $e) {
            return $this->error(500, $e->getMessage());
        }
    }

    public function content(\Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\HtmlControllerInterface
    {
        if ($this::user()->isBlacklisted() || !$this::user()->isLoggedIn()) {
            return $this->error(404);
        }
        try {
            $this->setupPage('user');
            $this->queuePageAssets();
            $this->injectUserIdentity();
            return $this->render('html', 'frontend/index');
        } catch (\Exception $e) {
            return $this->error(500, $e->getMessage());
        }
    }

    public function identity(\Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\HtmlControllerInterface
    {
        if ($this::user()->isBlacklisted() || !$this::user()->isLoggedIn()) {
            return $this->error(404);
        }
        try {
            $this->setupPage('user');
            $this->queuePageAssets();
            $this->injectUserIdentity();
            return $this->render('html', 'frontend/index');
        } catch (\Exception $e) {
            return $this->error(500, $e->getMessage());
        }
    }

    public function information(\Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\HtmlControllerInterface
    {
        if ($this::user()->isBlacklisted() || !$this::user()->isLoggedIn()) {
            return $this->error(404);
        }
        try {
            $this->setupPage('user');
            $this->queuePageAssets();
            $this->injectUserIdentity();
            return $this->render('html', 'frontend/index');
        } catch (\Exception $e) {
            return $this->error(500, $e->getMessage());
        }
    }

    public function legal(\Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\HtmlControllerInterface
    {
        if ($this::user()->isBlacklisted() || !$this::user()->isLoggedIn()) {
            return $this->error(404);
        }
        try {
            $this->setupPage('user');
            $this->queuePageAssets();
            $this->injectUserIdentity();
            return $this->render('html', 'frontend/index');
        } catch (\Exception $e) {
            return $this->error(500, $e->getMessage());
        }
    }

    public function media(\Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\HtmlControllerInterface
    {
        if ($this::user()->isBlacklisted() || !$this::user()->isLoggedIn()) {
            return $this->error(404);
        }
        try {
            $this->setupPage('user');
            $this->queuePageAssets();
            $this->injectUserIdentity();
            return $this->render('html', 'frontend/index');
        } catch (\Exception $e) {
            return $this->error(500, $e->getMessage());
        }
    }

    public function notifications(\Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\HtmlControllerInterface
    {
        if ($this::user()->isBlacklisted() || !$this::user()->isLoggedIn()) {
            return $this->error(404);
        }
        try {
            $this->setupPage('user');
            $this->queuePageAssets();
            $this->injectUserIdentity();
            return $this->render('html', 'frontend/index');
        } catch (\Exception $e) {
            return $this->error(500, $e->getMessage());
        }
    }

    public function organizations(\Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\HtmlControllerInterface
    {
        if ($this::user()->isBlacklisted() || !$this::user()->isLoggedIn()) {
            return $this->error(404);
        }
        try {
            $this->setupPage('user');
            $this->queuePageAssets();
            $this->injectUserIdentity();
            return $this->render('html', 'frontend/index');
        } catch (\Exception $e) {
            return $this->error(500, $e->getMessage());
        }
    }

    public function preferences(\Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\HtmlControllerInterface
    {
        if ($this::user()->isBlacklisted() || !$this::user()->isLoggedIn()) {
            return $this->error(404);
        }
        try {
            $this->setupPage('user');
            $this->queuePageAssets();
            $this->injectUserIdentity();
            return $this->render('html', 'frontend/index');
        } catch (\Exception $e) {
            return $this->error(500, $e->getMessage());
        }
    }

    public function profile(\Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\HtmlControllerInterface
    {

        try {
            $this->setupPage('user');
            $this->queuePageAssets();
            $this->injectUserIdentity();
            return $this->render('html', 'frontend/index');
        } catch (\Exception $e) {
            return $this->error(500, $e->getMessage());
        }
    }

    public function relationships(\Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\HtmlControllerInterface
    {
        if ($this::user()->isBlacklisted() || !$this::user()->isLoggedIn()) {
            return $this->error(404);
        }
        try {
            $this->setupPage('user');
            $this->queuePageAssets();
            $this->injectUserIdentity();
            return $this->render('html', 'frontend/index');
        } catch (\Exception $e) {
            return $this->error(500, $e->getMessage());
        }
    }

    public function security(\Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\HtmlControllerInterface
    {
        if ($this::user()->isBlacklisted() || !$this::user()->isLoggedIn()) {
            return $this->error(404);
        }
        try {
            $this->setupPage('user');
            $this->queuePageAssets();
            $this->injectUserIdentity();
            return $this->render('html', 'frontend/index');
        } catch (\Exception $e) {
            return $this->error(500, $e->getMessage());
        }
    }

    public function settings(\Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\HtmlControllerInterface
    {
        if ($this::user()->isBlacklisted() || !$this::user()->isLoggedIn()) {
            return $this->error(404);
        }
        try {
            $this->setupPage('user');
            $this->queuePageAssets();
            $this->injectUserIdentity();
            return $this->render('html', 'frontend/index');
        } catch (\Exception $e) {
            return $this->error(500, $e->getMessage());
        }
    }

    public function uploads(\Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\HtmlControllerInterface
    {
        if ($this::user()->isBlacklisted() || !$this::user()->isLoggedIn()) {
            return $this->error(404);
        }
        try {
            $this->setupPage('user');
            $this->queuePageAssets();
            $this->injectUserIdentity();
            return $this->render('html', 'frontend/index');
        } catch (\Exception $e) {
            return $this->error(500, $e->getMessage());
        }
    }

    protected function injectUserIdentity(): void
    {
        $this->getLayout()
            ->getContent('main')['user']
            ->addContext('identity', $this::user()->identity());
    }
}
