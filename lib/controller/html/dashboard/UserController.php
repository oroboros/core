<?php
namespace Oroboros\core\controller\html\dashboard;

/**
 * Controls site user operations from the admin dashboard
 *
 * @author Brian Dayhoff
 */
final class UserController extends \Oroboros\core\abstracts\controller\AbstractDashboardPage implements \Oroboros\core\interfaces\CoreInterface
{

    use \Oroboros\core\traits\html\DefinerPageContentUtility;

    const CLASS_SCOPE = 'dashboard';

    /**
     * Use the html default dashboard error controller
     */
    const ERROR_CONTROLLER = 'html\\dashboard\\ErrorController';

    public function index(): \Oroboros\core\interfaces\controller\DashboardPageInterface
    {
        try {
            $this->setupPage('index');
            $this->queueDashboardAssets();
            return $this->render('html', 'dashboard/index');
        } catch (\Exception $e) {
            return $this->error(500, $e->getMessage());
        }
    }

    public function settings(): \Oroboros\core\interfaces\controller\DashboardPageInterface
    {
        try {
            $this->setupPage('index');
            $this->queueDashboardAssets();
            return $this->render('html', 'dashboard/index');
        } catch (\Exception $e) {
            return $this->error(500, $e->getMessage());
        }
    }
    
    public function create(\Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null): \Oroboros\core\interfaces\controller\DashboardPageInterface
    {
        try {
            $this->setupPage('index');
            $this->queueDashboardAssets();
            return $this->render('html', 'dashboard/index');
        } catch (\Exception $e) {
            return $this->error(500, $e->getMessage());
        }
    }

    public function edit(\Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\DashboardPageInterface
    {
        try {
            $this->setupPage('index');
            $this->queueDashboardAssets();
            return $this->render('html', 'dashboard/index');
        } catch (\Exception $e) {
            return $this->error(500, $e->getMessage());
        }
    }

    public function delete(\Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\DashboardPageInterface
    {
        try {
            $this->setupPage('index');
            $this->queueDashboardAssets();
            return $this->render('html', 'dashboard/index');
        } catch (\Exception $e) {
            return $this->error(500, $e->getMessage());
        }
    }
    
    public function import(\Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\DashboardPageInterface
    {
        try {
            $this->setupPage('index');
            $this->queueDashboardAssets();
            return $this->render('html', 'dashboard/index');
        } catch (\Exception $e) {
            return $this->error(500, $e->getMessage());
        }
    }

    public function export(\Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\DashboardPageInterface
    {
        try {
            $this->setupPage('index');
            $this->queueDashboardAssets();
            return $this->render('html', 'dashboard/index');
        } catch (\Exception $e) {
            return $this->error(500, $e->getMessage());
        }
    }

    public function groupIndex(): \Oroboros\core\interfaces\controller\DashboardPageInterface
    {
        try {
            $this->setupPage('index');
            $this->queueDashboardAssets();
            return $this->render('html', 'dashboard/index');
        } catch (\Exception $e) {
            return $this->error(500, $e->getMessage());
        }
    }

    public function groupSettings(): \Oroboros\core\interfaces\controller\DashboardPageInterface
    {
        try {
            $this->setupPage('index');
            $this->queueDashboardAssets();
            return $this->render('html', 'dashboard/index');
        } catch (\Exception $e) {
            return $this->error(500, $e->getMessage());
        }
    }
    
    public function groupCreate(\Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null): \Oroboros\core\interfaces\controller\DashboardPageInterface
    {
        try {
            $this->setupPage('index');
            $this->queueDashboardAssets();
            return $this->render('html', 'dashboard/index');
        } catch (\Exception $e) {
            return $this->error(500, $e->getMessage());
        }
    }

    public function groupEdit(\Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\DashboardPageInterface
    {
        try {
            $this->setupPage('index');
            $this->queueDashboardAssets();
            return $this->render('html', 'dashboard/index');
        } catch (\Exception $e) {
            return $this->error(500, $e->getMessage());
        }
    }

    public function groupDelete(\Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\DashboardPageInterface
    {
        try {
            $this->setupPage('index');
            $this->queueDashboardAssets();
            return $this->render('html', 'dashboard/index');
        } catch (\Exception $e) {
            return $this->error(500, $e->getMessage());
        }
    }
    
    public function groupImport(\Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\DashboardPageInterface
    {
        try {
            $this->setupPage('index');
            $this->queueDashboardAssets();
            return $this->render('html', 'dashboard/index');
        } catch (\Exception $e) {
            return $this->error(500, $e->getMessage());
        }
    }

    public function groupExport(\Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\DashboardPageInterface
    {
        try {
            $this->setupPage('index');
            $this->queueDashboardAssets();
            return $this->render('html', 'dashboard/index');
        } catch (\Exception $e) {
            return $this->error(500, $e->getMessage());
        }
    }

    private function queueDashboardAssets()
    {
        $this->addScript('footer', 'dashboard', 'dashboard');
        $this->addScript('footer', 'chart.js', 'chart.js');
        $this->addScript('footer', 'demo-dashboard-chart-area', 'demo-dashboard-chart-area');
        $this->addScript('footer', 'demo-dashboard-chart-pie', 'demo-dashboard-chart-pie');
        $this->addScript('footer', 'demo-dashboard-datatables', 'demo-dashboard-datatables');
        $this->addStylesheet('stylesheet', 'dashboard', 'dashboard');
        $this->addStylesheet('stylesheet', 'chart.js', 'chart.js');
    }
}
