<?php
namespace Oroboros\core\controller\html\dashboard;

/**
 * Default Html Dashboard Error Controller
 * Provides error handling for html requests to dashboard pages
 *
 * @author Brian Dayhoff
 */
final class ErrorController extends \Oroboros\core\abstracts\controller\AbstractErrorDashboardPage implements \Oroboros\core\interfaces\CoreInterface
{

    const CLASS_SCOPE = 'dashboard';

    public function error(int $code = null, string $message = null, \Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\ErrorControllerInterface
    {
        $this->addScript('footer', 'dashboard', 'dashboard');
        $this->addStylesheet('stylesheet', 'dashboard', 'dashboard');
        return parent::error($code, $message, $arguments, $flags);
    }
}
