<?php
namespace Oroboros\core\controller\html\dashboard;

/**
 * Controls site design operations from the admin dashboard
 *
 * @author Brian Dayhoff
 */
final class DesignController extends \Oroboros\core\abstracts\controller\AbstractDashboardPage implements \Oroboros\core\interfaces\CoreInterface
{

    use \Oroboros\core\traits\html\DefinerPageContentUtility;

    const CLASS_SCOPE = 'dashboard';

    /**
     * Use the html default dashboard error controller
     */
    const ERROR_CONTROLLER = 'html\\dashboard\\ErrorController';

    public function index(): \Oroboros\core\interfaces\controller\DashboardPageInterface
    {
        try {
            $this->setupPage('index');
            $this->queueDashboardAssets();
            return $this->render('html', 'dashboard/index');
        } catch (\Exception $e) {
            return $this->error(500, $e->getMessage());
        }
    }

    public function layouts(): \Oroboros\core\interfaces\controller\DashboardPageInterface
    {
        try {
            $this->setupPage('index');
            $this->queueDashboardAssets();
            return $this->render('html', 'dashboard/index');
        } catch (\Exception $e) {
            return $this->error(500, $e->getMessage());
        }
    }

    public function themes(): \Oroboros\core\interfaces\controller\DashboardPageInterface
    {
        try {
            $this->setupPage('index');
            $this->queueDashboardAssets();
            return $this->render('html', 'dashboard/index');
        } catch (\Exception $e) {
            return $this->error(500, $e->getMessage());
        }
    }

    public function styles(): \Oroboros\core\interfaces\controller\DashboardPageInterface
    {
        try {
            $this->setupPage('index');
            $this->queueDashboardAssets();
            return $this->render('html', 'dashboard/index');
        } catch (\Exception $e) {
            return $this->error(500, $e->getMessage());
        }
    }

    private function queueDashboardAssets()
    {
        $this->addScript('footer', 'dashboard', 'dashboard');
        $this->addScript('footer', 'chart.js', 'chart.js');
        $this->addScript('footer', 'demo-dashboard-chart-area', 'demo-dashboard-chart-area');
        $this->addScript('footer', 'demo-dashboard-chart-pie', 'demo-dashboard-chart-pie');
        $this->addScript('footer', 'demo-dashboard-datatables', 'demo-dashboard-datatables');
        $this->addStylesheet('stylesheet', 'dashboard', 'dashboard');
        $this->addStylesheet('stylesheet', 'chart.js', 'chart.js');
    }
}
