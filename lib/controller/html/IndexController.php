<?php
namespace Oroboros\core\controller\html;

/**
 * Controls the index of the application
 *
 * @author Brian Dayhoff
 */
final class IndexController extends \Oroboros\core\abstracts\controller\AbstractPageController implements \Oroboros\core\interfaces\CoreInterface
{

    use \Oroboros\core\traits\html\DefinerPageContentUtility;

    const CLASS_SCOPE = 'frontend';

    /**
     * Use the standard html error controller
     */
    const ERROR_CONTROLLER = 'html\\ErrorController';

    /**
     * Handles the default index page
     * @return \Oroboros\core\interfaces\controller\ControllerInterface
     */
    public function index()
    {
        try {
            $this->setupPage('index');
            return $this->render('html', 'demo/index');
        } catch (\Exception $e) {
            d($e->getTrace());
            return $this->error(500, $e->getMessage());
        }
    }

    /**
     * Handles the default about page
     * @return \Oroboros\core\interfaces\controller\ControllerInterface
     */
    public function about()
    {
        try {
            $this->setupPage('about');
            return $this->render('html', 'demo/about');
        } catch (\Exception $e) {
            return $this->error(500, $e->getMessage());
        }
    }
}
