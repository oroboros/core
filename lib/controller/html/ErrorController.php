<?php
namespace Oroboros\core\controller\html;

/**
 * Default Html Error Controller
 * Provides error handling for html requests
 *
 * @author Brian Dayhoff
 */
final class ErrorController extends \Oroboros\core\abstracts\controller\AbstractErrorPageController implements \Oroboros\core\interfaces\CoreInterface
{

    const CLASS_SCOPE = 'frontend';
    
    /**
     * Use the http error controller as a fallback resource if this controller fails
     */
    const ERROR_CONTROLLER = 'http\\ErrorController';

}
