<?php
namespace Oroboros\core\controller\http;

/**
 * Default Http Error Controller
 * Provides error handling for http requests
 * 
 * This is the default fallback controller
 * for all http requests if higher level error controllers fail.
 * 
 * @author Brian Dayhoff
 */
final class ErrorController extends \Oroboros\core\abstracts\controller\AbstractErrorPageController implements \Oroboros\core\interfaces\CoreInterface
{

    const CLASS_SCOPE = 'http';

}
