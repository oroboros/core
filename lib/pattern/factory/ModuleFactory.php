<?php
namespace Oroboros\core\pattern\factory;

/**
 * Loads modules, and packages them with the provided parameters.
 * 
 * The return object will be ready for use immediately.
 *
 * @author Brian Dayhoff
 */
final class ModuleFactory extends \Oroboros\core\abstracts\pattern\factory\AbstractFactory implements \Oroboros\core\interfaces\CoreInterface
{

    /**
     * Constrains this factory to only modules.
     */
    const FACTORY_TARGET_INTERFACE = 'Oroboros\\core\\interfaces\\module\\ModuleInterface';

}
