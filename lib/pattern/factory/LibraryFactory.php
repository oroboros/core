<?php
namespace Oroboros\core\pattern\factory;

/**
 * Loads libraries, and packages them with the provided parameters.
 * 
 * The return object will be ready for use immediately.
 *
 * @author Brian Dayhoff
 */
final class LibraryFactory extends \Oroboros\core\abstracts\pattern\factory\AbstractFactory implements \Oroboros\core\interfaces\CoreInterface
{

    /**
     * Constrains this factory to only libraries.
     */
    const FACTORY_TARGET_INTERFACE = 'Oroboros\\core\\interfaces\\library\\LibraryInterface';

}
