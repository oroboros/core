<?php
namespace Oroboros\core\pattern\factory;

/**
 * Loads components, and packages them with the provided parameters.
 * 
 * The return object will be ready for use immediately.
 *
 * @author Brian Dayhoff
 */
final class ComponentFactory extends \Oroboros\core\abstracts\pattern\factory\AbstractFactory implements \Oroboros\core\interfaces\CoreInterface
{

    /**
     * Constrains this factory to only libraries.
     */
    const FACTORY_TARGET_INTERFACE = 'Oroboros\\core\\interfaces\\library\\component\\ComponentInterface';

    public function load(string $class, string $command = null, array $arguments = null, array $flags = null)
    {
        $loader = new \Oroboros\core\pattern\factory\LoaderProxy();
        return parent::load($class, $command, $arguments, $flags);
    }
}
