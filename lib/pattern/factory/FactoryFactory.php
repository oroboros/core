<?php
namespace Oroboros\core\pattern\factory;

/**
 * Loads other factories, and packages them with the provided parameters.
 * 
 * This will return the target factory instance, which will then return the actual desired instance.
 * 
 * Super meta, but it works well and is pretty flexible.
 *
 * @author Brian Dayhoff
 * @note This class will not load another instance of itself,
 *  so an infinite recursive loop does not occur.
 * @note There is a loader trait implemented on all base class type abstracts
 *  that provides a `load` method, so most all classes do not need to concern
 *  themselves with specific factories.
 * @see \Oroboros\core\traits\LoaderTrait
 */
final class FactoryFactory extends \Oroboros\core\abstracts\pattern\factory\AbstractFactory implements \Oroboros\core\interfaces\CoreInterface
{

    /**
     * Constrains this factory to only other factories.
     */
    const FACTORY_TARGET_INTERFACE = 'Oroboros\\core\\interfaces\\pattern\\factory\\FactoryInterface';

}
