<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\pattern\factory;

/**
 * Simple Loader Proxy
 * Used as a proxy for factories to leverage the loader trait,
 * which otherwise conflicts with their internals.
 *
 * @author Brian Dayhoff
 */
final class LoaderProxy extends \Oroboros\core\abstracts\AbstractBase implements \Oroboros\core\interfaces\CoreInterface
{

    /**
     * Designates the class as a loader.
     */
    const CLASS_TYPE = 'loader';

    use \Oroboros\core\traits\LoaderTrait {
        load as trait_load;
        getFullClassName as trait_getFullClassName;
    }

    /**
     * Public class loader method
     * @param string $type [controller, model, view, library]
     * @param string $instance The name of the class to generate an object instance of.
     *  The base PSR-4 vendor\package portion may be omitted.
     * @param string $command (optional) Not required unless the factorized class requires this parameter in its constructor.
     * @param array $arguments (optional) Not required unless the factorized class requires this parameter in its constructor.
     * @param array $flags (optional) Not required unless the factorized class requires this parameter in its constructor.
     * @return \Oroboros\core\interfaces\BaseInterface The response object depends on the sub-factory's scoped interface.
     *  All responses will implement the base interface.
     */
    public function load(string $type, string $instance, string $command = null, array $arguments = null, array $flags = null)
    {
        return $this->trait_load($type, $instance, $command, $arguments, $flags);
    }

    /**
     * Returns the full classname of a stub class, or false if it does not exist.
     * @param string $type [library|view|controller|component] (any keyword with a corresponding registered factory)
     * @param string $instance The stub class name
     * @return string|false Returns the full class name, or false if it does not exist.
     */
    public function getFullClassName(string $type, string $instance)
    {
        return $this->trait_getFullClassName($type, $instance);
    }
}
