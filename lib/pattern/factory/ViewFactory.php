<?php
namespace Oroboros\core\pattern\factory;

/**
 * Loads views, and packages them with the provided parameters.
 * 
 * The return object will be ready for use immediately.
 *
 * @author Brian Dayhoff
 */
final class ViewFactory extends \Oroboros\core\abstracts\pattern\factory\AbstractFactory implements \Oroboros\core\interfaces\CoreInterface
{

    /**
     * Constrains this factory to only views.
     */
    const FACTORY_TARGET_INTERFACE = 'Oroboros\\core\\interfaces\\view\\ViewInterface';

    /**
     * 
     * @param string $class The name of the class to generate an object instance of.
     * @param string $command (optional) Not required unless the factorized class requires this parameter in its constructor.
     * @param array $arguments (optional) Not required unless the factorized class requires this parameter in its constructor.
     * @param array $flags (optional) Not required unless the factorized class requires this parameter in its constructor.
     * @return \Oroboros\core\interfaces\BaseInterface The returned class will always be an instance of the base interface,
     *  and additionally will be an instance of the interface defined by the class constant `FACTORY_TARGET_INTERFACE`.
     */
    public function load(string $class, string $command = null, array $arguments = null, array $flags = null)
    {
        $this->verifyClass($class);
        if (is_null($arguments)) {
            $arguments = [];
        }
        if (!array_key_exists('template-engine', $arguments)) {
            $template_engine_class = $this::app()->environment()->get('baseline')->get('template')['engine'];
            if (
                $this::app()->environment()->has('application') && $this::app()->environment()->get('application')->has('template') && property_exists($this::app()->environment()->get('application')->get('template'), 'engine')
            ) {
                $template_engine_class = $this::app()->environment()->get('application')->get('template')['engine'];
            }
            $loader = new LoaderProxy();
            $arguments['template-engine'] = $loader->load('library', $template_engine_class);
        }
        $object = new $class($command, $arguments, $flags);
        return $object;
    }
}
