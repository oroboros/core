<?php
namespace Oroboros\core\pattern\factory;

/**
 * Loads controllers, and packages them with the provided parameters.
 * 
 * The return object will be ready for use immediately.
 *
 * @author Brian Dayhoff
 */
final class ControllerFactory extends \Oroboros\core\abstracts\pattern\factory\AbstractFactory implements \Oroboros\core\interfaces\CoreInterface
{

    /**
     * Constrains this factory to only controllers.
     */
    const FACTORY_TARGET_INTERFACE = 'Oroboros\\core\\interfaces\\controller\\ControllerInterface';

}
