<?php
namespace Oroboros\core\pattern\factory;

/**
 * Loads layouts, and packages them with the provided parameters.
 * 
 * The return object will be ready for use immediately.
 *
 * @author Brian Dayhoff
 */
final class LayoutFactory extends \Oroboros\core\abstracts\pattern\factory\AbstractFactory implements \Oroboros\core\interfaces\CoreInterface
{

    /**
     * Constrains this factory to only layouts.
     */
    const FACTORY_TARGET_INTERFACE = 'Oroboros\\core\\interfaces\\library\\layout\\LayoutInterface';

    public function load(string $class, string $command = null, array $arguments = null, array $flags = null)
    {
        $loader = new \Oroboros\core\pattern\factory\LoaderProxy();
        $engine_class = $class::TEMPLATE_ENGINE;
        $engine = $loader->load('library', $engine_class);
        if (is_null($arguments)) {
            $arguments = [];
        }
        $arguments['template-engine'] = $engine;
        return parent::load($class, $command, $arguments, $flags);
    }
}
