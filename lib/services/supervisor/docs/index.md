# Oroboros Supervisor Service Documentation {#title}

- [Overview](#overview)

## Overview {#overview}

This service handles connections to the Supervisor or Supervisord process manager.

This service provides both a [web api][1] and a [command line api][2].

[1]: /documentation/services/supervisor/html/
[2]: /documentation/services/supervisor/cli/