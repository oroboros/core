<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\coreSupervisor\interfaces\adapter\supervisor;

/**
 * Supervisor Adapter Interface
 * Defines a set of methods for interacting with supervisor
 * 
 * @author Brian Dayhoff
 */
interface SupervisorAdapterInterface
extends \Oroboros\core\interfaces\adapter\AdapterInterface
{

    /**
     * Instantiates the supervisor adapter
     * 
     * The supervisor adapter will use connections set in the .env file
     * as its available connections. A default connection can be configured
     * to be used if no connection key is provided using the standard connection
     * definition criteria for Oroboros.
     * 
     * If no default connection is provided, a valid connection key must be
     * provided upon instantiation.
     * For security purposes, connections cannot be added at runtime.
     * 
     * The object will not instantiate if no connections are defined.
     * 
     * @param string $command (optional) The connection key to use.
     *     If not provided, the default connection will be used.
     * @param array $arguments (optional) Any arguments to pass into the adapter
     * @param array $flags (optional) Any flags to pass into the adapter
     * @throws \InvalidArgumentException If the given connection key does not exist.
     * @throws \ErrorException If a connection to supervisor could not be made.
     * @throws \RuntimeException If the specified default connection is defined,
     *     but the corresponding connection is not provided in the .env.
     */
    public function __construct(string $command = null, array $arguments = null, array $flags = null);

    /**
     * Returns an object representation of a given process
     * @param string $process
     * @return \Supervisor\Process
     * @throws \InvalidArgumentException If the given process name does not exist
     */
    public function process(string $process): \Supervisor\Process;

    /**
     * Returns a boolean determination as to whether a given process exists
     * @param string $process
     * @return bool
     */
    public function hasProcess(string $process): bool;

    /**
     * Returns an indexed array of valid process names
     * @return array
     */
    public function list(): array;

    /**
     * Returns a container of payload details for a given process
     * @param string $process
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     * @throws \InvalidArgumentException If the given process name does not exist
     */
    public function payload(string $process): \Oroboros\core\interfaces\library\container\ContainerInterface;

    /**
     * Starts a given process
     * @param string $process
     * @return $this (method chainable)
     * @throws \InvalidArgumentException If the given process name does not exist
     */
    public function start(string $process, bool $wait = false): \Oroboros\core\interfaces\adapter\supervisor\SupervisorAdapterInterface;

    /**
     * Stops a given process
     * @param string $process
     * @return $this (method chainable)
     * @throws \InvalidArgumentException If the given process name does not exist
     */
    public function stop(string $process): \Oroboros\core\interfaces\adapter\supervisor\SupervisorAdapterInterface;

    /**
     * Returns a container of details outlining the status of a given process
     * @param string $process
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     * @throws \InvalidArgumentException If the given process name does not exist
     */
    public function status(string $process): \Oroboros\core\interfaces\library\container\ContainerInterface;

    /**
     * Returns a boolean determination as to whether
     * the supervisor connection is currently running
     * @return bool
     */
    public function isRunning(): bool;

    /**
     * Returns a boolean determination as to whether
     * a valid connection to supervisor was made
     * @return bool
     */
    public function isConnected(): bool;
}
