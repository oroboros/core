# Oroboros Beanstalk Service Documentation {#title}

- [Overview](#overview)

## Overview {#overview}

This service handles connections to beanstalk and beanstalkd.

This service provides both a [web api][1] and a [command line api][2].

[1]: /documentation/services/beanstalk/html/
[2]: /documentation/services/beanstalk/cli/