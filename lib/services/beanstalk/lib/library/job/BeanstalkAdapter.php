<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\coreBeanstalk\library\job;

/**
 * Beanstalk Adapter
 * Provides a standardized job queue adapter to interact with beanstalkd
 *
 * @author Brian Dayhoff
 */
final class BeanstalkAdapter extends \Oroboros\core\abstracts\library\job\AbstractJobQueueAdapter implements \Oroboros\core\interfaces\CoreInterface
{

    use \Oroboros\core\traits\pluggable\ServiceLibraryTrait;

    const SERVICE_NAME = 'beanstalk';
    const CLASS_TYPE = 'library';
    const CLASS_SCOPE = 'beanstalk';
    const QUEUE_CONTAINER = 'Oroboros\\coreBeanstalk\\library\\job\\BeanstalkQueue';
    const QUEUE_INDEX = 'Oroboros\\coreBeanstalk\\library\\job\\BeanstalkQueueIndex';
    const RESERVE_TIMEOUT = 10;

    private static $default_credentials = [
        'host' => '127.0.0.1',
        'port' => 11300,
        'timeout' => self::RESERVE_TIMEOUT,
    ];
    private static $default_options = [
        'pause-delay' => 86400
    ];
    private $connector = null;
    private $scope = null;
    private $current_job = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        if (is_null($command)) {
            $command = 'default';
        }
        $arguments = $this->checkEnvironmentConnection($command, $arguments);
        parent::__construct($command, $arguments, $flags);
        $this->initializeConnector();
    }

    public function getQueueIndex(): \Oroboros\core\interfaces\library\job\JobQueueIndexInterface
    {
        $queue_container = static::QUEUE_CONTAINER;
        $queue_index = static::QUEUE_INDEX;
        $index = [];
        foreach ($this->listQueues() as $queue) {
            $index['queue'] = $queue_container::init();
        }
        return $queue_index::init(null, $index);
    }

    public function getJob(string $queue): ?array
    {
        if (!$this->hasQueue($queue)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Provided queue [%2$s] does not exist.', get_class($this), $queue));
        }
        if ($this->scope !== $queue) {
            $this->scopeQueue($queue);
        }
        if (!is_null($this->current_job)) {
            // Do not re-scope the current job if the last job 
            // has not properly been marked as resolved/failed/killed/etc.
            return json_decode($this->current_job->getData(), true);
        }
        // If no jobs are reserved within the timeout, 
        // return null indicating that no jobs could be fetched.
        $this->current_job = $this->connector->reserveWithTimeout(static::RESERVE_TIMEOUT);
        if (is_null($this->current_job)) {
            return null;
        }
        return [
            'id' => $this->current_job->getId(),
            'data' => json_decode($this->current_job->getData(), true)
        ];
    }

    /**
     * Queues a job, and returns the resulting id and data
     * registered into the job queue for later identification.
     * @param string $queue
     * @param array $details
     * @return array The return array will be an associative array with
     *     key `id` corresponding to the job id,
     *     and `data` corresponding to the passed data.
     * @throws \InvalidArgumentException If the provided queue does not exist
     */
    public function queueJob(string $queue, array $details): array
    {
        if (!$this->hasQueue($queue)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Provided queue [%2$s] does not exist.', get_class($this), $queue));
        }
        if ($this->scope !== $queue) {
            $this->scopeQueue($queue);
        }
        $data = json_encode([
            $details['action'],
            $details['data']
        ]);
        $priority = $details['priority'];
        $delay = $details['delay'];
        $ttr = $details['ttr'];
        $job = $this->connector->put($data, $priority, $delay, $ttr);
        return [
            'id' => $job->getId(),
            'data' => $job->getData()
        ];
    }

    public function failJob(): \Oroboros\core\interfaces\library\job\JobQueueAdapterInterface
    {
        if (is_null($this->current_job)) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. No job is currently scoped.'));
        }
        $this->connector->bury($this->current_job);
        $this->current_job = null;
        return $this;
    }

    public function suspendJob(): \Oroboros\core\interfaces\library\job\JobQueueAdapterInterface
    {
        return $this->failJob();
    }

    public function deleteJob(): \Oroboros\core\interfaces\library\job\JobQueueAdapterInterface
    {
        if (is_null($this->current_job)) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. No job is currently scoped.'));
        }
        $this->connector->delete($this->current_job);
        $this->current_job = null;
        return $this;
    }

    public function hasQueue(string $key): bool
    {
        $queues = $this->listQueues();
        return in_array($key, $queues);
    }

    public function scopeQueue(string $key): \Oroboros\core\interfaces\library\job\JobQueueAdapterInterface
    {
        if (!$this->hasQueue($key)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Provided queue [%2$s] does not exist.', get_class($this), $key));
        }
        $this->connector->watch($key);
        $this->connector->useTube($key);
        $this->scope = $key;
        return $this;
    }

    public function unscopeQueue(): \Oroboros\core\interfaces\library\job\JobQueueAdapterInterface
    {
        $this->connector->ignore($this->scope);
        $this->scope = null;
        return $this;
    }

    public function pauseQueue(string $key, int $delay = null): \Oroboros\core\interfaces\library\job\JobQueueAdapterInterface
    {
        $previous = $this->scope;
        if (is_null($delay)) {
            $delay = self::$default_options['pause-delay'];
        }
        if (!$this->hasQueue($key)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Provided queue [%2$s] does not exist.', get_class($this), $key));
        }
        $this->scopeQueue($key);
        $this->connector->pauseTube($key, $delay);
        if (!is_null($previous)) {
            $this->scopeQueue($previous);
        }
        return $this;
    }

    public function resumeQueue(string $key): \Oroboros\core\interfaces\library\job\JobQueueAdapterInterface
    {
        $previous = $this->scope;
        if (!$this->hasQueue($key)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Provided queue [%2$s] does not exist.', get_class($this), $key));
        }
        $this->scopeQueue($key);
        $this->connector->resumeTube($key);
        if (!is_null($previous)) {
            $this->scopeQueue($previous);
        }
        return $this;
    }

    public function listQueues(): array
    {
        $queues = $this->connector->listTubes();
        return $queues;
    }

    public function queueInfo(string $key)
    {
        if (!$this->hasQueue($key)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Provided queue [%2$s] does not exist.', get_class($this), $key));
        }
        return $this->connector->stats();
    }

    public function addQueue(string $key): \Oroboros\core\interfaces\library\job\JobQueueAdapterInterface
    {
        return $this;
    }

    public function removeQueue(string $key): \Oroboros\core\interfaces\library\job\JobQueueAdapterInterface
    {

        return $this;
    }

    private function getHostAddress(): string
    {
        if (!$this->hasArgument('host')) {
            return self::$default_credentials['host'];
        }
        return (string) $this->getArgument('host');
    }

    private function getHostPort(): int
    {
        if (!$this->hasArgument('port')) {
            return self::$default_credentials['port'];
        }
        return (int) $this->getArgument('port');
    }

    private function getHostTimeout(): int
    {
        if (!$this->hasArgument('timeout')) {
            return self::$default_credentials['timeout'];
        }
        return (int) $this->getArgument('timeout');
    }

    private function checkEnvironmentConnection(string $key, array $args = null): array
    {
        if (is_null($args)) {
            $args = [];
        }
        if (!$this::app()->environment()->has('application') || $this::app()->environment()->get('application')->has('connections') || property_exists($this::app()->environment()->get('application')->get('connections'), 'beanstalkd')) {
            $args = array_replace_recursive(self::$default_credentials, $args);
        }
        $connections = $this::app()->environment()->get('application')->get('connections');
        if ($key === 'default') {
            if (property_exists($connections, 'defaults') && property_exists($connections['defaults'], 'beanstalkd')) {
                $key = $connections['defaults']['beanstalkd'];
            } else {
                $key = 'local';
            }
        }
        $beanstalk_connections = (array_key_exists('beanstalkd', $connections) ? $connections['beanstalkd'] : ['local' => self::$default_credentials]);
        if (array_key_exists($key, $beanstalk_connections)) {
            d($beanstalk_connections[$key]);
            $args = array_replace_recursive($beanstalk_connections[$key], $args);
        }
        return $args;
    }

    private function initializeConnector(): void
    {
        $connector = \Pheanstalk\Pheanstalk::create($this->getHostAddress(), $this->getHostPort(), $this->getHostTimeout());
        $this->connector = $connector;
    }
}
