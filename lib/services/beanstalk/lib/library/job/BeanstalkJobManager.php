<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\coreBeanstalk\library\job;

/**
 * Beanstalk Job Queue Manager
 * Manages job queues and workers for beanstalkd job queues
 *
 * @author Brian Dayhoff
 */
final class BeanstalkJobManager extends \Oroboros\core\abstracts\library\job\AbstractJobManager implements \Oroboros\core\interfaces\CoreInterface
{

    use \Oroboros\core\traits\pluggable\ServiceLibraryTrait;

    const SERVICE_NAME = 'beanstalk';
    const ADAPTER = 'job\\BeanstalkAdapter';
    const CONTAINER_CLASS = 'Oroboros\\coreBeanstalk\\library\\job\\BeanstalkQueue';
    const COLLECTION_CLASS = 'Oroboros\\coreBeanstalk\\library\\job\\BeanstalkQueueIndex';
    const JOB_TYPE = 'beanstalk';
    const WORKER_CLASS = 'job\\BeanstalkWorker';

    protected function initializeManager(): void
    {
        foreach ($this->getAdapter()->listQueues() as $queue) {
            $this->setQueue($queue);
        }
    }

    /**
     * Verifies that the job details match the specification for a valid job of this type
     * @param array $details
     * @return void
     * @throws \InvalidArgumentException If the provided details are not acceptable
     */
    function verifyJobDetails(array $details): void
    {
        if (!array_key_exists('action', $details)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('No action key provided in job details'));
        }
        if (!array_key_exists('data', $details)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('No data key provided in job details'));
        }
        if (!array_key_exists('delay', $details)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('No delay key provided in job details'));
        }
        if (!array_key_exists('priority', $details)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('No priority key provided in job details'));
        }
        if (!array_key_exists('ttr', $details)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('No ttr key provided in job details'));
        }
    }
}
