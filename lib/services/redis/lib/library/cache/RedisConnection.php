<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\coreRedis\library\cache;

/**
 * Redis Connection
 * Represents a connection to a redis cache
 *
 * @author Brian Dayhoff
 */
final class RedisConnection extends \Oroboros\core\abstracts\library\cache\AbstractConnection implements \Oroboros\core\interfaces\CoreInterface
{

    use \Oroboros\core\traits\pluggable\ServiceLibraryTrait;

    const SERVICE_NAME = 'redis';

    /**
     * All other method calls pass through to the Predis client.
     * @param string $name Method name called
     * @param array $arguments any supplied arguments
     * @return mixed
     */
    public function __call(string $name, array $arguments)
    {
        return call_user_func_array([$this->getConnection(), $name], $arguments);
    }

    public function connect()
    {
        $this->getConnection()->connect();
        return $this;
    }

    public function disconnect()
    {
        $this->getConnection()->disconnect();
        return $this;
    }

    public function getClient(): \Predis\Client
    {
        return $this->getConnection();
    }

    protected function loadConnection(string $key = null)
    {
        $credentials = $this::app()->environment()->get('application')->get('connections')['redis'][$key];
        return new \Predis\Client($credentials);
    }
}
