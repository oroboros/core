# Oroboros Redis Service Documentation {#title}

- [Overview](#overview)

## Overview {#overview}

This service handles connections to Redis.

This service provides both a [web api][1] and a [command line api][2].

[1]: /documentation/services/redis/html/
[2]: /documentation/services/redis/cli/