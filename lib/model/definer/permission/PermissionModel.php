<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\model\definer\permission;

/**
 * Permission Model
 * Provides a model for interacting with the permission definer
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
final class PermissionModel extends \Oroboros\core\abstracts\model\AbstractDefinerModel implements \Oroboros\core\interfaces\CoreInterface
{

    use \Oroboros\core\traits\model\permission\PermissionModelTrait;
    
    /**
     * Sets the default container returned to the permission container
     */
    const CONTAINER_CLASS = \Oroboros\core\library\auth\PermissionContainer::class;

    /**
     * Scopes the model to definers
     */
    const MODEL_TYPE = 'definer';

    /**
     * Scopes the model to users
     */
    const MODEL_SCOPE = 'permission';

    /**
     * Scope the model to the permission definer
     */
    const DEFINER_CLASS = \Oroboros\core\library\auth\PermissionDefiner::class;
    
    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->verifyPermissionModel();
        $this->initializePermissionModel();
    }

}
