<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\library\layout\frontend;

/**
 * Default Frontend Error Page Layout
 * This is the default layout for public facing error pages.
 *
 * @author Brian Dayhoff
 */
final class ErrorLayout extends \Oroboros\core\abstracts\library\layout\AbstractLayout implements \Oroboros\core\interfaces\CoreInterface
{

    /**
     * Default layout sections
     * @var array
     */
    private $layout_sections = [
        'header' => [
            'component' => 'header',
            'attributes' => [
                'id' => 'error-header',
                'class' => 'd-block order-first sticky-top'
            ]
        ],
        'main' => [
            'component' => 'error-content-wrapper',
            'attributes' => [
                'id' => 'error-content-wrapper',
                'class' => 'd-flex row d-block m-0 p-0'
            ]
        ],
        'footer' => [
            'component' => 'footer',
            'attributes' => [
                'id' => 'page-footer',
                'class' => 'd-flex order-last mx-0 px-0 w-100 mt-auto'
            ]
        ],
    ];

    protected function declareLayoutBase(): string
    {
        return 'html/layout/frontend/error';
    }

    protected function declareSections(): array
    {
        return $this->layout_sections;
    }
}
