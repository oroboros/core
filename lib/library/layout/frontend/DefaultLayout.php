<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\library\layout\frontend;

/**
 * Default Layout
 * This is the default layout if no other layout is defined by the controller.
 *
 * @author Brian Dayhoff
 */
final class DefaultLayout extends \Oroboros\core\abstracts\library\layout\AbstractLayout implements \Oroboros\core\interfaces\CoreInterface
{

    /**
     * Default layout sections
     * @var array
     */
    private $layout_sections = [
        'header' => [
            'component' => 'header',
            'attributes' => [
                'id' => 'page-header',
                'class' => 'col-12 order-first sticky-top'
            ]
        ],
        'main' => [
            'component' => 'content',
            'attributes' => [
                'id' => 'page-content',
                'class' => 'col-12 col-lg-6 order-1 order-lg-2'
            ]
        ],
        'footer' => [
            'component' => 'footer',
            'attributes' => [
                'id' => 'page-footer',
                'class' => 'col-12 order-last mt-auto'
            ]
        ],
    ];

    /**
     * Layout sections only enabled when debug mode is active
     * @var array
     */
    private $layout_debug_sections = [
        'debug' => [
            'component' => self::DEBUG_COMPONENT,
            'attributes' => [
                'id' => self::DEBUG_COMPONENT,
                'class' => 'd-flex row d-block m-0 p-0'
            ],
        ],
        'error' => [
            'component' => self::ERROR_COMPONENT,
            'attributes' => [
                'id' => self::ERROR_COMPONENT,
                'class' => 'd-flex row d-block m-0 p-0'
            ],
        ]
    ];

    protected function declareLayoutBase(): string
    {
        return 'html/layout/frontend/default';
    }

    protected function declareSections(): array
    {
        if ($this::app()->debugEnabled()) {
            return array_merge_recursive($this->layout_sections, $this->layout_debug_sections);
        }
        return $this->layout_sections;
    }
}
