<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\library\layout\frontend;

/**
 * Default Three Column Layout
 * This layout provides a left and right sidebar with a centralized content section.
 *
 * @author Brian Dayhoff
 */
final class ThreeColumnLayout extends \Oroboros\core\abstracts\library\layout\AbstractLayout implements \Oroboros\core\interfaces\CoreInterface
{

    /**
     * Default layout sections
     * @var array
     */
    private $layout_sections = [
        'header' => [
            'component' => 'header',
            'attributes' => [
                'id' => 'page-header',
                'class' => 'col-12 order-first sticky-top'
            ]
        ],
        'main' => [
            'component' => 'content',
            'attributes' => [
                'id' => 'page-content',
//                'class' => 'col-12 col-lg-6 order-1 order-lg-2'
            ]
        ],
        'left-sidebar' => [
            'component' => 'sidebar',
            'attributes' => [
                'id' => 'sidebar-left',
//                'class' => 'col-12 col-lg-3 order-2 order-lg-1 mx-0 px-0'
            ]
        ],
        'right-sidebar' => [
            'component' => 'sidebar',
            'attributes' => [
                'id' => 'sidebar-right',
//                'class' => 'col-12 col-lg-3 order-3 mx-0 px-0'
            ]
        ],
        'footer' => [
            'component' => 'footer',
            'attributes' => [
                'id' => 'page-footer',
                'class' => 'col-12 order-last mt-auto'
            ]
        ],
    ];

    protected function declareLayoutBase(): string
    {
        return 'html/layout/frontend/three-columns';
    }

    protected function declareSections(): array
    {
        return $this->layout_sections;
    }
}
