<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\library\layout\dashboard;

/**
 * Default Dashboard Layout
 * This is the default layout for login dashboards if no other layout is defined by the controller.
 *
 * @author Brian Dayhoff
 */
final class DefaultLayout extends \Oroboros\core\abstracts\library\layout\AbstractLayout implements \Oroboros\core\interfaces\CoreInterface
{

    /**
     * Default layout sections
     * @var array
     */
    private $layout_sections = [
        'header' => [
            'component' => 'dashboard-header',
            'attributes' => [
                'id' => 'dashboard-header',
                'class' => 'd-flex order-first sticky-top'
            ]
        ],
        'heading' => [
            'component' => 'dashboard-heading',
            'attributes' => [
                'id' => 'dashboard-heading',
                'class' => 'd-sm-flex align-items-center justify-content-between'
            ]
        ],
        'sidebar' => [
            'component' => 'dashboard-sidebar',
            'attributes' => [
                'id' => 'dashboard-sidebar',
                'class' => 'h-100 mx-0 px-0'
            ]
        ],
        'main' => [
            'component' => 'dashboard-content',
            'attributes' => [
                'id' => 'dashboard-content',
                'class' => 'd-flex mx-0 px-0'
            ]
        ],
        'footer' => [
            'component' => 'dashboard-footer',
            'attributes' => [
                'id' => 'dashboard-footer',
                'class' => 'd-flex order-last mx-0 px-0 w-100 fixed-bottom'
            ]
        ],
        'ui' => [
            'component' => 'dashboard-ui',
            'attributes' => [
                'id' => 'dashboard-ui',
                'class' => ''
            ]
        ]
    ];

    public function render(): string
    {
        return parent::render();
    }

    protected function declareLayoutBase(): string
    {
        return 'html/layout/dashboard/default';
    }

    protected function declareSections(): array
    {
        return $this->layout_sections;
    }

    protected function getDefaultStyles(): array
    {
        return array_merge_recursive(parent::getDefaultStyles(), [
            'dashboard'
        ]);
    }

    protected function getDefaultScripts(): array
    {
        return array_merge_recursive(parent::getDefaultScripts(), [
            'footer' => ['dashboard']
        ]);
    }
}
