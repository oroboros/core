<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\library\user;

/**
 * Default Blacklist User Identity
 * Represents a set of identity details for the blacklisted users and ip addresses.
 * 
 * Anyone with this identity will be blocked from all site functionality.
 * 
 * @todo Build operations that generate webserver level blacklist files will read
 *       the ip addresses mapped to this identity, so that those ip's stop hitting
 *       the app code entirely as well on each subsequent build operation.
 * 
 * This object does not function as a current user object,
 * but can be used to supply account information to one.
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
final class BlacklistUserIdentity extends \Oroboros\core\abstracts\library\user\AbstractUserIdentity implements \Oroboros\core\interfaces\CoreInterface
{

    /**
     * Defines the default username as blacklist
     */
    const DEFAULT_USERNAME = 'blacklist';

    /**
     * Returns the identity id
     * Always returns the default
     * 
     * @return string
     */
    public function get(): string
    {
        return static::DEFAULT_USERNAME;
    }
    
    /**
     * Nope.
     * 
     * @param string $permission Doesn't matter, always returns `false`.
     * @return bool
     */
    public function can(string $permission): bool
    {
        return false;
    }
    
    /**
     * Blacklist users always authenticate.
     * 
     * @param \Oroboros\core\interfaces\library\auth\CredentialContainerInterface $credentials
     * @return bool
     */
    public function authenticate(\Oroboros\core\interfaces\library\auth\CredentialContainerInterface $credentials): bool
    {
        return true;
    }
}
