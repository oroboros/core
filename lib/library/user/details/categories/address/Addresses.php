<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\library\user\details\categories\address;

/**
 * Addresses
 * Represents a mailing or physical address
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
final class Addresses extends \Oroboros\core\abstracts\library\user\AbstractUserDetailsCategory implements \Oroboros\core\interfaces\CoreInterface
{

    /**
     * Scope keyname to address
     */
    const ENTITY_DETAIL_DEFAULT_KEY = 'address';

    /**
     * Default value null
     */
    const ENTITY_DETAIL_DEFAULT_VALUE = null;
    
    protected function initializeEntity(): void
    {
        parent::initializeEntity();
        $this->initializeUserPrimaryAddress();
        $this->initializeUserHomeAddress();
        $this->initializeUserWorkAddress();
        $this->initializeUserAddresses();
        $this->initializeUserAddressBook();
    }

    protected function evaluateEntity(\Oroboros\core\interfaces\library\entity\EntityInterface $entity): void
    {
        parent::evaluateEntity($entity);
        $this->setUserPrimaryAddress($entity);
        $this->setUserHomeAddress($entity);
        $this->setUserWorkAddress($entity);
        $this->setUserAddresses($entity);
        $this->setUserAddressBook($entity);
    }

    protected function resetEntity(): void
    {
        parent::resetEntity();
        $this->resetUserPrimaryAddress();
        $this->resetUserHomeAddress();
        $this->resetUserWorkAddress();
        $this->resetUserAddresses();
        $this->resetUserAddressBook();
    }

    protected function setUserWorkAddress(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\address\WorkAddress::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserPrimaryAddress(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\address\PrimaryAddress::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserAddressBook(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\address\AddressBook::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserAddresses(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\address\Addresses::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserHomeAddress(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\address\HomeAddress::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function resetUserWorkAddress(): void
    {
        $this->setUserWorkAddress();
    }

    protected function resetUserPrimaryAddress(): void
    {
        $this->setUserPrimaryAddress();
    }

    protected function resetUserAddressBook(): void
    {
        $this->setUserAddressBook();
    }

    protected function resetUserAddresses(): void
    {
        $this->setUserAddresses();
    }

    protected function resetUserHomeAddress(): void
    {
        $this->setUserHomeAddress();
    }

    protected function initializeUserPrimaryAddress(): void
    {
        $this->resetUserPrimaryAddress();
    }

    protected function initializeUserAddressBook(): void
    {
        $this->resetUserAddressBook();
    }

    protected function initializeUserWorkAddress(): void
    {
        $this->resetUserWorkAddress();
    }

    protected function initializeUserAddresses(): void
    {
        $this->resetUserAddresses();
    }

    protected function initializeUserHomeAddress(): void
    {
        $this->resetUserHomeAddress();
    }

}
