<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\library\user\details\categories\identity;

/**
 * User Identity Details
 * User identity detail category for general identity details that uniquely identify the account
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
final class UserIdentity extends \Oroboros\core\abstracts\library\user\AbstractUserDetailsCategory implements \Oroboros\core\interfaces\CoreInterface
{

    /**
     * Scope keyname to identity
     */
    const ENTITY_DETAIL_DEFAULT_KEY = 'identity';

    /**
     * Default value null
     */
    const ENTITY_DETAIL_DEFAULT_VALUE = null;

    protected function initializeEntity(): void
    {
        parent::initializeEntity();
        $this->initializeUserBirthdate();
        $this->initializeUserCreated();
        $this->initializeUserDevice();
        $this->initializeUserDisplayName();
        $this->initializeUserEmail();
        $this->initializeUserPhoneNumber();
        $this->initializeUserSession();
        $this->initializeUserUsername();
    }

    protected function evaluateEntity(\Oroboros\core\interfaces\library\entity\EntityInterface $entity): void
    {
        parent::evaluateEntity($entity);
        $this->setUserBirthdate($entity);
        $this->setUserCreated($entity);
        $this->setUserDevice($entity);
        $this->setUserDisplayName($entity);
        $this->setUserEmail($entity);
        $this->setUserPhoneNumber($entity);
        $this->setUserSession($entity);
        $this->setUserUsername($entity);
    }

    protected function resetEntity(): void
    {
        parent::resetEntity();
        $this->resetUserBirthdate();
        $this->resetUserCreated();
        $this->resetUserDevice();
        $this->resetUserDisplayName();
        $this->resetUserEmail();
        $this->resetUserPhoneNumber();
        $this->resetUserSession();
        $this->resetUserUsername();
    }

    protected function setUserBirthdate(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\identity\Birthdate::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserCreated(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\identity\Created::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserDevice(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\identity\Device::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserDisplayName(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\identity\DisplayName::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserEmail(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\identity\Email::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserPhoneNumber(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\identity\PhoneNumber::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserSession(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\identity\Session::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserUsername(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\identity\Username::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function resetUserBirthdate(): void
    {
        $this->setUserBirthdate();
    }

    protected function resetUserCreated(): void
    {
        $this->setUserCreated();
    }

    protected function resetUserDevice(): void
    {
        $this->setUserDevice();
    }

    protected function resetUserDisplayName(): void
    {
        $this->setUserDisplayName();
    }

    protected function resetUserEmail(): void
    {
        $this->setUserEmail();
    }

    protected function resetUserPhoneNumber(): void
    {
        $this->setUserPhoneNumber();
    }

    protected function resetUserSession(): void
    {
        $this->setUserSession();
    }

    protected function resetUserUsername(): void
    {
        $this->setUserUsername();
    }

    protected function initializeUserBirthdate(): void
    {
        $this->resetUserBirthdate();
    }

    protected function initializeUserCreated(): void
    {
        $this->resetUserCreated();
    }

    protected function initializeUserDevice(): void
    {
        $this->resetUserDevice();
    }

    protected function initializeUserDisplayName(): void
    {
        $this->resetUserDisplayName();
    }

    protected function initializeUserEmail(): void
    {
        $this->resetUserEmail();
    }

    protected function initializeUserPhoneNumber(): void
    {
        $this->resetUserPhoneNumber();
    }

    protected function initializeUserSession(): void
    {
        $this->resetUserSession();
    }

    protected function initializeUserUsername(): void
    {
        $this->resetUserUsername();
    }
}
