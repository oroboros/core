<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\library\user\details\categories\profile;

/**
 * User Profile
 * User identity detail category for user display profile details
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
final class UserProfile extends \Oroboros\core\abstracts\library\user\AbstractUserDetailsCategory implements \Oroboros\core\interfaces\CoreInterface
{

    /**
     * Scope keyname to occupation
     */
    const ENTITY_DETAIL_DEFAULT_KEY = 'profile';

    /**
     * Default value null
     */
    const ENTITY_DETAIL_DEFAULT_VALUE = null;

    protected function initializeEntity(): void
    {
        parent::initializeEntity();
        $this->initializeUserActive();
        $this->initializeUserAvatar();
        $this->initializeUserBadge();
        $this->initializeUserStatus();
        $this->initializeUserLastSeen();
    }

    protected function evaluateEntity(\Oroboros\core\interfaces\library\entity\EntityInterface $entity): void
    {
        parent::evaluateEntity($entity);
        $this->setUserActive($entity);
        $this->setUserAvatar($entity);
        $this->setUserBadge($entity);
        $this->setUserStatus($entity);
        $this->setUserLastSeen($entity);
    }

    protected function resetEntity(): void
    {
        parent::resetEntity();
        $this->resetUserActive();
        $this->resetUserAvatar();
        $this->resetUserBadge();
        $this->resetUserStatus();
        $this->resetUserLastSeen();
    }

    protected function setUserActive(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\profile\Active::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserAvatar(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\profile\Avatar::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserBadge(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\profile\Badge::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserStatus(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\profile\Status::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserLastSeen(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\profile\LastSeen::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function resetUserActive(): void
    {
        $this->setUserActive();
    }

    protected function resetUserAvatar(): void
    {
        $this->setUserAvatar();
    }

    protected function resetUserBadge(): void
    {
        $this->setUserBadge();
    }

    protected function resetUserStatus(): void
    {
        $this->setUserStatus();
    }

    protected function resetUserLastSeen(): void
    {
        $this->setUserLastSeen();
    }

    protected function initializeUserActive(): void
    {
        $this->resetUserActive();
    }

    protected function initializeUserAvatar(): void
    {
        $this->resetUserAvatar();
    }

    protected function initializeUserBadge(): void
    {
        $this->resetUserBadge();
    }

    protected function initializeUserStatus(): void
    {
        $this->resetUserStatus();
    }

    protected function initializeUserLastSeen(): void
    {
        $this->resetUserLastSeen();
    }
}
