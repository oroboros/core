<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\library\user\details\categories\relationships;

/**
 * User Relationship Details
 * User identity detail category for relationships with other entities
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
final class UserRelationshipDetails extends \Oroboros\core\abstracts\library\user\AbstractUserDetailsCategory implements \Oroboros\core\interfaces\CoreInterface
{

    /**
     * Scope keyname to occupation
     */
    const ENTITY_DETAIL_DEFAULT_KEY = 'relationships';

    /**
     * Default value null
     */
    const ENTITY_DETAIL_DEFAULT_VALUE = null;

    protected function initializeEntity(): void
    {
        parent::initializeEntity();
        $this->initializeUserAffiliations();
        $this->initializeUserContacts();
        $this->initializeUserEmployees();
        $this->initializeUserEmployers();
        $this->initializeUserGroups();
        $this->initializeUserPeers();
        $this->initializeUserSupervisors();
        $this->initializeUserTags();
    }

    protected function evaluateEntity(\Oroboros\core\interfaces\library\entity\EntityInterface $entity): void
    {
        parent::evaluateEntity($entity);
        $this->setUserAffiliations($entity);
        $this->setUserContacts($entity);
        $this->setUserEmployees($entity);
        $this->setUserEmployers($entity);
        $this->setUserGroups($entity);
        $this->setUserPeers($entity);
        $this->setUserSupervisors($entity);
        $this->setUserTags($entity);
    }

    protected function resetEntity(): void
    {
        parent::resetEntity();
        $this->resetUserAffiliations();
        $this->resetUserContacts();
        $this->resetUserEmployees();
        $this->resetUserEmployers();
        $this->resetUserGroups();
        $this->resetUserPeers();
        $this->resetUserSupervisors();
        $this->resetUserTags();
    }

    protected function setUserPeers(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\relationships\Peers::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserSupervisors(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\relationships\Supervisors::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserEmployees(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\relationships\Employees::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserTags(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\relationships\Tags::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserAffiliations(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\relationships\Affiliations::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserGroups(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\relationships\Groups::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserEmployers(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\relationships\Employers::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserContacts(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\relationships\Contacts::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function resetUserPeers(): void
    {
        $this->setUserPeers();
    }

    protected function resetUserEmployees(): void
    {
        $this->setUserEmployees();
    }

    protected function resetUserTags(): void
    {
        $this->setUserTags();
    }

    protected function resetUserAffiliations(): void
    {
        $this->setUserAffiliations();
    }

    protected function resetUserGroups(): void
    {
        $this->setUserGroups();
    }

    protected function resetUserSupervisors(): void
    {
        $this->setUserSupervisors();
    }

    protected function resetUserEmployers(): void
    {
        $this->setUserEmployers();
    }

    protected function resetUserContacts(): void
    {
        $this->setUserContacts();
    }

    protected function initializeUserAffiliations(): void
    {
        $this->resetUserAffiliations();
    }

    protected function initializeUserGroups(): void
    {
        $this->resetUserGroups();
    }

    protected function initializeUserEmployees(): void
    {
        $this->resetUserEmployees();
    }

    protected function initializeUserTags(): void
    {
        $this->resetUserTags();
    }

    protected function initializeUserSupervisors(): void
    {
        $this->resetUserSupervisors();
    }

    protected function initializeUserPeers(): void
    {
        $this->resetUserPeers();
    }

    protected function initializeUserEmployers(): void
    {
        $this->resetUserEmployers();
    }

    protected function initializeUserContacts(): void
    {
        $this->resetUserContacts();
    }
}
