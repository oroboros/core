<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\library\user\details\categories\security;

/**
 * User Security Details
 * User identity detail category for security details
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
final class UserSecurityDetails extends \Oroboros\core\abstracts\library\user\AbstractUserDetailsCategory implements \Oroboros\core\interfaces\CoreInterface
{

    /**
     * Scope keyname to security
     */
    const ENTITY_DETAIL_DEFAULT_KEY = 'security';

    /**
     * Default value null
     */
    const ENTITY_DETAIL_DEFAULT_VALUE = null;

    protected function initializeEntity(): void
    {
        parent::initializeEntity();
        $this->initializeUserLocal();
        $this->initializeUserInternal();
        $this->initializeUserRoot();
        $this->initializeUserCore();
        $this->initializeUserSystem();
        $this->initializeUserTrusted();
        $this->initializeUserPublic();
        $this->initializeUserSecure();
        $this->initializeUserEnabled();
        $this->initializeUserLocked();
        $this->initializeUserHidden();
        $this->initializeUserVerified();
        $this->initializeUserMultifactor();
        $this->initializeUserRecovery();
        $this->initializeUserWhitelist();
        $this->initializeUserBlacklist();
        $this->initializeUserGhost();
        $this->initializeUserSuspicious();
    }

    protected function evaluateEntity(\Oroboros\core\interfaces\library\entity\EntityInterface $entity): void
    {
        parent::evaluateEntity($entity);
        $this->setUserLocal($entity);
        $this->setUserInternal($entity);
        $this->setUserRoot($entity);
        $this->setUserCore($entity);
        $this->setUserSystem($entity);
        $this->setUserTrusted($entity);
        $this->setUserPublic($entity);
        $this->setUserSecure($entity);
        $this->setUserEnabled($entity);
        $this->setUserLocked($entity);
        $this->setUserHidden($entity);
        $this->setUserVerified($entity);
        $this->setUserMultifactor($entity);
        $this->setUserRecovery($entity);
        $this->setUserWhitelist($entity);
        $this->setUserBlacklist($entity);
        $this->setUserGhost($entity);
        $this->setUserSuspicious($entity);
    }

    protected function resetEntity(): void
    {
        parent::resetEntity();
        $this->resetUserLocal();
        $this->resetUserInternal();
        $this->resetUserRoot();
        $this->resetUserCore();
        $this->resetUserSystem();
        $this->resetUserTrusted();
        $this->resetUserPublic();
        $this->resetUserSecure();
        $this->resetUserEnabled();
        $this->resetUserLocked();
        $this->resetUserHidden();
        $this->resetUserVerified();
        $this->resetUserMultifactor();
        $this->resetUserRecovery();
        $this->resetUserWhitelist();
        $this->resetUserBlacklist();
        $this->resetUserGhost();
        $this->resetUserSuspicious();
    }

    protected function setUserEnabled(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\security\Enabled::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserHidden(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\security\Hidden::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserLocked(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\security\Locked::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserSystem(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\security\System::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserMultifactor(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\security\MultiFactor::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserRecovery(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\security\Recovery::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserRoot(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\security\Root::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserCore(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\security\Core::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserLocal(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\security\LocalNetwork::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserInternal(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\security\InternalNetwork::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserTrusted(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\security\TrustedNetwork::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserPublic(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\security\PublicNetwork::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserSecure(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\security\SecureConnection::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserVerified(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\security\Verified::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserWhitelist(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\security\Whitelist::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserBlacklist(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\security\Blacklist::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserGhost(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\security\Ghost::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserSuspicious(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\security\Suspicious::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function resetUserEnabled(): void
    {
        $this->setUserEnabled();
    }

    protected function resetUserHidden(): void
    {
        $this->setUserHidden();
    }

    protected function resetUserLocked(): void
    {
        $this->setUserLocked();
    }

    protected function resetUserSystem(): void
    {
        $this->setUserSystem();
    }

    protected function resetUserMultifactor(): void
    {
        $this->setUserMultifactor();
    }

    protected function resetUserRecovery(): void
    {
        $this->setUserRecovery();
    }

    protected function resetUserRoot(): void
    {
        $this->setUserRoot();
    }

    protected function resetUserCore(): void
    {
        $this->setUserCore();
    }

    protected function resetUserLocal(): void
    {
        $this->setUserLocal();
    }

    protected function resetUserInternal(): void
    {
        $this->setUserInternal();
    }

    protected function resetUserTrusted(): void
    {
        $this->setUserTrusted();
    }

    protected function resetUserPublic(): void
    {
        $this->setUserPublic();
    }

    protected function resetUserSecure(): void
    {
        $this->setUserSecure();
    }

    protected function resetUserVerified(): void
    {
        $this->setUserVerified();
    }

    protected function resetUserWhitelist(): void
    {
        $this->setUserWhitelist();
    }

    protected function resetUserBlacklist(): void
    {
        $this->setUserBlacklist();
    }

    protected function resetUserGhost(): void
    {
        $this->setUserGhost();
    }

    protected function resetUserSuspicious(): void
    {
        $this->setUserSuspicious();
    }

    protected function initializeUserEnabled(): void
    {
        $this->resetUserEnabled();
    }

    protected function initializeUserHidden(): void
    {
        $this->resetUserHidden();
    }

    protected function initializeUserLocked(): void
    {
        $this->resetUserLocked();
    }

    protected function initializeUserSystem(): void
    {
        $this->resetUserSystem();
    }

    protected function initializeUserMultifactor(): void
    {
        $this->resetUserMultifactor();
    }

    protected function initializeUserRecovery(): void
    {
        $this->resetUserRecovery();
    }

    protected function initializeUserRoot(): void
    {
        $this->resetUserRoot();
    }

    protected function initializeUserCore(): void
    {
        $this->resetUserCore();
    }

    protected function initializeUserLocal(): void
    {
        $this->resetUserLocal();
    }

    protected function initializeUserInternal(): void
    {
        $this->resetUserInternal();
    }

    protected function initializeUserTrusted(): void
    {
        $this->resetUserTrusted();
    }

    protected function initializeUserPublic(): void
    {
        $this->resetUserPublic();
    }

    protected function initializeUserSecure(): void
    {
        $this->resetUserSecure();
    }

    protected function initializeUserVerified(): void
    {
        $this->resetUserVerified();
    }

    protected function initializeUserWhitelist(): void
    {
        $this->resetUserWhitelist();
    }

    protected function initializeUserBlacklist(): void
    {
        $this->resetUserBlacklist();
    }

    protected function initializeUserGhost(): void
    {
        $this->resetUserGhost();
    }

    protected function initializeUserSuspicious(): void
    {
        $this->resetUserSuspicious();
    }
}
