<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\library\user\details\categories\preferences;

/**
 * User Preferences
 * User identity detail category for preferences
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
final class UserPreferences extends \Oroboros\core\abstracts\library\user\AbstractUserDetailsCategory implements \Oroboros\core\interfaces\CoreInterface
{

    /**
     * Scope keyname to preferences
     */
    const ENTITY_DETAIL_DEFAULT_KEY = 'preferences';

    /**
     * Default value null
     */
    const ENTITY_DETAIL_DEFAULT_VALUE = null;

    protected function initializeEntity(): void
    {
        parent::initializeEntity();
        $this->initializeUserAccount();
        $this->initializeUserSettings();
        $this->initializeUserDisplay();
        $this->initializeUserProfile();
        $this->initializeUserContent();
        $this->initializeUserMessages();
        $this->initializeUserMedia();
        $this->initializeUserLegal();
        $this->initializeUserPrivacy();
        $this->initializeUserSecurity();
    }

    protected function evaluateEntity(\Oroboros\core\interfaces\library\entity\EntityInterface $entity): void
    {
        parent::evaluateEntity($entity);
        $this->setUserAccount($entity);
        $this->setUserSettings($entity);
        $this->setUserDisplay($entity);
        $this->setUserProfile($entity);
        $this->setUserContent($entity);
        $this->setUserMessages($entity);
        $this->setUserMedia($entity);
        $this->setUserLegal($entity);
        $this->setUserPrivacy($entity);
        $this->setUserSecurity($entity);
    }

    protected function resetEntity(): void
    {
        parent::resetEntity();
        $this->resetUserAccount();
        $this->resetUserSettings();
        $this->resetUserDisplay();
        $this->resetUserProfile();
        $this->resetUserContent();
        $this->resetUserMessages();
        $this->resetUserMedia();
        $this->resetUserLegal();
        $this->resetUserPrivacy();
        $this->resetUserSecurity();
    }

    protected function setUserMessages(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\preferences\Message::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserMedia(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\preferences\Media::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserDisplay(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\preferences\Display::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserLegal(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\preferences\Legal::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserAccount(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\preferences\Account::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserContent(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\preferences\Content::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserPrivacy(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\preferences\Privacy::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserProfile(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\preferences\Profile::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserSettings(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\preferences\Settings::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserSecurity(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\preferences\Security::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function resetUserMessages(): void
    {
        $this->setUserMessages();
    }

    protected function resetUserDisplay(): void
    {
        $this->setUserDisplay();
    }

    protected function resetUserLegal(): void
    {
        $this->setUserLegal();
    }

    protected function resetUserAccount(): void
    {
        $this->setUserAccount();
    }

    protected function resetUserContent(): void
    {
        $this->setUserContent();
    }

    protected function resetUserMedia(): void
    {
        $this->setUserMedia();
    }

    protected function resetUserPrivacy(): void
    {
        $this->setUserPrivacy();
    }

    protected function resetUserProfile(): void
    {
        $this->setUserProfile();
    }

    protected function resetUserSettings(): void
    {
        $this->setUserSettings();
    }

    protected function resetUserSecurity(): void
    {
        $this->setUserSecurity();
    }

    protected function initializeUserAccount(): void
    {
        $this->resetUserAccount();
    }

    protected function initializeUserContent(): void
    {
        $this->resetUserContent();
    }

    protected function initializeUserDisplay(): void
    {
        $this->resetUserDisplay();
    }

    protected function initializeUserLegal(): void
    {
        $this->resetUserLegal();
    }

    protected function initializeUserMedia(): void
    {
        $this->resetUserMedia();
    }

    protected function initializeUserMessages(): void
    {
        $this->resetUserMessages();
    }

    protected function initializeUserPrivacy(): void
    {
        $this->resetUserPrivacy();
    }

    protected function initializeUserProfile(): void
    {
        $this->resetUserProfile();
    }

    protected function initializeUserSettings(): void
    {
        $this->resetUserSettings();
    }

    protected function initializeUserSecurity(): void
    {
        $this->resetUserSecurity();
    }
}
