<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\library\user\details\categories\information;

/**
 * User Information
 * User identity detail category for account information related to the user identity
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
final class UserInformation extends \Oroboros\core\abstracts\library\user\AbstractUserDetailsCategory implements \Oroboros\core\interfaces\CoreInterface
{

    /**
     * Scope keyname to information
     */
    const ENTITY_DETAIL_DEFAULT_KEY = 'information';

    /**
     * Default value null
     */
    const ENTITY_DETAIL_DEFAULT_VALUE = null;

    protected function initializeEntity(): void
    {
        parent::initializeEntity();
        $this->initializeUserBiography();
        $this->initializeUserRole();
        $this->initializeUserOccupation();
        $this->initializeUserStatus();
        $this->initializeUserContactInfo();
    }

    protected function evaluateEntity(\Oroboros\core\interfaces\library\entity\EntityInterface $entity): void
    {
        parent::evaluateEntity($entity);
        $this->setUserBiography($entity);
        $this->setUserRole($entity);
        $this->setUserOccupation($entity);
        $this->setUserStatus($entity);
        $this->setUserContactInfo($entity);
    }

    protected function resetEntity(): void
    {
        parent::resetEntity();
        $this->resetUserBiography();
        $this->resetUserRole();
        $this->resetUserOccupation();
        $this->resetUserStatus();
        $this->resetUserContactInfo();
    }

    protected function setUserBiography(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\information\Biography::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserRole(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\information\Role::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserOccupation(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\information\Occupation::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserStatus(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\information\Status::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserContactInfo(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\information\ContactInfo::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function resetUserBiography(): void
    {
        $this->setUserBiography();
    }

    protected function resetUserRole(): void
    {
        $this->setUserRole();
    }

    protected function resetUserOccupation(): void
    {
        $this->setUserOccupation();
    }

    protected function resetUserStatus(): void
    {
        $this->setUserStatus();
    }

    protected function resetUserContactInfo(): void
    {
        $this->setUserContactInfo();
    }

    protected function initializeUserBiography(): void
    {
        $this->resetUserBiography();
    }

    protected function initializeUserRole(): void
    {
        $this->resetUserRole();
    }

    protected function initializeUserOccupation(): void
    {
        $this->resetUserOccupation();
    }

    protected function initializeUserStatus(): void
    {
        $this->resetUserStatus();
    }

    protected function initializeUserContactInfo(): void
    {
        $this->resetUserContactInfo();
    }
}
