<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\library\user\details\categories\notifications;

/**
 * User Notifications
 * User identity detail category for notifications
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
final class UserNotifications extends \Oroboros\core\abstracts\library\user\AbstractUserDetailsCategory implements \Oroboros\core\interfaces\CoreInterface
{

    /**
     * Scope keyname to occupation
     */
    const ENTITY_DETAIL_DEFAULT_KEY = 'notifications';

    /**
     * Default value null
     */
    const ENTITY_DETAIL_DEFAULT_VALUE = null;

    protected function initializeEntity(): void
    {
        parent::initializeEntity();
        $this->initializeUserActivity();
        $this->initializeUserEnvironment();
        $this->initializeUserSystem();
        $this->initializeUserApplication();
        $this->initializeUserCore();
        $this->initializeUserAnnouncements();
        $this->initializeUserAwards();
        $this->initializeUserBans();
        $this->initializeUserEmails();
        $this->initializeUserMessages();
        $this->initializeUserPenalties();
        $this->initializeUserPushNotifications();
        $this->initializeUserSecurity();
        $this->initializeUserSuspension();
    }

    protected function evaluateEntity(\Oroboros\core\interfaces\library\entity\EntityInterface $entity): void
    {
        parent::evaluateEntity($entity);
        $this->setUserActivity($entity);
        $this->setUserEnvironment($entity);
        $this->setUserSystem($entity);
        $this->setUserApplication($entity);
        $this->setUserCore($entity);
        $this->setUserAnnouncements($entity);
        $this->setUserAwards($entity);
        $this->setUserBans($entity);
        $this->setUserEmails($entity);
        $this->setUserMessages($entity);
        $this->setUserPenalties($entity);
        $this->setUserPushNotifications($entity);
        $this->setUserSecurity($entity);
        $this->setUserSuspension($entity);
    }

    protected function resetEntity(): void
    {
        parent::resetEntity();
        $this->resetUserActivity();
        $this->resetUserEnvironment();
        $this->resetUserSystem();
        $this->resetUserApplication();
        $this->resetUserCore();
        $this->resetUserAnnouncements();
        $this->resetUserAwards();
        $this->resetUserBans();
        $this->resetUserEmails();
        $this->resetUserMessages();
        $this->resetUserPenalties();
        $this->resetUserPushNotifications();
        $this->resetUserSecurity();
        $this->resetUserSuspension();
    }

    protected function setUserActivity(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\notifications\Activity::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserApplication(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\notifications\Application::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserSystem(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\notifications\System::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserEnvironment(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\notifications\Environment::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserCore(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\notifications\Core::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserAnnouncements(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\notifications\Announcements::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserAwards(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\notifications\Awards::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserBans(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\notifications\Bans::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserEmails(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\notifications\Emails::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserMessages(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\notifications\Messages::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserPenalties(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\notifications\Penalties::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserPushNotifications(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\notifications\Push::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserSecurity(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\notifications\Security::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserSuspension(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\notifications\Suspensions::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function resetUserActivity(): void
    {
        $this->setUserActivity();
    }

    protected function resetUserEnvironment(): void
    {
        $this->setUserEnvironment();
    }

    protected function resetUserApplication(): void
    {
        $this->setUserApplication();
    }

    protected function resetUserSystem(): void
    {
        $this->setUserSystem();
    }

    protected function resetUserCore(): void
    {
        $this->setUserCore();
    }

    protected function resetUserAnnouncements(): void
    {
        $this->setUserAnnouncements();
    }

    protected function resetUserAwards(): void
    {
        $this->setUserAwards();
    }

    protected function resetUserBans(): void
    {
        $this->setUserBans();
    }

    protected function resetUserEmails(): void
    {
        $this->setUserEmails();
    }

    protected function resetUserMessages(): void
    {
        $this->setUserMessages();
    }

    protected function resetUserPenalties(): void
    {
        $this->setUserPenalties();
    }

    protected function resetUserPushNotifications(): void
    {
        $this->setUserPushNotifications();
    }

    protected function resetUserSecurity(): void
    {
        $this->setUserSecurity();
    }

    protected function resetUserSuspension(): void
    {
        $this->setUserSuspension();
    }

    protected function initializeUserActivity(): void
    {
        $this->resetUserActivity();
    }

    protected function initializeUserEnvironment(): void
    {
        $this->resetUserEnvironment();
    }

    protected function initializeUserSystem(): void
    {
        $this->resetUserSystem();
    }

    protected function initializeUserApplication(): void
    {
        $this->resetUserApplication();
    }

    protected function initializeUserCore(): void
    {
        $this->resetUserCore();
    }

    protected function initializeUserAnnouncements(): void
    {
        $this->resetUserAnnouncements();
    }

    protected function initializeUserAwards(): void
    {
        $this->resetUserAwards();
    }

    protected function initializeUserBans(): void
    {
        $this->resetUserBans();
    }

    protected function initializeUserEmails(): void
    {
        $this->resetUserEmails();
    }

    protected function initializeUserMessages(): void
    {
        $this->resetUserMessages();
    }

    protected function initializeUserPenalties(): void
    {
        $this->resetUserPenalties();
    }

    protected function initializeUserPushNotifications(): void
    {
        $this->resetUserPushNotifications();
    }

    protected function initializeUserSecurity(): void
    {
        $this->resetUserSecurity();
    }

    protected function initializeUserSuspensions(): void
    {
        $this->resetUserSuspensions();
    }
}
