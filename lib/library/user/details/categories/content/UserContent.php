<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\library\user\details\categories\content;

/**
 * User Content Details
 * User identity detail category for content belonging to or submitted by the identity
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
final class UserContent extends \Oroboros\core\abstracts\library\user\AbstractUserDetailsCategory implements \Oroboros\core\interfaces\CoreInterface
{

    /**
     * Scope keyname to occupation
     */
    const ENTITY_DETAIL_DEFAULT_KEY = 'content';

    /**
     * Default value null
     */
    const ENTITY_DETAIL_DEFAULT_VALUE = null;

    protected function initializeEntity(): void
    {
        parent::initializeEntity();
        $this->initializeUserPosts();
        $this->initializeUserThreads();
        $this->initializeUserComments();
    }

    protected function evaluateEntity(\Oroboros\core\interfaces\library\entity\EntityInterface $entity): void
    {
        parent::evaluateEntity($entity);
        $this->setUserPosts($entity);
        $this->setUserThreads($entity);
        $this->setUserComments($entity);
    }

    protected function resetEntity(): void
    {
        parent::resetEntity();
        $this->resetUserPosts();
        $this->resetUserThreads();
        $this->resetUserComments();
    }

    protected function setUserPosts(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\content\Posts::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserThreads(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\content\Threads::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function setUserComments(\Oroboros\core\interfaces\library\entity\EntityInterface $entity = null): void
    {
        $class = \Oroboros\core\library\user\details\categories\content\Comments::class;
        $key = $class::ENTITY_DETAIL_DEFAULT_KEY;
        if (is_null($entity)) {
            $this[$key] = null;
            return;
        }
        $details = $this->load('library', $class, null, $this->getArguments(), $this->getFlags());
        $details->setLogger($this->getLogger());
        $details->setEntity($entity);
        $this[$key] = $details;
    }

    protected function resetUserPosts(): void
    {
        $this->setUserPosts();
    }

    protected function resetUserThreads(): void
    {
        $this->setUserThreads();
    }

    protected function resetUserComments(): void
    {
        $this->setUserComments();
    }

    protected function initializeUserPosts(): void
    {
        $this->resetUserPosts();
    }

    protected function initializeUserThreads(): void
    {
        $this->resetUserThreads();
    }

    protected function initializeUserComments(): void
    {
        $this->resetUserComments();
    }
}
