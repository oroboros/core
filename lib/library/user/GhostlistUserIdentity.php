<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\library\user;

/**
 * Default Ghostlist User Identity
 * Represents a set of identity details for the users and ip addresses flagged as ghosts.
 * 
 * Assigning this identity to any user will put the application into a
 * read-only state for them, prevent login, log them out if they are logged in,
 * and will silently discard all POST/PUT/DELETE requests from them while
 * returning the same output that would occur if the user's input did not
 * validate but was legitimate. All authentication tokens assigned to ghost
 * users always fail their auth and validation steps.
 * 
 * The system will make a concerted effort to bail early on all ghost user
 * requests, primarily serving only cached content and returning defaulted
 * fail mesages for forms, ajax, and rest requests to prevent requests
 * from ghosts having any particular impact on server load.
 * 
 * Ghost users are a variant blacklist strategy that allows requests to appear
 * to be legitimate without ever being allowed to touch data on the backend.
 * This can be used when attacks, spam, bruteforce attempts, or bots rotate ip
 * addresses that are blocked to continue an attack. This will circumvent
 * simple header status code detection using a blocking strategy that lies
 * about access rather than blocking it outright. This allows profiling
 * to be done silently on the attempt to analyze for developing a more
 * sophisticated defense strategy without risk of compromising actual
 * data or letting malicious requests through.
 * 
 * Anyone with this identity will be blocked from all site functionality.
 * 
 * This object does not function as a current user object,
 * but can be used to supply account information to one.
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
final class GhostlistUserIdentity extends \Oroboros\core\abstracts\library\user\AbstractUserIdentity implements \Oroboros\core\interfaces\CoreInterface
{

    /**
     * Defines the default username as ghost
     */
    const DEFAULT_USERNAME = 'ghost';

    /**
     * Returns the identity id
     * Always returns the default
     * 
     * @return string
     */
    public function get(): string
    {
        return static::DEFAULT_USERNAME;
    }
    
    /**
     * Ghost users always authenticate.
     * 
     * @param \Oroboros\core\interfaces\library\auth\CredentialContainerInterface $credentials
     * @return bool
     */
    public function authenticate(\Oroboros\core\interfaces\library\auth\CredentialContainerInterface $credentials): bool
    {
        return true;
    }
}
