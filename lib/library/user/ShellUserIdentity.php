<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\library\user;

/**
 * Default Shell User Identity
 * Represents a set of identity details for the bash command line api.
 * 
 * Calls received from bash will be assumed to have been made by this user
 * unless sudo is detected, in which case they will be interpreted as
 * received from root.
 * 
 * Calls to bash will assign this user, unless they transfer sudo,
 * in which case they will assign root.
 * 
 * Shell identity will defer directly to the system
 * user definition with no filtering as per the underlying operating system.
 * 
 * This object does not function as a current user object,
 * but can be used to supply account information to one.
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
final class ShellUserIdentity extends \Oroboros\core\abstracts\library\user\AbstractCliUserIdentity implements \Oroboros\core\interfaces\CoreInterface
{

    /**
     * Defines the default username as shell
     */
    const DEFAULT_USERNAME = 'shell';

}
