<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\library\log;

/**
 * Debug Logger
 * Psr-3 logger for development.
 * Logs to a Kint Dump string that is appended
 * when the destructor fires, so it does not disrupt page display.
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
final class DebugLogger extends \Oroboros\core\abstracts\library\log\AbstractLogger implements \Oroboros\core\interfaces\CoreInterface
{

    /**
     * Declare the logger scope as file
     */
    const LOGGER_SCOPE = 'debug';

    /**
     * The log of all debug dump information
     * @var string
     */
    private static $debug_log = '';

    /**
     * The number of instances of this object currently open.
     * The debug log will only be dumped when the final one is closed.
     * 
     * @var int
     */
    private static $instance_count = 0;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        self::$instance_count++;
    }

    /**
     * Prints the debug log at the end when the destructor fires.
     */
    public function __destruct()
    {
        self::$instance_count--;
        if (self::$instance_count === 0) {
            if (\Kint::$enabled_mode === false) {
                return;
            }
            if ($this::IS_CLI) {
                s(self::$debug_log);
                return;
            }
            d(self::$debug_log);
        }
    }

    protected function handleLogMessage(string $level, string $message, $context = []): void
    {
        parent::handleLogMessage($level, $message, $context);
        if (\Kint::$enabled_mode === false) {
            return;
        }
        self::$debug_log .= sprintf('[%1$s] %2$s', $level, $message) . PHP_EOL;
    }
}
