<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\library\log;

/**
 * Cli Logger
 * Psr-3 logger for cli. Logs to the command line.
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
final class CliLogger extends \Oroboros\core\abstracts\library\log\AbstractLogger implements \Oroboros\core\interfaces\CoreInterface
{

    /**
     * Declare the logger type as cli
     */
    const LOGGER_TYPE = 'cli';

    /**
     * Declare the logger scope as cli
     */
    const LOGGER_SCOPE = 'cli';
    
    /**
     * The CLI View object used to write to the cli console
     * 
     * @var \Oroboros\core\interfaces\view\CliViewInterface
     */
    private static $view = null;
    
    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeCliView();
    }
    
    /**
     * Logs messages directly to the console via the cli view object
     * 
     * @param string $level
     * @param string $message
     * @param type $context
     * @return void
     */
    protected function handleLogMessage(string $level, string $message, $context = []): void
    {
        parent::handleLogMessage($level, $message, $context);
        if (!is_null(self::$view)) {
            // Don't log before the view is set
            self::$view->$level($message, $context);
        }
    }
    
    /**
     * Initializes the cli view that will be used
     * to print messages to the terminal
     * 
     * @return void
     */
    private function initializeCliView(): void
    {
        if (is_null(self::$view)) {
            $view = $this->load('view', \Oroboros\core\view\cli\CliView::class, null, $this->getArguments(), $this->getFlags());
            self::$view = $view;
        }
    }

}
