<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\library\session;

/**
 * Default Session Token
 * Default session token implementation
 * 
 * This is the class that generates the default session id,
 * which will be the value of the session cookie, env var,
 * or value in a bearer token that designates the
 * authentication matchup for the uniquely
 * identifiable session.
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
final class SessionToken extends \Oroboros\core\abstracts\library\auth\AbstractToken implements \Oroboros\core\interfaces\library\session\SessionCommonInterface, \Oroboros\core\interfaces\CoreInterface
{

    use \Oroboros\core\traits\library\session\SessionCommonTrait;

    const CLASS_SCOPE = 'session';
    
    /**
     * Designates that this class session type is general purpose sessions
     */
    const SESSION_TYPE = self::CLASS_SCOPE;
    
    /**
     * Designates that this object fulfills the token scope
     */
    const SESSION_SCOPE = 'token';

    /**
     * Designates that this class is part of the session auth group
     */
    const AUTH_SCOPE = self::CLASS_SCOPE;
    
    /**
     * Insure we always have a base token at least as long
     * as the max PHP session token string length
     */
    const TOKEN_BYTE_COUNT = 64;
    
    /**
     * Truncate the generated token to the length
     * designated by the current php.ini setting.
     * 
     * @return string
     */
    protected function generateToken(): string
    {
        $token = parent::generateToken();
        $len = intval(ini_get('session.sid_length'));
        return substr($token, 0, $len);
    }

}
