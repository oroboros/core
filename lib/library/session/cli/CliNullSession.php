<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\library\session\cli;

/**
 * Cli Null Session
 * Cli session that nullifies session storage,
 * resulting in a fresh session on each request.
 *
 * This should be used for stateless cli requests of all types.
 * 
 * @author Brian Dayhoff
 */
final class CliNullSession extends \Oroboros\core\abstracts\library\session\AbstractCliSession implements \Oroboros\core\interfaces\CoreInterface
{
    /**
     * Designates that this session uses the null session handler,
     * which does not persist session data across requests.
     * 
     * Session data will be available throughout the individual runtime,
     * but is discarded on shutdown. A new session will be generated in memory
     * on each subsequent call, but will not persist beyond a single runtime,
     * resulting in typical stateless cli performance.
     */
    const SESSION_HANDLER_CLASS = \Oroboros\core\library\session\NullSessionHandler::class;
}
