<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\library\component\html;

/**
 * Bootstrap Error Debug Info
 * Represents a component wrapper for a debug error information detail view
 * @author Brian Dayhoff
 */
class ErrorDebugInfo extends \Oroboros\bootstrap\abstracts\library\component\html\AbstractBootstrapComponent implements \Oroboros\core\interfaces\CoreInterface
{

    const COMPONENT_TEMPLATE = 'component/debug/errors/error-info';
    const COMPONENT_KEY = 'error-info';

    private $context_keys = [
        'error',
        'exception',
        'system',
        'diagnostics',
    ];
    private $context_component_filters = [
        'error' => 'filterError'
    ];

    protected function declareValidContextKeys(): array
    {
        return array_merge(parent::declareValidContextKeys(), $this->context_keys);
    }

    protected function declareContextFilters(): array
    {
        return array_merge(parent::declareContextFilters(), $this->context_component_filters);
    }

    /**
     * Formats the error definition to strip out excessive details and info that
     * will crash the page on render, while still leaving enough information to
     * get a coherent stacktrace and understanding of the root issue.
     * 
     * @param iterable $error
     * @return array
     */
    protected function filterError(iterable $error): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $interface = \Oroboros\core\interfaces\library\container\ContainerInterface::class;
        $formatters = ['message', 'trace', 'code', 'flags', 'file', 'line', 'referer', 'controller', 'scope'];
        // Array compatibility
        if (is_array($error)) {
            $error = $this->containerize($error);
        }
        // Third party object compatibility
        if (is_object($error) && !($error instanceof $interface)) {
            $tmp = [];
            foreach ($error as $key => $value) {
                $tmp[$key] = $value;
            }
            $error = $this->containerize($tmp);
            unset($tmp);
        }
        $results = [];
        if ($error->has('arguments')) {
            // Causes infinite recursion.
            unset($error['arguments']);
        }
        foreach ($formatters as $formatter) {
            if ($error->has($formatter)) {
                $method = sprintf('format%1$s', ucfirst($formatter));
                $results[$formatter] = $this->$method($error[$formatter]);
            }
        }
        return $this->containerize($results);
    }

    /**
     * Formats any error message passed
     * 
     * @param string|null $message
     * @return string|null
     */
    private function formatMessage(string $message = null): ?string
    {
        return $message;
    }

    /**
     * Formats any flags passed with the error
     * 
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $flags
     * @return array
     */
    private function formatFlags(\Oroboros\core\interfaces\library\container\ContainerInterface $flags): array
    {
        $formatted = [];
        foreach ($flags as $key => $value) {
            if ($value === true) {
                $formatted[] = $key;
            }
        }
        return $formatted;
    }

    /**
     * Formats the error stacktrace
     * 
     * @param array $trace
     * @return array
     */
    private function formatTrace(array $trace): array
    {
        $formatted = [];
        foreach ($trace as $stacklevel) {
            $formatted[] = [
                'file' => $stacklevel['file'],
                'line' => $stacklevel['line'],
                'function' => $stacklevel['function'],
                'class' => $stacklevel['class'],
                'type' => $stacklevel['type'],
            ];
        }
        return $formatted;
    }

    /**
     * Formats the status code
     * 
     * @param int $code
     * @return int
     */
    private function formatCode(int $code): int
    {
        return $code;
    }

    /**
     * Formats the filename
     * 
     * @param string $file
     * @return string
     */
    private function formatFile(string $file): string
    {
        return $file;
    }

    /**
     * Formats the line number
     * 
     * @param int $line
     * @return int
     */
    private function formatLine(int $line): int
    {
        return $line;
    }

    /**
     * Formats the referer array
     * Referer args cause infinite recursion.
     * 
     * @param array $referer
     * @return array
     */
    private function formatReferer(array $referer): array
    {
        if (array_key_exists('args', $referer)) {
            unset($referer['args']);
        }
        return $referer;
    }

    /**
     * Formats the controller name
     * 
     * @param string $controller
     * @return string
     */
    private function formatController(string $controller): string
    {
        return $controller;
    }

    /**
     * Formats scope
     * 
     * @param string $scope
     * @return string
     */
    private function formatScope(string $scope): string
    {
        return $scope;
    }
}
