<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\library\component\html;

/**
 * Bootstrap Error Debugger
 * Represents a component wrapper for a Bootstrap responsive development debugger element 
 * for error debug information
 * @author Brian Dayhoff
 */
class ErrorDebug extends \Oroboros\bootstrap\abstracts\library\component\html\AbstractBootstrapComponent implements \Oroboros\core\interfaces\CoreInterface
{

    const COMPONENT_TEMPLATE = 'component/debug/error-debug';
    const COMPONENT_KEY = 'error-debug';

    private $context_keys = [
        'error',
        'exception',
        'system',
        'diagnostics',
    ];
    private $context_component_filters = [
        'error' => 'filterError'
    ];

    protected function declareValidContextKeys(): array
    {
        return array_merge(parent::declareValidContextKeys(), $this->context_keys);
    }

    protected function declareContextFilters(): array
    {
        return array_merge(parent::declareContextFilters(), $this->context_component_filters);
    }

    protected function getDefaultScripts(): array
    {
        return array_merge_recursive(parent::getDefaultScripts(), [
            'footer' => [sprintf('%1$s-%2$s', static::CLASS_SCOPE, static::COMPONENT_KEY)]
        ]);
    }
    
    protected function filterError($error)
    {
        d($error);
        return $error;
    }
}
