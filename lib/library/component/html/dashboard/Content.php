<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\library\component\html\dashboard;

/**
 * Bootstrap Dashboard Primary Content
 * Provides the primary content container for a dashboard layout.
 * @author Brian Dayhoff
 */
class Content extends \Oroboros\bootstrap\abstracts\library\component\html\AbstractBootstrapComponent implements \Oroboros\core\interfaces\CoreInterface
{

    const COMPONENT_TEMPLATE = 'component/dashboard/content/content';
    const COMPONENT_KEY = 'dashboard-content';

    private $context_keys = [
        'title',
        'widgets',
    ];
    private $context_component_filters = [
        'widgets' => 'filterTitle',
        'widgets' => 'filterWidgets',
    ];

    protected function declareValidContextKeys(): array
    {
        return array_merge(parent::declareValidContextKeys(), $this->context_keys);
    }

    protected function declareContextFilters(): array
    {
        return array_merge(parent::declareContextFilters(), $this->context_component_filters);
    }

    protected function getDefaultScripts(): array
    {
        return array_merge_recursive(parent::getDefaultScripts(), [
            'footer' => [sprintf('%1$s-%2$s', static::CLASS_SCOPE, static::COMPONENT_KEY)]
        ]);
    }

    protected function filterWidgets($widgets)
    {
        if (!is_iterable($widgets)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Provided dashboard widgets value must be iterable.', get_class($this)));
        }
        if (is_object($widgets) && $widgets instanceof \Oroboros\core\interfaces\library\container\ContainerInterface) {
            $widgets = $widgets->toArray();
        }
        $widget_objects = [];
        foreach ($widgets as $key => $widget) {
            if (!is_iterable($widget)) {
                continue;
            }
            if (!array_key_exists('component', $widget)) {
                $widget['component'] = 'dashboard-widget';
            }
            $widget_objects[$key] = $this->getComponent($widget['component'], (string) $key, $widget);
        }
//        d($widget_objects); exit;
        $component = $this->getComponent('dashboard-widget-row', 'dashboard-widget-row', ['widgets' => $widget_objects]);
//        d($widgets, $component); exit;
        return $component;
    }

    protected function filterTitle($title)
    {
        return $this::containerize($title);
    }
}
