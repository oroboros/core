<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\library\component\html\dashboard;

/**
 * Dashboard Sub-Heading Ui
 * Represents a component wrapper for the Oroboros dashboard page sub-heading ui section
 * 
 * @author Brian Dayhoff
 */
class SubheadingUi extends \Oroboros\bootstrap\abstracts\library\component\html\AbstractBootstrapComponent implements \Oroboros\core\interfaces\CoreInterface
{

    const COMPONENT_TEMPLATE = 'component/dashboard/content/sub-heading-ui';
    const COMPONENT_KEY = 'dashboard-subheading-ui';

    private $context_keys = [
        'ui'
    ];
    private $context_component_filters = [
//        'ui' => 'filterUi',
    ];
    
    public function render(): string
    {
//        d($this->getContextObject());
        return parent::render();
    }

    protected function declareValidContextKeys(): array
    {
        return array_merge(parent::declareValidContextKeys(), $this->context_keys);
    }

    protected function declareContextFilters(): array
    {
        return array_merge(parent::declareContextFilters(), $this->context_component_filters);
    }
    
//    protected function filterUi($ui)
//    {
//        if (is_null($ui)) {
//            return null;
//        }
//        $id = null;
//        $class = 'dashboard-subheading-ui';
//        if (is_string($ui)) {
//            $ui = [
//                'id' => $ui
//            ];
//        }
//        if (is_array($ui)) {
//            $ui = $this->containerize($ui);
//        }
//        if (!$ui->has('id')) {
//            $ui['id'] = $class;
//        }
//        if (!$ui->has('component')) {
//            $ui['component'] = $class;
//        }
//        $id = $ui['id'];
//        $class = $ui['component'];
//        $component = $this->getComponent($class, $id, $ui->toArray());
//        $component->setLogger($this->getLogger());
//        $component->setParent($this);
//        return $component;
//    }
}
