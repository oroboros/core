<?php
namespace Oroboros\core\library\router;

/**
 * Provides standard operation for single route objects for the command line.
 * While PSR-7 does not specify message body standards for the command line,
 * this class should emulate compatibility, so that cli requests can be handled
 * identically to the PSR-7 compliant http request process.
 *
 * @author Brian Dayhoff
 */
final class CliRoute extends \Oroboros\core\abstracts\library\router\AbstractRoute implements \Oroboros\core\interfaces\CoreInterface
{

    const ROUTE_SCOPE = 'cli';

}
