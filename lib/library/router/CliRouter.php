<?php
namespace Oroboros\core\library\router;

/**
 * Provides routing operations for command line requests.
 * While PSR-7 does not specify message body standards for the command line,
 * this class should emulate compatibility, so that cli requests can be handled
 * identically to the PSR-7 compliant http request process.
 *
 * @author Brian Dayhoff
 */
final class CliRouter extends \Oroboros\core\abstracts\library\router\AbstractRouter implements \Oroboros\core\interfaces\CoreInterface
{

    const ROUTER_SCOPE = 'cli';
    const ROUTE_CLASS = 'router\\CliRoute';

    private static $error_controllers = [
        'job' => 'cli\\ErrorController',
        'build' => 'cli\\ErrorController',
        '*' => 'cli\\ErrorController',
    ];
    private $controller = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
    }

    /**
     * Returns a PSR-7 valid ServerRequestInterface
     * 
     * This package currently utilizes Zend Diactoros for this purpose.
     * It may eventually switch to Guzzle in the future.
     * 
     * @note As the CLI does not define several of the typical http superglobals,
     *     we will need to manually construct a spoofed psr-7 object to avoid writing
     *     a massively different router api for the command line.
     *     The following criteria will be used to turn command line arguments into a uri:
     *     - Arguments prefixed with a "-" will translate into the query string.
     *     - Arguments prefixed with a "-" that contain a "=" are considered to directly define their query string value
     *     - Arguments prefixed with a "-" that do not contain a "=" will intercept the following argument as their value, provided it is not also prefixed with a "-"
     *     - Arguments prefixed with a "-" that are followed by another argument also prefixed with a "-" will be considered to be query string keys with no value
     *     - Arguments not prefixed with a "-" will be used as an argument for the last key that was prefixed with a "-" and did not supply its own argument.
     *     - Arguments not prefixed with a "-" will become part of the uri path if no argument prefixed by a "-" preceded them.
     *     - Post is always empty
     *     - Cookies are always empty
     *     - Session is always empty
     *     - Request method is always cli
     *     - Http Protocol is always http 1.1, as psr-7 does not support non-http protocols. This value is ignored internally if the request method is cli.
     * @return Psr\Http\Message\ServerRequestInterface
     */
    protected function initializeRequestObject()
    {
        global $argv;
        $args = $argv;
        array_shift($args);
        $params = [];
        $path = [];
        $nextkey = null;
        foreach ($args as $arg) {
            if (strpos($arg, '-') === 0 && strpos($arg, '=') !== false) {
                // The argument is included with the key
                $parts = explode('=', $arg);
                $key = array_shift($parts);
                $key = trim($key, '-');
                $value = array_shift($parts);
                $params[$key] = $value;
                $nextkey = null;
            } elseif (strpos($arg, '-') === 0) {
                // The argument does not have a key
                $params[trim($arg, ' -')] = null;
            } else {
                if (!is_null($nextkey)) {
                    // This is an argument for a flag
                    $params[$nextkey] = $arg;
                    $nextkey = null;
                    continue;
                }
                $path[] = $arg;
            }
        };
        $url = sprintf('http://%1$s/', $this::app()->getApplicationDomain());
        if (!empty($path)) {
            $url .= implode('/', array_map(function ($segment) {
                        return trim($segment);
                    }, $path)) . '/';
        }
        if (!empty($params)) {
            $paramset = '?';
            foreach ($params as $key => $value) {
                if (!is_null($value)) {
                    $paramset .= sprintf('%1$s=%2$s&', $key, $value);
                } else {
                    $paramset .= sprintf('%1$s&', $key);
                }
            }
            $paramset = rtrim($paramset, '&');
            $url .= $paramset;
        }
        $uri = new \Zend\Diactoros\Uri($url);
        $request = (new \Zend\Diactoros\ServerRequest())->withUri($uri)->withMethod(static::ROUTER_SCOPE);
        return $request;
    }

    /**
     * Must be overridden in the extension class to provide a default route
     * that handles errors when no route exists for the given request.
     * 
     * This route is always returned when no route exists for the given request.
     * 
     * @return Oroboros\core\interfaces\library\router\RouteInterface
     */
    public function loadErrorRoute(\Psr\Http\Message\ServerRequestInterface $request)
    {
        global $argv;
        $error_controller = $this->matchErrorControllerByCliArguments($argv);
        $error_route = new \Oroboros\core\library\router\CliErrorRoute();
        $error_route->match($request);
        $error_route->setController($error_controller);
        return $error_route;
    }

    protected function parseAcceptHeaderValues($values)
    {
        $valid = [];
        if (is_array($values)) {
            // Array of accept headers
            foreach ($values as $value) {
                $tmp = array_map('trim', explode(',', $value));
                $this->parseAcceptValues($tmp);
                foreach ($tmp as $val) {
                    if (!in_array($val, $valid)) {
                        $valid[] = $val;
                    }
                }
            }
        } else {
            // Single accept header
            $tmp = array_map('trim', explode(',', $values));
            $this->parseAcceptValues($tmp);
            foreach ($tmp as $val) {
                if (!in_array($val, $valid)) {
                    $valid[] = $val;
                }
            }
        }
        $valid[] = '*'; //catchall
        return $valid;
    }

    private function matchErrorControllerByCliArguments(array $args)
    {
        $match = null;
        array_shift($args);
        if (empty($args)) {
            $type = '*';
        } else {
            $type = array_shift($args);
        }
        if (array_key_exists($type, self::$error_controllers)) {
            $match = self::$error_controllers[$type];
        }
        if (is_null($match)) {
            $match = self::$error_controllers['*'];
        }
        return $match;
    }

    private function parseAcceptValues(array &$values)
    {
        foreach ($values as $key => $value) {
            if (strpos($value, ';') !== false) {
                // Remove meta
                $value = substr($value, 0, strpos($value, ';'));
            }
            if (strpos($value, '/') !== false) {
                $tmp = explode('/', $value);
                $value = array_pop($tmp);
            }
            if (strpos($value, '+') !== false) {
                $tmp = explode('+', $value);
                $value = array_shift($tmp);
            }
            if ($value === '*') {
                unset($values[$key]); // Only one wildcard will be added at the end with the lowest priority.
                continue;
            }
            $values[$key] = $value;
        }
    }
}
