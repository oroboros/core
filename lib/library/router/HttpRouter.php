<?php
namespace Oroboros\core\library\router;

/**
 * Provides routing operations for HTTP requests.
 * This should be compatible with PSR-7
 *
 * @author Brian Dayhoff
 */
final class HttpRouter extends \Oroboros\core\abstracts\library\router\AbstractRouter implements \Oroboros\core\interfaces\CoreInterface
{

    const ROUTER_SCOPE = 'http';
    const ROUTE_CLASS = 'router\\HttpRoute';

    private static $error_controllers = [
        'html' => 'html\\ErrorController',
        'json' => 'http\\ErrorController',
        'text' => 'http\\ErrorController',
        '*' => 'http\\ErrorController',
    ];
    private $controller = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
    }

    /**
     * Returns a PSR-7 valid ServerRequestInterface
     * 
     * This package currently utilizes Zend Diactoros for this purpose.
     * It may eventually switch to Guzzle in the future.
     * 
     * @return Psr\Http\Message\ServerRequestInterface
     */
    protected function initializeRequestObject()
    {
        $request = \Zend\Diactoros\ServerRequestFactory::fromGlobals(
                $_SERVER, $_GET, $_POST, $_COOKIE, $_FILES
        );
        return $request;
    }

    /**
     * Must be overridden in the extension class to provide a default route
     * that handles errors when no route exists for the given request.
     * 
     * This route is always returned when no route exists for the given request.
     * 
     * @return Oroboros\core\interfaces\library\router\RouteInterface
     */
    public function loadErrorRoute(\Psr\Http\Message\ServerRequestInterface $request)
    {
        $accept = $request->getHeaders()['accept'];
        $valid_accept = $this->parseAcceptHeaderValues($accept);
        $error_controller = $this->matchErrorControllerByAcceptValues($valid_accept);
        $error_route = new \Oroboros\core\library\router\HttpErrorRoute();
        $error_route->match($request);
        $error_route->setController($error_controller);
        return $error_route;
    }

    protected function parseAcceptHeaderValues($values)
    {
        $valid = [];
        if (is_array($values)) {
            // Array of accept headers
            foreach ($values as $value) {
                $tmp = array_map('trim', explode(',', $value));
                $this->parseAcceptValues($tmp);
                foreach ($tmp as $val) {
                    if (!in_array($val, $valid)) {
                        $valid[] = $val;
                    }
                }
            }
        } else {
            // Single accept header
            $tmp = array_map('trim', explode(',', $values));
            $this->parseAcceptValues($tmp);
            foreach ($tmp as $val) {
                if (!in_array($val, $valid)) {
                    $valid[] = $val;
                }
            }
        }
        $valid[] = '*'; //catchall
        return $valid;
    }

    private function matchErrorControllerByAcceptValues(array $accept)
    {
        $match = null;
        foreach ($accept as $valid) {
            if (array_key_exists($valid, self::$error_controllers)) {
                $match = self::$error_controllers[$valid];
                break;
            }
        }
        if (is_null($match)) {
            $match = self::$error_controllers['*'];
        }
        return $match;
    }

    private function parseAcceptValues(array &$values)
    {
        foreach ($values as $key => $value) {
            if (strpos($value, ';') !== false) {
                // Remove meta
                $value = substr($value, 0, strpos($value, ';'));
            }
            if (strpos($value, '/') !== false) {
                $tmp = explode('/', $value);
                $value = array_pop($tmp);
            }
            if (strpos($value, '+') !== false) {
                $tmp = explode('+', $value);
                $value = array_shift($tmp);
            }
            if ($value === '*') {
                unset($values[$key]); // Only one wildcard will be added at the end with the lowest priority.
                continue;
            }
            $values[$key] = $value;
        }
    }
}
