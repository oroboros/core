<?php
namespace Oroboros\core\library\router;

/**
 * Provides a default valid error route for when no matching route is found.
 * This route will simply return the 404 method of the cli error controller.
 * This should be compatible with PSR-7
 *
 * @author Brian Dayhoff
 */
final class CliErrorRoute extends \Oroboros\core\abstracts\library\router\AbstractRoute implements \Oroboros\core\interfaces\CoreInterface
{

    const ROUTE_SCOPE = 'cli';

}
