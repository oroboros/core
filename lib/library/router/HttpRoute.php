<?php
namespace Oroboros\core\library\router;

/**
 * Provides standard operation for single route objects for http requests.
 * This should be compatible with PSR-7
 *
 * @author Brian Dayhoff
 */
final class HttpRoute extends \Oroboros\core\abstracts\library\router\AbstractRoute implements \Oroboros\core\interfaces\CoreInterface
{

    const ROUTE_SCOPE = 'http';

}
