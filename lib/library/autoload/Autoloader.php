<?php
namespace Oroboros\core\library\autoload;

/**
 * This is a very generic implementation of the Psr-4 Autoload standard,
 * very slightly modified from the original class example provided by PHP-FIG
 * to suit the needs of Oroboros.
 * 
 * @see https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-4-autoloader-examples.md
 *
 * Given a foo-bar package of classes in the file system at the following
 * paths ...
 *
 *     /path/to/packages/foo-bar/
 *         src/
 *             Baz.php             # Foo\Bar\Baz
 *             Qux/
 *                 Quux.php        # Foo\Bar\Qux\Quux
 *         tests/
 *             BazTest.php         # Foo\Bar\BazTest
 *             Qux/
 *                 QuuxTest.php    # Foo\Bar\Qux\QuuxTest
 *
 * ... add the path to the class files for the \Foo\Bar\ namespace prefix
 * as follows:
 *
 *      <?php
 *      // instantiate the loader
 *      $loader = new \Example\Psr4AutoloaderClass;
 *
 *      // register the autoloader
 *      $loader->register();
 *
 *      // register the base directories for the namespace prefix
 *      $loader->addNamespace('Foo\Bar', '/path/to/packages/foo-bar/src');
 *      $loader->addNamespace('Foo\Bar', '/path/to/packages/foo-bar/tests');
 *
 * The following line would cause the autoloader to attempt to load the
 * \Foo\Bar\Qux\Quux class from /path/to/packages/foo-bar/src/Qux/Quux.php:
 *
 *      <?php
 *      new \Foo\Bar\Qux\Quux;
 *
 * The following line would cause the autoloader to attempt to load the
 * \Foo\Bar\Qux\QuuxTest class from /path/to/packages/foo-bar/tests/Qux/QuuxTest.php:
 *
 *      <?php
 *      new \Foo\Bar\Qux\QuuxTest;
 */
final class Autoloader
{

    /**
     * Only one instance of this needs to be registered.
     * This class was modified from the original to share a static list
     * of registered namespaces to improve performance,
     * and prevent duplicate SPL calls.
     * 
     * Provided any instance is registered,
     * there is no need to register any further instantiated object,
     * allowing further instances to focus on which packages to autoload
     * rather than having to duplicate registration logic.
     * @var bool
     */
    private static $is_registered = false;

    /**
     * Contains the base level registered object if already registered,
     * so any further object instance may perform unregistration if required.
     * 
     * @var callable
     */
    private static $registered_object = null;

    /**
     * An associative array where the key is a namespace prefix and the value
     * is an array of base directories for classes in that namespace.
     *
     * @var array
     */
    private static $prefixes = array();

    /**
     * Register loader with SPL autoloader stack.
     *
     * @return $this (method chainable)
     */
    public function register()
    {
        // Do not duplicate SPL registration if an instance is already registered.
        if (!self::$is_registered) {
            self::$registered_object = array($this, 'loadClass');
            spl_autoload_register(self::$registered_object);
            self::$is_registered = true;
        }
        return $this;
    }

    /**
     * Unregisters the autoloader from the SPL stack.
     * This is useful in the event that a more optimized or robust
     * autoloading paradigm is desired in place of this functionality,
     * such as Composer's compiled autoload functionality,
     * which would give a minor boost to performance.
     * 
     * This method takes no action if the autoloader is not presently registered.
     * 
     * @return $this (method chainable)
     */
    public function unregister()
    {
        if (self::$is_registered) {
            spl_autoload_unregister(self::$registered_object);
            self::$registered_object = null;
            self::$is_registered = false;
        }
        return $this;
    }

    /**
     * Adds a base directory for a namespace prefix.
     *
     * @param string $prefix The namespace prefix.
     * @param string $base_dir A base directory for class files in the
     * namespace.
     * @param bool $prepend If true, prepend the base directory to the stack
     * instead of appending it; this causes it to be searched first rather
     * than last.
     * @return void
     */
    public function addNamespace($prefix, $base_dir, $prepend = false)
    {
        // normalize namespace prefix
        $prefix = trim($prefix, '\\') . '\\';

        // normalize the base directory with a trailing separator
        $base_dir = rtrim($base_dir, DIRECTORY_SEPARATOR) . '/';

        // initialize the namespace prefix array
        if (isset(self::$prefixes[$prefix]) === false) {
            if ($prepend) {
                $prefixes = [$prefix => []];
                foreach (self::$prefixes as $key => $value) {
                    $prefixes[$key] = $value;
                }
                self::$prefixes = $prefixes;
            } else {
                self::$prefixes[$prefix] = array();
            }
        }

        // retain the base directory for the namespace prefix
        if ($prepend) {
            array_unshift(self::$prefixes[$prefix], $base_dir);
        } else {
            array_push(self::$prefixes[$prefix], $base_dir);
        }
    }

    /**
     * Returns an array of the currently registered namespaces
     * @return array
     */
    public static function getNamespaces(): array
    {
        $prefixes = [];
        foreach (self::$prefixes as $key => $value) {
            $prefixes[] = trim($key, '\\');
        }
        return $prefixes;
    }

    /**
     * Loads the class file for a given class name.
     *
     * @param string $class The fully-qualified class name.
     * @return mixed The mapped file name on success, or boolean false on
     * failure.
     */
    public function loadClass($class)
    {
        // the current namespace prefix
        $prefix = $class;

        // work backwards through the namespace names of the fully-qualified
        // class name to find a mapped file name
        while (false !== $pos = strrpos($prefix, '\\')) {

            // retain the trailing namespace separator in the prefix
            $prefix = substr($class, 0, $pos + 1);

            // the rest is the relative class name
            $relative_class = substr($class, $pos + 1);

            // try to load a mapped file for the prefix and relative class
            $mapped_file = $this->loadMappedFile($prefix, $relative_class);
            if ($mapped_file) {
                return $mapped_file;
            }

            // remove the trailing namespace separator for the next iteration
            // of strrpos()
            $prefix = rtrim($prefix, '\\');
        }

        // never found a mapped file
        return false;
    }
    
    /**
     * Returns the directory path associated with the namespace if one exists,
     * or null if it does not.
     * @param string $namespace
     * @return string|null
     */
    public function lookupNamespace(string $namespace): ?string
    {
        $namespace = rtrim($namespace, '\\') . '\\';
        if (array_key_exists($namespace, self::$prefixes)) {
            $dir = rtrim(self::$prefixes[$namespace][0], DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;
            return $dir;
        }
        return null;
    }

    /**
     * Load the mapped file for a namespace prefix and relative class.
     *
     * @param string $prefix The namespace prefix.
     * @param string $relative_class The relative class name.
     * @return mixed Boolean false if no mapped file can be loaded, or the
     * name of the mapped file that was loaded.
     */
    protected function loadMappedFile($prefix, $relative_class)
    {
        // are there any base directories for this namespace prefix?
        if (isset(self::$prefixes[$prefix]) === false) {
            return false;
        }

        // look through base directories for this namespace prefix
        foreach (self::$prefixes[$prefix] as $base_dir) {

            // replace the namespace prefix with the base directory,
            // replace namespace separators with directory separators
            // in the relative class name, append with .php
            $file = $base_dir
                . str_replace('\\', '/', $relative_class)
                . '.php';

            // if the mapped file exists, require it
            if ($this->requireFile($file)) {
                // yes, we're done
                return $file;
            }
        }

        // never found it
        return false;
    }

    /**
     * If a file exists, require it from the file system.
     *
     * @param string $file The file to require.
     * @return bool True if the file exists, false if not.
     */
    protected function requireFile($file)
    {
        if (file_exists($file)) {
            require $file;
            return true;
        }
        return false;
    }
}
