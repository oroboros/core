<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\library\auth\permissions\http;

/**
 * HTTP Permission
 * This permission is assigned to http users and processes.
 * Any permission in this set may only be scoped to http users and processes,
 * or command line processes that handle them.
 * If the request came from a cli origin and the cli task is not associated
 * with http functionality, all http permissions will be removed before the
 * final set is evaluated against.
 * 
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
final class HttpPermission extends \Oroboros\core\abstracts\library\auth\AbstractPermission implements \Oroboros\core\interfaces\CoreInterface
{

    /**
     * Designates that this class is part of the command line auth group
     */
    const AUTH_SCOPE = 'cli';

}
