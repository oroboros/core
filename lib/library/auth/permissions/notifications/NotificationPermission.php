<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\library\auth\permissions\notifications;

/**
 * Notification Permission
 * This permission is assigned based on notification preferences.
 * 
 * This permission may be self-revoked by users in most cases when they opt-out
 * of some communication format, and may be self-assigned when they opt in.
 * 
 * Permissions in this category are constrained by application settings but may
 * be self-managed by any user they apply to if they are enabled. This will determine
 * if messages are stored or sent to them of various types
 * (eg email, push notificatons, alerts, etc)
 * 
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
final class NotificationPermission extends \Oroboros\core\abstracts\library\auth\AbstractPermission implements \Oroboros\core\interfaces\CoreInterface
{

    /**
     * Designates that this class is part of the notifications auth group
     */
    const AUTH_SCOPE = 'notifications';

}
