<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\library\auth\login;

/**
 * Login Manager
 * Manages login operations, authentication, validation of credentials, etc
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
final class LoginManager extends \Oroboros\core\abstracts\library\auth\AbstractAuthManager implements \Oroboros\core\interfaces\CoreInterface
{

    const CLASS_SCOPE = 'login';

    /**
     * Designates that this class is part of the login auth group
     */
    const AUTH_SCOPE = self::CLASS_SCOPE;

    /**
     * This constant must be overridden to declare a
     * valid authentication credential class.
     * Credential classes must be fully qualified.
     */
    const AUTH_CREDENTIAL_CLASS = 'auth\\login\\LoginCredentials';

    /**
     * This constant must be overridden to declare a
     * valid authentication identity class or stub name.
     */
    const AUTH_IDENTITY_CLASS = 'auth\\login\\LoginIdentity';

    /**
     * This constant may be overridden to declare a
     * valid authentication nonce class or stub name.
     */
    const AUTH_NONCE_CLASS = 'auth\\login\\LoginNonce';

    /**
     * This constant may be overridden to declare a
     * valid authentication salt class or stub name.
     */
    const AUTH_SALT_CLASS = 'auth\\login\\LoginSalt';

}
