<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\library\auth;

/**
 * Permission Group Definer
 * Default permission group definer implementation
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
final class PermissionGroupDefiner extends \Oroboros\core\abstracts\library\auth\AbstractPermissionGroupDefiner implements \Oroboros\core\interfaces\CoreInterface
{

    use \Oroboros\core\traits\definer\JsonDefinerTrait {
        \Oroboros\core\traits\definer\JsonDefinerTrait::declareDataSet as private traitDeclareDataSet;
    }

    const CLASS_SCOPE = 'auth';

    /**
     * Designates that this class is part of the general auth group
     */
    const AUTH_SCOPE = self::CLASS_SCOPE;

    /**
     * Path to the definer definition config, relative to the application root.
     */
    const DEFINER_FILE = 'etc/permission/groups/defaults.json';

    /**
     * Preflights class constant declaration prior to calling parent constructor
     * and initializes the permission group definer
     * and initializes the permission dataset
     * @param string $command
     * @param array $arguments
     * @param array $flags
     */
    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        $this->validateDefinerFile();
        parent::__construct($command, $arguments, $flags);
        $this->initializePermissionGroupDefiner();
    }

    /**
     * Updates the declared data set to include any additional permission groups
     * 
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    protected function declareDataSet(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $data = $this->traitDeclareDataSet();
        return $data;
    }

    /**
     * Initializes the permission group definer
     * 
     * @return void
     */
    private function initializePermissionGroupDefiner(): void
    {
        
    }
}
