<?php
namespace Oroboros\core\library\database\view;

/**
 * Provides an object accessor for a MySQL View
 *
 * @author Brian Dayhoff
 */
final class MySQLView extends \Oroboros\core\abstracts\library\database\view\AbstractView implements \Oroboros\core\interfaces\library\database\format\mysql\FormatInterface, \Oroboros\core\interfaces\CoreInterface
{

    const CONNECTION_TYPE = 'mysql';

}
