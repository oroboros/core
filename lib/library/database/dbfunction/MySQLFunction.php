<?php
namespace Oroboros\core\library\database\dbfunction;

/**
 * Provides an object accessor for a MySQL Function
 *
 * @author Brian Dayhoff
 */
final class MySQLFunction extends \Oroboros\core\abstracts\library\database\dbfunction\AbstractDatabaseFunction implements \Oroboros\core\interfaces\library\database\format\mysql\FormatInterface, \Oroboros\core\interfaces\CoreInterface
{

    const CONNECTION_TYPE = 'mysql';

}
