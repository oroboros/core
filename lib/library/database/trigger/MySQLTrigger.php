<?php
namespace Oroboros\core\library\database\trigger;

/**
 * Provides an object accessor for a MySQL Trigger
 *
 * @author Brian Dayhoff
 */
final class MySQLTrigger extends \Oroboros\core\abstracts\library\database\trigger\AbstractTrigger implements \Oroboros\core\interfaces\library\database\format\mysql\FormatInterface, \Oroboros\core\interfaces\CoreInterface
{

    const CONNECTION_TYPE = 'mysql';

}
