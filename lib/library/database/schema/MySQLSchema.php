<?php
namespace Oroboros\core\library\database\schema;

/**
 * Provides an object accessor for a MySQL Schema
 *
 * @author Brian Dayhoff
 */
final class MySQLSchema extends \Oroboros\core\abstracts\library\database\schema\AbstractSchema implements \Oroboros\core\interfaces\library\database\format\mysql\FormatInterface, \Oroboros\core\interfaces\CoreInterface
{

    const CONNECTION_TYPE = 'mysql';

}
