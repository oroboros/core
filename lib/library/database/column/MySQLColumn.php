<?php
namespace Oroboros\core\library\database\column;

/**
 * Provides an object accessor for a MySQL Column
 *
 * @author Brian Dayhoff
 */
final class MySQLColumn extends \Oroboros\core\abstracts\library\database\column\AbstractColumn implements \Oroboros\core\interfaces\library\database\format\mysql\FormatInterface, \Oroboros\core\interfaces\CoreInterface
{

    const CONNECTION_TYPE = 'mysql';

}
