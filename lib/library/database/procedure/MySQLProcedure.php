<?php
namespace Oroboros\core\library\database\procedure;

/**
 * Provides an object accessor for a MySQL Procedure
 *
 * @author Brian Dayhoff
 */
final class MySQLProcedure extends \Oroboros\core\abstracts\library\database\procedure\AbstractProcedure implements \Oroboros\core\interfaces\library\database\format\mysql\FormatInterface, \Oroboros\core\interfaces\CoreInterface
{

    const CONNECTION_TYPE = 'mysql';

}
