<?php
namespace Oroboros\core\library\database\table;

/**
 * Provides an object accessor for a MySQL Table
 *
 * @author Brian Dayhoff
 */
final class MySQLTable extends \Oroboros\core\abstracts\library\database\table\AbstractTable implements \Oroboros\core\interfaces\library\database\format\mysql\FormatInterface, \Oroboros\core\interfaces\CoreInterface
{

    const CONNECTION_TYPE = 'mysql';

}
