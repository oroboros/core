<?php
namespace Oroboros\core\library\database\connection;

/**
 * Handles MySQL connections via MySQL
 *
 * @author Brian Dayhoff
 */
final class MySQL extends \Oroboros\core\abstracts\library\database\connection\AbstractConnection implements \Oroboros\core\interfaces\library\database\format\mysql\FormatInterface, \Oroboros\core\interfaces\CoreInterface
{

    const CONNECTION_TYPE = 'mysql';

}
