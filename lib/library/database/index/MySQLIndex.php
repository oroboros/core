<?php
namespace Oroboros\core\library\database\index;

/**
 * Provides an object accessor for a MySQL Index
 *
 * @author Brian Dayhoff
 */
final class MySQLIndex extends \Oroboros\core\abstracts\library\database\index\AbstractIndex implements \Oroboros\core\interfaces\library\database\format\mysql\FormatInterface, \Oroboros\core\interfaces\CoreInterface
{

    const CONNECTION_TYPE = 'mysql';

}
