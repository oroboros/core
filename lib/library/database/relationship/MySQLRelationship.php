<?php
namespace Oroboros\core\library\database\relationship;

/**
 * Provides an object accessor for a MySQL Relationship
 *
 * @author Brian Dayhoff
 */
final class MySQLRelationship extends \Oroboros\core\abstracts\library\database\relationship\AbstractRelationship implements \Oroboros\core\interfaces\library\database\format\mysql\FormatInterface, \Oroboros\core\interfaces\CoreInterface
{

    const CONNECTION_TYPE = 'mysql';

}
