<?php
namespace Oroboros\core\library\template\twig;

/**
 * Default Twig Extension
 * Allows addition of custom functionality to the Twig Engine
 *
 * @author Brian Dayhoff
 */
class TwigExtension extends \Twig\Extension\AbstractExtension
{

    public function getTokenParsers()
    {
        return array();
    }

    public function getNodeVisitors()
    {
        return array();
    }

    public function getFilters()
    {
        return array(
            'camelcase' => (new \Twig\TwigFilter('camelcase', function ($value) {
                    return \Oroboros\core\library\formatter\StringFormatter::camelcase($value);
                })),
            'brutecase' => (new \Twig\TwigFilter('brutecase', function ($value) {
                    return \Oroboros\core\library\formatter\StringFormatter::brutecase($value);
                })),
            'canonicalize' => (new \Twig\TwigFilter('canonicalize', function ($value) {
                    return \Oroboros\core\library\formatter\StringFormatter::canonicalize($value);
                })),
            'exerpt' => (new \Twig\TwigFilter('exerpt', function ($value, $max_length = 300, $suffix = '...') {
                    return \Oroboros\core\library\formatter\StringFormatter::exerpt($value, $max_length, $suffix);
                })),
            'extract' => (new \Twig\TwigFilter('extract', function ($value, $left = null, $right = null) {
                    return \Oroboros\core\library\formatter\StringFormatter::extract($value, $left, $right);
                })),
            'indent' => (new \Twig\TwigFilter('indent', function ($value, $count = 1, $character = ' ') {
                    return \Oroboros\core\library\formatter\StringFormatter::indent($value, $count, $character);
                })),
            'interpolate' => (new \Twig\TwigFilter('interpolate', function ($value, $context = [], $search_left = '[', $search_right = ']', $left_wrap = null, $right_wrap = null) {
                    return \Oroboros\core\library\formatter\StringFormatter::interpolate($value, $context, $search_left, $search_right, $left_wrap, $right_wrap);
                })),
            'leftpad' => (new \Twig\TwigFilter('leftpad', function ($value, $count = 1, $delimiter = ' ') {
                    return \Oroboros\core\library\formatter\StringFormatter::leftPad($value, $count, $delimiter);
                })),
            'rightpad' => (new \Twig\TwigFilter('rightpad', function ($value, $count = 1, $delimiter = ' ') {
                    return \Oroboros\core\library\formatter\StringFormatter::rightPad($value, $count, $delimiter);
                })),
            'split' => (new \Twig\TwigFilter('split', function ($value, $delimiter = PHP_EOL) {
                    return \Oroboros\core\library\formatter\StringFormatter::split($value, $delimiter);
                })),
            'wrap' => (new \Twig\TwigFilter('wrap', function ($value, $left = '[', $right = ']') {
                    return \Oroboros\core\library\formatter\StringFormatter::wrap($value, $left, $right);
                })),
        );
    }

    public function getTests()
    {
        return array(
            'boolean' => (new \Twig\TwigTest('boolean', function ($value) {
                    return is_bool($value);
                })),
            'true' => (new \Twig\TwigTest('true', function ($value) {
                    return $value === true;
                })),
            'false' => (new \Twig\TwigTest('false', function ($value) {
                    return $value === false;
                })),
            'object' => (new \Twig\TwigTest('object', function ($value) {
                    return is_object($value);
                })),
            'array' => (new \Twig\TwigTest('array', function ($value) {
                    return is_array($value);
                })),
            'string' => (new \Twig\TwigTest('string', function ($value) {
                    return is_string($value);
                })),
            'component' => (new \Twig\TwigTest('component', function ($value) {
                    return is_object($value) && ($value instanceof \Oroboros\core\interfaces\library\component\ComponentInterface);
                })),
            'container' => (new \Twig\TwigTest('container', function ($value) {
                    return is_object($value) && ($value instanceof \Oroboros\core\interfaces\library\container\ContainerInterface);
                })),
        );
    }

    public function getFunctions()
    {
        return array();
    }

    public function getOperators()
    {
        return array();
    }
}
