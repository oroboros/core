<?php
namespace Oroboros\core\library\template\twig;

/**
 * Extension of the default Twig Environment.
 * 
 * Corrects some underlying file permission errors and other inconsistencies
 * that break the build
 *
 * @author Brian Dayhoff
 */
final class TwigEnvironment extends \Twig\Environment
{

    public function __construct(\Twig\Loader\LoaderInterface $loader, $options = array(), $extensions = null, $modules = null)
    {
        parent::__construct($loader, $options);
        // Use our own cache layer that respects cli file permission needs for builds
        $this->setCache(new \Oroboros\core\library\template\twig\TwigCache($options['cache']));
        $this->addDefaultExtensions();
        $this->evaluateExtensions($extensions);
        $this->evaluateModules($modules);
    }

    /**
     * Evaluates any extensions to see if they provide twig extensions.
     * Evaluated extensions will be supplied by the template engine wrapper.
     * 
     * @param type $extensions
     * @return void
     */
    private function evaluateExtensions($extensions = null): void
    {
        if (is_null($extensions)) {
            return;
        }
        foreach ($extensions as $extension) {
            if ($extension->hasTemplateExtensions('twig')) {
                $this->importPluggableExtensions($extension);
            }
        }
    }

    /**
     * Evaluates an modules to see if they provide twig extensions.
     * Evaluated modules will be supplied by the template engine wrapper.
     * 
     * @param type $modules
     * @return void
     */
    private function evaluateModules($modules = null): void
    {
        if (is_null($modules)) {
            return;
        }
        foreach ($modules as $module) {
            if ($module->hasTemplateExtensions('twig')) {
                $this->importPluggableExtensions($module);
            }
        }
    }

    /**
     * Evaluates a pluggable and imports any twig extensions it provides.
     * @param \Oroboros\core\interfaces\interoperability\TemplateEngineSupportInterface $pluggable
     * @return void
     * @throws \Oroboros\core\exception\ErrorException
     */
    private function importPluggableExtensions(\Oroboros\core\interfaces\interoperability\TemplateEngineSupportInterface $pluggable): void
    {
        $expected = 'Twig\\Extension\\AbstractExtension';
        foreach ($pluggable->getTemplateExtensions('twig') as $key => $extension) {
            if (!(is_object($extension) && $extension instanceof $expected)) {
                throw new \Oroboros\core\exception\ErrorException(
                        sprintf('Error encountered in [%1$s]. Provided pluggable [%2$s] provided '
                            . 'a malformed Twig extension for key [%2$s]. '
                            . 'Expected instance of [%3$s]', get_class($this), $pluggable->name(), $key, $expected)
                );
            }
            $this->addExtension($extension);
        }
    }

    private function addDefaultExtensions(): void
    {
        $this->addExtension(new \Oroboros\core\library\template\twig\TwigExtension());
    }
}
