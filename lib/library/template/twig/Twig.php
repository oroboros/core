<?php
namespace Oroboros\core\library\template\twig;

/**
 * Wrapper for the Twig Template Engine
 *
 * @author Brian Dayhoff
 * @see https://twig.symfony.com/doc/2.x/api.html
 */
final class Twig extends \Oroboros\core\abstracts\library\template\AbstractTemplate implements \Oroboros\core\interfaces\CoreInterface
{

    /**
     * Defines Twig as the template engine
     */
    const TEMPLATE_ENGINE = 'twig';

    /**
     * The Twig template loader instance.
     * @var \Twig\Loader\FilesystemLoader
     */
    private static $loader = null;

    /**
     * The Twig Environment instance.
     * @var \Oroboros\core\library\template\twig\TwigEnvironment
     */
    private static $environment = null;

    public function renderTemplate()
    {
        $template_path = $this->getTemplate();
        $template = self::$environment->load($template_path . '.twig');
        $data = $this->unpackData($this->getData());
        try {
            $output = $template->render($data);
        } catch (\Exception $e) {
            d($e->getMessage(), $e->getTraceAsString(), $data, $template);
            exit;
        }
        return $output;
    }

    protected function loadTemplateEngineResources()
    {
        $this->loadTwigTemplateLoader();
    }

    protected function initializeTemplateEngine()
    {
        $this->loadTwigEnvironment();
    }

    private function unpackData($data)
    {
        $result = [];
        if (!is_iterable($data)) {
            return $data;
        }
        foreach ($data as $key => $value) {
            $result[$key] = $this->unpackData($value);
        }
        return $result;
    }

    private function loadTwigEnvironment()
    {
        if (is_null(self::$environment)) {
            // Only load the environment one time
            if ($this->hasArgument('template-environment')) {
                $environment = $this->getArgument('template-environment');
            } else {
                $environment = $this->loadDefaultTwigEnvironment();
            }
            $this->verifyTwigEnvironment($environment);
            self::$environment = $environment;
        }
    }

    private function loadDefaultTwigEnvironment()
    {
        $extensions = null;
        $modules = null;
        if ($this->hasArgument('extensions')) {
            $extensions = $this->getArgument('extensions');
        }
        if ($this->hasArgument('modules')) {
            $modules = $this->getArgument('modules');
        }
        $cache_dir = $this->getTwigTemplateCache();
        $environment = new \Oroboros\core\library\template\twig\TwigEnvironment(self::$loader, [
            'cache' => $cache_dir,
            'debug' => $this::app()->debugEnabled()
            ], $extensions, $modules);
        $environment->addExtension(new \Twig\Extension\DebugExtension());
        return $environment;
    }

    private function verifyTwigEnvironment($environment)
    {
        $expected = '\\Oroboros\\core\\library\\template\\twig\\TwigEnvironment';
    }

    private function loadTwigTemplateLoader()
    {
        if (is_null(self::$loader)) {
            // Only load the tem[late loader one time
            if ($this->hasArgument('template-loader')) {
                $loader = $this->getArgument('template-loader');
            } else {
                $loader = $this->loadDefaultTwigTemplateLoader();
            }
            $this->verifyTwigTemplateLoader($loader);
            self::$loader = $loader;
        }
    }

    private function loadDefaultTwigTemplateLoader()
    {
        $app_dir = $this::app()->getApplicationTemplateDirectory() . static::TEMPLATE_ENGINE . DIRECTORY_SEPARATOR;
        $template_dir = $this->getTwigTemplateDirectory();
        $loader = new \Twig\Loader\FilesystemLoader($app_dir);
        $loader->addPath($template_dir);
        $this->loadExtraTemplateDirectories($loader);
        return $loader;
    }

    private function verifyTwigTemplateLoader($environment)
    {
        $expected = '\\Twig\\Loader\\FilesystemLoader';
    }

    private function getTwigTemplateDirectory()
    {
        $dir = OROBOROS_CORE_TEMPLATES . static::TEMPLATE_ENGINE . DIRECTORY_SEPARATOR;
        $this->prepareDirectory($dir);
        return $dir;
    }

    private function prepareDirectory(string $dir): void
    {
        if (!file_exists($dir)) {
            // Create the template directory if it doesn't exist
            mkdir($dir);
            chmod($dir, 0770);
        }
    }

    private function loadExtraTemplateDirectories(\Twig\Loader\FilesystemLoader $loader): void
    {
        if ($this->hasArgument('extensions')) {
            $this->importPluggableTemplatePaths($this->getArgument('extensions')->toArray(), $loader);
        }
        if ($this->hasArgument('modules')) {
            $this->importPluggableTemplatePaths($this->getArgument('modules')->toArray(), $loader);
        }
    }

    private function importPluggableTemplatePaths(array $pluggables, \Twig\Loader\FilesystemLoader $loader): void
    {
        foreach ($pluggables as $key => $module) {
            foreach ($module->templates() as $path) {
                if (is_dir($path . static::TEMPLATE_ENGINE . DIRECTORY_SEPARATOR)) {
                    $loader->addPath($path . static::TEMPLATE_ENGINE . DIRECTORY_SEPARATOR);
                }
            }
        }
    }

    private function getTwigTemplateCache()
    {
        if (!$this::PACKAGE_CACHE_ROOT) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. '
                        . 'No writable cache directory detected. Please chown [%2$s] to user [%3$s:%4$s] from the command line, '
                        . 'or configure a cache directory in your environment file.', get_class($this), OROBOROS_CORE_BASE_DIRECTORY . 'cache' . DIRECTORY_SEPARATOR, $this::SYSTEM_USER, $this::SYSTEM_GROUP));
        }
        $dir = $this::PACKAGE_CACHE_ROOT . 'twig' . DIRECTORY_SEPARATOR;
        if (!file_exists($dir)) {
            // Create the template cache directory if it doesn't exist
            mkdir($dir);
            chmod($dir, 0770);
        }
        return $dir;
    }
}
