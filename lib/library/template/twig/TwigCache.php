<?php
namespace Oroboros\core\library\template\twig;

/**
 * Twig Filesystem Cache Extension
 * Fixes some filesystem errors that create difficulties for cli build operations
 *
 * @author Brian Dayhoff
 */
class TwigCache extends \Twig\Cache\FilesystemCache
{

    public function write(string $key, string $content): void
    {
        parent::write($key, $content);
        // Insure that the cache file is cli writeable, 
        // so it can be removed in build
        chmod($key, 0770);
        // Insure that the cache subdirectory is cli writeable, 
        // so it can be removed in build
        $dirname = dirname($key);
        chmod($dirname, 0770);
    }
}
