<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\library\cache;

/**
 * Null Cache
 * Provides a nullified non-caching layer compatible with Psr-6.
 * This can be used whenever caching is off or not configured,
 * so all objects have a potential cache implementation where appropriate,
 * and will always have a valid cache object which just tells them the cache
 * is always stale.
 *
 * @author Brian Dayhoff
 */
final class NullCache extends \Oroboros\core\abstracts\library\cache\AbstractCacheItemPool implements \Oroboros\core\interfaces\CoreInterface
{

    const CACHE_ITEM_CLASS = 'cache\\NullCacheItem';
    const CONNECTION_CLASS = 'cache\\NullConnection';

}
