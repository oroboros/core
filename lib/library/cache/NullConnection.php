<?php
/*
 * The MIT License
 *
 * Copyright 2019 mopsyd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\library\cache;

/**
 * Null Connection
 * A compatible connection object that does nothing.
 * Use for null caches or cache item pools that do not require a connection
 *
 * @author mopsyd
 */
final class NullConnection extends \Oroboros\core\abstracts\library\cache\AbstractConnection implements \Oroboros\core\interfaces\CoreInterface
{

    public function connect()
    {
        return $this;
    }

    public function disconnect()
    {
        return $this;
    }

    protected function loadConnection(string $key = null)
    {
        ;
    }
}
