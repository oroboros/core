<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\library\menu\login;

/**
 * Frontend Menu Definer
 * Defines the frontend navigation menu data.
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
final class MenuDefiner extends \Oroboros\core\abstracts\library\menu\AbstractMenuDefiner implements \Oroboros\core\interfaces\CoreInterface
{

    use \Oroboros\core\traits\definer\JsonDefinerTrait {
        \Oroboros\core\traits\definer\JsonDefinerTrait::declareDataSet as private traitDeclareDataSet;
    }

    /**
     * Path to the definer definition config, relative to the application root.
     */
    const DEFINER_FILE = 'etc/menus/frontend/menu.json';

    /**
     * Preflights class constant declaration prior to calling parent constructor
     * @param string $command
     * @param array $arguments
     * @param array $flags
     */
    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        $this->validateDefinerFile();
        parent::__construct($command, $arguments, $flags);
    }
    
    /**
     * Updates the declared data set to include any additional menu elements
     * based on user login status or module menus that should appear in the primary navigation.
     * 
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    protected function declareDataSet(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $data = $this->traitDeclareDataSet();
        if ($this->hasArgument('user')) {
            $data = $this->appendUserSpecificLinks($data);
        }
        if ($this->hasArgument('modules')) {
            $data = $this->appendModuleSpecificLinks($data);
        }
        return $data;
    }
    
    /**
     * Makes any menu corrections required for user login status.
     * 
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $data
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    protected function appendUserSpecificLinks(\Oroboros\core\interfaces\library\container\ContainerInterface $data): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        return $data;
    }
    
    /**
     * Adds any top-level link menus added by modules.
     * 
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $data
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    protected function appendModuleSpecificLinks(\Oroboros\core\interfaces\library\container\ContainerInterface $data): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        return $data;
    }
}
