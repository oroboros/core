<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\library\menu;

/**
 * Frontend Account Notifications Menu Definer
 * Generates the user notification menu for the frontend
 * 
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
final class AccountNotificationsMenuDefiner extends \Oroboros\core\abstracts\library\menu\AbstractMenuDefiner implements \Oroboros\core\interfaces\CoreInterface
{

    use \Oroboros\core\traits\definer\JsonDefinerTrait {
        \Oroboros\core\traits\definer\JsonDefinerTrait::declareDataSet as private traitDeclareDataSet;
    }

    /**
     * Path to the definer definition config, relative to the application root.
     */
    const DEFINER_FILE = 'etc/menus/ui-notifications.json';

    /**
     * Preflights class constant declaration prior to calling parent constructor
     * @param string $command
     * @param array $arguments
     * @param array $flags
     */
    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        $this->validateDefinerFile();
        parent::__construct($command, $arguments, $flags);
    }
    
    /**
     * Updates the declared data set to include any additional menu elements
     * based on user login status or module menus that should appear in the primary navigation.
     * 
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    protected function declareDataSet(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $data = $this->traitDeclareDataSet();
        if ($this->hasArgument('user')) {
            $data = $this->appendUserSpecificLinks($data);
        }
        if ($this->hasArgument('modules')) {
            $data = $this->appendModuleSpecificLinks($data);
        }
        $messages = $data['provide']['account-notifications'];
        $messages = $this->initializeRootNotifications($messages);
        $messages = $this->initializeSystemNotifications($messages);
        $messages = $this->initializeCoreNotifications($messages);
        $messages = $this->initializeApplicationNotifications($messages);
        $messages = $this->initializeEnvironmentNotifications($messages);
        $messages = $this->initializeActivityNotifications($messages);
        $messages = $this->initializeMessageNotifications($messages);
        $messages = $this->initializeAccountNotifications($messages);
        $messages = $this->initializeSecurityNotifications($messages);
        foreach ($this::user()->details()->notifications as $key => $category) {
            foreach ($category as $id => $notification) {
                d($key, $id, $notification);
            }
        }
        $provide = $data['provide'];
        $provide['account-notifications'] = $messages;
        $data['provide'] = $provide;
//        d($data, $messages, $this::user()->details()->notifications); exit;
        return $data;
    }
    
    /**
     * Makes any menu corrections required for user login status.
     * 
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $data
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    protected function appendUserSpecificLinks(\Oroboros\core\interfaces\library\container\ContainerInterface $data): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        return $data;
    }
    
    /**
     * Adds any top-level link menus added by modules.
     * 
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $data
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    protected function appendModuleSpecificLinks(\Oroboros\core\interfaces\library\container\ContainerInterface $data): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        return $data;
    }
    
    private function initializeRootNotifications(iterable $messages): iterable
    {
        if (!$this->user()->can('*')) {
            // Nope.
            return $messages;
        }
        if ($this::user()->identity()->id() === 'root') {
            $credentials = \Oroboros\core\library\auth\Credentials::init(null, [
                'identity' => $this::user()->identity(),
                'password' => $this->load('library', \Oroboros\core\library\auth\LoginPassword::class, \Oroboros\core\library\user\RootUserIdentity::DEFAULT_PASSWORD),
            ]);
            if ($this::user()->identity()->authenticate($credentials)) {
                // Default root password. Nag to reset.
                $message = [
                    'id' => 'reset-root-password',
                    'permission' => '*',        // Only visible to root
                    'href' => '/user/security/',
                    'title' => 'Reset Root Password',
                    'date' => 'Just now',
                    'icon' => [
                        'color' => 'danger',
                        'icon' => 'fas fa-exclamation-triangle',
                    ],
                    'content' => 'You are using the root login password. This needs to be reset to a more secure value.',
                    'locked' => true,           // This cannot be marked as read
                    'dismissable' => false,     // This cannot be dismissed
                    'priority' => PHP_INT_MIN,  // This will always show first
                    'type' => 'notification',   // This will always show in notifications first if applicable
                ];
                $messages[$message['id']] = $message;
            }
        }
        return $messages;
    }
    
    private function initializeCoreNotifications(iterable $messages): iterable
    {
        if (!$this->user()->can('notifications.core.view')) {
            // Nope.
            return $messages;
        }
        return $messages;
    }
    
    private function initializeApplicationNotifications(iterable $messages): iterable
    {
        if (!$this->user()->can('notifications.application.view')) {
            // Nope.
            return $messages;
        }
        return $messages;
    }
    
    private function initializeSystemNotifications(iterable $messages): iterable 
    {
        if (!$this->user()->can('notifications.system.view')) {
            // Nope.
            return $messages;
        }
        
        return $messages;
    }
    
    private function initializeEnvironmentNotifications(iterable $messages): iterable
    {
        if (!$this->user()->can('notifications.environment.view')) {
            // Nope.
            return $messages;
        }
        
        return $messages;
    }
    
    private function initializeActivityNotifications(iterable $messages): iterable
    {
        if (!$this->user()->can('notifications.activity.view')) {
            // Nope.
            return $messages;
        }
        
        return $messages;
    }
    
    private function initializeMessageNotifications(iterable $messages): iterable
    {
        if (!$this->user()->can('notifications.message.view')) {
            // Nope.
            return $messages;
        }
        
        return $messages;
    }
    
    private function initializeAccountNotifications(iterable $messages): iterable
    {
        if (!$this->user()->can('notifications.account.view')) {
            // Nope.
            return $messages;
        }
        
        return $messages;
    }
    
    private function initializeSecurityNotifications(iterable $messages): iterable
    {
        if (!$this->user()->can('notifications.security.view')) {
            // Nope.
            return $messages;
        }
        
        return $messages;
    }
}
