<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\library\hook;

/**
 * Data Hook Set
 * A PSR-11 container that transforms key/value pairs into DataHook objects,
 * where the key is the name of the hook, and the value is the replacement
 * value to inject into the hook.
 * 
 * Data hooks replace the match entirely with the data.
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
final class DataHookSet extends \Oroboros\core\abstracts\library\hook\AbstractHookSet implements \Oroboros\core\interfaces\CoreInterface
{

    /**
     * Designates to generate data hooks
     */
    const HOOK_CLASS = 'hook\\DataHook';

    /**
     * Fires on the container key setter. Allows only string keys,
     * and values may be null, scalar, arrays, or objects.
     *
     * @param scalar|stringable $key The passed key that the setter is supposed to have
     * @param mixed $value The value passed for the setter
     * @param string $method The internal method that the operation arose from.
     * @return void
     * @tbrows \Oroboros\core\exception\container\ContainerException if the key is not one of null, scalar, array, or object
     */
    protected function onSet($key, $value, $method = null)
    {
        try {
            parent::onSet($key, $value, $method);
        } catch (\Oroboros\core\exception\container\ContainerException $e) {
            // Non-scalar value
            if (is_array($value) || is_object($value)) {
                return;
            }
            throw new \Oroboros\core\exception\container\ContainerException(
                    sprintf('Error encountered in [%1$s]. Provided value not understood.', get_class($this))
            );
        }
    }
}
