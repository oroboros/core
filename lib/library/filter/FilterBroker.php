<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\library\filter;

/**
 * Default Filter Broker
 * Provides default filters, as defined by the application.
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
final class FilterBroker extends \Oroboros\core\abstracts\library\filter\AbstractFilterBroker implements \Oroboros\core\interfaces\CoreInterface
{

    /**
     * Default filter data is only pulled one time.
     * 
     * @var \Oroboros\core\interfaces\container\ContainerInterface
     */
    private static $default_filters = null;

    /**
     * Provides the application config filters.json details if one exists,
     * otherwise a null set.
     * 
     * This will always compile clean.
     * 
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    protected function declareDataSet(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        if (!is_null(self::$default_filters)) {
            return self::$default_filters;
        }
        $filename = $this::app()->getApplicationRoot() . 'config/filters.json';
        if (is_readable($filename)) {
            $data = $this->load('library', 'parser\\JsonParser', $filename)->fetch();
            self::$default_filters = $this->containerize(['defaults' => $data->toArray()]);
            return self::$default_filters;
        }
        self::$default_filters = parent::declareDataSet();
        return self::$default_filters;
    }
}
