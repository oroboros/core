<?php
namespace Oroboros\core\library\parser\cli;

/**
 * Parses markdown into command line plaintext format
 *
 * @author Brian Dayhoff
 */
final class MarkdownParser extends \Oroboros\core\abstracts\library\parser\AbstractParser implements \Oroboros\core\interfaces\CoreInterface
{

    use \Oroboros\core\traits\cli\CliOutputUtilityTrait;

    /**
     * Sets the scope of the parser to markdown html format
     */
    const PARSER_FORMAT = 'md';

    private static $regex = [
        'reference' => '^\[([\x00-\x7F]+)\][\s]{0,}\:[\s]{0,}([\x00-\x7F]+)',
        'bold' => '\*\*([\x00-\x7F]+)\*\*|\_\_([\x00-\x7F]+)\_\_',
        'italic' => '\*([\x00-\x7F]+)\*|\_([\x00-\x7F]+)\_',
        'inline-code' => '[\`]([\x00-\x7F][^`\r\n]+)[\`]',
        'underline' => '',
        'strikethrough' => '\~\~([\x00-\x7F]+)\~\~',
        'ordered-list' => '',
        'unordered-list' => '',
        'title-1' => '',
        'title-2' => '',
        'title-3' => '',
        'title-4' => '',
        'title-5' => '',
        'title-6' => '',
        'blockquote' => '',
        'codeblock' => '',
        'code' => '',
        'horizontal-rule' => '',
        'line-break' => '',
        'link' => '\[([^\[\]]*)\]\((.*?)\)',
        'url' => '',
        'reference-link' => '',
        'image' => '',
        'image-link' => '',
        'tasklist' => '',
    ];
    private $references = [];
    private $blocks = [];
    private $data = [];
    private $raw = null;
    private $domain = '';
    private $link_base = '';
    private $terminal_columns = 80;
    private $terminal_lines = 24;

    /**
     * 
     * @param string $command Represents the file path of the given file.
     * @param array $args
     * @param array $flags Flags to pass to the constructor
     * @throws OutOfBoundsException If the given file ($command) does not exist or is not readable.
     *  This exception should be handled upstream.
     */
    public function __construct(string $command = null, array $args = null, array $flags = null)
    {
        if (is_null($args)) {
            $args = [];
        }
        $this->initializeIOStreams();
        $this->initializeCustomArgs($args);
        parent::__construct($command, $args, $flags);
    }

    /**
     * Parses the file path given to the parser.
     * Should throw an \OutOfBoundsException if the file is missing
     * or incorrectly formatted for the given parser.
     * 
     * If the file can be parsed correctly,
     * this method should return an array of the parsed file data.
     * @return array
     * @throws OutOfBoundsException if the file cannot resolve to a valid data set for any reason
     */
    protected function parseFile(string $file_location)
    {
        if (!file_exists($file_location) || !is_readable($file_location)) {
            throw new \OutOfBoundsException(sprintf('Error encountered in [%1$s] parser class [%2$s]. Provided file [%3$s] could not be parsed as valid markdown.', get_class($this), static::PARSER_FORMAT, $file_location));
        }
        $raw = file_get_contents($file_location);
        $this->raw = $raw;
        $data = $this->evaluateMarkdown($raw);
        return ['output' => rtrim(implode(PHP_EOL, $this->data), PHP_EOL) . PHP_EOL];
    }

    protected function generateMarkdownBlocks(string $raw)
    {
        $blocks = explode(PHP_EOL . PHP_EOL, $raw);
        foreach ($blocks as $key => $block) {
            $block .= PHP_EOL;
        }
        $this->blocks = $blocks;
    }

    protected function renderBlock(string $block)
    {
        // Perform block-specific tasks here

        $block = $this->insertReferences($block);

        // Perform line-specific tasks here
        $data = explode(PHP_EOL, $block);
        foreach ($data as $line) {
            $this->data[] = $this->evaluateLine($line);
        }
//        $this->data[] = PHP_EOL;
    }

    protected function evaluateMarkdown(string $data)
    {
        $data = $this->getReferences($data);
        $this->generateMarkdownBlocks($data);
        foreach ($this->blocks as $key => $block) {
            $this->renderBlock($block);
        }
//        s($data, $this->data);
//        exit(sprintf('Exit breakpoint at line [%1$s] of file [%2$s]' . PHP_EOL, __LINE__, __FILE__));
    }

    private function evaluateLine(string $line)
    {
        $line = $this->evaluateLineTitles($line);
        $line = $this->evaluateLineTextFormatting($line);
        $line = $this->evaluateLineLinks($line);
        return $line . $this->getFormatCode('reset');
    }

    private function evaluateLineTitles(string $line): string
    {
        if (strpos($line, '###### ') !== false) {
            // H5 Heading
            $line = str_replace('###### ', $this->getFormatCode('bold'), $line) . PHP_EOL;
        }
        if (strpos($line, '##### ') !== false) {
            // H5 Heading
            $line = str_replace('##### ', $this->getFormatCode('bold'), $line) . PHP_EOL;
        }
        if (strpos($line, '#### ') !== false) {
            // H4 Heading
            $line = str_replace('#### ', $this->getFormatCode('bold'), $line) . PHP_EOL;
        }
        if (strpos($line, '### ') !== false) {
            // H3 Heading
            $line = str_replace('### ', $this->getFormatCode('bold'), $line) . PHP_EOL;
        }
        if (strpos($line, '## ') !== false) {
            // H2 Heading
            $line = str_replace('## ', $this->getFormatCode('bold') . $this->getFormatCode('underline'), $line) . PHP_EOL;
        }
        if (strpos($line, '# ') !== false) {
            // H1 Heading
            $line = str_replace('# ', $this->getFormatCode('red') . $this->getFormatCode('bold'), $line) . PHP_EOL;
        }
        return $line;
    }

    /**
     * Generates a fully qualified link based on the supplied domain
     * and path prefixes
     * @param string $link
     * @return string
     */
    private function generateLink(string $link): string
    {
        if (strpos($link, 'http') !== 0) {
            // Link needs to be formatted
            if (strpos($link, '/') !== 0) {
                // Link is relative
                $link = $this->getLinkBase() . $link;
            }
            $link = $this->getDomain() . $link;
        }
        return $link;
    }

    /**
     * Formats reference links to output on the terminal legibly
     * with correct hyperlinking.
     * 
     * @param string $line The line to evaluate
     * @return string The terminal formatted link
     */
    private function matchReferenceLinks(string $line): string
    {
        $regex = $this->getExpression('link');
        $matches = [];
        preg_match_all($regex, $line, $matches);
        if (empty($matches[0])) {
            return $line;
        }
        foreach ($matches[0] as $key => $match) {
            $replace = $match;
            $text = $matches[1][$key];
            $link = $this->generateLink($matches[2][$key]);
            $formatted = $this->getFormatCode('yellow') . $this->make_http_link($text, $link);
            $line = str_replace($match, $formatted, $line);
        }
        $line = str_replace(PHP_EOL, null, $line);
        return $line;
    }

    /**
     * Formats links to output on the terminal legibly with correct hyperlinking.
     * 
     * This DOES NOT render links that have referential links listed at the end 
     * of the document.
     * @param string $line The line to evaluate
     * @return string The terminal formatted link
     */
    private function matchFullLinks(string $line): string
    {
        $regex = $this->getExpression('link');
        $matches = [];
        preg_match_all($regex, $line, $matches);
        if (empty($matches[0])) {
            return $line;
        }
        foreach ($matches[0] as $key => $match) {
            $replace = $match;
            $text = $matches[1][$key];
            $link = $this->generateLink($matches[2][$key]);
            $formatted = $this->getFormatCode('yellow') . $this->make_http_link($text, $link) . $this->getFormatCode('reset');
            $line = str_replace($match, $formatted, $line);
        }
        return $line;
    }

    private function matchStrikethrough(string $line): string
    {
        $regex = $this->getExpression('strikethrough');
        $matches = [];
        preg_match_all($regex, $line, $matches);
        if (empty($matches[0])) {
            return $line;
        }
        foreach ($matches[0] as $key => $match) {
            $replace = $match;
            $text = trim($match, '~');
            $formatted = $this->getFormatCode('strikethrough') . $text . $this->getFormatCode('reset');
            $line = str_replace($match, $formatted, $line);
        }
        return $line;
    }

    private function matchUnderline(string $line): string
    {
        return $line;
    }

    private function matchBold(string $line): string
    {
        $regex = $this->getExpression('bold');
        $matches = [];
        preg_match_all($regex, $line, $matches);
        if (empty($matches[0])) {
            return $line;
        }
        foreach ($matches[0] as $key => $match) {
            $replace = $match;
            $text = trim($match, '*_');
            $formatted = $this->getFormatCode('bold') . $text . $this->getFormatCode('reset');
            $line = str_replace($match, $formatted, $line);
        }
        return $line;
    }

    private function matchItalic(string $line): string
    {
        $regex = $this->getExpression('italic');
        $matches = [];
        preg_match_all($regex, $line, $matches);
        if (empty($matches[0])) {
            return $line;
        }
        foreach ($matches[0] as $key => $match) {
            $replace = $match;
            $text = trim($match, '*_');
            $formatted = $this->getFormatCode('italic') . $text . $this->getFormatCode('reset');
            $line = str_replace($match, $formatted, $line);
        }
        return $line;
    }

    private function matchInlineCode(string $line): string
    {
        $regex = $this->getExpression('inline-code');
        $matches = [];
        preg_match_all($regex, $line, $matches);
        if (empty($matches[0])) {
            return $line;
        }
        foreach ($matches[0] as $key => $match) {
            $replace = $match;
            $text = trim($match, '`');
            $formatted = $this->getFormatCode('cyan') . $text . $this->getFormatCode('reset');
            $line = str_replace($match, $formatted, $line);
        }
        return $line;
    }

    /**
     * Returns the relative link base supplied by the referencing object, if any
     * @return string
     */
    private function getLinkBase(): string
    {
        return $this->link_base;
    }

    /**
     * Returns the domain supplied by the referencing object, if any
     * @return string
     */
    private function getDomain(): string
    {
        return $this->domain;
    }

    /**
     * Formats the supplied line to perform link-related parsing operations
     * @param string $line
     * @return string
     */
    private function evaluateLineLinks(string $line): string
    {
        $line = $this->matchFullLinks($line);
        return $line;
    }

    /**
     * Formats the supplied line to perform text formatting operations
     * @param string $line
     * @return string
     */
    private function evaluateLineTextFormatting(string $line): string
    {
        $line = $this->matchInlineCode($line);
        $line = $this->matchBold($line);
        $line = $this->matchItalic($line);
        $line = $this->matchUnderline($line);
        $line = $this->matchStrikethrough($line);
        return $line;
    }

    /**
     * Returns a regular expression to search for specific pattern matches
     * @param string $key The regex key
     * @return string The resulting regex
     * @throws \InvalidArgumentException if the supplied regex key is not known
     */
    private function getExpression(string $key): string
    {
        if (!array_key_exists($key, self::$regex)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [$1%s]. Provided key [%2$s] is not a known regular expression.', get_class($this), $key));
        }
        return sprintf('@%1$s@', self::$regex[$key]);
    }

    private function initializeCustomArgs(array $arguments)
    {
        if (array_key_exists('columns', $arguments)) {
            $this->terminal_columns = $arguments['columns'];
        }
        if (array_key_exists('lines', $arguments)) {
            $this->terminal_lines = $arguments['lines'];
        }
        if (array_key_exists('domain', $arguments)) {
            $this->domain = $arguments['domain'];
        }
        if (array_key_exists('link-base', $arguments)) {
            $this->link_base = $arguments['link-base'];
        }
    }

    private function getReferences(string $block): string
    {
        $regex = $this->getExpression('reference');
        $lines = explode(PHP_EOL, $block);
        $matches = [];
        foreach ($lines as $line) {
            if (preg_match($regex, $line) === 1) {
                $key = trim(substr($line, 0, strpos($line, ']')), '[] ');
                $val = trim(substr($line, strpos($line, ']:') + 2), ' ' . PHP_EOL);
                $matches[$key] = $val;
                $block = str_replace($line, null, $block);
            }
        }
        $this->references = $matches;
        return $block;
    }

    private function insertReferences(string $block)
    {
        foreach ($this->references as $key => $reference) {
            $pos = strpos($block, '(' . $key . ')');
            if ($pos !== false) {
                $block = str_replace('(' . $key . ')', '(' . $reference . ')', $block);
            }
        }
        return $block;
    }
}
