<?php
namespace Oroboros\core\library\parser;

/**
 * Parses CSV data and allows the object to be used as an iterable representation of the data set
 *
 * @author Brian Dayhoff
 */
final class CsvParser extends \Oroboros\core\abstracts\library\parser\AbstractParser implements \Oroboros\core\interfaces\CoreInterface
{

    /**
     * Represents the field headers, if present
     * @var array
     */
    private $field_headers = [];

    /**
     * Represents whether or not headers are defined
     * @var bool
     */
    private $has_headers = false;

    /**
     * Sets the scope of the parser to CSV format
     */
    const PARSER_FORMAT = 'csv';

    /**
     * 
     * @param string $command Represents the file path of the given file.
     * @param array $arguments
     * @param array $flags Flags to pass to the constructor
     *  Valid: [(bool) 'headers': Determines if the first line of the CSV contains column headers]
     * @throws OutOfBoundsException If the given file ($command) does not exist or is not readable.
     *  This exception should be handled upstream.
     */
    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
    }

    /**
     * Determines whether headers exist or not for the current data set.
     * @return bool
     */
    public function hasHeaders()
    {
        return $this->has_headers;
    }

    /**
     * Returns false if there are no headers. Otherwise returns an indexed array
     * of headers ordered by the ordinal position of the column
     * @return bool|array
     */
    public function getHeaders()
    {
        if (!$this->has_headers) {
            return false;
        }
        return $this->field_headers;
    }

    /**
     * Parses the file path given to the parser.
     * Should throw an \OutOfBoundsException if the file is missing
     * or incorrectly formatted for the given parser.
     * 
     * If the file can be parsed correctly,
     * this method should return an array of the parsed file data.
     * @return array
     * @throws OutOfBoundsException if the file cannot resolve to a valid data set for any reason
     */
    protected function parseFile(string $file_location)
    {
        $field_raw = [];
        $field_headers = [];
        $use_field_headers = $this->hasFlag('headers');
        $fh = fopen($file_location, 'r');
        if ($use_field_headers) {
            $field_headers = fgetcsv($fh);
            $this->field_headers = $field_headers;
        }
        while (!feof($fh)) {
            $tmp = fgetcsv($fh);
            if (!is_array($tmp)) {
                // End of file line, do not use.
                continue;
            }
            if ($use_field_headers) {
                $tmp = $this->formatRowWithHeaders($tmp);
            }
            $field_raw[] = $tmp;
        }
        return $field_raw;
    }

    /**
     * Applies the field header keys to the array, and returns the associative result.
     * @param array $row
     * @return array
     */
    private function formatRowWithHeaders(array $row)
    {
        $formatted = [];
        foreach ($row as $key => $value) {
            $formatted[$this->field_headers[$key]] = $value;
        }
        return $formatted;
    }
}
