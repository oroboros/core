<?php
namespace Oroboros\core\library\parser;

/**
 * Parses JSON data and allows the object to be used as an iterable representation of the data set
 *
 * @author Brian Dayhoff
 */
final class JsonParser extends \Oroboros\core\abstracts\library\parser\AbstractParser implements \Oroboros\core\interfaces\CoreInterface
{

    /**
     * Sets the scope of the parser to CSV format
     */
    const PARSER_FORMAT = 'json';

    /**
     * 
     * @param string $command Represents the file path of the given file.
     * @param array $arguments
     * @param array $flags Flags to pass to the constructor
     *  Valid: [(bool) 'headers': Determines if the first line of the CSV contains column headers]
     * @throws OutOfBoundsException If the given file ($command) does not exist or is not readable.
     *  This exception should be handled upstream.
     */
    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
    }

    /**
     * Parses the file path given to the parser.
     * Should throw an \OutOfBoundsException if the file is missing
     * or incorrectly formatted for the given parser.
     * 
     * If the file can be parsed correctly,
     * this method should return an array of the parsed file data.
     * @return array
     * @throws OutOfBoundsException if the file cannot resolve to a valid data set for any reason
     */
    protected function parseFile(string $file_location)
    {
        $raw = file_get_contents($file_location);
        $data = json_decode($raw, true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new \OutOfBoundsException(sprintf('Error encountered in [%1$s] parser class [%2$s]. Provided file [%3$s] could not be parsed as valid json.', get_class($this), static::PARSER_FORMAT, $file_location));
        }
        return $data;
    }
}
