<?php
namespace Oroboros\core\library\parser\html;

/**
 * Parses markdown into html
 *
 * @author Brian Dayhoff
 */
final class MarkdownParser extends \Oroboros\core\abstracts\library\parser\AbstractParser implements \Oroboros\core\interfaces\CoreInterface
{

    /**
     * Sets the scope of the parser to markdown html format
     */
    const PARSER_FORMAT = 'md';

    /**
     * HTML markdown parser library
     * @var \Parsedown
     */
    private static $parser = null;

    /**
     * 
     * @param string $command Represents the file path of the given file.
     * @param array $arguments
     * @param array $flags Flags to pass to the constructor
     * @throws OutOfBoundsException If the given file ($command) does not exist or is not readable.
     *  This exception should be handled upstream.
     */
    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        $this->initializeParser();
        parent::__construct($command, $arguments, $flags);
    }

    /**
     * Parses the file path given to the parser.
     * Should throw an \OutOfBoundsException if the file is missing
     * or incorrectly formatted for the given parser.
     * 
     * If the file can be parsed correctly,
     * this method should return an array of the parsed file data.
     * @return array
     * @throws OutOfBoundsException if the file cannot resolve to a valid data set for any reason
     */
    protected function parseFile(string $file_location)
    {
        if (!file_exists($file_location) || !is_readable($file_location)) {
            throw new \OutOfBoundsException(sprintf('Error encountered in [%1$s] parser class [%2$s]. Provided file [%3$s] could not be parsed as valid markdown.', get_class($this), static::PARSER_FORMAT, $file_location));
        }
        $raw = file_get_contents($file_location);
        $data = $this->getParser()->parse($raw);
        return ['output' => $data];
    }

    /**
     * Returns the internal parser object
     * @return \Parsedown
     */
    protected function getParser()
    {
        return self::$parser;
    }

    /**
     * Initializes the internal parser object if it does not already exist.
     * @return void
     */
    private function initializeParser()
    {
        if (is_null(self::$parser)) {
            self::$parser = new \ParsedownExtra();
        }
    }
}
