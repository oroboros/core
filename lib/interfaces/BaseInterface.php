<?php
namespace Oroboros\core\interfaces;

/**
 * Base interface that designates child implementors are part of Oroboros.
 * 
 * @author Brian Dayhoff
 */
interface BaseInterface
{
    // no-op
}
