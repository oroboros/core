<?php
namespace Oroboros\core\interfaces\model;

/**
 * Provides a contract for standardized operation of models.
 * 
 * @author Brian Dayhoff
 */
interface ModelInterface extends \Oroboros\core\interfaces\BaseInterface, \Oroboros\core\interfaces\environment\EnvironmentInterface, \Oroboros\core\interfaces\pattern\command\CommandSubjectInterface, \Oroboros\core\interfaces\library\event\EventAwareInterface, \Oroboros\core\interfaces\library\filter\FilterAwareInterface
{

    /**
     * Oroboros models always use the standard constructor
     * 
     * @param string $command per implementation
     * @param array $arguments per implementation
     * @param array $flags per implementation
     * 
     * @throws \Oroboros\core\exception\model\MissingCredentialsException
     *         If credentials are required but have not been supplied
     * @throws \Oroboros\core\exception\model\InvalidCredentialsException
     *         If the supplied credentials are not valid for the
     *         given connection requested
     * @throws \Oroboros\core\exception\model\MissingResourceException
     *         If the underlying data source is not reachable.
     *         This could be a database that is not enabled or blocked by
     *         network rules, a missing file, an unloadable object,
     *         or an unreachable api
     */
    public function __construct(string $command = null, array $arguments = null, array $flags = null);

    /**
     * Returns all data within the column scope given, or all data if no column is given
     * 
     * @param array $columns
     * @param array $order_by
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     * @throws \Oroboros\core\exception\ErrorException
     */
    public function pull(array $columns = null, array $order_by = null): \Oroboros\core\interfaces\library\data\DataContainerInterface;

    /**
     * Returns the value associated with the given column name for the currently scoped record.
     * @param string $key
     * @return mixed
     * @throws \InvalidArgumentException if the given key does not correspond to a known column.
     * @throws \ErrorException If the model is not currently scoped to a record.
     *     This may occur if it was not initially scoped, or if it has previously been deleted.
     */
    public function get(string $key);

    /**
     * Sets the value of the given column name to the given value for the currently scoped record.
     * @param string $key
     * @param type $value
     * @return $this (method chainable)
     * @throws \InvalidArgumentException If the given value is not
     *     appropriate for the specified column and cannot be updated.
     *     This indicates either a nonexistent column, a value type mismatch,
     *     or a failing foreign key constraint.
     * @throws \ErrorException If the model is not currently scoped to a record.
     *     This may occur if it was not initially scoped, or if it has previously been deleted.
     * @todo Cascade updates to corresponding models with foreign keys set to cascade
     *     that would be affected by the update.
     */
    public function set(string $key, $value): \Oroboros\core\interfaces\model\ModelInterface;

    /**
     * Inserts a new row into the database, and scopes the model object to it.
     * Will return a clone of the model scoped to the new record, leaving the
     * original object still usable in its original context.
     * @param array $details Associative array with key: column name, value: column insert value
     * @return \Oroboros\core\interfaces\model\ModelInterface
     *     Will return a clone of the current object scoped to the new record.
     * @throws \ErrorException If a new record could not be created with the given details.
     */
    public function create(array $details): \Oroboros\core\interfaces\model\ModelInterface;
    
    /**
     * Deletes a key or property from the model if it can be deleted
     * 
     * if no key is passed, deletes the entire dataset.
     * 
     * @param string $key
     * @return \Oroboros\core\interfaces\model\ModelInterface
     * @throws \Oroboros\core\exception\model\ReadonlyException
     *         if the data source is readonly,
     *         if a key constraint fails, or any
     *         other criteria prevents deletion.
     */
    public function delete(string $key = null): \Oroboros\core\interfaces\model\ModelInterface;
    
    /**
     * Commits any data changes back into data storage
     * This should be called in the destructor of the model,
     * and may also be called externally at any time to
     * force a record update.
     * 
     * @return \Oroboros\core\interfaces\model\ModelInterface
     */
    public function commit(): \Oroboros\core\interfaces\model\ModelInterface;

    /**
     * Returns a container of values keyed by column name,
     * with values corresponding to the data type of the associated column.
     * 
     * This can be used for type verification of insert values.
     * 
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    public function getKeys(): \Oroboros\core\interfaces\library\data\DataContainerInterface;

    /**
     * Returns a boolean determination as to whether a given
     * column name exists within the scope of the current model.
     * @param string $key
     * @return bool
     */
    public function hasKey(string $key): bool;
}
