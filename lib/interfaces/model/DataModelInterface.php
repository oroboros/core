<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\interfaces\model;

/**
 * Data Model Interface
 * Defines methods required of Data Models
 * 
 * A data model follows the typical model convention
 * of operating against a known datastore or database.
 * It abstracts the implementation details of the data layer
 * and provides a consistent model api for interaction with it.
 * 
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
interface DataModelInterface extends ModelInterface
{

    /**
     * Deletes the associated record corresponding to the model object, and purges its values from memory.
     * The model object should then be either expired or re-initialized to a new record.
     * @return $this (method chainable)
     * @throws \ErrorException If the model is not currently scoped to a record.
     *     This may occur if it was not initially scoped,
     *     if it has previously been deleted,
     *     if a database connection could not be established,
     *     or if a key constraint prevents deleting the row.
     */
    public function delete(string $key = null): \Oroboros\core\interfaces\model\ModelInterface;

    /**
     * Releases the binding to the underlying database record,
     * restoring the model to its default unscoped state.
     * The scoped record is not modified or deleted.
     * The model object should be either expired or rescoped
     * to a new record after calling this method.
     * Has no effect if the record is not currently scoped.
     * @return $this (method chainable)
     */
    public function flush(): \Oroboros\core\interfaces\model\ModelInterface;

    /**
     * Scopes the model object to the first row corresponding to the given column values.
     * For best results, distinctly identifying column values should be supplied.
     * Only the first row of a multiple set will be considered.
     * @param array $columns Associative array corresponding to key: column name, value: column value.
     *     These details must isolate the expected row.
     *     Primary keys and unique constraint columns are preferable for isolation.
     * @return $this (method chainable)
     * @throws \InvalidArgumentException If no keys were provided,
     *     or no rows were returned with the given provided keys
     * @throws \ErrorException If a query error occurred due to malformed syntax from
     *     improper parameters or if the database cannot be reached.
     */
    public function lookup(array $columns): \Oroboros\core\interfaces\model\ModelInterface;

    /**
     * Batches several new rows into the corresponding table,
     * and returns a container of model objects scoped to each new row.
     * @param array $rows a multidimensional array. Values should be
     *     associative arrays where the key is the column name and the value
     *     is the new value for that column. Top level array may be either
     *     indexed or associative. Return keys of the container will correspond
     *     to string cast equivalent of the top level array keys provided.
     * @param bool $break_on_error (optional). If true will halt batch inserts
     *     if an error is encountered. If false, will skip errors and continue
     *     the batch for any subsequent rows. Default true.
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     * @throws \ErrorException If a new record could not be created with the given details
     *     for any given sub-array. Note that this may result in incomplete inserts.
     */
    public function batch(array $rows, bool $break_on_error = true): \Oroboros\core\interfaces\library\data\DataContainerInterface;
}
