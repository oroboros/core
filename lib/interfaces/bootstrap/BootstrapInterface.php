<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Oroboros\core\interfaces\bootstrap;

/**
 * Defines standard operation of bootstrap class objects
 * @author Brian Dayhoff
 */
interface BootstrapInterface extends \Oroboros\core\interfaces\BaseInterface, \Oroboros\core\interfaces\environment\EnvironmentInterface, \Oroboros\core\interfaces\pattern\observer\ObserverInterface, \Oroboros\core\interfaces\pattern\command\CommandSubjectInterface, \Oroboros\core\interfaces\library\event\EventAwareInterface, \Oroboros\core\interfaces\library\filter\FilterAwareInterface
{

    /**
     * Bootstraps the program. May only occur one time.
     */
    public function bootstrap();

    /**
     * Calls the cleanup/garbage collection process.
     */
    public function teardown();
    
    
    /**
     * Returns the currently loaded modules
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    public function getLoadedModules(): \Oroboros\core\interfaces\library\container\ContainerInterface;
    
    /**
     * Returns the currently loaded extensions
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    public function getLoadedExtensions(): \Oroboros\core\interfaces\library\container\ContainerInterface;
    
    /**
     * Returns the currently loaded services
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    public function getLoadedServices(): \Oroboros\core\interfaces\library\container\ContainerInterface;

    /**
     * Handles any errors not caught by the controller.
     * @param \Exception $error Any exception not handled by the controller.
     */
    public function handleUncaughtError(\Exception $error);
}
