<?php
namespace Oroboros\core\interfaces\exception;

/**
 * Base interface for Exceptions
 * 
 * @author Brian Dayhoff
 */
interface ExceptionInterface extends \Oroboros\core\interfaces\environment\EnvironmentInterface
{
    /**
     * Designates the class type as an exception
     */
    const CLASS_TYPE = 'exception';
}
