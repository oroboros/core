<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\interfaces\view;

/**
 * Command Line View Interface
 * Designates methods that must be available to command line views
 * 
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
interface CliViewInterface extends ViewInterface
{
    
    /**
     * Cli Views follow the Oroboros Standard Constructor format.
     * They must take the following flags and adhere to their intended functionality:
     * 
     * `quiet`: if `true`, all non-error output will be silenced.
     * `silent`: if `true`, all output, including error output will be silenced.
     * `debug`: if `true` and neither `quiet` nor `silent` are also true,
     *          prints debug messages. Otherwise debug messages are silenced.
     * `colors`: if `false`, no color codes are to be applied to output messages of any sort.
     *           This is to prevent log output from being illegible.
     * 
     * @param string $command
     * @param array $arguments
     * @param array $flags
     */
    public function __construct(string $command = null, array $arguments = null, array $flags = null);
    
    /**
     * Clears the console.
     * Returns self for method chaining.
     * 
     * @return \Oroboros\core\interfaces\view\CliViewInterface
     */
    public function clear(): \Oroboros\core\interfaces\view\CliViewInterface;
    
    /**
     * Prints a message in raw format, with the option to write to either stdout or stderr
     * If the quiet flag or silent flag were passed on view instantiation, these will not be displayed.
     * Returns self for method chaining.
     * 
     * @param string $message
     * @param string $stream
     * @return \Oroboros\core\interfaces\view\CliViewInterface
     * @throws \Oroboros\core\exception\InvalidArgumentException If an invalid stream type is specified
     */
    public function raw(string $message, string $stream = 'stdout'): \Oroboros\core\interfaces\view\CliViewInterface;
    
    /**
     * Prints a message in standard console format to stdout
     * If the quiet flag or silent flag were passed on view instantiation, these will not be displayed.
     * Returns self for method chaining.
     * 
     * @param string $message
     * @return \Oroboros\core\interfaces\view\CliViewInterface
     */
    public function print(string $message): \Oroboros\core\interfaces\view\CliViewInterface;
    
    /**
     * Prints a message indicating a successful operation in console format to stdout
     * If the quiet flag or silent flag were passed on view instantiation, these will not be displayed.
     * Returns self for method chaining.
     * 
     * @param string $message
     * @return \Oroboros\core\interfaces\view\CliViewInterface
     */
    public function success(string $message): \Oroboros\core\interfaces\view\CliViewInterface;
    
    /**
     * Prints a message in debug console format to stdout
     * If a debug flag was not passed on view instantiation, or if the quiet flag
     * or silent flag were passed on view instantiation, these will not be displayed.
     * Returns self for method chaining.
     * 
     * @param string $message
     * @return \Oroboros\core\interfaces\view\CliViewInterface
     */
    public function debug(string $message): \Oroboros\core\interfaces\view\CliViewInterface;
    
    /**
     * Prints a message in info console format to stdout
     * If the quiet flag or silent flag were passed on view instantiation, these will not be displayed.
     * Returns self for method chaining.
     * 
     * @param string $message
     * @return \Oroboros\core\interfaces\view\CliViewInterface
     */
    public function info(string $message): \Oroboros\core\interfaces\view\CliViewInterface;
    
    /**
     * Prints a message in notice console format to stdout
     * If the quiet flag or silent flag were passed on view instantiation, these will not be displayed.
     * Returns self for method chaining.
     * 
     * @param string $message
     * @return \Oroboros\core\interfaces\view\CliViewInterface
     */
    public function notice(string $message): \Oroboros\core\interfaces\view\CliViewInterface;
    
    /**
     * Prints a message in warning message in low priority error format to stderr
     * If the silent flag was passed on view instantiation, these will not be displayed.
     * They will still display if the quiet flag is passed.
     * Returns self for method chaining.
     * 
     * @param string $message
     * @return \Oroboros\core\interfaces\view\CliViewInterface
     */
    public function warning(string $message): \Oroboros\core\interfaces\view\CliViewInterface;
    
    /**
     * Prints a message in error console format to stderr
     * If the silent flag was passed on view instantiation, these will not be displayed.
     * They will still display if the quiet flag is passed.
     * Returns self for method chaining.
     * 
     * @param string $message
     * @return \Oroboros\core\interfaces\view\CliViewInterface
     */
    public function error(string $message): \Oroboros\core\interfaces\view\CliViewInterface;
    
    /**
     * Prints a message in elevated priority error console format to stderr
     * If the silent flag was passed on view instantiation, these will not be displayed.
     * They will still display if the quiet flag is passed.
     * Returns self for method chaining.
     * 
     * @param string $message
     * @return \Oroboros\core\interfaces\view\CliViewInterface
     */
    public function critical(string $message): \Oroboros\core\interfaces\view\CliViewInterface;
    
    /**
     * Prints a message in high priority error console format to stderr
     * If the silent flag was passed on view instantiation, these will not be displayed.
     * They will still display if the quiet flag is passed.
     * Returns self for method chaining.
     * 
     * @param string $message
     * @return \Oroboros\core\interfaces\view\CliViewInterface
     */
    public function alert(string $message): \Oroboros\core\interfaces\view\CliViewInterface;
    
    /**
     * Prints a message in emergency maximum priority error console format to stderr
     * If the silent flag was passed on view instantiation, these will not be displayed.
     * They will still display if the quiet flag is passed.
     * Returns self for method chaining.
     * 
     * @param string $message
     * @return \Oroboros\core\interfaces\view\CliViewInterface
     */
    public function emergency(string $message): \Oroboros\core\interfaces\view\CliViewInterface;

    /**
     * Prompts a shell user for input and returns a string representing their reply.
     * 
     * @param string $message If supplied, will print a message on the line preceding the prompt.
     * @param string $prefix Prints a prefix for the input field.
     * @param bool $secure
     * @return string
     */
    public function read(string $message = null, string $prefix = ' > ', bool $secure = false): string;

    /**
     * Returns a single keypress character.
     * Only works on unix systems.
     * 
     * @param string $message
     * @param string $prefix
     * @param bool $secure
     * @return string
     */
    public function getKey(string $message = null, string $prefix = ' > ', bool $secure = false): string;

    /**
     * Returns the keycode of a single keypress only.
     * Only works on unix systems.
     * 
     * If you wish to get the literal character of a keypress, use `getKey`.
     * If you need to track non-printing characters (eg arrow keys, enter, etc), use this method.
     * 
     * @param string $message
     * @param string $prefix
     * @param bool $secure
     * @return string
     */
    public function getKeyCode(string $message = null, string $prefix = ' > ', bool $secure = false): string;
    
    /**
     * Resolves a given keycode into the character it represents.
     * Keycodes are expected to be in string format, as if they
     * were passed in from cli input.
     * 
     * Returns the string the character resolves to, or null if it does not resolve.
     * 
     * @param string $code
     * @return string
     */
    public function resolveKeyCode(string $code): ?string;
        
}
