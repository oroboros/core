<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\interfaces\view;

/**
 * Command Line Interactive Shell View Interface
 * 
 * Designates methods that must be available to command line views that act
 * as an interactive shell for user input.
 * 
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
interface InteractiveCliViewInterface extends CliViewInterface
{
    /**
     * Takes a set of setup parameters represented in a container and creates
     * an interactive shell from them to prompt user input, validate it within
     * a specific acceptable scope, and return the result when it validates.
     * 
     * The given container MUST contain the following keys:
     * 
     * - "valid": designates what qualifies as a valid response. This uses the
     *            resolver api, any format passed that works for resolvers is valid.
     * - "keyname": designates what key the given input applies to. This is
     *              required to pass to the filter method so it can do contextual
     *              lookups based on the relevant key being processed.
     * - "method": designates what prompt method to use to obtain input.
     *             valid values are ["text", "key", and "code"].
     *             "text" will return a line of text entered by the user.
     *             "key" will return a single key pressed by the user as a
     *                   resolved character (only available on unix shells)
     *             "code" will return a single key pressed by the user as
     *                    the keycode of the keypress (only available on unix shells)
     * 
     * The given container MAY contain the following optional keys:
     * 
     * - "title": (string) A title to use for the option displayed.
     * - "content": (string|iterable) Any descriptive text used to designate
     *              what the option being requested is for.
     * - "default": (string) The default value to use if the user returns an
     *              empty string. If this is present and no input is entered,
     *              this value will be used in place of the empty user response.
     * - "options": (\Oroboros\core\interfaces\library\container\ContainerInterface)
     *              A list of predefined options, where the key is the option to
     *              match response text to, and the value is the descriptive text
     *              of the option. These options will not be enforced internally,
     *              they simply indicate any special options that may be handled
     *              by program logic internally a bit differently than a literal
     *              text string. Only the "exit" option will be handled within
     *              the view, all others are expected to be handled by the controller
     *              or component referencing this object.
     * "typecast": (string|false) designates whether or not to resolve the
     *             returned user input into the variable type it represents.
     *             If false, it will be left as a string. If a string, it must
     *             be a valid php variable type. By default, 'string', 'integer',
     *             'boolean', 'array' will be handled (arrays split on commas and
     *             trim whitespace from results)
     *             Implementations may extend upon this to accept additional types
     *             that they provide logic to resolve to (eg: directory, interface,
     *             classname, etc).
     * 
     * Other keynames may be present but will be ignored if they are
     * not required by implementations. Implementations should not fail
     * due to the presence of keys they do not use, which allows the option
     * definition to remain portable between the view and other use cases.
     * 
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $opt
     * @return mixed
     * @throws \Oroboros\core\exception\ExitException
     *         If the user enters "exit" as their response, indicating they wish
     *         to terminate the interactive shell.
     * @throws \Oroboros\core\exception\ErrorException
     *         If the option passed is malformed. This indicates the passed
     *         configuration needs to be adjusted.
     * @throws \Oroboros\core\exception\LogicException
     *         If the flags passed to the view indicate that there should not
     *         be visible output. Interactive shells require visible output,
     *         and unit testing requires extension classes honor their
     *         underlying methods cleanly. If the "quiet" or "silent" flag was
     *         passed, this method cannot resolve.
     */
    public function getOpt(\Oroboros\core\interfaces\library\container\ContainerInterface $opt);
}
