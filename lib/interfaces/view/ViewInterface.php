<?php
namespace Oroboros\core\interfaces\view;

/**
 * Provides a contract for standardized operation of views.
 * 
 * @author Brian Dayhoff
 */
interface ViewInterface extends \Oroboros\core\interfaces\BaseInterface, \Oroboros\core\interfaces\environment\EnvironmentInterface, \Oroboros\core\interfaces\library\event\EventAwareInterface, \Oroboros\core\interfaces\library\filter\FilterAwareInterface
{
    
}
