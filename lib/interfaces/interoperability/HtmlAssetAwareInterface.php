<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\interfaces\interoperability;

/**
 * HTML Asset Aware Interface
 * Designates that the object implementing it correctly handles the 
 * standard Oroboros methods for aggregating page assets that need to be 
 * externally referenced via script tags, links, etc.
 * 
 * These include anything that would need a meta preload, prefetch,
 * or dns-preload operation to speed page execution.
 * 
 * @note the trait \Oroboros\core\traits\html\HtmlAssetUtilityTrait provides all of the functionality required to honor this interface.
 * 
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
interface HtmlAssetAwareInterface
{

    /**
     * Returns a container of the scripts required by the component,
     * indexed by the section of the dom they should appear in.
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    public function getScripts(): \Oroboros\core\interfaces\library\container\ContainerInterface;

    /**
     * Returns a container of the stylesheets required by the component.
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    public function getStyles(): \Oroboros\core\interfaces\library\container\ContainerInterface;

    /**
     * Returns a container of the fonts required by the component.
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    public function getFonts(): \Oroboros\core\interfaces\library\container\ContainerInterface;

    /**
     * Returns a container of the fonts required by the component.
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    public function getImages(): \Oroboros\core\interfaces\library\container\ContainerInterface;

    /**
     * Adds a script by key identifier. Provide the section of the dom that
     * the script should compile into, and the key name of the script to compile.
     * @param string $section
     * @param string $key
     * @return void
     */
    public function addScript(string $section, string $key): void;

    /**
     * Returns whether the component has a specified script as a dependency
     * @param string $key
     * @return bool
     */
    public function hasScript(string $key): bool;

    /**
     * Removes a given script as a dependency, if it exists
     * @param string $key
     * @return void
     */
    public function removeScript(string $key): void;

    /**
     * Adds a stylesheet by key identifier.
     * @param string $key
     * @return void
     */
    public function addStyle(string $key): void;

    /**
     * Returns whether the component has a given stylesheet as a dependency
     * @param string $key
     * @return bool
     */
    public function hasStyle(string $key): bool;

    /**
     * Removes a given stylesheet as a dependency, if it exists
     * @param string $key
     * @return void
     */
    public function removeStyle(string $key): void;

    /**
     * Adds a font by key identifier.
     * @param string $key
     * @return void
     */
    public function addFont(string $key): void;

    /**
     * Returns whether the component has a given font as a dependency
     * @param string $key
     * @return bool
     */
    public function hasFont(string $key): bool;

    /**
     * Removes a given font as a dependency, if it exists
     * @param string $key
     * @return void
     */
    public function removeFont(string $key): void;

    /**
     * Adds a image by key identifier.
     * @param string $key
     * @return void
     */
    public function addImage(string $key): void;

    /**
     * Returns whether the component has a given image as a dependency
     * @param string $key
     * @return bool
     */
    public function hasImage(string $key): bool;

    /**
     * Removes a given image as a dependency, if it exists
     * @param string $key
     * @return void
     */
    public function removeImage(string $key): void;
}
