<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\interfaces\module;

/**
 * Module Interface
 * Defines a set of methods common to all modules
 *
 * Modules represent pre-packaged opinionated additions to the platform.
 * In constrast to services and extensions, modules provide default controller
 * logic that can make decisions about execution when referenced.
 * 
 * Well formed modules do not alter existing execution if existing execution
 * does not explicitly request functionality from the module, and the routing
 * provided does not point to any controller provided by the module.
 * Modules may freely excercise their own opinion when they are explicitly
 * referenced, or routing directs to a controller provided by the module.
 * 
 * A good example of a module would be a series of pages that provides
 * a payment gateway, and takes care of processing the transaction,
 * entering it into the database, and validating the exchange before
 * redirecting back to existing application functionality.
 * 
 * Modules may provide opinion when operating in their own scope.
 * Modules should not hijack existing operations with their opinion,
 * and must only operate when routing explicitly directs to the
 * module or its assets.
 * 
 * Modules may additionally provide supporting services that facilitate
 * the logic they contribute, if such services are not included in the
 * underlying platform.
 * 
 * @author Brian Dayhoff
 */
interface ModuleInterface extends \Oroboros\core\interfaces\pluggable\PluggableInterface, \Oroboros\core\interfaces\extension\ExtensionInterface, \Oroboros\core\interfaces\pattern\command\CommandSubjectInterface, \Oroboros\core\interfaces\interoperability\TemplateEngineSupportInterface, \Oroboros\core\interfaces\library\event\EventWatcherInterface, \Oroboros\core\interfaces\library\filter\FilterWatcherInterface
{

    /**
     * Returns a boolean designation as to whether the module provides
     * resources for the given route.
     * Resources may include scripts, stylesheets, media, fonts, data, or any
     * other relevant resources that the controller may need to obtain to
     * facilitate the module functionality.
     * If this method returns true, the function `getModuleResources` should
     * return a collection of appropriate resources.
     * If this method returns false, then `getModuleResources` should throw
     * an InvalidArgumentException when given the same route.
     * 
     * @param \Oroboros\core\interfaces\library\router\RouteInterface $route
     * @return bool
     */
    public function hasRouteResources(\Oroboros\core\interfaces\library\router\RouteInterface $route): bool;

    /**
     * Returns a collection of resources to use for the given route.
     * If ANY resources are obtainable for this route, the method `hasRouteResources`
     * should return `true` when given the same route. If no resources exist,
     * `hasRouteResources` should return `false` for the same route, and this
     * method should throw an \InvalidArgumentException.
     * 
     * @param \Oroboros\core\interfaces\library\router\RouteInterface $route
     * @return \Oroboros\core\interfaces\library\container\CollectionInterface
     * @throws \InvalidArgumentException If no resources exist for the given route
     */
    public function getRouteResources(\Oroboros\core\interfaces\library\router\RouteInterface $route): \Oroboros\core\interfaces\library\container\CollectionInterface;
}
