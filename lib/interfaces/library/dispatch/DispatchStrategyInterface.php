<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\interfaces\library\dispatch;

/**
 * Dispatch Strategy Interface
 * Defines a set of methods for obtaining the correct dispatcher
 * to handle a request or response
 * 
 * @author Brian Dayhoff
 */
interface DispatchStrategyInterface extends \Oroboros\core\interfaces\pattern\strategy\StrategyInterface, \Oroboros\core\interfaces\library\LibraryInterface
{

    /**
     * Evaluates the given subject, and returns a dispatcher associated
     * with serving the provided subject correctly if a match is found.
     * @param type $subject
     * @return \Oroboros\core\interfaces\library\dispatch\DispatchInterface
     * @throws \ErrorException if no matching dispatcher could
     *     resolve for the given subject
     */
    public function evaluate($subject): \Oroboros\core\interfaces\library\dispatch\DispatchInterface;

    /**
     * Defines a new dispatch object and match condition, or replaces an existing one
     * @param string $key
     * @param string $class
     * @param callable $condition
     * @return void
     */
    public static function setDispatchHandler(string $key, string $class, callable $condition): void;
}
