<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\interfaces\library\error;

/**
 * Error Handler Interface
 * Designates the methods required to function as an oroboros error handler
 * 
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
interface ErrorHandlerInterface extends \Oroboros\core\interfaces\library\LibraryInterface
{

    /**
     * Registers the error handler
     * 
     * @return void
     */
    public function register(): void;

    /**
     * Unregisters the error handler
     * 
     * @return void
     */
    public function unregister(): void;

    /**
     * Restores the previous error handler if this error handler is currently registered.
     * If this error handler is not currently registered, does nothing
     * 
     * @return void
     */
    public function restorePrevious(): void;

    /**
     * Returns a boolean designation as to whether the error handler
     * is currently registered.
     * If the error handler is registered directly, this must return `true`
     * If the error handler is encapsulated within an error handler
     * aggregate that returns `true` for the same method call,
     * this must also return `true`
     * 
     * If none of the above apply, this must return `false`.
     * 
     * @return bool
     */
    public function isRegistered(): bool;

    /**
     * Activates the error handler
     * 
     * @return void
     */
    public function activate(): void;

    /**
     * Deactivates the error handler
     * 
     * @return void
     */
    public function deactivate(): void;

    /**
     * Returns a boolean designation as to whether the error handler
     * is currently activated.
     * If the error handler is not registered, this must return `false`
     * If the error handler is activated directly, this must return `true`
     * If the error handler is encapsulated within an error handler
     * aggregate that returns `true` for the same method call,
     * this must also return `true`
     * 
     * If none of the above apply, this must return `false`.
     * 
     * @return bool
     */
    public function isActive(): bool;

    /**
     * Receives an error and determines if the error was handled.
     * If the error was handled, this must return `true`.
     * If the error was not handled, this must return `false`,
     * If the error handler is not registered, this must return `false`.
     * If the error handler is not activated, this must return `false`.
     * 
     * @param int $errno The level of error raised
     * @param string $errstr The error message
     * @param string $errfile The file the error originated from
     * @param int $errline The line number within the file that the error originated from
     * @param array $errcontext An array of all variables existing in the error scope
     * @return bool
     */
    public function handle(int $errno, string $errstr, string $errfile = null, int $errline = null, array $errcontext = null): bool;

    /**
     * Returns the code of the currently scoped error,
     * or null if no error is currently scoped
     * 
     * @return int|null
     */
    public function getCode(): ?int;

    /**
     * Returns the message of the currently scoped error,
     * or null if no error is currently scoped
     * 
     * @return string|null
     */
    public function getMessage(): ?string;

    /**
     * Returns the file name of the currently scoped error,
     * or null if no error is currently scoped
     * 
     * @return string|null
     */
    public function getFile(): ?string;

    /**
     * Returns the line number of the currently scoped error,
     * or null if no error is currently scoped
     * 
     * @return int|null
     */
    public function getLine(): ?int;

    /**
     * Returns the context of the currently scoped error,
     * or null if no error is currently scoped
     * 
     * @return array|null
     */
    public function getContext(): ?array;

    /**
     * Returns whether or not the currently scoped error was handled.
     * Returns `true` if this object handled the error successfully,
     * `false` in all other cases.
     * @return bool
     */
    public function getStatus(): bool;

    /**
     * Returns a log of all errors passed to this error handler,
     * and a boolean designation as to whether the each of them
     * was marked as handled by this error handler.
     */
    public function getLog();
}
