<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\interfaces\library\entity;

/**
 * Client Organization Entity Interface
 * Defines the standard methods expected to be implemented across all
 * client organization entities.
 * 
 * Client organizations represent all organizations under representing a group
 * that is externally interacted with by the application which is under
 * third party control. Client organizations are prevented from accessing
 * internal application access rights reserved for application ownership.
 * They may have identical access rights assigned to them which perform
 * only in their own scope, but do not affect overall application
 * functionality (eg they may be able to create or manage users within their
 * own organization, but cannot create internal users or manage them).
 * 
 * Client organizations can only manage usage that is allotted to
 * them by the application or it's ownership.
 * 
 * This includes all affiliates, clients, vendors, remote api providers,
 * or any other source that is not explicitly under the control of
 * the application.
 * 
 * @author Brian Dayhoff
 */
interface ClientOrganizationEntityInterface extends OrganizationEntityInterface
{
    
}
