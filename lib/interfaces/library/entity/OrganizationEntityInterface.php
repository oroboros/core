<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\interfaces\library\entity;

/**
 * Organization Entity Interface
 * Defines the standard methods expected to be implemented across all organization entities.
 * 
 * Organizations represent all tangible collective constructs such as but not limited to:
 * networks, companies, governments, affiliate groups, etc
 * 
 * @author Brian Dayhoff
 */
interface OrganizationEntityInterface extends GroupEntityInterface, \Oroboros\core\interfaces\library\auth\PermissionAwareInterface, \Oroboros\core\interfaces\library\auth\PermissionGroupAwareInterface
{

    /**
     * Organization entities use the standard constructor
     * 
     * Constructor will raise an exception if any entity or organization
     * class constants are malformed
     * 
     * @param string $command
     * @param array $arguments
     * @param array $flags
     * @throws \Oroboros\core\exception\core\InvalidClassException
     */
    public function __construct(string $command = null, array $arguments = null, array $flags = null);
}
