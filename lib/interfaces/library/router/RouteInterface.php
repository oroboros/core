<?php
namespace Oroboros\core\interfaces\library\router;

/**
 * Designates how a valid route object operates
 * @author Brian Dayhoff
 */
interface RouteInterface
extends \Oroboros\core\interfaces\library\LibraryInterface
{

    /**
     * Sets the HTTP request method, or equivalent command line identifier
     * @param string $protocol
     */
    public function setProtocol(string $protocol);

    /**
     * Sets the controller that will handle this route if it is valid.
     * @param string $controller
     */
    public function setController(string $controller);

    /**
     * Defines the controller method to call for the route if it is valid.
     * @param string $method
     */
    public function setControllerMethod(string $method);

    /**
     * Returns a boolean determination as to whether the route matches the specified request.
     * @return bool
     */
    public function match(\Psr\Http\Message\ServerRequestInterface $request);

    /**
     * Returns the instantiated controller.
     * This method will package the request and route in the instantiated controller,
     * in addition to any dependencies injected externally.
     * 
     * @param string $command (optional)
     * @param array $arguments (optional)
     * @param array $flags (optional)
     * @return \Oroboros\core\interfaces\controller\ControllerInterface
     */
    public function getController(string $command = null, array $arguments = null, array $flags = null): \Oroboros\core\interfaces\controller\ControllerInterface;

    /**
     * Returns the name of the controller method associated with the route.
     * @return string
     */
    public function getMethod();
}
