<?php
namespace Oroboros\core\interfaces\library\router;

/**
 * Designates how routing operations will be performed
 * @author Brian Dayhoff
 */
interface RouterInterface
extends \Oroboros\core\interfaces\library\LibraryInterface
{
    
}
