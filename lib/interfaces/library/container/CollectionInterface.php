<?php
/*
 * The MIT License
 *
 * @author Brian Dayhoff <Brian Dayhoff@me.com>
 * @copyright (c) 2018, Brian Dayhoff <Brian Dayhoff@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\interfaces\library\container;

/**
 * Collection Interface
 *
 * This is the base level interface for collections,
 * which represent a container of containers.
 *
 * Where individual containers are concerned with being scoped to a
 * specific realm of content, the collection is in turn concerned
 * with being scoped to a specific container type.
 *
 * This is used to insure that a given collection will return accurate results
 * when its child containers are iterated over for any given purpose.
 *
 * @author Brian Dayhoff <Brian Dayhoff@me.com>
 */
interface CollectionInterface
extends ContainerInterface
{

    /**
     * Returns a string representing an interface that is used
     * to validate values set in the collection,
     * which must extend \Oroboros\core\interfaces\library\container\ContainerInterface
     *
     * Values that correctly implement the given interface MUST
     * be accepted as valid values for the collection.
     *
     * Values that do not correctly implement the given interface MUST NOT
     * be accepted as valid values for the collection, and should raise
     * a valid instance of \Psr\Container\ContainerExceptionInterface
     * to block the setter attempt.
     *
     * Cases where the provided return value of this method do not resolve
     * to the correct interface or an interface extending it MUST raise a
     * `\Oroboros\core\exception\core\InvalidClassException`
     * in their constructor and block instantiation of the collection entirely.
     *
     * @return string
     * @see \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    public function getExpectedContainerType();
}
