<?php
namespace Oroboros\core\interfaces\library\template;

/**
 * Designates how templating operations should be handled
 * 
 * @author Brian Dayhoff
 */
interface TemplateInterface extends \Oroboros\core\interfaces\library\LibraryInterface
{
    
}
