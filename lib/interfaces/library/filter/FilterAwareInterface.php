<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\interfaces\library\filter;

/**
 * Filter Aware Interface
 * Designates that a class is capable of registering filters and handling filter callbacks
 * 
 * @note There is a trait that fully honors this interface
 * @see \Oroboros\core\traits\pattern\FilterAwareTrait
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
interface FilterAwareInterface extends \Oroboros\core\interfaces\library\action\ActionAwareInterface
{

    /**
     * Returns a boolean determination as to whether the given object
     * is an event watcher. This is object specific, not class specific.
     * 
     * If the second optional argument is passed as `true`, it will instead
     * return if any class in the watch list matches the class of the given watcher.
     * 
     * @param \Oroboros\core\interfaces\pattern\observer\ObserverInterface $watcher
     * @param bool $check_class If this argument is passed as true, this method
     *        will return whether *any* existing registered watcher matches the
     *        same class as the given watcher. This is useful in the event that
     *        classes are intended to only serve as a registration mechanism,
     *        and should not double-register events.
     * @return bool
     */
    public static function isWatchingFilters(\Oroboros\core\interfaces\pattern\observer\ObserverInterface $watcher, bool $check_class = false): bool;

    /**
     * Adds an object to the watcher list, which will alert it to registration 
     * or unregistration of filters, so it can add it's callbacks.
     * 
     * Any object in the watch list will be automatically attached to all new
     * instances of the filter aware class.
     * 
     * Objects added to the watch list **will not** be automatically attached
     * to existing instances of the class. They must be added to the watch list
     * prior to the creation of those objects if they need to hook into them.
     * 
     * Watchers will automatically be attached during object instantiation
     * and detached during object destruction.
     * 
     * @param \Oroboros\core\interfaces\pattern\observer\ObserverInterface $watcher
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException
     *         If the given watcher is already in the watch list.
     */
    public static function watchFilters(\Oroboros\core\interfaces\pattern\observer\ObserverInterface $watcher): void;

    /**
     * Removes an object from the watcher list, if it exists in it.
     * 
     * This will not trigger existing objects in the watcher list to detach,
     * it will only prevent new object instances from automatically attaching.
     * 
     * If the object does not exist in the list of watchers, it will raise an InvalidArgumentException
     * 
     * @param \Oroboros\core\interfaces\pattern\observer\ObserverInterface $watcher
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException if the given
     *         watcher is not a registered filter watcher.
     */
    public static function ignoreFilters(\Oroboros\core\interfaces\pattern\observer\ObserverInterface $watcher): void;
    
    /**
     * Reinitializes filter watchers.
     * 
     * This should be used if the underlying list of filter
     * watchers changed since class instantiation.
     * 
     * This will tell the object to release the existing bindings
     * and rebind to the updated master list.
     * 
     * @return void
     */
    public function reinitializeFilterWatchers(): void;

    /**
     * Observer event binding for filter registration
     * 
     * @param \Oroboros\core\interfaces\library\filter\FilterManagerInterface $subject
     */
    public function onFilterRegister(\Oroboros\core\interfaces\library\filter\FilterManagerInterface $subject): void;

    /**
     * Observer event binding for filter unregistration
     * 
     * @param type $subject
     * @return void
     */
    public function onFilterUnregister(\Oroboros\core\interfaces\library\filter\FilterManagerInterface $subject): void;

    /**
     * Observer event binding for filter execution
     * 
     * @param type $subject
     * @return void
     */
    public function onFilterRun(\Oroboros\core\interfaces\library\filter\FilterManagerInterface $subject): void;

    /**
     * Observer event binding for filter error status
     * 
     * @param type $subject
     * @return void
     */
    public function onFilterError(\Oroboros\core\interfaces\library\filter\FilterManagerInterface $subject): void;

    /**
     * Observer event binding for filter that has completed running successfully.
     * This fires immediately prior to resettting.
     * 
     * @param type $subject
     * @return void
     */
    public function onFilterComplete(\Oroboros\core\interfaces\library\filter\FilterManagerInterface $subject): void;

    /**
     * Observer event binding for completion of an filter action. This method
     * will only be called when the filter manager returns to a clean state.
     * 
     * @param type $subject
     * @return void
     */
    public function onFilterReady(\Oroboros\core\interfaces\library\filter\FilterManagerInterface $subject): void;

    /**
     * Observer event binding for filter callback execution
     * 
     * @param type $subject
     * @return void
     */
    public function onFilterCallbackRun(\Oroboros\core\interfaces\library\filter\FilterManagerInterface $subject): void;

    /**
     * Observer event binding for filter callback error status
     * 
     * @param type $subject
     * @return void
     */
    public function onFilterCallbackError(\Oroboros\core\interfaces\library\filter\FilterManagerInterface $subject): void;

    /**
     * Observer event binding for filter callback error status
     * 
     * @param type $subject
     * @return void
     */
    public function onFilterCallbackComplete(\Oroboros\core\interfaces\library\filter\FilterManagerInterface $subject): void;

    /**
     * Observer event binding for filter callback binding
     * 
     * @param type $subject
     * @return void
     */
    public function onFilterCallbackBind(\Oroboros\core\interfaces\library\filter\FilterManagerInterface $subject): void;

    /**
     * Observer event binding for filter callback unbinding
     * 
     * @param type $subject
     * @return void
     */
    public function onFilterCallbackUnbind(\Oroboros\core\interfaces\library\filter\FilterManagerInterface $subject): void;

    /**
     * Observer event binding for filter queue registration
     * 
     * @param type $subject
     * @return void
     */
    public function onFilterQueueRegister(\Oroboros\core\interfaces\library\filter\FilterManagerInterface $subject): void;

    /**
     * Observer event binding for filter queue unregistration
     * 
     * @param type $subject
     * @return void
     */
    public function onFilterQueueUnregister(\Oroboros\core\interfaces\library\filter\FilterManagerInterface $subject): void;

    /**
     * Returns a boolean designation as to whether a given filter queue exists.
     * 
     * @param string $queue
     * @return bool
     */
    public function hasFilterQueue(string $queue): bool;

    /**
     * Returns a list of the names of valid queues for this object.
     * 
     * @return array
     */
    public function listFilterQueues(): array;

    /**
     * Returns a boolean designation as to whether a given filter exists
     * in the current scope, or in an arbitrarily specified scope if the
     * second argument is passed.
     * 
     * If this returns false, this class does not have the specified filter,
     * and attempts to bind callbacks to it will raise an exception.
     * 
     * If this returns true, the filter is valid and callbacks may be bound to it,
     * provided they are not bound to an existing key name.
     * 
     * @param string $filter The filter name
     * @param string|null $queue The queue name if supplied, or the currently scoped queue if not
     * @return bool
     */
    public function hasFilter(string $filter, string $queue = null): bool;

    /**
     * Lists all the filters in the given queue, or the currently scoped
     * queue if no queue is supplied.
     * 
     * @param string $queue
     * @return array
     * @throws \Oroboros\core\exception\InvalidArgumentException
     *         if a queue name is given that does not exist.
     */
    public function listFilters(string $queue = null): array;

    /**
     * Returns a boolean designation as to whether a given filter has a bound
     * callback of the specified keyname
     * 
     * This will return false if the filter does not exist, or if it does but
     * the given key is not bound to it.
     * 
     * If `hasFilter` returns `true` for the same filter and this method
     * returns `false`, then it is safe to bind an filter callback for
     * the specified key.
     * 
     * If both this method and `hasFilter` return `false` for the same filter,
     * or if both return `true`, attempting to bind a callback with the
     * specified key will raise an exception. In the first case, the filter does
     * not exist, and in the second, the key is already in use.
     * 
     * @param string $filter The filter name
     * @param string|null $queue The queue name if supplied, or the currently scoped queue if not
     * @param string $key The identifier for the given callback
     * @return bool
     */
    public function hasFilterCallback(string $filter, string $key, string $queue = null): bool;

    /**
     * Binds a callback to the given filter in the specified queue,
     * under the name of `$key`
     * 
     * @param string $filter The name of the filter to bind the callback to
     * @param string $key The identifier for the callback
     * @param callable $callback The callback to fire when the filter fires
     * @param string|null $queue The queue to bind a callback into. If no queue 
     *        is supplied, the currently scoped queue will be used.
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException if the manager
     *         reports an error binding the callback. This can happen if the
     *         callback does not validate, or if the key is already registered.
     */
    public function bindFilterCallback(string $filter, string $key, callable $callback, string $queue = null): void;

    /**
     * Removes a given filter callback from the filter and queue specified.
     * If no queue is specified, the currently scoped queue will be used.
     * 
     * @param string $filter The name of the filter to bind the callback to
     * @param string $key The identifier for the callback
     * @param string|null $queue The queue to bind a callback into. If no queue 
     *        is supplied, the currently scoped queue will be used.
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    public function unbindFilterCallback(string $filter, string $key, string $queue = null): void;

    /**
     * Lists the filter callbacks in the specified filter of the given queue, or
     * the currently scoped queue if no queue identifier is passed.
     * 
     * @param string $filter
     * @param string $queue
     * @return array
     * @throws \Oroboros\core\exception\InvalidArgumentException
     *         If a given filter or queue do not exist.
     */
    public function listFilterCallbacks(string $filter, string $queue = null): array;
}
