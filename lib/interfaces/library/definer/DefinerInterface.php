<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\interfaces\library\definer;

/**
 * Definer Interface
 * Declares a common set of methods for interacting with definer objects.
 * 
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
interface DefinerInterface extends \Oroboros\core\interfaces\library\LibraryInterface
{

    /**
     * Returns the compiled dataset if it has been compiled.
     * 
     * If the dataset is not ready for use, will raise an exception.
     * 
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     * @throws \Oroboros\core\exception\ErrorException if the dataset has not yet been compiled.
     */
    public function fetch(): \Oroboros\core\interfaces\library\container\ContainerInterface;

    /**
     * Returns a boolean designation as to whether the dataset
     * is prepared for use correctly.
     * 
     * If all required keys have been satisfied and the data has been compiled,
     * this will return true. Otherwise it will return false.
     * 
     * If this returns false, an exception will be raised when attempting
     * to fetch the finished data.
     * 
     * @return bool
     */
    public function isCompiled(): bool;

    /**
     * Returns a boolean determination as to whether the final data set
     * can compile with the current set of satisfied keys.
     * 
     * @return bool
     */
    public function canCompile(): bool;

    /**
     * Compiles the final dataset output. All required keys must be satisfied.
     * An ErrorException will be raised if there are missing required keys.
     * 
     * @return \Oroboros\core\interfaces\library\definer\DefinerInterface returns $this (method chainable)
     * @throws \Oroboros\core\exception\ErrorException if any required keys have not yet been passed definitions.
     */
    public function compile(): \Oroboros\core\interfaces\library\definer\DefinerInterface;

    /**
     * Returns a container with all of the required keys,
     * where the key is their key name and the value is
     * the required format.
     * 
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    public function getRequired(): \Oroboros\core\interfaces\library\container\ContainerInterface;

    /**
     * Returns an associative array of any currently unsatisfied required keys,
     * where the key is the keyname and the value is the expected format
     * 
     * @return array
     */
    public function getUnsatisfiedRequiredKeys(): array;

    /**
     * Returns a boolean designation as to whether any required keys
     * are currently unsatisfied.
     * 
     * @return bool
     */
    public function hasUnsatisfiedRequiredKeys(): bool;

    /**
     * Returns a boolean designation as to whether a supplied
     * key name is required for the dataset to compile.
     * 
     * @param string $key The name of the key to evaluate
     * @return bool
     */
    public function hasRequiredKey(string $key): bool;

    /**
     * Returns a boolean determination as to whether a given required key is satisfied.
     * 
     * Returns true if it has a value supplied and false if not.
     * 
     * If a non-existent key is passed, raises an exception.
     * 
     * @param string $key
     * @return bool
     * @throws \Oroboros\core\exception\InvalidArgumentException if the given key is not a required key
     */
    public function isRequiredKeySatisfied(string $key): bool;

    /**
     * Sets the insert value for the given required key.
     * 
     * This will raise an exception if the value is the wrong type,
     * or if the key is not a required key.
     * 
     * This can be checked safely with `hasRequiredKey`
     * and `isValidRequiredValue` respectively.
     * 
     * @param string $key
     * @param type $value
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException if the designated key
     *         is not a required key, or if the type of the value is not valid
     *         for the required type.
     */
    public function setRequiredKey(string $key, $value): void;
    
    /**
     * Returns the current value for the given required key.
     * @param string $key
     * @return mixed
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    public function getRequiredValue(string $key);

    /**
     * Returns a boolean designation as to whether a supplied
     * value satisfies the type requirement of a required key.
     * 
     * If the keyname exists and the value is valid, returns true.
     * Otherwise returns false.
     * 
     * @param string $key The name of the key to evaluate
     * @return bool
     */
    public function isValidRequiredValue(string $key, $value): bool;

    /**
     * Returns a container with all of the optional keys,
     * where the key is their key name and the value is
     * their required format.
     * 
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    public function getOptional(): \Oroboros\core\interfaces\library\container\ContainerInterface;

    /**
     * Returns an associative array of any currently unsatisfied optional keys,
     * where the key is the keyname and the value is the expected format
     * 
     * @return array
     */
    public function getUnsatisfiedOptionalKeys(): array;

    /**
     * Returns a boolean designation as to whether any optional keys
     * are currently unsatisfied.
     * 
     * @return bool
     */
    public function hasUnsatisfiedOptionalKeys(): bool;

    /**
     * Returns a boolean designation as to whether a supplied
     * key name is within the set of optional keys acceptable
     * for the dataset to compile with.
     * 
     * @param string $key The name of the key to evaluate
     * @return bool
     */
    public function hasOptionalKey(string $key): bool;

    /**
     * Returns a boolean determination as to whether a given required key is satisfied.
     * 
     * Returns true if it has a value supplied and false if not.
     * 
     * If a non-existent key is passed, raises an exception.
     * 
     * @param string $key
     * @return bool
     * @throws \Oroboros\core\exception\InvalidArgumentException if the given key is not an optional key
     */
    public function isOptionalKeySatisfied(string $key): bool;

    /**
     * Sets the insert value for the given optional key.
     * 
     * This will raise an exception if the value is the wrong type,
     * or if the key is not an optional key.
     * 
     * This can be checked safely with `hasOptionalKey`
     * and `isValidOptionalValue` respectively.
     * 
     * @param string $key
     * @param type $value
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException if the designated key
     *         is not an optional key, or if the type of the value is not valid
     *         for the required type.
     */
    public function setOptionalKey(string $key, $value): void;
    
    /**
     * Returns the current value for the given optional key.
     * @param string $key
     * @return mixed
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    public function getOptionalValue(string $key);

    /**
     * Returns a boolean designation as to whether a supplied
     * value satisfies the type requirement of a required key.
     * 
     * If the keyname exists and the value is valid, returns true.
     * Otherwise returns false.
     * 
     * @param string $key The name of the key to evaluate
     * @return bool
     */
    public function isValidOptionalValue(string $key, $value): bool;
}
