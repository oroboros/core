<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\interfaces\library\log;

/**
 * Logger Interface
 * Defines the methods required for the Oroboros implementation of psr-3 logging
 * 
 * This interface only insures that loggers work with
 * Oroboros factories in addition to fulfilling the
 * Psr-3 criteria,
 * 
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
interface LoggerInterface extends \Oroboros\core\interfaces\library\LibraryInterface, \Psr\Log\LoggerInterface
{

    /**
     * Binds the Psr-3 log level for emergencies directly to the interface
     */
    const LOG_LEVEL_EMERGENCY = \Psr\Log\LogLevel::EMERGENCY;

    /**
     * Binds the Psr-3 log level for alerts directly to the interface
     */
    const LOG_LEVEL_ALERT = \Psr\Log\LogLevel::ALERT;

    /**
     * Binds the Psr-3 log level for critical errors directly to the interface
     */
    const LOG_LEVEL_CRITICAL = \Psr\Log\LogLevel::CRITICAL;

    /**
     * Binds the Psr-3 log level for errors directly to the interface
     */
    const LOG_LEVEL_ERROR = \Psr\Log\LogLevel::ERROR;

    /**
     * Binds the Psr-3 log level for warnings directly to the interface
     */
    const LOG_LEVEL_WARNING = \Psr\Log\LogLevel::WARNING;

    /**
     * Binds the Psr-3 log level for notices directly to the interface
     */
    const LOG_LEVEL_NOTICE = \Psr\Log\LogLevel::NOTICE;

    /**
     * Binds the Psr-3 log level for info messages directly to the interface
     */
    const LOG_LEVEL_INFO = \Psr\Log\LogLevel::INFO;

    /**
     * Binds the Psr-3 log level for debug messages directly to the interface
     */
    const LOG_LEVEL_DEBUG = \Psr\Log\LogLevel::DEBUG;

    /**
     * System is unusable.
     *
     * @param string $message
     * @param array  $context
     *
     * @return void
     */
    public function emergency($message, array $context = []);

    /**
     * Action must be taken immediately.
     *
     * Example: Entire website down, database unavailable, etc. This should
     * trigger the SMS alerts and wake you up.
     *
     * @param string $message
     * @param array  $context
     *
     * @return void
     */
    public function alert($message, array $context = []);

    /**
     * Critical conditions.
     *
     * Example: Application component unavailable, unexpected exception.
     *
     * @param string $message
     * @param array  $context
     *
     * @return void
     */
    public function critical($message, array $context = []);

    /**
     * Runtime errors that do not require immediate action but should typically
     * be logged and monitored.
     *
     * @param string $message
     * @param array  $context
     *
     * @return void
     */
    public function error($message, array $context = []);

    /**
     * Exceptional occurrences that are not errors.
     *
     * Example: Use of deprecated APIs, poor use of an API, undesirable things
     * that are not necessarily wrong.
     *
     * @param string $message
     * @param array  $context
     *
     * @return void
     */
    public function warning($message, array $context = []);

    /**
     * Normal but significant events.
     *
     * @param string $message
     * @param array  $context
     *
     * @return void
     */
    public function notice($message, array $context = []);

    /**
     * Interesting events.
     *
     * Example: User logs in, SQL logs.
     *
     * @param string $message
     * @param array  $context
     *
     * @return void
     */
    public function info($message, array $context = []);

    /**
     * Detailed debug information.
     *
     * @param string $message
     * @param array  $context
     *
     * @return void
     */
    public function debug($message, array $context = []);

    /**
     * Logs with an arbitrary level.
     *
     * @param mixed  $level
     * @param string $message
     * @param array  $context
     *
     * @return void
     *
     * @throws \Psr\Log\InvalidArgumentException
     */
    public function log($level, $message, array $context = []);
}
