<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\interfaces\library\reflector;

/**
 * Reflector Property Interface
 * Common interface shared by all reflection objects that represent properties
 * 
 * @link https://www.php.net/manual/en/class.reflectionproperty.php
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
interface ReflectorPropertyInterface extends ReflectorInterface
{

    const IS_STATIC = 16;
    const IS_PUBLIC = 1;
    const IS_PROTECTED = 2;
    const IS_PRIVATE = 4;

    /**
     * 
     * @param mixed $class
     * @param string $name
     * @param bool $return
     * @return string
     */
    public static function export(mixed $class, string $name, bool $return = null): string;

    /**
     * 
     * @return string
     */
    public function __toString(): string;

    /**
     * 
     * @param string $name
     * @param int $flags
     * @return array
     */
    public function getAttributes(string $name = null, int $flags = 0): array;

    /**
     * 
     * @return \ReflectionClass
     */
    public function getDeclaringClass(): \ReflectionClass;

    /**
     * 
     * @return mixed
     */
    public function getDefaultValue();

    /**
     * 
     * @return string|false
     */
    public function getDocComment();

    /**
     * 
     * @return int
     */
    public function getModifiers(): int;

    /**
     * 
     * @return string
     */
    public function getName(): string;

    /**
     * 
     * @return ReflectionType|null
     */
    public function getType(): ?ReflectionType;

    /**
     * 
     * @param object|null $object
     * @return mixed
     */
    public function getValue(?object $object = null): mixed;

    /**
     * 
     * @return bool
     */
    public function hasDefaultValue(): bool;

    /**
     * 
     * @return bool
     */
    public function hasType(): bool;

    /**
     * 
     * @return bool
     */
    public function isDefault(): bool;

    /**
     * 
     * @param object|null $object
     * @return bool
     */
    public function isInitialized(?object $object = null): bool;

    /**
     * 
     * @return bool
     */
    public function isPrivate(): bool;

    /**
     * 
     * @return bool
     */
    public function isProtected(): bool;

    /**
     * 
     * @return bool
     */
    public function isPublic(): bool;

    /**
     * 
     * @return bool
     */
    public function isStatic(): bool;

    /**
     * 
     * @param bool $accessible
     * @return void
     */
    public function setAccessible(bool $accessible): void;

    /**
     * 
     * @param object $object
     * @param mixed $value
     * @return void
     */
    public function setValue(object $object, mixed $value): void;
}
