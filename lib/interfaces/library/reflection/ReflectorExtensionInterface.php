<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\interfaces\library\reflector;

/**
 * Reflector Extension Interface
 * Common interface shared by all reflection objects that represent non-zend extensions
 * 
 * @link https://www.php.net/manual/en/class.reflectionextension.php
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
interface ReflectorExtensionInterface extends ReflectorInterface
{

    const IS_PUBLIC = 1;
    const IS_PROTECTED = 2;
    const IS_PRIVATE = 4;

    /**
     * 
     * @param string $name
     * @param string $return
     * @return string
     */
    public static function export(string $name, string $return = false): string;

    /**
     * 
     * @return string
     */
    public function __toString(): string;

    /**
     * 
     * @return array
     */
    public function getClasses(): array;

    /**
     * 
     * @return array
     */
    public function getClassNames(): array;

    /**
     * 
     * @return array
     */
    public function getConstants(): array;

    /**
     * 
     * @return array
     */
    public function getDependencies(): array;

    /**
     * 
     * @return array
     */
    public function getFunctions(): array;

    /**
     * 
     * @return array
     */
    public function getINIEntries(): array;

    /**
     * 
     * @return string
     */
    public function getName(): string;

    /**
     * 
     * @return string|null
     */
    public function getVersion(): ?string;

    /**
     * 
     * @return void
     */
    public function info(): void;

    /**
     * 
     * @return bool
     */
    public function isPersistent(): bool;

    /**
     * 
     * @return bool
     */
    public function isTemporary(): bool;
}
