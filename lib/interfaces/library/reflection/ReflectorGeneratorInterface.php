<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\interfaces\library\reflector;

/**
 * Reflector Generator Interface
 * Common interface shared by all reflection generator objects
 * 
 * @link https://www.php.net/manual/en/class.reflectionreference.php
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
interface ReflectorGeneratorInterface extends ReflectorInterface
{

    /**
     * 
     * @return string
     */
    public function getExecutingFile(): string;

    /**
     * 
     * @return \Generator
     */
    public function getExecutingGenerator(): \Generator;

    /**
     * 
     * @return int
     */
    public function getExecutingLine(): int;

    /**
     * 
     * @return \ReflectionFunctionAbstract
     */
    public function getFunction(): \ReflectionFunctionAbstract;

    /**
     * 
     * @return object|null
     */
    public function getThis(): ?object;

    /**
     * 
     * @param int $options
     * @return array
     */
    public function getTrace(int $options = DEBUG_BACKTRACE_PROVIDE_OBJECT): array;
}
