<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\interfaces\library\reflector;

/**
 * Reflector Function Abstract Interface
 * Common interface shared by all reflection objects that extend \ReflectionFunctionAbstract
 * 
 * @link https://www.php.net/manual/en/class.reflectionfunctionabstract.php
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
interface ReflectorFunctionAbstractInterface extends ReflectorInterface
{

    /**
     * 
     * @param string $name
     * @param int $flags
     * @return array
     */
    public function getAttributes(string $name = null, int $flags = 0): array;

    /**
     * 
     * @return \ReflectionClass|null
     */
    public function getClosureScopeClass(): ?\ReflectionClass;

    /**
     * 
     * @return object|null
     */
    public function getClosureThis(): ?object;

    /**
     * 
     * @return string|false
     */
    public function getDocComment();

    /**
     * 
     * @return int|false
     */
    public function getEndLine();

    /**
     * 
     * @return \ReflectionExtension|null
     */
    public function getExtension(): ?\ReflectionExtension;

    /**
     * 
     * @return string|false
     */
    public function getExtensionName();

    /**
     * 
     * @return string|false
     */
    public function getFileName();

    /**
     * 
     * @return string
     */
    public function getName(): string;

    /**
     * 
     * @return string
     */
    public function getNamespaceName(): string;

    /**
     * 
     * @return int
     */
    public function getNumberOfParameters(): int;

    /**
     * 
     * @return int
     */
    public function getNumberOfRequiredParameters(): int;

    /**
     * 
     * @return array
     */
    public function getParameters(): array;

    /**
     * 
     * @return \ReflectionType|null
     */
    public function getReturnType(): ?\ReflectionType;

    /**
     * 
     * @return string
     */
    public function getShortName(): string;

    /**
     * 
     * @return int|false
     */
    public function getStartLine();

    /**
     * 
     * @return array
     */
    public function getStaticVariables(): array;

    /**
     * 
     * @return bool
     */
    public function hasReturnType(): bool;

    /**
     * 
     * @return bool
     */
    public function inNamespace(): bool;

    /**
     * 
     * @return bool
     */
    public function isClosure(): bool;

    /**
     * 
     * @return bool
     */
    public function isDeprecated(): bool;

    /**
     * 
     * @return bool
     */
    public function isGenerator(): bool;

    /**
     * 
     * @return bool
     */
    public function isInternal(): bool;

    /**
     * 
     * @return bool
     */
    public function isUserDefined(): bool;

    /**
     * 
     * @return bool
     */
    public function isVariadic(): bool;

    /**
     * 
     * @return bool
     */
    public function returnsReference(): bool;
}
