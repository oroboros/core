<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\interfaces\library\reflector;

/**
 * Reflector Class Interface
 * Common interface shared by all reflection objects that represent classes
 * 
 * @link https://www.php.net/manual/en/class.reflectionclass.php
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
interface ReflectorClassInterface extends ReflectorInterface
{

    const IS_IMPLICIT_ABSTRACT = 16;
    const IS_EXPLICIT_ABSTRACT = 32;
    const IS_FINAL = 64;

    /**
     * 
     * @param mixed $argument
     * @param bool $return
     * @return string
     */
    public static function export(mixed $argument, bool $return = false): string;

    /**
     * 
     * @return string
     */
    public function __toString(): string;

    /**
     * 
     * @param string $name
     * @param int $flags
     * @return array
     */
    public function getAttributes(string $name = null, int $flags = 0): array;

    /**
     * 
     * @param string $name
     * @return mixed
     */
    public function getConstant(string $name): mixed;

    /**
     * 
     * @param int|null $filter
     * @return array
     */
    public function getConstants(?int $filter = null): array;

    /**
     * 
     * @return ReflectionMethod|null
     */
    public function getConstructor(): ?ReflectionMethod;

    /**
     * 
     * @return array
     */
    public function getDefaultProperties(): array;

    /**
     * 
     * @return string|false
     */
    public function getDocComment();

    /**
     * 
     * @return int|false
     */
    public function getEndLine();

    /**
     * 
     * @return \ReflectionExtension|null\
     */
    public function getExtension(): ?\ReflectionExtension;

    /**
     * 
     * @return string|false
     */
    public function getExtensionName();

    /**
     * 
     * @return string|false
     */
    public function getFileName();

    /**
     * 
     * @return array
     */
    public function getInterfaceNames(): array;

    /**
     * 
     * @return array
     */
    public function getInterfaces(): array;

    /**
     * 
     * @param string $name
     * @return \ReflectionMethod
     */
    public function getMethod(string $name): \ReflectionMethod;

    /**
     * 
     * @param int|null $filter
     * @return array
     */
    public function getMethods(?int $filter = null): array;

    /**
     * 
     * @return int
     */
    public function getModifiers(): int;

    /**
     * 
     * @return string
     */
    public function getName(): string;

    /**
     * 
     * @return string
     */
    public function getNamespaceName(): string;

    /**
     * 
     * @return ReflectionClass|false
     */
    public function getParentClass();

    /**
     * 
     * @param int|null $filter
     * @return array
     */
    public function getProperties(?int $filter = null): array;

    /**
     * 
     * @param string $name
     * @return ReflectionProperty
     */
    public function getProperty(string $name): ReflectionProperty;

    /**
     * 
     * @param string $name
     * @return ReflectionClassConstant|false
     */
    public function getReflectionConstant(string $name);

    /**
     * 
     * @param int|null $filter
     * @return array
     */
    public function getReflectionConstants(?int $filter = null): array;

    /**
     * 
     * @return string
     */
    public function getShortName(): string;

    /**
     * 
     * @return int|false
     */
    public function getStartLine();

    /**
     * 
     * @return array|null
     */
    public function getStaticProperties(): ?array;

    /**
     * 
     * @param string $name
     * @param mixed $def_value
     */
    public function getStaticPropertyValue(string $name, &$def_value = null);

    /**
     * 
     * @return array
     */
    public function getTraitAliases(): array;

    /**
     * 
     * @return array
     */
    public function getTraitNames(): array;

    /**
     * 
     * @return array
     */
    public function getTraits(): array;

    /**
     * 
     * @param string $name
     * @return bool
     */
    public function hasConstant(string $name): bool;

    /**
     * 
     * @param string $name
     * @return bool
     */
    public function hasMethod(string $name): bool;

    /**
     * 
     * @param string $name
     * @return bool
     */
    public function hasProperty(string $name): bool;

    /**
     * 
     * @param ReflectionClass|string $interface
     * @return bool
     */
    public function implementsInterface($interface): bool;

    /**
     * 
     * @return bool
     */
    public function inNamespace(): bool;

    /**
     * 
     * @return bool
     */
    public function isAbstract(): bool;

    /**
     * 
     * @return bool
     */
    public function isAnonymous(): bool;

    /**
     * 
     * @return bool
     */
    public function isCloneable(): bool;

    /**
     * 
     * @return bool
     */
    public function isFinal(): bool;

    /**
     * 
     * @param object $object
     * @return bool
     */
    public function isInstance(object $object): bool;

    /**
     * 
     * @return bool
     */
    public function isInstantiable(): bool;

    /**
     * 
     * @return bool
     */
    public function isInterface(): bool;

    /**
     * 
     * @return bool
     */
    public function isInternal(): bool;

    /**
     * 
     * @return bool
     */
    public function isIterable(): bool;

    /**
     * 
     * @param \ReflectionClass|string $class
     * @return bool
     */
    public function isSubclassOf($class): bool;

    /**
     * 
     * @return bool
     */
    public function isTrait(): bool;

    /**
     * 
     * @return bool
     */
    public function isUserDefined(): bool;

    /**
     * 
     * @param mixed $args
     * @return object
     */
    public function newInstance(mixed ...$args): object;

    /**
     * 
     * @param array $args
     * @return object
     */
    public function newInstanceArgs(array $args = []): object;

    /**
     * 
     * @return object
     */
    public function newInstanceWithoutConstructor(): object;

    /**
     * 
     * @param string $name
     * @param mixed $value
     * @return void
     */
    public function setStaticPropertyValue(string $name, $value): void;
}
