<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\interfaces\library\reflector;

/**
 * Reflector Function Interface
 * Common interface shared by all reflection objects that represent non-zend extensions
 * 
 * @link https://www.php.net/manual/en/class.reflectionfunction.php
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
interface ReflectorFunctionInterface extends ReflectorFunctionAbstractInterface
{

    const IS_DEPRECATED = 262144;

    /**
     * 
     * @param string $name
     * @param string $return
     * @return string
     */
    public static function export(string $name, string $return = null): string;

    /**
     * 
     * @return string
     */
    public function __toString(): string;

    /**
     * 
     * @return \Closure
     */
    public function getClosure(): \Closure;

    /**
     * 
     * @note takes any number of arguments
     * @param mixed $args
     * @return mixed
     */
    public function invoke(...$args);

    /**
     * 
     * @param array $args
     * @return mixed
     */
    public function invokeArgs(array $args);

    /**
     * 
     * @return bool
     */
    public function isDisabled(): bool;
}
