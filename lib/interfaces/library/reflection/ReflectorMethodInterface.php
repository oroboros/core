<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\interfaces\library\reflector;

/**
 * Reflector Method Interface
 * Common interface shared by all reflection objects that represent reflection methods
 * 
 * @link https://www.php.net/manual/en/class.reflectionmethod.php
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
interface ReflectorMethodInterface extends ReflectorFunctionAbstractInterface
{

    const IS_STATIC = 16;
    const IS_PUBLIC = 1;
    const IS_PROTECTED = 2;
    const IS_PRIVATE = 4;
    const IS_ABSTRACT = 64;
    const IS_FINAL = 32;

    /**
     * 
     * @param string $class
     * @param string $name
     * @param bool $return
     * @return string
     */
    public static function export(string $class, string $name, bool $return = false): string;

    /**
     * 
     * @return string
     */
    public function __toString(): string;

    /**
     * 
     * @return \Closure
     */
    public function getClosure(): \Closure;

    /**
     * 
     * @return \ReflectionClass
     */
    public function getDeclaringClass(): \ReflectionClass;

    /**
     * 
     * @return int
     */
    public function getModifiers(): int;

    /**
     * 
     * @return \ReflectionMethod
     */
    public function getPrototype(): \ReflectionMethod;

    /**
     * 
     * @param object $object
     * @param type $args
     * @return mixed
     */
    public function invoke(object $object = null, ...$args = null);

    /**
     * 
     * @param array $args
     * @param type $args
     * @return mixed
     */
    public function invokeArgs(object $object = null, ...$args = null);

    /**
     * 
     * @return bool
     */
    public function isAbstract(): bool;

    /**
     * 
     * @return bool
     */
    public function isConstructor(): bool;

    /**
     * 
     * @return bool
     */
    public function isDestructor(): bool;

    /**
     * 
     * @return bool
     */
    public function isFinal(): bool;

    /**
     * 
     * @return bool
     */
    public function isPrivate(): bool;

    /**
     * 
     * @return bool
     */
    public function isProtected(): bool;

    /**
     * 
     * @return bool
     */
    public function isPublic(): bool;

    /**
     * 
     * @return bool
     */
    public function isStatic(): bool;

    /**
     * 
     * @param bool $accessible
     * @return void
     */
    public function setAccessible(bool $accessible): void;
}
