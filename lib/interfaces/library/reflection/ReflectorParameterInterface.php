<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\interfaces\library\reflector;

/**
 * Reflector Parameter Interface
 * Common interface shared by all reflection objects that represent parameters
 * 
 * @link https://www.php.net/manual/en/class.reflectionproperty.php
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
interface ReflectorParameterInterface extends ReflectorInterface
{

    /**
     * 
     * @return string
     */
    public function __toString(): string;

    /**
     * 
     * @return bool
     */
    public function allowsNull(): bool;

    /**
     * 
     * @return bool
     */
    public function canBePassedByValue(): bool;

    /**
     * 
     * @param string $function
     * @param string $parameter
     * @param bool $return
     * @return string
     */
    public static function export(string $function, string $parameter, bool $return = null): string;

    /**
     * 
     * @param string $name
     * @param int $flags
     * @return array
     */
    public function getAttributes(string $name = null, int $flags = 0): array;

    /**
     * 
     * @return \ReflectionClass|null
     */
    public function getClass(): ?\ReflectionClass;

    /**
     * 
     * @return \ReflectionClass|null
     */
    public function getDeclaringClass(): ?\ReflectionClass;

    /**
     * 
     * @return \ReflectionFunctionAbstract
     */
    public function getDeclaringFunction(): \ReflectionFunctionAbstract;

    /**
     * 
     * @return mixed
     */
    public function getDefaultValue();

    /**
     * 
     * @return string|null
     */
    public function getDefaultValueConstantName(): ?string;

    /**
     * 
     * @return string
     */
    public function getName(): string;

    /**
     * 
     * @return int
     */
    public function getPosition(): int;

    /**
     * 
     * @return \ReflectionType|null
     */
    public function getType(): ?\ReflectionType;

    /**
     * 
     * @return bool
     */
    public function hasType(): bool;

    /**
     * 
     * @return bool
     */
    public function isArray(): bool;

    /**
     * 
     * @return bool
     */
    public function isCallable(): bool;

    /**
     * 
     * @return bool
     */
    public function isDefaultValueAvailable(): bool;

    /**
     * 
     * @return bool
     */
    public function isDefaultValueConstant(): bool;

    /**
     * 
     * @return bool
     */
    public function isOptional(): bool;

    /**
     * 
     * @return bool
     */
    public function isPassedByReference(): bool;

    /**
     * 
     * @return bool
     */
    public function isVariadic(): bool;
}
