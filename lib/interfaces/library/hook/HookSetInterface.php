<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\interfaces\library\hook;

/**
 * Hook Set Interface
 * Declares a common set of methods a class must honor to qualify as a hook set
 * 
 * Hook sets are PSR-11 compliant containers.
 * Hook sets automatically generate hook objects from supplied key/value pairs
 * 
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
interface HookSetInterface extends \Oroboros\core\interfaces\library\container\ContainerInterface
{
    /**
     * Iterates over all contained hooks, sequentially replacing hooks 
     * in the given string with their values where they are relevant.
     * 
     * @param string $string
     * @return string
     */
    public function replace(string $string);

    /**
     * Iterates over all contained hooks, sequentially nullifying
     * their existence in the given string.
     * 
     * @param string $string
     * @return string
     */
    public function nullify(string $string): string;
}
