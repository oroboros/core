<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\interfaces\library\hook;

/**
 * Hook Interface
 * Declares a common set of methods a class must honor to satisfy the hook api
 * 
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
interface HookInterface extends \Oroboros\core\interfaces\library\LibraryInterface
{

    /**
     * Standard Oroboros Constructor
     * 
     * @param string $command Sets the key that will be used as the hook definition. Case sensitive.
     * @param array $arguments may contain a key `value` which will set the value if it exists.
     *                         If implementations require dependencies,
     *                         they should also be present as keys in this argument.
     * @param array $flags optional flags to modify operations internally as per implementation
     */
    public function __construct(string $command = null, array $arguments = null, array $flags = null);

    /**
     * Replaces any instances of the hook definition in the supplied string with the hook value.
     * If no instances exist, returns the string exactly as it was supplied.
     * This method is exception safe.
     * 
     * @param string $string
     * @return string
     */
    public function replace(string $string);

    /**
     * Removes any instances of the supplied hook from the supplied string entirely,
     * without replacing them with any data.
     * If no instances exist, returns the string exactly as it was supplied.
     * This method is exception safe.
     * 
     * @param string $string
     * @return string
     */
    public function nullify(string $string): string;

    /**
     * Returns a boolean designation as to whether a supplied string has matches for this hook.
     * This method is exception safe.
     * 
     * @param string $string
     * @return bool
     */
    public function isMatch(string $string): bool;

    /**
     * Provides the key that will be matched within the hook definition.
     * This method is exception safe.
     * 
     * @return string
     */
    public function getKey(): string;

    /**
     * Returns the exact hook that will be matched if it exists.
     * This method is exception safe.
     * 
     * @return string
     */
    public function getHook(): string;

    /**
     * Provides the value that will be injected into a match.
     * This method is exception safe.
     * 
     * @return scalar|null
     */
    public function getValue();
}
