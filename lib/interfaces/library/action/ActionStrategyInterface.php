<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\interfaces\library\action;

/**
 * Action Strategy Interface
 * Designates the methods that must be available for action strategies
 * 
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
interface ActionStrategyInterface extends \Oroboros\core\interfaces\pattern\strategy\StrategyInterface, \Oroboros\core\interfaces\library\LibraryInterface
{

    /**
     * Registers an action class as handling a specific action type
     * 
     * @param string $type
     * @param string $action
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException if the given
     *         action does not resolve to a class, or does not implement the
     *         action interface
     */
    public static function registerActionType(string $type, string $action): void;

    /**
     * Preflight method to insure that a given action/queue is valid.
     * @param string $type
     * @param string $queue
     * @return void
     * @throws \InvalidArgumentException If the given action or queue is not valid.
     */
    public function verifyAction(string $type, string $queue): void;

    /**
     * Returns the action object designated
     * @param string $type
     * @param string $queue
     * @return \Oroboros\core\interfaces\library\action\ActionInterface
     */
    public function getAction(string $type, string $queue): \Oroboros\core\interfaces\library\action\ActionInterface;
}
