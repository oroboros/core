<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\interfaces\library\action;

/**
 * Action Manager Interface
 * Designates the methods that must be available for
 * a class to act as an action manager.
 * 
 * Action managers create, register, and organize actions into queues for the
 * parent object, and relay commands from their parent to the actions, as well
 * as relaying updates during action runtime from the action back to the parent.
 * 
 * They allow the parent object to mark errors as recoverable, break operation,
 * and designate that no further action is needed because the task has already
 * been resolved.
 * 
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
interface ActionManagerInterface extends \Oroboros\core\interfaces\library\LibraryInterface, \Oroboros\core\interfaces\pattern\director\DirectorInterface, \Oroboros\core\interfaces\pattern\observer\ObservableInterface, \Oroboros\core\interfaces\pattern\observer\ObserverInterface
{

    /**
     * Dependency injection method for the action subject that controls the
     * actions supplied by the manager. This method may only be called one time
     * per instantiation.
     * 
     * If a subject was already set, it will raise an error exception.
     * 
     * @param \Oroboros\core\interfaces\library\action\ActionAwareInterface $subject
     * @return void
     * @throws \Oroboros\core\exception\ErrorException
     */
    public function setSubject(\Oroboros\core\interfaces\library\action\ActionAwareInterface $subject): void;

    /**
     * Returns the manager action group name.
     * 
     * @return string
     */
    public function getGroup(): string;

    /**
     * Returns the manager action name, if any.
     * 
     * @return string|null
     */
    public function getName(): ?string;

    /**
     * Returns the factorizable classname or stubname of acceptable 
     * action classes to enter into a queue.
     * 
     * @return string
     */
    public function getActionClass(): string;

    /**
     * Returns the observer priority
     * 
     * @return int
     */
    public function getObserverPriority(): int;

    /**
     * Observer event binding for action registration
     * 
     * @param \Oroboros\core\interfaces\library\action\ActionInterface $subject
     * @return void
     */
    public function onActionRegister(\Oroboros\core\interfaces\library\action\ActionInterface $subject): void;

    /**
     * Observer event binding for action unregistration
     * 
     * @param \Oroboros\core\interfaces\library\action\ActionInterface $subject
     * @return void
     */
    public function onActionUnregister(\Oroboros\core\interfaces\library\action\ActionInterface $subject): void;

    /**
     * Observer event binding for action execution
     * 
     * @param \Oroboros\core\interfaces\library\action\ActionInterface $subject
     * @return void
     */
    public function onActionRun(\Oroboros\core\interfaces\library\action\ActionInterface $subject): void;

    /**
     * Observer event binding for action error status
     * 
     * @param \Oroboros\core\interfaces\library\action\ActionInterface $subject
     * @return void
     */
    public function onActionError(\Oroboros\core\interfaces\library\action\ActionInterface $subject): void;

    /**
     * Observer event binding for action that has completed running successfully.
     * This fires immediately prior to resettting.
     * 
     * @param \Oroboros\core\interfaces\library\action\ActionInterface $subject
     * @return void
     */
    public function onActionComplete(\Oroboros\core\interfaces\library\action\ActionInterface $subject): void;

    /**
     * Observer event binding for completion of an action action. This method
     * will only be called when the action manager returns to a clean state.
     * 
     * @param \Oroboros\core\interfaces\library\action\ActionInterface $subject
     * @return void
     */
    public function onActionReady(\Oroboros\core\interfaces\library\action\ActionInterface $subject): void;

    /**
     * Observer event binding for action callback execution
     * 
     * @param \Oroboros\core\interfaces\library\action\ActionInterface $subject
     * @return void
     */
    public function onActionCallbackRun(\Oroboros\core\interfaces\library\action\ActionInterface $subject): void;

    /**
     * Observer event binding for action callback error status
     * 
     * @param \Oroboros\core\interfaces\library\action\ActionInterface $subject
     * @return void
     */
    public function onActionCallbackError(\Oroboros\core\interfaces\library\action\ActionInterface $subject): void;

    /**
     * Observer event binding for action callback error status
     * 
     * @param \Oroboros\core\interfaces\library\action\ActionInterface $subject
     * @return void
     */
    public function onActionCallbackComplete(\Oroboros\core\interfaces\library\action\ActionInterface $subject): void;

    /**
     * Observer event binding for action callback binding
     * 
     * @param \Oroboros\core\interfaces\library\action\ActionInterface $subject
     * @return void
     */
    public function onActionCallbackBind(\Oroboros\core\interfaces\library\action\ActionInterface $subject): void;

    /**
     * Observer event binding for action callback unbinding
     * 
     * @param \Oroboros\core\interfaces\library\action\ActionInterface $subject
     * @return void
     */
    public function onActionCallbackUnbind(\Oroboros\core\interfaces\library\action\ActionInterface $subject): void;

    /**
     * Queues a new action in the specified action queue.
     * @param string $queue
     * @param array $details
     * @return $this (method chainable)
     * @throws \Oroboros\core\exception\InvalidArgumentException If the given queue is not valid
     */
    public function registerAction(string $queue, string $key): \Oroboros\core\interfaces\library\action\ActionManagerInterface;

    /**
     * Unregisters an action. All callbacks registered to run within the action
     * will be removed.
     * 
     * @param string $queue
     * @param string $key
     * @return \Oroboros\core\interfaces\library\action\ActionManagerInterface
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    public function unregisterAction(string $queue, string $key): \Oroboros\core\interfaces\library\action\ActionManagerInterface;

    /**
     * Lists the actions registered within a queue, if it is possible to do so
     * @param string $queue
     * @return array
     * @throws \Oroboros\core\exception\InvalidArgumentException If the given queue is not valid
     */
    public function listActions(string $queue): array;

    /**
     * Returns a boolean designation as to whether a given queue has
     * a registered action for it of a specific key.
     * 
     * Returns false if the queue does not exist or if the action does not have
     * a bound callback within the queue for the specified key.
     * 
     * @param string $queue
     * @param string $key
     * @return bool
     */
    public function hasAction(string $queue, string $key): bool;

    /**
     * Runs an entire action queue. All callbacks within the action
     * will be executed with the given arguments.
     * 
     * The return value of this method is intended to be determined by extension.
     * By default, an associative array of each callback return value will be
     * returned at the end of the execution. This may be further manipulated or
     * altered by child class implementation.
     * 
     * @param string $queue The queue containing the action to run
     * @param string $action The action to run
     * @param type $arguments Any arguments to pass to the action
     * @return type
     * @throws \Oroboros\core\exception\InvalidArgumentException If the specified
     *         queue does not exist, or the action does not exist in the specified
     *         queue
     */
    public function runAction(string $queue, string $action, $arguments = null);

    /**
     * Runs a specific action callback with the given arguments.
     * 
     * @param string $queue The queue containing the action
     * @param string $action The action containing the callback
     * @param string $key The callback identifier
     * @param array $arguments Arguments to pass to the callback (uses `call_user_func_array`)
     * @return mixed Child implementations will determine the output, if any
     * @throws \Oroboros\core\exception\InvalidArgumentException
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    public function runActionCallback(string $queue, string $action, string $key, array $arguments = []);

    /**
     * Binds a callback into the given action of the given queue,
     * which will run whenever the action fires.
     * 
     * This is the method external objects should use to hook into actions
     * defined by the subject.
     * 
     * If the given queue does not exist, or the given action does not exist,
     * an error exception will be raised.
     * 
     * An error exception will also be raised if the given key already has
     * an associated callback assigned for it.
     * 
     * @param string $queue The name of the queue
     * @param string $action The name of the action to bind to
     * @param string $key The identifier for the callback
     * @param callable $callback The operation to run when the action fires
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    public function bindActionCallback(string $queue, string $action, string $key, callable $callback): void;

    /**
     * Unbinds an existing callback registered to the specified action queue
     * under the given keyname. This will cause that callback to no longer
     * fire when the queue is run.
     * 
     * This will raise an exception if the queue does not exist, or if there
     * is no callback registered within it to the given key.
     * 
     * @param string $queue The name of the queue
     * @param string $action The name of the action the callback is bound to
     * @param string $key The identifier of the callback to remove
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    public function unbindActionCallback(string $queue, string $action, string $key): void;

    /**
     * Returns a boolean designation as to whether a callback exists with
     * a given keyname in the specified action of the specified queue.
     * 
     * Returns false if the queue, $action, or callback key do not exist.
     * Returns true if all three exist.
     * 
     * @param string $queue
     * @param string $action
     * @param string $key
     * @return bool
     */
    public function hasActionCallback(string $queue, string $action, string $key): bool;

    /**
     * Returns an array of callback key names registered into the given action
     * and queue specified.
     * 
     * Will throw an invalid argument exception if either the queue
     * or action specified do not exist.
     * 
     * @param string $queue
     * @param string $action
     * @return array
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    public function listActionCallbacks(string $queue, string $action): array;

    /**
     * Returns a ActionQueue object corresponding to the specified queue identifier
     * @param string $queue
     * @return \Oroboros\core\interfaces\library\action\ActionQueueInterface
     * @throws \Oroboros\core\exception\InvalidArgumentException If the given queue is not valid
     */
    public function getQueue(string $queue): \Oroboros\core\interfaces\library\action\ActionQueueInterface;

    /**
     * Returns a boolean determination as to whether a given queue is valid
     * 
     * @param string $queue
     * @return bool
     */
    public function hasQueue(string $queue): bool;

    /**
     * Registers an action queue into the master queue index.
     * 
     * @param string $queue
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    public function registerQueue(string $queue): void;

    /**
     * Removes an action queue from the master queue index
     * 
     * @param string $queue
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    public function unregisterQueue(string $queue): void;

    /**
     * Returns a list of the queues currently registered.
     * 
     * @return array
     */
    public function listQueues(): array;

    /**
     * Returns all applicable current observer event data
     * 
     * @return array
     */
    public function getCurrentDetails(): array;

    /**
     * Returns the queue currently being worked, if called during an action
     * or callback event, otherwise null.
     * 
     * @return string|null
     */
    public function getCurrentQueue(): ?string;

    /**
     * Returns the keyname of the current action being run if called during an
     * action or callback event, otherwise null
     * @return string|null
     */
    public function getCurrentActionId(): ?string;

    /**
     * Returns the current action being executed if called during an action
     * or callback event, otherwise null
     * 
     * @return \Oroboros\core\interfaces\library\action\ActionInterface|null
     */
    public function getCurrentAction(): ?\Oroboros\core\interfaces\library\action\ActionInterface;

    /**
     * Returns the keyname of the callback being currently fired if called
     * during an action or callback event, otherwise null
     * 
     * @return callable|null
     */
    public function getCurrentCallbackId(): ?string;

    /**
     * Returns the current callback being executed if called during an action
     * or callback event, otherwise null
     * 
     * @return callable|null
     */
    public function getCurrentCallback(): ?callable;

    /**
     * Returns the data supplied to pass to the currently executing action or
     * callback if called during an action or callback event, otherwise null
     * 
     * This will always be an array during an event, even if no data was passed.
     * 
     * @return array|null
     */
    public function getCurrentData(): ?array;

    /**
     * Returns the exception thrown by a callback or action when they are
     * running and throw any exception, otherwise null
     * 
     * @note any instance of `\Oroboros\core\exception\core\InvalidClassException`
     *       will be allowed to bubble up to the bootstrap object.
     * @return \Exception|null
     */
    public function getCurrentError(): ?\Exception;

    /**
     * Returns an array of the resulting values returned from each callback
     * that has run during an action event, or null if no action is running
     * 
     * @return array|null
     */
    public function getCurrentResult(): ?array;

    /**
     * Returns the value of the last successfully run callback if called during
     * an action or callback event, otherwise null.
     * 
     * @note either of those may also cause this to be null if
     *       the specific action or callback returns null or void.
     * @return array|null
     */
    public function getCurrentCallbackResult();

    /**
     * Marks a currently running action or callback error as recoverable,
     * which will surpress it bubbling up to the main thread.
     * 
     * Recoverable status is always reset to false after it applies
     * 
     * @return void
     */
    public function markErrorRecoverable(): void;

    /**
     * Returns a boolean determination as to whether a currently scoped action
     * or callback error is marked as resolved.
     * 
     * @return bool
     */
    public function isErrorRecoverable(): bool;

    /**
     * Marks a currently running action as resolved. No further callbacks will
     * fire and it will return it's existing results immediately.
     * 
     * Resolved status is always reset immediately after it is applied
     * 
     * @return void
     */
    public function markActionResolved(): void;

    /**
     * Checks whether a given running action has been marked as resolved.
     * 
     * @return bool
     */
    public function isActionResolved(): bool;

    /**
     * Verifies that an action can be called for the given queue.
     * This will throw an exception if the queue/action is not valid,
     * but will not call any heavy operations during validation.
     * @param string $queue
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException If the given queue or worker is not valid
     */
    public function verify(string $queue): void;
}
