<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\interfaces\library\action;

/**
 * Action Interface
 * Defines the public methods for the action api
 * 
 * Actions run sets of callbacks organized under a single common group name,
 * and report back to observers during the execution steps of them.
 * 
 * During execution, they provide an api to allow parent objects to mark errors
 * as recoverable or break operation without running any further callbacks.
 * This can be achieved when the callbacks bound to their observer events fire,
 * allowing parent objects granular control over what happens within them,
 * what is allowed to register callbacks, and most other related criteria
 * to runtime.
 * 
 * They can be used to implement any manner of system that requires running a
 * sequence of callbacks, such as events, filters, a job system, or any number
 * of other things. In doing so, they insure that the controlling object retains
 * control and is aware of execution throughout all operations.
 * 
 * 
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
interface ActionInterface extends \Oroboros\core\interfaces\pattern\director\WorkerInterface, \Oroboros\core\interfaces\library\LibraryInterface, \Oroboros\core\interfaces\pattern\observer\ObservableInterface
{

    /**
     * Runs the action.
     * 
     * @param type $args
     */
    public function run($args = null);

    /**
     * This marks an error caught as recoverable, which allows a parent object
     * to surpress the exception from bubbling up further in the main runtime.
     * 
     * This value will always be reset to false after each recoverred error.
     * 
     * @return void
     */
    public function markErrorRecoverable(): void;

    /**
     * Returns whether anything has marked the current error recoverable.
     * 
     * @return bool
     */
    public function isErrorRecoverable(): bool;

    /**
     * This marks an action as resolved, which will break further callbacks
     * within the action from running. The existing results
     * will be returned as-is.
     * 
     * This value will always be reset to false after each recovered error.
     * 
     * @return void
     */
    public function markActionResolved(): void;

    /**
     * Returns whether a running action has been marked as resolved
     * 
     * @return bool
     */
    public function isResolved(): bool;

    /**
     * Returns the name of the action
     * @return string
     */
    public function getName(): string;

    /**
     * Binds the given subject to the action. There must not be a subject
     * currently bound, and the subject must pass any criteria from
     * `verifySubject` prior to binding.
     * 
     * If any of these are not the case, an error exception will be raised.
     * 
     * @param \Oroboros\core\interfaces\library\action\ActionManagerInterface $subject
     * @return void
     * @throws \Oroboros\core\exception\ErrorException If there is already a
     *         bound subject, or if the given subject does not pass any criteria
     *         from method `verifySubject`
     */
    public function bind(\Oroboros\core\interfaces\library\action\ActionManagerInterface $subject): void;

    /**
     * Unbinds the subject from the action and restores the action to it's unbound state.
     * The `$subject` argument must **exactly match** the object internally held
     * in the subject binding.
     * 
     * If there is no currently bound subject or if the given subject
     * does not match, an error exception will be raised.
     * 
     * @param \Oroboros\core\interfaces\library\action\ActionManagerInterface $subject
     * @return void
     * @throws \Oroboros\core\exception\ErrorException if the given subject
     *         does not exactly match the bound subject, or if there is no
     *         currently bound subject.
     */
    public function unbind(\Oroboros\core\interfaces\library\action\ActionManagerInterface $subject): void;

    /**
     * Runs a specific action callback.
     * 
     * @param string $key the identifier of the callback
     * @param array $args Any arguments to pass to the callback
     * @param boolean $skip_reset Used internally to designate that this call
     *        is part of a broader running event and the call should not
     *        explicitly reset arguments.
     * @return mixed
     * @throws \Oroboros\core\exception\InvalidArgumentException
     *         if the specified callback does not exist.
     */
    public function runCallback(string $key, array $args = [], bool $skip_reset = false);

    /**
     * Returns a boolean designation as to whether a given callback key exists
     * 
     * @param string $key
     * @return bool
     */
    public function hasCallback(string $key): bool;

    /**
     * Returns the callable registered for the specified key
     * 
     * @param string $key
     * @return callable
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    public function getCallback(string $key): callable;

    /**
     * Returns an array of registered callbacks for the action.
     * 
     * @return array
     */
    public function listCallbacks(): array;

    /**
     * Registers a callback against the action with the given key.
     * 
     * Throws an exception if the key is already in use.
     * 
     * @param string $key
     * @param callable $callback
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    public function registerCallback(string $key, callable $callback): void;

    /**
     * Removes a callback registered by the given key.
     * 
     * Throws an exception if the key does not exist.
     * 
     * @param string $key
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    public function unregisterCallback(string $key): void;

    /**
     * Director pattern  compatibility
     * 
     * @param array $arguments
     * @param array $flags
     * @return mixed
     */
    public function executeTask(array $arguments = [], array $flags = []);

    /**
     * Returns an array of all current observer event reporting keys
     * 
     * @return array
     */
    public function getCurrentAction(): array;

    /**
     * Getter for the current action results during a "run" or "error" event,
     * which will be an array of each subsequent return value of the callbacks
     * referenced during the action up to that point. It will be null in all
     * other cases.
     * 
     * @return array|null
     */
    public function getCurrentActionResults(): ?array;

    /**
     * Getter for the current action error during the "error" observer event.
     * It will be null in all other cases.
     * 
     * @return \Exception|null
     */
    public function getCurrentActionError(): ?\Exception;

    /**
     * Getter for the currently scoped action arguments. This will have an array
     * value during the observer event "run", during "error", and during "complete"
     * if "complete" was fired at the end of an action run event. It will be null
     * in all other cases.
     * 
     * @return array|null
     */
    public function getCurrentActionArguments(): ?array;

    /**
     * Getter for the currently scoped callback id. This occurs when bound,
     * unbound, an action is run, or a single callback is run.
     * Null in all other cases.
     * 
     * @return string|null
     */
    public function getCurrentActionCallbackId(): ?string;

    /**
     * Getter for the currently scoped callback. This occurs when bound,
     * unbound, an action is run, or a single callback is run.
     * Null in all other cases.
     * 
     * @return callable|null
     */
    public function getCurrentActionCallback(): ?callable;

    /**
     * Getter for the arguments passed to the last successfully run callback
     * when the "callback-complete" or "run-callback" event is active.
     * Null in all other cases.
     * 
     * @return type
     */
    public function getCurrentActionCallbackArguments(): ?array;

    /**
     * Getter for the results of the last successfully run callback when the "callback-complete"
     * event is active. Null in all other cases.
     * 
     * @note individual callbacks may explicitly return `null`
     *       or not return anything, in which case it will
     *       still be `null`.
     * @return type
     */
    public function getCurrentActionCallbackResults();

    /**
     * Getter for the current action callback error when the "callback-error"
     * event is active. Null in all other cases.
     * 
     * @return \Exception|null
     */
    public function getCurrentActionCallbackError(): ?\Exception;
}
