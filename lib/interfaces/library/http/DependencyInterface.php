<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\interfaces\library\http;

/**
 * Dependency Interface
 * Defines a standard set of operations for registration and removal of dependencies
 * (scripts, styles, fonts, etc)
 * @author Brian Dayhoff
 */
interface DependencyInterface extends \Oroboros\core\interfaces\library\LibraryInterface
{

    /**
     * Returns the semantic tag used to identify the resource in html source.
     * @return string
     */
    public function __toString();

    /**
     * Sets the source location.
     * May be a relative or absolute uri.
     * If a relative link is passed, the filesystem will be checked for the resource.
     * 
     * @param string $source
     * @return $this (method chainable)
     */
    public function withSource(string $source): \Oroboros\core\interfaces\library\http\DependencyInterface;

    /**
     * Sets the sourcemap location, if applicable.
     * May be a relative or absolute uri.
     * If a relative link is passed, the filesystem will be checked for the resource.
     * 
     * @param string $source_map
     * @return $this (method chainable)
     */
    public function withSourceMap(string $source_map): \Oroboros\core\interfaces\library\http\DependencyInterface;

    /**
     * Removes the registered sourcemap location, if any exists.
     * @param string $source_map
     * @return $this (method chainable)
     */
    public function withoutSourceMap(): \Oroboros\core\interfaces\library\http\DependencyInterface;

    /**
     * Sets the cdn location, if applicable.
     * Must be an absolute uri.
     * If integrity and crossorigin user are required, those methods must be called separately.
     * 
     * @param string $cdn
     * @return $this (method chainable)
     */
    public function withCdn(string $cdn): \Oroboros\core\interfaces\library\http\DependencyInterface;

    /**
     * Removes the cdn location, if any exists.
     * @return $this (method chainable)
     */
    public function withoutCdn(): \Oroboros\core\interfaces\library\http\DependencyInterface;

    public function withVersion(string $version): \Oroboros\core\interfaces\library\http\DependencyInterface;

    /**
     * Returns the key name of the dependency.
     * @return string
     */
    public function getKey(): string;

    /**
     * Returns the source uri for the dependency.
     * @return \Psr\Http\Message\UriInterface|null
     */
    public function getSource(): ?\Psr\Http\Message\UriInterface;

    /**
     * Returns the source uri for the cdn, if applicable.
     * Returns null if no cdn is registered, or if the USE_CDN flag is not not set or is false.
     * @return \Psr\Http\Message\UriInterface|null
     */
    public function getCdn(): ?\Psr\Http\Message\UriInterface;

    /**
     * Gets the host name of the dependency.
     * If the dependency supplies a relative uri, this will always be the local host name.
     * 
     * @return \Psr\Http\Message\UriInterface|null
     */
    public function getHost(): ?\Psr\Http\Message\UriInterface;

    /**
     * Returns the version, if it exists.
     * @return string|null
     */
    public function getVersion(): ?string;

    /**
     * Returns a list of any expected fonts required by the dependency.
     * @return array
     */
    public function getExpectedFonts(): array;

    /**
     * Returns a list of any expected scripts required by the dependency.
     * @return array
     */
    public function getExpectedScripts(): array;

    /**
     * Returns a list of any expected stylesheets required by the dependency.
     * @return array
     */
    public function getExpectedStylesheets(): array;

    /**
     * Gets the prefetch link for the dependency.
     * @return string
     */
    public function getPrefetch(): string;

    /**
     * Gets the preload link for the dependency.
     * @return string
     */
    public function getPreload(): string;

    /**
     * Gets the preconnect link for the dependency host.
     * @return string
     */
    public function getPreconnect(): string;

    /**
     * Gets the sha1 integrity hash if one exists, or null if one does not exist.
     * @return string|null
     */
    public function getIntegrity(): ?string;

    /**
     * Gets the crossorigin username if one exists, or null if one does not exist.
     * @return string|null
     */
    public function getCrossOrigin(): ?string;

    /**
     * Gets an array of the dependencies that must be included
     * prior to the inclusion of the dependency.
     * @return array
     */
    public function getDependencies(): array;
}
