<?php
namespace Oroboros\core\interfaces\library\parser;

/**
 * Establishes a standard approach to parsing data formats.
 * Makes data objects iterable, in accordance with the PHP \Iterator interface,
 * which lets them be used in loops. Also implements \ArrayAccess,
 * so most array driven PHP internals will operate on the object as if it
 * were an array (array_key_exists being the outlier)
 * 
 * @author Brian Dayhoff
 */
interface ParserInterface
extends \Oroboros\core\interfaces\library\LibraryInterface, \Iterator, \ArrayAccess
{

    /**
     * Returns the value for the current iteration
     * @return mixed
     */
    public function current();

    /**
     * Returns the key for the current iteration
     * @return scalar
     */
    public function key();

    /**
     * Increments the iterator
     * @return void
     */
    public function next();

    /**
     * @return void
     */
    public function rewind();

    /**
     * Returns a boolean determination as to whether any remaining interations exist
     * @return bool
     */
    public function valid();

    /**
     * Checks whether a key exists
     * @param mixed $offset A scalar value of the specified offset (underlying implementation requires mixed, not scalar)
     */
    public function offsetExists($offset);

    /**
     * Gets a key
     * @param mixed $offset A scalar value of the specified offset (underlying implementation requires mixed, not scalar)
     */
    public function offsetGet($offset);

    /**
     * Sets a key
     * @param mixed $offset A scalar value of the specified offset (underlying implementation requires mixed, not scalar)
     */
    public function offsetSet($offset, $value);

    /**
     * Unsets a key
     * @param mixed $offset A scalar value of the specified offset (underlying implementation requires mixed, not scalar)
     */
    public function offsetUnset($offset);
}
