<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\interfaces\library\database\format\mysql;

/**
 * Mysql Format Interface
 * Defines the SQL syntax elements and PDO driver used for
 * mysql connections and query/procedure formatting.
 * 
 * @author Brian Dayhoff
 */
interface FormatInterface extends \Oroboros\core\interfaces\library\database\format\DatabaseFormatInterface
{

    const DEFAULT_HOST = '127.0.0.1';
    const DEFAULT_PORT = 3306;
    const LEFT_DELIMITER = '`';
    const RIGHT_DELIMITER = '`';
    const SEPARATOR = '.';
    const DELIMITER = ';';
    const TEMP_DELIMITER = '$$';

    /**
     * The PDO Driver, and the identifying key for the scope of the datase related class
     */
    const DATABASE_DRIVER = 'mysql';
    const INFORMATION_SCHEMA = 'INFORMATION_SCHEMA';
    const INFORMATION_TABLE_SCHEMAS = 'SCHEMATA';
    const INFORMATION_TABLE_TABLES = 'TABLES';
    const INFORMATION_TABLE_VIEWS = 'VIEWS';
    const INFORMATION_TABLE_COLUMNS = 'COLUMNS';
    const INFORMATION_TABLE_INDEXES = 'STATISTICS';
    const INFORMATION_TABLE_RELATIONSHIPS = 'KEY_COLUMN_USAGE';
    const INFORMATION_SCHEMA_IDENTIFIER = 'SCHEMA_NAME';    // In the event this is different from the selector on all other tables
    const INFORMATION_SCHEMA_SELECTOR = 'TABLE_SCHEMA';
    const INFORMATION_TABLE_SELECTOR = 'TABLE_NAME';
    const INFORMATION_COLUMN_SELECTOR = 'COLUMN_NAME';
    const INFORMATION_VIEW_SELECTOR = 'TABLE_NAME';
    const INFORMATION_INDEX_SELECTOR = 'INDEX_NAME';
    const INFORMATION_RELATIONSHIP_SELECTOR = 'CONSTRAINT_NAME';
    const INFORMATION_TYPE_SELECTOR = 'DATA_TYPE';
    const INFORMATION_POSITION_SELECTOR = 'ORDINAL_POSITION';
    const INFORMATION_PRIVILEGES_SELECTOR = 'PRIVILEGES';
    const PRIMARY_KEY_SELECTOR = 'PRIMARY';
    const RELATIONSHIP_SCHEMA_SELECTOR = 'REFERENCED_TABLE_SCHEMA';
    const RELATIONSHIP_TABLE_SELECTOR = 'REFERENCED_TABLE_NAME';
    const RELATIONSHIP_COLUMN_SELECTOR = 'REFERENCED_COLUMN_NAME';

    /**
     * Database type connection class
     */
    const CONNECTION_CLASS = 'database\\connection\\MySQL';

    /**
     * Database type query class
     */
    const QUERY_CLASS = 'database\\query\\MySQLQuery';

    /**
     * Database type query condition class
     */
    const QUERY_CONDITION_CLASS = 'database\\query\\MySQLClause';

    /**
     * Database type query join class
     */
    const QUERY_JOIN_CLASS = 'database\\query\\MySQLJoin';

    /**
     * Database type schema class
     */
    const SCHEMA_CLASS = 'database\\schema\\MySQLSchema';

    /**
     * Database type table class
     */
    const TABLE_CLASS = 'database\\table\\MySQLTable';

    /**
     * Database type column class
     */
    const COLUMN_CLASS = 'database\\column\\MySQLColumn';

    /**
     * Database type column class
     */
    const INDEX_CLASS = 'database\\index\\MySQLIndex';

    /**
     * Database type column class
     */
    const RELATIONSHIP_CLASS = 'database\\relationship\\MySQLRelationship';

    /**
     * Database type view class
     */
    const VIEW_CLASS = 'database\\view\\MySQLView';

    /**
     * Database type view class
     */
    const FUNCTION_CLASS = 'database\\dbfunction\\MySQLFunction';

    /**
     * Database type view class
     */
    const PROCEDURE_CLASS = 'database\\procedure\\MySQLProcedure';

    /**
     * Database type view class
     */
    const TRIGGER_CLASS = 'database\\trigger\\MySQLTrigger';

}
