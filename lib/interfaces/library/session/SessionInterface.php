<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\interfaces\library\session;

/**
 *
 * @author Brian Dayhoff
 */
interface SessionInterface extends SessionCommonInterface
{

    /**
     * Starts the session if it is not already started.
     * This method may be safely called on an already-active session.
     * @return $this (method chainable)
     */
    public function start(): \Oroboros\core\interfaces\library\session\SessionInterface;

    /**
     * Destroys the current session.
     * @return $this (method chainable)
     */
    public function destroy(): \Oroboros\core\interfaces\library\session\SessionInterface;

    /**
     * Synchronizes the current session container to
     * the currently defined session keys.
     * 
     * This method can be used to synchronize the current session object
     * in the event that external changes directly to the superglobal occur.
     * 
     * The session will be started if it is not currently active to enable this operation.
     * 
     * @return $this (method chainable)
     */
    public function sync(): \Oroboros\core\interfaces\library\session\SessionInterface;

    /**
     * Resets the session, so a new session token will be issued.
     * All existing keys are persisted into the new session,
     * but a new session cookie will be issued for the new one.
     * 
     * The session will be started if it is not currently active to enable this operation.
     * 
     * @return $this (method chainable)
     */
    public function reset(): \Oroboros\core\interfaces\library\session\SessionInterface;

    /**
     * Flushes all keys from the current session so it is in an empty state.
     * 
     * The session will be started if it is not currently active to enable this operation.
     * 
     * @return $this (method chainable)
     */
    public function flush(): \Oroboros\core\interfaces\library\session\SessionInterface;

    /**
     * Populates the session with the provided array of values.
     * Existing keys are replaced if provided.
     * Existing keys not provided are retained.
     * 
     * The session will be started if it is not currently active to enable this operation.
     * 
     * @param array $values
     * @return $this (method chainable)
     */
    public function populate(array $values): \Oroboros\core\interfaces\library\session\SessionInterface;

    /**
     * Returns a boolean determination as to whether the currently active session is empty.
     * 
     * The session will be started if it is not currently active to make this determination.
     * 
     * @return bool
     */
    public function isEmpty(): bool;

    /**
     * Returns a boolean determination as to whether the session is currently active.
     * @return bool
     */
    public function isActive(): bool;

    /**
     * Retrieves the given key from the session
     * 
     * The session will be started if it is not currently active to enable this operation.
     * 
     * @param string $key
     * @return mixed
     */
    public function get(string $key);

    /**
     * Sets the given key into the session as the provided value
     * 
     * The session will be started if it is not currently active to enable this operation.
     * 
     * @param string $key
     * @param type $value
     * @return $this (method chainable)
     */
    public function set(string $key, $value): \Oroboros\core\interfaces\library\session\SessionInterface;

    /**
     * Returns a boolean determination as to whether the given key exists in the session.
     * 
     * The session will be started if it is not currently active to make this determination.
     * 
     * @param string $key
     * @return bool
     */
    public function has(string $key): bool;

    /**
     * Returns all keys currently defined in the session as an array.
     * The returned array is decoupled from the actual session superglobal,
     * and represents only a snapshot of their state when the method is called.
     * 
     * The session will be started if it is not currently active to enable this operation.

     * @return array
     */
    public function fetch(): array;

    /**
     * Closes the session and discards any variables altered since runtime started.
     * Does not destroy the session, it can be started on another execution
     * in the same state it was in when this runtime operation started.
     * 
     * Does nothing if the session is not currently open.
     * 
     * @return $this (method chainable)
     */
    public function abort(): \Oroboros\core\interfaces\library\session\SessionInterface;

    /**
     * Closes the session and persists any variables altered since runtime started.
     * Does not destroy the session, it can be started on another execution
     * in the same state it was closed in.
     * 
     * Does nothing if the session is not currently open.
     * 
     * @return $this (method chainable)
     */
    public function close(): \Oroboros\core\interfaces\library\session\SessionInterface;
}
