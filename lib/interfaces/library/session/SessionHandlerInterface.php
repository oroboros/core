<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\interfaces\library\session;

/**
 * Session Handler Interface
 * Defines methods required to satisfy the underlying
 * php session handler api in the context of oroboros
 * 
 * This uses the php7 oop functionality for session handling,
 * which is not backwards compatible to php versions prior to php 7.0
 * 
 * This will allow objects implementing this interface to cover all aspects
 * of php internals when set as a session handler with `session_set_save_handler`
 * 
 * Method documentation is derived directly from the php.net specifications.
 * 
 * @link https://www.php.net/manual/en/function.session-set-save-handler.php
 * @link https://www.php.net/manual/en/class.sessionhandlerinterface.php
 * @author Brian Dayhoff
 */
interface SessionHandlerInterface extends SessionCommonInterface, \SessionHandlerInterface, \SessionIdInterface, \SessionUpdateTimestampHandlerInterface
{

    /**
     * Opens the session
     * 
     * The open callback works like a constructor in classes and is executed
     * when the session is being opened.
     * 
     * It is the first callback function executed when the session is started
     * automatically or manually with `session_start()`.
     * 
     * Return value is `true` for success, `false` for failure.
     * 
     * @param string $path
     * @param string $name
     * @return bool
     * @see \SessionHandlerInterface
     * @link https://www.php.net/manual/en/function.session-set-save-handler.php
     */
    public function open(string $path, string $name);

    /**
     * Closes the session
     * 
     * The close callback works like a destructor in classes and is executed
     * after the session write callback has been called.
     * 
     * It is also invoked when `session_write_close()` is called.
     * Return value should be `true` for success, `false` for failure.
     * 
     * @return bool
     * @see \SessionHandlerInterface
     * @link https://www.php.net/manual/en/function.session-set-save-handler.php
     */
    public function close(): bool;

    /**
     * Reads from the session
     * 
     * The read callback must always return a session encoded (serialized) string,
     * or an empty string if there is no data to read.
     * 
     * This callback is called internally by PHP when the session starts or
     * when `session_start()` is called.
     * 
     * Before this callback is invoked PHP will invoke the `open` callback.
     * 
     * The value this callback returns must be in exactly the same serialized format
     * that was originally passed for storage to the write callback.
     * 
     * The value returned will be unserialized automatically by PHP and used to
     * populate the `$_SESSION` superglobal.
     * 
     * While the data looks similar to `serialize()` please note it is a
     * different format which is specified in the `session.serialize_handler`
     * ini setting.
     * 
     * @param type $id
     * @return string
     * @see \SessionHandlerInterface
     * @link https://www.php.net/manual/en/function.session-set-save-handler.php
     */
    public function read(string $id): string;

    /**
     * Writes to the session
     * 
     * The write callback is called when the session needs to be saved and closed.
     * 
     * This callback receives the current session ID a serialized version
     * the `$_SESSION` superglobal.
     * 
     * The serialization method used internally by PHP is specified in the
     * `session.serialize_handler` ini setting.
     * 
     * The serialized session data passed to this callback should be stored against
     * the passed session ID.
     * When retrieving this data, the read callback must return the exact value
     * that was originally passed to the write callback.
     * 
     * This callback is invoked when PHP shuts down or explicitly
     * when `session_write_close()` is called.
     * 
     * Note that after executing this function PHP will internally execute
     * the `close` callback.
     * 
     * @note The "write" handler is not executed until after the output stream
     *       is closed. Thus, output from debugging statements in the "write"
     *       handler will never be seen in the browser. If debugging output
     *       is necessary, it is suggested that the debug output be written
     *       to a file instead.
     * 
     * @param string $id
     * @param string $data
     * @see \SessionHandlerInterface
     * @link https://www.php.net/manual/en/function.session-set-save-handler.php
     */
    public function write(string $id, string $data);

    /**
     * Destroys the session
     * 
     * This callback is executed when a session is destroyed
     * with `session_destroy()` or with `session_regenerate_id()` with the
     * destroy parameter set to `true`.
     * 
     * Return value should be `true` for success, `false` for failure.
     * 
     * @param string $id
     * @return bool
     * @see \SessionHandlerInterface
     * @link https://www.php.net/manual/en/function.session-set-save-handler.php
     */
    public function destroy(string $id): bool;

    /**
     * Garbage collection
     * 
     * The garbage collector callback is invoked internally by PHP periodically
     * in order to purge old session data.
     * 
     * The frequency is controlled by `session.gc_probability` and `session.gc_divisor`.
     * 
     * The value of lifetime which is passed to this callback can be set
     * in `session.gc_maxlifetime`.
     * 
     * Return value should be `true` for success, `false` for failure.
     * 
     * @param int $max_lifetime
     * @return int|false
     * @see \SessionHandlerInterface
     * @link https://www.php.net/manual/en/function.session-set-save-handler.php
     */
    public function gc(int $max_lifetime);

    /**
     * Creates a session id
     * 
     * This callback is executed when a new session ID is required.
     * 
     * No parameters are provided, and the return value should be a string that
     * is a valid session ID for your handler.
     * 
     * @return string
     * @see \SessionIdInterface
     * @link https://www.php.net/manual/en/function.session-set-save-handler.php
     */
    public function create_sid(): string;

    /**
     * 
     * This callback is executed when a session is updated.
     * `id` is the session ID, `data` is the session data.
     * 
     * The return value should be `true` for success, `false` for failure.
     * 
     * @param string $id
     * @param string $data
     * @return bool
     * @see \SessionUpdateTimestampHandlerInterface
     * @link https://www.php.net/manual/en/function.session-set-save-handler.php
     */
    public function updateTimestamp(string $id, string $data): bool;

    /**
     * Session ID validation
     * 
     * This callback is executed when a session is to be started, a session ID
     * is supplied and `session.use_strict_mode` is enabled.
     * 
     * The key is the session ID to validate. A session ID is valid, if a session
     * with that ID already exists.
     * 
     * The return value should be `true` for success, `false` for failure.
     * 
     * @param string $id
     * @return bool
     * @see \SessionUpdateTimestampHandlerInterface
     * @link https://www.php.net/manual/en/function.session-set-save-handler.php
     */
    public function validateId(string $id): bool;
}
