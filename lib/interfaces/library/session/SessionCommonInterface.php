<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\interfaces\library\session;

/**
 * Session Common Interface
 * Interface shared by all session related objects
 * 
 * Classes implementing this interface are declaring that
 * their purpose is related to session management.
 * 
 * @satisfiedBy \Oroboros\core\traits\library\session\SessionCommonTrait
 * @see \Oroboros\core\traits\library\session\SessionCommonTrait
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
interface SessionCommonInterface extends \Oroboros\core\interfaces\library\LibraryInterface
{

    /**
     * Returns the session type designated by the class.
     * 
     * @return string|null
     */
    public function getSessionType(): string;

    /**
     * Returns the session scope designated by the class if it is declared,
     * or null if it is not.
     * 
     * @return string|null
     */
    public function getSessionScope(): ?string;
}
