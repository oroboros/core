<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\interfaces\library\event;

/**
 * Event Aware Interface
 * Designates that a class is capable of registering events and handling
 * event callbacks
 * 
 * Classes implementing this interface will allow external objects to register
 * callbacks against their declared event bindings. These callbacks will then
 * run in sequence when a given event fires.
 * 
 * @note There is a trait that fully honors this interface
 * @see \Oroboros\core\traits\pattern\EventAwareTrait
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
interface EventAwareInterface extends \Oroboros\core\interfaces\library\action\ActionAwareInterface
{

    /**
     * Returns a boolean determination as to whether the given object
     * is an event watcher. This is object specific, not class specific.
     * 
     * If the second optional argument is passed as `true`, it will instead
     * return if any class in the watch list matches the class of the given watcher.
     * 
     * @param \Oroboros\core\interfaces\pattern\observer\ObserverInterface $watcher
     * @param bool $check_class If this argument is passed as true, this method
     *        will return whether *any* existing registered watcher matches the
     *        same class as the given watcher. This is useful in the event that
     *        classes are intended to only serve as a registration mechanism,
     *        and should not double-register events.
     * @return bool
     */
    public static function isWatchingEvents(\Oroboros\core\interfaces\pattern\observer\ObserverInterface $watcher, bool $check_class = false): bool;

    /**
     * Adds an object to the watcher list, which will alert it to registration 
     * or unregistration of events, so it can add it's callbacks.
     * 
     * Any object in the watch list will be automatically attached to all new
     * instances of the event aware class.
     * 
     * Objects added to the watch list **will not** be automatically attached
     * to existing instances of the class. They must be added to the watch list
     * prior to the creation of those objects if they need to hook into them.
     * 
     * Watchers will automatically be attached during object instantiation
     * and detached during object destruction.
     * 
     * @param \Oroboros\core\interfaces\pattern\observer\ObserverInterface $watcher
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException
     *         If the given watcher is already in the watch list.
     */
    public static function watchEvents(\Oroboros\core\interfaces\pattern\observer\ObserverInterface $watcher): void;

    /**
     * Removes an object from the watcher list, if it exists in it.
     * 
     * This will not trigger existing objects in the watcher list to detach,
     * it will only prevent new object instances from automatically attaching.
     * 
     * If the object does not exist in the list of watchers, it will raise an InvalidArgumentException
     * 
     * @param \Oroboros\core\interfaces\pattern\observer\ObserverInterface $watcher
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException if the given
     *         watcher is not a registered event watcher.
     */
    public static function ignoreEvents(\Oroboros\core\interfaces\pattern\observer\ObserverInterface $watcher): void;

    /**
     * Observer event binding for event registration
     * 
     * @param \Oroboros\core\interfaces\library\event\EventManagerInterface $subject
     */
    public function onEventRegister(\Oroboros\core\interfaces\library\event\EventManagerInterface $subject): void;
    
    /**
     * Reinitializes event watchers.
     * 
     * This should be used if the underlying list of event
     * watchers changed since class instantiation.
     * 
     * This will tell the object to release the existing bindings
     * and rebind to the updated master list.
     * 
     * @return void
     */
    public function reinitializeEventWatchers(): void;

    /**
     * Observer event binding for event unregistration
     * 
     * @param type $subject
     * @return void
     */
    public function onEventUnregister(\Oroboros\core\interfaces\library\event\EventManagerInterface $subject): void;

    /**
     * Observer event binding for event execution
     * 
     * @param type $subject
     * @return void
     */
    public function onEventFire(\Oroboros\core\interfaces\library\event\EventManagerInterface $subject): void;

    /**
     * Observer event binding for event error status
     * 
     * @param type $subject
     * @return void
     */
    public function onEventError(\Oroboros\core\interfaces\library\event\EventManagerInterface $subject): void;

    /**
     * Observer event binding for event that has completed running successfully.
     * This fires immediately prior to resettting.
     * 
     * @param type $subject
     * @return void
     */
    public function onEventComplete(\Oroboros\core\interfaces\library\event\EventManagerInterface $subject): void;

    /**
     * Observer event binding for completion of an event action. This method
     * will only be called when the event manager returns to a clean state.
     * 
     * @param type $subject
     * @return void
     */
    public function onEventReady(\Oroboros\core\interfaces\library\event\EventManagerInterface $subject): void;

    /**
     * Observer event binding for event callback execution
     * 
     * @param type $subject
     * @return void
     */
    public function onEventCallbackFire(\Oroboros\core\interfaces\library\event\EventManagerInterface $subject): void;

    /**
     * Observer event binding for event callback error status
     * 
     * @param type $subject
     * @return void
     */
    public function onEventCallbackError(\Oroboros\core\interfaces\library\event\EventManagerInterface $subject): void;

    /**
     * Observer event binding for event callback error status
     * 
     * @param type $subject
     * @return void
     */
    public function onEventCallbackComplete(\Oroboros\core\interfaces\library\event\EventManagerInterface $subject): void;

    /**
     * Observer event binding for event callback binding
     * 
     * @param type $subject
     * @return void
     */
    public function onEventCallbackBind(\Oroboros\core\interfaces\library\event\EventManagerInterface $subject): void;

    /**
     * Observer event binding for event callback unbinding
     * 
     * @param type $subject
     * @return void
     */
    public function onEventCallbackUnbind(\Oroboros\core\interfaces\library\event\EventManagerInterface $subject): void;

    /**
     * Observer event binding for event queue registration
     * 
     * @param type $subject
     * @return void
     */
    public function onEventQueueRegister(\Oroboros\core\interfaces\library\event\EventManagerInterface $subject): void;

    /**
     * Observer event binding for event queue unregistration
     * 
     * @param type $subject
     * @return void
     */
    public function onEventQueueUnregister(\Oroboros\core\interfaces\library\event\EventManagerInterface $subject): void;

    /**
     * Returns a boolean designation as to whether a given event queue exists.
     * 
     * @param string $queue
     * @return bool
     */
    public function hasEventQueue(string $queue): bool;

    /**
     * Returns a list of the names of valid queues for this object.
     * 
     * @return array
     */
    public function listEventQueues(): array;

    /**
     * Returns a boolean designation as to whether a given event exists
     * in the current scope, or in an arbitrarily specified scope if the
     * second argument is passed.
     * 
     * If this returns false, this class does not have the specified event,
     * and attempts to bind callbacks to it will raise an exception.
     * 
     * If this returns true, the event is valid and callbacks may be bound to it,
     * provided they are not bound to an existing key name.
     * 
     * @param string $event The event name
     * @param string|null $queue The queue name if supplied, or the currently scoped queue if not
     * @return bool
     */
    public function hasEvent(string $event, string $queue = null): bool;

    /**
     * Lists all the events in the given queue, or the currently scoped
     * queue if no queue is supplied.
     * 
     * @param string $queue
     * @return array
     * @throws \Oroboros\core\exception\InvalidArgumentException
     *         if a queue name is given that does not exist.
     */
    public function listEvents(string $queue = null): array;

    /**
     * Returns a boolean designation as to whether a given event has a bound
     * callback of the specified keyname
     * 
     * This will return false if the event does not exist, or if it does but
     * the given key is not bound to it.
     * 
     * If `hasEvent` returns `true` for the same event and this method
     * returns `false`, then it is safe to bind an event callback for
     * the specified key.
     * 
     * If both this method and `hasEvent` return `false` for the same event,
     * or if both return `true`, attempting to bind a callback with the
     * specified key will raise an exception. In the first case, the event does
     * not exist, and in the second, the key is already in use.
     * 
     * @param string $event The event name
     * @param string $key The identifier for the given callback
     * @param string|null $queue The queue name if supplied, or the currently scoped queue if not
     * @return bool
     */
    public function hasEventCallback(string $event, string $key, string $queue = null): bool;

    /**
     * Binds a callback to the given event in the specified queue,
     * under the name of `$key`
     * 
     * @param string $event The name of the event to bind the callback to
     * @param string $key The identifier for the callback
     * @param callable $callback The callback to fire when the event fires
     * @param string|null $queue The queue to bind a callback into. If no queue 
     *        is supplied, the currently scoped queue will be used.
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException if the manager
     *         reports an error binding the callback. This can happen if the
     *         callback does not validate, or if the key is already registered.
     */
    public function bindEventCallback(string $event, string $key, callable $callback, string $queue = null): void;

    /**
     * Removes a given event callback from the event and queue specified.
     * If no queue is specified, the currently scoped queue will be used.
     * 
     * @param string $event The name of the event to bind the callback to
     * @param string $key The identifier for the callback
     * @param string|null $queue The queue to bind a callback into. If no queue 
     *        is supplied, the currently scoped queue will be used.
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    public function unbindEventCallback(string $event, string $key, string $queue = null): void;

    /**
     * Lists the event callbacks in the specified event of the given queue, or
     * the currently scoped queue if no queue identifier is passed.
     * 
     * @param string $event
     * @param string $queue
     * @return array
     * @throws \Oroboros\core\exception\InvalidArgumentException
     *         If a given event or queue do not exist.
     */
    public function listEventCallbacks(string $event, string $queue = null): array;
}
