<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\interfaces\library\component;

/**
 * Defines the standard functionality for Components.
 * Components represent interchangeable sections of output content,
 * and handle the organization of all scripts, styles, templating,
 * and component relationships between nested or related other components.
 * 
 * This allows dependencies required for components to operate,
 * and additional components required to interact with them to be
 * automatically registered when a component is registered for output.
 * @author Brian Dayhoff
 */
interface ComponentInterface extends \Oroboros\core\interfaces\library\LibraryInterface
{

    /**
     * Dependency Injection method for the template engine.
     * @param \Oroboros\core\interfaces\library\template\TemplateInterface $template_engine
     * return void
     */
    public static function setTemplateEngine(\Oroboros\core\interfaces\library\template\TemplateInterface $template_engine): void;

    /**
     * Sets the key name for the component.
     * @param string $key
     * @return $this (method chainable)
     */
    public function setKey(string $key): \Oroboros\core\interfaces\library\component\ComponentInterface;

    /**
     * Returns the key name of the component instance
     * @return string
     */
    public function getKey(): string;

    /**
     * String casting method
     * @return string
     * @note PHP will crash with a fatal error if an exception occurs inside
     *     a `__toString` method. For this reason, a safe empty string will
     *     be returned by default, and the exception message and stacktrace
     *     will be returned if debug mode is enabled, so the error can be
     *     detected on the page content in a development environment safely.
     * @todo Exceptions here need to log when Psr-3 is implemented, as they
     *     cannot otherwise be safely detected.
     */
    public function __toString(): string;

    /**
     * Renders the component output, and returns it as a string.
     * @return string
     */
    public function render(): string;

    /**
     * Adds a child component
     * @param \Oroboros\core\interfaces\library\component\ComponentInterface $child
     * @return $this (method chainable)
     */
    public function addChild(\Oroboros\core\interfaces\library\component\ComponentInterface $child): \Oroboros\core\interfaces\library\component\ComponentInterface;

    /**
     * Returns whether a child exists or not
     * @param string $key
     * @return bool
     */
    public function hasChild(string $key): bool;

    /**
     * Removes a child component if it exists
     * @param string $key
     * @return $this (method chainable)
     */
    public function removeChild(string $key): \Oroboros\core\interfaces\library\component\ComponentInterface;

    /**
     * Returns the child associated with the given key, or null if it does not exist.
     * @param string $key
     * @return \Oroboros\core\interfaces\library\component\ComponentInterface|null
     */
    public function getChild(string $key): ?\Oroboros\core\interfaces\library\component\ComponentInterface;

    /**
     * Sets the component parent instance.
     * The component will set itself as a child of the parent when this occurs.
     * @param \Oroboros\core\interfaces\library\component\ComponentInterface $parent
     * @return \Oroboros\core\interfaces\library\component\ComponentInterface
     */
    public function setParent(\Oroboros\core\interfaces\library\component\ComponentInterface $parent): \Oroboros\core\interfaces\library\component\ComponentInterface;

    /**
     * Removes the parent/child association if any exists.
     * The current instance will also be removed as a child
     * of the parent when this occurs.
     * @return $this (method chainable)
     */
    public function removeParent(): \Oroboros\core\interfaces\library\component\ComponentInterface;

    /**
     * Returns the parent component instance, or null if no parent exists.
     * @return \Oroboros\core\interfaces\library\component\ComponentInterface|null
     */
    public function getParent(): ?\Oroboros\core\interfaces\library\component\ComponentInterface;

    /**
     * Returns a boolean determination as to whether or not the component has a parent.
     * @return bool
     */
    public function hasParent(): bool;

    /**
     * Returns an array of keys that are considered valid context for this component
     * @return array
     */
    public function getContextValidKeys(): array;

    /**
     * Returns a boolean designation as to whether a given context key is valid
     * @return array
     */
    public function isValidContext(string $key): bool;

    /**
     * Adds context, which translates into variables
     * made available to the template.
     * @param string $key
     * @param type $value
     * @return $this (method chainable)
     */
    public function addContext(string $key, $value): \Oroboros\core\interfaces\library\component\ComponentInterface;

    /**
     * Returns whether a given context key exists
     * @param string $key
     * @return bool
     */
    public function hasContext(string $key): bool;

    /**
     * Removes a given context key, if it exists
     * @param string $key
     * @return $this (method chainable)
     */
    public function removeContext(string $key): \Oroboros\core\interfaces\library\component\ComponentInterface;

    /**
     * Adds a data section to the component
     * @param string $key
     * @param array $value
     * @return $this (method chainable)
     */
    public function addData(string $key, array $value): \Oroboros\core\interfaces\library\component\ComponentInterface;

    /**
     * Removes a given data key if it exists
     * @param string $key
     * @return $this (method chainable)
     */
    public function removeData(string $key): \Oroboros\core\interfaces\library\component\ComponentInterface;

    /**
     * Returns whether a given data key exists
     * @param string $key
     * @return bool
     */
    public function hasData(string $key): bool;

    /**
     * 
     * @param string $key
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     * @throws \InvalidArgumentException If the provided data key does not exist.
     */
    public function getData(string $key): \Oroboros\core\interfaces\library\container\ContainerInterface;

    /**
     * Sets the builder object associated with collecting data from the component
     * @param \Oroboros\core\interfaces\pattern\builder\BuilderInterface $builder
     * @return $this (method chainable)
     */
    public function setBuilder(\Oroboros\core\interfaces\pattern\builder\BuilderInterface $builder): \Oroboros\core\interfaces\library\component\ComponentInterface;

    /**
     * Returns the associated builder object
     * @return \Oroboros\core\interfaces\pattern\builder\BuilderInterface
     */
    public function getBuilder(): \Oroboros\core\interfaces\pattern\builder\BuilderInterface;
}
