<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\interfaces\library\file;

/**
 * File Writer Interface
 * Defines methods expected to be honored by file writer classes
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
interface FileWriterInterface extends \Oroboros\core\interfaces\library\LibraryInterface
{

    /**
     * The file permission that should be used
     * if not declared when the object is instantiated
     */
    const DEFAULT_FILE_PERMISSION = 0664;

    /**
     * The directory permission that should be used for created directories
     * if not declared when the object is instantiated
     */
    const DEFAULT_DIRECTORY_PERMISSION = 0775;

    /**
     * Sets the name of the currently scoped file
     * @param string $filename
     * @return void
     */
    public function setFile(string $filename): void;

    /**
     * Returns the currently scoped filename
     * @return string
     * @throws \ErrorException if no filename has been supplied
     */
    public function getFile(): string;

    /**
     * Returns the currently scoped directory
     * @return string
     * @throws \ErrorException if no filename has been supplied
     */
    public function getDirectory(): string;

    /**
     * returns the full path to the currently scoped file.
     * @return string
     * @throws \ErrorException if no filename or no directory have been supplied
     */
    public function getFullPath(): string;

    /**
     * Sets the path of the currently scoped directory
     * @param string $directory
     * @return void
     */
    public function setDirectory(string $directory): void;

    /**
     * Returns a boolean designation as to whether
     * the object can be used to write in its current state.
     * 
     * If this returns true, a write operation should occur without error or exceptions.
     * 
     * If this returns false, a write operation attempt will throw an ErrorException.
     * 
     * @return bool Returns true if a write operation can occur in the current state, false otherwise
     */
    public function isWriteable(): bool;

    /**
     * Returns the current value of a requested option key
     * @param string $key The name of the requested key
     * @return mixed
     * @throws \InvalidArgumentException if an invalid option key is supplied
     */
    public function getOption(string $key);

    /**
     * Sets a file write option.
     * @param string $key
     * @param type $value
     * @return void
     * @throws \InvalildArgumentException If the keyname is not known, or the supplied value is not valid for the given key
     */
    public function setOption(string $key, $value): void;

    /**
     * Writes the given content to the file.
     * @param string $content The content to write to the file.
     * @return void
     * @throws \ErrorException If there is no currently writeable file and directory set for the current options.
     */
    public function write(string $content): void;

    /**
     * Copies a given stream resource to the file.
     * @param resource $stream A stream resource to copy to the file.
     * @return void
     * @throws \ErrorException If there is no currently writeable file and directory set for the current options.
     * @throws \InvalidArgumentException If `$stream` is not a resource.
     */
    public function stream($stream): void;
}
