<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\interfaces\library\auth;

/**
 * Nonce Interface
 * Defines the methods required to qualify as an oroboros nonce
 * 
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
interface NonceInterface extends AuthInterface, \Serializable
{

    /**
     * If the given `$command` parameter is a string, the object will be
     * instantiated as a representation of the given string as the nonce.
     * 
     * If the given string has already been expired, this will raise an
     * `\Oroboros\core\exception\OutOfBoundsException`
     * 
     * @param string $command
     * @param array $arguments
     * @param array $flags
     * @throws \Oroboros\core\exception\OutOfBoundsException If a nonce string
     *         is passed as `$command` that has already been expired.
     * @throws \Oroboros\core\exception\core\InvalidClassException
     *         If the class constant `NONCE_HASH_ALGORITHM` does not define
     *         a valid hash method. These may be checked for validity with `hash_algos()`
     */
    public function __construct(string $command = null, array $arguments = null, array $flags = null);

    /**
     * Returns the string representation of the nonce
     * 
     * @return string
     */
    public function __toString(): string;

    /**
     * Serializes the nonce in tiny format for later direct retrieval
     * The serialized data can be stored in a user session as a tiny
     * representation of the original nonce object.
     * 
     * @return string|null
     */
    public function serialize(): ?string;

    /**
     * Restores a serialized nonce directly. This can be used to retrieve a
     * previously generated form nonce for validation purposes.
     * 
     * @param string $data
     * @return void
     */
    public function unserialize(string $data): void;

    /**
     * Generates a valid nonce string.
     * This should not return a nonce that has
     * already been used.
     * 
     * @return string
     */
    public function generate(): string;

    /**
     * Expires the nonce represented by the object.
     * The nonce represented by the object should not validate again when the
     * same class is called with the same string to initialize it.
     * 
     * @return void
     */
    public function expire(): void;

    /**
     * Returns a boolean designation as to whether the given nonce
     * string matches the nonce internally stored in the object.
     * 
     * Calling this method will also expire the nonce, however
     * this object will still `match` as `true` as long as it
     * exists for further verification. The represented nonce
     * cannot be generated again though.
     * 
     * @param string $nonce
     * @return bool
     */
    public function match(string $nonce): bool;

    /**
     * Validates that a given nonce has not been used already.
     * 
     * This will return false if a given nonce string already exists.
     * 
     * @param string $nonce
     * @return bool
     */
    public function validate(string $nonce): bool;
}
