<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Identity is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\interfaces\library\auth;

/**
 * Identity Manager Strategy Interface
 * Defines the methods required for a valid identity manager strategy
 * 
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
interface IdentityManagerStrategyInterface extends AuthInterface, \Oroboros\core\interfaces\pattern\strategy\StrategyInterface
{

    /**
     * Registration method for auth managers.
     * Only the string class name or stub class name is needed.
     * 
     * If the passed string does not resolve to a class implementing
     * `\Oroboros\core\interfaces\library\auth\AuthManagerInterface`, then
     * an `\Oroboros\core\exception\InvalidArgumentException` will be raised.
     * 
     * @param string $manager
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    public static function registerManager(string $manager): void;

    /**
     * Returns a boolean designation as to whether a given type
     * identifier can resolve to a given auth manager.
     * 
     * If this method returns `true`, the method `getManager`
     * must not raise an exception for the same key.
     * 
     * If this method returns `false`, the method `getManager` MUST
     * raise an exception for the same key.
     * 
     * @param string $type
     * @return bool
     */
    public function hasManager(string $type): bool;

    /**
     * Returns the manager used to satisfy the specific type of permission resolution.
     * 
     * If the method `hasManager` returns `true` for the same key, this method MUST
     * return an instance of `\Oroboros\core\interfaces\library\auth\IdentityManagerInterface`
     * 
     * If the method `hasManager` returns `false`, for the same key, this method
     * MUST throw an `\Oroboros\core\exception\InvalidArgumentException`
     * 
     * @param string $type
     * @return \Oroboros\core\interfaces\library\auth\IdentityManagerInterface
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    public function getManager(string $type): \Oroboros\core\interfaces\library\auth\IdentityManagerInterface;
}
