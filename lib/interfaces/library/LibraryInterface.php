<?php
namespace Oroboros\core\interfaces\library;

/**
 * Provides a contract for standardized operation of libraries.
 * 
 * @author Brian Dayhoff
 */
interface LibraryInterface
extends \Oroboros\core\interfaces\BaseInterface
, \Oroboros\core\interfaces\environment\EnvironmentInterface
, \Oroboros\core\interfaces\pattern\command\CommandSubjectInterface
{
    
}
