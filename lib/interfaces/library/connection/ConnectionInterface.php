<?php
namespace Oroboros\core\interfaces\library\connection;

/**
 * Establishes a standard approach to obtaining a database PDO connection object.
 * @author Brian Dayhoff
 */
interface ConnectionInterface
extends \Oroboros\core\interfaces\library\LibraryInterface
{

    /**
     * Establishes a connection to the connected resource, if possible.
     * @return $this (method chainable)
     * @throws \ErrorException If a connection could not be established.
     *  The message will disclose the reason.
     *  The code will correspond to the original code returned from the resource.
     */
    public function connect();

    /**
     * Terminates the connection, if applicable.
     * @return $this (method chainable)
     */
    public function disconnect();
}
