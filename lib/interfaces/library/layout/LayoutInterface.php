<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\interfaces\library\layout;

/**
 * Layout Interface
 * Defines methods used for interaction with a page layout object.
 * 
 * @author Brian Dayhoff
 */
interface LayoutInterface extends \Oroboros\core\interfaces\library\LibraryInterface, \Oroboros\core\interfaces\library\theme\ThemableInterface, \Oroboros\core\interfaces\interoperability\HtmlAssetAwareInterface
{

    /**
     * Dependency injection method for the template engine. The template engine must
     * implement the template interface, and must be an instance of the
     * template engine defined by the class constant TEMPLATE_ENGINE
     * @param \Oroboros\core\interfaces\library\template\TemplateInterface $engine
     * @return void
     * @throws \InvalidArgumentException If the provided template engine does not
     *     match the defined acceptable template engine for the layout class,
     *     which is defined by the class constant TEMPLATE_ENGINE
     */
    public function setTemplateEngine(\Oroboros\core\interfaces\library\template\TemplateInterface $engine): void;

    /**
     * Adds content to a given section.
     * @param string $section The name of the section to add content to.
     * @param string key The identifying key of the content.
     * @param string|array|Oroboros\core\interfaces\library\component\ComponentInterface $content The content payload.
     *     This may be a string, array, or component instance.
     * @return $this (method chainable)
     * @throws \InvalidArgumentException If the provided content section does not exist in this layout.
     */
    public function setContent(string $section, string $key, $content): \Oroboros\core\interfaces\library\layout\LayoutInterface;

    /**
     * Removes content if it exists.
     * @param string $section The name of the section to add content to.
     * @param string key The identifying key of the content.
     * @return $this (method chainable)
     * @throws \InvalidArgumentException If the provided content section does not exist in this layout.
     */
    public function removeContent(string $section, string $key): \Oroboros\core\interfaces\library\layout\LayoutInterface;

    /**
     * Renders the layout output into a string of dom elements
     * that can be inserted into a page template.
     * @return string
     */
    public function render(): string;

    /**
     * Returns the existing content designated by section (and optional key), if it exists. Returns null if it does not exist.
     * @param string $section The name of the section to add content to.
     * @param string key (optional) The identifying key of the content. If not provided, the entire section will be returned.
     * @return mixed
     * @throws \InvalidArgumentException If the provided content section does not exist in this layout.
     */
    public function getContent(string $section, string $key = null);
}
