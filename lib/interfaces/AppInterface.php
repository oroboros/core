<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\interfaces;

/**
 * Designates that a class is part of an application grouping.
 * This represents a commonly shared interface for all oroboros-based
 * classes in an application scope. It has not specific requirements
 * aside from those shared by the base interface, which all
 * oroboros classes and child classes thereof share.
 * 
 * All app-specific interfaces should extend this interface, and implement
 * the extended interface on all classes in their application.
 * 
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
interface AppInterface extends BaseInterface
{
    /**
     * Declare the following class constant as the name of your application.
     * This should correspond to the same value in your app-default.json file.
     */
    //const APP_NAME = 'your-app-name';
}
