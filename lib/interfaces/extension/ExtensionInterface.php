<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\interfaces\extension;

/**
 * Extension Interface
 * Defines a set of methods common to all extensions
 * 
 * Services represent functionality that automatically hooks into
 * events, output, and object behavior to provide additional options,
 * formatting, or supporting logic.
 * 
 * Well formed extensions provide additional sets of available resources
 * without any opinion as to how or when they are used.
 * 
 * A good example of an extension would be a set of libraries that output
 * data into several formats not provided by the underlying platform out
 * of the box.
 * 
 * Extensions should not make execution decisions
 * or have opinion about what should occur outside
 * of their scope.
 * 
 * Extensions may also provide additional services that support the purpose
 * of the extension, for any provided logic that makes more sense as a service
 * than an extension.
 *
 * @author Brian Dayhoff
 */
interface ExtensionInterface extends \Oroboros\core\interfaces\pluggable\PluggableInterface, \Oroboros\core\interfaces\pattern\command\CommandSubjectInterface, \Oroboros\core\interfaces\interoperability\TemplateEngineSupportInterface, \Oroboros\core\interfaces\library\event\EventWatcherInterface, \Oroboros\core\interfaces\library\filter\FilterWatcherInterface
{

    /**
     * Any additional libraries provided by the extension
     * @return array
     */
    public function libraries(): array;

    /**
     * Any additional views provided by the extension
     * @return array
     */
    public function views(): array;

    /**
     * Any additional services provided by the extension
     * @return array
     */
    public function services(): array;

    /**
     * Any additional job actions provided by the extension
     * @return array
     */
    public function actions(): array;

    /**
     * Any additional script resources provided by the extension
     * @return array
     */
    public function scripts(): array;

    /**
     * Any additional stylesheet resources provided by the extension
     * @return array
     */
    public function styles(): array;

    /**
     * Any additional font resources provided by the extension
     * @return array
     */
    public function fonts(): array;

    /**
     * Any additional media assets provided by the extension
     * @return array
     */
    public function media(): array;

    /**
     * Any additional components provided by the extension
     * @return array
     */
    public function components(): array;

    /**
     * Any additional templates provided by the extension
     * @return array
     */
    public function templates(): array;
}
