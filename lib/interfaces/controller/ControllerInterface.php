<?php
namespace Oroboros\core\interfaces\controller;

/**
 * Provides a contract for standardized operation of controllers.
 * 
 * @author Brian Dayhoff
 */
interface ControllerInterface extends \Oroboros\core\interfaces\BaseInterface, \Oroboros\core\interfaces\environment\EnvironmentInterface, \Oroboros\core\interfaces\pattern\observer\ObservableInterface, \Oroboros\core\interfaces\pattern\command\CommandSubjectInterface, \Oroboros\core\interfaces\library\event\EventAwareInterface, \Oroboros\core\interfaces\library\filter\FilterAwareInterface
{
    
    /**
     * Controllers use the standard Oroboros constructor format so they may
     * be loaded correctly by the controller factory.
     * 
     * @param string $command
     * @param array $arguments
     * @param array $flags
     */
    public function __construct(string $command = null, array $arguments = null, array $flags = null);

    /**
     * This method is called within a controller to signal an irrecoverable error.
     * 
     * Non-error controllers should return their specified error controller.
     * Error controllers should break operation by throwing an exception
     * implementing `\Oroboros\core\interfaces\exception\ErrorExceptionInterface`,
     * which will signal to the bootstrap class not to execute any additional
     * runtime operations, and run cleanup.
     * 
     * web error controllers may render a default response as needed for clean
     * error pages or rest repsonses rather than throwing an exception.
     * 
     * @param int $code
     * @param string $message
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $arguments
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $flags
     * @return \Oroboros\core\interfaces\controller\ErrorControllerInterface
     */
    public function error(int $code = null, string $message = null, \Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\ErrorControllerInterface;

    /**
     * This method will receive the user object and the currently selected route
     * to determine if the given user is properly authenticated to call the
     * selected controller method.
     * 
     * This will always be asked by the bootstrap object prior to calling
     * a controller method.
     * 
     * Controllers subrouting should also execute this method prior to calling
     * any method on another controller.
     * 
     * You may override this method to add any relevant authentication constraints
     * to the method being referenced appropriate to the controller scope.
     * 
     * Additional authentication may be required for partial content, which may
     * be performed within the body of the method referenced. However, if this
     * method returns `false`, calling the referenced method with the same user
     * should *always* generate a not authorized or forbidden error, and if it
     * returns `true`, then calling the same method *should not* generate a
     * not authorized or forbidden error.
     * 
     * This method **must not** prevent execution of the method named `error`.
     * The error method will be called if this method returns `false`.
     * 
     * @param string $method The name of the method to call on the controller
     * @param \Oroboros\core\interfaces\library\user\UserInterface $user
     *        The object representing the currently scoped user.
     * @return bool
     */
    public function authenticate(string $method, \Oroboros\core\interfaces\library\user\UserInterface $user): bool;
    
    /**
     * Registers a given view type as valid for the controller scope being referenced.
     * 
     * This method is used to allow modules and extensions to register new views
     * to handle more granular purposes than the underlying system ships with.
     * 
     * @param string $key A canonicalized lower case string representing
     *        the identifier for the view type.
     * @param string $class The name of a concrete class implementing
     *        `\Oroboros\core\interfaces\view\ViewInterface`
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException if the key is
     *         malformed, or the given class name does not
     *         resolve to a concrete class implementing
     *         `\Oroboros\core\interfaces\view\ViewInterface`
     */
    public static function addViewType(string $key, string $class): void;

    /**
     * Makes an added class scope known.
     * If this method *has not* been called on the underlying abstract to
     * declare a scope valid, then it must raise a
     * `\Oroboros\core\exception\OutOfBoundsException` in it's constructor if a
     * child class with the unknown scope attempts to call `parent::__construct`
     * 
     * Child classes are responsible for registering their own scopes prior
     * to calling `parent::__construct`
     * 
     * Class scopes are used extensively to relationally group objects that
     * work within a similar scope but are otherwise unrelated.
     * 
     * @param string $scope
     * @return void
     */
    public static function addClassScope(string $scope): void;

    /**
     * Performs the render step after any controller route
     * method has been executed.
     * 
     * This method is internally responsible for dispatch, handling views,
     * and sending the response body to whatever client it is interacting with.
     * 
     * @param string $format
     * @param string $template
     * @param array $flags
     * @return \Oroboros\core\interfaces\controller\ControllerInterface
     * @throws \Oroboros\core\exception\OutOfBoundsException if the `$format`
     *         does not correspond to a known view type.
     */
    public function render(string $format, string $template, array $flags = null): \Oroboros\core\interfaces\controller\ControllerInterface;
        
    /**
     * Provides the response data to the bootstrap object,
     * so it can be sent to the client.
     * @return mixed
     */
    public function getResponse();

    /**
     * Returns the error controller used by the controller.
     * This must be public so the bootstrap class can obtain the correct
     * error controller if there is an issue loading the standard controller.
     * 
     * @return \Oroboros\core\interfaces\controller\ErrorControllerInterface
     */
    public function getErrorController(): \Oroboros\core\interfaces\controller\ErrorControllerInterface;
}
