<?php
namespace Oroboros\core\interfaces\controller\socket;

/**
 * Provides a contract for standardized operation of socket error controllers.
 * 
 * @author Brian Dayhoff
 */
interface SocketErrorControllerInterface
extends SocketControllerInterface, \Oroboros\core\interfaces\controller\HttpErrorControllerInterface
{
    
}
