<?php
namespace Oroboros\core\interfaces\controller\socket;

/**
 * Provides a contract for standardized operation of socket controllers.
 * 
 * @author Brian Dayhoff
 */
interface SocketControllerInterface
extends \Oroboros\core\interfaces\controller\HttpControllerInterface
{
    
}
