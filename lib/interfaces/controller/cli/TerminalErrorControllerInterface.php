<?php
namespace Oroboros\core\interfaces\controller\cli;

/**
 * Provides a contract for standardized operation of terminal error controllers.
 * 
 * @author Brian Dayhoff
 */
interface TerminalErrorControllerInterface
extends TerminalControllerInterface, \Oroboros\core\interfaces\controller\CliErrorControllerInterface
{
    
}
