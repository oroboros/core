<?php
namespace Oroboros\core\interfaces\controller\cli;

/**
 * Provides a contract for standardized operation of job error controllers.
 * 
 * @author Brian Dayhoff
 */
interface JobErrorControllerInterface
extends TerminalControllerInterface, \Oroboros\core\interfaces\controller\CliErrorControllerInterface
{
    
}
