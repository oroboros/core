<?php
namespace Oroboros\core\interfaces\controller\cli;

/**
 * Provides a contract for standardized operation of socket server controllers.
 * 
 * @author Brian Dayhoff
 */
interface SocketServerControllerInterface
extends \Oroboros\core\interfaces\controller\CliControllerInterface
, \Oroboros\core\interfaces\pattern\observer\ObserverInterface
{
    
}
