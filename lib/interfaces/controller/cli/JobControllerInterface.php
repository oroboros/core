<?php
namespace Oroboros\core\interfaces\controller\cli;

/**
 * Provides a contract for standardized operation of job controllers.
 * 
 * @author Brian Dayhoff
 */
interface JobControllerInterface
extends \Oroboros\core\interfaces\controller\CliControllerInterface
, \Oroboros\core\interfaces\pattern\observer\ObserverInterface
{
    
}
