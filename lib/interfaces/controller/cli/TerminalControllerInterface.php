<?php
namespace Oroboros\core\interfaces\controller\cli;

/**
 * Provides a contract for standardized operation of terminal controllers.
 * 
 * @author Brian Dayhoff
 */
interface TerminalControllerInterface
extends \Oroboros\core\interfaces\controller\CliControllerInterface
, \Oroboros\core\interfaces\pattern\observer\ObserverInterface
{
    
}
