<?php
namespace Oroboros\core\interfaces\controller\cli;

/**
 * Provides a contract for standardized operation of job error controllers.
 * 
 * @author Brian Dayhoff
 */
interface SocketServerErrorControllerInterface
extends SocketServerControllerInterface, \Oroboros\core\interfaces\controller\CliErrorControllerInterface
{
    
}
