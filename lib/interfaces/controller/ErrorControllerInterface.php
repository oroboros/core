<?php
namespace Oroboros\core\interfaces\controller;

/**
 * Provides a contract for standardized operation of controllers.
 * 
 * @author Brian Dayhoff
 */
interface ErrorControllerInterface
extends ControllerInterface
{

    /**
     * This method is called on the error controller when an error occurs.
     * 
     * @param int $code
     * @param string $message
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $arguments
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $flags
     * @return \Oroboros\core\interfaces\controller\ErrorControllerInterface
     */
    public function error(int $code = null, string $message = null, \Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\ErrorControllerInterface;
}
