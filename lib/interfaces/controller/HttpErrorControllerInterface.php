<?php
namespace Oroboros\core\interfaces\controller;

/**
 * Provides a contract for standardized operation of controllers.
 * 
 * @author Brian Dayhoff
 */
interface HttpErrorControllerInterface
extends HttpControllerInterface, ErrorControllerInterface
{
    
}
