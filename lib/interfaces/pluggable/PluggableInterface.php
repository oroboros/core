<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\interfaces\pluggable;

/**
 * Pluggable interface
 * Defines baseline methods for pluggable additions to the system
 * This is extended upon by constructs such as modules, extensions, and services
 * 
 * @author Brian Dayhoff
 */
interface PluggableInterface extends \Oroboros\core\interfaces\BaseInterface, \Oroboros\core\interfaces\library\event\EventAwareInterface, \Oroboros\core\interfaces\library\filter\FilterAwareInterface
{

    /**
     * Returns the identifier slug name of the pluggable
     * @return string
     */
    public function name(): string;

    /**
     * Returns the human readable name of the pluggable
     * @return string
     */
    public function title(): string;

    /**
     * Returns the description of the pluggable
     * @return string
     */
    public function description(): string;

    /**
     * Returns the absolute directory path of the pluggable
     * @return string
     */
    public function path(): string;

    /**
     * Returns the absolute directory path of the pluggable asset code source
     * @return string
     */
    public function source(): string;

    /**
     * Returns the absolute directory path of the pluggable asset configuration
     * 
     * @return string
     */
    public function config(): string;

    /**
     * Returns the vendor/package name of the pluggable
     * @return string
     */
    public function baseNamespace(): string;

    /**
     * Returns the semver compatible version of the pluggable
     * @return string
     */
    public function version(): string;

    /**
     * Returns the manifest as an associative array
     * @return array
     */
    public function manifest(): array;
}
