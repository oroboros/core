<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\interfaces\application;

/**
 * Application Interface
 * Defines the methods required to declare an application on this platform.
 * Objects implementing this interface are provided to the bootstrap object,
 * and are expected to provide any paths, configurations, extensions, modules,
 * template directories, base namespaces, routes, models, controllers, views,
 * trigger bindings, filters, connection credentials, environment files,
 * and other criteria needed to override the defaults or add desired functionality.
 * 
 * As the base library is intended to operate strictly as middleware,
 * the application class leveraging this interface facilitates using it
 * as a standalone system, service, or application.
 * 
 * This object must be implemented in such a way that all returned values are immutable.
 * If objects are stored internally, and they are not implemented in such a way
 * as to be read only, return values must return a clone of the object rather
 * than the internally stored object, so that objects acted upon externally
 * do not propogate back to internals of this object.
 * 
 * Values and properties returned by the public methods of this object
 * after instantiation must not change over time.
 * There must be no point during runtime where any value obtained from this
 * object differs from the return value at any other point during runtime.
 * This insures the integrity and security of the application definition,
 * which is referred to extensively throughout the system with the
 * expectation that values returned are immutable and consistent.
 * 
 * @author Brian Dayhoff
 */
interface ApplicationInterface extends \Oroboros\core\interfaces\BaseInterface, \Oroboros\core\interfaces\library\event\EventAwareInterface, \Oroboros\core\interfaces\library\filter\FilterAwareInterface
{

    /**
     * Returns the configured domain, or localhost if not configured.
     * @return string
     */
    public function getApplicationDomain();

    /**
     * Returns base namespace of the application.
     * This is used for Psr-4 registration.
     * Format [vendorName\\packageName]
     * @return string
     */
    public function getBaseNamespace(): string;

    /**
     * Returns the root directory of the application.
     * Must return a fully qualified path to a valid readable directory.
     * @return string
     */
    public function getApplicationRoot(): string;

    /**
     * Returns the source directory of application templates.
     * This is used for template overrides.
     * Return null if not applicable.
     * Must return a fully qualified path to a valid readable directory if not null.
     * @return string|null
     */
    public function getApplicationTemplateDirectory(): ?string;

    /**
     * Returns the public folder of the application.
     * This is used for generating links to frontend content.
     * Return null if not applicable.
     * Must return a fully qualified path to a valid readable directory if not null.
     * @return string|null
     */
    public function getApplicationPublicDirectory(): ?string;

    /**
     * Returns the source folder for frontend scripts and stylesheet source.
     * This is used for running build tasks for the application.
     * Return null if not applicable.
     * Must return a fully qualified path to a valid readable directory if not null.
     * @return string|null
     */
    public function getApplicationEtcDirectory(): ?string;

    /**
     * Returns the unit test directory.
     * This is used for running automated unit tests.
     * Must return a fully qualified path to a valid readable directory.
     * Return null if not applicable.
     * @return string|null
     */
    public function getApplicationTestDirectory(): ?string;

    /**
     * Returns the full classname of the bootstrap class used to boot the application.
     * Must be a class implementing Oroboros\core\interfaces\bootstrap\BootstrapInterface
     * @return string
     */
    public function getBootstrapClass(): string;

    /**
     * Return an array of routes to populate the router.
     * Return an empty array if there are no routes.
     * @param array|null $routes If supplied from a child class,
     *     the routes passed back will take precedence over the base routes.
     *     If an application does not want to include default routes,
     *     return the routes directly. If including the default routes
     *     is desireable, return `parent::getRoutes($your_new_routes);`
     * @return array
     */
    public function getRoutes(array $routes = null): array;

    /**
     * Return an array of component declarations.
     * Return an empty array if there are no component declarations.
     * @param array|null $components If supplied from a child class,
     *     the components passed back will take precedence over the
     *     base components on duplicate keys, and details not supplied
     *     for components will use default values. If an application
     *     does not want to utilize the underlying components, then it
     *     should just return its own set. If it wants to include
     *     default components also or inherit defaults,
     *     return `parent::getComponents($your_new_components);`
     * @param array $components
     * @return array
     */
    public function getComponents(array $components = null): array;

    /**
     * Returns the filename of the application config file.
     * Must return a fully qualified path to a valid readable json file.
     * @return string
     */
    public function getApplicationConfig(): string;

    /**
     * Return a command, if any, that should be passed to the
     * bootstrap object on application instantiation.
     * Must explicitly return a string or null.
     * May not return void (must explicitly return null or an array).
     * @return string|null
     */
    public function getBootstrapCommand(): ?string;

    /**
     * Return commands, if any, that should be passed to the
     * bootstrap object on application instantiation.
     * Must explicitly return an array or null.
     * May not return void (must explicitly return null or an array).
     * @return array|null
     */
    public function getBootstrapArguments(): ?array;

    /**
     * Return flags, if any, that should be passed to the
     * bootstrap object on application instantiation.
     * Must explicitly return an array or null.
     * May not return void (must explicitly return null or an array).
     * @return array|null
     */
    public function getBootstrapFlags(): ?array;

    /**
     * Returns a boolean designation as to whether debug mode is enabled or not.
     * @return bool
     */
    public function debugEnabled(): bool;
}
