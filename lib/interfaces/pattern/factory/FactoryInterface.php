<?php
namespace Oroboros\core\interfaces\pattern\factory;

/**
 * Provides a standard for object factorization
 * @author Brian Dayhoff
 */
interface FactoryInterface extends \Oroboros\core\interfaces\BaseInterface, \Oroboros\core\interfaces\environment\EnvironmentInterface, \Oroboros\core\interfaces\pattern\PatternInterface
{
    
}
