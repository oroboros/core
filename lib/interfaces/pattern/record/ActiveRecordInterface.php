<?php
namespace Oroboros\core\interfaces\pattern\record;

/**
 * Defines the standard for Active Record pattern application
 * @author Brian Dayhoff
 */
interface ActiveRecordInterface
extends \Oroboros\core\interfaces\BaseInterface, \Oroboros\core\interfaces\environment\EnvironmentInterface, \Oroboros\core\interfaces\pattern\PatternInterface
{

    /**
     * Performs a select statement, and returns an array of ActiveRecordRow objects associated with the result set.
     * @param array $where (optional) If defined, an array of where clauses
     * @param int $limit (optional) If defined, the return limit for selection
     * @param int $paginate (optional) If defined, the pagination limit for selection
     * @return array
     */
    public function get(array $where = null, int $limit = null, int $paginate = null);

    /**
     * Performs an update statement
     * @param array $where (optional) If defined, an array of where clauses
     * @param int $limit (optional) If defined, the maximum row count to update
     * @return $this (method chainable)
     */
    public function set(array $where = null, int $limit = null);

    /**
     * Performs an insert statement
     * @param array $values The values to insert. Should be an array of arrays,
     *  where each nested array is an associative set where the key is the column name,
     *  and the value is the insert value.
     * @return $this (method chainable)
     */
    public function add(array $values);

    /**
     * Performs a delete statement
     * @param array $where (optional) If defined, an array of where clauses
     * @param int $limit (optional) If defined, the max count of rows to delete
     * @return $this (method chainable)
     */
    public function delete(array $where = null, int $limit = null);
}
