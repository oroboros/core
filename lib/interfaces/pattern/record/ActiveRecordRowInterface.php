<?php
namespace Oroboros\core\interfaces\pattern\record;

/**
 * Defines the standard for Active Record row objects
 * @author Brian Dayhoff
 */
interface ActiveRecordRowInterface
extends \Oroboros\core\interfaces\BaseInterface, \Oroboros\core\interfaces\environment\EnvironmentInterface, \Oroboros\core\interfaces\pattern\PatternInterface
{
    
}
