<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\interfaces\pattern\builder;

/**
 * Builder Pattern Interface
 * Defines the methods for interacting with a builder object
 * 
 * @author Brian Dayhoff
 */
interface BuilderInterface extends \Oroboros\core\interfaces\pattern\director\WorkerInterface, \Oroboros\core\interfaces\pattern\PatternInterface
{

    /**
     * Sets a value in the builder for the given category and key
     * @param string $category
     * @param string $key
     * @param type $value
     * @return $this (method chainable)
     * @throws \InvalidArgumentException If the given category does not exist,
     *     the given key is not valid for the given category,
     *     or the provided value is not a valid type for the given category and key.
     */
    public function set(string $category, string $key, $value): \Oroboros\core\interfaces\pattern\builder\BuilderInterface;

    /**
     * Removes a value from the specified category key
     * @param string $category
     * @param string $key
     * @return $this (method chainable)
     * @throws \InvalidArgumentException If the supplied category does not exist
     */
    public function remove(string $category, string $key): \Oroboros\core\interfaces\pattern\builder\BuilderInterface;

    /**
     * Returns either the entire specified category,
     * or the given key of the specified category if
     * an optional key name is supplied.
     * 
     * @param string $category
     * @param string $key
     * @return mixed
     * @throws \InvalidArgumentException if the given category does not exist,
     *     or if a key is supplied and it does not exist in the given category.
     */
    public function get(string $category, string $key = null);

    /**
     * Returns whether or not a given category exists,
     * or if a key exists in a given category if the optional keyname is also supplied.
     * This method must not throw an exception.
     * 
     * @param string $category
     * @param string $key (optional)
     * @return bool
     */
    public function has(string $category, string $key = null): bool;

    /**
     * Override this method in a child class to define the build process.
     * This should return the fully built expected result.
     * @param array $flags Any flags to send to the builder to modify the output method
     * @return mixed
     */
    public function build(array $flags = []);

    /**
     * Resets the builder to its default state.
     * This can be used to recycle a builder for multiple related purposes
     * with the same output types but different values.
     * @return $this (method chainable)
     */
    public function reset(): \Oroboros\core\interfaces\pattern\builder\BuilderInterface;

    /**
     * Returns the categories, with their validation rules
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    public function getCategories(): \Oroboros\core\interfaces\library\container\ContainerInterface;

    /**
     * Returns whether or not a given value is valid for the given category and key.
     * Will always return false if the category does not exist,
     * or if the key is not valid for the given category.
     * 
     * Will return true if the given value matches the validation rules for the given category and key
     * 
     * @param string $category
     * @param string $key
     * @param type $value
     * @return bool
     */
    public function isValid(string $category, string $key, $value): bool;
}
