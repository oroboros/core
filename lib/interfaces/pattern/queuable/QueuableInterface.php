<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\interfaces\pattern\queuable;

/**
 * Queuable Interface
 * Designates an object as queuable.
 * Queuable objects are capable of running queues that take a single set of arguments
 * and pass then to multiple options that may or may not exist.
 * 
 * This is useful for running queues of actions, jobs, events, filters, 
 * decorators, or any other instance that needs a queued set of callbacks that
 * fire in ordered priority.
 * @note There is a trait that fully satisfies this interface
 * @see \Oroboros\core\traits\pattern\QueuableTrait
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
interface QueuableInterface extends \Oroboros\core\interfaces\pattern\PatternInterface
{
    
    /**
     * Lists the queues that exist for the current event queue type
     * @return array
     */
    public function listQueues(): array;
    
    /**
     * Returns a EventQueue object corresponding to the specified queue identifier
     * @param string $queue
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     * @throws \InvalidArgumentException If the given queue is not valid
     */
    public function getQueue(string $queue): \Oroboros\core\interfaces\library\container\ContainerInterface;
    
    /**
     * Returns the id of the current running queue item, if any
     * @return int|null
     */
    public function getId(): ?int;

    /**
     * returns the data set to run on the queue
     * @return string|null
     */
    public function getData(): ?array;

    /**
     * Gets the name of the queue currently running, if any
     * @return string|null
     */
    public function getQueueItem(): ?string;

    /**
     * Gets the error generated from the current queue pointer, if any
     * @return \Exception|null
     */
    public function getError(): ?\Exception;
    
    /**
     * Verifies that the details match the specification for a valid queue of this type.
     * These will be the arguments passed in when a new queue is generated.
     * 
     * @param array $details
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException If the provided details are not acceptable
     */
    public function verifyQueueDetails(array $details): void;
}
