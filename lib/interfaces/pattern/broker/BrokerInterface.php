<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\interfaces\pattern\broker;

/**
 * Broker Pattern Interface
 * Designates the methods required to function as a broker object
 * 
 * @see \Oroboros\core\traits\pattern\BrokerTrait
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
interface BrokerInterface extends \Oroboros\core\interfaces\pattern\PatternInterface
{

    /**
     * Takes a subject and analyzes whether it can provide the resources required of it
     * 
     * Whatever requirements the subject is obligated to satisfy by the broker
     * to interface with it must be analyzed in this method.
     * 
     * If ALL requirements are satisfiable, this method MUST return `true`.
     * 
     * If ALL requirements are not satisfiable, this method MUST return `false`.
     * 
     * If this method returns `true`, then method `analyze` MUST return the
     * relevant data set for the given subject when given the same subject.
     * 
     * all other cases return `false`
     * 
     * An invalid class exception MAY be raised in the event that the class
     * implementing this interface has an internal logic error that needs
     * developer attention. It should otherwise be non-blocking.
     * 
     * @param object $subject
     * @return bool
     * @throws \Oroboros\core\exception\core\InvalidClassException
     */
    public function analyze(object $subject): bool;

    /**
     * If method `analyze` returns `true`, this method MUST return
     * the relevant dataset for the subject.
     * 
     * If method `analyze` returns `false`, this method MUST return
     * a non-blocking empty container.
     * 
     * If a class defines a class container, that container will be used.
     * Otherwise the default generic container will be used.
     * 
     * An invalid class exception MAY be raised in the event that the class
     * implementing this interface has an internal logic error that needs
     * developer attention. It should otherwise be non-blocking.
     * 
     * @param object $subject
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     * @throws \Oroboros\core\exception\core\InvalidClassException
     */
    public function resolve(object $subject): \Oroboros\core\interfaces\library\container\ContainerInterface;
}
