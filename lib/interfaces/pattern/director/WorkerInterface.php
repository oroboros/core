<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\interfaces\pattern\director;

/**
 * Worker Pattern Interface
 * Defines the methods required for interacting with a worker pattern
 * 
 * @author Brian Dayhoff
 */
interface WorkerInterface extends \Oroboros\core\interfaces\BaseInterface, \Oroboros\core\interfaces\environment\EnvironmentInterface, \Oroboros\core\interfaces\pattern\PatternInterface
{

    /**
     * This method is used by the director to dependency inject itself as a dependency of the worker.
     * Workers are expected to forward tasks outside of their scope back to the director
     * to be handled by another worker.
     * @param \Oroboros\core\interfaces\pattern\director\DirectorInterface $director
     * @return void
     */
    public function setDirector(\Oroboros\core\interfaces\pattern\director\DirectorInterface $director): void;

    /**
     * Workers must override this method to define the task they are designated to perform.
     * Calling this method should fully resolve the task if the given arguments and are valid.
     * @param array $arguments (optional) If any arguments are required
     *     to complete the task, they should be supplied in this argument.
     *     Tasks may fail if expected argument keys are not present or not valid.
     * @param array $flags (optional) Any modifier flags that alter the output
     *     of the given task should be passed in this argument.
     *     Tasks should not fail if flags are omitted, and should
     *     use default definitions in the absence of flags.
     * @return mixed
     * @throws \InvalidArgumentException If any arguments required for execution are missing or invalid
     * @throws \ErrorException If downstream failure of the given task occurs.
     */
    public function executeTask(array $arguments = [], array $flags = []);
}
