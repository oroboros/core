<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\interfaces\pattern\director;

/**
 * Director Pattern Interface
 * Defines the methods required for interacting with a director pattern
 * @author Brian Dayhoff
 */
interface DirectorInterface extends \Oroboros\core\interfaces\BaseInterface, \Oroboros\core\interfaces\environment\EnvironmentInterface, \Oroboros\core\interfaces\pattern\PatternInterface
{

    /**
     * Adds a worker to the director pool.
     * @param \Oroboros\core\interfaces\pattern\director\WorkerInterface $worker
     * @return $this (method chainable)
     * @throws \ErrorException If the provided worker does not implement the expected interface defined by the director
     * @throws \ErrorException If the director defines a worker scope, and the provided worker is out of scope
     */
    public function addWorker(\Oroboros\core\interfaces\pattern\director\WorkerInterface $worker): \Oroboros\core\interfaces\pattern\director\DirectorInterface;

    /**
     * Returns a container of registered worker names
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    public function listWorkers(): \Oroboros\core\interfaces\library\container\ContainerInterface;

    /**
     * Returns whether or not a worker is associated with a given task
     * @param string $task The key name for the worker
     * @return bool
     */
    public function hasWorker(string $task): bool;

    /**
     * Returns the worker object associated with the specified task
     * @param string $task The key name for the worker
     * @return \Oroboros\core\interfaces\pattern\director\WorkerInterface
     * @throws \InvalidArgumentException If the given task type does not have a worker
     */
    public function getWorker(string $task): \Oroboros\core\interfaces\pattern\director\WorkerInterface;

    /**
     * Removes a specified worker from the directors worker pool if it exists.
     * @param string $key Corresponds to the value of the class constant WORKER_TASK defined for the given worker
     * @return $this (method chainable)
     */
    public function removeWorker(string $key): \Oroboros\core\interfaces\pattern\director\DirectorInterface;

    /**
     * Executes a given task against a worker object designated to handle execution.
     * 
     * @param string $task The keyword corresponding to the task to execute
     * @param array $arguments (optional) Any arguments that should be passed to the worker to resolve the task
     * @param array $flags (optyional) Any flags that should be passed to the worker to resolve the task
     * @return mixed
     * @throws \InvalidArgumentException If no worker is registered to handle the specified task
     * @throws \Oroboros\core\exception\core\InvalidClassException Invalid class exceptions within work tasks are not caught, and propagate if encountered.
     * @throws \ErrorException If a valid task was requested, but the task failed to execute with the given parameters.
     */
    public function execute(string $task, array $arguments = [], array $flags = []);
}
