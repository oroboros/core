<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\interfaces\pattern\actionable;

/**
 * Designates the methods required for a class to be considered Actionable
 * 
 * @note There is a trait that fully honors this interface available
 * @see \Oroboros\core\traits\pattern\ActionableTrait
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
interface ActionableInterface extends \Oroboros\core\interfaces\pattern\PatternInterface
{

    /**
     * Returns a boolean designation as to whether a given action key has
     * an action already bound to it.
     * 
     * @param string $key
     * @return bool
     */
    public static function hasAction(string $key): bool;

    /**
     * Returns an array of all of the existing action keys.
     * 
     * @return array
     */
    public static function listActions(): array;

    /**
     * Unregisters an existing action.
     * 
     * @param string $key
     * @throws \InvalidArgumentException If the registered action
     *     does not exist
     */
    public static function unregisterAction(string $key): void;

    /**
     * Registers a callback action.
     * Actions are agnostic of specific implementations, and can be called by any
     * valid equivalent action to handle the payload.
     * @param string $key
     * @param callable $action
     * @throws \InvalidArgumentException If the registered action
     *     is not callable, or if it is already registered.
     */
    public static function registerAction(string $key, callable $action): void;

    /**
     * Reurns a boolean designation as to whether the given subject value
     * can be safely registered as an action.
     * 
     * @param type $subject
     * @return bool
     */
    public static function isValidAction($subject): bool;

    /**
     * Replaces a callback action with another callback
     * 
     * @param string $key
     * @param callable $action
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException if there is
     *         no existing action key already
     */
    public static function replaceAction(string $key, callable $action): void;

    /**
     * Fires a registered callback for an action.
     * The action will be passed the below arguments directly in the same order.
     * @param string $key The keyname of the action callback
     * @param array $args arguments passed on action fire
     * @param array $flags flags passed on actionfire
     * @throws \Oroboros\core\exception\InvalidArgumentException if the specified
     *         action key does not exist.
     * @return mixed
     */
    public static function callAction(string $key, array $args = null, array $flags = null);
}
