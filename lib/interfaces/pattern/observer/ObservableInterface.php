<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\interfaces\pattern\observer;

/**
 * Observable Interface
 * Defines a set of methods that allow an object to automatically
 * update observers when events fire within.
 * This interface uses the underlying Spl Publisher interface dedicated to this purpose.
 * 
 * @author Brian Dayhoff
 */
interface ObservableInterface extends \Oroboros\core\interfaces\BaseInterface, \SplSubject, \Oroboros\core\interfaces\pattern\PatternInterface
{

    /**
     * Attach an SplObserver
     * @param Oroboros\core\interfaces\pattern\observer\ObserverInterface $observer
     * @return void 
     */
    public function attach(\SplObserver $observer);

    /**
     * Detach an observer
     * @param Oroboros\core\interfaces\pattern\observer\ObserverInterface $observer
     * @return void
     */
    public function detach(\SplObserver $observer);

    /**
     * Notify an observer
     * @return void 
     */
    public function notify();

    /**
     * Returns the event name for the observable event
     * @return string
     */
    public function getObserverEvent(): string;
}
