<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\interfaces\pattern\command;

/**
 * Baseline Command Pattern Interface
 * Defines the baseline methods for command patterns
 * 
 * @author Brian Dayhoff
 */
interface CommandInterface extends \Oroboros\core\interfaces\BaseInterface, \Oroboros\core\interfaces\environment\EnvironmentInterface, \Oroboros\core\interfaces\pattern\PatternInterface
{

    /**
     * Returns the command string, or an empty string if no command was passed.
     * @return string
     */
    public function __toString(): string;

    /**
     * Returns the command string passed when the object was instantiated,
     * or null if no command was passed.
     * @return string|null
     */
    public function getCommand(): ?string;

    /**
     * Runs the command operation on the subject.
     * The return value will be whatever the final outcome from the subject is.
     * @return mixed
     * @throws \BadMethodCallException If no subject is currently defined.
     * @throws \ErrorException If the subject responds with any exception during execution,
     *     It will be recast to an ErrorException,
     *     and will retain the original message and code.
     */
    public function execute();

    /**
     * Sets the subject that the command object operates upon.
     * @param \Oroboros\core\interfaces\pattern\command\CommandSubjectInterface $subject
     * @return $this (method chainable)
     */
    public function setSubject(\Oroboros\core\interfaces\pattern\command\CommandSubjectInterface $subject): \Oroboros\core\interfaces\pattern\command\CommandInterface;

    /**
     * Releases the subject the command object operates upon.
     * @return $this (method chainable)
     */
    public function releaseSubject(): \Oroboros\core\interfaces\pattern\command\CommandInterface;

    /**
     * Returns the subject that the command object operates upon,
     * or null if the subject is not currently defined.
     * @return \Oroboros\core\interfaces\pattern\command\CommandSubjectInterface|null
     */
    public function getSubject(): ?\Oroboros\core\interfaces\pattern\command\CommandSubjectInterface;

    /**
     * Returns the interface name of the expected subject.
     * Provided subjects must implement the interface declared
     * within this method in addition to the command subject interface.
     * @return string (name of a valid interface. Only interfaces, not concrete or abstract classes).
     */
    public function declareSubjectType(): string;
}
