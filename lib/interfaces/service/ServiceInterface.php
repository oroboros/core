<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\interfaces\service;

/**
 * Service Interface
 * Defines a set of methods common to all services
 * 
 * Services represent functionality that automatically hooks into
 * events, output, and object behavior to provide additional options,
 * formatting, or supporting logic.
 * 
 * Well formed services do not intrinsically alter execution,
 * but provide more robust or varied options for behavior, allowing
 * the underlying logic more options for how to handle scenarios presented.
 * 
 * A good example of a service would be an Oauth2 connector,
 * or a driver for a database type that does not ship with
 * the underlying package.
 * 
 * Services should not make execution decisions
 * or have opinion about what should occur.
 *
 * @author Brian Dayhoff
 */
interface ServiceInterface extends \Oroboros\core\interfaces\pluggable\PluggableInterface, \Oroboros\core\interfaces\pattern\command\CommandSubjectInterface, \Oroboros\core\interfaces\library\event\EventWatcherInterface, \Oroboros\core\interfaces\library\filter\FilterWatcherInterface
{

    /**
     * Any additional supporting libraries provided by the service
     * @return array
     */
    public function libraries(): array;

    /**
     * Any filters that should be run against output,
     * and the data definitions that trigger them
     * @return array
     */
    public function filters(): array;

    /**
     * Any hooks that should fire on events,
     * and the event definitions that trigger them
     * @return array
     */
    public function hooks(): array;

    /**
     * Any drivers that enable accessing additional external resources
     * @return array
     */
    public function drivers(): array;

    /**
     * Any additional job actions provided by the extension
     * @return array
     */
    public function actions(): array;
}
