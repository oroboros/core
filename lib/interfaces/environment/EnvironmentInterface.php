<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\interfaces\environment;

/**
 * Environment Interface
 * This interface contains immutable class constants corresponding to the environment values read at compile time.
 * 
 * This acts as a reference point to insure that system environment values are easily accessible,
 * and also prevents loading classes without the standard bootstrap process from running
 * (missing global constants will prevent this interface from compiling,
 * which will in turn prevent the rest of the classes in the package from compiling).
 * 
 * @author Brian Dayhoff
 */
interface EnvironmentInterface
{

    /**
     * Whether or not a CI server was detected
     * (eg Jenkins, CodeShip, Travis CI, Bitbucket Pipelines, Gitlab, etc).
     * If this is true, it indicates that runtime is running in a test environment,
     * which may entail loading alternate configurations.
     */
    const CI = OROBOROS_CORE_CI;

    /**
     * The defined runtime environment. Default is local.
     */
    const ENVIRONMENT = OROBOROS_CORE_ENVIRONMENT;

    /**
     * Whether or not debug mode is currently enabled.
     */
    const DEBUG = OROBOROS_CORE_DEBUG;

    /**
     * The hostname for the current request.
     */
    const HOSTNAME = OROBOROS_CORE_HOST;

    /**
     * The full uri of the current request.
     */
    const URI = OROBOROS_CORE_URI;

    /**
     * Whether or not the current request originated from the command line.
     * If this is false, the request is http.
     */
    const IS_CLI = OROBOROS_CORE_CLI;

    /**
     * Whether SSL applies to the current request.
     * This will always be false if the request originated from the command line.
     */
    const IS_SSL = OROBOROS_CORE_SSL;

    /**
     * The root folder for the base package.
     */
    const PACKAGE_ROOT = OROBOROS_CORE_BASE_DIRECTORY;

    /**
     * The root folder location of the config directory.
     */
    const CONFIG_ROOT = OROBOROS_CORE_CONFIG;

    /**
     * The root folder location of the setup directory.
     */
    const SETUP_ROOT = OROBOROS_CORE_SETUP;

    /**
     * The root folder location of the base package php code.
     */
    const PACKAGE_SOURCE_ROOT = OROBOROS_CORE_LIB;

    /**
     * The composer vendor directory.
     */
    const PACKAGE_VENDOR_ROOT = OROBOROS_CORE_VENDOR;

    /**
     * The configured template directory.
     */
    const PACKAGE_TEMPLATE_ROOT = OROBOROS_CORE_TEMPLATES;

    /**
     * The configured cache directory.
     * This will be false if no writable cache directories could be detected.
     */
    const PACKAGE_CACHE_ROOT = OROBOROS_CORE_CACHE;

    /**
     * The configured log directory.
     * This will be false if no writable log directories could be detected.
     */
    const PACKAGE_LOG_ROOT = OROBOROS_CORE_LOGS;

    /**
     * The root Psr-4 vendor/package namespace prefix for the base package.
     */
    const PACKAGE_NAMESPACE_ROOT = OROBOROS_CORE_NAMESPACE;

    /**
     * The $PATH variable from the system.
     * Split on colon for a list of global path directories.
     */
    const TMP_DIRECTORY = OROBOROS_CORE_TMP;

    /**
     * The $PATH variable from the system.
     * Split on colon for a list of global path directories.
     */
    const SYSTEM_PATH = OROBOROS_CORE_SYSTEM_PATH;

    /**
     * The system language definition. This can be used to apply i18n standards to logging if required.
     * This is the system language, not the client http request language,
     * and should translate log messages but not neccessarily output content.
     */
    const SYSTEM_LANG = OROBOROS_CORE_LANG;

    /**
     * The user name of the current system user executing the request.
     * This will either be the webserver username, or the command line username if called from cli.
     */
    const SYSTEM_USER = OROBOROS_CORE_SERVER_USERNAME;

    /**
     * The user primary group name of the current system user executing the request.
     * This will either be the webserver user group, or the command line user group if called from cli.
     */
    const SYSTEM_GROUP = OROBOROS_CORE_SERVER_USERGROUP;

}
