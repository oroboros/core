<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\http;

/**
 * Http Response Trait
 * A set of common utilities for handling http responses
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
trait HttpResponseTrait
{

    /**
     * Sets the http status code for the response payload
     * @param int $code
     * @return $this (method chainable)
     */
    protected function addStatusCode(int $code): \Oroboros\core\interfaces\controller\HttpControllerInterface
    {
        $this->getLogger()->debug('[type][scope][class] HTTP response status code set to [code].', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'code' => $code
        ]);
        $this->getResponseDirector()->getWorker('response')->set('code', 'status', $code);
        return $this;
    }

    /**
     * Returns the currently defined http status code
     * @return int
     */
    protected function getStatusCode(): int
    {
        return $this->getResponseDirector()->getWorker('response')->get('code', 'status');
    }

    protected function addCookie(string $category, string $key, $value): \Oroboros\core\interfaces\controller\HttpControllerInterface
    {
        $this->getLogger()->debug('[type][scope][class] Adding cookie [cookie-name] in category [cookie-category].', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'cookie-category' => $category,
            'cookie-name' => $key,
        ]);
        $this->getResponseDirector()->getWorker('cookie')->set($category, $key, $value);
        return $this;
    }

    protected function removeCookie(string $category, string $key): \Oroboros\core\interfaces\controller\HttpControllerInterface
    {
        $this->getLogger()->debug('[type][scope][class] Removing cookie [cookie-name] in category [cookie-category].', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'cookie-category' => $category,
            'cookie-name' => $key,
        ]);
        $this->getResponseDirector()->getWorker('cookie')->remove($category, $key);
        return $this;
    }

    protected function hasCookie(string $category, string $key): bool
    {
        return $this->getResponseDirector()->getWorker('cookie')->has($category, $key);
    }

    protected function resetCookie(): \Oroboros\core\interfaces\controller\HttpControllerInterface
    {
        $this->getLogger()->debug('[type][scope][class] Cookies reset.', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
        ]);
        $this->getResponseDirector()->getWorker('cookie')->reset();
        return $this;
    }

    protected function buildCookie(array $flags = [])
    {
        return $this->getResponseDirector()->getWorker('cookie')->build($flags);
    }

    protected function addHeader(string $key, $value): \Oroboros\core\interfaces\controller\HttpControllerInterface
    {
        $this->getLogger()->debug('[type][scope][class] Adding header [header-name].', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'header-name' => $key,
        ]);
        $this->getResponseDirector()->getWorker('header')->set('header', $key, $value);
        return $this;
    }

    protected function removeHeader(string $key): \Oroboros\core\interfaces\controller\HttpControllerInterface
    {
        $this->getLogger()->debug('[type][scope][class] Removing header [header-name].', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'header-name' => $key,
        ]);
        $this->getResponseDirector()->getWorker('header')->remove('header', $key);
        return $this;
    }

    protected function hasHeader(string $key): bool
    {
        return $this->getResponseDirector()->getWorker('header')->has('header', $key);
    }

    protected function resetHeader(): \Oroboros\core\abstracts\controller\AbstractHttpController
    {
        $this->getResponseDirector()->getWorker('header')->reset();
        return $this;
    }

    protected function buildHeader(array $flags = [])
    {
        return $this->getResponseDirector()->getWorker('header')->build($flags);
    }

    /**
     * Redirects the page to a new location
     * 
     * If you are using this method in a controller,
     * just return the value of this so the bootstrap
     * can close up clean. Do not `exit` or `die` from in
     * the controller.
     * 
     * @param string $uri The uri to redirect to
     * @param int $code `301` or `302` default `301`
     * @return \Oroboros\core\interfaces\controller\HttpControllerInterface
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    protected function redirect(string $uri, int $code = 301): \Oroboros\core\interfaces\controller\HttpControllerInterface
    {
        if (!in_array($code, [301, 302], true)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Provided status code '
                        . '[%2$s] is not a valid redirect code.', get_class($this), $code)
            );
        }
        $this->getLogger()->debug('[type][scope][class] Triggered redirection to [uri].', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'uri' => $uri,
        ]);
//        $this->resetHeader();
//        $this->addHeader('Location', $uri);
//        $this->addStatusCode($code);
        header("HTTP/1.1 301 Moved Permanently");
        header(sprintf('Location: %1$s', $uri));
        return $this;
    }
}
