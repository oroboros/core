<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\http;

/**
 * Http Controller Common Utility
 * A set of common utilities for handling http requests
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
trait HttpControllerCommonUtility
{

    use HttpResponseTrait;

    /**
     * Container of headers to output with the request.
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $headers = null;

    /**
     * The builder associated with the response, which packages the page details
     * required to send to the view for construction of the response object.
     * 
     * This object will generate the collection handed off to the view
     * when the render method is called.
     * 
     * @var \Oroboros\core\library\http\ResponseBuilder
     */
    private $response_builder = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeResponseBuilder();
        $this->setObserverEvent('http-ready');
    }

    /**
     * Expands upon the underlying abstract, to parse script inheritance for
     * html requests, and aggregate client side performance optimizations.
     * @param string $format
     * @param string $template
     * @param array $flags
     * @return $this (method chainable)
     * @note a lot of this will eventually migrate to the html view.
     */
    public function render(string $format, string $template, array $flags = null): \Oroboros\core\interfaces\controller\ControllerInterface
    {
        $this->getLogger()->debug('[type][scope][class] Rendering output in format [format] with template [template].', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'format' => $format,
            'template' => $template,
        ]);
        return parent::render($format, $template, $flags);
    }

    /**
     * Handles http errors that occur within the controller scope.
     * 
     * @param int $code The status code for the error
     * @param string $message Optional message body
     */
    public function error(int $code = null, string $message = null, \Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\ErrorControllerInterface
    {
        if (is_null($code)) {
            $code = 404;
        }
        return parent::error($code, $message, $arguments, $flags);
    }

    public function getResponseBuilders(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $container = $this::containerize();
        $container['header'] = $this->load('library', 'http\\HeaderBuilder');
        $container['cookie'] = $this->load('library', 'http\\CookieBuilder');
        return $container;
    }
}
