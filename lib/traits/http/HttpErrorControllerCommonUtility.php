<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\http;

/**
 * Http Error Controller Common Utility
 * A set of common utilities for handling http errors
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
trait HttpErrorControllerCommonUtility
{

    use HttpControllerCommonUtility;

    public function error(int $code = null, string $message = null, \Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\ErrorControllerInterface
    {
        if (is_null($code)) {
            $code = static::DEFAULT_ERROR_CODE;
        }
        http_response_code($code);
        $this->setObserverEvent('error');
        if ($this::app()->debugEnabled() && $this->hasArgument('error')) {
            $this->getLogger()->debug(sprintf('[type][scope][class] HTTP Error '
                    . 'controller triggered with message [error-message] occurred.'
                    . '%1$s%2$s- At line [error-line] of file [error-file]'
                    . '%1$s%2$s- Trace: [error-trace].', PHP_EOL, '    '), [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
                'error-message' => $this->getArgument('error')->getMessage(),
                'error-file' => $this->getArgument('error')->getFile(),
                'error-line' => $this->getArgument('error')->getLine(),
                'error-trace' => $this->getArgument('error')->getTraceAsString(),
            ]);
        }
    }
}
