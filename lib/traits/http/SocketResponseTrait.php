<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\http;

/**
 * Provides abstraction methods for controllers to serve socket request content,
 * and package it appropriately to send to the view
 * 
 * This trait must be implemented on a controller,
 * which provides the underlying existing abstraction used within it.
 * 
 * These methods grant socket controllers a simplified means to set data that
 * will be parsed into json response view output,
 * without concern for actual format (which is a view consideration).
 * 
 * These methods are attached to the lowest level socket controller and error controller.
 * Further controller abstraction specific to an application should
 * abstract most of this away, so it is not a concern for high level controllers.
 * (eg, use a decorator, model, or other construct that populates
 * the default values from a database or something)
 * 
 * @author Brian Dayhoff
 * @note This trait assumes it will be attached to
 *     an instance of \Oroboros\core\interfaces\controllers\SocketControllerInterface.
 *     Do not use this trait on other classes that do not extend from that.
 */
trait SocketResponseTrait
{

    /**
     * Container of script objects representing known scripts.
     * @var Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $scripts = null;

    /**
     * Container of stylesheet objects representing known stylesheets.
     * @var Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $styles = null;

    /**
     * Container of font objects representing known fonts.
     * @var Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $fonts = null;

    /**
     * The layout object used to provide the page structure.
     * @var Oroboros\core\interfaces\library\layout\LayoutInterface
     */
    private $layout;

    /**
     * Expands upon the underlying abstract, to parse script inheritance for
     * html requests, and aggregate client side performance optimizations.
     * @param string $format
     * @param string $template
     * @param array $flags
     * @return $this (method chainable)
     * @note a lot of this will eventually migrate to the html view.
     */
    public function render(string $format, string $template, array $flags = null): \Oroboros\core\interfaces\controller\ControllerInterface
    {
        if (is_null($flags)) {
            $flags = [];
        }
        $preconnect = [];
        $prefetch = [];
        $preload = [];
        if (property_exists($flags, 'scripts')) {
            // As scripts can be registered in the DOM in three locations,
            // the aggregation of scripts is more complex than other assets,
            // and they hence have their own dedicated aggregation scheme.
            $scripts = $this->aggregateScriptsBySection($flags['scripts']);
            $flags['scripts'] = $scripts;
            foreach ($scripts as $set) {
                $preconnect = $this->getPreconnectTags($set, $preconnect);
                $prefetch = $this->getPrefetchTags($set, $prefetch);
                $preload = $this->getPreloadTags($set, $preload);
            }
        }
        if (property_exists($flags, 'styles')) {
            $styles = $this->aggregateStylesheetDependencies($flags['styles']);
            $flags['styles'] = $styles;
            $preconnect = $this->getPreconnectTags($styles, $preconnect);
            $prefetch = $this->getPrefetchTags($styles, $prefetch);
            $preload = $this->getPreloadTags($styles, $preload);
        }
        if (property_exists($flags, 'fonts')) {
            $fonts = $this->aggregateFontDependencies($flags['fonts']);
            $flags['fonts'] = $fonts;
            $preconnect = $this->getPreconnectTags($fonts, $preconnect);
            $prefetch = $this->getPrefetchTags($fonts, $prefetch);
            $preload = $this->getPreloadTags($fonts, $preload);
        }
        $flags['head']['preconnect'] = $preconnect;
        $flags['head']['prefetch'] = $prefetch;
        $flags['head']['preload'] = $preload;
        return parent::render($format, $template, $flags);
    }

    /**
     * Returns a script object representing the provided key,
     * or null if it does not exist.
     * 
     * @param string $key The keyword representing the requested asset
     * @return null|\Oroboros\core\library\http\Script
     */
    protected function getScript(string $key)
    {
        if (!$this->scripts->has($key)) {
            return null;
        }
        return $this->scripts->get($key);
    }

    /**
     * Returns a stylesheet object representing the provided key,
     * or null if it does not exist.
     * 
     * @param string $key The keyword representing the requested asset
     * @return null|\Oroboros\core\library\http\Stylesheet
     */
    protected function getStyle(string $key)
    {
        if (!$this->styles->has($key)) {
            return null;
        }
        return $this->styles->get($key);
    }

    /**
     * Returns a font object representing the provided key,
     * or null if it does not exist.
     * 
     * @param string $key The keyword representing the requested asset
     * @return null|\Oroboros\core\library\http\Font
     */
    protected function getFont(string $key)
    {
        if (!$this->fonts->has($key)) {
            return null;
        }
        return $this->fonts->get($key);
    }

    /**
     * 
     * @param string $value
     * @return $this (method chainable)
     */
    protected function addCharset(string $value): \Oroboros\core\abstracts\controller\AbstractHttpController
    {
        $this->getResponseDirector()->getWorker('meta')->set('charset', 'charset', $value);
        return $this;
    }

    /**
     * 
     * @param string $value
     * @return $this (method chainable)
     */
    protected function addLanguage(string $value): \Oroboros\core\abstracts\controller\AbstractHttpController
    {
        $this->getResponseDirector()->getWorker('header')->set('header', 'Content-Language', $value);
        $this->getResponseDirector()->getWorker('response')->set('lang', 'page', $value);
        return $this;
    }

    /**
     * 
     * @param string $value
     * @return $this (method chainable)
     */
    protected function addPageId(string $value): \Oroboros\core\abstracts\controller\AbstractHttpController
    {
        $this->getResponseDirector()->getWorker('body')->set('id', 'page', $value);
        return $this;
    }

    /**
     * 
     * @param string $value
     * @return $this (method chainable)
     */
    protected function addPageClass(string $value): \Oroboros\core\abstracts\controller\AbstractHttpController
    {
        $this->getResponseDirector()->getWorker('body')->set('class', 'page', $value);
        return $this;
    }

    /**
     * 
     * @param string $value
     * @return $this (method chainable)
     */
    protected function addViewport(string $value): \Oroboros\core\abstracts\controller\AbstractHttpController
    {
        $this->getResponseDirector()->getWorker('meta')->set('viewport', 'name', 'viewport');
        $this->getResponseDirector()->getWorker('meta')->set('viewport', 'content', $value);
        return $this;
    }

    /**
     * 
     * @param string $value
     * @return $this (method chainable)
     */
    protected function addTitle(string $value): \Oroboros\core\abstracts\controller\AbstractHttpController
    {
        $this->getResponseDirector()->getWorker('head')->set('title', 'page', $value);
        return $this;
    }

    /**
     * 
     * @param string $value
     * @return $this (method chainable)
     */
    protected function addAuthor(string $value): \Oroboros\core\abstracts\controller\AbstractHttpController
    {
        $this->getResponseDirector()->getWorker('meta')->set('author', 'name', 'author');
        $this->getResponseDirector()->getWorker('meta')->set('author', 'content', $value);
        return $this;
    }

    /**
     * 
     * @param string $value
     * @return $this (method chainable)
     */
    protected function addDescription(string $value): \Oroboros\core\abstracts\controller\AbstractHttpController
    {
        $this->getResponseDirector()->getWorker('meta')->set('description', 'name', 'description');
        $this->getResponseDirector()->getWorker('meta')->set('description', 'content', $value);
        return $this;
    }

    /**
     * Sets arbitrary meta tags.
     * @param string $name Name of the meta tag
     * @param array $values Associative array of attribute > value(s) for the meta tag
     * @return $this (method chainable)
     */
    protected function addMeta(string $name, array $values): \Oroboros\core\abstracts\controller\AbstractHttpController
    {
        $this->getResponseDirector()->getWorker('meta')->set('meta', $name, $values);
        return $this;
    }

    /**
     * 
     * @param string $category
     * @param string $key
     * @return $this (method chainable)
     */
    protected function removeMeta(string $category, string $key): \Oroboros\core\abstracts\controller\AbstractHttpController
    {
        $this->getResponseDirector()->getWorker('meta')->remove($category, $key);
        return $this;
    }

    /**
     * 
     * @param string $category
     * @param string $key
     * @return bool
     */
    protected function hasMeta(string $category, string $key): bool
    {
        return $this->getResponseDirector()->getWorker('meta')->has($category, $key);
    }

    /**
     * 
     * @return $this (method chainable)
     */
    protected function resetMeta(): \Oroboros\core\abstracts\controller\AbstractHttpController
    {
        $this->getResponseDirector()->getWorker('meta')->reset();
        return $this;
    }

    /**
     * 
     * @param array $flags
     * @return type
     */
    protected function buildMeta(array $flags = [])
    {
        return $this->getResponseDirector()->getWorker('meta')->build($flags);
    }

    /**
     * 
     * @param string $category
     * @param string $key
     * @param type $value
     * @return $this (method chainable)
     */
    protected function addScript(string $category, string $key, $value): \Oroboros\core\abstracts\controller\AbstractHttpController
    {
        $this->getResponseDirector()->getWorker('script')->set($category, $key, $value);
        return $this;
    }

    /**
     * 
     * @param string $category
     * @param string $key
     * @return $this (method chainable)
     */
    protected function removeScript(string $category, string $key): \Oroboros\core\abstracts\controller\AbstractHttpController
    {
        $this->getResponseDirector()->getWorker('script')->remove($category, $key);
        return $this;
    }

    /**
     * 
     * @param string $category
     * @param string $key
     * @return bool
     */
    protected function hasScript(string $category, string $key): bool
    {
        return $this->getResponseDirector()->getWorker('script')->has($category, $key);
    }

    /**
     * 
     * @return $this (method chainable)
     */
    protected function resetScript(): \Oroboros\core\abstracts\controller\AbstractHttpController
    {
        $this->getResponseDirector()->getWorker('script')->reset();
        return $this;
    }

    /**
     * 
     * @param array $flags
     * @return $this (method chainable)
     */
    protected function buildScript(array $flags = [])
    {
        return $this->getResponseDirector()->getWorker('script')->build($flags);
    }

    /**
     * 
     * @param string $category
     * @param string $key
     * @param type $value
     * @return $this (method chainable)
     */
    protected function addStylesheet(string $category, string $key, $value): \Oroboros\core\abstracts\controller\AbstractHttpController
    {
        $this->getResponseDirector()->getWorker('stylesheet')->set($category, $key, $value);
        return $this;
    }

    /**
     * 
     * @param string $category
     * @param string $key
     * @return $this (method chainable)
     */
    protected function removeStylesheet(string $category, string $key): \Oroboros\core\abstracts\controller\AbstractHttpController
    {
        $this->getResponseDirector()->getWorker('stylesheet')->remove($category, $key);
        return $this;
    }

    /**
     * 
     * @param string $category
     * @param string $key
     * @return bool
     */
    protected function hasStylesheet(string $category, string $key): bool
    {
        return $this->getResponseDirector()->getWorker('stylesheet')->has($category, $key);
    }

    /**
     * 
     * @return $this (method chainable)
     */
    protected function resetStylesheet(): \Oroboros\core\abstracts\controller\AbstractHttpController
    {
        $this->getResponseDirector()->getWorker('stylesheet')->reset();
        return $this;
    }

    /**
     * 
     * @param array $flags
     * @return type
     */
    protected function buildStylesheet(array $flags = [])
    {
        return $this->getResponseDirector()->getWorker('stylesheet')->build($flags);
    }

    /**
     * 
     * @param string $category
     * @param string $key
     * @param type $value
     * @return $this (method chainable)
     */
    protected function addFont(string $category, string $key, $value): \Oroboros\core\abstracts\controller\AbstractHttpController
    {
        $this->getResponseDirector()->getWorker('font')->set($category, $key, $value);
        return $this;
    }

    /**
     * 
     * @param string $category
     * @param string $key
     * @return $this (method chainable)
     */
    protected function removeFont(string $category, string $key): \Oroboros\core\abstracts\controller\AbstractHttpController
    {
        $this->getResponseDirector()->getWorker('font')->remove($category, $key);
        return $this;
    }

    /**
     * 
     * @param string $category
     * @param string $key
     * @return bool
     */
    protected function hasFont(string $category, string $key): bool
    {
        return $this->getResponseDirector()->getWorker('font')->has($category, $key);
    }

    /**
     * 
     * @return $this (method chainable)
     */
    protected function resetFont(): \Oroboros\core\abstracts\controller\AbstractHttpController
    {
        $this->getResponseDirector()->getWorker('font')->reset();
        return $this;
    }

    /**
     * 
     * @param array $flags
     * @return type
     */
    protected function buildFont(array $flags = [])
    {
        return $this->getResponseDirector()->getWorker('font')->build($flags);
    }

    /**
     * 
     * @param string $category
     * @param string $key
     * @param type $value
     * @return $this (method chainable)
     */
    protected function addData(string $category, string $key, $value): \Oroboros\core\abstracts\controller\AbstractHttpController
    {
        $this->getResponseDirector()->getWorker('data')->set($category, $key, $value);
        return $this;
    }

    /**
     * 
     * @param string $category
     * @param string $key
     * @return $this (method chainable)
     */
    protected function removeData(string $category, string $key): \Oroboros\core\abstracts\controller\AbstractHttpController
    {
        $this->getResponseDirector()->getWorker('data')->remove($category, $key);
        return $this;
    }

    /**
     * 
     * @param string $category
     * @param string $key
     * @return bool
     */
    protected function hasData(string $category, string $key): bool
    {
        return $this->getResponseDirector()->getWorker('data')->has($category, $key);
    }

    /**
     * 
     * @return $this (method chainable)
     */
    protected function resetData(): \Oroboros\core\abstracts\controller\AbstractHttpController
    {
        $this->getResponseDirector()->getWorker('data')->reset();
        return $this;
    }

    /**
     * 
     * @param array $flags
     * @return type
     */
    protected function buildData(array $flags = [])
    {
        return $this->getResponseDirector()->getWorker('data')->build($flags);
    }

    /**
     * 
     * @param string $category
     * @param string $key
     * @param type $value
     * @return $this (method chainable)
     */
    protected function addContent(string $category, string $key, $value): \Oroboros\core\abstracts\controller\AbstractHttpController
    {
        $this->getLayout()->setContent($category, $key, $value);
//        $this->getResponseDirector()->getWorker('body')->set($category, $key, $value);
        return $this;
    }

    /**
     * 
     * @param string $category
     * @param string $key
     * @return $this (method chainable)
     */
    protected function removeContent(string $category, string $key): \Oroboros\core\abstracts\controller\AbstractHttpController
    {
        $this->getResponseDirector()->getWorker('body')->remove($category, $key);
        return $this;
    }

    /**
     * 
     * @param string $category
     * @param string $key
     * @return bool
     */
    protected function hasContent(string $category, string $key): bool
    {
        return $this->getResponseDirector()->getWorker('body')->has($category, $key);
    }

    /**
     * 
     * @return $this (method chainable)
     */
    protected function resetContent(): \Oroboros\core\abstracts\controller\AbstractHttpController
    {
        $this->getResponseDirector()->getWorker('body')->reset();
        return $this;
    }

    /**
     * Returns a component instance representing the given name
     * @return \Oroboros\core\interfaces\library\component\ComponentInterface
     */
    protected function getComponent(string $name, string $key, array $details = [], array $flags = []): \Oroboros\core\interfaces\library\component\ComponentInterface
    {
        $details['builder'] = $this->getResponseDirector()->getWorker('component');
        return $this->load('component', $name, $key, $details, $flags);
    }

    /**
     * 
     * @param array $flags
     * @return type
     */
    protected function buildContent(array $flags = [])
    {
        return $this->getResponseDirector()->getWorker('body')->build($flags);
    }

    protected function getLayout(): \Oroboros\core\interfaces\library\layout\LayoutInterface
    {
        return $this->layout;
    }

    protected function initializeLayout(string $layout = null): void
    {
        if (is_null($layout)) {
            $layout = static::CLASS_SCOPE . '\\' . static::LAYOUT_CLASS;
        }
        $this->layout = $this->load('layout', $layout, null, ['director' => $this->getResponseDirector()]);
    }

    /**
     * Builds the output to pass to the view.
     * The page layout will be added to the original context.
     * @param array $flags
     * @return Oroboros\core\interfaces\library\container\ResponseContainerInterface
     */
    protected function buildOutput(array $flags = []): \Oroboros\core\interfaces\library\container\ResponseContainerInterface
    {
        $layout = $this->getLayout();
        $this->getResponseDirector()->getWorker('font')->set('layout', 'page', $layout);
        $this->getResponseDirector()->getWorker('script')->set('layout', 'page', $layout);
        $this->getResponseDirector()->getWorker('stylesheet')->set('layout', 'page', $layout);
        $this->getResponseDirector()->getWorker('body')->set('layout', 'page', $layout);
        $output = $this->getResponseDirector()->build($flags);
        return $output;
    }

    /**
     * Establishes a set of script objects that handle script output,
     * so scripts can be referenced by keyword without further concern.
     * @return void
     */
    private function initializeScripts()
    {
        $scripts = [];
        foreach ($this->getModel('script')->fetch() as $key => $value) {
            $scripts[$key] = $this->load('library', 'http\\Script', $key, $value);
        }
        $this->scripts = $this::containerize($scripts);
    }

    /**
     * Establishes a set of stylesheet objects that handle script output,
     * so stylesheets can be referenced by keyword without further concern.
     * @return void
     */
    private function initializeStyles()
    {
        $styles = [];
        foreach ($this->getModel('style')->fetch() as $key => $value) {
            $styles[$key] = $this->load('library', 'http\\Stylesheet', $key, $value);
        }
        $this->styles = $this::containerize($styles);
    }

    /**
     * Establishes a set of font objects that handle script output,
     * so fonts can be referenced by keyword without further concern.
     * @return void
     */
    private function initializeFonts()
    {
        $fonts = [];
        foreach ($this->getModel('font')->fetch() as $key => $value) {
            $fonts[$key] = $this->load('library', 'http\\Font', $key, $value);
        }
        $this->fonts = $this::containerize($fonts);
    }

    /**
     * Recurses script declarations by section, orders them so that dependencies
     * are included prior to dependents, and filters out any duplicate listings
     * that come up in separate sections.
     * 
     * The end result is that script dependencies are always declared prior to
     * their dependents, and no duplicate script inclusion will occur across sections.
     * In the event that a duplicate is encountered, the section containing the
     * duplicate that occurs earliest in the DOM will retain priority,
     * and other later includes will be removed.
     * 
     * @param array $scripts
     * @return array
     */
    private function aggregateScriptsBySection(array $scripts)
    {
        $results = [];
        foreach ($scripts as $section => $required) {
            if (empty($required)) {
                $results[$section] = [];
                continue;
            }
            $results[$section] = $this->aggregateScriptDependencies($required);
        }
        return $this->filterDuplicateIncludes($results);
    }

    /**
     * Recursively scrapes script dependencies, and orders them so
     * dependencies are always listed prior to their dependents.
     * 
     * @param array $scripts
     * @return array
     */
    private function aggregateScriptDependencies(array $scripts)
    {
        $results = [];
        foreach ($scripts as $script) {
            if (!$this->scripts->has($script)) {
                continue;
            }
            $object = $this->scripts->get($script);
            $deps = $this->getScriptDependencies($object);
            foreach ($deps as $key => $dep) {
                if (!property_exists($results, $key)) {
                    $results[$key] = $dep;
                }
            }
            $results[$object->getKey()] = $object;
        }
        return $results;
    }

    /**
     * Insures that scripts do not have duplicate includes.
     * This method will retain the earliest include point for scripts,
     * and remove any subsequent declarations.
     * 
     * As scripts are already ordered by dependency chain in each subsequent section,
     * this insures that dependencies are still included with all of their
     * sub-dependencies prior to their declaration, but are not declared
     * more than one time.
     * 
     * @param array $dependencies
     * @return array
     */
    private function filterDuplicateIncludes(array $dependencies)
    {
        $includes = [];
        foreach ($dependencies as $section => $elements) {
            $keys = array_keys($elements);
            foreach ($keys as $key) {
                if (in_array($key, $includes)) {
                    unset($dependencies[$section][$key]);
                    continue;
                }
                $includes[] = $key;
            }
        }
        return $dependencies;
    }

    /**
     * Recursively scrapes script dependencies for a single script,
     * and returns an array ordered by dependency chain.
     * 
     * If any dependency is not found, will return an empty set to prevent
     * inclusion of any invalid scripts, or any scripts that depend on
     * invalid scripts.
     * 
     * @param \Oroboros\core\interfaces\library\http\ScriptInterface $script
     * @return array
     */
    private function getScriptDependencies(\Oroboros\core\interfaces\library\http\ScriptInterface $script)
    {
        $results = [];
        $deps = $script->getDependencies();
        foreach ($deps as $dependency) {
            if (!$this->scripts->has($dependency)) {
                // Script is not valid, as it's dependencies do not exist.
                // The entire chain is invalid, return an empty set.
                return [];
            }
            foreach ($this->getScriptDependencies($this->scripts->get($dependency)) as $key => $dep) {
                if (!property_exists($results, $key)) {
                    $results[$key] = $dep;
                }
            }
        }
        $results[$script->getKey()] = $script;
        return $results;
    }

    /**
     * Recursively scrapes stylesheet dependencies, and orders them so
     * dependencies are always listed prior to their dependents.
     * 
     * @param array $styles
     * @return array
     */
    private function aggregateStylesheetDependencies(array $styles)
    {
        $results = [];
        foreach ($styles as $style) {
            if (!$this->styles->has($style)) {
                continue;
            }
            $object = $this->styles->get($style);
            $deps = $this->getStylesheetDependencies($object);
            foreach ($deps as $key => $dep) {
                if (!property_exists($results, $key)) {
                    $results[$key] = $dep;
                }
            }
            $results[$object->getKey()] = $object;
        }
        return $results;
    }

    /**
     * Recursively scrapes stylesheet dependencies for a single stylesheet,
     * and returns an array ordered by dependency chain.
     * 
     * If any dependency is not found, will return an empty set to prevent
     * inclusion of any invalid stylesheets, or any stylesheets that depend on
     * invalid stylesheets.
     * 
     * @param \Oroboros\core\interfaces\library\http\StylesheetInterface $stylesheet
     * @return array
     */
    private function getStylesheetDependencies(\Oroboros\core\interfaces\library\http\StylesheetInterface $stylesheet)
    {
        $results = [];
        $deps = $stylesheet->getDependencies();
        foreach ($deps as $dependency) {
            if (!$this->styles->has($dependency)) {
                // Stylesheet is not valid, as it's dependencies do not exist.
                // The entire chain is invalid, return an empty set.
                return [];
            }
            foreach ($this->getStylesheetDependencies($this->styles->get($dependency)) as $key => $dep) {
                if (!property_exists($results, $key)) {
                    $results[$key] = $dep;
                }
            }
        }
        $results[$stylesheet->getKey()] = $stylesheet;
        return $results;
    }

    /**
     * Recursively scrapes font dependencies, and orders them so
     * dependencies are always listed prior to their dependents.
     * 
     * @param array $fonts
     * @return array
     */
    private function aggregateFontDependencies(array $fonts)
    {
        $results = [];
        foreach ($fonts as $font) {
            if (!$this->fonts->has($font)) {
                continue;
            }
            $object = $this->fonts->get($font);
            $deps = $this->getFontDependencies($object);
            foreach ($deps as $key => $dep) {
                if (!property_exists($results, $key)) {
                    $results[$key] = $dep;
                }
            }
            $results[$object->getKey()] = $object;
        }
        return $results;
    }

    /**
     * Recursively scrapes font dependencies for a single font,
     * and returns an array ordered by dependency chain.
     * 
     * If any dependency is not found, will return an empty set to prevent
     * inclusion of any invalid fonts, or any font that depend on
     * invalid fonts.
     * 
     * @param \Oroboros\core\interfaces\library\http\FontInterface $font
     * @return array
     */
    private function getFontDependencies(\Oroboros\core\interfaces\library\http\FontInterface $font)
    {
        $results = [];
        $deps = $font->getDependencies();
        foreach ($deps as $dependency) {
            if (!$this->fonts->has($dependency)) {
                // Font is not valid, as it's dependencies do not exist.
                // The entire chain is invalid, return an empty set.
                return [];
            }
            foreach ($this->getFontDependencies($this->fonts->get($dependency)) as $key => $dep) {
                if (!property_exists($results, $key)) {
                    $results[$key] = $dep;
                }
            }
        }
        $results[$font->getKey()] = $font;
        return $results;
    }

    /**
     * Returns an array of prefetch tags for a given set of assets.
     * Assets should be an iterable set of \Oroboros\core\interfaces\library\http\DependencyInterface objects.
     * 
     * @param array $assets
     * @param array $existing
     * @return array
     */
    private function getPrefetchTags(array $assets, array $existing = [])
    {
        $tags = $existing;
        foreach ($assets as $asset) {
            $tag = $asset->getPrefetch();
            if (!in_array($tag, $tags)) {
                $tags[] = $tag;
            }
        }
        return $tags;
    }

    /**
     * Returns an array of preload tags for a given set of assets.
     * Assets should be an iterable set of \Oroboros\core\interfaces\library\http\DependencyInterface objects.
     * 
     * @param array $assets
     * @param array $existing
     * @return array
     */
    private function getPreloadTags(array $assets, array $existing = [])
    {
        $tags = $existing;
        foreach ($assets as $asset) {
            $tag = $asset->getPreload();
            if (!in_array($tag, $tags)) {
                $tags[] = $tag;
            }
        }
        return $tags;
    }

    /**
     * Returns an array of preconnect tags for a given set of assets.
     * Assets should be an iterable set of \Oroboros\core\interfaces\library\http\DependencyInterface objects.
     * 
     * @param array $assets
     * @param array $existing
     * @return array
     */
    private function getPreconnectTags(array $assets, array $existing = [])
    {
        $tags = $existing;
        foreach ($assets as $asset) {
            $tag = $asset->getPreconnect();
            if (!in_array($tag, $tags)) {
                $tags[] = $tag;
            }
        }
        return $tags;
    }
}
