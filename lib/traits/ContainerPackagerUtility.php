<?php
/*
 * The MIT License
 *
 * @author Brian Dayhoff <Brian Dayhoff@me.com>
 * @copyright (c) 2018, Brian Dayhoff <Brian Dayhoff@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits;

/**
 * Container Packager Utility
 *
 * Wrap it up in a pretty bow and ship it.
 *
 * Provides automated containerization,
 * with an extension method to declare a
 * different container class if needed.
 *
 * @see \Oroboros\core\abstracts\library\container\AbstractContainer
 * @see \Oroboros\core\interfaces\library\container\ContainerInterface
 * @see \Oroboros\core\traits\ContainerUtilityTrait
 *
 * @author Brian Dayhoff <Brian Dayhoff@me.com>
 */
trait ContainerPackagerUtility
{

    /**
     * Packages an array in an interable, countable, array-accessible, serializable,
     * json-serializable, Psr-11 compliant container object.
     *
     * These container objects can be used interchangeably as both an object and an array.
     *
     * @example $container['id'] === $container->id //evaluates to true
     *
     * @param array $subject The contents to package in a container.
     * @param int $count The limit to how large the container can be.
     *     The default is -1 (no limit). Any integer value greater than -1
     *     will prevent the container from accepting any more arguments
     *     than the defined limit.
     *     This limit is also set on recasts automatically, unless the limit
     *     is explicitly loosened programmatically on an active container instance.
     *
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     *
     * @see \Oroboros\core\abstracts\library\container\AbstractContainer
     * @see \Oroboros\core\interfaces\library\container\ContainerInterface
     * @see \Oroboros\core\traits\ContainerUtilityTrait
     *
     * @note The container class provides both a hook and a filter
     *     that can be extended  to automatically validate (through the hook)
     *     or filter contents (through the filter)
     *     at EVERY SINGLE STEP OF ITS INTERNALS.
     *     By default, none of these are activated, but if you have
     *     a special purpose container that needs to insure explicit data integrity,
     *     or an explicit output format, this is a simple answer that
     *     does not require filtering contents through an external class.
     *     This can be easily wired in by just creating specialized containers
     *     meant for specific contents.
     */
    protected static function containerize($subject = array(), $count = -1)
    {
        $expected = 'Oroboros\\core\\interfaces\\library\\container\\ContainerInterface';
        if (is_object($subject) && ( $subject instanceof $expected )) {
            $subject = $subject->toArray();
        }
        if (!is_array($subject)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Invalid parameters passed in [%1$s]. '
                        . 'Provided containerization subject must '
                        . 'be an array or an instance of [%2$s]', get_called_class(), $expected)
            );
        }
        $class = get_called_class();
        $container_class = $class::declareContainerClass();
        self::validateContainerClass($container_class);
        $instance = new $container_class($count);
        return $instance->fromArray($subject);
    }

    /**
     * Allows for a runtime determinable containerization strategy
     * without overriding the default container.
     *
     * If your class needs to be able to distribute multiple container types,
     * use this method in any context where the association needs to be
     * dynamically determined by logic outside this trait.
     *
     * Otherwise it functions identically to how `containerize` works,
     * except it takes an arbitrary container class name as its first parameter.
     *
     * @param string $container_class The name of any class
     *     that implements \Oroboros\core\interfaces\library\container\ContainerInterface
     * @param array $subject
     * @param int $count
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     *
     * @throws \Oroboros\core\\exception\core\InvalidClassException If provided
     *     with a misconfigured or non-existing container class
     */
    protected static function containerizeInto($container_class, array $subject = array(), $count = -1)
    {
        $class = get_called_class();
        self::validateContainerClass($container_class);
        $instance = new $container_class($count);
        return $instance->fromArray($subject);
    }

    /**
     * This method returns the name of the container class distributed by this method.
     *
     * It may be overridden to use an alternate, specialized container
     * in any given class without additional work.
     *
     * The provided class must be an absolute classname, and it must be a class
     * that implements `Oroboros\core\interfaces\library\container\ContainerInterface`.
     *
     * @return string
     *
     * @see \Oroboros\core\abstracts\library\container\AbstractContainer
     * @see \Oroboros\core\interfaces\library\container\ContainerInterface
     * @see \Oroboros\core\traits\ContainerUtilityTrait
     */
    protected static function declareContainerClass()
    {
        $class = get_called_class();
        if (defined($class . '::CONTAINER_CLASS')) {
            return $class::CONTAINER_CLASS;
        }
        return 'Oroboros\\core\\library\\container\\Container';
    }

    /**
     * Validates that the declared container class is a valid implementation of
     * the provided container interface, to enforce maximum interoperability.
     *
     * Any class can satisfy this requirement by doing one of the two following options:
     *
     * - Extend the provided abstract (see below).
     *     No additional method declarations are required.
     *
     * - Use the provided trait, and implement the provided interface
     *     on your own class (see below).
     *     No additional method declarations are required.
     *
     * This method may not be overridden, as the container class is integral
     * to the stable functionality of the system.
     *
     * @return void
     *
     * @throws \Oroboros\core\\exception\core\core\InvalidClassException 
     *     If the declared container does not implement the expected interface.
     * @see \Oroboros\core\abstracts\library\container\AbstractContainer
     * @see \Oroboros\core\interfaces\library\container\ContainerInterface
     * @see \Oroboros\core\traits\ContainerUtilityTrait
     * @internal
     */
    private static function validateContainerClass($container_class): void
    {
        $class = get_called_class();
        $expected_interface = 'Oroboros\\core\\interfaces\\library\\container\\ContainerInterface';
        if (!class_exists($container_class)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Class [%1$s] is misconfigured. Declared container class [%2$s] '
                        . 'is not a valid class. Please report this '
                        . 'issue to the developer of [%1$s].', $class, $container_class)
            );
        }
        $interfaces = class_implements($container_class);
        if ($interfaces === false || !in_array($expected_interface, $interfaces)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Class [%1$s] is misconfigured. Declared container class [%2$s] '
                        . 'is not a valid implementation of [%3$s]. Please report this '
                        . 'issue to the developer of [%1$s].', $class, $container_class, $expected_interface)
            );
        }
    }
}
