<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\component\html\form;

/**
 * Component Html Form Utilities Trait
 * Common methods for html components representing
 * inputs, checkboxes, buttons, textareas,
 * or other form control elements.
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
trait ComponentHtmlFormFieldUtilitiesTrait
{

    /**
     * Returns the parsed form data for the given form
     * 
     * @param \Psr\Http\Message\ServerRequestInterface $response
     * @return mixed
     * @throws \Oroboros\core\exception\OutOfBoundsException
     */
    public function unpackFormResponse(\Psr\Http\Message\ServerRequestInterface $response)
    {
        throw new \Oroboros\core\exception\OutOfBoundsException(
                sprintf('Error encountered in [%1$s]. '
                    . 'Provided response does not match the '
                    . 'criteria of this form.', get_class($this))
        );
        $name = $this->getContext('name');
        return $this->getFormParser()->fetch();
    }

    /**
     * Override this method to declare keys that should be passed from arguments 
     * into the context for the template, if they are provided.
     * @return array
     */
    protected function declareValidContextKeys(): array
    {
        return array_merge(parent::declareValidContextKeys(), $this->form_context_keys);
    }

    /**
     * Declares the context filters used to handle form context
     * 
     * @return array
     */
    protected function declareContextFilters(): array
    {
        return array_merge(parent::declareContextFilters(), $this->context_form_filters);
    }

    /**
     * Verifies that the given form field parameter is valid
     * 
     * @param mixed $value
     * @return mixed
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    protected function filterAccept($value)
    {
        $this->verifyParameter('accept', $required);
        if (is_null($value)) {
            return $value;
        }
        return $value;
    }

    /**
     * Verifies that the given form field parameter is valid
     * 
     * @param mixed $value
     * @return mixed
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    protected function filterAlt($value)
    {
        $this->verifyParameter('alt', $required);
        if (is_null($value)) {
            return $value;
        }
        return $value;
    }

    /**
     * Verifies that the given form field parameter is valid
     * 
     * @param mixed $value
     * @return mixed
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    protected function filterAutocomplete($value)
    {
        $this->verifyParameter('autocomplete', $required);
        if (is_null($value)) {
            return $value;
        }
        return $value;
    }

    /**
     * Verifies that the given form field autofocus is valid
     * 
     * @param string $autofocus
     * @return string|null
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    protected function filterAutofocus(bool $autofocus = null): ?bool
    {
        $this->verifyParameter('autofocus', $autofocus);
        if (is_null($autofocus)) {
            return $autofocus;
        }
        return $autofocus;
    }

    /**
     * Verifies that the given form field parameter is valid
     * 
     * @param mixed $value
     * @return mixed
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    protected function filterChecked($value)
    {
        $this->verifyParameter('checked', $required);
        if (is_null($value)) {
            return $value;
        }
        return $value;
    }

    /**
     * Verifies that the given form field cols is valid
     * 
     * @param string $cols
     * @return string|null
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    protected function filterCols(int $cols = null): ?int
    {
        $this->verifyParameter('cols', $cols);
        if (is_null($cols)) {
            return $cols;
        }
        return $cols;
    }

    /**
     * Verifies that the given form field dirname is valid
     * 
     * @param bool $dirname
     * @return bool|null
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    protected function filterDirname(bool $dirname = null): ?bool
    {
        $this->verifyParameter('dirname', $dirname);
        if (is_null($dirname) || $dirname === false) {
            return null;
        }
        return true;
    }

    /**
     * Verifies that the given form field disabled is valid
     * 
     * @param string $disabled
     * @return string|null
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    protected function filterDisabled(bool $disabled = null): ?bool
    {
        $this->verifyParameter('disabled', $disabled);
        if (is_null($disabled)) {
            return $disabled;
        }
        return $disabled;
    }

    /**
     * Verifies that the given form field form is valid
     * 
     * @param string $form
     * @return string|null
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    protected function filterForm(string $form = null): ?string
    {
        $this->verifyParameter('form', $form);
        if (is_null($form)) {
            return $form;
        }
        return $form;
    }

    /**
     * Verifies that the given form field parameter is valid
     * 
     * @param mixed $value
     * @return mixed
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    protected function filterFormAction($value)
    {
        $this->verifyParameter('formaction', $required);
        if (is_null($value)) {
            return $value;
        }
        return $value;
    }

    /**
     * Verifies that the given form field parameter is valid
     * 
     * @param mixed $value
     * @return mixed
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    protected function filterFormEnctype($value)
    {
        $this->verifyParameter('formenctype', $required);
        if (is_null($value)) {
            return $value;
        }
        return $value;
    }

    /**
     * Verifies that the given form field parameter is valid
     * 
     * @param mixed $value
     * @return mixed
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    protected function filterFormMethod($value)
    {
        $this->verifyParameter('formmethod', $required);
        if (is_null($value)) {
            return $value;
        }
        return $value;
    }

    /**
     * Verifies that the given form field parameter is valid
     * 
     * @param mixed $value
     * @return mixed
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    protected function filterFormTarget($value)
    {
        $this->verifyParameter('formtarget', $required);
        if (is_null($value)) {
            return $value;
        }
        return $value;
    }

    /**
     * Verifies that the given form field label is valid
     * 
     * @param string $label
     * @return string|null
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    protected function filterLabel(string $label = null): ?string
    {
        if (is_null($label)) {
            return $label;
        }
        return $label;
    }

    /**
     * Verifies that the given form field parameter is valid
     * 
     * @param mixed $value
     * @return mixed
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    protected function filterList($value)
    {
        $this->verifyParameter('list', $required);
        if (is_null($value)) {
            return $value;
        }
        return $value;
    }

    /**
     * Verifies that the given form field parameter is valid
     * 
     * @param mixed $value
     * @return mixed
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    protected function filterMax($value)
    {
        $this->verifyParameter('max', $required);
        if (is_null($value)) {
            return $value;
        }
        return $value;
    }

    /**
     * Verifies that the given form field maxlength is valid
     * 
     * @param string $maxlength
     * @return string|null
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    protected function filterMaxlength(int $maxlength = null): ?int
    {
        $this->verifyParameter('maxlength', $maxlength);
        if (is_null($maxlength)) {
            return $maxlength;
        }
        return $maxlength;
    }

    /**
     * Verifies that the given form field parameter is valid
     * 
     * @param mixed $value
     * @return mixed
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    protected function filterMin($value)
    {
        $this->verifyParameter('min', $required);
        if (is_null($value)) {
            return $value;
        }
        return $value;
    }

    /**
     * Verifies that the given form field parameter is valid
     * 
     * @param mixed $value
     * @return mixed
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    protected function filterMinLength($value)
    {
        $this->verifyParameter('minlength', $required);
        if (is_null($value)) {
            return $value;
        }
        return $value;
    }

    /**
     * Verifies that the given form field parameter is valid
     * 
     * @param mixed $value
     * @return mixed
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    protected function filterMultiple($value)
    {
        $this->verifyParameter('multiple', $required);
        if (is_null($value)) {
            return $value;
        }
        return $value;
    }

    /**
     * Verifies that the given form field name is valid
     * 
     * @param string $name
     * @return string|null
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    protected function filterName(string $name = null): ?string
    {
        $this->verifyParameter('name', $label);
        if (is_null($name)) {
            return $name;
        }
        return $name;
    }

    /**
     * Verifies given parameters to insure they are valid
     * for the given parameter type
     * 
     * @param string $type
     * @param mixed $parameter
     * @return void
     */
    protected function verifyParameter(string $type, $parameter): void
    {
        if (array_key_exists($type, self::$textarea_valid_parameters)) {
            $valid = self::$textarea_valid_parameters[$type];
            if (!$this->getResolver()->resolve($parameter, $valid)) {
                throw new \Oroboros\core\exception\InvalidArgumentException(
                        sprintf('Error encountered in [%1$s]. Provided parameter '
                            . '[%2$s] is not valid for component [%3$s].'
                            , get_class($this), $type, static::COMPONENT_KEY)
                );
            }
        }
    }

    /**
     * Verifies that the given form field parameter is valid
     * 
     * @param mixed $value
     * @return mixed
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    protected function filterPattern($value)
    {
        $this->verifyParameter('pattern', $required);
        if (is_null($value)) {
            return $value;
        }
        return $value;
    }

    /**
     * Verifies that the given form field placeholder is valid
     * 
     * @param string $placeholder
     * @return string|null
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    protected function filterPlaceholder(string $placeholder = null): ?string
    {
        $this->verifyParameter('placeholder', $placeholder);
        if (is_null($placeholder)) {
            return $placeholder;
        }
        return $placeholder;
    }

    /**
     * Verifies that the given form field readonly is valid
     * 
     * @param string $readonly
     * @return string|null
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    protected function filterReadonly(bool $readonly = null): ?bool
    {
        $this->verifyParameter('readonly', $readonly);
        if (is_null($readonly)) {
            return $readonly;
        }
        return $readonly;
    }

    /**
     * Verifies that the given form field required is valid
     * 
     * @param string $required
     * @return string|null
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    protected function filterRequired(bool $required = null): ?bool
    {
        $this->verifyParameter('required', $required);
        if (is_null($required)) {
            return $required;
        }
        return $required;
    }

    /**
     * Verifies that the given form field rows is valid
     * 
     * @param string $rows
     * @return string|null
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    protected function filterRows(int $rows = null): ?int
    {
        $this->verifyParameter('rows', $rows);
        if (is_null($rows)) {
            return $rows;
        }
        return $rows;
    }

    /**
     * Verifies that the given form field parameter is valid
     * 
     * @param mixed $value
     * @return mixed
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    protected function filterSize($value)
    {
        $this->verifyParameter('size', $required);
        if (is_null($value)) {
            return $value;
        }
        return $value;
    }

    /**
     * Verifies that the given form field parameter is valid
     * 
     * @param mixed $value
     * @return mixed
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    protected function filterSrc($value)
    {
        $this->verifyParameter('src', $required);
        if (is_null($value)) {
            return $value;
        }
        return $value;
    }

    /**
     * Verifies that the given form field parameter is valid
     * 
     * @param mixed $value
     * @return mixed
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    protected function filterStep($value)
    {
        $this->verifyParameter('step', $required);
        if (is_null($value)) {
            return $value;
        }
        return $value;
    }

    /**
     * Verifies that the given form field type is a valid html5 field type
     * 
     * @param string $type
     * @return string|null
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    protected function filterType(string $type = null): ?string
    {
        if (is_null($type)) {
            return $type;
        }
        if (!in_array($type, $this->declareValidFieldTypes(), true)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Provided field type '
                        . '[%2$s] is not valid. Valid field types are [%3$s]'
                        , get_class($this), $type, implode(', ', $this->declareValidFieldTypes()))
            );
        }
        return $type;
    }

    /**
     * Verifies that the given form field parameter is valid
     * 
     * @param mixed $value
     * @return mixed
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    protected function filterValidate($value)
    {
        $this->verifyParameter('validate', $required);
        if (is_null($value)) {
            return $value;
        }
        return $value;
    }

    /**
     * Verifies that the given form field value is valid
     * 
     * @param null|scalar $value
     * @return string|null
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    protected function filterValue($value = null)
    {
        $this->verifyParameter('value', $value);
        if (is_null($value)) {
            return $value;
        }
        if (!is_scalar($value)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Key [%2$s] '
                        . 'must be scalar or null. Received value of '
                        . 'type [%3$s]', get_class($this), 'value', gettype($value))
            );
        }
        return $value;
    }

    /**
     * Verifies that the given form field parameter is valid
     * 
     * @param mixed $value
     * @return mixed
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    protected function filterWidth($value)
    {
        $this->verifyParameter('width', $required);
        if (is_null($value)) {
            return $value;
        }
        return $value;
    }

    /**
     * Verifies that the given form field wrap is valid
     * 
     * @param string $wrap
     * @return string|null
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    protected function filterWrap(string $wrap = null): ?string
    {
        $this->verifyParameter('wrap', $wrap);
        if (is_null($wrap)) {
            return $wrap;
        }
        return $wrap;
    }

    /**
     * Override this method to expand or constrain
     * the valid field types for the given component.
     * 
     * The default behavior is to return a list of
     * all valid html5 input types.
     * 
     * This method's return value is used to validate
     * a given field type entry.
     * 
     * @return array
     */
    protected function declareValidFieldTypes(): array
    {
        return self::$valid_form_input_types;
    }

    /**
     * Returns the form parser valid for the form represented by the component
     * 
     * @param string $command
     * @return \Oroboros\core\interfaces\library\form\FormParserInterface
     */
    protected function getFormParser(string $command = null): \Oroboros\core\interfaces\library\form\FormParserInterface
    {
        if (is_null($command)) {
            $command = $this->getCommand();
        }
        $expected = \Oroboros\core\interfaces\library\form\FormParserInterface::class;
        $class = static::FORM_PARSER_CLASS;
        return $this->load('library', $class, $command, $this->getArguments(), $this->getFlags());
    }

    /**
     * Verifies that the form parser class declaration is valid
     * 
     * @return void
     * @throws \Oroboros\core\exception\core\InvalidClassException
     */
    private function verifyFormParserClass(): void
    {
        $expected = \Oroboros\core\interfaces\library\form\FormParserInterface::class;
        if (is_null(self::FORM_PARSER_CLASS)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. Expected class constant '
                        . '[%2$s] is not defined. Expected class or stub class name '
                        . 'resolving to an instance of [%3$s]. This class is not '
                        . 'useable in it\'s current state.'
                        , get_class($this), 'FORM_PARSER_CLASS', $expected)
            );
        }
        $class = $this->getFullClassName('component', static::FORM_PARSER_CLASS);
        if ($class === false || !in_array($expected, class_implements($class))) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. Class constant '
                        . '[%2$s] with value [%3$s] does not resolve to a '
                        . 'class implementing interface [%4$s]. This class is not '
                        . 'useable in it\'s current state.'
                        , get_class($this), 'FORM_PARSER_CLASS', static::FORM_PARSER_CLASS, $expected)
            );
        }
    }

    /**
     * Verifies that the form label class is valid
     * 
     * @return void
     * @throws \Oroboros\core\exception\core\InvalidClassException
     */
    private function verifyFormLabelClass(): void
    {
        $expected = \Oroboros\core\interfaces\library\component\ComponentInterface::class;
        if (is_null(self::FORM_FIELD_LABEL_CLASS)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. Expected class constant '
                        . '[%2$s] is not defined. Expected class or stub class name '
                        . 'resolving to an instance of [%3$s]. This class is not '
                        . 'useable in it\'s current state.'
                        , get_class($this), 'FORM_FIELD_LABEL_CLASS', $expected)
            );
        }
        $class = $this->getFullClassName('component', static::FORM_FIELD_LABEL_CLASS);
        if ($class === false || !in_array($expected, class_implements($class))) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. Class constant '
                        . '[%2$s] with value [%3$s] does not resolve to a '
                        . 'class implementing interface [%4$s]. This class is not '
                        . 'useable in it\'s current state.'
                        , get_class($this), 'FORM_FIELD_LABEL_CLASS', static::FORM_FIELD_LABEL_CLASS, $expected)
            );
        }
    }

    /**
     * Verifies that the form field default type declaration
     * 
     * @return void
     * @throws \Oroboros\core\exception\core\InvalidClassException
     */
    private function verifyFormFieldDefaultType(): void
    {
        $expected = $this->declareValidFieldTypes();
        if (is_null(self::FORM_FIELD_DEFAULT_TYPE)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. Expected class constant '
                        . '[%2$s] is not defined. Expected class or stub class name '
                        . 'resolving to one of [%3$s]. This class is not '
                        . 'useable in it\'s current state.'
                        , get_class($this), 'FORM_FIELD_DEFAULT_TYPE', implode(', ', $expected))
            );
        }
        if (!in_array(static::FORM_FIELD_DEFAULT_TYPE, $expected)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. Class constant '
                        . '[%2$s] with value [%3$s] does not resolve to '
                        . 'one of [%4$s]. This class is not '
                        . 'useable in it\'s current state.'
                        , get_class($this), 'FORM_FIELD_DEFAULT_TYPE'
                        , static::FORM_FIELD_DEFAULT_TYPE, implode(', ', $expected))
            );
        }
    }
}
