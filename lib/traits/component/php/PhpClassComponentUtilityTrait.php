<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\component\php;

/**
 * Php Class Component Utility Trait
 * Provides common methods to php components for validating and constructing
 * php constructs for classes, interfaces, and traits
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
trait PhpClassComponentUtilityTrait
{

    use PhpFunctionComponentUtilityTrait;

    /**
     * Formats given interface names to insure they are 
     * correctly declared for the template
     * 
     * @param type $interfaces
     * @return array
     */
    protected function filterInterfaces($interfaces)
    {
        if ($interfaces === false) {
            return $interfaces;
        }
        if (is_string($interfaces)) {
            $interfaces = [$interfaces];
        }

        return $interfaces;
    }

    /**
     * Preflights given constant values to insure they are well formed.
     * 
     * @param type $constants
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface|bool (false)
     */
    protected function filterConstants($constants)
    {
        if ($constants === false) {
            return $constants;
        }
        if (!is_iterable($constants)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Supplied context value for [%2$s] must be iterable.', get_class($this), 'constants')
            );
        }
        $invalid = [];
        foreach ($constants as $key => $constant) {
            if (!is_string($key)) {
                $invalid[] = $key;
            }
        }
        if (!empty($invalid)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Supplied context value for [%2$s] must have associative keys. Supplied set contained [%3$s] invalid numerical keys.', get_class($this), 'constants', count($invalid))
            );
        }
        $output = [];
        foreach ($constants as $key => $constant) {
            $component = $this->load('component', 'php\\ClassConstantComponent', $key, ['builder' => $this->getBuilder(), 'context' => $this->getConstantDefaultDefinition($key, $constant)]);
            $output[$key] = $component;
        }
        return $this->containerize($output);
    }

    /**
     * Preflights given class property values to insure they are well formed.
     * 
     * @param type $properties
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface|bool (false)
     */
    protected function filterProperties($properties)
    {
        if ($properties === false) {
            return $properties;
        }
        if (!is_iterable($properties)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Supplied context value for [%2$s] must be iterable.', get_class($this), 'properties')
            );
        }
        $invalid = [];
        foreach ($properties as $key => $property) {
            if (!is_string($key)) {
                $invalid[] = $key;
            }
        }
        if (!empty($invalid)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Supplied context value for [%2$s] must have associative keys. Supplied set contained [%3$s] invalid numerical keys.', get_class($this), 'properties', count($invalid))
            );
        }
        $output = [];
        foreach ($properties as $key => $property) {
            $component = $this->load('component', 'php\\ClassPropertyComponent', $key, ['builder' => $this->getBuilder(), 'context' => $property]);
            $output[$key] = $component;
        }
        return $this->containerize($output);
    }

    /**
     * Constructs a set of method components from the given arguments
     * and returns a container of constructed components
     * @param iterable $methods
     * @return bool|\Oroboros\core\interfaces\library\container\ContainerInterface
     * @throws \Oroboros\core\exception\InvalidArgumentException if the provided value is not iterable
     */
    protected function filterMethods($methods)
    {
        if ($methods === false) {
            return $methods;
        }
        if (!is_iterable($methods)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Provided method set declaration value must be iterable.', get_class($this))
            );
        }
        $output = [];
        foreach ($methods as $key => $method) {
            $output[$key] = $this->filterMethod($method);
        }
        return $this->containerize($output);
    }

    /**
     * Constructs a method component from the given arguments
     * @param iterable $method
     * @return \Oroboros\core\interfaces\library\component\ComponentInterface
     * @throws \Oroboros\core\exception\InvalidArgumentException if the provided value is not iterable
     */
    protected function filterMethod($method): \Oroboros\core\interfaces\library\component\ComponentInterface
    {
        if (!is_iterable($method)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Provided method declaration value must be iterable.', get_class($this))
            );
        }
        if (is_object($method) && $method instanceof \Oroboros\core\interfaces\library\container\ContainerInterface) {
            $method = $method->toArray();
        }
        if (!array_key_exists('name', $method)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Provided method declaration must provide a name.', get_class($this))
            );
        }
        $method = $this->preflightMethodArguments($method['name'], $method);
        $component = $this->load('component', $this->getMethodComponentName(), $method['name'], ['builder' => $this->getBuilder(), 'context' => $method]);
        return $component;
    }

    /**
     * Filters trait inclusion declarations for a class or trait
     * @param iterable $method
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     * @throws \Oroboros\core\exception\InvalidArgumentException if the provided value is not iterable or traits are malformed
     */
    protected function filterTraitDeclarations($traits)
    {
        if ($traits === false) {
            return $traits;
        }
        if (!is_iterable($traits)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Provided traits declaration value must be iterable.', get_class($this))
            );
        }
        if (is_object($traits) && $traits instanceof \Oroboros\core\interfaces\library\container\ContainerInterface) {
            $traits = $traits->toArray();
        }
        $result = [];
        foreach ($traits as $key => $trait) {
            if (in_array($trait, $result)) {
                // do not double include traits
                continue;
            }
            $this->preflightClassNameStructure($trait);
            $result[$key] = $trait;
        }
        return $this->containerize($result);
    }

    /**
     * Filters the visibility of a function name, constant, class property, etc
     * @param string $visibility
     * @return string
     * @throws \Oroboros\core\exception\InvalidArgumentException if the given value is not one of public, protected, or private
     */
    protected function filterVisibility($visibility): string
    {
        if (!is_string($visibility)) {
            $visibility = 'public';
        }
        $expected = ['public', 'protected', 'private'];
        if (!in_array($visibility, $expected)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Property visibility may only be one of [%2$s].', get_class($this), implode(', ', $expected))
            );
        }
        return $visibility;
    }

    /**
     * Returns the stub name of the method component to use for generating method components
     * @return string
     */
    protected function getMethodComponentName(): string
    {
        return 'php\\MethodComponent';
    }

    /**
     * Generates a semantically correct constant definer using defaults 
     * when passed a string or only partially formed definition,
     * and preflights keys for validity.
     * @param string $key
     * @param type $value
     * @return array
     */
    protected function getConstantDefaultDefinition(string $key, $value): array
    {
        $defaults = [
            'docblock' => false,
            'visibility' => 'public'
        ];
        $values = [];
        if (!is_iterable($value)) {
            $values['value'] = $value;
        } else {
            foreach ($value as $key => $prop) {
                $values[$key] = $prop;
            }
        }
        if (!array_key_exists('name', $values)) {
            $values['name'] = $key;
        }
        $values = array_merge($defaults, $values);
        $values['name'] = $this->filterConstantName($values['name']);
        $values['value'] = $this->filterConstantValue($values['value']);
        $values['visibility'] = $this->filterVisibility($values['visibility']);
        if (array_key_exists('docblock', $values)) {
            $values['docblock'] = $this->filterDocblock($values['docblock']);
        }
        return $values;
    }

    /**
     * Prepares an array of method data to be passed to the component as context keys
     * @param string $key
     * @param array $method
     * @return array
     * @throws \Oroboros\core\exception\InvalidArgumentException if any errors in method values are encountered during preflight
     */
    protected function preflightMethodDeclaration(string $key, array $method): array
    {
        $method = $this->preflightMethodVisibility($key, $method);
        $method = $this->preflightMethodName($key, $method);
        $method = $this->preflightMethodArguments($key, $method);
        $method = $this->preflightMethodReturn($key, $method);
        return $method;
    }

    /**
     * Adds a default visibility of public if no visibility is declared
     * @param string $key
     * @param array $method
     * @return array
     */
    protected function preflightMethodVisibility(string $key, array $method): array
    {
        if (!array_key_exists('visibility', $method)) {
            $method['visibility'] = 'public';
        }
        return $method;
    }

    /**
     * Insures a name key exists, and casts it to camelCase if it is not already.
     * @param string $key
     * @param array $method
     * @return array
     * @throws \Oroboros\core\exception\InvalidArgumentException if no name key exists.
     */
    protected function preflightMethodName(string $key, array $method): array
    {
        if (!array_key_exists('name', $method)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Supplied interface method [%2$s] must supply a name.', get_class($this), $key)
            );
        }
        $method['name'] = $this->camel_case($method['name']);
        return $method;
    }

    /**
     * Formats arguments to conform to the component and php valid language construct declarations
     * @param string $key
     * @param array $method
     * @return array
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    protected function preflightMethodArguments(string $key, array $method): array
    {
        if (array_key_exists('arguments', $method) && $method['arguments'] !== false && $method['arguments'] !== null) {
            if (!is_iterable($method['arguments'])) {
                throw new \Oroboros\core\exception\InvalidArgumentException(
                        sprintf('Error encountered in [%1$s]. Supplied interface method [%2$s] does not have a valid argument declaration. Value must be iterable, false, or null', get_class($this), $key)
                );
            }
            if (is_object($method['arguments']) && $method['arguments'] instanceof \Oroboros\core\interfaces\library\container\ContainerInterface) {
                $method['arguments'] = $method['arguments']->toArray();
            }
            foreach ($method['arguments'] as $key => $argument) {
                $this->preflightArgumentDeclaration($key, $argument);
                if (is_object($argument) && $argument instanceof \Oroboros\core\interfaces\library\container\ContainerInterface) {
                    $argument = $argument->toArray();
                }
                $method['arguments'][$key] = $this->preflightMethodArgument($key, $argument);
            }
        }
        return $method;
    }

    /**
     * Checks syntax and type compatibility on the argument key and values
     * @param type $key
     * @param type $argument
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    protected function preflightArgumentDeclaration($key, $argument): void
    {
        if (!is_string($key)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Supplied interface method argument key must be a string.', get_class($this))
            );
        }
        if (!is_iterable($argument)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Supplied interface method argument [%2$s] must be iterable.', get_class($this), $key)
            );
        }
        if (is_object($argument) && $argument instanceof \Oroboros\core\interfaces\library\container\ContainerInterface) {
            $argument = $argument->toArray();
        }
    }

    /**
     * Formats the method argument declaration for the method component template
     * @param string $key
     * @param array $argument
     * @return array
     */
    protected function preflightMethodArgument(string $key, array $argument): array
    {
        return $argument;
    }

    /**
     * Formats the method return typehint for the method component template
     * @param string $key
     * @param array $method
     * @return array
     */
    protected function preflightMethodReturn(string $key, array $method): array
    {
        if (!array_key_exists('return', $method)) {
            $method['return'] = false;
        }
        if ($method['return'] === false) {
            return $method;
        }
        if (is_string($method['return'])) {
            $method['return'] = [
                'nullable' => false,
                'value' => $method['return']
            ];
            if (strpos($method['return'], '?') === 0) {
                $method['return']['nullable'] = true;
                $method['return']['value'] = ltrim($method['return']['value'], '?');
            }
        }
        if ($method['return']['value'] === 'void') {
            $method['return']['nullable'] = false;
        }
        return $method;
    }

    protected function preflightClassNameStructure($class): void
    {
        if (!is_string($class)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Provided class, interface, or trait name to evalate must be a string.', get_class($this))
            );
        }
        if (strpos($class, '\\') === 0) {
            // Skip single initial backslash for globalization, 
            // because it is valid but breaks the below logic, 
            // which is otherwise sound.
            $class = substr($class, 1);
        }
        if (strpos($class, '\\') !== false) {
            $audit = explode('\\', $class);
        } else {
            $audit = [$class];
        }
        if (strpos($class, ' ') !== false) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Provided class, interface, or trait name [%2$s] cannot contain spaces.', get_class($this), $class)
            );
        }
        if (strpos($class, '-') !== false) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Provided class, interface, or trait name [%2$s] cannot contain hyphens.', get_class($this), $class)
            );
        }
        foreach ($audit as $segment) {
            if ($segment === '') {
                d($audit, $segment, $class);
                throw new \Oroboros\core\exception\InvalidArgumentException(
                        sprintf('Error encountered in [%1$s]. Provided class, interface, or trait name [%2$s] contains double-backslashes.', get_class($this), $class)
                );
            }
            if (preg_match('/^[a-zA-Z_\x80-\xff][a-zA-Z0-9_\x80-\xff]*$/', $segment) !== 1) {
                throw new \Oroboros\core\exception\InvalidArgumentException(
                        sprintf('Error encountered in [%1$s]. Provided class, interface, or trait name [%2$s] contains illegal characters.', get_class($this), $class)
                );
            }
        }
    }
}
