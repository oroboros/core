<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\component\php;

/**
 * Php Function Component Utility Trait
 * Provides common methods for handling generation of php functions, 
 * methods, and interface methods dynamically via components.
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
trait PhpFunctionComponentUtilityTrait
{

    /**
     * Checks a set of php expressions for syntax errors.
     * If they pass syntax validation, will return a container of expression components.
     * @param iterable|false $expressions
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     * @throws \Oroboros\core\exception\InvalidArgumentException if any provided expression does not validate correctly.
     */
    protected function filterExpressionSet($expressions)
    {
        if ($expressions === false) {
            return $expressions;
        }
        if (!is_iterable($expressions)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Provided expression set must be a iterable.')
            );
        }
        $result = [];
        foreach ($expressions as $key => $expression) {
            if (is_string($expression)) {
                $expression = [
                    'expression' => $expression
                ];
            }
            if (is_object($expression) && $expression instanceof \Oroboros\core\abstracts\library\component\php\AbstractExpressionComponent) {
                // already valid
                $result[$key] = $expression;
                continue;
            }
            if (!is_iterable($expression)) {
                throw new \Oroboros\core\exception\InvalidArgumentException(
                        sprintf('Error encountered in [%1$s]. Provided expression set for key [%2$s] must be a iterable. Received [%3$s].', get_class($this), $key, gettype($expression))
                );
            }
            $component = $this->load('component', 'php\\ExpressionComponent', $expression['expression'], ['builder' => $this->getBuilder(), 'context' => $expression]);
            $result[$key] = $component;
        }
        return $this->containerize($result);
    }

    /**
     * Checks a php expression for syntax errors
     * @param string $expression
     * @return string
     * @throws \Oroboros\core\exception\InvalidArgumentException if the provided value is not valid syntax
     */
    protected function filterExpression($expression): string
    {
        if (!is_string($expression)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Provided expression must be a string, received [%2$s].', get_class($this), gettype($expression))
            );
        }
        $expression = rtrim($expression, ';') . ';';
        $file = tmpfile();
        $path = stream_get_meta_data($file)['uri'];
        fwrite($file, $expression);
        $test_string = sprintf('php -l %1$s >/dev/null 2>&1; echo $?', $path);
        $result = exec($test_string);
        fclose($file);
        // Delete any relic file if it persists
        if (is_readable($path)) {
            unlink($path);
        }
        if ($result !== '0') {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Provided expression [%2$s] is not valid php syntax.', get_class($this), $expression)
            );
        }
        return rtrim($expression, ';');
    }

    /**
     * Constructs a docblock component from the given arguments
     * @param iterable $docblock
     * @return bool|\Oroboros\core\interfaces\library\component\ComponentInterface
     * @throws \Oroboros\core\exception\InvalidArgumentException if the provided value is not iterable
     */
    protected function filterDocblock($docblock)
    {
        if ($docblock === false) {
            return $docblock;
        }
        if (is_object($docblock) && $docblock instanceof \Oroboros\core\abstracts\library\component\php\AbstractDocblockComponent) {
            // Already a docblock, nothing to do
            return $docblock;
        }
        if (!is_iterable($docblock)) {
            d($docblock);
            exit;
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Provided docblock declaration value must be iterable.', get_class($this))
            );
        }
        $component = $this->load('component', 'php\\DocblockComponent', $this->getCommand(), ['builder' => $this->getBuilder(), 'context' => $docblock]);
        return $component;
    }

    /**
     * Constructs a function component from the given arguments
     * @param iterable $function
     * @return bool|\Oroboros\core\interfaces\library\component\ComponentInterface
     * @throws \Oroboros\core\exception\InvalidArgumentException if the provided value is not iterable
     */
    protected function filterFunction($function)
    {
        if ($function === false) {
            return $function;
        }
        if (!is_iterable($function)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Provided function declaration value must be iterable.', get_class($this))
            );
        }
        $component = $this->load('component', 'php\\FunctionComponent', $this->getCommand(), ['builder' => $this->getBuilder()]);
        return $component;
    }

    /**
     * Filters the value of a constant to insure it is semantically correct php syntax
     * @param mixed $value
     * @return mixed
     * @throws \Oroboros\core\exception\InvalidArgumentException if the provided value is valid for a constant
     */
    protected function filterConstantValue($value)
    {
        if (is_array($value)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Constant values cannot be arrays.', get_class($this))
            );
        }
        if (is_object($value)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Constant values cannot be objects.', get_class($this))
            );
        }
        return $value;
    }

    /**
     * Filters the value of a constant name to insure it is semantically correct php syntax.
     * If it is lower case, it will be cast to upper case.
     * If it is canonical, hyphens will be cast to underscores.
     * Existence of spaces will raise an exception.
     * Leading numbers or underscores will raise an exception. Leading numbers 
     * are not valid php.
     * Leading underscores are valid php, but are explicitly advised against 
     * due to potential conflict with future magic constants.
     * 
     * @param mixed $name
     * @return string
     * @throws \Oroboros\core\exception\InvalidArgumentException if the provided value is valid for a constant
     */
    protected function filterConstantName($name): string
    {
        $formatted = $name;
        if (!is_string($name)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Constant names must be a string.', get_class($this))
            );
        }
        if (strpos($name, ' ') !== false) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Constant names cannot contain spaces (evaluating [%2$s]).', get_class($this), $name)
            );
        }
        if (strpos($name, PHP_EOL) !== false) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Constant names cannot contain line breaks (evaluating [%2$s]).', get_class($this), $name)
            );
        }
        if (strpos($name, '-') !== false) {
            $formatted = str_replace('-', '_', $formatted);
        }
        $formatted = strtoupper($formatted);
        if (strpos($formatted, '_') === 0 or strpos($formatted, '_') === strlen($formatted)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Constant names cannot be prefixed or suffixed with underscores, '
                        . 'as advised by the PHP manual as potentially not future portable (evaluating [%2$s]).', get_class($this), $name)
            );
        }
        if (is_numeric(substr($formatted, 0, 1))) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Constant names cannot be prefixed '
                        . 'by an integer (evaluating [%2$s]).', get_class($this), $name)
            );
        }
        if (strpos($formatted, '_') === 0 or strpos($formatted, '_') === strlen($formatted)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Constant names cannot be prefixed or suffixed with underscores, '
                        . 'as advised by the PHP manual as potentially not future portable (evaluating [%2$s]).', get_class($this), $name)
            );
        }
        return $formatted;
    }
}
