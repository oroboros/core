<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\component;

/**
 * Component Context Filter Utility
 * Adds functionality to filter context to components.
 * 
 * This is not behavior of the underlying base level component abstraction,
 * but it is commonly used by numerous extensions of that.
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
trait ComponentContextFilterUtility
{

    /**
     * Override  this function to perform internal transformations 
     * on context added to the component. This method will be called 
     * from the addContext method prior to setting the value into 
     * the context container to pass to the template. The return value
     * of this method will be the value used for the given key.
     * 
     * @note If a filter returns a component or a container,
     *       it will be returned directly.
     *       Arrays will be assumed to need component transformation.
     *       If you are encountering an error for no known valid component
     *       on a custom key, return a container or pre-package it as a component
     *       in your filter method.
     * 
     * @param string $key
     * @param type $value
     * @return mixed
     */
    protected function filterContext(string $key, $value)
    {
        $filters = $this->declareContextFilters();
        $valid = array_keys($filters);
        // Do not filter keys not belonging to this component
        if (!in_array($key, $valid)) {
            return $value;
        }
        if (array_key_exists($key, $filters) && method_exists($this, $filters[$key])) {
            $method = $filters[$key];
            $this->getLogger()->debug('[type][scope][class] Filtering component context for key [key] via method [method].', [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
                'key' => $key,
                'method' => $method,
            ]);
            $value = $this->$method($value);
        }
        return $value;
    }

    /**
     * Override this method to declare filters for given context keys.
     * This should return an associative array, where the key is the context key,
     * and the value is a function to pass the given context value into.
     * The given function must accept a single mixed argument,
     * and return the formatted value.
     * The function must also be either protected or public.
     * @return array
     */
    protected function declareContextFilters(): array
    {
        return [];
    }
}
