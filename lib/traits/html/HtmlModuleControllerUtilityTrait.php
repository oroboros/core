<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Oroboros\core\traits\html;

/**
 * Html Module Controller Utility Trait
 *
 * @author Brian Dayhoff
 */
trait HtmlModuleControllerUtilityTrait
{

    use \Oroboros\core\traits\html\HtmlControllerCommonUtilityTrait;
    use \Oroboros\core\traits\pluggable\ModuleControllerTrait;

    private $sections = null;
    private $content = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeSections();
        $this->initializeContent();
    }

    protected function hasSection(string $section): bool
    {
        return $this->sections->has($section);
    }

    protected function getSection(string $section)
    {
        if (!$this->hasSection($section)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Provided section [%2$s] does not exist. Valid sections are [%3$s]', get_class($this), $section, implode(', ', array_keys($this->sections->toArray()))));
        }
        return $this->sections[$section];
    }

    protected function hasSectionContent(string $section): bool
    {
        return $this->content->has($section);
    }

    protected function addPageMeta(string $section)
    {
        $this->getLogger()->debug('[type][scope][class] Adding page metadata for section [section].', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'section' => $section,
        ]);
        $content = $this->getSectionPageContent($section);
        $this->addCharset($content['document']['charset']);
        $this->addLanguage($content['document']['lang']);
        $this->addTitle($content['head']['title']);
        $this->addAuthor($content['head']['author']);
        $this->addDescription($content['head']['description']);
        $this->addPageId($content['body']['id']);
        $this->addPageClass($content['body']['class']);
    }

    protected function addPageAssets(string $section)
    {
        $this->getLogger()->debug('[type][scope][class] Adding page assets for section [section].', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'section' => $section,
        ]);
        $content = $this->getSectionPageContent($section);
        foreach ($content['scripts'] as $section => $scripts) {
            foreach ($scripts as $key => $script) {
                $this->addScript($section, $key, $script);
            }
        }
        foreach ($content['styles'] as $key => $style) {
            $this->addStylesheet('stylesheet', $key, $style);
        }
        foreach ($content['fonts'] as $key => $font) {
            $this->addFont('font', $key, $font);
        }
    }

    private function initializeSections()
    {
        if (is_null($this->sections)) {
            $sections = $this->load('library', 'parser\\JsonParser', $this->getModule()->config() . 'sections.json')->fetch();
            $output = [];
            foreach ($sections as $section => $details) {
                $output[$section] = $this->verifySection($section, $details);
            }
            $this->sections = $this->containerize($output);
        }
    }

    private function initializeContent()
    {
        if (is_null($this->content)) {
            $content = $this->load('library', 'parser\\JsonParser', $this->getModule()->config() . 'content.json')->fetch()->toArray();
            $details = $content;
            unset($details['sections']);
            $content = $this->containerize($content['sections']);
            $details = $this->containerize($details);
            foreach ($content as $section => $existing) {
                if (is_array($existing)) {
                    $existing = $this->containerize($existing);
                }
                $content[$section] = $this->updateChildren($existing, $this->containerize($details));
            }
            $this->content = $content;
        }
    }

    private function verifySection(string $section, array $details)
    {
        return $details;
    }

    private function updateChildren(\Oroboros\core\interfaces\library\container\ContainerInterface $subject, \Oroboros\core\interfaces\library\container\ContainerInterface $replacements): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        foreach ($replacements as $key => $value) {
            if (!$subject->has($key)) {
                $subject[$key] = $this->containerize([]);
                if (is_array($value)) {
                    $subject[$key] = $this->updateChildren($subject[$key], $this->containerize($value));
                    continue;
                }
                $subject[$key] = $value;
                continue;
            }
            if (is_array($subject[$key])) {
                $subject[$key] = $this->containerize($subject[$key]);
            }
            if (is_iterable($subject[$key]) && $replacements->has($key)) {
                $subject[$key] = $this->updateChildren($subject[$key], $this->containerize($replacements[$key]));
                continue;
            }
        }
        return $subject;
    }

    private function getSectionPageContent(string $section): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        if (!$this->hasSectionContent($section)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Provided section [%2$s] does not exist. Valid sections are [%3$s]', get_class($this), $section, implode(', ', array_keys($this->content->toArray()))));
        }
        return $this->containerize($this->content[$section]);
    }
}
