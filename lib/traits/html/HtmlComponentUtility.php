<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\html;

/**
 * Html Component Utility
 * Common method for loading components by slug name
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
trait HtmlComponentUtility
{

    /**
     * Returns a component instance representing the given name
     * @return \Oroboros\core\interfaces\library\component\ComponentInterface
     */
    protected function getComponent(string $name, string $key, array $details = [], array $flags = []): \Oroboros\core\interfaces\library\component\ComponentInterface
    {
        $details['context'] = $details;
        $details['builder'] = $this->getResponseDirector()->getWorker('component');
        $details['request'] = $this->getArgument('request');
        $details['route'] = $this->getArgument('route');
        $details['response-director'] = $this->getResponseDirector();
        $this->getLogger()->debug('[type][scope][class] Loading component [name] with identifier [key].', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'name' => $name,
            'key' => $key,
        ]);
        if ($this->getResponseDirector()->getWorker('component')->getComponentDefinitions()->has('html') && $this->getResponseDirector()->getWorker('component')->getComponentDefinitions()['html']->has($name)) {
            // Loads components with canonicalized identifiers
            $definer = $this->getResponseDirector()->getWorker('component')->getComponentDefinitions()['html'][$name];
            $details['component'] = $name;
            return $this->load('component', $definer['object'], $key, $details, $flags);
        }
        return $this->load('component', $name, $key, $details, $flags);
    }
}
