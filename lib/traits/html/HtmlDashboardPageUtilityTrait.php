<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\html;

/**
 * Html Dashboard Page Utility Trait
 * Provides logic for login dashboard controllers
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
trait HtmlDashboardPageUtilityTrait
{

    /**
     * Bounces anyone who is not logged in, is ghostlisted,
     * or does not have the dashboard view privilege
     * 
     * @return bool
     * @throws \Oroboros\core\exception\auth\AuthenticationException
     *         If any authentication criteria are invalid
     */
    public function authenticate(string $method, \Oroboros\core\interfaces\library\user\UserInterface $user): bool
    {
        if ($this::user()->isGhost()) {
            // attacking hacker or bot
            throw new \Oroboros\core\exception\auth\AuthenticationException('The requested page does not exist on this server.', 404);
        }
        if (!$this::user()->isLoggedIn()) {
            // not logged in
            $this->redirect('/login/', 302);
            return true;
            throw new \Oroboros\core\exception\auth\AuthenticationException('You must login to view this page.', 401);
        }
        if (!$this::user()->can('http.dashboard.view')) {
            // dashboard revoked
            throw new \Oroboros\core\exception\auth\AuthenticationException('You require additional access rights to view this page.', 403);
        }
        return parent::authenticate($method, $user);
    }

    /**
     * Returns the public error controller for non-logged in users, and the
     * dashboard error controller for all other cases.
     * 
     * @return \Oroboros\core\interfaces\controller\ErrorControllerInterface
     */
    public function getErrorController(): \Oroboros\core\interfaces\controller\ErrorControllerInterface
    {
        if (!$this::user()->isLoggedIn()) {
            return $this->getPublicErrorController();
        }
        return parent::getErrorController();
    }

    /**
     * Verifies that a dashboard viewer is authorized to view the page.
     * 
     * This will return `false` if any of the below apply:
     * - User is blacklisted
     * - User is ghostlisted
     * - User is not logged in
     * - User lacks the dashboard view permission
     * 
     * If this returns `false`, dashboard controller methods
     * should immediately return themselves and take no further action.
     * 
     * If this returns `true`, it is safe to display the dashboard
     * content to the viewer.
     * 
     * @return bool
     */
    protected function verifyRegistered(): bool
    {
        if ($this::user()->isBlacklisted() || $this::user()->isGhost() || !$this::user()->isLoggedIn() || !$this::user()->can('http.dashboard.view')) {
            // Nope.
            return false;
        }
        // Sure.
        return true;
    }

    /**
     * Returns the public facing error controller for cases when the user
     * is not logged in.
     * 
     * @return \Oroboros\core\interfaces\controller\ControllerInterface
     */
    protected function getPublicErrorController(): \Oroboros\core\interfaces\controller\ControllerInterface
    {
        $class = static::FRONTEND_ERROR_CONTROLLER;
        $controller = $this->load('controller', $class, $this->getCommand(), $this->getArguments(), $this->getFlags());
        return $controller;
    }
}
