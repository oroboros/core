<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\html;

/**
 * Html Error Controller Common Utility Trait
 * A set of common utilities for html error controllers
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
trait HtmlErrorControllerCommonUtilityTrait
{

    use HtmlControllerCommonUtilityTrait;
    use DefinerPageContentUtility;

    private static $error_defaults = null;

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
//        $this->initializeErrorPageDefinitions();
        $this->initializeScripts();
        $this->initializeStyles();
        $this->initializeFonts();
        $this->initializeLayout();
    }

    public function error(int $code = null, string $message = null, \Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\ErrorControllerInterface
    {
        if (is_null($code)) {
            $code = static::DEFAULT_ERROR_CODE;
        }
        $method = 'error' . $code;
        if (!method_exists($this, $method)) {
            $method = 'error' . static::DEFAULT_ERROR_CODE;
        }
        if (is_null($arguments)) {
            $arguments = $this->containerize();
        }
        if (is_null($flags)) {
            $flags = $this->containerize($this->getFlags());
        }
        if ($this::app()->debugEnabled()) {
            $arguments['controller'] = get_class($this);
            $arguments['method'] = __FUNCTION__;
            $trace = debug_backtrace(1);
            $reference = $trace[1];
            $arguments['referer'] = $reference;
            $arguments['trace'] = $trace;
        }
        return $this->$method($message, $this->buildErrorArguments($code, $message, $arguments, $flags), $flags);
    }

    protected function error401(string $message = null, \Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\ErrorControllerInterface
    {
        $content = [];
        $this->setupErrorLayout(401);
        $this->addStatusCode(401);
        if ($this::app()->debugEnabled() && $arguments->has('debug')) {
            $error_debug = $this->getComponent('error-info', 'error-info', $arguments['debug']);
            $this->addSectionContent('error', $this->containerize(['error' => $error_debug]));
        }
        $this->getLogger()->debug('[type][scope][class] A controller error of type [error-type] was called with message [error-message].', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'error-type' => 401,
            'error-message' => $message,
        ]);
        return $this->render('html', 'error/401', $content);
    }

    protected function error403(string $message = null, \Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\ErrorControllerInterface
    {
        $content = [];
        $this->setupErrorLayout(403);
        $this->addStatusCode(403);
        if ($this::app()->debugEnabled() && $arguments->has('debug')) {
            $error_debug = $this->getComponent('error-info', 'error-info', $arguments['debug']);
            $this->addSectionContent('error', $this->containerize(['error' => $error_debug]));
        }
        $this->getLogger()->debug('[type][scope][class] A controller error of type [error-type] was called with message [error-message].', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'error-type' => 403,
            'error-message' => $message,
        ]);
        return $this->render('html', 'error/403', $content);
    }

    protected function error404(string $message = null, \Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\ErrorControllerInterface
    {
        $content = [];
        $this->setupErrorLayout(404);
        $this->addStatusCode(404);
        if ($this::app()->debugEnabled() && $arguments->has('debug')) {
            $error_debug = $this->getComponent('error-info', 'error-info', $arguments['debug']);
            $this->addSectionContent('error', $this->containerize(['error' => $error_debug]));
        }
        $this->getLogger()->debug('[type][scope][class] A controller error of type [error-type] was called with message [error-message].', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'error-type' => 404,
            'error-message' => $message,
        ]);
        return $this->render('html', 'error/404', $content);
    }

    protected function error500(string $message = null, \Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\ErrorControllerInterface
    {
        $content = [];
        $this->setupErrorLayout(500);
        $this->addStatusCode(500);
        if ($this::app()->debugEnabled() && $arguments->has('debug')) {
            $error_debug = $this->getComponent('error-info', 'error-info', $arguments['debug']);
            $this->addSectionContent('error', $this->containerize(['error' => $error_debug]));
        }
        $this->getLogger()->debug('[type][scope][class] A controller error of type [error-type] was called with message [error-message].', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'error-type' => 500,
            'error-message' => $message,
        ]);
        return $this->render('html', 'error/500', $content);
    }

    protected function error501(string $message = null, \Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\ErrorControllerInterface
    {
        $content = [];
        $this->setupErrorLayout(501);
        $this->addStatusCode(501);
        if ($this::app()->debugEnabled() && $arguments->has('debug')) {
            $error_debug = $this->getComponent('error-info', 'error-info', $arguments['debug']);
            $this->addSectionContent('error', $this->containerize(['error' => $error_debug]));
        }
        $this->getLogger()->debug('[type][scope][class] A controller error of type [error-type] was called with message [error-message].', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'error-type' => 501,
            'error-message' => $message,
        ]);
        return $this->render('html', 'error/500', $content);
    }

    protected function buildErrorArguments(int $code = null, string $message = null, \Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $output = [];
        if ($this::app()->debugEnabled()) {
            $output['debug']['error'] = $this->getErrorDebugContent($code, $message, $arguments, $flags);
        }
        return $this->containerize($output);
    }

    protected function getErrorDebugContent(int $code = null, string $message = null, \Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $this->getLogger()->debug('[type][scope][class] Retrieving error debug information for code [error-code] and message [error-message].', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'error-code' => $code,
            'error-message' => $message,
        ]);
        $debug = [];
        if (is_null($arguments)) {
            $arguments = $this->containerize();
        }
        if (is_null($flags)) {
            $flags = $this->containerize($this->getFlags());
        }
        $debug['arguments'] = [];
        foreach ($arguments as $key => $value) {
            $debug['arguments'][$key] = $value;
        }
        $debug['arguments'] = $arguments;
        if (!is_null($code)) {
            $debug['code'] = $code;
        }
        if (!is_null($message)) {
            $debug['message'] = $message;
        }
        if (!is_null($flags)) {
            $debug['flags'] = $flags;
        }
        if ($arguments->has('referer')) {
            $debug['file'] = $arguments['referer']['file'];
            $debug['line'] = $arguments['referer']['line'];
            if (array_key_exists('class', $arguments['referer'])) {
                $debug['referer']['class'] = $arguments['referer']['class'];
                $debug['referer']['method'] = $arguments['referer']['function'];
                $debug['referer']['type'] = $arguments['referer']['type'];
                if (array_key_exists('args', $arguments['referer'])) {
                    $debug['referer']['args'] = $arguments['referer']['args'];
                }
            }
        }
        if ($arguments->has('trace')) {
            $debug['trace'] = $arguments['trace'];
        }
        $debug['controller'] = get_class($this);
        $debug['scope'] = static::CLASS_SCOPE;
        return $this->containerize($debug);
    }

    private function setupErrorLayout(int $code): void
    {
        $this->setupPage('error', (string) $code);
    }

    private function getErrorDefinition(int $code): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        if (!self::$error_defaults->has((string) $code)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'No definition exists for supplied error code [%2$s]', get_class($this), $code));
        }
        return $this->containerize(self::$error_defaults[(string) $code]);
    }

    private function initializeErrorPageDefinitions(): void
    {
        if (is_null(self::$error_defaults)) {
            $output_type = 'html';
            $file = $this->getJsonPageConfigFile();
            if (!is_readable($file)) {
                // Error Page Config must exist and be readable
                throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. '
                            . 'Expected configuration file [%2$s] does not exist or '
                            . 'is not readable.', get_class($this), $file));
            }
            $data = $this->load('library', 'parser\\JsonParser', $file)->fetch();
            if (!$data->has($output_type)) {
                // Error Page Config must have an html section
                throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. '
                            . 'Expected configuration key [%2$s] does not exist in '
                            . 'error configuration file [%3$s].', get_class($this), $output_type, $file));
            }
            self::$error_defaults = $this->containerize($data[$output_type]);
        }
    }
}
