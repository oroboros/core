<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\html;

/**
 * Definer Page Content Utility
 * Provides methods to a controller for parsing page output details from a definer object.
 * This allows output to be configured from whatever source the relevant definer uses,
 * without the controller needing to be directly coupled with the data aggregation.
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
trait DefinerPageContentUtility
{

    /**
     * Represents the parsed page data
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $content = null;

    /**
     * Fetches the definer designated to handle the current page content payload
     * 
     * @param string|null $section
     * @param string|null $subsection
     * @param array|null $arguments
     * @return \Oroboros\core\interfaces\library\content\ContentDefinerInterface
     */
    protected function getDefiner(string $section = null, string $subsection = null, array $arguments = null): \Oroboros\core\interfaces\library\content\ContentDefinerInterface
    {
        if (is_null($arguments)) {
            $arguments = [];
        }
        $section = $this->brutecase(str_replace('-', ' ', $section));
        $path = trim(sprintf('content\\%1$s\\%2$s', static::CLASS_SCOPE, sprintf('%1$sContentDefiner', ucfirst($section))), '\\');
        $definer = $this->load('library', $path, $subsection, $this->getArguments(), $this->getFlags());
        return $definer;
    }

    /**
     * Fetches the definer designated to handle the specified menu
     * 
     * @param string $section
     * @param string $subsection
     * @param array $arguments
     * @return \Oroboros\core\interfaces\library\menu\MenuDefinerInterface
     */
    protected function getMenuDefiner(string $section = null, string $subsection = null, array $arguments = null): \Oroboros\core\interfaces\library\menu\MenuDefinerInterface
    {
        if (is_null($arguments)) {
            $arguments = [];
        }
        $section = $this->brutecase(str_replace('-', ' ', $section));
        $path = trim(sprintf('menu\\%1$s\\%2$s', static::CLASS_SCOPE, sprintf('%1$sDefiner', ucfirst($section))), '\\');
        $definer = $this->load('library', $path, $subsection, $this->getArguments(), $this->getFlags());
        return $definer;
    }

    /**
     * Sets up page based on the page and subsection definition(s) provided
     * @param string $page
     * @return void
     */
    protected function setupPage(string $page, string $subsection = null, array $details = null): void
    {
        $this->getLogger()->debug('[type][scope][class] Creating page setup definition for page [page].', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'page' => $page,
        ]);
        $definer = $this->getDefiner($page, $subsection, $details);
        $this->content = $this->packageContentDefiner($definer, $subsection, $details);
        $this->addCharset($this->content['document']['charset']);
        $this->addLanguage($this->content['document']['lang']);
        $this->setupDocumentHead($page, $subsection);
        $this->setupDocumentBody($page, $subsection);
        $this->setupDocumentScripts($page, $subsection);
        $this->setupDocumentStyles($page, $subsection);
        $this->setupDocumentFonts($page, $subsection);
        $this->setupDocumentMedia($page, $subsection);
    }

    /**
     * Packages page details for the document head output
     * @param string $page
     * @return void
     */
    protected function setupDocumentHead(string $page, string $subsection = null): void
    {
        $this->getLogger()->debug('[type][scope][class] Creating document head definition for page [page].', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'page' => $page,
        ]);
        $content = $this->content['head'];
        if (!is_null($subsection) && $content->has($subsection)) {
            $content = $content[$subsection];
        } elseif (!is_null($subsection) && $content->has((int) $subsection)) {
            $content = $content[(int) $subsection];
        }
        if ($content->has('title')) {
            $this->addTitle($content['title']);
        }
        if ($content->has('author')) {
            $this->addAuthor($content['author']);
        }
        if ($content->has('description')) {
            $this->addDescription($content['description']);
        }
    }

    /**
     * Initializes page scripts for output
     * @param string $page
     * @return void
     */
    protected function setupDocumentScripts(string $page, string $subsection = null): void
    {
        $this->getLogger()->debug('[type][scope][class] Creating page script definition for page [page].', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'page' => $page,
        ]);
        $content = $this->content['scripts'];
        if (!is_null($subsection) && $content->has($subsection)) {
            $content = $content[$subsection];
        } elseif (!is_null($subsection) && $content->has((int) $subsection)) {
            $content = $content[(int) $subsection];
        }
        foreach ($content as $section => $scripts) {
            foreach ($scripts as $key => $script) {
                $this->addScript($section, $script, $script);
            }
        }
    }

    /**
     * Initializes page stylesheets for output
     * @param string $page
     * @return void
     */
    protected function setupDocumentStyles(string $page, string $subsection = null): void
    {
        $this->getLogger()->debug('[type][scope][class] Creating page stylesheet definition for page [page].', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'page' => $page,
        ]);
        $content = $this->content['styles'];
        if (!is_null($subsection) && $content->has($subsection)) {
            $content = $content[$subsection];
        } elseif (!is_null($subsection) && $content->has((int) $subsection)) {
            $content = $content[(int) $subsection];
        }
        foreach ($content as $key => $style) {
            $this->addStylesheet('stylesheet', $style, $style);
        }
    }

    /**
     * Initializes page fonts for output
     * @param string $page
     * @return void
     */
    protected function setupDocumentFonts(string $page, string $subsection = null): void
    {
        $this->getLogger()->debug('[type][scope][class] Creating page font definition for page [page].', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'page' => $page,
        ]);
        $content = $this->content['fonts'];
        if (!is_null($subsection) && $content->has($subsection)) {
            $content = $content[$subsection];
        } elseif (!is_null($subsection) && $content->has((int) $subsection)) {
            $content = $content[(int) $subsection];
        }
        foreach ($content as $key => $font) {
            $this->addFont('font', $font, $font);
        }
    }

    /**
     * Initializes page media for output
     * @param string $page
     * @return void
     */
    protected function setupDocumentMedia(string $page, string $subsection = null): void
    {
        $this->getLogger()->debug('[type][scope][class] Creating page media definition for page [page].', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'page' => $page,
        ]);
        $content = $this->content['media'];
        if (!is_null($subsection) && $content->has($subsection)) {
            $content = $content[$subsection];
        } elseif (!is_null($subsection) && $content->has((int) $subsection)) {
            $content = $content[(int) $subsection];
        }
    }

    /**
     * Initializes callbacks uris to send to the frontend for ajax calls
     * @param string $page
     * @return void
     */
    protected function setupDocumentCallbacks(string $page, string $subsection = null): void
    {
        $this->getLogger()->debug('[type][scope][class] Creating page callback definition for page [page].', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'page' => $page,
        ]);
        $content = $this->content['callbacks'];
        if (!is_null($subsection) && $content->has($subsection)) {
            $content = $content[$subsection];
        } elseif (!is_null($subsection) && $content->has((int) $subsection)) {
            $content = $content[(int) $subsection];
        }
    }

    /**
     * Initializes data that is used to set up the document body
     * @param string $page
     * @return void
     */
    protected function setupDocumentBody(string $page, string $subsection = null): void
    {
        $this->getLogger()->debug('[type][scope][class] Creating page body definition for page [page].', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'page' => $page,
        ]);
        $content = $this->content['body'];
        if (!is_null($subsection) && $content->has($subsection)) {
            $content = $content[$subsection];
        } elseif (!is_null($subsection) && $content->has((int) $subsection)) {
            $content = $content[(int) $subsection];
        }
        if ($content->has('layout')) {
            $this->initializeLayout(static::CLASS_SCOPE . '\\' . $content['layout']);
        }
        if ($content->has('id')) {
            $this->addPageId($content['id']);
        }
        if ($content->has('class')) {
            $this->addPageClass($content['class']);
        }
        $this->setupDocumentContent($page, $subsection);
    }

    /**
     * Initializes content to place in the document body to send to the view
     * @param string $page
     * @return void
     */
    protected function setupDocumentContent(string $page, string $subsection = null): void
    {
        $this->getLogger()->debug('[type][scope][class] Creating page content definition for page [page].', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'page' => $page,
        ]);
        $body = $this->content['body'];
        if (!is_null($subsection) && $body->has($subsection)) {
            $body = $body[$subsection];
        } elseif (!is_null($subsection) && $body->has((int) $subsection)) {
            $body = $body[(int) $subsection];
        }
        $body = $body['content'];
        foreach ($body as $section => $section_context) {
            if (!$this->getLayout()->hasSection($section)) {
                continue;
            }
            $this->addSectionContent($section, $section_context);
        }
    }

    /**
     * Adds content to the layout for matching section keys
     * @param string $page
     * @param array $content
     * @return void
     */
    protected function addSectionContent(string $section, \Oroboros\core\interfaces\library\container\ContainerInterface $content): void
    {
        $this->getLogger()->debug('[type][scope][class] Adding section content for section [section].', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'page' => $section,
        ]);
        foreach ($content as $key => $value) {
            if ($this->getLayout()->hasSection($section) && $key !== 'content' && $this->getLayout()->checkContext($section, $key)) {
                $this->getLayout()->setContext($section, $key, $value);
                continue;
            }
            $this->addContent($section, $key, $this->recurseContent($key, $value));
        }
    }

    /**
     * Packages the supplied definer to make it compile ready
     * 
     * @param \Oroboros\core\interfaces\library\content\ContentDefinerInterface $definer
     * @param array|null $details
     * @return \Oroboros\core\interfaces\library\content\ContentDefinerInterface
     */
    private function packageContentDefiner(\Oroboros\core\interfaces\library\content\ContentDefinerInterface $definer, string $subsection = null, array $details = null): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $app = $this::app()->config()['app'];
        if (is_null($details)) {
            $details = [];
        }
        foreach ($definer->getRequired() as $key => $value) {
            if ($key === 'subsection') {
                $definer->setRequiredKey($key, $subsection);
                continue;
            }
            if ($this->app()->environment()->get('application')->has($key) && is_string($this->app()->environment()->get('application')->get($key))) {
                $definer->setRequiredKey($key, $this->app()->environment()->get('application')->get($key));
                continue;
            }
            if ($this->checkDefinerKeyMenu($key, $value)) {
                $definer->setRequiredKey($key, $this->getMenuDefiner($key, $details));
                continue;
            }
            if (array_key_exists($key, $details)) {
                $definer->setRequiredKey($key, $details[$key]);
                continue;
            }
            if ($app->has($key)) {
                $definer->setRequiredKey($key, $app[$key]);
            }
        }
        foreach ($definer->getOptional() as $key => $value) {
            if ($key === 'subsection') {
                $definer->setOptionalKey($key, $subsection);
                continue;
            }
            if ($this->app()->environment()->get('application')->has($key) && is_string($this->app()->environment()->get('application')->get($key))) {
                $definer->setOptionalKey($key, $this->app()->environment()->get('application')->get($key));
                continue;
            }
            if ($this->checkDefinerKeyMenu($key, $value)) {
                $definer->setOptionalKey($key, $this->getMenuDefiner($key, $details));
                continue;
            }
            if (array_key_exists($key, $details)) {
                $definer->setOptionalKey($key, $details[$key]);
                continue;
            }
            if ($app->has($key)) {
                $definer->setOptionalKey($key, $app[$key]);
            }
        }
        if (!$definer->canCompile()) {
            foreach ($definer->getUnsatisfiedRequiredKeys() as $key => $format) {
                try {
                    $menu_definer = $this->getMenuDefiner($key, $subsection);
                    $menu_data = $menu_definer->compile()->fetch();
                    $definer->setRequiredKey($key, $menu_data);
                    $menu_data = $this->filterModuleMenuAdditions($key, $menu_data);
                } catch (\Oroboros\core\exception\ErrorException $e) {
                    // Expected
                    continue;
                }
            }
        }
        if (!$definer->canCompile()) {
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. Provided content '
                        . 'definer [%2$s] cannot compile with the provided details. '
                        . 'The following required keys are unsatisfied: [%3$s].'
                        , get_class($this), get_class($definer)
                        , implode(', ', array_keys($definer->getUnsatisfiedRequiredKeys())))
            );
        }
        $definer->compile();
        return $definer->fetch();
    }

    /**
     * Returns a boolean designation as to whether a given
     * key represents an expected menu definer
     * 
     * @param string $key
     * @param type $details
     * @return bool
     */
    private function checkDefinerKeyMenu(string $key, $details): bool
    {
        return false;
    }

    /**
     * Recursively packages components based on data definitions of the content
     * 
     * @param string $key
     * @param mixed $data
     * @return mixed
     */
    private function recurseContent(string $key, $data)
    {
        if (is_string($data)) {
            return $data;
        }
        $output = $this->containerize();
        if (is_iterable($data)) {
            if (is_array($data)) {
                $data = $this->containerize($data);
            }
            if ($data->has('content')) {
                $data['content'] = $this->recurseContent('content', $data['content']);
            }
            if ($data->has('component')) {
                try {
                    return $output = $this->getComponent($data['component'], $key, $data->toArray());
                } catch (\Exception $e) {
                    d($data, $key, get_class($e), $e->getMessage(), $e->getTrace());
                    exit;
                }
            }
            foreach ($data as $key => $value) {
                $output[$key] = $this->recurseContent($key, $value);
            }
        }
        return $output;
    }

    /**
     * Updates nested containers recursively
     * 
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $original
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $replacements
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private function recurseContainerData(\Oroboros\core\interfaces\library\container\ContainerInterface $original, \Oroboros\core\interfaces\library\container\ContainerInterface $replacements): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $class = get_class($original);
        $interface = \Oroboros\core\interfaces\library\container\ContainerInterface::class;
        $values = [];
        foreach ($original as $key => $value) {
            if ($replacements->has($key)) {
                if (($value instanceof $interface) && ($replacements[$key] instanceof $interface)) {
                    $value = $this->recurseContainerData($value, $replacements[$key]);
                } else {
                    $value = $replacements[$key];
                }
            }
            $values[$key] = $value;
        }
        foreach ($replacements as $key => $value) {
            if (!array_key_exists($key, $values)) {
                $values[$key] = $value;
            }
        }
        return $class::init(null, $values);
    }

    /**
     * Checks definer menus for module updates and applies them if they exist.
     * 
     * @param string $name
     * @param \Oroboros\core\interfaces\library\menu\MenuContainerInterface $menu
     * @return \Oroboros\core\interfaces\library\menu\MenuContainerInterface
     */
    private function filterModuleMenuAdditions(string $name, \Oroboros\core\interfaces\library\menu\MenuContainerInterface $menu): \Oroboros\core\interfaces\library\menu\MenuContainerInterface
    {
        $this->getLogger()->debug('[type][scope][class] Checking module menu definition filters for menu [name].', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'menu' => $name,
        ]);
        if (!$this->hasArgument('modules')) {
            return $menu;
        }
        $menu_name = sprintf('%1$s:%2$s', static::CLASS_SCOPE, $name);
        foreach ($this->getArgument('modules') as $key => $module) {
            if ($module->hasMenuData($menu_name)) {
                $menu = $module->updateMenu($menu_name, $menu);
            }
        }
        return $menu;
    }
}
