<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\html;

/**
 * Html Login Page Utility Trait
 * Provides common methods for html login pages
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
trait HtmlLoginPageUtilityTrait
{

    use \Oroboros\core\traits\controller\ControllerLoginUtilityTrait;
    use \Oroboros\core\traits\html\DefinerPageContentUtility;

    /**
     * Parsed form data
     * @var \Oroboros\core\interfaces\library\form\FormDataInterface
     */
    private $form_values = null;

    /**
     * The supplied credentials from the client.
     * 
     * @var \Oroboros\core\interfaces\library\auth\CredentialContainerInterface
     */
    private $auth_keys = null;

    /**
     * Represents any authentication/validation errors that occurred during a
     * login attempt that should be relayed back to the user
     * 
     * @var array
     */
    private $auth_errors = [];

    /**
     * If the request to the login route resolves as being of type http GET,
     * this method will handle the response.
     * 
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $args
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $flags
     * @return \Oroboros\core\interfaces\controller\ControllerInterface
     */
    protected function handleHttpGetLogin(\Oroboros\core\interfaces\library\container\ContainerInterface $args = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\ControllerInterface
    {
//        d($this::user()->identity(), $_SESSION); exit;
        if ($this::user()->isLoggedIn()) {
            return $this->redirect('/dashboard/');
        }
        if (!$this::user()->can('http.login.submit')) {
            // User is not allowed to login. 403 the page.
            return $this->error(403);
        }
        if ($this::user()->has('login-form-errors')) {
            // Form errors from the prior page view
            $this->auth_errors = $this::user()->get('login-form-errors');
            $this::user()->unset('login-form-errors');
        }
        $this->getLogger()->debug('[type][scope][class] Initializing login page request.', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
        ]);
        try {
            $this->setupPage('login');
            $this->setupLoginComponent();
            $this->addStylesheet('stylesheet', 'login-theme', 'login-theme');
            return $this->render('html', 'login/index');
        } catch (\Exception $e) {
            d(get_class($e), $e->getMessage(), $e->getTrace());
            exit;
            return $this->error(500, $e->getMessage());
        }
        return $this;
    }

    /**
     * If the request to the login route resolves as being of type http POST,
     * this method will handle the response.
     * 
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $args
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $flags
     * @return \Oroboros\core\interfaces\controller\ControllerInterface
     */
    protected function handleHttpPostLogin(\Oroboros\core\interfaces\library\container\ContainerInterface $args = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\ControllerInterface
    {
        if ($this::user()->isLoggedIn()) {
            return $this->redirect('/dashboard/');
        }
        $this->getLogger()->debug('[type][scope][class] Verifying login form submission.', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
        ]);
        try {
            if ($this::user()->isGhost()) {
                // Ghost users can't login
                // Do not validate form, just return
                // the non-authentication error
                throw new \Oroboros\core\exception\auth\AuthenticationException('Ghost users are not permitted to login.');
            }
            $this->validateLoginRequest();
            $this->authenticateLoginRequest($this->auth_keys);
            $this->updateLoggedInUser($this->auth_keys);
            $this->getLogger()->debug('[type][scope][class] Clean login.', [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
            ]);
            return $this->redirect('/dashboard/', 302);
        } catch (\Oroboros\core\exception\auth\RefererException $e) {
            // Attempt to remotely post form or csrf
            // @todo pass this to the websec module if it is loaded for blacklisting
            $this->getLogger()->info('[type][scope][class] Offsite login attempt detected. Possible CSRF attack. The request was blocked from submitting login details.', [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
            ]);
            $this->auth_errors['identity'][] = $e->getMessage();
            $this->auth_errors['identity'][] = 'CSRF Error.';
//            return $this->handleHttpGetLogin($args, $flags);
        } catch (\Oroboros\core\exception\auth\ValidationException $e) {
            // Invalid form fields
            $this->getLogger()->debug('[type][scope][class] Invalid login form validation detected.', [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
            ]);
        } catch (\Oroboros\core\exception\auth\AuthenticationException $e) {
            // User does not exist, password invalid, not registered, not verified, etc
            $this->getLogger()->info('[type][scope][class] Invalid user request detected.', [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
            ]);
        } catch (\Oroboros\core\exception\InvalidArgumentException $e) {
            // No nonce in session. 500 error.
            $this->clearLoginNonce();
            $this->getLogger()->warning('[type][scope][class] Login nonce not detected in user session. Authentication cannot proceed.', [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
            ]);
            return $this->error(500, 'Nonce not found in session. Unable to authenticate.');
        }
        if (!is_null($this->form_values) && $this->form_values->has('nonce')) {
            unset($this->form_values['nonce']);
        }
        $this->clearLoginNonce();
        $this->auth_errors['identity'][] = 'Unable to login with the supplied credentials.';
        $this::user()->set('login-form-errors', $this->auth_errors);
        return $this->redirect(sprintf('/%1$s/', static::CLASS_SCOPE));
//        return $this->handleHttpGetLogin($args, $flags);
    }

    /**
     * If the request to the logout route resolves as being of type http GET,
     * this method will handle the response.
     * 
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $args
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $flags
     * @return \Oroboros\core\interfaces\controller\ControllerInterface
     */
    protected function handleHttpLogout(\Oroboros\core\interfaces\library\container\ContainerInterface $args = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\ControllerInterface
    {
        try {
            if (!$this::user()->isLoggedIn()) {
                // Can't logout if not logged in
                return $this->redirect(sprintf('/%1$s/', static::CLASS_SCOPE));
            }
            $this::user()->session()->destroy();
            return $this->redirect(sprintf('/%1$s/', static::CLASS_SCOPE));
        } catch (\Exception $e) {
            return $this->error(500, $e->getMessage());
        }
        return $this;
    }

    /**
     * Initializes the login component
     * 
     * @return void
     */
    protected function setupLoginComponent(): void
    {
        $this->getLogger()->debug('[type][scope][class] Generating login component.', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
        ]);
        $details = [];
        if (!is_null($this->form_values)) {
            $details = $this->form_values->toArray();
        }
        if (!is_null($this->auth_keys)) {
            $this->auth_keys = $this->auth_keys->toArray();
        }
        $this->auth_keys = $this->getDefaultLoginCredentials($this->auth_keys);
        foreach ($this->auth_keys as $key => $value) {
            if ($key === 'identity' && $value === 'root') {
                $value = 'root@localhost';
            }
            $details[$key] = $value;
        }
        $component = $this->getLayout()->getContent('main')[static::CLASS_SCOPE];
        $component->addContext('credentials', $this->getDefaultLoginCredentials($this->auth_keys->toArray()));
        $component->addContext('csrf', $this->containerize([
                'nonce' => unserialize($this::user()->get($this->getLoginNonceKey())),
                'token' => $this::user()->getAuthToken()
        ]));
        if (!empty($this->auth_errors)) {
            $component->addContext('errors', $this->containerize($this->auth_errors));
        }
    }

    /**
     * Validates that the login form was sent from an appropriate referrer
     * and that expected security tokens were properly relayed.
     * 
     * @return void
     * @throws \Oroboros\core\exception\auth\RefererException
     */
    protected function validateLoginRequest(): void
    {
        try {
            $this->form_values = $this->parsePostForm();
//            d($this->form_values, $_SESSION);
            $this->verifyLoginReferer($this->getArgument('request'));
            $this->verifyLoginAuthToken($this->getArgument('request'));
//            $this->verifyLoginNonce($this->getArgument('request'));
            $auth_keys = $this->filterAuthKeys($this->form_values->toArray());
            $this->auth_keys = $this->getAuthManager()->credentials($auth_keys);
        } catch (\Oroboros\core\exception\auth\RefererException $e) {
            // Off-site login attempt, this is a 
            // hacker bombing login with a script
            // This should be blacklisted
            // @todo pass this to the websec module
            $this->getLogger()->info('[type][scope][class] Detected off-site login attempt with exception message [exception-message]. The request was prevented.', [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
                'exception-message' => $e->getMessage(),
            ]);
            throw $e;
        } catch (\Oroboros\core\exception\auth\ValidationException $e) {
            // Token mismatch, this is a csrf attempt
            // This should be blacklisted
            // @todo pass this to the websec module
            $this->getLogger()->info('[type][scope][class] Detected CSRF login attempt with exception message [exception-message]. The request was prevented.', [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
                'exception-message' => $e->getMessage(),
            ]);
            throw $e;
        }
    }

    /**
     * Checks the identity and credentials of the submitted request
     * 
     * @param \Oroboros\core\interfaces\library\auth\CredentialContainerInterface $credentials
     * @return void
     */
    protected function authenticateLoginRequest(\Oroboros\core\interfaces\library\auth\CredentialContainerInterface $credentials): void
    {
        $this->getLogger()->debug('[type][scope][class] Authenticating login request credentials.', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
        ]);
        $identity = $this::user()->lookup($credentials->identity->id());
        if (is_null($identity)) {
            // No identity found
            throw new \Oroboros\core\exception\auth\AuthenticationException(
                    sprintf('Error encountered in [%1$s]. No user found with '
                        . 'user identity [%2$s]',
                        get_class($this), $credentials->identity->id())
            );
        }
        if (!$identity->authenticate($credentials)) {
            // Credential or host mismatch
            throw new \Oroboros\core\exception\auth\AuthenticationException(
                    sprintf('Error encountered in [%1$s]. User with identity '
                        . '[%2$s] could not authenticate against the '
                        . 'supplied credentials.',
                        get_class($this), $credentials->identity->id())
            );
        }
    }

    protected function updateLoggedInUser(\Oroboros\core\interfaces\library\auth\CredentialContainerInterface $credentials): void
    {
        $this->getLogger()->debug('[type][scope][class] Updating login user.', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
        ]);
        $this::user()->authenticate($credentials->identity->id(), $credentials);
    }

    /**
     * Parses the data from the post form
     * 
     * @return \Oroboros\core\interfaces\library\form\FormDataInterface
     * @throws \OutOfBoundsException
     */
    private function parsePostForm(): \Oroboros\core\interfaces\library\form\FormDataInterface
    {
        $form = null;
        try {
            $form = $this->extractFormValues(static::FORM_PARSER, static::CLASS_SCOPE);
        } catch (\OutOfBoundsException $e) {
            // Wrong form format
            throw $e;
        }
        return $form;
    }

    /**
     * Filters the login auth keys
     * 
     * @param array $form
     * @return array
     */
    private function filterAuthKeys(array $form): array
    {
        $filter_keys = ['identity', 'password', 'nonce', 'token'];
        $auth_keys = [];
        foreach ($form as $key => $value) {
            if (!in_array($key, $filter_keys, true)) {
                continue;
            }
            $auth_keys[$key] = $value;
        }
        if ($auth_keys['identity'] === 'root@localhost') {
            $auth_keys['identity'] = 'root';
        }
        return $auth_keys;
    }
}
