<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\html;

/**
 * Html Asset Utility Trait
 * This trait provides common methods for handling assets required for html pages.
 * This functionality is shared between components, layouts, themes,
 * and may also be used by modules or extensions on a case by case.
 * 
 * @usage Call the private method `initializeHtmlAssets` in your constructor after parent::__construct()
 * @note This trait completely satisfies interface \Oroboros\core\interfaces\interoperability\HtmlAssetAwareInterface
 * @see \Oroboros\core\interfaces\interoperability\HtmlAssetAwareInterface
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
trait HtmlAssetUtilityTrait
{

    /**
     * Scripts required by the component
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $scripts = null;

    /**
     * Stylesheets required by the component
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $styles = null;

    /**
     * Fonts required by the component
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $fonts = null;

    /**
     * Fonts required by the component
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $images = null;

    /**
     * Returns a container of the scripts required by the component,
     * indexed by the section of the dom they should appear in.
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    public function getScripts(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        return $this->scripts;
    }

    /**
     * Returns a container of the stylesheets required by the component.
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    public function getStyles(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        return $this->styles;
    }

    /**
     * Returns a container of the fonts required by the component.
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    public function getFonts(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        return $this->fonts;
    }

    /**
     * Returns a container of the fonts required by the component.
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    public function getImages(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        return $this->images;
    }

    /**
     * Adds a script by key identifier. Provide the section of the dom that
     * the script should compile into, and the key name of the script to compile.
     * @param string $section
     * @param string $key
     * @return void
     */
    public function addScript(string $section, string $key): void
    {
        $this->getLogger()->debug('[type][scope][class] Adding script [key] in section [section].', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'key' => $key,
            'section' => $section,
        ]);
        if (!$this->scripts->has($section)) {
            $this->scripts[$section] = $this::containerize([]);
        }
        $sect = $this->scripts[$section];
        $sect[$key] = $key;
        $this->scripts[$section] = $sect;
    }

    /**
     * Returns whether the component has a specified script as a dependency
     * @param string $key
     * @return bool
     */
    public function hasScript(string $key): bool
    {
        return $this->scripts->has($key);
    }

    /**
     * Removes a given script as a dependency, if it exists
     * @param string $key
     * @return void
     */
    public function removeScript(string $key): void
    {
        if ($this->scripts->has($key)) {
            $this->getLogger()->debug('[type][scope][class] Removing script [key].', [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
                'key' => $key,
            ]);
            unset($this->scripts[$key]);
        }
    }

    /**
     * Adds a stylesheet by key identifier.
     * @param string $key
     * @return void
     */
    public function addStyle(string $key): void
    {
        $this->getLogger()->debug('[type][scope][class] Adding stylesheet [key].', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'key' => $key,
        ]);
        $this->styles[$key] = $key;
    }

    /**
     * Returns whether the component has a given stylesheet as a dependency
     * @param string $key
     * @return bool
     */
    public function hasStyle(string $key): bool
    {
        return $this->styles->has($key);
    }

    /**
     * Removes a given stylesheet as a dependency, if it exists
     * @param string $key
     * @return void
     */
    public function removeStyle(string $key): void
    {
        if ($this->styles->has($key)) {
            $this->getLogger()->debug('[type][scope][class] Removing stylesheet [key].', [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
                'key' => $key,
            ]);
            unset($this->styles[$key]);
        }
    }

    /**
     * Adds a font by key identifier.
     * @param string $key
     * @return void
     */
    public function addFont(string $key): void
    {
        $this->getLogger()->debug('[type][scope][class] Adding font [key].', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'key' => $key,
        ]);
        $this->fonts[$key] = $key;
    }

    /**
     * Returns whether the component has a given font as a dependency
     * @param string $key
     * @return bool
     */
    public function hasFont(string $key): bool
    {
        return $this->fonts->has($key);
    }

    /**
     * Removes a given font as a dependency, if it exists
     * @param string $key
     * @return void
     */
    public function removeFont(string $key): void
    {
        if ($this->fonts->has($key)) {
            $this->getLogger()->debug('[type][scope][class] Removing font [key].', [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
                'key' => $key,
            ]);
            unset($this->fonts[$key]);
        }
    }

    /**
     * Adds a image by key identifier.
     * @param string $key
     * @return void
     */
    public function addImage(string $key): void
    {
        $this->getLogger()->debug('[type][scope][class] Adding image [key].', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
            'key' => $key,
        ]);
        $this->images[$key] = $key;
    }

    /**
     * Returns whether the component has a given image as a dependency
     * @param string $key
     * @return bool
     */
    public function hasImage(string $key): bool
    {
        return $this->images->has($key);
    }

    /**
     * Removes a given image as a dependency, if it exists
     * @param string $key
     * @return void
     */
    public function removeImage(string $key): void
    {
        if ($this->images->has($key)) {
            $this->getLogger()->debug('[type][scope][class] Removing image [key].', [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
                'key' => $key,
            ]);
            unset($this->images[$key]);
        }
    }

    /**
     * Override to provide default script dependencies for the component.
     * Must return an array with three keys (head, body, footer). Each
     * key is an indexed array of key identifiers for the script objects
     * to compile.
     * @return array
     */
    protected function getDefaultScripts(): array
    {
        return [
            'head' => [],
            'body' => [],
            'footer' => []
        ];
    }

    /**
     * Override to provide default stylesheet dependencies.
     * Must return an indexed array of stylesheet key identifiers
     * for stylesheet objects to compile.
     * @return array
     */
    protected function getDefaultStyles(): array
    {
        return [];
    }

    /**
     * Override to provide default font dependencies.
     * Must return an indexed array of font key identifiers
     * for font objects to compile.
     * @return array
     */
    protected function getDefaultFonts(): array
    {
        return [];
    }

    /**
     * Override to provide default image dependencies.
     * Must return an indexed array of image key identifiers
     * for image objects to compile.
     * @return array
     */
    protected function getDefaultImages(): array
    {
        return [];
    }

    /**
     * Call this method in your constructor after parent::construct 
     * to initialize html assets
     * @return void
     */
    private function initializeHtmlAssets(): void
    {
        $this->getLogger()->debug('[type][scope][class] Initializing HTML assets.', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
        ]);
        $this->initializeScripts();
        $this->initializeStyles();
        $this->initializeFonts();
        $this->initializeImages();
    }

    private function initializeScripts(): void
    {
        $this->getLogger()->debug('[type][scope][class] Initializing scripts.', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
        ]);
        if (is_null($this->scripts)) {
            $defaults = $this->getDefaultScripts();
            $this->scripts = $this::containerize($defaults);
        }
        if ($this->hasArgument('scripts')) {
            foreach ($this->getArgument('scripts') as $section => $scripts) {
                foreach ($scripts as $script) {

                    $this->addScript($section, $script);
                }
            }
        }
    }

    private function initializeStyles(): void
    {
        $this->getLogger()->debug('[type][scope][class] Initializing stylesheets.', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
        ]);
        if (is_null($this->styles)) {
            $defaults = $this->getDefaultStyles();
            $this->styles = $this::containerize($defaults);
        }
        if ($this->hasArgument('styles')) {
            foreach ($this->getArgument('styles') as $style) {
                $this->addStyle($style);
            }
        }
    }

    private function initializeFonts(): void
    {
        $this->getLogger()->debug('[type][scope][class] Initializing fonts.', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
        ]);
        if (is_null($this->fonts)) {
            $defaults = $this->getDefaultFonts();
            $this->fonts = $this::containerize($defaults);
        }
        if ($this->hasArgument('fonts')) {
            foreach ($this->getArgument('fonts') as $font) {
                $this->addFont($font);
            }
        }
    }

    private function initializeImages(): void
    {
        $this->getLogger()->debug('[type][scope][class] Initializing images.', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
        ]);
        if (is_null($this->images)) {
            $defaults = $this->getDefaultImages();
            $this->images = $this::containerize($defaults);
        }
        if ($this->hasArgument('images')) {
            foreach ($this->getArgument('images') as $image) {
                $this->addImage($image);
            }
        }
    }
}
