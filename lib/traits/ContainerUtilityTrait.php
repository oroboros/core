<?php
/*
 * The MIT License
 *
 * @author Brian Dayhoff <Brian Dayhoff@me.com>
 * @copyright (c) 2018, Brian Dayhoff <Brian Dayhoff@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits;

/**
 * <Container Trait>
 * An extremely basic Psr-11 container optimized for fast performance
 * and maximum interoperability. Can be added to any class to satisfy
 * any/all of the following interface requirements:
 *
 * Satisfies `\Iterator`, `\Traversable`, `\ArrayAccess`, `\Countable`,
 * `\Serializable`, `\JsonSerializable`, `\Psr\Container\ContainerInterface`
 *
 * So basically it's fully serializable, fully iterable, and can be used as
 * either an array or object in practice, can be json_encoded directly,
 * and is a valid dependency injection container.
 *
 * Object and array notation are both valid, and both resolve to the same result.
 * Eg `$container->id === $container['id']`
 *
 * It also has internal events you can bind to when just about
 * any possible thing happens inside the container,
 * to automate filtering and validation for pretty much any purpose.
 *
 * This logic allows you to enforce a size limit for memory management
 * similar to how `SplFixedArray` does,
 * although it is not a full implementation of `SplFixedArray`,
 * and by default does not enforce a size constraint unless you
 * explicitly pass a `$size` value into the constructor
 * or call `setSize`
 * (`setSize` will immediately truncate excess keys when called,
 * so be careful with that). If a size constraint is in place,
 * an exception will be raised when any further set operation occurs,
 * to prevent holding more data in memory than is acceptable.
 *
 * Good stuff.
 *
 * This is from my primary project, Oroboros.
 *
 * This logic has unit tested at 100% coverage in that project.
 *
 * I had to refactor out a couple of the other project dependencies
 * (mainly the type-checker validation logic and the project-specific exceptions,
 * which were swapped with the ones for this project),
 * but it was very loosely coupled to begin with, so it shouldn't be an issue.
 *
 * --------
 *
 * @author Brian Dayhoff <Brian Dayhoff@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @category internal
 * @package oroboros/core
 * @subpackage core
 * @version 0.2.5
 * @since 0.2.5
 *
 * @satisfies \Psr\Container\ContainerInterface
 *     This is a valid implementation of the Psr-11 spec
 *     (note: this container for versatility purposes enforces a scalar key,
 *     whereas Psr-11 documentation states that only strings
 *     should be used for keys. If you want to get fully pedantic,
 *     subclass and override the getters to only allow strings.
 *     If you want to do this, you only have to override the `_onSet`
 *     method and throw an exception if it does not receive a string key,
 *     because all internals fire that method prior to setting.
 *     This was an intentional design decision because this class is meant
 *     as a general purpose container, not explicitly a dependency container.
 *     All scalars are valid as far as this container is concerned,
 *     so you can set defaults responses for `true` or `false`,
 *     which are not normally valid array keys).
 * @satisfies \Serializable
 *     Lets you `serialize` only the values in the container
 *     without serializing the full object, and get an
 *     exact copy of it back when you `unserialize` it.
 * @satisfies \JsonSerializable
 *     Lets you use `json_encode` natively on it
 * @satisfies \ArrayAccess
 *     Lets you use various normal PHP array methods on it as if it were an array.
 * @satisfies \Countable
 *     Lets `count($container)` work as expected, as if it were a simple array.
 * @satisfies \Iterator
 *     Lets `foreach` work on it normally, and makes `is_iterable` resolve to `true`
 *
 * @note You need to also implement the above interfaces
 *     on your class to enable all of the PHP internal magic fully.
 *     This trait is a drop-in compatibility layer for all of these.
 *
 * @note You may see this trait in its original context here:
 *     https://bitbucket.org/oroborosframework/oroboros-core/src/eaea7f36e3a0ec218bc388c0023391a6e32f18e5/src/core/traits/core/container/ContainerTrait.php?at=codex&fileviewer=file-view-default
 */
trait ContainerUtilityTrait
{

    /**
     * Represents the pointer to which of the current keys the
     * container represents during iteration
     * @var int
     */
    private $container_pointer = 0;

    /**
     * Represents the maximum size of the container. If the value is -1, no limit
     * is set on the number of entries. Any value greater than -1 sets the maximum
     * number of values to the _container_size value, and any values set in excess
     * of this value are discarded.
     * @var int
     */
    private $container_size = -1;

    /**
     * Represents the keys for the given values of the container
     * @var array
     */
    private $container_keys = array();

    /**
     * Represents the values contained in the container
     * @var array
     */
    private $container_values = array();

    /**
     * Represents the valid count modes for \Countable
     * @var array
     */
    private $container_count_modes = array(
        COUNT_NORMAL,
        COUNT_RECURSIVE
    );

    /**
     * <Container Constructor>
     * The container constructor sets the maximum size for the container.
     * The default value of -1 designates that there is no limit to the size.
     *
     * This method fires the container instantiation trigger.
     */
    public function _construct($size = -1)
    {
        $this->onContainerInstantiation(func_get_args(), __FUNCTION__);
        try {
            if (!is_int($size)) {
                throw new \Oroboros\core\exception\core\InvalidClassException(
                        '$size must be an integer.');
            }
        } catch (\Psr\Container\ContainerExceptionInterface $e) {
            $this->onContainerException($e, __FUNCTION__);
            throw $e;
        }
        $this->onSetSize($size, __FUNCTION__);

        $this->container_size = $this->filterSetSize($size, __FUNCTION__);
        $this->rewind();
    }

    /**
     * <Container Destructor>
     * Fires the container teardown trigger.
     */
    public function __destruct()
    {
        $this->onContainerTeardown(__FUNCTION__);
    }

    /**
     * <PHP Magic Getter>
     * Points the PHP magic getter method to the
     * standardized container internals.
     * @param scalar $name
     * @return mixed
     */
    public function __get($name)
    {
        $this->onGet($name, __FUNCTION__);
        return $this->get($name);
    }

    /**
     * <PHP Magic Setter>
     * Points the PHP magic setter method to the
     * standardized container internals.
     * @param scalar $name
     * @param mixed $value
     * @return void
     */
    public function __set($name, $value)
    {
        $this->onSet($name, $value, __FUNCTION__);
        return $this->offsetSet($name, $value);
    }

    /**
     * <PHP Magic Checker>
     * Points the PHP magic checker method to the
     * standardized container internals.
     * @param scalar $name
     * @return bool
     */
    public function __isset($name)
    {
        $this->onExists($name, __FUNCTION__);
        return $this->has($name);
    }

    /**
     * <PHP Magic Unsetter>
     * Points the PHP magic unsetter method to the
     * standardized container internals.
     * @param scalar $name
     * @return void
     */
    public function __unset($name)
    {
        $this->onUnset($name, __FUNCTION__);
        return $this->offsetUnset($name);
    }

    /**
     * <Container Psr-11 Getter>
     * Finds an entry of the container by its identifier and returns it.
     *
     * This implementation also accepts scalar values and stringable objects
     * as valid id parameters.
     *
     * @param scalar $id Identifier of the entry to look for.
     * @return mixed Entry.
     * @throws NotFoundExceptionInterface  No entry was found for **this** identifier.
     * @throws ContainerExceptionInterface Error while retrieving the entry.
     * @see \Psr\Container\ContainerInterface
     * @link https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-11-container.md
     * @satisfies \Psr\Container\ContainerInterface::get
     */
    public function get($id)
    {
        $this->onGet($id, __FUNCTION__);
        if (is_object($id) && method_exists($id, '__toString')) {
            $id = (string) $id;
        }
        try {
            return $this->offsetGet($id);
        } catch (\Psr\Container\ContainerExceptionInterface $e) {
            //All pre-existing container exceptions bubble up on their own.
            throw $e;
        } catch (\Exception $e) {
            //Standardize all other exceptions to container exceptions
            $this->onException($e, __FUNCTION__);
            throw $e;
        }
    }

    /**
     * <Container Psr-11 Key Check>
     * Returns true if the container can return an entry for the given identifier.
     * Returns false otherwise.
     *
     * This implementation also accepts scalar values and stringable objects
     * as valid id parameters.
     *
     * `has($id)` returning true does not mean that `get($id)` will not throw an exception.
     * It does however mean that `get($id)` will not throw a `NotFoundExceptionInterface`.
     *
     * @param scalar $id Identifier of the entry to look for.
     * @return bool
     * @see \Psr\Container\ContainerInterface
     * @link https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-11-container.md
     * @satisfies \Psr\Container\ContainerInterface::has
     */
    public function has($id): bool
    {
        if (is_object($id) && method_exists($id, '__toString')) {
            $id = (string) $id;
        }
        $this->onExists($id, __FUNCTION__);
        return $this->offsetExists($id);
    }

    /**
     * <Container Iteration Rewind>
     * Rewinds the iterator to the beginning of the iterable subset
     * @return void
     * @link http://php.net/manual/en/class.iterator.php
     * @satisfies \Iterator::rewind
     */
    public function rewind()
    {
        $this->onRewind(__FUNCTION__);
        $this->container_pointer = $this->filterRewind(0, __FUNCTION__);
    }

    /**
     * <Container Current Value Iterable Getter>
     * Returns the value of the current iterable subset
     * @return mixed
     * @link http://php.net/manual/en/class.iterator.php
     * @satisfies \Iterator::current
     */
    public function current()
    {
        $current = $this->filterKey($this->container_values[$this->container_pointer]);
        $this->onCurrent($current, __FUNCTION__);
        return $this->filterCurrent($current, __FUNCTION__);
    }

    /**
     * <Container Key Iterable Getter>
     * Returns the current key of the iterable subset
     * @return scalar
     * @link http://php.net/manual/en/class.iterator.php
     * @satisfies \Iterator::key
     */
    public function key()
    {
        $key = $this->filterKey($this->container_keys[$this->container_pointer]);
        $this->onKey($key, __FUNCTION__);
        return $this->filterKey($key, __FUNCTION__);
    }

    /**
     * <Container Iteration Next>
     * Increments the iterator by one pointer position.
     * @return void
     * @link http://php.net/manual/en/class.iterator.php
     * @satisfies \Iterator::next
     */
    public function next()
    {
        $this->onNext($this->container_pointer, 1, __FUNCTION__);
        $increment = $this->filterNextIncrement($this->container_pointer, 1, __FUNCTION__);
        $updated_pointer = $this->container_pointer + $increment;
        $this->container_pointer = $this->filterNextResult($updated_pointer, $increment, __FUNCTION__);
    }

    /**
     * <Container Iteration Valid>
     * Returns whether the end of the iteration has been reached.
     * @return bool
     * @link http://php.net/manual/en/class.iterator.php
     * @satisfies \Iterator::valid
     */
    public function valid()
    {
        $valid = array_key_exists($this->container_pointer, $this->container_keys);
        $this->onValid($valid, __FUNCTION__);
        return $this->filterValid($valid, __FUNCTION__);
    }

    /**
     * <Container Countable Count>
     * Returns the number of elements in the container
     * @return int
     * @link http://php.net/manual/en/class.countable.php
     * @satisfies \Countable::count
     */
    public function count($mode = COUNT_NORMAL)
    {
        $this->onCount(count($this->container_keys), $mode, __FUNCTION__);
        return $this->filterCount(count($this->container_values, $mode), $mode, __FUNCTION__);
    }

    /**
     * <Container ArrayAccess Offset Check>
     * Checks if a given offset exists, if no conditions prevent checking.
     * Typically used with the native PHP isset()
     * @param scalar $offset
     * @return bool
     * @link http://php.net/manual/en/class.arrayaccess.php
     * @satisfies \ArrayAccess::offsetExists
     */
    public function offsetExists($offset)
    {
        try {
            if (!is_int($offset) && !is_string($offset)) {
                throw new \Oroboros\core\exception\core\InvalidClassException(
                        'Container key must be scalar. Additionally for dependency containers, '
                        . 'it must be a string.');
            }
        } catch (\Psr\Container\ContainerExceptionInterface $e) {
            $this->onContainerException($e, __FUNCTION__);
            throw $e;
        }
        $key = $this->filterKey($offset);
        $this->onExists($offset, __FUNCTION__);
        $exists = in_array($key, $this->container_keys, true);
        return $this->filterExists($key, $exists, __FUNCTION__);
    }

    /**
     * <Container ArrayAccess Offset Getter>
     * Gets a given offset, if no conditions prevent getting.
     * Typically used with the native PHP array key references
     * @param scalar $offset
     * @return mixed
     * @link http://php.net/manual/en/class.arrayaccess.php
     * @satisfies \ArrayAccess::offsetGet
     */
    public function offsetGet($offset)
    {
        try {
            if (!is_int($offset) && !is_string($offset)) {
                throw new \Oroboros\core\exception\core\InvalidClassException(
                        'Container key must be scalar. Additionally for dependency containers, '
                        . 'it must be a string.');
            }
        } catch (\Psr\Container\ContainerExceptionInterface $e) {
            $this->onContainerException($e, __FUNCTION__);
            throw $e;
        } {
            try {
                $key = array_search($this->filterKey($offset, __FUNCTION__), $this->container_keys);
                if ($key === false) {
                    throw new \Oroboros\core\exception\container\NotFoundException(
                            sprintf('Element with identifier [%1$s] not found in container '
                                . '[%2$s] at line [%3$s] of [%4$s]', $offset, get_class($this), __LINE__, __METHOD__));
                }
            } catch (\Psr\Container\ContainerExceptionInterface $e) {
                $this->onContainerException($e, __FUNCTION__);
                throw $e;
            }
        }
        $this->onGet($key, __FUNCTION__);
        $result = $this->container_values[$key];
        return $this->filterGet($key, $result, __FUNCTION__);
    }

    /**
     * <Container ArrayAccess Offset Setter>
     * Sets a given offset, if no conditions prevent setting.
     * Typically used with the native PHP array key set references
     * @param scalar $offset
     * @param mixed $value
     * @return void
     * @link http://php.net/manual/en/class.arrayaccess.php
     * @satisfies \ArrayAccess::offsetSet
     */
    public function offsetSet($offset, $value)
    {
        $this->onSet($offset, $value, __FUNCTION__);
        try {
            if (!is_int($offset) && !is_string($offset)) {
                throw new \Oroboros\core\exception\core\InvalidClassException(
                        'Container key must be scalar. Additionally for dependency containers, '
                        . 'it must be a string.');
            }
        } catch (\Psr\Container\ContainerExceptionInterface $e) {
            $this->onContainerException($e, __FUNCTION__);
            throw $e;
        }
        $key = $this->filterKey($offset, __FUNCTION__);
        $this->checkSize();
        $next = $this->count();
        $this->onSet($key, $value, __FUNCTION__);
        $key = $this->filterSetKey($key, $value, __FUNCTION__);
        $value = $this->filterSetValue($key, $value, __FUNCTION__);
        if (in_array($key, $this->container_keys)) {
            // If the key already exists, replace it.
            $index = array_search($key, $this->container_keys);
            $this->container_values[$index] = $value;
            return;
        }
        // If the key does not already exist, append it.
        $this->container_keys[$next] = $key;
        $this->container_values[$next] = $value;
    }

    /**
     * <Container ArrayAccess Offset UnSetter>
     * Unsets a given offset, if no conditions prevent unsetting.
     * Typically used with the native PHP array key unset references
     * @param scalar $offset
     * @return void
     * @link http://php.net/manual/en/class.arrayaccess.php
     * @satisfies \ArrayAccess::offsetUnset
     */
    public function offsetUnset($offset)
    {
        try {
            if (!is_int($offset) && !is_string($offset)) {
                throw new \Oroboros\core\exception\core\InvalidClassException(
                        'Container key must be scalar. Additionally for dependency containers, '
                        . 'it must be a string.');
            }
            $key = array_search($this->filterKey($offset, __FUNCTION__), $this->container_keys);
        } catch (\Psr\Container\ContainerExceptionInterface $e) {
            $this->onContainerException($e, __FUNCTION__);
            throw $e;
        }
        $this->onUnset($key, __FUNCTION__);
        $key = $this->filterUnset($key, __FUNCTION__);
        if ($key !== false) {
            unset($this->container_keys[$key]);
            unset($this->container_values[$key]);
            //Increment the pointer if it sits at a nonexistent key
            $increment = $this->filterNextIncrement($this->container_pointer, ( $this->container_pointer !== $key ? 0 : 1), __FUNCTION__);
            $pointer = $this->filterNextResult($this->container_pointer + $increment, $increment, __FUNCTION__);
            $this->container_pointer = $pointer;
        }
    }

    /**
     * <Container Serializable Serialize>
     * Serializes the value set of the container, and returns the subset
     * of keys and values and the internal size designator as a serialized
     * array.
     * @return string Serialized representation of the collection internal data set
     * @link http://php.net/manual/en/class.serializable.php
     * @satisfies \Serializable::serialize
     */
    public function serialize()
    {
        $serial = array(
            'size' => $this->getSize(),
            'values' => $this->toArray()
        );
        $this->onSerialize($serial, __FUNCTION__);
        return serialize($this->filterSerialize($serial, __FUNCTION__));
    }

    /**
     * <Container Serializable Unserialize>
     * Unserializes previously stored data to restore
     * a previously serialized container.
     *
     * Calling this method directly will truncate the contents of the container,
     * and replace all values with the serialized set if it has the correct
     * serialization schema required of this method.
     *
     * @param string $data Serialized data to restore
     * @return void
     * @link http://php.net/manual/en/class.serializable.php
     * @satisfies \Serializable::unserialize
     */
    public function unserialize($data)
    {
        $serial = unserialize($data);
        $this->onUnserialize($data, __FUNCTION__);
        $serial = $this->filterUnserialize($serial, __FUNCTION__);
        $this->container_keys = array();
        $this->container_values = array();
        $this->setSize($serial['size']);
        $this->checkSize(count($serial['values']));
        foreach ($serial['values'] as $key => $value) {
            $this->offsetSet($this->filterKey($key, __FUNCTION__), $value);
        }
    }

    /**
     * <Container Json Serialize>
     * Hooks into json_encode if the \JsonSerializable interface
     * exists on the class using this trait
     * @return array
     * @link http://php.net/manual/en/class.jsonserializable.php
     * @satisfies \JsonSerializable::jsonSerialize
     */
    public function jsonSerialize()
    {
        $results = $this->toArray();
        $this->onJsonSerialize($results, __FUNCTION__);
        return $this->filterJsonSerialize($results, __FUNCTION__);
    }

    /**
     * <Container Array Cast>
     * Returns a vanilla PHP array representation of the
     * container keys and values
     * @return array
     */
    public function toArray()
    {
        $values = array();
        foreach ($this->container_keys as $pointer => $key) {
            $values[$this->filterKey($key, __FUNCTION__)] = $this->container_values[$pointer];
        }
        $this->onToArray($values, __FUNCTION__);
        return $this->filterToArray($values, __FUNCTION__);
    }

    /**
     * <Container Array Parse>
     * Creates a new instance of the same class with
     * the given values provided in the array.
     * @param array|\Traversable $array
     * @param int $size The size of the new Container instance to return. Default is no limit
     * @return \oroboros\core\interfaces\contract\core\container\ContainerContract
     */
    public function fromArray($array, $size = -1)
    {
        $class = get_class($this);
        try {
            if (!is_integer($size)) {
                throw new \Oroboros\core\exception\core\InvalidClassException(
                        'Size must be an integer');
            }
            if (!( is_array($array) || ( is_object($array) && ( $array instanceof \Traversable ) ) )) {
                throw new \Oroboros\core\exception\core\InvalidClassException(
                        '$array must be an array, or a traversable object');
            }
        } catch (\Psr\Container\ContainerExceptionInterface $e) {
            $this->onContainerException($e, __FUNCTION__);
            throw $e;
        }
        $this->onFromArray($array, $size, __FUNCTION__);
        $size = $this->filterSetSize($size, __FUNCTION__);
        $array = $this->filterFromArray($array, $size, __FUNCTION__);
        $this->onNewContainerInstance($class, $size, __FUNCTION__);
        $instance = $this->filterNewContainerInstance($class, $size, __FUNCTION__);
        $this->checkSize(count($array));
        foreach ($array as $key => $value) {
            $instance[$this->filterKey($key, __FUNCTION__)] = $value;
        }
        return $instance;
    }

    /**
     * <Container Size Setter>
     * Sets the size constraint of the container.
     *
     * If the size is smaller than the current count of keys,
     * excess keys will be dropped.
     *
     * @param int $size an integer value of -1 or greater
     * @return void
     */
    public function setSize($size)
    {
        try {
            if (!is_int($size)) {
                throw new \Oroboros\core\exception\core\InvalidClassException(
                        'Size must be an integer.');
            }
        } catch (\Psr\Container\ContainerExceptionInterface $e) {
            $this->onContainerException($e, __FUNCTION__);
            throw $e;
        }
        $this->onSetSize($size, __FUNCTION__);
        $this->container_size = $this->filterSetSize($size, __FUNCTION__);
        $this->truncateSize($this->container_size);
    }

    /**
     * <Container Size Getter>
     * Returns the size constraint of the container
     * @return int
     */
    public function getSize()
    {
        $this->onGetSize($this->container_size, __FUNCTION__);
        return $this->filterGetSize($this->container_size, __FUNCTION__);
    }

    /**
     * <Container Pairs Caster>
     * Returns an array of arrays representing a key-value pair of the container
     * contents, where the first element is the key and the second element is
     * the value.
     * if they exist.
     * @return array
     */
    public function asPairs()
    {
        $pairs = array();
        foreach ($this->container_keys as $pointer => $key) {
            $pairs[] = array(
                $this->filterKey($key, __FUNCTION__),
                $this->container_values[$pointer]
            );
        }
        $this->onAsPairs($pairs, __FUNCTION__);
        return $this->filterAsPairs($pairs, __FUNCTION__);
    }

    /**
     * <Container Pairs Parser>
     * Returns an instance of the same class populated from a pairs array
     * @param array|\Traversable $pairs
     * @param int $size
     * @return type
     */
    public function fromPairs($pairs, $size = -1)
    {
        $class = get_class($this);
        try {
            if (!( is_array($pairs) || (is_object($pairs) && ($pairs instanceof \Traversable) ) )) {
                throw new \Oroboros\core\exception\core\InvalidClassException(
                        '$pairs must be an array or traversable object.');
            }
            if (!is_int($size)) {
                throw new \Oroboros\core\exception\core\InvalidClassException(
                        '$size must be an integer.');
            }
        } catch (\Psr\Container\ContainerExceptionInterface $e) {
            $this->onContainerException($e, __FUNCTION__);
            throw $e;
        }
        $this->onFromPairs($pairs, $size, __FUNCTION__);
        $size = $this->filterSetSize($size, __FUNCTION__);
        $pairs = $this->filterFromPairs($pairs, $size, __FUNCTION__);
        $this->onNewContainerInstance($class, $size, __FUNCTION__);
        $instance = $this->filterNewContainerInstance($class, $size, __FUNCTION__);
        $this->checkSize(count($pairs));
        foreach ($pairs as $pair) {
            $key = array_shift($pair);
            $value = array_shift($pair);
            $instance[$this->filterKey($key, __FUNCTION__)] = $value;
        }
        return $instance;
    }
    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     *
     * @note There is a lot of methods that do nothing here.
     *     You can override these for event bindings.
     *
     * -------------------------------------------------------------------------
     */

    /**
     * Internal getter for the object-scope keys. Bypasses filtering operations.
     * @return array
     */
    protected function getInternalKeys(): array
    {
        return $this->container_keys;
    }

    /**
     * Internal getter for the object-scope values. Bypasses filtering operations.
     * @return array
     */
    protected function getInternalValues(): array
    {
        return $this->container_values;
    }

    /**
     * <Container Setter Trigger Method>
     * This method triggers whenever a set operation occurs.
     * It allows any preflight operations prior to the operation to occur
     * if need be. The return value is ignored.
     *
     * This method is best used for authorization checks, or setup of required
     * assets needed for a set operation if they do not exist.
     *
     * @param scalar|stringable $key The passed key that the setter is supposed to have
     * @param mixed $value The value passed for the setter
     * @param string $method The internal method that the operation arose from.
     * @return void
     */
    protected function onSet($key, $value, $method = null)
    {
        //no-op
    }

    /**
     * <Container Setter Key Filter Method>
     * This method triggers whenever a set operation occurs, providing a means
     * of mutating the key if need be, or performing other operations with it.
     *
     * The key actually used in the set operation will be the return result of this method.
     *
     * @param scalar|stringable $key The passed key that the setter is supposed to have
     * @param mixed $value The key passed for the setter
     * @param string $method The internal method that the operation arose from.
     * @return void
     */
    protected function filterSetKey($key, $value, $method = null)
    {
        return $key;
    }

    /**
     * <Container Setter Value Filter Method>
     * This method triggers whenever a set operation occurs, providing a means
     * of mutating the value if need be, or performing other operations with it.
     *
     * The value actually used in the set operation will be the return result of this method.
     * This method fires after the key filter, and will receive the filtered key,
     * if it was changed.
     *
     * @param scalar|stringable $key The passed key that the setter is supposed to have
     * @param mixed $value The value passed for the setter
     * @param string $method The internal method that the operation arose from.
     * @return void
     */
    protected function filterSetValue($key, $value, $method = null)
    {
        return $value;
    }

    /**
     * <Container Getter Trigger Method>
     * This method triggers whenever a get operation occurs.
     * It allows any preflight operations prior to the operation to occur
     * if need be. The return value is ignored.
     *
     * This method is best used for authorization checks, or setup of required
     * assets needed for a get operation if they do not exist.
     *
     * @param type $key The passed key that the getter is supposed to have
     * @param string $method The internal method that the operation arose from.
     * @return void
     */
    protected function onGet($key, $method = null)
    {
        //no-op
    }

    /**
     * <Container Getter Value Filter Method>
     * This method triggers whenever a get operation occurs, providing a means
     * of mutating the value if need be, or performing other operations with it.
     *
     * The value actually used in the response will be the return result of this method.
     * Getter filters do not have a key filter method, so operations to determine an alternate
     * key will also need to be accounted for within this method if required.
     *
     * @param scalar|stringable $key The passed key that the setter is supposed to have
     * @param mixed $value The value passed for the getter
     * @param string $method The internal method that the operation arose from.
     * @return void
     */
    protected function filterGet($key, $value, $method = null)
    {
        return $value;
    }

    /**
     * <Container Unset Trigger Method>
     * This method triggers whenever an unset operation occurs.
     * It allows any preflight operations prior to the operation to occur
     * if need be. The return value is ignored.
     *
     * This method is best used for authorization checks, or setup of required
     * assets needed for an unset operation if they do not exist.
     *
     * @param type $key The passed key that the setter is supposed to have
     * @param string $method The internal method that the operation arose from.
     * @return void
     */
    protected function onUnset($key, $method = null)
    {
        //no-op
    }

    protected function filterUnset($key, $method = null)
    {
        return $key;
    }

    /**
     * <Container Rewind Trigger Method>
     * This method triggers whenever a rewind operation occurs prior to iteration.
     * It allows any preflight operations prior to the operation to occur
     * if need be. The return value is ignored.
     *
     * This method is best used for authorization checks, or setup of required
     * assets needed for a set operation if they do not exist.
     *
     * @param string $method The internal method that the operation arose from.
     * @return void
     */
    protected function onRewind($method = null)
    {
        //no-op
    }

    protected function filterRewind($offset, $method = null)
    {
        return $offset;
    }

    /**
     * <Container Current Trigger Method>
     * This method triggers whenever a current operation occurs during iteration.
     * It allows any preflight operations prior to the operation to occur
     * if need be. The return value is ignored.
     *
     * This method is best used for authorization checks, or setup of required
     * assets needed for a set operation if they do not exist.
     *
     * @param type $current
     * @param string $method The internal method that the operation arose from.
     * @return void
     */
    protected function onCurrent($current, $method = null)
    {
        //no-op
    }

    protected function filterCurrent($current, $method = null)
    {
        return $current;
    }

    /**
     * <Container Key Trigger Method>
     * This method triggers whenever a key operation occurs during iteration.
     * It allows any preflight operations prior to the operation to occur
     * if need be. The return value is ignored.
     *
     * This method is best used for authorization checks, or setup of required
     * assets needed for a set operation if they do not exist.
     *
     * @param type $key
     * @param string $method The internal method that the operation arose from.
     * @return void
     */
    protected function onKey($key, $method = null)
    {
        //no-op
    }

    protected function filterKey($key, $method = null)
    {
        return $key;
    }

    /**
     * <Container Next Trigger Method>
     * This method triggers whenever a next operation occurs during iteration.
     * It allows any preflight operations prior to the operation to occur
     * if need be. The return value is ignored.
     *
     * This method is best used for authorization checks, or setup of required
     * assets needed for a set operation if they do not exist.
     *
     * @param type $current
     * @param type $increment
     * @param string $method The internal method that the operation arose from.
     * @return void
     */
    protected function onNext($current, $increment, $method = null)
    {
        //no-op
    }

    protected function filterNextIncrement($current, $increment, $method = null)
    {
        return $increment;
    }

    protected function filterNextResult($updated_current, $increment, $method = null)
    {
        return $updated_current;
    }

    /**
     * <Container Valid Trigger Method>
     * This method triggers whenever a valid check operation occurs during iteration.
     * It allows any preflight operations prior to the operation to occur
     * if need be. The return value is ignored.
     *
     * This method is best used for authorization checks, or setup of required
     * assets needed for a set operation if they do not exist.
     *
     * @param type $valid
     * @param string $method The internal method that the operation arose from.
     * @return void
     */
    protected function onValid($valid, $method = null)
    {
        //no-op
    }

    protected function filterValid($valid, $method = null)
    {
        return $valid;
    }

    /**
     * <Container Key Exists Trigger Method>
     * This method triggers whenever a key check operation occurs for any reason.
     * It allows any preflight operations prior to the operation to occur
     * if need be. The return value is ignored.
     *
     * This method is best used for authorization checks, or setup of required
     * assets needed for a set operation if they do not exist.
     *
     * @param type $key
     * @param string $method The internal method that the operation arose from.
     * @return void
     */
    protected function onExists($key, $method = null)
    {
        //no-op
    }

    protected function filterExists($key, $designation, $method = null)
    {
        return $designation;
    }

    /**
     * <Container Count Trigger Method>
     * This method triggers whenever a count operation occurs.
     * It allows any preflight operations prior to the operation to occur
     * if need be. The return value is ignored.
     *
     * This method is best used for authorization checks, or setup of required
     * assets needed for a set operation if they do not exist.
     *
     * @param type $count
     * @param type $mode
     * @param string $method The internal method that the operation arose from.
     * @return void
     */
    protected function onCount($count, $mode, $method = null)
    {
        //no-op
    }

    protected function filterCount($count, $mode, $method = null)
    {
        return $count;
    }

    /**
     * <Container Serialize Trigger Method>
     * This method triggers whenever a serialize operation occurs.
     * It allows any preflight operations prior to the operation to occur
     * if need be. The return value is ignored.
     *
     * This method is best used for authorization checks, or setup of required
     * assets needed for a set operation if they do not exist.
     *
     * @param type $serial
     * @param string $method The internal method that the operation arose from.
     * @return void
     */
    protected function onSerialize($serial, $method = null)
    {
        //no-op
    }

    protected function filterSerialize($serial, $method = null)
    {
        return $serial;
    }

    /**
     * <Container Unserialize Trigger Method>
     * This method triggers whenever an unserialize operation occurs.
     * It allows any preflight operations prior to the operation to occur
     * if need be. The return value is ignored.
     *
     * This method is best used for authorization checks, or setup of required
     * assets needed for a set operation if they do not exist.
     *
     * @param type $data
     * @param string $method The internal method that the operation arose from.
     * @return void
     */
    protected function onUnserialize($data, $method = null)
    {
        //no-op
    }

    protected function filterUnserialize($data, $method = null)
    {
        return $data;
    }

    /**
     * <Container Json Serialize Trigger Method>
     * This method triggers whenever a json serialize operation occurs.
     * It allows any preflight operations prior to the operation to occur
     * if need be. The return value is ignored.
     *
     * This method is best used for authorization checks, or setup of required
     * assets needed for a set operation if they do not exist.
     *
     * @param type $data
     * @param string $method The internal method that the operation arose from.
     * @return void
     */
    protected function onJsonSerialize($data, $method = null)
    {
        //no-op
    }

    protected function filterJsonSerialize($data, $method = null)
    {
        return $data;
    }

    /**
     * <Container To Array Trigger Method>
     * This method triggers whenever a toArray operation occurs.
     * It allows any preflight operations prior to the operation to occur
     * if need be. The return value is ignored.
     *
     * This method is best used for authorization checks, or setup of required
     * assets needed for a set operation if they do not exist.
     *
     * @param type $array
     * @param string $method The internal method that the operation arose from.
     * @return void
     */
    protected function onToArray($array, $method = null)
    {
        //no-op
    }

    protected function filterToArray($array, $method = null)
    {
        return $array;
    }

    /**
     * <Container From Array Trigger Method>
     * This method triggers whenever a fromArray operation occurs.
     * It allows any preflight operations prior to the operation to occur
     * if need be. The return value is ignored.
     *
     * This method is best used for authorization checks, or setup of required
     * assets needed for a set operation if they do not exist.
     *
     * @param type $array
     * @param int $size The size parameter passed, indicating how large the
     *     maximum size of the resulting container should be.
     * @param string $method The internal method that the operation arose from.
     * @return void
     */
    protected function onFromArray($array, $size, $method = null)
    {
        //no-op
    }

    protected function filterFromArray($array, $size, $method = null)
    {
        return $array;
    }

    /**
     * <Container Size Setter Trigger Method>
     * This method triggers whenever a setSize operation occurs.
     * It allows any preflight operations prior to the operation to occur
     * if need be. The return value is ignored.
     *
     * This method is best used for authorization checks, or setup of required
     * assets needed for a set operation if they do not exist.
     *
     * @param int $size The size parameter passed, indicating how large the
     *     maximum size of the resulting container should be.
     * @param string $method The internal method that the operation arose from.
     * @return void
     */
    protected function onSetSize($size, $method = null)
    {
        //no-op
    }

    protected function filterSetSize($size, $method = null)
    {
        return $size;
    }

    /**
     * <Container Get Size Trigger Method>
     * This method triggers whenever a getSize operation occurs.
     * It allows any preflight operations prior to the operation to occur
     * if need be. The return value is ignored.
     *
     * This method is best used for authorization checks, or setup of required
     * assets needed for a set operation if they do not exist.
     *
     * @param int $size The size parameter passed, indicating how large the
     *     maximum size of the resulting container should be.
     * @param string $method The internal method that the operation arose from.
     * @return void
     */
    protected function onGetSize($size, $method = null)
    {
        //no-op
    }

    protected function filterGetSize($size, $method = null)
    {
        return $size;
    }

    /**
     * <Container As Pairs Trigger Method>
     * This method triggers whenever an asPairs operation occurs.
     * It allows any preflight operations prior to the operation to occur
     * if need be. The return value is ignored.
     *
     * This method is best used for authorization checks, or setup of required
     * assets needed for a set operation if they do not exist.
     *
     * @param type $pairs
     * @param string $method The internal method that the operation arose from.
     * @return void
     */
    protected function onAsPairs($pairs, $method = null)
    {
        //no-op
    }

    protected function filterAsPairs($pairs, $method = null)
    {
        return $pairs;
    }

    /**
     * <Container To Pairs Trigger Method>
     * This method triggers whenever a toPairs operation occurs.
     * It allows any preflight operations prior to the operation to occur
     * if need be. The return value is ignored.
     *
     * This method is best used for authorization checks, or setup of required
     * assets needed for a set operation if they do not exist.
     *
     * @param type $pairs
     * @param int $size The size parameter passed, indicating how large the
     *     maximum size of the resulting container should be.
     * @param string $method The internal method that the operation arose from.
     * @return void
     */
    protected function onFromPairs($pairs, $size, $method = null)
    {
        //no-op
    }

    protected function filterFromPairs($pairs, $size, $method = null)
    {
        return $pairs;
    }

    /**
     * <Container Psr-11 Exception Trigger Method>
     * This method triggers whenever a container exception is thrown,
     * immediately prior to the exception being raised.
     * It allows any preflight operations prior to the operation to occur
     * if need be. The return value is ignored, and the exception cannot
     * be caught at this point.
     *
     * This method is best used for logging.
     *
     * @param \Psr\Container\ContainerExceptionInterface $exception
     * @param string $method The internal method that the operation arose from.
     * @return void
     */
    protected function onContainerException(\Psr\Container\ContainerExceptionInterface $exception, $method = null)
    {
        //no-op
    }

    /**
     * <Container Generic Exception Trigger Method>
     * This method triggers whenever a container exception is thrown,
     * immediately prior to the exception being raised.
     * It allows any preflight operations prior to the operation to occur
     * if need be. The return value is ignored, and the exception cannot
     * be caught at this point.
     *
     * This method is best used for logging.
     *
     * @note Exceptions that trigger this method will be automatically recast to
     *     Psr-11 container exceptions, which will also fire the
     *     _onContainerException method. Logging of the same exception
     *     in these cases is redundant.
     *
     * @param \Psr\Container\ContainerExceptionInterface $exception
     * @param string $method The internal method that the operation arose from.
     * @return void
     */
    protected function onException(\Exception $e, $method = null)
    {
        //no-op
    }

    /**
     * <Container Generic Exception Trigger Method>
     * This method triggers whenever a container exception is thrown,
     * immediately prior to the exception being raised.
     * It allows any preflight operations prior to the operation to occur
     * if need be. The return value is ignored, and the exception cannot
     * be caught at this point.
     *
     * @note Exceptions that trigger this method will be automatically recast to
     *     Psr-11 container exceptions, which will also fire the
     *     _onContainerException method. Logging of the same exception
     *     in these cases is redundant.
     *
     * @param string $class The name of the class to be created, which will
     *     be a fully qualified instance of
     *     \oroboros\core\interfaces\contract\core\container\ContainerContract
     *     represented as a string
     * @param mixed $args The arguments to be passed into the constructor.
     *     If the constructor only receives one argument, this will be that
     *     argument. Otherwise it will be an array of arguments.
     * @param string $method The internal method that the operation arose from.
     * @return void
     */
    protected function onNewContainerInstance($class, $args, $method = null)
    {
        //no-op
    }

    /**
     * <Container Object Generation Trigger Method>
     * This method triggers whenever a new container instance is slated
     * to be generated, immediately prior to the instance being created.
     * It allows any preflight operations prior to the operation to occur
     * if need be. The return value is ignored.
     *
     * This method is best used for substitution or calling
     * factories/prototypers/generators as needed.
     *
     * @param string $class The name of the class to be created, which will
     *     be a fully qualified instance of
     *     \oroboros\core\interfaces\contract\core\container\ContainerContract
     *     represented as a string
     * @param mixed $args The arguments to be passed into the constructor.
     *     If the constructor only receives one argument, this will be that
     *     argument. Otherwise it will be an array of arguments.
     * @param string $method The internal method that the operation arose from.
     * @return \oroboros\core\interfaces\contract\core\container\ContainerContract
     *     This method MUST return an instantiated object.
     */
    protected function filterNewContainerInstance($class, $args, $method = null)
    {
        return new $class($args);
    }

    /**
     * <Container Object Instantiation Trigger Method>
     * This method triggers from the constructor of the container. It allows any
     * instantiation parameters to be handled prior to following through with
     * instantiation. The return value is ignored.
     *
     * @param array $args The arguments to be passed into the constructor.
     *     If the constructor only receives one argument, this will be an array
     *     of arguments.
     * @param string $method The internal method that the operation arose from.
     * @return void
     */
    protected function onContainerInstantiation($args, $method = null)
    {
        //no-op
    }

    /**
     * <Container Object Teardown Trigger Method>
     * This method triggers from the destructor of the container. It allows any
     * garbage collection, unregistration, or cleanup tasks to take place prior
     * to the object being released from memory. The return value is ignored.
     *
     * @param string $method The internal method that the operation arose from.
     * @return void
     */
    protected function onContainerTeardown($method = null)
    {
        //no-op
    }

    protected function addValue($key, $value)
    {
        if (!is_scalar($key)) {
            throw new \Oroboros\core\exception\container\ContainerException(
                    'Container key must be scalar. '
                    . 'Additionally for dependency containers, '
                    . 'it must be a string.');
        }
        $existing = array_search($key, $this->container_keys);
        if ($existing === false) {
            $this->container_keys[] = $key;
            $existing = array_search($key, $this->container_keys);
        }
        $this->container_keys[$existing] = $key;
        $this->container_values[$existing] = $value;
    }

    protected function removeValue($key)
    {
        if (!is_scalar($key)) {
            throw new \Oroboros\core\exception\container\ContainerException(
                    'Container key must be scalar. '
                    . 'Additionally for dependency containers, '
                    . 'it must be a string.');
        }
        $existing = array_search($key, $this->container_keys);
        if ($existing === false) {
            return;
        }
        unset($this->container_keys[$existing]);
        unset($this->container_values[$existing]);
    }

    /**
     * Checks if additions would exceed the size limit
     * @param int $elements
     * @return void
     */
    private function checkSize($elements = 1)
    {
        $proposed_size = $this->count() + $elements;
        $size = $this->getSize();
        if ($size !== -1 && $proposed_size > $size) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Proposed new size of [%1$s] exceeds the limit available [%2$s] for '
                        . '[%3$s] at line [%4$s] of [%5$s]', $proposed_size, $size, get_class($this), __LINE__, __METHOD__));
        }
    }

    /**
     * Drops additional keys if the size is reduced below
     * the current count of elements.
     * @param int $allowed
     * @return void
     */
    private function truncateSize($allowed)
    {
        $count = $this->count();
        if ($allowed === -1 || $count <= $allowed) {
            return;
        }
        $key_count = 0;
        foreach ($this->container_keys as $pointer => $key) {
            $key_count++;
            if ($key_count <= $allowed) {
                continue;
            }
            $k = $this->container_keys[$pointer];
            $this->onUnset($k, __FUNCTION__);
            unset($this->container_keys[$pointer]);
            unset($this->container_values[$pointer]);
        }
    }
}
