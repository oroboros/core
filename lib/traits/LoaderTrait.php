<?php
namespace Oroboros\core\traits;

/**
 * Localizes dynamic factorization into base abstract class types,
 * so they can factorize further required objects with little complexity.
 * 
 * This is just a facade for the two pass factory implementation,
 * so objects can be returned from a single method call.
 * It lazy loads its internals so there is no need to instantiate it.
 * 
 * It also has an accessor to expand shortened class names
 * to their fully qualified name for instance checking purposes.
 *
 * @author Brian Dayhoff
 */
trait LoaderTrait
{

    /**
     * The namespace prefix for further factorization.
     * Allows the base namespace to be abstracted out entirely.
     * @var string
     */
    private static $namespace_base = 'Oroboros\\core';
    private static $factory_factory = null;
    private static $loader_initialized = false;
    private static $factory_prefix_mapping = [
        'adapter' => 'adapter',
        'bootstrap' => 'bootstrap',
        'controller' => 'controller',
        'model' => 'model',
        'view' => 'view',
        'extension' => 'extensions',
        'module' => 'modules',
        'service' => 'services',
        'component' => 'library\\component',
        'layout' => 'library\\layout',
    ];

    /**
     * 
     * @param string $type [controller, model, view, library]
     * @param string $instance The name of the class to generate an object instance of.
     *  The base PSR-4 vendor\package portion may be omitted.
     * @param string $command (optional) Not required unless the factorized class requires this parameter in its constructor.
     * @param array $arguments (optional) Not required unless the factorized class requires this parameter in its constructor.
     * @param array $flags (optional) Not required unless the factorized class requires this parameter in its constructor.
     * @return \Oroboros\core\interfaces\BaseInterface The response object depends on the sub-factory's scoped interface.
     *  All responses will implement the base interface.
     */
    protected function load(string $type, string $instance, string $command = null, array $arguments = null, array $flags = null)
    {
        if (!self::$loader_initialized) {
            // Perform initialization if it has not already occurred.
            // This should be done the first time this method is referenced in any given class type, and not repeat.
            $this->initializeLoader();
        }
        $full_name = $this->getFullClassName($type, $instance);
        if (!$full_name) {
            // Class not found
            $this->getLogger()->warning('[type][scope][class] Class [class-type] with instance [class-instance] not found.', [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
                'class-type' => $type,
                'class-instance' => $instance,
            ]);
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'No class found of type [%2$s] with instance [%3$s]. '
                        . 'Evaluating namespaces [%4$s].', get_class($this), $type, $instance, implode(', ', \Oroboros\core\library\autoload\Autoloader::getNamespaces())));
        }
        $factory_instance = self::$namespace_base . '\\pattern\\factory\\' . ucfirst($type) . 'Factory';
        $sub_factory = self::$factory_factory->load($factory_instance);
        $instance = $sub_factory->load($full_name, $command, $arguments, $flags);
        try {
            $this->getLogger()->debug('[type][scope][class] Loaded class [class-type] with instance [class-instance].', [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
                'class-type' => $type,
                'class-instance' => get_class($instance),
            ]);
        } catch (\Oroboros\core\exception\ErrorException $e) {
            // Logger not loaded yet
        }
        return $instance;
    }

    /**
     * Returns the fully qualified class name of a given truncated instance.
     * @param string $type [controller, model, view, library]
     * @param string $instance The name of the class to generate an object instance of.
     *  The base PSR-4 vendor\package portion may be omitted.
     * @return string
     */
    protected function getFullClassName(string $type, string $instance)
    {
        foreach (\Oroboros\core\library\autoload\Autoloader::getNamespaces() as $prefix) {
            $prefix = $this->getLoaderPrefix($prefix, $type);
            if (strpos($instance, $prefix) !== 0) {
                $full_name = $prefix . '\\' . $instance;
            } else {
                // Do not double prepend. If the given class name
                // is already fully qualified, return it as is.
                // This insures compatibility with explicit class declarations,
                // preventing enforced abstraction that extending logic
                // does not want to use.
                $full_name = $instance;
            }
            if (class_exists($full_name)) {
                return $full_name;
            }
        }
        return false;
    }

    /**
     * Performs initialization of the default base level factory factory,
     * so loader operations can commence without issue thereafter.
     * 
     * This should only occur one time for any given class type.
     */
    private function initializeLoader()
    {
        self::$factory_factory = new \Oroboros\core\pattern\factory\FactoryFactory();
        // Do not redundantly load additional factory factories or it will get immensely difficult to debug.
        self::$loader_initialized = true;
    }

    private function getLoaderPrefix(string $prefix, string $type): string
    {
//        echo 'Prefix: ' . $prefix . PHP_EOL;
//        echo 'Type: ' . $prefix . PHP_EOL;
//        echo PHP_EOL;
        if (!array_key_exists($type, self::$factory_prefix_mapping)) {
            return $prefix . '\\' . $type;
        }
        return $prefix . '\\' . self::$factory_prefix_mapping[$type];
    }
}
