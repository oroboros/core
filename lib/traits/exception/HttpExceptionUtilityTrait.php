<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\exception;

/**
 * Http Exception Utility Trait
 * All http error exceptions must extend \ErrorException
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
trait HttpExceptionUtilityTrait
{

    use ExceptionUtilityTrait;

    public function __construct(string $message = "", int $code = 0, int $severity = E_ERROR, string $filename = '__FILE__', int $lineno = PHP_INT_MAX, \Exception $previous = null)
    {
        $trace = debug_backtrace(1)[1];
        if ($filename === '__FILE__') {
            $filename = $trace['file'];
        }
        if ($lineno === PHP_INT_MAX) {
            $lineno = $trace['line'];
        }
        if ($code === 0 && $previous !== null) {
            $code = $previous->getCode();
        }
        $this->validateStatusCode();
        return parent::__construct($message, $code, $severity, $filename, $lineno, $previous);
    }

    public function getStatusCode(): int
    {
        return static::STATUS_CODE;
    }

    private function validateStatusCode(): void
    {
        $class = get_class($this);
        $expected = 'STATUS_CODE';
        if (!defined($class . '::' . $expected)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(sprintf('Error encountered in [%1$s]. Expected class constant [%2$s] is not defined.', $class, $expected));
        }
    }
}
