<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\model;

/**
 * Model Aware Utility Trait
 * Provides methods required for dynamically loading models
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
trait ModelAwareUtilityTrait
{

    private static $model_strategy = null;

    /**
     * Loads a model corresponding to the given key and returns it
     * 
     * @param string $key The key identifier for the model
     * @param string $command The command to pass to the model
     * @param array $arguments Any arguments required by the model
     * @param array $flags Any flags to pass to the model
     * @return \Oroboros\core\interfaces\model\ModelInterface
     * @throws \Oroboros\core\exception\InvalidArgumentException
     *         If no model is known for the corresponding key
     * @throws \Oroboros\core\exception\core\InvalidClassException
     *         If a model is broken, or the strategy loading it is broken.
     *         This can also occur if the class implementing this trait
     *         has declared an invalid `MODEL_STRATEGY` class constant value.
     * @throws \Oroboros\core\exception\ErrorException
     *         If a model cannot load with the supplied arguments
     */
    protected function getModel(string $key, string $command = null, array $args = null, array $flags = null): \Oroboros\core\interfaces\model\ModelInterface
    {
        $this->initializeModelAware();
        return self::$model_strategy->get($key, $command, $args, $flags);
    }
    
    /**
     * Returns the fully qualified classname of the model
     * corresponding to the given key.
     * 
     * @param string $key
     * @return string
     * @throws \Oroboros\core\exception\InvalidArgumentException
     *         if the specified model identifier is not registered
     *         to any known model
     */
    protected function modelType(string $key): string
    {
        $this->initializeModelAware();
        return self::$model_strategy->type($key);
    }
    
    /**
     * Returns a boolean designation as to whether
     * a model exists for a given key identifier.
     * 
     * @param string $key
     * @return bool
     */
    protected function hasModel(string $key): bool
    {
        $this->initializeModelAware();
        return self::$model_strategy->has($key);
    }
    
    /**
     * Returns an associative array of all models currently registered,
     * where the key is the identifier for the model, and the value is
     * the fully qualified class name of the model to load.
     * 
     * @return array
     */
    protected function listModels(): array
    {
        $this->initializeModelAware();
        return self::$model_strategy->index();
    }
    
    /**
     * Returns the model strategy object directly for
     * model registration or more complex operations.
     * 
     * @return \Oroboros\core\interfaces\model\ModelStrategyInterface
     */
    protected function getModelStrategy(): \Oroboros\core\interfaces\model\ModelStrategyInterface
    {
        $this->initializeModelAware();
        return self::$model_strategy;
    }

    /**
     * Initializes the model aware functionality
     * This is automatically called the first time a model is requested.
     * 
     * @return void
     * @throws \Oroboros\core\exception\core\InvalidClassException
     */
    private function initializeModelAware(): void
    {
        if (is_null(self::$model_strategy)) {
            $proxy = new \Oroboros\core\pattern\factory\LoaderProxy();
            $resolver = $proxy->load('library', 'resolver\\ArgumentResolver');
            $class = \Oroboros\core\model\ModelStrategy::class;
            $expected = \Oroboros\core\interfaces\model\ModelStrategyInterface::class;
            $const = sprintf('%1$s::%2$s', get_class($this), 'MODEL_STRATEGY');
            if (defined($const)) {
                $class = constant($const);
            }
            if (!$resolver->resolve($class, $expected)) {
                throw new \Oroboros\core\exception\core\InvalidClassException(
                        sprintf('Error encountered in [%1$s]. Optional '
                            . 'class constant [%2$s] with value [%3$s] does '
                            . 'not resolve to a valid class name or stub class '
                            . 'name implementing expected interface [%4$s]. '
                            . 'This class is not useable in it\'s current state.'
                            , get_class($this), $class, 'MODEL_STRATEGY', $expected)
                );
            }
            $strategy = new $class();
            self::$model_strategy = $strategy;
        }
    }
}
