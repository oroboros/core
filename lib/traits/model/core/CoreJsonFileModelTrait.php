<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\model\core;

/**
 * Core Json File Model Trait
 * Forces core scope for core models
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
trait CoreJsonFileModelTrait
{

    /**
     * Forces the core config dir rather than
     * letting the app determine the path
     * 
     * This is used for cases where apps need to fallback
     * to the core definition because their own is absent.
     * 
     * @return string
     */
    protected function defineSourceFilePath(): string
    {
        $path = OROBOROS_CORE_CONFIG;
        return $path;
    }

    /**
     * Core configs are always readonly
     * 
     * @return bool
     */
    public function isReadonly(): bool
    {
        return true;
    }

    /**
     * Core config models are always readonly.
     * This method will always raise an exception.
     * 
     * @param array $details
     * @return \Oroboros\core\interfaces\model\ModelInterface
     */
    public function create(array $details): \Oroboros\core\interfaces\model\ModelInterface
    {
        $this->coreReadonlyErrorMessage();
    }

    /**
     * Core config models are always readonly.
     * This method will always raise an exception.
     * 
     * @param string $key
     * @param type $value
     * @return \Oroboros\core\interfaces\model\ModelInterface
     */
    public function set(string $key, $value): \Oroboros\core\interfaces\model\ModelInterface
    {
        $this->coreReadonlyErrorMessage();
    }

    /**
     * Core config models are always readonly.
     * This method will always raise an exception.
     * 
     * @param string $key
     * @return \Oroboros\core\interfaces\model\ModelInterface
     * @throws \Oroboros\core\exception\model\ReadonlyException
     *         Core config models are always readonly
     */
    public function delete(string $key = null): \Oroboros\core\interfaces\model\ModelInterface
    {
        $this->coreReadonlyErrorMessage();
    }

    /**
     * Core config models do not mutate their data points,
     * they are always readonly.
     * 
     * @return void
     */
    protected function saveDataChanges(): void
    {
        // no-op
    }

    /**
     * Throws the exception all core config models throw
     * when an attempt to edit the underlying data is made.
     * 
     * @return void
     * @throws \Oroboros\core\exception\model\ReadonlyException
     */
    private function coreReadonlyErrorMessage(): void
    {
        throw new \Oroboros\core\exception\model\ReadonlyException(
                sprintf('Error encountered in [%1$s]. An attempt to edit a '
                    . 'core config was made. Core configs are always readonly.'
                    , get_class($this))
        );
    }
}
