<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\library\log;

/**
 * Logger Aware Trait
 * A drop-in implementation of Psr-3 `\Psr\Log\LoggerrAwareInterface`
 * for Oroboros classes
 * 
 * All of the logic in this trait is self contained and has
 * no underlying dependencies.
 *
 * @satisfies \Oroboros\core\interfaces\library\log\LoggerAwareInterface
 * @satisfies \Psr\Log\LoggerAwareInterface
 * @see \Psr\Log\LoggerAwareInterface
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
trait LoggerAwareTrait
{

    /**
     * Represents the logger set into the class.
     * The logger may be replaced at any time.
     * 
     * @var \Psr\Log\LoggerInterface
     */
    private $logger = null;

    /**
     * Sets a logger instance on the object.
     *
     * @param LoggerInterface $logger
     * @return void
     */
    public function setLogger(\Psr\Log\LoggerInterface $logger)
    {
        $this->logger = $logger;
        $this->setArgument('logger', $logger);
    }

    /**
     * Internal getter for the logger object.
     * @return \Psr\Log\LoggerInterface
     */
    protected function getLogger(): \Psr\Log\LoggerInterface
    {
        if (is_null($this->logger)) {
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. Logger cannot be '
                        . 'returned because no logger has been defined in '
                        . 'this object.', get_class($this))
            );
        }
        return $this->logger;
    }

    /**
     * Declares the arguments to pass to the logger
     * that is automatically instantiated.
     * 
     * By default this passes the same arguments passed
     * into the costructor of the object itself.
     * 
     * You may override this method to pass specialized arguments,
     * dependencies, or append extra arguments to the logger constructor
     * 
     * @return array|null
     */
    protected function declareLoggerArguments(): ?array
    {
        return $this->getArguments();
    }

    /**
     * Declares the flags to pass to the logger
     * that is automatically instantiated.
     * 
     * By default this passes the same flags passed
     * into the costructor of the object itself.
     * 
     * You may override this method to pass specialized flags or
     * append extra flags to the logger constructor
     * 
     * @return array|null
     */
    protected function declareLoggerFlags(): ?array
    {
        return $this->getFlags();
    }

    /**
     * Initializes a default logger. This should be called
     * in the constructor of the implementing class.
     * 
     * If the class constant `DEFAULT_LOGGER` is defined with a
     * valid class or stub class name resolving to a valid class
     * implementing `\Oroboros\core\interfaces\library\log\LoggerInterface`,
     * then an instance of that class will be automatically loaded and
     * set as the logger. If the constant is not defined,
     * the default null logger will be used.
     * 
     * @return void
     */
    private function initializeDefaultLogger(): void
    {
        $psr_expected = \Psr\Log\LoggerInterface::class;
        if ($this->hasArgument('logger') && is_object($this->getArgument('logger')) && ($this->getArgument('logger') instanceof $psr_expected)) {
            // A logger was explicitly dependency injected
            $this->setLogger($this->getArgument('logger'));
            return;
        }
        if ($this instanceof \Oroboros\core\interfaces\library\log\LoggerInterface) {
            // Prevent infinite recursion
            $this->setLogger($this);
            return;
        }
        if ($this instanceof \Oroboros\core\pattern\factory\LoaderProxy) {
            // Prevent infinite recursion
            return;
        }
        $expected = \Oroboros\core\interfaces\library\log\LoggerInterface::class;
        $default = \Oroboros\core\library\log\NullLogger::class;
        $const = sprintf('%1$s::%2$s', get_class($this), 'DEFAULT_LOGGER');
        if (!defined($const)) {
            $logger = new $default(null, $this->declareLoggerArguments(), $this->declareLoggerFlags());
            $this->setLogger($logger);
            return;
        }
        $default = constant($const);
        $proxy = new \Oroboros\core\pattern\factory\LoaderProxy();
        $class = $proxy->getFullClassName('library', $default);
        if ($class === false || !in_array($expected, class_implements($class))) {
            // Broken logger definition. Bail.
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. Optional class constant '
                        . '[%2$] with value [%3$s] must define a valid class name '
                        . 'or stub class name implementing expected interface '
                        . '[%4$s]. This class is not useable in it\'s current state.',
                        get_class($this), 'DEFAULT_LOGGER', $default, $expected)
            );
        }
        $logger = $proxy->load('library', $default, null, $this->declareLoggerArguments(), $this->declareLoggerFlags());
        $this->setLogger($logger);
    }
}
