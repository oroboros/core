<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\library\log;

/**
 * Logger Trait
 * Provides a drop in compatibility trait for \Psr\Log\LoggerInterface
 *
 * @satisfies \Psr\Log\LoggerInterface
 * @satisfies \Oroboros\core\interfaces\library\log\LoggerInterface
 * @see \Psr\Log\LoggerInterface
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
trait LoggerTrait
{

    private static $valid_log_levels = [
        'emergency' => \Psr\Log\LogLevel::EMERGENCY,
        'alert' => \Psr\Log\LogLevel::ALERT,
        'critical' => \Psr\Log\LogLevel::CRITICAL,
        'error' => \Psr\Log\LogLevel::ERROR,
        'warning' => \Psr\Log\LogLevel::WARNING,
        'notice' => \Psr\Log\LogLevel::NOTICE,
        'info' => \Psr\Log\LogLevel::INFO,
        'debug' => \Psr\Log\LogLevel::DEBUG,
    ];

    /**
     * System is unusable.
     *
     * @param string $message
     * @param array  $context
     *
     * @return void
     */
    public function emergency($message, array $context = [])
    {
        $this->log(\Psr\Log\LogLevel::EMERGENCY, $message, $context);
    }

    /**
     * Action must be taken immediately.
     *
     * Example: Entire website down, database unavailable, etc. This should
     * trigger the SMS alerts and wake you up.
     *
     * @param string $message
     * @param array  $context
     *
     * @return void
     */
    public function alert($message, array $context = [])
    {
        $this->log(\Psr\Log\LogLevel::ALERT, $message, $context);
    }

    /**
     * Critical conditions.
     *
     * Example: Application component unavailable, unexpected exception.
     *
     * @param string $message
     * @param array  $context
     *
     * @return void
     */
    public function critical($message, array $context = [])
    {
        $this->log(\Psr\Log\LogLevel::CRITICAL, $message, $context);
    }

    /**
     * Runtime errors that do not require immediate action but should typically
     * be logged and monitored.
     *
     * @param string $message
     * @param array  $context
     *
     * @return void
     */
    public function error($message, array $context = [])
    {
        $this->log(\Psr\Log\LogLevel::ERROR, $message, $context);
    }

    /**
     * Exceptional occurrences that are not errors.
     *
     * Example: Use of deprecated APIs, poor use of an API, undesirable things
     * that are not necessarily wrong.
     *
     * @param string $message
     * @param array  $context
     *
     * @return void
     */
    public function warning($message, array $context = [])
    {
        $this->log(\Psr\Log\LogLevel::WARNING, $message, $context);
    }

    /**
     * Normal but significant events.
     *
     * @param string $message
     * @param array  $context
     *
     * @return void
     */
    public function notice($message, array $context = [])
    {
        $this->log(\Psr\Log\LogLevel::NOTICE, $message, $context);
    }

    /**
     * Interesting events.
     *
     * Example: User logs in, SQL logs.
     *
     * @param string $message
     * @param array  $context
     *
     * @return void
     */
    public function info($message, array $context = [])
    {
        $this->log(\Psr\Log\LogLevel::INFO, $message, $context);
    }

    /**
     * Detailed debug information.
     *
     * @param string $message
     * @param array  $context
     *
     * @return void
     */
    public function debug($message, array $context = [])
    {
        $this->log(\Psr\Log\LogLevel::DEBUG, $message, $context);
    }

    /**
     * Logs with an arbitrary level.
     *
     * @param mixed  $level
     * @param string $message
     * @param array  $context
     *
     * @return void
     *
     * @throws \Psr\Log\InvalidArgumentException
     */
    public function log($level, $message, array $context = [])
    {
        if ($level === \Psr\Log\LogLevel::DEBUG && !$this::app()->debugEnabled())
        {
            // No debug logging as per app settings
            return;
        }
        $this->handleLogMessage($level, $this->interpolateLogMessage($message, $context), $context);
    }
    
    /**
     * Override this method to handle a log event.
     * This method will be handed the interpolated message, log level, and context.
     * 
     * Interpolation of context will have already been
     * applied when it reaches this method.
     * 
     * @param string $level
     * @param string $message
     * @return void
     */
    protected function handleLogMessage(string $level, string $message, array $context = []): void
    {
//        s(get_class($this), $level, $message, $this->getFlags());
        // no-op
    }
    
    
    /**
     * Performs interpolation of context on a log message
     * 
     * @param type $message
     * @param array $context
     * @return string
     */
    protected function interpolateLogMessage($message, array $context = []): string
    {
        foreach ($context as $key => $value) {
            $context[$key] = sprintf('[%1$s]', $value);
        }
        $formatted = \Oroboros\core\library\formatter\StringFormatter::interpolate($message, $context);
        return str_replace('[]', null, $formatted);
    }

    /**
     * 
     * 
     * @param type $level
     * @throws \Oroboros\core\exception\log\InvalidArgumentException
     *         If an invalid log level is passed. This is just an extension
     *         of the Psr-3 invalid argument exception.
     */
    private function verifyLogLevel($level)
    {
        if (!is_string($level) || !array_key_exists($level, self::$valid_log_levels)) {
            throw new \Oroboros\core\exception\log\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Invalid log level '
                        . '[%2$s] supplied. Valid log levels are [%3$s]',
                        get_class($this), ((is_null($level) || is_scalar($level)) ? $level : gettype($level)),
                        implode(', ', array_keys(self::$valid_log_levels)))
            );
        }
    }
}
