<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\library\session;

/**
 * Session Common Trait
 * Common methods for all session constructs
 * 
 * @satisfies \Oroboros\core\interfaces\library\session\SessionCommonInterface
 * @see \Oroboros\core\interfaces\library\session\SessionCommonInterface
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
trait SessionCommonTrait
{
    
    /**
     * Returns the session type designated by the class.
     * 
     * @return string|null
     */
    public function getSessionType(): string
    {
        return static::SESSION_TYPE;
    }
    
    /**
     * Returns the session scope designated by the class if it is declared,
     * or null if it is not.
     * 
     * @return string|null
     */
    public function getSessionScope(): ?string
    {
        return static::SESSION_SCOPE;
    }

    private function initializeSessionCommon(): void
    {
       // no-op. Placeholder for further extension. 
    }

    /**
     * Verifies session common functionality
     * 
     * @return void
     * @throws \Oroboros\core\exception\core\InvalidClassException
     *         If the class constants `SESSION_TYPE` or `SESSION_SCOPE`
     *         are not defined, or are declared as any value other
     *         than `null` or a string.
     */
    private function verifySessionCommon(): void
    {
        $this->verifySessionType();
        $this->verifySessionScope();
    }

    /**
     * Verifies that session type is properly declared
     * 
     * `const SESSION_TYPE` must be a string.
     * 
     * @return void
     * @throws \Oroboros\core\exception\core\InvalidClassException
     */
    private function verifySessionType(): void
    {
        $const_name = 'SESSION_TYPE';
        $const = sprintf('%1$s::%2$s', get_class($this), $const_name);
        if (!defined($const)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. Expected class constant '
                        . '[%2$s] is not defined. This class is not useable in '
                        . 'it\'s current state.', get_class($this), $const_name)
            );
        }
        $const_val = constant($const);
        if (!is_string($const_val)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. Expected class constant '
                        . '[%2$s] is not improperly declared. Expected a string '
                        . 'or null, value of type [%3$s] is declared. This class '
                        . 'is not useable in it\'s current state.'
                        , get_class($this), $const_name, gettype($const_val))
            );
        }
    }

    /**
     * Verifies that session scope is properly declared or null
     * 
     * @return void
     * @throws \Oroboros\core\exception\core\InvalidClassException
     */
    private function verifySessionScope(): void
    {
        $const_name = 'SESSION_SCOPE';
        $const = sprintf('%1$s::%2$s', get_class($this), $const_name);
        if (!defined($const)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. Expected class constant '
                        . '[%2$s] is not defined. This class is not useable in '
                        . 'it\'s current state.', get_class($this), $const_name)
            );
        }
        $const_val = constant($const);
        if (is_null($const_val)) {
            return;
        }
        if (!is_string($const_val)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. Expected class constant '
                        . '[%2$s] is not improperly declared. Expected a string '
                        . 'or null, value of type [%3$s] is declared. This class '
                        . 'is not useable in it\'s current state.'
                        , get_class($this), $const_name, gettype($const_val))
            );
        }
    }
}
