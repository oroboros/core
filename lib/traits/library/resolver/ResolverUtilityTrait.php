<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\library\resolver;

/**
 * Resolver Utility Trait
 * Provides a simple internal api to automatically generate a resolver
 * for the class to use to resolve complex argument criteria
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
trait ResolverUtilityTrait
{

    /**
     * The resolver used to validate passed parameters
     * 
     * @var \Oroboros\core\interfaces\library\resolver\ResolverInterface
     */
    private static $resolver = null;

    /**
     * Returns the resolver used to validate parameters declarations
     * 
     * @return \Oroboros\core\interfaces\library\resolver\ResolverInterface
     */
    protected function getResolver(): \Oroboros\core\interfaces\library\resolver\ResolverInterface
    {
        $this->initializeResolver();
        return self::$resolver;
    }

    /**
     * Initializes the resolver used for parameter validation
     * 
     * @return void
     */
    private function initializeResolver(): void
    {
        if (is_null(self::$resolver)) {
            $class = 'resolver\\ArgumentResolver';
            $def = sprintf('%1$s::%2$s', get_class($this), 'RESOLVER_CLASS');
            if (defined($def) && !is_null(constant($def))) {
                $class = $this->getDeclaredResolverClass();
            }
            $resolver = $this->load('library', $class, $this->getCommand(), $this->getArguments(), $this->getFlags());
            self::$resolver = $resolver;
        }
    }

    /**
     * Gets the declared resolver class from the class constant
     */
    private function getDeclaredResolverClass(): string
    {
        $def = sprintf('%1$s::%2$s', get_class($this), 'RESOLVER_CLASS');
        $this->verifyResolverClass(constant($def));
        return constant($def);
    }

    /**
     * Verifies the declared resolver class
     * 
     * @param string $class
     * @throws \Oroboros\core\exception\core\InvalidClassException
     */
    private function verifyResolverClass(string $class)
    {
        if (!is_string($class)) {
            // not a string
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. Expected resolver value '
                        . 'must be of type [%2$s], received type [%3$s] for '
                        . 'expected class constant [%4$s]. This class is not '
                        . 'useable in it\'s current state.'
                        , get_class($this), 'string', gettype($class), 'RESOLVER_CLASS')
            );
        }
        $expected = \Oroboros\core\interfaces\library\resolver\ResolverInterface::class;
        $proxy = new \Oroboros\core\pattern\factory\LoaderProxy();
        if ($proxy->getFullClassName('library', $class) === false || (!in_array($expected, class_implements($proxy->getFullClassName('library', $class))))) {
            // not a valid class
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. Expected resolver value '
                        . 'for class constant with key [%2$s] and declared value [%3$s]'
                        . 'must be a string class name or stub class name resolving '
                        . 'to a class implementing expected interface [%4$s] '
                        . 'This class is not useable in it\'s current state.'
                        , get_class($this), 'string', gettype($class), 'RESOLVER_CLASS')
            );
        }
    }
}
