<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\library\entity;

/**
 * Entity Common Trait
 * 
 * @satisfies \Oroboros\core\interfaces\library\entity\EntityCommonInterface
 * @see \Oroboros\core\interfaces\library\entity\EntityCommonInterface
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
trait EntityCommonTrait
{

    /**
     * Entities use the standard constructor
     * 
     * Constructor will raise an exception if any entity class constants are malformed
     * 
     * @param string $command
     * @param array $arguments
     * @param array $flags
     * @throws \Oroboros\core\exception\core\InvalidClassException
     */
    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        $this->verifyEntity();
        parent::__construct($command, $arguments, $flags);
        $this->initializeEntity();
    }

    /**
     * Returns the entity type
     * 
     * @return string
     */
    public function getEntityType(): string
    {
        return $this->declareEntityType();
    }

    /**
     * Returns the entity scope, or null if there is no entity scope
     * 
     * @return string|null
     */
    public function getEntityScope(): ?string
    {
        return $this->declareEntityScope();
    }

    /**
     * Returns the entity category, or null if there is no entity category
     * 
     * @return string|null
     */
    public function getEntityCategory(): ?string
    {
        return $this->declareEntityCategory();
    }

    /**
     * Returns the entity subcategory, or null if there is no entity subcategory
     * 
     * @return string|null
     */
    public function getEntitySubCategory(): ?string
    {
        return $this->declareEntitySubCategory();
    }

    /**
     * Returns the entity context, or null if there is no entity context
     * 
     * @return string|null
     */
    public function getEntityContext(): ?string
    {
        return $this->declareEntityContext();
    }

    /**
     * Declares the entity type
     * 
     * The default behavior returns the value of the class constant `ENTITY_TYPE`
     * You may override this method if you wish to use a different
     * definition method
     * 
     * @return string
     */
    protected function declareEntityType(): string
    {
        return static::ENTITY_TYPE;
    }

    /**
     * Declares the entity scope
     * 
     * The default behavior returns the value of the class constant `ENTITY_SCOPE`
     * You may override this method if you wish to use a different
     * definition method
     * 
     * @return string|null
     */
    protected function declareEntityScope(): ?string
    {
        return static::ENTITY_SCOPE;
    }

    /**
     * Declares the entity category
     * 
     * The default behavior returns the value of the class constant `ENTITY_CATEGORY`
     * You may override this method if you wish to use a different
     * definition method
     * 
     * @return string|null
     */
    protected function declareEntityCategory(): ?string
    {
        return static::ENTITY_CATEGORY;
    }

    /**
     * Declares the entity subcategory
     * 
     * The default behavior returns the value of the class constant `ENTITY_SUBCATEGORY`
     * You may override this method if you wish to use a different
     * definition method
     * 
     * @return string|null
     */
    protected function declareEntitySubCategory(): ?string
    {
        return static::ENTITY_SUBCATEGORY;
    }

    /**
     * Declares the entity context
     * 
     * The default behavior returns the value of the class constant `ENTITY_CONTEXT`
     * You may override this method if you wish to use a different
     * definition method
     * 
     * @return string|null
     */
    protected function declareEntityContext(): ?string
    {
        return static::ENTITY_CONTEXT;
    }

    /**
     * Override this method to perform any
     * additional entity initialization tasks
     * 
     * @return void
     */
    protected function initializeEntity(): void
    {
        // no-op
    }

    /**
     * Verifies that the entity class constants are valid for the entity api
     * 
     * @return void
     * @throws \Oroboros\core\exception\core\InvalidClassException
     */
    private function verifyEntity(): void
    {
        $this->verifyEntityType();
        $this->verifyEntityScope();
        $this->verifyEntityCategory();
        $this->verifyEntitySubCategory();
        $this->verifyEntityContext();
    }

    /**
     * Verifies that the entity type is a string
     * 
     * This is the only mandatory entity constant
     * 
     * @return void
     * @throws \Oroboros\core\exception\core\InvalidClassException
     */
    private function verifyEntityType(): void
    {
        if (!is_string(static::ENTITY_TYPE)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. Required class '
                        . 'constant [%2$s] is not defined as a valid value. '
                        . 'This class is not useable in it\'s current state.'
                        , get_Class($this), 'ENTITY_TYPE')
            );
        }
    }

    /**
     * Verifies that the entity scope is either a string or null
     * 
     * @return void
     * @throws \Oroboros\core\exception\core\InvalidClassException
     */
    private function verifyEntityScope(): void
    {
        if (!is_null(static::ENTITY_SCOPE) && !is_string(static::ENTITY_SCOPE)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. Class '
                        . 'constant [%2$s] is not defined as a valid value. '
                        . 'Expected string or null. This class is not useable '
                        . 'in it\'s current state.'
                        , get_Class($this), 'ENTITY_SCOPE')
            );
        }
    }

    /**
     * Verifies that the entity category is either a string or null
     * 
     * @return void
     * @throws \Oroboros\core\exception\core\InvalidClassException
     */
    private function verifyEntityCategory(): void
    {
        if (!is_null(static::ENTITY_CATEGORY) && !is_string(static::ENTITY_CATEGORY)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. Class '
                        . 'constant [%2$s] is not defined as a valid value. '
                        . 'Expected string or null. This class is not useable '
                        . 'in it\'s current state.'
                        , get_Class($this), 'ENTITY_CATEGORY')
            );
        }
    }

    /**
     * Verifies that the entity subcategory is either a string or null
     * 
     * @return void
     * @throws \Oroboros\core\exception\core\InvalidClassException
     */
    private function verifyEntitySubCategory(): void
    {
        if (!is_null(static::ENTITY_SUBCATEGORY) && !is_string(static::ENTITY_SUBCATEGORY)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. Class '
                        . 'constant [%2$s] is not defined as a valid value. '
                        . 'Expected string or null. This class is not useable '
                        . 'in it\'s current state.'
                        , get_Class($this), 'ENTITY_SUBCATEGORY')
            );
        }
    }

    /**
     * Verifies that the entity context is either a string or null
     * 
     * @return void
     * @throws \Oroboros\core\exception\core\InvalidClassException
     */
    private function verifyEntityContext(): void
    {
        if (!is_null(static::ENTITY_CONTEXT) && !is_string(static::ENTITY_CONTEXT)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. Class '
                        . 'constant [%2$s] is not defined as a valid value. '
                        . 'Expected string or null. This class is not useable '
                        . 'in it\'s current state.'
                        , get_Class($this), 'ENTITY_CONTEXT')
            );
        }
    }
}
