<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Identity is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this identity notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\library\auth;

/**
 * Identity Group Aware Trait
 * Provides the methods required for an object to be able to resolve
 * identity group assignment
 * 
 * @satisfies \Oroboros\core\interfaces\library\auth\IdentityGroupAwareInterface
 * @see \Oroboros\core\interfaces\library\auth\IdentityGroupAwareInterface
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
trait IdentityGroupAwareTrait
{

    /**
     * The identity group manager used to determine
     * the correct identity group(s) for the scoped identity and identity group(s)
     * 
     * @var \Oroboros\core\interfaces\library\auth\IdentityGroupManagerInterface
     */
    private $identity_group_manager = null;

    /**
     * Internal getter for the identity group manager
     * 
     * @return \Oroboros\core\interfaces\library\auth\IdentityGroupManagerInterface
     */
    protected function getIdentityGroupManager(): \Oroboros\core\interfaces\library\auth\IdentityGroupManagerInterface
    {
        return $this->identity_group_manager;
    }

    /**
     * Verifies that the current object has the correct configuration
     * for identity group aware functionality
     * 
     * @return void
     * @throws \Oroboros\core\exception\core\InvalidClassException
     */
    private function verifyIdentityGroupAware(): void
    {
        
    }

    /**
     * Initializes the identity group aware functionality
     * 
     * @return void
     */
    private function initializeIdentityGroupAware(): void
    {
        $this->initializeIdentityGroupManager();
        $this->initializeIdentityGroupManager();
    }

    /**
     * Initializes the identity group manager
     * 
     * @return void
     */
    private function initializeIdentityGroupManager(): void
    {
        if (is_null($this->identity_group_manager)) {
            $default = \Oroboros\core\library\auth\IdentityGroupManager::class;
            $manager = $this->load('library', $default, $this->getCommand(), $this->getArguments(), $this->getFlags());
            $this->identity_group_manager = $manager;
        }
    }
}
