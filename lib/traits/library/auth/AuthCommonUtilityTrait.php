<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\library\auth;

/**
 * Auth Common Utility Trait
 * Provides common methods all auth api objects have, as per the AuthInterface
 *
 * @see \Oroboros\core\interfaces\library\auth\AuthInterface
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
trait AuthCommonUtilityTrait
{

    /**
     * Returns the authentication scope of the current object.
     * 
     * @return string
     */
    public static function getAuthScope(): string
    {
        return static::AUTH_SCOPE;
    }

    /**
     * Initializes common methods shared by all auth objects
     * 
     * @return void
     */
    private function initializeAuthCommon(): void
    {
        $this->verifyAuthScope();
    }

    /**
     * Verifies that the current class has properly declared it's auth scope
     * 
     * @return void
     * @throws \Oroboros\core\exception\core\InvalidClassException
     */
    private function verifyAuthScope(): void
    {
        if (is_null(static::AUTH_SCOPE)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. Expected class constant '
                        . '[%2$s] is not defined. This class is not useable in '
                        . 'it\'s current state.', get_class($this), 'AUTH_SCOPE')
            );
        }
    }

    /**
     * Performs verification of class constant values
     * 
     * @param string $constant
     * @param string $expected
     * @param bool $required
     * @return void
     */
    private function preflightConstant(string $constant, string $expected, bool $required = false): void
    {
        $value = constant(sprintf('%1$s::%2$s', get_class($this), $constant));
        if (is_null($value) && !$required) {
            return;
        }
        if (is_null($value)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. Expected class constant '
                        . '[%2$s] is not defined.', get_class($this), $constant)
            );
        }
        $class = $this->getFullClassName('library', $value);
        if ($class === false) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. Class constant [%2$s] '
                        . 'with value [%3$s] must define a valid class or stub '
                        . 'class name implementing interface [%4$s].'
                        , get_class($this), $constant, $value, $expected)
            );
        }
        if (!in_array($expected, class_implements($class), true)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. Class constant [%2$s] '
                        . 'with value [%3$s] exists as a class but does not resolve to a '
                        . 'class name implementing interface [%4$s].'
                        , get_class($this), $constant, $value, $expected)
            );
        }
    }
}
