<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\library\auth;

/**
 * Permission Group Aware Trait
 * Provides the methods required for an object to be able to resolve
 * permission group assignment
 * 
 * @satisfies \Oroboros\core\interfaces\library\auth\PermissionGroupAwareInterface
 * @see \Oroboros\core\interfaces\library\auth\PermissionGroupAwareInterface
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
trait PermissionGroupAwareTrait
{

    /**
     * A container of all permission groups associated with the scoped identity
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface 
     */
    private $permission_groups = null;

    /**
     * The permission group manager used to determine
     * the correct permission group(s) for the scoped identity and identity group(s)
     * 
     * @var \Oroboros\core\interfaces\library\auth\IdentityGroupManagerInterface
     */
    private $permission_group_manager = null;

    /**
     * The identity group assigned to the scoped subject.
     * If the identity is not valid, the default public group will be used.
     * @var string
     */
    private $identity_group = null;

    /**
     * The identity group manager used to determine
     * the correct identity group for the scoped identity
     * 
     * @var \Oroboros\core\interfaces\library\auth\IdentityGroupManagerInterface
     */
    private $identity_group_manager = null;

    /**
     * Internal getter for the identity group manager
     * 
     * @return \Oroboros\core\interfaces\library\auth\IdentityGroupManagerInterface
     */
    protected function getPermissionGroupManager(): \Oroboros\core\interfaces\library\auth\PermissionGroupManagerInterface
    {
        return $this->permission_group_manager;
    }

    /**
     * Internal getter for the identity group manager
     * 
     * @return \Oroboros\core\interfaces\library\auth\IdentityGroupManagerInterface
     */
    protected function getIdentityGroupManager(): \Oroboros\core\interfaces\library\auth\IdentityGroupManagerInterface
    {
        return $this->identity_group_manager;
    }
    
    /**
     * Verifies that the current object has the correct configuration
     * for permission group aware functionality
     * 
     * @return void
     * @throws \Oroboros\core\exception\core\InvalidClassException
     */
    private function verifyPermissionGroupAware(): void
    {
        
    }

    /**
     * Initializes the permission group aware functionality
     * 
     * @return void
     */
    private function initializePermissionGroupAware(): void
    {
        $this->initializeIdentityGroupManager();
        $this->initializePermissionGroupManager();
    }

    /**
     * Initializes the permission group manager
     * 
     * @return void
     */
    private function initializePermissionGroupManager(): void
    {
        if (is_null($this->permission_group_manager)) {
            $default = \Oroboros\core\library\auth\PermissionGroupManager::class;
            $manager = $this->load('library', $default, $this->getCommand(), $this->getArguments(), $this->getFlags());
            $this->permission_group_manager = $manager;
        }
    }

    /**
     * Initializes the identity group manager
     * 
     * @return void
     */
    private function initializeIdentityGroupManager(): void
    {
        if (is_null($this->identity_group_manager)) {
            $default = \Oroboros\core\library\auth\IdentityManager::class;
            $manager = $this->load('library', $default, $this->getCommand(), $this->getArguments(), $this->getFlags());
            $this->identity_group_manager = $manager;
        }
    }
}
