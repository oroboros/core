<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\library\auth;

/**
 * Permission Aware Trait
 * Provides the methods required for an object to be able to authenticate
 * with the permission api
 * 
 * @satisfies \Oroboros\core\interfaces\library\auth\PermissionAwareInterface
 * @see \Oroboros\core\interfaces\library\auth\PermissionAwareInterface
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
trait PermissionAwareTrait
{

    /**
     * The permission manager object used to fetch permissions
     * and authenticate against them.
     * 
     * @var \Oroboros\core\interfaces\library\auth\PermissionManagerInterface
     */
    private $permission_manager = null;

    /**
     * Returns a boolean determination as to whether the scoped identity
     * has access to a corresponding permission node.
     * @param string $permission
     * @return bool
     */
    public function can(string $permission): bool
    {
        return $this->permissions->check($permission, $this);
    }
    
    /**
     * Verifies that the current object has the correct configuration
     * for permission group aware functionality
     * 
     * @return void
     * @throws \Oroboros\core\exception\core\InvalidClassException
     */
    private function verifyPermissionAware(): void
    {
        
    }

    private function initializePermissionAware(): void
    {
        $this->initializePermissionManager();
    }

    private function initializePermissionManager(): void
    {
        
    }
}
