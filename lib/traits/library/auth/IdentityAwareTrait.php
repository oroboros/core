<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Identity is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this identity notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\library\auth;

/**
 * Identity Aware Trait
 * Provides the methods required for an object to be derive it's identity
 * or confirm that a given identity belongs to it
 * 
 * @satisfies \Oroboros\core\interfaces\library\auth\IdentityAwareInterface
 * @see \Oroboros\core\interfaces\library\auth\IdentityAwareInterface
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
trait IdentityAwareTrait
{

    /**
     * The identity manager object used to fetch identitys
     * and authenticate against them.
     * 
     * @var \Oroboros\core\interfaces\library\auth\IdentityManagerInterface
     */
    private $identity_manager = null;

    /**
     * Returns a boolean determination as to whether the scoped identity
     * has access to a corresponding identity node.
     * @param string $identity
     * @return bool
     */
    public function can(string $identity): bool
    {
        return $this->identitys->check($identity, $this);
    }
    
    /**
     * Verifies that the current object has the correct configuration
     * for identity aware functionality
     * 
     * @return void
     * @throws \Oroboros\core\exception\core\InvalidClassException
     */
    private function verifyIdentityAware(): void
    {
        
    }

    /**
     * Initializes identity aware functionality
     * 
     * @return void
     */
    private function initializeIdentityAware(): void
    {
        $this->initializeIdentityManager();
    }

    /**
     * Initializes the identity manager
     * 
     * @return void
     */
    private function initializeIdentityManager(): void
    {
        if (is_null($this->identity_manager)) {
            $default = \Oroboros\core\library\auth\IdentityManager::class;
            $manager = $this->load('library', $default, $this->getCommand(), $this->getArguments(), $this->getFlags());
            $this->identity_manager = $manager;
        }
    }
}
