<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\router;

/**
 * Common utilities for http route objects
 *
 * @author Brian Dayhoff
 */
trait HttpRouteUtilityTrait
{

    use RouteUtilityTrait;

    /**
     * The domain (without subdomain) parsed from the request
     * @var string
     */
    private $domain = null;

    /**
     * The subdomain (without domain) parsed from the request
     * @var string
     */
    private $subdomain = '';

    /**
     * The request type as determined from the request
     * (eg: html, ajax, etc)
     * @var string
     */
    private $type = 'html';

    public function __construct(string $command = null, array $arguments = null, array $flags = null)
    {
        parent::__construct($command, $arguments, $flags);
        $this->initializeArguments();
    }

    public function setSubdomain(string $subdomain)
    {
        $this->subdomain = $subdomain;
    }

    public function setDomain(string $domain)
    {
        $this->domain = $domain;
    }

    public function setRequestType(string $type)
    {
        $this->type = $type;
    }

    public function getMatchCriteria(\Psr\Http\Message\ServerRequestInterface $request)
    {
        return [
            'permission' => $this->verifyPermission(),
            'sameorigin' => $this->verifySameorigin($request),
            'network' => $this->verifyNetwork($request),
            'type' => $this->verifyRequestType($request),
            'subdomain' => $this->verifySubdomain(),
            'domain' => $this->verifyDomain(),
            'designation' => parent::match($request),
            'internal' => $this->verifyInternal(),
        ];
    }

    public function match(\Psr\Http\Message\ServerRequestInterface $request)
    {
        $conditions = $this->getMatchCriteria($request);
        return (in_array(false, $conditions) ? false : true);
    }

    /**
     * Overridden to supply the user, request type, domain, and subdomain to the controller
     * @param string $command (optional)
     * @param array $arguments (optional)
     * @param array $flags (optional)
     * @return \Oroboros\core\interfaces\controller\ControllerInterface
     */
    public function getController(string $command = null, array $arguments = null, array $flags = null)
    {
        if (is_null($arguments)) {
            $arguments = [];
        }
        $arguments['user'] = $this->user;
        $arguments['type'] = $this->getArgument('type');
        $arguments['subdomain'] = $this->getArgument('subdomain');
        $arguments['domain'] = $this->getArgument('domain');
        return parent::getController($command, $arguments, $flags);
    }

    /**
     * Returns the permission node associated with the route
     * @return string
     */
    public function getPermission(): string
    {
        return $this->getArgument('permission');
    }

    /**
     * Returns the subdomain associated with the route
     * @return string
     */
    public function getSubdomain(): string
    {
        return $this->getArgument('subdomain');
    }

    /**
     * Returns the domain associated with the route
     * @return string
     */
    public function getDomain(): string
    {
        return $this->getArgument('domain');
    }

    /**
     * Returns the request type associated with the route
     * @return string
     */
    public function getRequestType(): string
    {
        return $this->getArgument('type');
    }

    /**
     * Returns the scope associated with the route
     * @return string
     */
    public function getScope(): string
    {
        $fullclass = $this->getControllerClassName();
        return $fullclass::CLASS_SCOPE;
    }

    /**
     * Returns whether the supplied controller scope matches the supplied scope
     * If the supplied scope is wildcarded ('*'), also returns true
     * @param string $scope
     * @return bool
     */
    public function hasScope(string $scope): bool
    {
        return $this->getScope() === $scope || $scope === '*';
    }

    /**
     * Returns the fully qualified class name of the controller associated with the route
     * @return string
     */
    public function getControllerClassName(): string
    {
        return $this->getFullClassName('controller', $this->getArgument('controller'));
    }

    /**
     * Returns whether a given subdomain is a direct match, without regard to domain.
     * If either the listed domain for this route or the provided subdomain argument === '*',
     * it will also match as true.
     * @param string $domain
     * @return bool
     */
    public function hasSubdomain(string $subdomain): bool
    {
        return $this->getArgument('subdomain') === $subdomain || $this->getArgument('subdomain') === '*' || $subdomain === '*';
    }

    /**
     * Returns whether a given domain is a direct match, without regard to subdomain.
     * If either the listed domain for this route or the provided domain argument === '*',
     * it will also match as true.
     * @param string $domain
     * @return bool
     */
    public function hasDomain(string $domain): bool
    {
        return $this->getArgument('domain') === $domain || $this->getArgument('domain') === '*' || $domain === '*';
    }

    /**
     * Returns whether a given route type is a direct match.
     * If either the listed route type for this route or the provided type argument === '*',
     * it will also match as true.
     * @param string $domain
     * @return bool
     */
    public function isType(string $type): bool
    {
        return $this->getArgument('type') === '*' || $this->getArgument('type') === $type || $type === '*';
    }

    /**
     * Returns whether the current route is flagged as internal only
     * If this is true, the route will only return true if the current user
     * is part of the core organization.
     * @return bool
     */
    public function isInternal(): bool
    {
        return ((is_null($this->getArgument('internal'))) ? false : $this->getArgument('internal'));
    }

    /**
     * Returns whether the current route is flagged as network only.
     * If this is true, the domain must either match or be wildcarded,
     * though the subdomain does not explicitly require matching.
     * @return bool
     */
    public function isNetwork(): bool
    {
        return ((is_null($this->getArgument('network'))) ? true : $this->getArgument('network'));
    }

    /**
     * Returns whether the current route is flagged as only accessible
     * from the same subdomain/domain pairing or explicit wildcard subdomain.
     * If this returns true, matches will only pass if they have
     * identical domain/subdomain, or if listed domain or subdomain
     * is wildcarded and the other matches.
     * @return bool
     */
    public function isSameorigin(): bool
    {
        return ((is_null($this->getArgument('network'))) ? true : $this->getArgument('network'));
    }

    /**
     * Returns whether the current route is flagged as public
     * @return bool
     */
    public function isPublic(): bool
    {
        return $this->getArgument('public');
    }

    private function verifyRequestType(\Psr\Http\Message\ServerRequestInterface $request)
    {
        $request_type = ($request->hasHeader('X-Requested-With') && strtolower($request->getHeaderLine('X-Requested-With')) === 'xmlhttprequest') ? 'ajax' : 'html';
        return $this->isType($request_type);
    }

    private function verifyPermission()
    {
        return ((is_null($this->getArgument('permission'))) ? true : $this->user->can($this->getArgument('permission')));
    }

    private function verifySameorigin(\Psr\Http\Message\ServerRequestInterface $request)
    {
        if (!$this->isSameorigin()) {
            return true;
        }
        return $this->verifySubdomain($request) && $this->verifyDomain($request);
    }

    private function verifyNetwork(\Psr\Http\Message\ServerRequestInterface $request)
    {
        if (!$this->isNetwork()) {
            return true;
        }
        return $this->verifyDomain($request);
    }

    private function verifySubdomain()
    {
        return $this->hasSubdomain($this->subdomain);
    }

    private function verifyDomain()
    {
        return $this->hasDomain($this->domain);
    }

    private function verifyInternal()
    {
        if (!$this->isInternal()) {
            return true;
        }
        return $this->user->can('internal.access');
    }

    private function initializeArguments(): void
    {
        if ($this->hasArgument('user')) {
            $this->setUser($this->getArgument('user'));
        }
        if ($this->hasArgument('domain')) {
            $this->setDomain($this->getArgument('domain'));
        }
        if ($this->hasArgument('subdomain')) {
            $this->setSubdomain($this->getArgument('subdomain'));
        }
        if ($this->hasArgument('type')) {
            $this->setRequestType($this->getArgument('type'));
        }
        if ($this->hasArgument('request')) {
            $this->match($this->getArgument('request'));
        }
    }
}
