<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\job;

/**
 * Description of JobQueueUtilityTrait
 *
 * @author Brian Dayhoff
 */
trait JobQueueUtilityTrait
{

    private static $job_strategy = null;

    private function listJobQueues(): \Oroboros\core\interfaces\library\job\JobQueueIndexInterface
    {
        
    }

    private function getJobQueue(): \Oroboros\core\interfaces\library\job\JobManagerInterface
    {
        
    }

    protected function getJob(string $queue, string $name)
    {
        
    }

    protected function registerJob(string $queue, string $name, \Oroboros\core\interfaces\library\container\ContainerInterface $data = null)
    {
        
    }

    protected function unregisterJob(string $queue, string $name)
    {
        
    }

    protected function checkJob(string $queue, string $name)
    {
        
    }

    protected function startJob(string $queue, string $name)
    {
        
    }

    protected function killJob(string $queue, string $name)
    {
        
    }

    private function initializeJobQueues()
    {
        if (is_null(self::$job_strategy)) {
            self::$job_strategy = $this->load('library', 'job\\JobManagerStrategy', self::CLASS_TYPE);
            $this->getLogger()->debug('[type][scope][class] Job queue initialization complete.', [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE
            ]);
        }
    }
}
