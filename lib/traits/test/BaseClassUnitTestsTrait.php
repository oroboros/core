<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\test;

/**
 * Base Class Unit Tests Trait
 * Provides PHPUnit test cases for all
 * children of `\Oroboros\core\abstracts\AbstractBase`,
 * which is pretty much everything except one off
 * adapter subjects or third party class wrappers
 *
 * @coversDefaultClass \Oroboros\core\abstracts\AbstractBase
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
trait BaseClassUnitTestsTrait
{
    /**
     * Tests the constructor for the test subject
     * 
     * @covers ::__construct
     * @covers ::__destruct
     * @group initialization
     * @return void
     */
    public function testConstructor(): void
    {
        try {
            $this->resetCurrentTestSubject();
            $this->assertNull($this->getCurrentTestSubject(), sprintf('[ test scope: %1$s ] Test subject [%2$s] failed to unset.\n', $this->getTestScope(), $this->declareTestSubjectClass()));
            $this->instantiateTestSubject();
            $this->assertInstanceOf($this->declareExpectedInterface(), $this->getCurrentTestSubject(), sprintf('[ test scope: %1$s ] Test subject [%2$s] failed to reinstantiate as instanceof expected [%3$s].', $this->getTestScope(), $this->declareTestSubjectClass(), $this->declareExpectedInterface()));
        } catch (\Exception $e) {
            $this->fail(sprintf('Test subject instantiation failed with '
                    . 'exception type [%2$s], and message [%3$s]'
                    , $this->getTestScope(), get_class($e), $e->getMessage()));
        }
    }
    
    /**
     * Tests whether the defined subject implements the expected interface definition.
     * 
     * @group interfaces
     * @return void
     */
    public function testInterface(): void
    {
        $this->assertInstanceOf($this->declareExpectedInterface(), $this->getCurrentTestSubject(), sprintf('[ test scope: %1$s ] Test subject [%2$s] is not an instanceof expected [%3$s].', $this->getTestScope(), $this->declareTestSubjectClass(), $this->declareExpectedInterface()));
    }
}
