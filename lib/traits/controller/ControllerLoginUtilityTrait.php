<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\controller;

/**
 * Controller Login Utility Trait
 * Provides common methods for login controllers to authenticate users
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
trait ControllerLoginUtilityTrait
{

    use ControllerFormUtilityTrait;

    /**
     * Represents the authentication manager for the declared login scope.
     * 
     * @var \Oroboros\core\interfaces\library\auth\AuthManagerInterface
     */
    private $auth_manager = null;

    /**
     * Handles login authentication presentation
     * 
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $args
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $flags
     * @return \Oroboros\core\interfaces\controller\ControllerInterface
     */
    public function login(\Oroboros\core\interfaces\library\container\ContainerInterface $args = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\ControllerInterface
    {
        switch (strtolower($this->getArgument('request')->getMethod())) {
            case 'cli':
                $this->getLogger()->debug('[type][scope][class] Handling CLI login attempt.', [
                    'class' => get_class($this),
                    'type' => static::CLASS_TYPE,
                    'scope' => static::CLASS_SCOPE,
                ]);
                return $this->handleCliLogin($args, $flags);
            case 'get':
                $this->getLogger()->debug('[type][scope][class] Handling HTTP GET login attempt.', [
                    'class' => get_class($this),
                    'type' => static::CLASS_TYPE,
                    'scope' => static::CLASS_SCOPE,
                ]);
                return $this->handleHttpGetLogin($args, $flags);
            case 'post':
                $this->getLogger()->debug('[type][scope][class] Handling HTTP POST login attempt.', [
                    'class' => get_class($this),
                    'type' => static::CLASS_TYPE,
                    'scope' => static::CLASS_SCOPE,
                ]);
                return $this->handleHttpPostLogin($args, $flags);
            default:
                return $this->error(404, 'Invalid login request method.');
        }
    }

    /**
     * Handles logs out of the system
     * 
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $args
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $flags
     * @return \Oroboros\core\interfaces\controller\ControllerInterface
     */
    public function logout(\Oroboros\core\interfaces\library\container\ContainerInterface $args = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\ControllerInterface
    {
        $this->getLogger()->debug('[type][scope][class] Logging out user.', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
        ]);
        switch (strtolower($this->getArgument('request')->getMethod())) {
            case 'cli':
                return $this->handleCliLogout($args, $flags);
            case 'get':
                return $this->handleHttpLogout($args, $flags);
            default:
                return $this->error(404, 'Invalid logout request method.');
        }
    }

    /**
     * If the request to the login route resolves as being of type cli,
     * this method will handle the response.
     * 
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $args
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $flags
     * @return \Oroboros\core\interfaces\controller\ControllerInterface
     */
    protected function handleCliLogin(\Oroboros\core\interfaces\library\container\ContainerInterface $args = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\ControllerInterface
    {
        $this->verifyLoginReferer($this->getArgument('request'));
        return $this;
    }

    /**
     * If the request to the logout route resolves as being of type cli,
     * this method will handle the response.
     * 
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $args
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $flags
     * @return \Oroboros\core\interfaces\controller\ControllerInterface
     */
    protected function handleCliLogout(\Oroboros\core\interfaces\library\container\ContainerInterface $args = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\ControllerInterface
    {
        $this->verifyLoginReferer($this->getArgument('request'));
        return $this;
    }

    /**
     * If the request to the logout route resolves as being of type http GET,
     * this method will handle the response.
     * 
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $args
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $flags
     * @return \Oroboros\core\interfaces\controller\ControllerInterface
     */
    protected function handleHttpGetLogin(\Oroboros\core\interfaces\library\container\ContainerInterface $args = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\ControllerInterface
    {
        return $this;
    }

    /**
     * If the request to the logout route resolves as being of type http GET,
     * this method will handle the response.
     * 
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $args
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $flags
     * @return \Oroboros\core\interfaces\controller\ControllerInterface
     */
    protected function handleHttpLogout(\Oroboros\core\interfaces\library\container\ContainerInterface $args = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\ControllerInterface
    {
        return $this;
    }

    /**
     * If the request to the login route resolves as being of type http POST,
     * this method will handle the response.
     * 
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $args
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $flags
     * @return \Oroboros\core\interfaces\controller\ControllerInterface
     */
    protected function handleHttpPostLogin(\Oroboros\core\interfaces\library\container\ContainerInterface $args = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\ControllerInterface
    {
        $this->verifyLoginReferer($this->getArgument('request'));
        return $this;
    }

    /**
     * Performs authentication and validation of a submitted login attempt
     * 
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $args
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $flags
     * @return \Oroboros\core\interfaces\controller\ControllerInterface
     */
    public function verify(\Oroboros\core\interfaces\library\container\ContainerInterface $args = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\ControllerInterface
    {
        $this->verifyLoginReferer($this->getArgument('request'));
        return $this;
    }

    /**
     * Returns the auth manager suited to the current scope
     * 
     * @return \Oroboros\core\interfaces\library\auth\AuthManagerInterface
     */
    protected function getAuthManager(): \Oroboros\core\interfaces\library\auth\AuthManagerInterface
    {
        $this->initializeAuthManager();
        return $this->auth_manager;
    }

    /**
     * Verifies that the session auth token contains the correct value
     * 
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @return void
     * @throws \Oroboros\core\exception\auth\RefererException
     *         If the session auth token is not defined correctly
     */
    protected function verifyLoginAuthToken(\Psr\Http\Message\ServerRequestInterface $request): void
    {
        $this->getLogger()->debug('[type][scope][class] Verifying login auth token.', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
        ]);
        if (!$this->form_values->has('token')) {
            // Missing auth token in form.
            $this->getLogger()->info('[type][scope][class] Missing session auth token '
                . 'in login form. Login attempt was supressed.', [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
            ]);
            throw new \Oroboros\core\exception\auth\RefererException(
                    sprintf('Error encountered in [%1$s]. Missing session auth token '
                        . 'in login request.', get_class($this))
            );
        }
        if (!$this->form_values->token === (string) $this::user()->getAuthToken()) {
            // Auth token mismatch.
            $this->getLogger()->info('[type][scope][class] Auth token mismatch '
                . 'in login form. Login attempt was supressed.', [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
            ]);
            throw new \Oroboros\core\exception\auth\RefererException(
                    sprintf('Error encountered in [%1$s]. Auth token mismatch '
                        . 'in login request.', get_class($this))
            );
        }
    }

    /**
     * Verifies that the login form nonce contains the expected value
     * 
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @return void
     * @throws \Oroboros\core\exception\auth\RefererException
     *         If the login nonce is not correctly defined
     */
    protected function verifyLoginNonce(\Psr\Http\Message\ServerRequestInterface $request): void
    {
        $this->getLogger()->debug('[type][scope][class] Verifying login nonce.', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
        ]);
        $nonce_key = $this->getLoginNonceKey();
        if (!$this::user()->has($nonce_key) || $this::user()->isGhost()) {
            // Missing nonce or untrusted source
            $this->getLogger()->info('[type][scope][class] Missing nonce key '
                . '[login-nonce] in user session. Login attempt was supressed.', [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
                'login-nonce' => $nonce_key,
            ]);
            throw new \Oroboros\core\exception\auth\RefererException(
                    sprintf('Error encountered in [%1$s]. Missing nonce key [%2$s] '
                        . 'in login request.', get_class($this), $nonce_key)
            );
        }
        if (!$this->form_values->has('nonce')) {
            // Missing nonce in form
            $this->getLogger()->info('[type][scope][class] Missing nonce key '
                . '[login-nonce] in login form. Login attempt was supressed.', [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
                'login-nonce' => $nonce_key,
            ]);
            throw new \Oroboros\core\exception\auth\RefererException(
                    sprintf('Error encountered in [%1$s]. Missing nonce key [%2$s] '
                        . 'in provided login form values.', get_class($this), $nonce_key)
            );
        }
        $existing = unserialize($this::user()->get($nonce_key));
        if ($this->form_values->nonce !== (string) $existing) {
            // Nonce mismatch
            $this->getLogger()->info('[type][scope][class] Nonce key mismatch for key  '
                . '[login-nonce] in login form. Login attempt was supressed.', [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
                'login-nonce' => $nonce_key,
            ]);
            throw new \Oroboros\core\exception\auth\RefererException(
                    sprintf('Error encountered in [%1$s]. Nonce key mismatch for key [%2$s] '
                        . 'in provided login form values.', get_class($this), $nonce_key)
            );
        }
    }

    /**
     * Verifies that the referer is from the expected source
     * 
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @return void
     * @throws \Oroboros\core\exception\auth\RefererException
     *         If the login referer is not from a valid page
     */
    protected function verifyLoginReferer(\Psr\Http\Message\ServerRequestInterface $request): void
    {
        $this->getLogger()->debug('[type][scope][class] Verifying login referer.', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
        ]);

        $expected = sprintf('%1$s/%2$s/', OROBOROS_CORE_HOST, static::CLASS_SCOPE);
        $headers = $request->getHeaders();
        if (!$request->hasHeader('referer')) {
            // No referer exists at all. This is a remote call
            // to the login form directly.
            // Bots can circumvent this by setting the right
            // request header but this is a dumb bot or script kiddie.
            $this->getLogger()->info('[type][scope][class] Login referer missing. Remote login attempt was prevented.', [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
            ]);
            throw new \Oroboros\core\exception\auth\RefererException(
                    sprintf('Error encountered in [%1%s]. No referer header '
                        . 'exists in the login request. This is a potentially '
                        . 'malicious login attempt.', get_class($this))
            );
        }
        if (!in_array($expected, $request->getHeader('referer'), true)) {
            // Has a referer but was not referred from login. This is insufficient 
            // for the standard login page. If a header embedded login button 
            // is desired, it must override this method and loosen allowed referers.
            $this->getLogger()->info('[type][scope][class] Login referer mismatch. Suspicious login attempt was prevented.', [
                'class' => get_class($this),
                'type' => static::CLASS_TYPE,
                'scope' => static::CLASS_SCOPE,
            ]);
            throw new \Oroboros\core\exception\auth\RefererException(
                    sprintf('Error encountered in [%1%s]. Referer did not originate from expected source [%2$s]. '
                        . 'exists in the login request. This is a potentially '
                        . 'This is a possibly malicious or automated login attempt.', get_class($this), $expected)
            );
        }
        // All good then.
        $this->getLogger()->debug('[type][scope][class] Login referer verified.', [
            'class' => get_class($this),
            'type' => static::CLASS_TYPE,
            'scope' => static::CLASS_SCOPE,
        ]);
    }

    /**
     * Fetches the default credentials.
     * The form nonce will be cleared from the user
     * if a previous one exists, and replaced with a new one.
     * 
     * @return \Oroboros\core\interfaces\library\auth\CredentialContainerInterface
     */
    protected function getDefaultLoginCredentials(array $credentials = null): \Oroboros\core\interfaces\library\auth\CredentialContainerInterface
    {
        if (is_null($credentials)) {
            $credentials = [];
        }
        if (array_key_exists('nonce', $credentials)) {
            unset($credentials['nonce']);
        }
        $this->clearLoginNonce();
        $nonce_key = $this->getLoginNonceKey();
        $credentials = $this->getAuthManager()->credentials($credentials);
        $this::user()->set($nonce_key, serialize($credentials['nonce']));
        return $credentials;
    }

    /**
     * Returns the nonce key appropriate to the current scope
     * 
     * @return string
     */
    protected function getLoginNonceKey(): string
    {
        $nonce_key = trim(sprintf('%1$s.%2$s.%3$s', static::CLASS_TYPE, static::CLASS_SCOPE, 'nonce'), '.');
        return $nonce_key;
    }

    /**
     * Clears the login form nonce from the user session
     * 
     * @return void
     */
    protected function clearLoginNonce(): void
    {
        $nonce_key = $this->getLoginNonceKey();
        if ($this::user()->has($nonce_key)) {
            $nonce = unserialize($this::user()->get($nonce_key));
            $nonce->expire();
            $this::user()->unset($nonce_key);
            unset($nonce);
        }
    }

    /**
     * Initializes the auth manager if it is not already initialized.
     * 
     * @return void
     * @throws \Oroboros\core\exception\ErrorException if there is no
     *         auth manager matching the current class scope
     */
    private function initializeAuthManager(): void
    {
        if (is_null($this->auth_manager)) {
            $strategy = $this->load('library', 'auth\\ManagerStrategy', $this->getCommand(), $this->getArguments(), $this->getFlags());
            $scope = static::CLASS_SCOPE;
            if (!$strategy->hasManager($scope)) {
                throw new \Oroboros\core\exception\ErrorException(
                        sprintf('Error encountered in [%1$s]. No authentication scope '
                            . 'exists for [%2$s].', get_class($this), $scope)
                );
            }
            $this->auth_manager = $strategy->getManager($scope);
        }
    }
}
