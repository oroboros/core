<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\database;

/**
 * Database Object Utility Trait
 * Provides object lookup and aliasing utilities for database objects
 *
 * @author Brian Dayhoff
 */
trait DatabaseObjectUtilityTrait
{

    /**
     * Container of column objects used as selectors in the query
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $columns = null;

    /**
     * Container of table objects used as selectors in the query
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $tables = null;

    /**
     * Container of schemas objects used as selectors in the query
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $schemas = null;

    /**
     * Container of index objects used as selectors in the query
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $indexes = null;

    /**
     * Container of relationship objects used as selectors in the query
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $relationships = null;

    /**
     * Alias mapping for known tables.
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $table_aliases = null;

    /**
     * Container of column alias mapping, used to determine the AS clauses for selectors
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $column_aliases = null;

    private function getAliasedTableName(string $table): string
    {
        $key = str_replace(static::LEFT_DELIMITER, null, str_replace(static::RIGHT_DELIMITER, null, $table));
        if ($this->table_aliases->has($key)) {
            $table = sprintf('%2$s%1$s%3$s', $this->table_aliases[$key], static::LEFT_DELIMITER, static::RIGHT_DELIMITER);
        }
        return $table;
    }

    private function getTableAliasedColumnName(string $column): string
    {
        $key = str_replace(static::LEFT_DELIMITER, null, str_replace(static::RIGHT_DELIMITER, null, $column));
        $tmp = explode(static::SEPARATOR, $key);
        if (count($tmp) === 3) {
            // schema included
            $schema = $tmp[0];
            $table = $tmp[1];
            $column = $tmp[2];
        } elseif (count($tmp) === 2) {
            // schema not included
            $schema = $this->schema;
            $table = $tmp[0];
            $column = $tmp[1];
        } else {
            // Only table included
            $schema = $this->schema;
            $table = $this->table;
            $column = $tmp[0];
        }
        $col_key = $schema . static::SEPARATOR . $table . static::SEPARATOR . $column;
        $table_alias = $this->getAliasedTableName($schema . static::SEPARATOR . $table);
        return sprintf('%1$s%5$s%3$s%2$s%4$s', $table_alias, $column, static::LEFT_DELIMITER, static::RIGHT_DELIMITER, static::SEPARATOR);
    }

    private function getAliasedColumnName(string $column): string
    {
        $key = str_replace(static::LEFT_DELIMITER, null, str_replace(static::RIGHT_DELIMITER, null, $column));
        $tmp = explode(static::SEPARATOR, $key);
        if (count($tmp) === 3) {
            // schema included
            $schema = $tmp[0];
            $table = $tmp[1];
            $column = $tmp[2];
        } elseif (count($tmp) === 2) {
            // schema not included
            $schema = $this->schema;
            $table = $tmp[0];
            $column = $tmp[1];
        } else {
            // Only table included
            $schema = $this->schema;
            $table = $this->table;
            $column = $tmp[0];
        }
        $col_key = $schema . static::SEPARATOR . $table . static::SEPARATOR . $column;
        if ($this->column_aliases->has($col_key)) {
            return $this->column_aliases->get($col_key);
        }
        return $column;
    }

    private function verifySchema($schema): \Oroboros\core\interfaces\library\database\schema\SchemaInterface
    {
        $expected = 'Oroboros\\core\\interfaces\\library\\database\\schema\\SchemaInterface';
        if (!is_string($schema) && !(is_object($schema) && ($schema instanceof $expected))) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided table must be a string or instance of [%2$s]', get_class($this), $expected));
        }
        if (is_object($schema)) {
            return $schema;
        }
        // Fetch the corresponding column object and return it.
        $schema = str_replace(static::LEFT_DELIMITER, null, str_replace(static::RIGHT_DELIMITER, null, $schema));
        try {
            $object = $this->getSchemaObject($schema);
        } catch (\InvalidArgumentException $e) {
            // Column does not exist in the schema registry. Load it.
            $object = $this->load('library', static::SCHEMA_CLASS, $schema, [
                'connection' => $this->getConnection()->getName(),
                static::INFORMATION_SCHEMA_SELECTOR => $schema
            ]);
        }
        return $object;
    }

    private function verifyTable($table): \Oroboros\core\interfaces\library\database\table\TableInterface
    {
        $expected = 'Oroboros\\core\\interfaces\\library\\database\\table\\TableInterface';
        if (!is_string($table) && !(is_object($table) && ($table instanceof $expected))) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided table must be a string or instance of [%2$s]', get_class($this), $expected));
        }
        if (is_object($table)) {
            return $table;
        }
        // Fetch the corresponding column object and return it.
        $table = str_replace(static::LEFT_DELIMITER, null, str_replace(static::RIGHT_DELIMITER, null, $table));
        $tmp = explode(static::SEPARATOR, $table);
        if (count($tmp) === 3 || count($tmp) === 2) {
            $schema = array_shift($tmp);
            $table = array_shift($tmp);
        } elseif (count($tmp) === 1) {
            $schema = $this->getSchema()->getName();
            $table = array_shift($tmp);
        } else {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Table designation could not be derived from [%2$s].', get_class($this), $table));
        }
        try {
            $object = $this->getTableObject($schema, $table);
        } catch (\InvalidArgumentException $e) {
            // Column does not exist in the schema registry. Load it.
            $object = $this->load('library', static::TABLE_CLASS, $table, [
                'connection' => $this->getConnection()->getName(),
                static::INFORMATION_SCHEMA_SELECTOR => $schema,
                static::INFORMATION_TABLE_SELECTOR => $table
            ]);
        }
        return $object;
    }

    private function verifyColumn($column): \Oroboros\core\interfaces\library\database\column\ColumnInterface
    {
        $expected = 'Oroboros\\core\\interfaces\\library\\database\\column\\ColumnInterface';
        if (!is_string($column) && !(is_object($column) && ($column instanceof $expected))) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided column must be a string or instance of [%2$s]', get_class($this), $expected));
        }
        if (is_object($column)) {
            return $column;
        }
        // Fetch the corresponding column object and return it.
        $column = str_replace(static::LEFT_DELIMITER, null, str_replace(static::RIGHT_DELIMITER, null, $column));
        $tmp = explode(static::SEPARATOR, $column);
        if (count($tmp) === 3) {
            $schema = array_shift($tmp);
            $table = array_shift($tmp);
            $column = array_shift($tmp);
        } elseif (count($tmp) === 2) {
            $schema = $this->getSchema()->getName();
            $table = array_shift($tmp);
            $column = array_shift($tmp);
        } else {
            $schema = $this->getSchema()->getName();
            if ($this instanceof \Oroboros\core\interfaces\library\database\table\TableInterface) {
                $table = $this->getName();
            } else {
                $table = $this->getTable()->getName();
            }
            $column = array_shift($tmp);
        }
        unset($tmp);
        try {
            $object = $this->getColumnObject($schema, $table, $column);
        } catch (\InvalidArgumentException $e) {
            // Column does not exist in the schema registry. Load it.
            $object = $this->load('library', static::COLUMN_CLASS, $column, [
                'connection' => $this->getConnection()->getName(),
                'table_aliases' => $this->table_aliases,
                'column_aliases' => $this->column_aliases,
                static::INFORMATION_SCHEMA_SELECTOR => $schema,
                static::INFORMATION_TABLE_SELECTOR => $table,
                static::INFORMATION_COLUMN_SELECTOR => $column
            ]);
        } catch (\Exception $e) {
            d('Error retrieving column', $e->getMessage(), $e->getTraceAsString());
            exit;
        }
        return $object;
    }

    private function initializeTableAliases(): void
    {
        if ($this->hasArgument('table_aliases')) {
            $this->table_aliases = $this->getArgument('table_aliases');
        } else {
            $this->table_aliases = $this::containerize();
        }
    }

    private function initializeColumnAliases(): void
    {
        if ($this->hasArgument('column_aliases')) {
            $this->column_aliases = $this->getArgument('column_aliases');
        } else {
            $this->column_aliases = $this::containerize();
        }
    }
}
