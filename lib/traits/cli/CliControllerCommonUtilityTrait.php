<?php
namespace Oroboros\core\traits\cli;

/**
 * Controller Common Utilities Trait
 *
 * @author Brian Dayhoff
 */
trait CliControllerCommonUtilityTrait
{

//
//    /**
//     * The Psr-6 cache object. A null cache will be used if none was provided.
//     * @var \dynamiq\app\interfaces\library\user\UserInterface
//     */
//    private $cache = null;

    /**
     * Represents any notification messages about why login failed.
     * @var array
     */
    private $form_messages = [];
    private $page_scripts = null;
    private $page_styles = null;
    private $page_media = null;
    private $page_fonts = null;

    protected function loadPageDetails(array $arguments = []): void
    {
        $page = $this->getPageDetails($this->getArgument('request'), $this->getArgument('route'));
        if (array_key_exists('charset', $arguments)) {
            $this->addCharset($arguments['charset']);
        } elseif ($page->has('charset')) {
            $this->addCharset($page->get('charset'));
        }
        if (array_key_exists('lang', $arguments)) {
            $this->addLanguage($arguments['lang']);
        } elseif ($page->has('lang')) {
            $this->addLanguage($page->get('lang'));
        }
        if (array_key_exists('title', $arguments)) {
            $this->addTitle(($page->has('title') ? $page->get('title') : null) . $arguments['title']);
        } elseif ($page->has('title')) {
            $this->addTitle($page->get('title'));
        }
        if (array_key_exists('author', $arguments)) {
            $this->addAuthor($arguments['author']);
        } elseif ($page->has('author')) {
            $this->addAuthor($page->get('author'));
        }
        if (array_key_exists('description', $arguments)) {
            $this->addDescription($arguments['description']);
        } elseif ($page->has('description')) {
            $this->addDescription($page->get('description'));
        }
        if (array_key_exists('id', $arguments)) {
            $this->addPageId($arguments['id']);
        } elseif ($page->has('id')) {
            $this->addPageId($page->get('id'));
        }
        $layout = $this->getLayout();
        $layout_key = $layout::LAYOUT_KEY;
        $class = $page->has('class') ? $page->get('class') : null;
        if (is_null($class) || $class === '') {
            $class = 'layout-' . $layout_key;
        } else {
            $class = sprintf('%1$s %2$s', 'layout-' . $layout_key, $class);
        }
        $this->addPageClass($class);
        if ($page->has('heading')) {
            $details = [
                'context' => [
                    'id' => 'page-title',
                    'context' => 'title',
                    'parent' => 'page-content',
                    'title' => $page->get('heading') . (array_key_exists('heading', $arguments) ? $arguments['heading'] : null)
                ]
            ];
            if (array_key_exists('subheading', $arguments)) {
                $details['context']['subtitle'] = $arguments['subheading'];
            } elseif ($page->has('subheading') && !is_null($page->get('subheading'))) {
                $details['context']['subtitle'] = $page->get('subheading');
            }
            try {
                $component = $this->getComponent('html\\PageTitle', 'page-title', $details);
                $this->addContent('primary', 'page-title', $component);
            } catch (\Exception $e) {
                d('error loading component', $e->getMessage(), $e->getTraceAsString());
                exit;
            }
        }
    }

    protected function getPageDetails(\Psr\Http\Message\ServerRequestInterface $request, \Oroboros\core\interfaces\library\router\RouteInterface $route): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $this->getPageLayout($request, $route);
        $defaults = $this->getDefaultPageDetails();
        $subdomain = $this->getArgument('subdomain');
        $domain = $this->getArgument('domain');
        if ($subdomain === 'www') {
            $subdomain = '';
        }
        $uri = $route->getUri();
        try {
            // Route lookup
            try {
                $model = $this->load('model', 'PageModel', 'page');
                $details = [
                    'subdomain' => $subdomain,
                    'domain' => $domain,
                    'route' => $uri,
                    'deleted_at' => NULL
                ];
                $page = $model->lookup($details);
            } catch (\Exception $e) {
                // Use the default untitled page definition
                $details['route'] = NULL;
                try {
                    $page = $model->lookup($details);
                } catch (\Exception $e) {

                    d('uri lookup also failed', $e->getMessage(), $e->getTraceAsString());
                    exit;
                }
            }
            $result = $this::containerize(array_replace_recursive($defaults->toArray(), [
                    'id' => $page::canonicalize(str_replace('/', ' ', trim($uri, ' /'))),
                    'class' => $this->getLayout()::LAYOUT_KEY,
                    'title' => $page->get('title'),
                    'heading' => $page->get('heading'),
                    'subheading' => $page->get('subheading'),
                    'description' => $page->get('description'),
                    'author' => $page->get('author'),
            ]));
        } catch (\InvalidArgumentException $e) {
            // No page entry
            d($e->getMessage(), $e->getTraceAsString());
            $result = $defaults;
        }
        $result = $result->toArray();
        foreach ($result as $key => $value) {
            if (is_null($result[$key])) {
                unset($result[$key]);
            }
        }
        return $this::containerize($result);
    }

    /**
     * Returns a component instance representing the given name
     * @return \Oroboros\core\interfaces\library\component\ComponentInterface
     */
    protected function getComponent(string $name, string $key, array $details = [], array $flags = []): \Oroboros\core\interfaces\library\component\ComponentInterface
    {
        $details['builder'] = $this->getResponseDirector()->getWorker('component');
        $details['user'] = $this::user();
        $details['request'] = $this->getArgument('request');
        $details['route'] = $this->getArgument('route');
        return $this->load('component', $name, $key, $details, $flags);
    }
//
//    /**
//     * Dependency injection method for the user object.
//     * Users may only be set one time, and may not be 
//     * redefined after the fact within the same runtime.
//     * 
//     * @param null|\Psr\Cache\interfaces\library\user\UserInterface $user
//     * @return void
//     * @throws \ErrorException If the user has already been set
//     */
//    protected function setCache(\Oroboros\core\interfaces\library\cache\CachePoolInterface $cache = null): void
//    {
//        if (is_null($cache)) {
//            try {
//                $defaults = $this::app()->config()->get('schemas')->get('default');
//                if (
//                    !array_key_exists('cache', $defaults) || !$defaults['cache']['enabled']) {
//                    throw new \Oroboros\core\exception\NotFoundException();
//                }
//                $cache_type = $defaults['cache']['type'];
//                $connection = $defaults['cache']['connection'];
//            } catch (\Psr\Container\NotFoundExceptionInterface $e) {
//                // Use the null cache
//                $cache_type = 'null';
//                $connection = null;
//            }
//            $cache = $this->load('library', 'cache\\CacheStrategy')->evaluate($cache_type, $connection);
//        }
//        $this->cache = $cache;
//    }
//
//    /**
//     * Returns the user object currently scoped to the request
//     * @return \dynamiq\app\interfaces\library\user\UserInterface
//     */
//    protected function getUser(): \dynamiq\app\interfaces\library\user\UserInterface
//    {
//        return $this->user;
//    }
//
//    /**
//     * Returns the Psr-6 cache pool object currently scoped to the controller
//     * @return \dynamiq\app\interfaces\library\user\UserInterface
//     */
//    protected function getCache(): \dynamiq\app\interfaces\library\user\UserInterface
//    {
//        return $this->cache;
//    }
//    protected function initializeLayout(string $layout = null): void
//    {
//        if (is_null($layout)) {
//            $layout = static::CLASS_SCOPE . '\\' . static::LAYOUT_CLASS;
//        }
//        try {
//            $fullclass = $this->getFullClassName('library', 'layout\\' . $layout);
//            if (!$fullclass)
//            {
//                throw new \Oroboros\core\exception\InvalidArgumentException('Error encountered in [%1$s]. No known layout class exists for [%2$s].', get_class($this), $layout);
//            }
//            
//        } catch (\InvalidArgumentException $e) {
//
//        }
//        $this->layout = $this->load('layout', $layout, null, ['director' => $this->getResponseDirector()]);
//    }

    protected function queuePageAssets()
    {
        try {
            foreach ($this->getPageScripts() as $category => $scripts) {
                foreach ($scripts as $id => $data) {
                    $this->addScript($category, $id, $data);
                }
            }
            foreach ($this->getPageStylesheets() as $category => $details) {
                foreach ($details as $id => $data) {
                    $this->addStylesheet($category, $id, $data);
                }
            }
            foreach ($this->getPageMedia() as $category => $details) {
                //            foreach ($details as $id => $data) {
                //                $this->setData($category, $id, $data);
                //            }
            }
            foreach ($this->getPageFonts() as $category => $details) {
                foreach ($details as $id => $data) {
                    $this->addFont($category, $id, $data);
                }
            }
            foreach ($this->getPageContent() as $category => $details) {
                foreach ($details as $id => $data) {
                    $this->addContent($category, $id, $data);
                }
            }
            foreach ($this->getPageData() as $category => $details) {
                foreach ($details as $id => $data) {
                    $this->addData($category, $id, $data);
                }
            }
        } catch (\Exception $e) {
            d($e->getMessage(), $e->getTraceAsString());
            exit;
        }
    }

    protected function queuePageContent()
    {
        $this->loadBreadcrumbs();
        $this->loadUi();
    }

    protected function loadBreadcrumbs()
    {
        
    }

    protected function loadUi()
    {
        
    }

    protected function loadForm(string $slug, array $values = [], array $configuration = [])
    {
        $model = $this->load('model', 'FormModel', $slug)->lookup([
            'slug' => $slug,
            'enabled' => 1,
            'deleted_at' => null
        ]);
        $groups = $this->load('model', 'FormGroupModel', 'form-groups')->pull([
            'form' => $model->get('slug'),
            'enabled' => 1,
            'deleted_at' => null
            ], ['order' => 'asc']);
        $fields = $this->load('model', 'FormFieldModel', 'form-fields')->pull([
            'form' => $model->get('slug'),
            'enabled' => 1,
            'deleted_at' => null
            ], ['group' => 'asc', 'order' => 'asc']);
        try {
            $field_set = $this->aggregateFormValues($values, $this->aggregateFormFields($groups, $fields, $this::containerize($configuration)));
        } catch (\Psr\Container\NotFoundInterface $e) {
            
        }
        $this->queueErrorMessages();
        $context = [
            'id' => $model->get('slug'),
            'class' => (array_key_exists('class', $configuration)) ? $configuration['class'] : $model->get('class'),
            'name' => (array_key_exists('name', $configuration)) ? $configuration['name'] : $model->get('name'),
            'method' => (array_key_exists('method', $configuration)) ? $configuration['method'] : $model->get('method'),
            'action' => (array_key_exists('action', $configuration)) ? $configuration['action'] : $model->get('action'),
            'context' => (array_key_exists('context', $configuration)) ? $configuration['context'] : $model->get('context'),
            'component' => (array_key_exists('component', $configuration)) ? $configuration['component'] : $model->get('slug'),
            'groups' => $field_set
        ];
        $form = $this->getComponent('html\\Form', $model->get('slug'), [
            'user' => $this::user(),
            'context' => $context
        ]);
        return $form;
    }

    private function aggregateFormValues(array $values, \Oroboros\core\interfaces\library\container\ContainerInterface $fields)
    {
        $results = $fields->toArray();
        foreach ($results as $fgk => $field_group) {
            foreach ($field_group['fields'] as $key => $field) {
                if (!array_key_exists($field['name'], $values)) {
                    continue;
                }
                if ($field['type'] === 'select' && array_key_exists($field['name'], $values) && is_array($values[$field['name']])) {
                    $results[$fgk]['fields'][$key]['options'] = $values[$field['name']];
                } else {
                    $results[$fgk]['fields'][$key]['value'] = $values[$field['name']];
                }
            }
        }
        return $this::containerize($results);
    }

    private function queueErrorMessages(): void
    {
        $content = [];
        if (empty($this->form_messages)) {
            // noting to do
            return;
        }
        foreach ($this->form_messages as $key => $message) {
            $component = $this->getComponent('html\\Banner', 'error-' . $key, [
                'context' => [
                    'class' => 'alert alert-danger',
                    'content' => $message,
                    'dismissable' => true
                ]
            ]);
            $content['error-' . $key] = $component;
        }
        $notification_wrapper = $this->getComponent('html\\Wrapper', 'form-errors', [
            'context' => [
                'id' => 'form-errors',
                'class' => 'd-flex flex-column',
                'context' => 'form-errors',
                'category' => 'notifications',
                'parent' => 'page-content',
                'content' => $content
            ]
        ]);
        $this->addContent('primary', 'form-errors', $notification_wrapper);
    }

    private function aggregateFormFields(\Oroboros\core\interfaces\library\container\ContainerInterface $groups, \Oroboros\core\interfaces\library\container\ContainerInterface $fields, \Oroboros\core\interfaces\library\container\ContainerInterface $config)
    {
        $results = [];
        $groupset = $this->sortFieldGroups($groups);
        $fieldset = $this->sortFields($fields, $groupset);
        foreach ($groupset as $key => $group) {
            try {
                $results[$key] = $this->formatFieldGroup($group, $fieldset[$key], $config);
            } catch (\Psr\Container\NotFoundExceptionInterface $e) {
                $results[$key] = [];
            }
        }
        return $this::containerize($results);
    }

    private function sortFieldGroups(\Oroboros\core\interfaces\library\container\ContainerInterface $groups): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $results = [];
        $order = [];
        foreach ($groups as $group) {
            if (!$this::user()->can($group['permission'])) {
                continue;
            }
            $order[intval($group['order'])] = $group;
        }
        ksort($order);
        foreach ($order as $group) {
            $results[$group['slug']] = $group;
        }
        return $this::containerize($results);
    }

    private function sortFields(\Oroboros\core\interfaces\library\container\ContainerInterface $fields, \Oroboros\core\interfaces\library\container\ContainerInterface $groups): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $results = [];
        foreach ($groups as $group) {
            $order = [];
            foreach ($fields as $field) {
                if (is_null($field['group']) || $field['group'] !== $group['slug']) {
                    continue;
                }
                $field['readonly'] = $this::user()->can($field['permission']) ? false : $field['readonly'];
                $field['disabled'] = $this::user()->can($field['permission']) ? false : $field['readonly'];
                $order[intval($field['order'])] = $field;
            }
            ksort($order);
            foreach ($order as $field) {
                $results[$group['slug']][$field['slug']] = $field;
            }
        }
        return $this::containerize($results);
    }

    private function formatFieldGroup(array $group, array $fields, \Oroboros\core\interfaces\library\container\ContainerInterface $config = null): array
    {
        if (is_null($config)) {
            $config = $this::containerize();
        }
        $fieldset = [];
        foreach ($fields as $key => $field) {
            $fieldset[$key] = $this->formatField($field, $config->has($key) ? $config->get($key) : []);
        }
        $details = [
            'id' => sprintf('%1$s-%2$s', $group['form'], $group['slug']),
            'class' => $group['class'],
            'context' => $group['context'],
            'parent' => $group['form'],
            'icon' => $group['icon'],
            'hidden' => $group['hidden'] !== 0 ? true : false,
            'internal' => $group['internal'] !== 0 ? true : false,
            'fields' => $fieldset
        ];
        return $details;
    }

    private function formatField(array $field, array $config = []): array
    {
        $results = [
            'type' => $field['type'],
            'id' => str_replace('--', '-', sprintf('%1$s-%2$s-%3$s', $field['form'], $field['group'], $field['slug'])),
            'name' => $field['name'],
            'class' => $field['class'],
            'icon' => $field['icon'],
            'placeholder' => $field['placeholder'],
            'tooltip' => $field['tooltip'],
            'value' => $field['value'],
            'content' => $field['content'],
            'autocomplete' => $field['autocomplete'] ? true : false,
            'autocorrect' => $field['autocorrect'] ? true : false,
            'autocapitalize' => $field['autocapitalize'] ? true : false,
            'autofocus' => $field['autofocus'] ? true : false,
            'required' => $field['required'] ? true : false,
            'readonly' => $field['readonly'] ? true : false,
            'hidden' => $field['hidden'] ? true : false,
            'label' => $field['label'],
            'aria-label' => $field['aria_label'],
            'internal' => $field['internal'] ? true : false,
            'context' => $field['context'],
            'parent' => $field['group'],
        ];
        if ($results['type'] === 'select') {
            $results['options'] = $config;
        }
        foreach ($config as $key => $value) {
            $results[$key] = $value;
        }
        return $results;
    }

    private function getPageLayout(\Psr\Http\Message\ServerRequestInterface $request, \Oroboros\core\interfaces\library\router\RouteInterface $route): void
    {
        $subdomain = ($this->hasArgument('subdomain') ? $this->getArgument('subdomain') : '');
        $domain = ($this->hasArgument('domain') ? $this->getArgument('domain') : $this::app()::environment()->get('application')->get('domain'));
        $uri = $route->getUri();
        $details = [
            'subdomain' => $subdomain,
            'domain' => $domain,
            'route' => $uri
        ];
        try {
            // Lookup the layout by page
            $layout_key = $this->load('model', 'PageModel', 'layout-lookup')->lookup($details)->get('layout');
            $layout_record = $this->load('model', 'LayoutModel', 'layout-record')->lookup([
                'slug' => $layout_key,
                'deleted_at' => NULL
            ]);
        } catch (\InvalidArgumentException $e) {
            // Load the default
            d('Layout not found.', $e->getMessage(), $e->getTraceAsString());
            exit;
            $layout_record = $this->load('model', 'LayoutModel', 'layout-record')->lookup([
                'slug' => 'default'
            ]);
        }
        $this->initializeLayout($layout_record->get('class'));
    }

    private function getDefaultPageDetails(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $defaults = [
            'id' => '',
            'class' => '',
            'charset' => 'utf-8',
            'lang' => 'en',
            'title' => 'Untitled Page',
            'heading' => 'Untitled Page',
            'description' => 'No description provided.'
        ];
        return $this::containerize($defaults);
    }

    private function getPageScripts(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $request = $this->getArgument('request');
        $route = $this->getArgument('route');
        $result = [
            'head' => [],
            'body' => [],
            'footer' => []
        ];
        if (is_null($request) || is_null($route)) {
            // Out of scope, return a nulled script container
            return $this::containerize($result);
        }
        $subdomain = $this->getArgument('subdomain');
        $domain = $this->getArgument('domain');
        if ($subdomain === 'www') {
            $subdomain = '';
        }
        $uri = $route->getUri();
        $details = [
            'subdomain' => $subdomain,
            'domain' => $domain,
            'route' => $uri,
            'permission' => array_keys($this::user()->getPermissions()->toArray())
        ];
        foreach ($this->load('model', 'PageScriptModel', 'page-scripts')->pull($details) as $script) {
            $result[$script['location']][$script['script']] = $script['script'];
        }
        $scripts = $this->load('model', 'PageScriptModel', 'page-scripts')->pull($details);
        return $this::containerize($result);
    }

    private function getPageStylesheets(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $result = [];
        return $this::containerize($result);
    }

    private function getPageFonts(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $result = [];
        return $this::containerize($result);
    }

    private function getPageMedia(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $result = [];
        return $this::containerize($result);
    }

    private function getPageContent(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $result = [];
        return $this::containerize($result);
    }

    private function getPageData(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $result = [];
        $result = [
            'user' => [
                'current' => $this::containerize([
                    'username' => $this::user()->getUsername(),
                    'organization' => $this::user()->getOrganization(),
                    'role' => $this::user()->getUserGroup(),
                    'enabled' => $this::user()->isEnabled(),
                    'verified' => $this::user()->isVerified(),
                    'logged_in' => $this::user()->isLoggedIn(),
                    'details' => $this::user()->getDetails()
                ])
            ]
        ];
        return $this::containerize($result);
    }
}
