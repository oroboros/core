<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\cli;

/**
 * Cli Process Utility Trait
 * Contains common functionality for command line diagnostics of the
 * current thread, process, and user/group calling execution of it.
 *
 * @author Brian Dayhoff
 */
trait CliProcessUtilityTrait
{

    /**
     * The process id of the current runtime.
     * @var string
     */
    private static $pid = null;

    /**
     * The username of the current cli session user.
     * @var string
     */
    private static $uid = null;

    /**
     * The group id of the current cli session user.
     * @var string
     */
    private static $gid = null;

    /**
     * The inode of the current cli session user.
     * @var string
     */
    private static $inode = null;

    /**
     * The username of the current cli session user.
     * @var string
     */
    private static $current_user = null;

    /**
     * Returns the process id of the current runtime.
     * @return int
     */
    protected function getPid(): int
    {
        return self::$pid;
    }

    /**
     * Returns the group id of the user executing the current runtime.
     * @return int
     */
    protected function getGid(): int
    {
        return self::$gid;
    }

    /**
     * Returns the user id of the script owner.
     * @return int
     */
    protected function getUid(): int
    {
        return self::$uid;
    }

    /**
     * Returns the inode of the user executing the current runtime.
     * @return string
     */
    protected function getInode()
    {
        return self::$uid;
    }

    /**
     * Returns the username of the current cli session user executing the script.
     * @return string
     */
    protected function getCliUser(): string
    {
        return self::$current_user;
    }

    private function initializePid(): void
    {
        if (is_null(self::$pid)) {
            self::$pid = getmypid();
        }
    }

    private function initializeGid(): void
    {
        if (is_null(self::$gid)) {
            self::$gid = posix_getgid();
        }
    }

    private function initializeUid(): void
    {
        if (is_null(self::$uid)) {
            self::$uid = posix_getuid();
        }
    }

    private function initializeInode(): void
    {
        if (is_null(self::$inode)) {
            self::$inode = getmyinode();
        }
    }

    private function initializeCurrentCliUser(): void
    {
        if (is_null(self::$current_user)) {
            self::$current_user = get_current_user();
        }
    }
}
