<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\cli;

/**
 * Cli Output Utility Trait
 * Contains common functionality for command line output
 *
 * @author Brian Dayhoff
 */
trait CliOutputUtilityTrait
{

    private static $format_codes = [
        // CLI Syntax
        'normal' => "\e[0m",
        'reset' => "\e[0m",
        'bold' => "\e[1m",
        'remove-bold' => "\e[21m",
        'dim' => "\e[2m",
        'remove-dim' => "\e[22m",
        'underline' => "\e[4m",
        'remove-underline' => "\e[24m",
        'blink' => "\e[5m",
        'remove-blink' => "\e[25m",
        'hidden' => "\e[8m",
        'remove-hidden' => "\e[28m",
        'italic' => "\e[3m",
        'strikethrough' => "\e[9m",
        // CLI Colors - Foreground
        'black' => "\033[0;30m",
        'white' => "\033[0;38m",
        'red' => "\033[0;31m",
        'cyan' => "\033[0;36m",
        'blue' => "\033[0;34m",
        'green' => "\033[0;32m",
        'gray' => "\033[0;90m",
        'brown' => "\033[0;33m",
        'purple' => "\033[2;35m",
        'magenta' => "\033[0;35m",
        'yellow' => "\033[0;33m",
        'dark-gray' => "\033[1;30m",
        'light-red' => "\033[1;31m",
        'light-blue' => "\033[1;34m",
        'light-cyan' => "\033[1;36m",
        'light-gray' => "\033[2;37m",
        'light-green' => "\033[1;32m",
        'light-white' => "\033[1;38m",
        'light-purple' => "\033[1;35m",
        'light-yellow' => "\033[1;93m",
        'light-magenta' => "\033[1;35m",
        // CLI Colors - Background
        'background-default' => "\033[49m",
        'background-black' => "\033[40m",
        'background-white' => "\e[107m",
        'background-red' => "\033[41m",
        'background-blue' => "\033[44m",
        'background-cyan' => "\033[46m",
        'background-green' => "\033[42m",
        'background-yellow' => "\033[43m",
        'background-magenta' => "\033[45m",
        'background-light-gray' => "\033[47m",
        // CLI Link Construction
        'link-prefix' => "\e]8;;",
        'link-bridge' => "\e\\",
        'link-suffix' => "\e]8;;\e\\\n",
    ];
    private static $stdout = null;
    private static $stderr = null;
    private static $stdin = null;

    /**
     * Gets the literal value of a given format code key.
     * 
     * @param string $code
     * @return string
     */
    protected function getFormatCode(string $code = null): string
    {
        if (is_null($code)) {
            return "";
        }
        $this->verifyFormatKey($code);
        return self::$format_codes[$code];
    }

    /**
     * Clears the cli console screen.
     * 
     * @return void
     */
    protected function clear_screen(): void
    {
        if (PHP_OS == 'WINNT') {
            system('cls');
        } else {
            fwrite(self::$stdout, chr(27) . chr(91) . 'H' . chr(27) . chr(91) . 'J');
        }
    }

    /**
     * 
     * @param string $prefix
     * @param bool $secure
     * @return string
     * @throws \RuntimeException if the terminal device cannot be opened at `/dev/tty` This will only occur if `$secure` is `true`
     */
    protected function read_input(string $prefix = ' > ', bool $secure = false): string
    {
        if ($secure) {
            $terminal_device = '/dev/tty';
            $h = fopen($terminal_device, 'r');
            if ($h === false) {
                throw new \RuntimeException(sprintf('Error encountered in [%1$s]. '
                            . 'Failed to open terminal device [%2$s]'
                            , get_class($this), $terminal_device));
            }
            fwrite(self::$stdout, $prefix);
            $line = rtrim(fgets($h), "\r\n");
            fclose($h);
        } elseif (PHP_OS == 'WINNT') {
            fwrite(self::$stdout, $prefix);
            $line = stream_get_line(STDIN, 1024, PHP_EOL);
        } else {
            $line = readline($prefix);
            if (!empty($line)) {
                readline_add_history($line);
            }
        }
        return $line;
    }
    
    /**
     * Reads a keypress and returns the next key pressed.
     * 
     * @param string $prefix
     * @param bool $secure
     * @return string
     * @throws \RuntimeException
     * @throws \Oroboros\core\exception\ErrorException if the underlying
     *         operating system is windows, which does not support this
     *         functionality.
     */
    protected function read_keypress(string $prefix = null, bool $secure = false): string
    {
        $key = null;
        if ($secure) {
            // Secure shells
            $terminal_device = '/dev/tty';
            $h = fopen($terminal_device, 'r');
            if ($h === false) {
                throw new \RuntimeException(sprintf('Error encountered in [%1$s]. '
                            . 'Failed to open terminal device [%2$s]'
                            , get_class($this), $terminal_device));
            }
            fwrite(self::$stdout, $prefix);
            while ($key === null) {
                $key = ord(fgetc($h));
            }
            fclose($h);
        } elseif (PHP_OS == 'WINNT') {
            // Windows only
            throw new \Oroboros\core\exception\ErrorException(
                sprintf('Error encountered in [%1$s]. Keypress functionality is not supported in Windows environments.', get_class($this))
            );
        } else {
            // All others
            fwrite(self::$stdout, $prefix);
            system('stty cbreak');
            while ($key === null) {
                $key = ord(fgetc(self::$stdin));
            }
        }
        return $key;
    }
    
    /**
     * Formats a link to be ctrl+clickable in console and still appear as plaintext.
     * Returns the formatted link string, which can be safely sent to the console to display correctly.
     * 
     * @param string $text
     * @param string $target
     * @return string
     */
    protected function make_http_link(string $text, string $target): string
    {
        $output = $this->getFormatCode('link-prefix') . $target . $this->getFormatCode('link-bridge') . $text . $this->getFormatCode('link-suffix');
        return $output;
    }
    
    /**
     * Prints a link to the console.
     * 
     * @param string $text
     * @param string $target
     */
    protected function print_link(string $text, string $target): void
    {
        $this->renderCliMessage(self::$stdout, $this->getFormatCode('yellow') . $this->make_http_link($text, $target) . $this->getFormatCode('reset'));
    }

    /**
     * Formats a given message to indicate a successful operation
     * and prints it to the console.
     * 
     * @param string $message
     * @return void
     */
    protected function success_message(string $message): void
    {
        $msg_code = $this->getFormatCode('green');
        $reset_code = $this->getFormatCode('reset');
        $parameter_code = $this->getFormatCode('yellow');
        $message = str_replace('[', '[' . $parameter_code, $message);
        $message = str_replace(']', $msg_code . ']', $message);
        $this->renderCliMessage(self::$stdout, $message, $msg_code, $reset_code, $parameter_code);
    }

    /**
     * Prints a given message to the console.
     * Content in square braces will be formatted to yellow to be distinct
     * from the rest of the text body.
     * 
     * @param string $message
     * @return void
     */
    protected function print_message(string $message): void
    {
        $msg_code = 'reset';
        $reset_code = 'reset';
        $parameter_code = 'yellow';
        $message = str_replace('[', '[' . $this->getFormatCode($parameter_code), $message);
        $message = str_replace(']', $this->getFormatCode($msg_code) . ']', $message);
        $this->renderCliMessage(self::$stdout, $message, $msg_code, $reset_code, $parameter_code);
    }

    /**
     * Prints a raw message to the console without any formatting.
     * Existing formatting will not be removed.
     * 
     * @param string $message
     * @param string $stream
     * @return void
     * @throws \InvalidArgumentException if a given stream option is not valid (eg not stdin or stderr)
     */
    protected function print_raw(string $message, string $stream = 'stdout'): void
    {
        if ($stream === 'stdout') {
            $stream = &self::$stdout;
        } elseif ($stream === 'stderr') {
            $stream = &self::$stderr;
        } else {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Unrecognized output stream [%2$s].', get_class($this), $stream));
        }
        $this->renderCliMessage($stream, $message);
    }

    /**
     * Prints a debug format message to the console.
     * 
     * @param string $message
     * @return void
     */
    protected function debug_message(string $message): void
    {
        $msg_code = 'light-gray';
        $reset_code = 'reset';
        $parameter_code = 'yellow';
        $message = str_replace('[', '[' . $this->getFormatCode($parameter_code), $message);
        $message = str_replace(']', $this->getFormatCode($msg_code) . ']', $message);
        $this->renderCliMessage(self::$stdout, sprintf('[DEBUG] %1$s', $message), $msg_code, $reset_code, $parameter_code);
    }

    /**
     * Prints an info format message to the console.
     * 
     * @param string $message
     * @return void
     */
    protected function info_message(string $message): void
    {
        $msg_code = 'cyan';
        $reset_code = 'reset';
        $parameter_code = 'yellow';
        $message = str_replace('[', '[' . $this->getFormatCode($parameter_code), $message);
        $message = str_replace(']', $this->getFormatCode($msg_code) . ']', $message);
        $this->renderCliMessage(self::$stdout, sprintf('[INFO] %1$s', $message), $msg_code, $reset_code, $parameter_code);
    }

    /**
     * Prints a notice format message to the console.
     * 
     * @param string $message
     * @return void
     */
    protected function notice_message(string $message): void
    {
        $msg_code = 'blue';
        $reset_code = 'reset';
        $parameter_code = 'yellow';
        $message = str_replace('[', '[' . $this->getFormatCode($parameter_code), $message);
        $message = str_replace(']', $this->getFormatCode($msg_code) . ']', $message);
        $this->renderCliMessage(self::$stdout, sprintf('[NOTICE] %1$s', $message), $msg_code, $reset_code, $parameter_code);
    }

    /**
     * Prints a warning format message to the console.
     * 
     * @param string $message
     * @return void
     */
    protected function warning_message(string $message): void
    {
        $msg_code = 'yellow';
        $reset_code = 'reset';
        $parameter_code = 'reset';
        $message = str_replace('[', '[' . $this->getFormatCode($parameter_code), $message);
        $message = str_replace(']', $this->getFormatCode($msg_code) . ']', $message);
        $this->renderCliMessage(self::$stderr, sprintf('[WARNING] %1$s', $message), $msg_code, $reset_code, $parameter_code);
    }

    /**
     * Prints an error format message to the console.
     * 
     * @param string $message
     * @return void
     */
    protected function error_message(string $message): void
    {
        $msg_code = 'light-red';
        $reset_code = 'reset';
        $parameter_code = 'yellow';
        $message = str_replace('[', '[' . $this->getFormatCode($parameter_code), $message);
        $message = str_replace(']', $this->getFormatCode($msg_code) . ']', $message);
        $this->renderCliMessage(self::$stderr, sprintf('[ERROR] %1$s', $message), $msg_code, $reset_code, $parameter_code);
    }

    /**
     * Prints a critical error format message to the console.
     * 
     * @param string $message
     * @return void
     */
    protected function critical_message(string $message): void
    {
        $msg_code = 'red';
        $reset_code = 'reset';
        $parameter_code = 'yellow';
        $message = str_replace('[', '[' . $this->getFormatCode($parameter_code), $message);
        $message = str_replace(']', $this->getFormatCode($msg_code) . ']', $message);
        $this->renderCliMessage(self::$stderr, sprintf('[CRITICAL] %1$s', $message), $msg_code, $reset_code, $parameter_code);
    }

    /**
     * Prints an alert format message to the console.
     * 
     * @param string $message
     * @return void
     */
    protected function alert_message(string $message): void
    {        
        $msg_code = 'background-red';
        $reset_code = 'reset';
        $parameter_code = 'yellow';
        $message = str_replace('[', '[' . $this->getFormatCode($parameter_code), $message);
        $message = str_replace(']', $this->getFormatCode($msg_code) . ']', $message);
        $this->renderCliMessage(self::$stderr, sprintf('[ALERT] %1$s', $message), $msg_code, $reset_code, $parameter_code);
    }

    /**
     * Prints an emergency formamtted message to the console.
     * 
     * @param string $message
     * @return void
     */
    protected function emergency_message(string $message): void
    {        
        $msg_code = 'background-red';
        $reset_code = 'reset';
        $parameter_code = 'yellow';
        $message = str_replace('[', '[' . $this->getFormatCode($parameter_code), $message);
        $message = str_replace(']', $this->getFormatCode($msg_code) . ']', $message);
        $this->renderCliMessage(self::$stderr, sprintf('[EMERGENCY] %1$s', $message), $msg_code, $reset_code, $parameter_code);
    }

    /**
     * Formats a given message with the given format code.
     * 
     * @param string $message
     * @param string $format
     * @param string $closing_format
     * @return type
     */
    protected function format_message(string $message, string $format, string $closing_format = 'reset')
    {
        $this->verifyFormatKey($format);
        $this->verifyFormatKey($closing_format);
        return sprintf('%1$s%2$s%3$s', $this->getFormatCode($format), $message, $this->getFormatCode($closing_format));
    }

    /**
     * Checks if a given format key exists.
     * 
     * @param string $format
     * @return void
     * @throws \InvalidArgumentException if the format key is not known.
     */
    protected function verifyFormatKey(string $format): void
    {
        if (!array_key_exists($format, self::$format_codes)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided formatting key [%2$s] is not valid.', get_class($this), $format));
        }
    }

    /**
     * Renders a given message format to the given stream in cli message format.
     * 
     * @param type $stream
     * @param string $message
     * @param string $msg_code
     * @param string $reset_code
     * @param string $parameter_code
     * @return void
     */
    private function renderCliMessage($stream, string $message, string $msg_code = 'reset', string $reset_code = 'reset', string $parameter_code = 'yellow'): void
    {
        $msg = sprintf('%2$s%1$s%3$s%4$s', $message, $this->getFormatCode($msg_code), $this->getFormatCode($reset_code), PHP_EOL);
        fwrite($stream, $msg);
    }

    /**
     * Initializes the I/O streams for cli console interaction.
     * @return void
     */
    private function initializeIOStreams(): void
    {
        if (is_null(self::$stdout)) {
            self::$stdout = fopen('php://stdout', 'w');
        }
        if (is_null(self::$stderr)) {
            self::$stderr = fopen('php://stderr', 'w');
        }
        if (is_null(self::$stdin)) {
            self::$stdin = fopen('php://stdin', 'r');
        }
    }
}
