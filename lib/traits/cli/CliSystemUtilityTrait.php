<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\cli;

/**
 * Cli System Utility Trait
 * Contains common functionality for command line operating system interaction
 *
 * @author Brian Dayhoff
 */
trait CliSystemUtilityTrait
{

    /**
     * The operating system the application is running on
     * @var string
     */
    private static $os = null;

    /**
     * The operating system version the application is running on
     * @var string
     */
    private static $os_version = null;

    /**
     * Returns the name of the operating system the application is running on
     * @return string
     */
    protected function getOs(): string
    {
        return self::$os;
    }

    /**
     * Returns the version of the operating system the application is running on
     * @return string
     */
    protected function getOsVersion(): string
    {
        return self::$os_version;
    }

    private function initializeOs(): void
    {
        if (is_null(self::$base_cwd)) {
            self::$os = php_uname('s');
        }
    }

    private function initializeOsVersion(): void
    {
        if (is_null(self::$os_version)) {
            self::$os_version = php_uname('v');
        }
    }
}
