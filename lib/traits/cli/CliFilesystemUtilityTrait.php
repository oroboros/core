<?php
/*
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\cli;

/**
 * Cli Filesystem Utility Trait
 * Contains common functionality for command line filesystem management
 *
 * @author Brian Dayhoff
 */
trait CliFilesystemUtilityTrait
{

    /**
     * The fully qualified absolute filepath to the initial
     * current working directory on script execution.
     * @var string
     */
    private static $base_cwd = null;

    /**
     * The user groups of the current user, if applicable.
     * @var array
     */
    private static $current_user_groups = null;

    /**
     * The actual current working directory.
     * Fully qualified absolute filepath.
     * @var string
     */
    private $cwd = null;

    /**
     * Returns the fully qualified filesystem path to the current working directory.
     * The given path will be suffixed with a directory separator appropriate
     * to the system running the application.
     * @return string
     */
    protected function getPath(): string
    {
        return $this->cwd;
    }

    /**
     * Sets the current working directory to a new path,
     * and navigates to the given path.
     * @param string $path
     * @return void
     * @throws \InvalidArgumentException If the given path is not a directory.
     */
    protected function setPath(string $path): void
    {
        if (!is_dir($path)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. Provided path [%2$s] is not a directory.', get_class($this), $path));
        }
        $path = realpath($path) . DIRECTORY_SEPARATOR;
        $this->cwd = $path;
        chdir($this->cwd);
    }

    /**
     * Synchronizes the registered current working directory
     * with the literal current working directory.
     * This is always called on instantiation, and on all calls to `getPath`
     * to insure the current path is not out of sync if it is changed elsewhere.
     * 
     * It may be manually called at any time also.
     * 
     * @return void
     */
    protected function syncPath(): void
    {
        $path = getcwd();
        $this->setPath($path);
    }

    /**
     * Returns to the directory that the program initialized in,
     * and sets the current working directory to that path.
     * 
     * This should generally be called after operations that require changing
     * the path to prevent operating in an out of scope directory.
     * @return void
     */
    protected function resetPath(): void
    {
        $this->setPath(self::$base_cwd);
    }

    private function initializeCwd(): void
    {
        if (is_null(self::$base_cwd)) {
            self::$base_cwd = getcwd() . DIRECTORY_SEPARATOR;
        }
        $this->cwd = getcwd() . DIRECTORY_SEPARATOR;
    }
}
