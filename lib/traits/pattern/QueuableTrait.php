<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\pattern;

/**
 * Queuable Trait
 * 
 * Honors the QueuableInterface
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
trait QueuableTrait
{
    /**
     * The queue currently being worked
     * @var \Oroboros\core\interfaces\library\event\EventQueueInterface
     */
    private $queue = null;

    /**
     * The registered event id when an event is acted upon or registered
     * @var scalar
     */
    private $registered_id = null;

    /**
     * The registered data when an event is acted upon or registered
     * @var string
     */
    private $registered_data = null;

    /**
     * The registered queue that the event is operating against when
     * an event is acted upon or registered.
     * @var string
     */
    private $registered_queue = null;

    /**
     * The registered error that the event generated when
     * an event is acted upon and encounters problems.
     * @var \Exception
     */
    private $registered_error = null;

    /**
     * Queues a given action, and fires an observer event `queue`
     * for callbacks to extract event references for external registration.
     * @param string $queue
     * @param array $details
     * @return $this (method chainable)
     */
    public function queue(string $queue, array $details = []): \Oroboros\core\interfaces\pattern\queuable\QueuableInterface
    {
        $this->verifyQueueDetails($details);
//        $event = $this->getAdapter()->queueEvent($queue, $details);
        $this->setEventQueue($queue);
        $this->setEventId($command);
        $this->setEventData($event['data']);
        $this->setObserverEvent('queue');
        $this->resetEventQueue();
        $this->resetEventId();
        $this->resetEventData();
        return $this;
    }
    
    /**
     * Lists the queues that exist for the current event queue type
     * @return array
     */
    public function listQueues(): array
    {
        return array_keys($this->queue->toArray());
    }
    
    /**
     * Returns a EventQueue object corresponding to the specified queue identifier
     * @param string $queue
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     * @throws \InvalidArgumentException If the given queue is not valid
     */
    public function getQueue(string $queue): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        if (!$this->hasQueue($queue)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided queue [%2$s] does not exist.', get_class($this), $queue));
        }
        return $this->queue->get($queue);
    }

    /**
     * Returns a boolean determination as to whether a given queue is valid
     * @param string $queue
     * @return bool
     */
    public function hasQueue(string $queue): bool
    {
        return $this->queue->has($queue);
    }

    /**
     * Returns the id of the current running queue item, if any
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->registered_id;
    }

    /**
     * returns the data set to run on the queue
     * @return string|null
     */
    public function getData(): ?array
    {
        return $this->registered_data;
    }

    /**
     * Gets the name of the queue currently running, if any
     * @return string|null
     */
    public function getQueueItem(): ?string
    {
        return $this->registered_queue;
    }

    /**
     * Gets the error generated from the current queue pointer, if any
     * @return \Exception|null
     */
    public function getError(): ?\Exception
    {
        return $this->registered_error;
    }

    /**
     * Verifies that the queue details match the specification for a valid event queue
     * 
     * @param array $details
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException If the provided
     *         details are missing or not acceptable
     */
    public function verifyQueueDetails(array $details): void
    {
        $resolver = $this->load('library', 'resolver\\Resolver');
        foreach ($this->declareRequiredQueueDetailKeys() as $key => $format) {
            if (!array_key_exists($key, $details)) {
                // Missing required key
                throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                            . 'Required queue detail key [%2$s] was not provided', get_class($this), $key));
            }
            if (!$resolver->resolve($details[$key], $format)) {
                // Wrong format
                throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                            . 'Required queue detail key [%2$s] was does not follow expected format.', get_class($this), $key));
            }
        }
    }

    protected function runQueue(string $queue)
    {
        
    }

    /**
     * Override this method to declare any required queue format keys.
     * Return an associative array where the key is the required key name,
     * and the value is a resolvable type
     * 
     * @see \Oroboros\core\interfaces\library\resolver\ResolverInterface
     * @return array
     */
    protected function declareRequiredQueueDetailKeys(): array
    {
        return [];
    }

    protected function setQueue(string $queue): void
    {
        $this->registered_queue = $queue;
    }

    protected function setId($id): void
    {
        if (!is_scalar($id) || is_null($id)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                sprintf('Error encountered in [%1$s]. Queue id key must be a non-null scalar value.', get_class($this))
            );
        }
        $this->registered_id = $id;
    }

    protected function setData(string $data): void
    {
        $this->registered_data = $data;
    }

    protected function setError(\Exception $error): void
    {
        $this->registered_error = $error;
    }

    protected function resetQueue(): void
    {
        $this->registered_queue = null;
    }

    protected function resetId(): void
    {
        $this->registered_id = null;
    }
    
    protected function resetAction(): void
    {
        $this->registered_action = null;
    }

    protected function resetData(): void
    {
        $this->registered_data = null;
    }

    protected function resetError(): void
    {
        $this->registered_error = null;
    }

    /**
     * Loads the queue if it is not already loaded
     * 
     * Call this in your constructor
     * @param $class The name of a valid container or collection class.
     *        May be a stub name. If not supplied the default container
     *        will be used.
     * @return void
     */
    private function initializeQueue(string $class = null): void
    {
        if (!is_null($this->queue)) {
            return;
        }
        if (is_null($class)) {
            $class = 'Oroboros\\core\\library\\container\\Container';
        }
        $proxy = new \Oroboros\core\pattern\factory\LoaderProxy();
        $classname = $proxy->getFullClassName('library', $class);
        $this->queue = $classname::init(null, []);
    }
}
