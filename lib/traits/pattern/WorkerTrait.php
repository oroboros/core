<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\pattern;

/**
 * Worker Trait
 * Mixin to make classes into workers.
 * The class must also implement the worker interface to be considered a valid worker.
 * 
 * Usage
 * - Implement Oroboros\core\interfaces\pattern\director\WorkerInterface on the class
 * - define class constant WORKER_SCOPE with a string keyword designating the scope of the worker
 * - define class constant WORKER_TASK with a string keyword designating the task name
 * - call `$this->initializeWorker();` in your constructor, _after calling `parent::__construct` (if applicable)_
 * - override public function executeTask, as per the instruction outlined in the comment for that method
 *
 * @author Brian Dayhoff
 * @note Workers task execution should be fully encapsulated within the `executeTask` method.
 *     They may call other methods or objects as required,
 *     but the task should not require additional method calls
 *     within the worker object.
 * @see Oroboros\core\interfaces\pattern\director\WorkerInterface
 */
trait WorkerTrait
{

    /**
     * Represents the director that controls the worker.
     * Workers are expected to forward any tasks outside of their scope
     * back to the director to have them handled by another worker.
     * 
     * @var \Oroboros\core\interfaces\pattern\director\DirectorInterface
     */
    private $director = null;

    /**
     * This method is used by the director to set itself as a dependency of the worker.
     * Workers are expected to forward tasks outside of their scope back to the director
     * to be handled by another worker.
     * @param \Oroboros\core\interfaces\pattern\director\DirectorInterface $director
     * @return void
     */
    public function setDirector(\Oroboros\core\interfaces\pattern\director\DirectorInterface $director): void
    {
        $this->director = $director;
    }

    /**
     * This method is used to obtain the director, in the event that the worker
     * needs to resolve a task outside of its scope.
     * 
     * The worker should call the task back against the director,
     * who will forward the task to another worker to be resolved.
     * 
     * @return \Oroboros\core\interfaces\pattern\director\DirectorInterface
     */
    protected function getDirector(): \Oroboros\core\interfaces\pattern\director\DirectorInterface
    {
        return $this->director;
    }

    private function initializeWorker(): void
    {
        $this->verifyClassScope();
        $this->verifyWorkerScope();
        $this->verifyWorkerTask();
    }

    private function verifyClassScope(): void
    {
        if (is_null(static::CLASS_SCOPE)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(sprintf('Class [%1$s] is invalid.'
                        . 'Class constant [%2$s] must be defined.', get_class($this), 'CLASS_SCOPE'));
        }
    }

    private function verifyWorkerScope(): void
    {
        if (is_null(static::WORKER_SCOPE)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(sprintf('Class [%1$s] is invalid.'
                        . 'Class constant [%2$s] must be defined for a worker class to be valid.', get_class($this), 'WORKER_SCOPE'));
        }
    }

    private function verifyWorkerTask(): void
    {
        if (is_null(static::WORKER_SCOPE)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(sprintf('Class [%1$s] is invalid.'
                        . 'Class constant [%2$s] must be defined for a worker class to be valid.', get_class($this), 'WORKER_TASK'));
        }
    }
}
