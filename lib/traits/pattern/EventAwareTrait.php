<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\pattern;

/**
 * Event Aware Trait
 * Provides the methods required for a class to be event aware
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
trait EventAwareTrait
{

    /**
     * The event manager object
     * 
     * @var \Oroboros\core\interfaces\library\event\EventManagerInterface
     */
    private $event_manager = null;

    /**
     * A list of valid event queues.
     * These are generated any time queues
     * are registered with the manager.
     * @var array
     */
    private $valid_event_queues = [];

    /**
     * The name of the current queue being worked.
     * If no event queue is currently being worked, this will return null.
     * 
     * @var string|null
     */
    private $current_event_queue = null;

    /**
     * The default bindings for the spl observer that
     * makes the event system work correctly.
     * 
     * You may add additional bindings, but do not remove these.
     * 
     * @var array
     */
    private static $default_event_observer_bindings = [
        'event.register' => 'onEventRegister',
        'event.unregister' => 'onEventUnregister',
        'event.run' => 'onEventFire',
        'event.error' => 'onEventError',
        'event.complete' => 'onEventComplete',
        'event.ready' => 'onEventReady',
        'event.run-callback' => 'onEventCallbackFire',
        'event.callback-error' => 'onEventCallbackError',
        'event.callback-complete' => 'onEventCallbackComplete',
        'event.bind-callback' => 'onEventCallbackBind',
        'event.unbind-callback' => 'onEventCallbackUnbind',
        'event.register-queue' => 'onEventQueueRegister',
        'event.unregister-queue' => 'onEventQueueUnregister',
    ];

    /**
     * Represents an array of objects that have been designated to be watching
     * for event registration so they can bind their callbacks.
     * 
     * These objects will always be notified when any instance of the event aware
     * class instantiates they can be bound to it.
     * 
     * @var array
     */
    private static $event_watchers = [];

    /**
     * Represents an array of objects that have been designated to be listening
     * for event fire, complete, and error statuses so they can relay information
     * to an unrelated object.
     * 
     * @var array
     */
    private $event_listeners = [];

    /**
     * Returns a boolean determination as to whether the given object
     * is an event watcher. This is object specific, not class specific.
     * 
     * If the second optional argument is passed as `true`, it will instead
     * return if any class in the watch list matches the class of the given watcher.
     * 
     * @param \Oroboros\core\interfaces\pattern\observer\ObserverInterface $watcher
     * @param bool $check_class If this argument is passed as true, this method
     *        will return whether *any* existing registered watcher matches the
     *        same class as the given watcher. This is useful in the event that
     *        classes are intended to only serve as a registration mechanism,
     *        and should not double-register events.
     * @return bool
     */
    public static function isWatchingEvents(\Oroboros\core\interfaces\pattern\observer\ObserverInterface $watcher, bool $check_class = false): bool
    {
        if (!$check_class) {
            return in_array($watcher, self::$event_watchers, true);
        }
        $class = get_class($watcher);
        foreach (self::$event_watchers as $watcher) {
            if (get_class($watcher) === $class) {
                return true;
            }
        }
        return false;
    }

    /**
     * Adds an object to the watcher list, which will alert it to registration 
     * or unregistration of events, so it can add it's callbacks.
     * 
     * Any object in the watch list will be automatically attached to all new
     * instances of the event aware class.
     * 
     * Objects added to the watch list **will not** be automatically attached
     * to existing instances of the class. They must be added to the watch list
     * prior to the creation of those objects if they need to hook into them.
     * 
     * Watchers will automatically be attached during object instantiation
     * and detached during object destruction.
     * 
     * @param \Oroboros\core\interfaces\pattern\observer\ObserverInterface $watcher
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException
     *         If the given watcher is already in the watch list.
     */
    public static function watchEvents(\Oroboros\core\interfaces\pattern\observer\ObserverInterface $watcher): void
    {
        if (static::isWatchingEvents($watcher)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Provided subject of class '
                        . '[%2$s] is already a registered event watcher.'
                        , static::class, get_class($watcher))
            );
        }
        self::$event_watchers[] = $watcher;
    }

    /**
     * Removes an object from the watcher list, if it exists in it.
     * 
     * This will not trigger existing objects in the watcher list to detach,
     * it will only prevent new object instances from automatically attaching.
     * 
     * If the object does not exist in the list of watchers, it will raise an InvalidArgumentException
     * 
     * @param \Oroboros\core\interfaces\pattern\observer\ObserverInterface $watcher
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException if the given
     *         watcher is not a registered event watcher.
     */
    public static function ignoreEvents(\Oroboros\core\interfaces\pattern\observer\ObserverInterface $watcher): void
    {
        if (!static::isWatchingEvents($watcher)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Provided subject of class '
                        . '[%2$s] is not a registered event watcher.'
                        , static::class, get_class($watcher))
            );
        }
        $key = array_search($watcher, self::$event_watchers, true);
        unset(self::$event_watchers[$key]);
    }

    /**
     * Reinitializes event watchers.
     * 
     * This should be used if the underlying list of event
     * watchers changed since class instantiation.
     * 
     * This will tell the object to release the existing bindings
     * and rebind to the updated master list.
     * 
     * @return void
     */
    public function reinitializeEventWatchers(): void
    {
        $this->unbindEventWatchers();
        $this->initializeEventWatchers();
        $this->setObserverEvent('event-watchers-reinitialized');
    }

    /**
     * Observer event binding for event registration
     * 
     * @param \Oroboros\core\interfaces\library\event\EventManagerInterface $subject
     */
    final public function onEventRegister(\Oroboros\core\interfaces\library\event\EventManagerInterface $subject): void
    {
        $this->handleEventRegister(
            $subject->getCurrentQueue(),
            $subject->getCurrentActionId(),
            $subject->getCurrentAction(),
            $subject
        );
        $key = $subject->getObserverEvent();
        $remove_prefix = trim(sprintf('%1$s:%2$s', 'event-manager', static::CLASS_TYPE), ':.') . '.';
        if (strpos($key, $remove_prefix) === 0) {
            $key = substr($key, strlen($remove_prefix));
        }
        $old_queue = $this->current_event_queue;
        $this->current_event_queue = $subject->getCurrentQueue();
        $this->setObserverEvent($key);
        $this->current_event_queue = $old_queue;
    }

    /**
     * Observer event binding for event unregistration
     * 
     * @param type $subject
     * @return void
     */
    final public function onEventUnregister(\Oroboros\core\interfaces\library\event\EventManagerInterface $subject): void
    {
        $this->handleEventUnregister(
            $subject->getCurrentQueue(),
            $subject->getCurrentActionId(),
            $subject->getCurrentAction(),
            $subject
        );
        $key = $subject->getObserverEvent();
        $remove_prefix = trim(sprintf('%1$s:%2$s', 'event-manager', static::CLASS_TYPE), ':.') . '.';
        if (strpos($key, $remove_prefix) === 0) {
            $key = substr($key, strlen($remove_prefix));
        }
        $old_queue = $this->current_event_queue;
        $this->current_event_queue = $subject->getCurrentQueue();
        $this->setObserverEvent($key);
        $this->current_event_queue = $old_queue;
    }

    /**
     * Observer event binding for event execution
     * 
     * @param type $subject
     * @return void
     */
    final public function onEventFire(\Oroboros\core\interfaces\library\event\EventManagerInterface $subject): void
    {
        $this->handleEventFire(
            $subject->getCurrentQueue(),
            $subject->getCurrentActionId(),
            $subject->getCurrentData(),
            $subject->getCurrentAction(),
            $subject
        );
        $key = $subject->getObserverEvent();
        $remove_prefix = trim(sprintf('%1$s:%2$s', 'event-manager', static::CLASS_TYPE), ':.') . '.';
        if (strpos($key, $remove_prefix) === 0) {
            $key = substr($key, strlen($remove_prefix));
        }
        $old_queue = $this->current_event_queue;
        $this->current_event_queue = $subject->getCurrentQueue();
        $this->setObserverEvent($key);
        $this->current_event_queue = $old_queue;
    }

    /**
     * Observer event binding for event error status
     * 
     * @param type $subject
     * @return void
     */
    final public function onEventError(\Oroboros\core\interfaces\library\event\EventManagerInterface $subject): void
    {
        $this->handleEventError(
            $subject->getCurrentQueue(),
            $subject->getCurrentActionId(),
            $subject->getCurrentCallbackId(),
            $subject->getCurrentError(),
            $subject->getCurrentData(),
            $subject->getCurrentCallback(),
            $subject->getCurrentAction(),
            $subject
        );
        $key = $subject->getObserverEvent();
        $remove_prefix = trim(sprintf('%1$s:%2$s', 'event-manager', static::CLASS_TYPE), ':.') . '.';
        if (strpos($key, $remove_prefix) === 0) {
            $key = substr($key, strlen($remove_prefix));
        }
        $old_queue = $this->current_event_queue;
        $this->current_event_queue = $subject->getCurrentQueue();
        $this->setObserverEvent($key);
        $this->current_event_queue = $old_queue;
    }

    /**
     * Observer event binding for event that has completed running successfully.
     * This fires immediately prior to resettting.
     * 
     * @param type $subject
     * @return void
     */
    final public function onEventComplete(\Oroboros\core\interfaces\library\event\EventManagerInterface $subject): void
    {
        $this->handleEventComplete(
            $subject->getCurrentDetails(),
            $subject
        );
        $key = $subject->getObserverEvent();
        $remove_prefix = trim(sprintf('%1$s:%2$s', 'event-manager', static::CLASS_TYPE), ':.') . '.';
        if (strpos($key, $remove_prefix) === 0) {
            $key = substr($key, strlen($remove_prefix));
        }
        $old_queue = $this->current_event_queue;
        $this->current_event_queue = $subject->getCurrentQueue();
        $this->setObserverEvent($key);
        $this->current_event_queue = $old_queue;
    }

    /**
     * Observer event binding for completion of an event action. This method
     * will only be called when the event manager returns to a clean state.
     * 
     * @param type $subject
     * @return void
     */
    final public function onEventReady(\Oroboros\core\interfaces\library\event\EventManagerInterface $subject): void
    {
        $this->handleEventReady($subject);
        $key = $subject->getObserverEvent();
        $remove_prefix = trim(sprintf('%1$s:%2$s', 'event-manager', static::CLASS_TYPE), ':.') . '.';
        if (strpos($key, $remove_prefix) === 0) {
            $key = substr($key, strlen($remove_prefix));
        }
        $old_queue = $this->current_event_queue;
        $this->current_event_queue = $subject->getCurrentQueue();
        $this->setObserverEvent($key);
        $this->current_event_queue = $old_queue;
    }

    /**
     * Observer event binding for event callback execution
     * 
     * @param type $subject
     * @return void
     */
    final public function onEventCallbackFire(\Oroboros\core\interfaces\library\event\EventManagerInterface $subject): void
    {
        $this->handleEventCallbackFire(
            $subject->getCurrentQueue(),
            $subject->getCurrentActionId(),
            $subject->getCurrentCallbackId(),
            $subject->getCurrentData(),
            $subject->getCurrentCallback(),
            $subject->getCurrentAction(),
            $subject
        );
        $key = $subject->getObserverEvent();
        $remove_prefix = trim(sprintf('%1$s:%2$s', 'event-manager', static::CLASS_TYPE), ':.') . '.';
        if (strpos($key, $remove_prefix) === 0) {
            $key = substr($key, strlen($remove_prefix));
        }
        $old_queue = $this->current_event_queue;
        $this->current_event_queue = $subject->getCurrentQueue();
        $this->setObserverEvent($key);
        $this->current_event_queue = $old_queue;
    }

    /**
     * Observer event binding for event callback error status
     * 
     * @param type $subject
     * @return void
     */
    final public function onEventCallbackError(\Oroboros\core\interfaces\library\event\EventManagerInterface $subject): void
    {
        $this->handleEventCallbackError(
            $subject->getCurrentQueue(),
            $subject->getCurrentActionId(),
            $subject->getCurrentCallbackId(),
            $subject->getCurrentError(),
            $subject->getCurrentData(),
            $subject->getCurrentCallback(),
            $subject->getCurrentAction(),
            $subject
        );
        $key = $subject->getObserverEvent();
        $remove_prefix = trim(sprintf('%1$s:%2$s', 'event-manager', static::CLASS_TYPE), ':.') . '.';
        if (strpos($key, $remove_prefix) === 0) {
            $key = substr($key, strlen($remove_prefix));
        }
        $old_queue = $this->current_event_queue;
        $this->current_event_queue = $subject->getCurrentQueue();
        $this->setObserverEvent($key);
        $this->current_event_queue = $old_queue;
    }

    /**
     * Observer event binding for event callback error status
     * 
     * @param type $subject
     * @return void
     */
    final public function onEventCallbackComplete(\Oroboros\core\interfaces\library\event\EventManagerInterface $subject): void
    {
        $this->handleEventCallbackComplete(
            $subject->getCurrentQueue(),
            $subject->getCurrentActionId(),
            $subject->getCurrentCallbackId(),
            $subject->getCurrentData(),
            $subject->getCurrentCallbackResult(),
            $subject->getCurrentCallback(),
            $subject->getCurrentAction(),
            $subject
        );
        $key = $subject->getObserverEvent();
        $remove_prefix = trim(sprintf('%1$s:%2$s', 'event-manager', static::CLASS_TYPE), ':.') . '.';
        if (strpos($key, $remove_prefix) === 0) {
            $key = substr($key, strlen($remove_prefix));
        }
        $old_queue = $this->current_event_queue;
        $this->current_event_queue = $subject->getCurrentQueue();
        $this->setObserverEvent($key);
        $this->current_event_queue = $old_queue;
    }

    /**
     * Observer event binding for event callback binding
     * 
     * @param type $subject
     * @return void
     */
    final public function onEventCallbackBind(\Oroboros\core\interfaces\library\event\EventManagerInterface $subject): void
    {
        $this->handleEventCallbackBind(
            $subject->getCurrentQueue(),
            $subject->getCurrentActionId(),
            $subject->getCurrentCallbackId(),
            $subject->getCurrentCallback(),
            $subject->getCurrentAction(),
            $subject
        );
        $key = $subject->getObserverEvent();
        $remove_prefix = trim(sprintf('%1$s:%2$s', 'event-manager', static::CLASS_TYPE), ':.') . '.';
        if (strpos($key, $remove_prefix) === 0) {
            $key = substr($key, strlen($remove_prefix));
        }
        $old_queue = $this->current_event_queue;
        $this->current_event_queue = $subject->getCurrentQueue();
        $this->setObserverEvent($key);
        $this->current_event_queue = $old_queue;
    }

    /**
     * Observer event binding for event callback unbinding
     * 
     * @param type $subject
     * @return void
     */
    final public function onEventCallbackUnbind(\Oroboros\core\interfaces\library\event\EventManagerInterface $subject): void
    {
        $this->handleEventCallbackUnbind(
            $subject->getCurrentQueue(),
            $subject->getCurrentActionId(),
            $subject->getCurrentCallbackId(),
            $subject->getCurrentCallback(),
            $subject->getCurrentAction(),
            $subject
        );
        $key = $subject->getObserverEvent();
        $remove_prefix = trim(sprintf('%1$s:%2$s', 'event-manager', static::CLASS_TYPE), ':.') . '.';
        if (strpos($key, $remove_prefix) === 0) {
            $key = substr($key, strlen($remove_prefix));
        }
        $old_queue = $this->current_event_queue;
        $this->current_event_queue = $subject->getCurrentQueue();
        $this->setObserverEvent($key);
        $this->current_event_queue = $old_queue;
    }

    /**
     * Observer event binding for event queue registration
     * 
     * @param type $subject
     * @return void
     */
    final public function onEventQueueRegister(\Oroboros\core\interfaces\library\event\EventManagerInterface $subject): void
    {
        $this->handleEventQueueRegister(
            $subject->getCurrentQueue(),
            $subject
        );
    }

    /**
     * Observer event binding for event queue unregistration
     * 
     * @param type $subject
     * @return void
     */
    final public function onEventQueueUnregister(\Oroboros\core\interfaces\library\event\EventManagerInterface $subject): void
    {
        $this->handleEventQueueUnregister(
            $subject->getCurrentQueue(),
            $subject
        );
    }

    /**
     * Imports an event broker and registers any events/callbacks designated by it.
     * 
     * @param \Oroboros\core\interfaces\library\event\EventBrokerInterface $broker
     * @return void
     */
    public function importEventBroker(\Oroboros\core\interfaces\library\event\EventBrokerInterface $broker): void
    {
        if (!$broker->analyze($this)) {
            // Nothing to do
            return;
        }
        $data = $broker->resolve($this);
        foreach ($data as $queue => $events) {
            if (!$this->hasEventQueue($queue)) {
                $this->registerEventQueue($queue);
            }
            foreach ($events as $event => $callbacks) {
                if (!$this->hasEvent($event, $queue)) {
                    $this->registerEvent($event, $queue);
                }
                foreach ($callbacks as $key => $callback) {
                    if (!$this->hasEventCallback($event, $key, $queue)) {
                        if (!is_callable($callback)) {
                            throw new \Oroboros\core\exception\ErrorException(
                                    sprintf('Error encountered in [%1$s]. Provided '
                                        . 'broker supplied callback [%2$s] in event '
                                        . '[%3$s] of queue [%4$s] by broker [%5$s] '
                                        . 'is not a valid callback.'
                                        , get_class($this), $key, $event, $queue, get_class($broker))
                            );
                        }
                        $this->bindEventCallback($event, $key, $callback, $queue);
                    }
                }
            }
        }
    }

    /**
     * Returns a boolean designation as to whether a given event queue exists.
     * 
     * @param string $queue
     * @return bool
     */
    public function hasEventQueue(string $queue): bool
    {
        return $this->getEventManager()->hasQueue($queue);
    }

    /**
     * Returns a list of the names of valid queues for this object.
     * 
     * @return array
     */
    public function listEventQueues(): array
    {
        return $this->getEventManager()->listQueues();
    }

    /**
     * Returns a boolean designation as to whether a given event exists
     * in the current scope, or in an arbitrarily specified scope if the
     * second argument is passed.
     * 
     * If this returns false, this class does not have the specified event,
     * and attempts to bind callbacks to it will raise an exception.
     * 
     * If this returns true, the event is valid and callbacks may be bound to it,
     * provided they are not bound to an existing key name.
     * 
     * @param string $event The event name
     * @param string|null $queue The queue name if supplied, or the currently scoped queue if not
     * @return bool
     */
    public function hasEvent(string $event, string $queue = null): bool
    {
        if (is_null($queue)) {
            $queue = $this->getCurrentEventQueue();
        }
        return $this->getEventManager()->hasAction($queue, $event);
    }

    /**
     * Lists all the events in the given queue, or the currently scoped
     * queue if no queue is supplied.
     * 
     * @param string $queue
     * @return array
     * @throws \Oroboros\core\exception\InvalidArgumentException
     *         if a queue name is given that does not exist.
     */
    public function listEvents(string $queue = null): array
    {
        if (is_null($queue)) {
            $queue = $this->getCurrentEventQueue();
        }
        if (!$this->hasEventQueue($queue)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Specified queue name '
                        . '[%2$s] does not exist.', get_class($this) . $queue)
            );
        }
        return $this->getEventManager()->listActions($queue);
    }

    /**
     * Returns a boolean designation as to whether a given event has a bound
     * callback of the specified keyname
     * 
     * This will return false if the event does not exist, or if it does but
     * the given key is not bound to it.
     * 
     * If `hasEvent` returns `true` for the same event and this method
     * returns `false`, then it is safe to bind an event callback for
     * the specified key.
     * 
     * If both this method and `hasEvent` return `false` for the same event,
     * or if both return `true`, attempting to bind a callback with the
     * specified key will raise an exception. In the first case, the event does
     * not exist, and in the second, the key is already in use.
     * 
     * @param string $event The event name
     * @param string|null $queue The queue name if supplied, or the currently scoped queue if not
     * @param string $key The identifier for the given callback
     * @return bool
     */
    public function hasEventCallback(string $event, string $key, string $queue = null): bool
    {
        if (is_null($queue)) {
            $queue = $this->getCurrentEventQueue();
        }
        if (!$this->hasEvent($event, $queue)) {
            return false;
        }
        return $this->getEventManager()->hasActionCallback($queue, $event, $key);
    }

    /**
     * Binds a callback to the given event in the specified queue,
     * under the name of `$key`
     * 
     * @param string $event The name of the event to bind the callback to
     * @param string $key The identifier for the callback
     * @param callable $callback The callback to fire when the event fires
     * @param string|null $queue The queue to bind a callback into. If no queue 
     *        is supplied, the currently scoped queue will be used.
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException if the manager
     *         reports an error binding the callback. This can happen if the
     *         callback does not validate, or if the key is already registered.
     */
    public function bindEventCallback(string $event, string $key, callable $callback, string $queue = null): void
    {
        if (is_null($queue)) {
            $queue = $this->getCurrentEventQueue();
        }
        if (!$this->hasEvent($event, $queue)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. No event with '
                        . 'identifier [%2$s] exists in event scope [%3$s].'
                        , get_class($this), $event, $queue)
            );
        }
        if ($this->hasEventCallback($event, $key, $queue)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Callback with '
                        . 'identifier [%2$s] already has a callback bound to '
                        . 'event [%3$s] of scope [%4$s].'
                        , get_class($this), $key, $event, $queue)
            );
        }
        $this->getEventManager()->bindActionCallback($queue, $event, $key, $callback);
    }

    /**
     * Removes a given event callback from the event and queue specified.
     * If no queue is specified, the currently scoped queue will be used.
     * 
     * @param string $event The name of the event to bind the callback to
     * @param string $key The identifier for the callback
     * @param string|null $queue The queue to bind a callback into. If no queue 
     *        is supplied, the currently scoped queue will be used.
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    public function unbindEventCallback(string $event, string $key, string $queue = null): void
    {
        if (is_null($queue)) {
            $queue = $this->getCurrentEventQueue();
        }
        if (!$this->hasEvent($event, $queue)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. No event with '
                        . 'identifier [%2$s] exists in event scope [%3$s].'
                        , get_class($this), $event, $queue)
            );
        }
        if (!$this->hasEventCallback($event, $key, $queue)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Callback with '
                        . 'identifier [%2$s] does not have a callback bound to '
                        . 'event [%3$s] of scope [%4$s].'
                        , get_class($this), $key, $event, $queue)
            );
        }
        $this->getEventManager()->unbindActionCallback($queue, $event, $key);
    }

    /**
     * Lists the event callbacks in the specified event of the given queue, or
     * the currently scoped queue if no queue identifier is passed.
     * 
     * @param string $event
     * @param string $queue
     * @return array
     * @throws \Oroboros\core\exception\InvalidArgumentException
     *         If a given event or queue do not exist.
     */
    public function listEventCallbacks(string $event, string $queue = null): array
    {
        if (is_null($queue)) {
            $queue = $this->getCurrentEventQueue();
        }
        if (!$this->hasEvent($event, $queue)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. No event with '
                        . 'identifier [%2$s] exists in event scope [%3$s].'
                        , get_class($this), $event, $queue)
            );
        }
        return $this->getEventManager()->listActionCallbacks($queue, $event);
    }

    /**
     * Returns the currently scoped event id during an event run, registration, or unregistration
     * If an event is not running, will return null.
     * 
     * @return string|null
     */
    public function getCurrentEventId(): ?string
    {
        return $this->getEventManager()->getCurrentActionId();
    }

    /**
     * Returns the currently scoped event during an event run, registration, or unregistration
     * If an event is not running, will return null.
     * 
     * @return \Oroboros\core\interfaces\library\event\EventInterface|null
     */
    public function getCurrentEvent(): ?\Oroboros\core\interfaces\library\event\EventInterface
    {
        return $this->getEventManager()->getCurrentAction();
    }

    /**
     * Returns the currently scoped event error an event error status
     * If an event is not running, will return null.
     * 
     * @return \Exception|null
     */
    public function getCurrentEventError(): ?\Exception
    {
        return $this->getEventManager()->getCurrentError();
    }

    /**
     * Returns the currently scoped callback id during an event run, callback run,
     * event callback registration, or event callback unregistration
     * If an event is not running, will return null.
     * 
     * @return string|null
     */
    public function getCurrentEventCallbackId(): ?string
    {
        return $this->getEventManager()->getCurrentCallbackId();
    }

    /**
     * Returns the currently scoped callback during an event run, event callback run,
     * event callback registration, or event callback unregistration
     * If an event is not running, will return null.
     * 
     * @return callable|null
     */
    public function getCurrentEventCallback(): ?callable
    {
        return $this->getEventManager()->getCurrentCallback();
    }

    /**
     * Returns the scope of the current event queue.
     * 
     * @return string
     * @throws \Oroboros\core\exception\ErrorException If no queue has been 
     *         scoped at all. Any class utilizing the event system should
     *         scope it's initial queue within it's constructor, and update
     *         the given queue as appropriate throughout runtime.
     */
    public function getCurrentEventQueue(): string
    {
        if (is_null($this->current_event_queue)) {
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. '
                        . 'No event queue is currently scoped.', get_class($this))
            );
        }
        return $this->current_event_queue;
    }

    /**
     * Fires a single event action by name.
     * 
     * if the supplied event, queue, or action key do not match, an invalid argument exception will be raised.
     * 
     * @param string $event The name of the event containng the action
     * @param string $key The name of the action
     * @param array $arguments Any arguments to hand to the event action.
     * @param string|null $queue The name of the queue containing the action.
     *        If none is supplied, the currently scoped queue will be used.
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException If the queue
     *         does not exist, the event does not exist in the queue, or the
     *         action does not exist in the event.
     */
    protected function fireEventCallback(string $event, string $key, array $arguments = [], string $queue = null): void
    {
        if (is_null($queue)) {
            $queue = $this->getCurrentEventQueue();
        }
        if (!$this->hasEvent($event, $queue)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. No event with '
                        . 'identifier [%2$s] exists in event scope [%3$s].'
                        , get_class($this), $event, $queue)
            );
        }
        if (!$this->hasEventCallback($event, $key, $queue)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Callback with '
                        . 'identifier [%2$s] does not have a callback bound to '
                        . 'event [%3$s] of scope [%4$s].'
                        , get_class($this), $key, $event, $queue)
            );
        }
        $this->getEventManager()->runActionCallback($queue, $event, $key, $arguments);
    }

    /**
     * Returns a boolean designation as to whether the current event
     * has been marked as recoverable or not.
     * 
     * This value is meaningless if an event is not currently running.
     * 
     * @return bool
     */
    protected function isEventErrorRecoverable(): bool
    {
        return $this->getEventManager()->isErrorRecoverable();
    }

    /**
     * Marks an error during an event as recoverable, which will allow the event
     * to continue running and prevent the exception from bubbling up in the
     * main thread.
     * 
     * @return void
     */
    protected function markEventErrorRecoverable(): void
    {
        $this->getEventManager()->markErrorRecoverable();
    }

    /**
     * Returns a boolean designation as to whether the current event
     * has been marked as resolved.
     * 
     * This value is meaningless if an event is not currently running.
     * 
     * @return bool
     */
    protected function isEventResolved(): bool
    {
        return $this->getEventManager()->isActionResolved();
    }

    /**
     * Marks an event as resolved. If called during a running event,
     * this will tell the event queue to halt further callback execution
     * and return the existing results as is.
     * 
     * This has no effect if no event is currently running.
     * 
     * @return void
     */
    protected function markEventResolved(): void
    {
        $this->getEventManager()->markActionResolved();
    }

    /**
     * Sets the event queue scope to the given event queue.
     * This will insure that only currently scoped events fire.
     * 
     * @param string $queue
     * @return void
     * @throws \Oroboros\core\exception\ErrorException If an attempt is made to
     *         scope to an event queue that does not exist.
     */
    protected function scopeEventQueue(string $queue): void
    {
        if (!$this->hasEventQueue($queue)) {
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. Provided event queue '
                        . '[%2$s] does not exist.', get_class($this), $queue)
            );
        }
        $this->current_event_queue = $queue;
    }

    /**
     * Registers an event queue with the event manager.
     * 
     * @param string $queue
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException if an attempt
     *         is made to register a queue that already exists.
     */
    protected function registerEventQueue(string $queue): void
    {
        try {
            $this->getEventManager()->registerQueue($queue);
            $this->valid_event_queues = $this->getEventManager()->listQueues();
        } catch (\Oroboros\core\exception\InvalidArgumentException $e) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. '
                        . 'Provided event queue [%2$s] is already '
                        . 'registered.', get_class($this), $queue)
                    , $e->getCode(), $e);
        }
    }

    /**
     * Unregisters an event queue, which unbinds all events within it and
     * releases all callbacks registered to them.
     * 
     * @param string $queue
     * @return void
     * @throws \Oroboros\core\exception\ErrorException if an attempt is made
     *         to unregister the currently scoped queue.
     * @throws \Oroboros\core\exception\InvalidArgumentException if an attempt
     *         is made to unregister a non-existent queue.
     */
    protected function unregisterEventQueue(string $queue): void
    {
        if ($queue === $this->current_event_queue) {
            // Disallow unregistering the current queue
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. Cannot unregister the '
                        . 'current event queue [%2$s].', get_class($this), $queue)
            );
        }
        try {
            // Event manager will handle missing queue issues
            $this->getEventManager()->unregisterQueue($queue);
            $this->valid_event_queues = $this->getEventManager()->listQueues();
        } catch (\Oroboros\core\exception\InvalidArgumentException $e) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. '
                        . 'Provided event queue [%2$s] is already '
                        . 'registered.', get_class($this), $queue)
                    , $e->getCode(), $e);
        }
    }

    /**
     * Returns the event manager object
     * 
     * @return \Oroboros\core\interfaces\library\event\EventManagerInterface
     * @throws \Oroboros\core\exception\core\InvalidClassException If the implementing
     *         class has not called the initialization method, which makes it
     *         impossible to return the event manager.
     */
    protected function getEventManager(): \Oroboros\core\interfaces\library\event\EventManagerInterface
    {
        if (is_null($this->event_manager)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. Referenced the event '
                        . 'manager before calling method [%2$s]. '
                        . 'This class is not useable in it\'s current state.', get_class($this), 'initializeEvents')
            );
        }
        return $this->event_manager;
    }
    /**
     * -------------------------------------------------------------------------
     * Observer Internal Bindings
     * 
     * These methods will be called when various event manager observer events
     * fire. They have been structured to provide the relevant data to each
     * particular case, and can be overridden to handle specifics on a case by
     * case basis. The default behavior is to do nothing.
     * -------------------------------------------------------------------------
     */

    /**
     * Observer event binding for event registration
     * 
     * This can be overridden if you want to track event registration and
     * perform additional steps, such as importing callbacks into it.
     * 
     * Default behavior is to do nothing.
     * 
     * @param string $queue The name of the queue an action was entered into
     * @param string $id The name of the event entered into the queue
     * @param \Oroboros\core\interfaces\library\event\EventInterface $event
     *        The event object registered
     * @param \Oroboros\core\interfaces\library\event\EventManagerInterface $manager
     *        The event manager that triggered the event
     * @return void
     */
    protected function handleEventRegister(string $queue, string $id, \Oroboros\core\interfaces\library\event\EventInterface $event, \Oroboros\core\interfaces\library\event\EventManagerInterface $manager): void
    {
        // no-op
    }

    /**
     * Observer event binding for event unregistration
     * 
     * This can be overridden if you want to track when entire events are
     * removed and perform additional steps.
     * 
     * Default behavior is to do nothing.
     * 
     * @param string $queue The name of the queue an action was removed from
     * @param string $id The name of the event removed from the queue
     * @param \Oroboros\core\interfaces\library\event\EventInterface $event
     *        The event object unregistered
     * @param \Oroboros\core\interfaces\library\event\EventManagerInterface $manager
     *        The event manager that triggered the event
     * @return void
     */
    protected function handleEventUnregister(string $queue, string $id, \Oroboros\core\interfaces\library\event\EventInterface $event, \Oroboros\core\interfaces\library\event\EventManagerInterface $manager): void
    {
        // no-op
    }

    /**
     * Observer event binding for event execution
     * 
     * This can be overridden if you want to track when specific events fire
     * and perform additional steps prior to any callbacks running.
     * 
     * Default behavior is to do nothing.
     * 
     * @param string $queue The queue containing the currently firing event
     * @param string $event_id The identifier for the currently firing event
     * @param array $data The data passed to the event when it was called
     * @param \Oroboros\core\interfaces\library\event\EventInterface $event
     *        The currently firing event object
     * @param \Oroboros\core\interfaces\library\event\EventManagerInterface $manager
     *        The event manager that triggered the observer event
     * @return void
     */
    protected function handleEventFire(string $queue, string $event_id, array $data, \Oroboros\core\interfaces\library\event\EventInterface $event, \Oroboros\core\interfaces\library\event\EventManagerInterface $manager): void
    {
        // no-op
    }

    /**
     * Observer event binding for event error status
     * 
     * This can be overridden if you want to mitigate the failure of an
     * event run, and log errors, perform rollbacks, or any other
     * relevant cleanup.
     * 
     * Default behavior is to do nothing.
     * 
     * @param string $queue The queue containing the currently firing event
     * @param string $event_id The identifier for the currently firing event
     * @param string $callback_id The identifier for the currently firing callback
     * @param \Exception $error The exception raised during the event
     * @param array $data The data passed to the event when it was called
     * @param callable $callback The literal callback that is currently
     *        in scope at the time of the exception
     * @param \Oroboros\core\interfaces\library\event\EventInterface $event
     *        The currently firing event object
     * @param \Oroboros\core\interfaces\library\event\EventManagerInterface $manager
     *        The event manager that triggered the observer event
     * @return void
     */
    protected function handleEventError(string $queue, string $event_id, string $callback_id, \Exception $error, array $data, callable $callback, \Oroboros\core\interfaces\library\event\EventInterface $event, \Oroboros\core\interfaces\library\event\EventManagerInterface $manager): void
    {
        // no-op
    }

    /**
     * Fires when the event manager completes a clean execution.
     * 
     * This can be overridden if you want to analyze the final results of
     * a completed event.
     * 
     * Default behavior is to do nothing.
     * 
     * @param array $details An array of all relevant event completion details.
     *        Values will vary based on the specific event that fired but keys are consistent.
     * @param \Oroboros\core\interfaces\library\event\EventManagerInterface $manager
     *        The event manager that triggered the observer event
     * @return void
     */
    protected function handleEventComplete(array $details, \Oroboros\core\interfaces\library\event\EventManagerInterface $manager): void
    {
        // no-op
    }

    /**
     * Fires when the event manager returns to ready state.
     * 
     * This can be overridden if you want a continuous event cycle, where the
     * next is kicked off immediately upon completion of the last.
     * 
     * Default behavior is to do nothing.
     * 
     * @param \Oroboros\core\interfaces\library\event\EventManagerInterface $manager
     *        The event manager that triggered the observer event
     * @return void
     */
    protected function handleEventReady(\Oroboros\core\interfaces\library\event\EventManagerInterface $manager): void
    {
        // no-op
    }

    /**
     * Observer event binding for event callback execution
     * 
     * This can be overridden if you want to call some other process when
     * specific callback events fire.
     * 
     * Default behavior is to do nothing.
     * 
     * @param string $queue The queue containing the currently firing event
     * @param string $event_id The identifier for the currently firing event
     * @param string $callback_id The identifier for the currently firing callback
     * @param array $data The data passed to the event when it was called
     * @param callable $callback The literal callback that is currently firing
     * @param \Oroboros\core\interfaces\library\event\EventInterface $event
     *        The currently firing event object
     * @param \Oroboros\core\interfaces\library\event\EventManagerInterface $manager
     *        The event manager that triggered the observer event
     * @return void
     */
    protected function handleEventCallbackFire(string $queue, string $event_id, string $callback_id, array $data, callable $callback, \Oroboros\core\interfaces\library\event\EventInterface $event, \Oroboros\core\interfaces\library\event\EventManagerInterface $manager): void
    {
        // no-op
    }

    /**
     * Observer event binding for event callback error status
     * 
     * This can be overridden if you want to log errors, break operation,
     * or perform rollback steps to mitigate problems.
     * 
     * Default behavior is to do nothing.
     * 
     * @param string $queue The queue containing the currently firing event
     * @param string $event_id The identifier for the currently firing event
     * @param string $callback_id The identifier for the currently firing callback
     * @param \Exception $error The exception raised during the callback
     * @param array $data The data passed to the event when it was called
     * @param callable $callback The literal callback that is currently firing
     * @param \Oroboros\core\interfaces\library\event\EventInterface $event
     *        The currently firing event object
     * @param \Oroboros\core\interfaces\library\event\EventManagerInterface $manager
     *        The event manager that triggered the observer event
     * @return void
     */
    protected function handleEventCallbackError(string $queue, string $event_id, string $callback_id, \Exception $error, array $data, callable $callback, \Oroboros\core\interfaces\library\event\EventInterface $event, \Oroboros\core\interfaces\library\event\EventManagerInterface $manager): void
    {
        // no-op
    }

    /**
     * Observer event binding for event callback error status
     * 
     * This can be overridden if you want to obtain a specific callback
     * operation result, or if you want to tell the event manager to
     * break operation during a run because the objective has already
     * been handled for the specific event.
     * 
     * Default behavior is to do nothing.
     * 
     * @param string $queue The queue that the last callback ran in
     * @param string $event_id The event id of the event the last callback ran in
     * @param string $callback_id The identifier for the event that just completed
     * @param array $data The data passed to the event callback
     * @param type $result The return result of the callback
     * @param callable $callback The literal callable callback
     * @param \Oroboros\core\interfaces\library\event\EventInterface $event
     *        The event object containing the callback that just ran
     * @param \Oroboros\core\interfaces\library\event\EventManagerInterface $manager
     *        The event manager that triggered the observer event
     * @return void
     */
    protected function handleEventCallbackComplete(string $queue, string $event_id, string $callback_id, array $data, $result, callable $callback, \Oroboros\core\interfaces\library\event\EventInterface $event, \Oroboros\core\interfaces\library\event\EventManagerInterface $manager): void
    {
        // no-op
    }

    /**
     * Observer event binding for event callback binding
     * 
     * This can be overridden if you want to track callback event binds,
     * which can be useful for debugging how pluggables and exterior objects
     * hook into the events of the class
     * 
     * Default behavior is to do nothing.
     * 
     * @param string $queue The name of the queue that had a callback bound to one of it's events
     * @param string $event_id The name of the event the callback was bound to
     * @param string $callback_id The name of the callback
     * @param callable $callback The callable callback reference
     * @param \Oroboros\core\interfaces\library\event\EventInterface $event The event object
     * @param \Oroboros\core\interfaces\library\event\EventManagerInterface $manager
     *        The event manager object that triggered the observer event
     * @return void
     */
    protected function handleEventCallbackBind(string $queue, string $event_id, string $callback_id, callable $callback, \Oroboros\core\interfaces\library\event\EventInterface $event, \Oroboros\core\interfaces\library\event\EventManagerInterface $manager): void
    {
        // no-op
    }

    /**
     * Observer event binding for event callback unbinding
     * 
     * This can be overridden if you want to track callback event unbinds,
     * which can be useful for debugging how pluggables and exterior objects
     * hook into the events of the class
     * 
     * Default behavior is to do nothing.
     * 
     * @param string $queue The name of the queue that had a callback unbound from one of its events
     * @param string $event_id The name of the event the callback was unbound from
     * @param string $callback_id The name of the callback
     * @param callable $callback The callable callback reference
     * @param \Oroboros\core\interfaces\library\event\EventInterface $event The event object
     * @param \Oroboros\core\interfaces\library\event\EventManagerInterface $manager
     *        The event manager object that triggered the observer event
     * @return void
     */
    protected function handleEventCallbackUnbind(string $queue, string $event_id, string $callback_id, callable $callback, \Oroboros\core\interfaces\library\event\EventInterface $event, \Oroboros\core\interfaces\library\event\EventManagerInterface $manager): void
    {
        // no-op
    }

    /**
     * Observer event binding for event queue registration
     * 
     * This can be overridden if you want a to run post-processes or
     * register events when a new queue is registered.
     * 
     * Default behavior is to do nothing.
     * 
     * @param string $queue The name of the newly registered queue
     * @param \Oroboros\core\interfaces\library\event\EventManagerInterface $manager
     *        The event manager that triggered the observer event
     * @return void
     */
    protected function handleEventQueueRegister(string $queue, \Oroboros\core\interfaces\library\event\EventManagerInterface $manager): void
    {
        // no-op
    }

    /**
     * Observer event binding for event queue unregistration
     * 
     * This can be overridden if you want to run post-processes
     * when an event queue is removed.
     * 
     * Default behavior is to do nothing.
     * 
     * @param string $queue The name of the unregistered queue
     * @param \Oroboros\core\interfaces\library\event\EventManagerInterface $manager
     *        The event manager that triggered the observer event
     * @return void
     */
    protected function handleEventQueueUnregister(string $queue, \Oroboros\core\interfaces\library\event\EventManagerInterface $manager): void
    {
        // no-op
    }

    /**
     * You may override this method to add additional event observer bindings,
     * however you MUST also include the results of
     * `parent::declareEventManagerObserverEventBindings()` or you will break
     * your class event system.
     * 
     * @return array
     */
    protected function declareEventManagerObserverEventBindings(): array
    {
        return self::$default_event_observer_bindings;
    }

    /**
     * Override this method to declare the default event queues
     * for the current class.
     * 
     * These will be registered with the event manager automatically.
     * 
     * @return array
     */
    protected function declareDefaultEventQueues(): array
    {
        return [];
    }

    /**
     * Creates a new event within a given queue (or the currently scoped queue
     * if not supplied). The event queue will be created if it does not exist.
     * 
     * @param string $event
     * @param string $queue
     * @return void
     */
    protected function registerEvent(string $event, string $queue = null): void
    {
        if (is_null($queue)) {
            $queue = $this->getCurrentEventQueue();
        }
        try {
            // Create the queue if it does not already exist
            if (!$this->getEventManager()->hasQueue($queue)) {
                $this->registerEventQueue($queue);
            }
            $this->getEventManager()->registerAction($queue, $event);
        } catch (\Oroboros\core\exception\InvalidArgumentException $e) {
            // Only occurs with a pre-existing event name
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Could not create event '
                        . '[%2$s] in queue [%3$] because it '
                        . 'already exists.', get_class($this), $event, $queue)
            );
        }
    }

    /**
     * Fires a specific event. All of the actions within the event will run with the given arguments.
     * 
     * If the supplied queue or event do not exist, an invalid argument exception will be raised.
     * 
     * @param string $event The name of the event
     * @param array $arguments Any arguments to hand to the event
     * @param string|null $queue The name of the queue containing the event.
     *        If none is supplied, the currently scoped queue will be used.
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException If the queue
     *         does not exist or the event does not exist in the queue.
     */
    protected function fireEvent(string $event, array $arguments = [], string $queue = null): void
    {
        if (is_null($queue)) {
            $queue = $this->getCurrentEventQueue();
        }
        if (!$this->hasEvent($event, $queue)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. No event with '
                        . 'identifier [%2$s] exists in event scope [%3$s].'
                        , get_class($this), $event, $queue)
            );
        }
        $this->getEventManager()->runAction($queue, $event, $arguments);
    }

    /**
     * Unregisters an event from the specified queue, releasing all observer 
     * bindings to it and deleting all registered callbacks for it.
     * 
     * If no queue is supplied, the currently scoped queue will be used.
     * 
     * @param string $event
     * @param string $queue
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException If the given
     *         event does not exist.
     */
    protected function unregisterEvent(string $event, string $queue = null): void
    {
        if (is_null($queue)) {
            $queue = $this->getCurrentEventQueue();
        }
        try {
            // Create the queue if it does not already exist
            if (!$this->getEventManager()->hasQueue($queue)) {
                $this->registerEventQueue($queue);
            }
            $this->getEventManager()->unregisterAction($queue, $event);
        } catch (\Oroboros\core\exception\InvalidArgumentException $e) {
            // Only occurs with a non-existent event name
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Could not remove event '
                        . '[%2$s] in queue [%3$s] because it '
                        . 'does not exist.', get_class($this), $event, $queue)
            );
        }
    }

    /**
     * Adds the spl observer bindings for the event manager
     * 
     * @return void
     */
    private function bindEventObserverEvents(): void
    {
        foreach ($this->declareEventManagerObserverEventBindings() as $key => $callback) {
            $key = trim(sprintf('%1$s:%2$s.%3$s', 'event-manager', static::CLASS_TYPE, trim($key, '.')), '.');
            $callback = [$this, $callback];
            $this->bindObserverEvent($key, $callback);
        }
    }

    /**
     * Removes the spl observer bindings for the event manager
     * 
     * @return void
     */
    private function unbindEventObserverEvents(): void
    {
        foreach ($this->declareEventManagerObserverEventBindings() as $key => $callback) {
            $key = trim(sprintf('%1$s.%2$s.%3$s', 'event-manager', static::CLASS_TYPE, trim($key, '.')), '.');
            $this->unbindObserverEvent($key);
        }
    }

    /**
     * Notifies event watchers of a relevant event action.
     * 
     * @param string $type The type of action performed (eg: register, unregister, etc)
     * @param string $queue The queue that the action applies to (eg: init, ready, error, shutdown, etc)
     * @param string $event The name of the event affected
     * @return void
     */
    private function updateEventWatchers(string $type, string $queue, string $event): void
    {
        $key = trim(sprintf('%1$s.%2$s.%3$s', $type, $queue, $event), '.');
        $this->setObserverEvent($key);
    }

    /**
     * Call this method in the constructor of the event aware class to initialize
     * @return void
     */
    private function initializeEvents(): void
    {
        if (is_null($this->event_manager)) {
            $this->initializeEventWatchers();
            $this->event_manager = $this->load('library', 'event\\EventStrategy', $this->getCommand(), $this->getArguments(), $this->getFlags())->bind($this);
            $this->bindEventObserverEvents();
            $this->initializeDefaultEventQueues();
            // Use the default event queue if none is specified and the default exists
            if (!$this->hasEventQueue('init')) {
                $this->registerEventQueue('init');
            }
            $this->scopeEventQueue('init');
            if ($this->hasArgument('event-broker') && ($this->getArgument('event-broker') instanceof \Oroboros\core\interfaces\library\event\EventBrokerInterface)) {
                $this->importEventBroker($this->getArgument('event-broker'));
            }
        }
    }

    /**
     * Performs the binding of the current object to registered watchers,
     * so that they may inject events as needed into new event queues.
     * 
     * @return void
     */
    private function initializeEventWatchers(): void
    {
        foreach (self::$event_watchers as $watcher) {
            $this->attach($watcher);
        }
    }

    /**
     * Removes bindings of existing watchers from the object.
     * This should be run in the destructor.
     * 
     * @return void
     */
    private function unbindEventWatchers(): void
    {
        foreach (self::$event_watchers as $watcher) {
            $this->detach($watcher);
        }
    }

    /**
     * Performs initialization of any declared default event queues
     * 
     * @return void
     */
    private function initializeDefaultEventQueues(): void
    {
        $defaults = $this->declareDefaultEventQueues();
        foreach ($defaults as $key => $value) {
            if (is_int($key)) {
                // assume the value is the name and no default actions exist
                if ($this->hasEventQueue($value)) {
                    // Do not double register queues
                    continue;
                }
                $this->registerEventQueue($value);
            } else {
                // assume the key is the name and the value is a set of one or more events to bind into them
                if (!$this->hasEventQueue($key)) {
                    // Do not double register queues
                    $this->registerEventQueue($key);
                }
                if (is_string($value)) {
                    // Create an event from the value
                    if (!$this->hasEvent($value, $key)) {
                        // Do not double-register events
                        $this->registerEvent($value, $key);
                    }
                } elseif (is_iterable($value)) {
                    foreach ($value as $name) {
                        // Create events from all values
                        if (!$this->hasEvent($value, $name)) {
                            // Do not double-register events
                            $this->registerEvent($key, $name);
                        }
                    }
                }
            }
        }
    }
}
