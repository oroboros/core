<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\pattern;

/**
 * Filter Aware Trait
 * Provides the methods required for a class to be filter aware
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
trait FilterAwareTrait
{

    /**
     * The filter manager object
     * 
     * @var \Oroboros\core\interfaces\library\filter\FilterManagerInterface
     */
    private $filter_manager = null;

    /**
     * A list of valid filter queues.
     * These are generated any time queues
     * are registered with the manager.
     * @var array
     */
    private $valid_filter_queues = [];

    /**
     * The name of the current queue being worked.
     * If no filter queue is currently being worked, this will return null.
     * 
     * @var string|null
     */
    private $current_filter_queue = null;

    /**
     * The default bindings for the spl observer that
     * makes the filter system work correctly.
     * 
     * You may add additional bindings, but do not remove these.
     * 
     * @var array
     */
    private static $default_filter_observer_bindings = [
        'register' => 'onFilterRegister',
        'unregister' => 'onFilterUnregister',
        'run' => 'onFilterRun',
        'error' => 'onFilterError',
        'complete' => 'onFilterComplete',
        'ready' => 'onFilterReady',
        'run-callback' => 'onFilterCallbackRun',
        'callback-error' => 'onFilterCallbackError',
        'callback-complete' => 'onFilterCallbackComplete',
        'bind-callback' => 'onFilterCallbackBind',
        'unbind-callback' => 'onFilterCallbackUnbind',
        'register-queue' => 'onFilterQueueRegister',
        'unregister-queue' => 'onFilterQueueUnregister',
    ];

    /**
     * Represents an array of objects that have been designated to be watching
     * for filter registration so they can bind their callbacks.
     * 
     * These objects will always be automatically bound to all subsequent
     * instances of the filter aware class that instantiates.
     * 
     * @var array
     */
    private static $filter_watchers = [];

    /**
     * Returns a boolean determination as to whether the given object
     * is an event watcher. This is object specific, not class specific.
     * 
     * If the second optional argument is passed as `true`, it will instead
     * return if any class in the watch list matches the class of the given watcher.
     * 
     * @param \Oroboros\core\interfaces\pattern\observer\ObserverInterface $watcher
     * @param bool $check_class If this argument is passed as true, this method
     *        will return whether *any* existing registered watcher matches the
     *        same class as the given watcher. This is useful in the event that
     *        classes are intended to only serve as a registration mechanism,
     *        and should not double-register events.
     * @return bool
     */
    public static function isWatchingFilters(\Oroboros\core\interfaces\pattern\observer\ObserverInterface $watcher, bool $check_class = false): bool
    {
        if (!$check_class) {
            return in_array($watcher, self::$filter_watchers, true);
        }
        $class = get_class($watcher);
        foreach (self::$filter_watchers as $watcher) {
            if (get_class($watcher) === $class) {
                return true;
            }
        }
        return false;
    }

    /**
     * Adds an object to the watcher list, which will alert it to registration 
     * or unregistration of filters, so it can add it's callbacks.
     * 
     * Any object in the watch list will be automatically attached to all new
     * instances of the filter aware class.
     * 
     * Objects added to the watch list **will not** be automatically attached
     * to existing instances of the class. They must be added to the watch list
     * prior to the creation of those objects if they need to hook into them.
     * 
     * Watchers will automatically be attached during object instantiation
     * and detached during object destruction.
     * 
     * @param \Oroboros\core\interfaces\pattern\observer\ObserverInterface $watcher
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException
     *         If the given watcher is already in the watch list.
     */
    public static function watchFilters(\Oroboros\core\interfaces\pattern\observer\ObserverInterface $watcher): void
    {
        if (static::isWatchingFilters($watcher)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Provided subject of class '
                        . '[%2$s] is already a registered filter watcher.'
                        , static::class, get_class($watcher))
            );
        }
        self::$filter_watchers[] = $watcher;
    }

    /**
     * Removes an object from the watcher list, if it exists in it.
     * 
     * This will not trigger existing objects in the watcher list to detach,
     * it will only prevent new object instances from automatically attaching.
     * 
     * If the object does not exist in the list of watchers, it will raise an InvalidArgumentException
     * 
     * @param \Oroboros\core\interfaces\pattern\observer\ObserverInterface $watcher
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException if the given
     *         watcher is not a registered filter watcher.
     */
    public static function ignoreFilters(\Oroboros\core\interfaces\pattern\observer\ObserverInterface $watcher): void
    {
        if (!static::isWatchingFilters($watcher)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Provided subject of class '
                        . '[%2$s] is not a registered filter watcher.'
                        , static::class, get_class($watcher))
            );
        }
        $key = array_search($watcher, self::$filter_watchers, true);
        unset(self::$filter_watchers[$key]);
    }

    /**
     * Reinitializes filter watchers.
     * 
     * This should be used if the underlying list of filter
     * watchers changed since class instantiation.
     * 
     * This will tell the object to release the existing bindings
     * and rebind to the updated master list.
     * 
     * @return void
     */
    public function reinitializeFilterWatchers(): void
    {
        $this->unbindFiltertWatchers();
        $this->initializeFilterWatchers();
        $this->setObserverEvent('filter-watchers-reinitialized');
    }

    /**
     * Observer event binding for filter registration
     * 
     * @param \Oroboros\core\interfaces\library\filter\FilterManagerInterface $subject
     */
    final public function onFilterRegister(\Oroboros\core\interfaces\library\filter\FilterManagerInterface $subject): void
    {
        $this->handleFilterRegister(
            $subject->getCurrentQueue(),
            $subject->getCurrentActionId(),
            $subject->getCurrentAction(),
            $subject
        );
        $key = $subject->getObserverEvent();
        $remove_prefix = trim(sprintf('%1$s:%2$s', 'filter-manager', static::CLASS_TYPE), ':.') . '.';
        if (strpos($key, $remove_prefix) === 0) {
            $key = substr($key, strlen($remove_prefix));
        }
        $old_queue = $this->current_filter_queue;
        $this->current_filter_queue = $subject->getCurrentQueue();
        $this->setObserverEvent($key);
        $this->current_filter_queue = $old_queue;
    }

    /**
     * Observer event binding for filter unregistration
     * 
     * @param type $subject
     * @return void
     */
    final public function onFilterUnregister(\Oroboros\core\interfaces\library\filter\FilterManagerInterface $subject): void
    {
        $this->handleFilterUnregister(
            $subject->getCurrentQueue(),
            $subject->getCurrentActionId(),
            $subject->getCurrentAction(),
            $subject
        );
        $key = $subject->getObserverEvent();
        $remove_prefix = trim(sprintf('%1$s:%2$s', 'filter-manager', static::CLASS_TYPE), ':.') . '.';
        if (strpos($key, $remove_prefix) === 0) {
            $key = substr($key, strlen($remove_prefix));
        }
        $old_queue = $this->current_filter_queue;
        $this->current_filter_queue = $subject->getCurrentQueue();
        $this->setObserverEvent($key);
        $this->current_filter_queue = $old_queue;
    }

    /**
     * Observer event binding for filter execution
     * 
     * @param type $subject
     * @return void
     */
    final public function onFilterRun(\Oroboros\core\interfaces\library\filter\FilterManagerInterface $subject): void
    {
        $this->handleFilterRun(
            $subject->getCurrentQueue(),
            $subject->getCurrentActionId(),
            $subject->getCurrentData(),
            $subject->getCurrentAction(),
            $subject
        );
        $key = $subject->getObserverEvent();
        $remove_prefix = trim(sprintf('%1$s:%2$s', 'filter-manager', static::CLASS_TYPE), ':.') . '.';
        if (strpos($key, $remove_prefix) === 0) {
            $key = substr($key, strlen($remove_prefix));
        }
        $old_queue = $this->current_filter_queue;
        $this->current_filter_queue = $subject->getCurrentQueue();
        $this->setObserverEvent($key);
        $this->current_filter_queue = $old_queue;
    }

    /**
     * Observer event binding for filter error status
     * 
     * @param type $subject
     * @return void
     */
    final public function onFilterError(\Oroboros\core\interfaces\library\filter\FilterManagerInterface $subject): void
    {
        $this->handleFilterError(
            $subject->getCurrentQueue(),
            $subject->getCurrentActionId(),
            $subject->getCurrentCallbackId(),
            $subject->getCurrentError(),
            $subject->getCurrentData(),
            $subject->getCurrentCallback(),
            $subject->getCurrentAction(),
            $subject
        );
        $key = $subject->getObserverEvent();
        $remove_prefix = trim(sprintf('%1$s:%2$s', 'filter-manager', static::CLASS_TYPE), ':.') . '.';
        if (strpos($key, $remove_prefix) === 0) {
            $key = substr($key, strlen($remove_prefix));
        }
        $old_queue = $this->current_filter_queue;
        $this->current_filter_queue = $subject->getCurrentQueue();
        $this->setObserverEvent($key);
        $this->current_filter_queue = $old_queue;
    }

    /**
     * Observer event binding for filter that has completed running successfully.
     * This fires immediately prior to resettting.
     * 
     * @param type $subject
     * @return void
     */
    final public function onFilterComplete(\Oroboros\core\interfaces\library\filter\FilterManagerInterface $subject): void
    {
        $this->handleFilterComplete(
            $subject->getCurrentDetails(),
            $subject
        );
        $key = $subject->getObserverEvent();
        $remove_prefix = trim(sprintf('%1$s:%2$s', 'filter-manager', static::CLASS_TYPE), ':.') . '.';
        if (strpos($key, $remove_prefix) === 0) {
            $key = substr($key, strlen($remove_prefix));
        }
        $old_queue = $this->current_filter_queue;
        $this->current_filter_queue = $subject->getCurrentQueue();
        $this->setObserverEvent($key);
        $this->current_filter_queue = $old_queue;
    }

    /**
     * Observer event binding for completion of an filter action. This method
     * will only be called when the filter manager returns to a clean state.
     * 
     * @param type $subject
     * @return void
     */
    final public function onFilterReady(\Oroboros\core\interfaces\library\filter\FilterManagerInterface $subject): void
    {
        $this->handleFilterReady($subject);
        $key = $subject->getObserverEvent();
        $remove_prefix = trim(sprintf('%1$s:%2$s', 'filter-manager', static::CLASS_TYPE), ':.') . '.';
        if (strpos($key, $remove_prefix) === 0) {
            $key = substr($key, strlen($remove_prefix));
        }
        $old_queue = $this->current_filter_queue;
        $this->current_filter_queue = $subject->getCurrentQueue();
        $this->setObserverEvent($key);
        $this->current_filter_queue = $old_queue;
    }

    /**
     * Observer event binding for filter callback execution
     * 
     * @param type $subject
     * @return void
     */
    final public function onFilterCallbackRun(\Oroboros\core\interfaces\library\filter\FilterManagerInterface $subject): void
    {
        $this->handleFilterCallbackRun(
            $subject->getCurrentQueue(),
            $subject->getCurrentActionId(),
            $subject->getCurrentCallbackId(),
            $subject->getCurrentData(),
            $subject->getCurrentCallback(),
            $subject->getCurrentAction(),
            $subject
        );
        $key = $subject->getObserverEvent();
        $remove_prefix = trim(sprintf('%1$s:%2$s', 'filter-manager', static::CLASS_TYPE), ':.') . '.';
        if (strpos($key, $remove_prefix) === 0) {
            $key = substr($key, strlen($remove_prefix));
        }
        $old_queue = $this->current_filter_queue;
        $this->current_filter_queue = $subject->getCurrentQueue();
        $this->setObserverEvent($key);
        $this->current_filter_queue = $old_queue;
    }

    /**
     * Observer event binding for filter callback error status
     * 
     * @param type $subject
     * @return void
     */
    final public function onFilterCallbackError(\Oroboros\core\interfaces\library\filter\FilterManagerInterface $subject): void
    {
        $this->handleFilterCallbackError(
            $subject->getCurrentQueue(),
            $subject->getCurrentActionId(),
            $subject->getCurrentCallbackId(),
            $subject->getCurrentError(),
            $subject->getCurrentData(),
            $subject->getCurrentCallback(),
            $subject->getCurrentAction(),
            $subject
        );
        $key = $subject->getObserverEvent();
        $remove_prefix = trim(sprintf('%1$s:%2$s', 'filter-manager', static::CLASS_TYPE), ':.') . '.';
        if (strpos($key, $remove_prefix) === 0) {
            $key = substr($key, strlen($remove_prefix));
        }
        $old_queue = $this->current_filter_queue;
        $this->current_filter_queue = $subject->getCurrentQueue();
        $this->setObserverEvent($key);
        $this->current_filter_queue = $old_queue;
    }

    /**
     * Observer event binding for filter callback error status
     * 
     * @param type $subject
     * @return void
     */
    final public function onFilterCallbackComplete(\Oroboros\core\interfaces\library\filter\FilterManagerInterface $subject): void
    {
        $this->handleFilterCallbackComplete(
            $subject->getCurrentQueue(),
            $subject->getCurrentActionId(),
            $subject->getCurrentCallbackId(),
            $subject->getCurrentData(),
            $subject->getCurrentCallbackResult(),
            $subject->getCurrentCallback(),
            $subject->getCurrentAction(),
            $subject
        );
        $key = $subject->getObserverEvent();
        $remove_prefix = trim(sprintf('%1$s:%2$s', 'filter-manager', static::CLASS_TYPE), ':.') . '.';
        if (strpos($key, $remove_prefix) === 0) {
            $key = substr($key, strlen($remove_prefix));
        }
        $old_queue = $this->current_filter_queue;
        $this->current_filter_queue = $subject->getCurrentQueue();
        $this->setObserverEvent($key);
        $this->current_filter_queue = $old_queue;
    }

    /**
     * Observer event binding for filter callback binding
     * 
     * @param type $subject
     * @return void
     */
    final public function onFilterCallbackBind(\Oroboros\core\interfaces\library\filter\FilterManagerInterface $subject): void
    {
        $this->handleFilterCallbackBind(
            $subject->getCurrentQueue(),
            $subject->getCurrentActionId(),
            $subject->getCurrentCallbackId(),
            $subject->getCurrentCallback(),
            $subject->getCurrentAction(),
            $subject
        );
        $key = $subject->getObserverEvent();
        $remove_prefix = trim(sprintf('%1$s:%2$s', 'filter-manager', static::CLASS_TYPE), ':.') . '.';
        if (strpos($key, $remove_prefix) === 0) {
            $key = substr($key, strlen($remove_prefix));
        }
        $old_queue = $this->current_filter_queue;
        $this->current_filter_queue = $subject->getCurrentQueue();
        $this->setObserverEvent($key);
        $this->current_filter_queue = $old_queue;
    }

    /**
     * Observer event binding for filter callback unbinding
     * 
     * @param type $subject
     * @return void
     */
    final public function onFilterCallbackUnbind(\Oroboros\core\interfaces\library\filter\FilterManagerInterface $subject): void
    {
        $this->handleFilterCallbackUnbind(
            $subject->getCurrentQueue(),
            $subject->getCurrentActionId(),
            $subject->getCurrentCallbackId(),
            $subject->getCurrentCallback(),
            $subject->getCurrentAction(),
            $subject
        );
        $key = $subject->getObserverEvent();
        $remove_prefix = trim(sprintf('%1$s:%2$s', 'filter-manager', static::CLASS_TYPE), ':.') . '.';
        if (strpos($key, $remove_prefix) === 0) {
            $key = substr($key, strlen($remove_prefix));
        }
        $old_queue = $this->current_filter_queue;
        $this->current_filter_queue = $subject->getCurrentQueue();
        $this->setObserverEvent($key);
        $this->current_filter_queue = $old_queue;
    }

    /**
     * Observer event binding for filter queue registration
     * 
     * @param type $subject
     * @return void
     */
    final public function onFilterQueueRegister(\Oroboros\core\interfaces\library\filter\FilterManagerInterface $subject): void
    {
        $this->handleFilterQueueRegister(
            $subject->getCurrentQueue(),
            $subject
        );
    }

    /**
     * Observer event binding for filter queue unregistration
     * 
     * @param type $subject
     * @return void
     */
    final public function onFilterQueueUnregister(\Oroboros\core\interfaces\library\filter\FilterManagerInterface $subject): void
    {
        $this->handleFilterQueueUnregister(
            $subject->getCurrentQueue(),
            $subject
        );
    }

    /**
     * Imports an filter broker and registers any filters/callbacks designated by it.
     * 
     * @param \Oroboros\core\interfaces\library\filter\FilterBrokerInterface $broker
     * @return void
     */
    public function importFilterBroker(\Oroboros\core\interfaces\library\filter\FilterBrokerInterface $broker): void
    {
        if (!$broker->analyze($this)) {
            // Nothing to do
            return;
        }
        $data = $broker->resolve($this);
        foreach ($data as $queue => $filters) {
            if (!$this->hasFilterQueue($queue)) {
                $this->registerFilterQueue($queue);
            }
            foreach ($filters as $filter => $callbacks) {
                if (!$this->hasFilter($filter, $queue)) {
                    $this->registerFilter($filter, $queue);
                }
                foreach ($callbacks as $key => $callback) {
                    if (!$this->hasFilterCallback($filter, $key, $queue)) {
                        if (!is_callable($callback)) {
                            throw new \Oroboros\core\exception\ErrorException(
                                    sprintf('Error encountered in [%1$s]. Provided '
                                        . 'broker supplied callback [%2$s] in filter '
                                        . '[%3$s] of queue [%4$s] by broker [%5$s] '
                                        . 'is not a valid callback.'
                                        , get_class($this), $key, $filter, $queue, get_class($broker))
                            );
                        }
                        $this->bindFilterCallback($filter, $key, $callback, $queue);
                    }
                }
            }
        }
    }

    /**
     * Returns a boolean designation as to whether a given filter queue exists.
     * 
     * @param string $queue
     * @return bool
     */
    public function hasFilterQueue(string $queue): bool
    {
        return $this->getFilterManager()->hasQueue($queue);
    }

    /**
     * Returns a list of the names of valid queues for this object.
     * 
     * @return array
     */
    public function listFilterQueues(): array
    {
        return $this->getFilterManager()->listQueues();
    }

    /**
     * Returns a boolean designation as to whether a given filter exists
     * in the current scope, or in an arbitrarily specified scope if the
     * second argument is passed.
     * 
     * If this returns false, this class does not have the specified filter,
     * and attempts to bind callbacks to it will raise an exception.
     * 
     * If this returns true, the filter is valid and callbacks may be bound to it,
     * provided they are not bound to an existing key name.
     * 
     * @param string $filter The filter name
     * @param string|null $queue The queue name if supplied, or the currently scoped queue if not
     * @return bool
     */
    public function hasFilter(string $filter, string $queue = null): bool
    {
        if (is_null($queue)) {
            $queue = $this->getCurrentFilterQueue();
        }
        return $this->getFilterManager()->hasAction($queue, $filter);
    }

    /**
     * Lists all the filters in the given queue, or the currently scoped
     * queue if no queue is supplied.
     * 
     * @param string $queue
     * @return array
     * @throws \Oroboros\core\exception\InvalidArgumentException
     *         if a queue name is given that does not exist.
     */
    public function listFilters(string $queue = null): array
    {
        if (is_null($queue)) {
            $queue = $this->getCurrentFilterQueue();
        }
        if (!$this->hasFilterQueue($queue)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Specified queue name '
                        . '[%2$s] does not exist.', get_class($this) . $queue)
            );
        }
        return $this->getFilterManager()->listActions($queue);
    }

    /**
     * Returns a boolean designation as to whether a given filter has a bound
     * callback of the specified keyname
     * 
     * This will return false if the filter does not exist, or if it does but
     * the given key is not bound to it.
     * 
     * If `hasFilter` returns `true` for the same filter and this method
     * returns `false`, then it is safe to bind an filter callback for
     * the specified key.
     * 
     * If both this method and `hasFilter` return `false` for the same filter,
     * or if both return `true`, attempting to bind a callback with the
     * specified key will raise an exception. In the first case, the filter does
     * not exist, and in the second, the key is already in use.
     * 
     * @param string $filter The filter name
     * @param string|null $queue The queue name if supplied, or the currently scoped queue if not
     * @param string $key The identifier for the given callback
     * @return bool
     */
    public function hasFilterCallback(string $filter, string $key, string $queue = null): bool
    {
        if (is_null($queue)) {
            $queue = $this->getCurrentFilterQueue();
        }
        if (!$this->hasFilter($filter, $queue)) {
            return false;
        }
        return $this->getFilterManager()->hasActionCallback($queue, $filter, $key);
    }

    /**
     * Binds a callback to the given filter in the specified queue,
     * under the name of `$key`
     * 
     * @param string $filter The name of the filter to bind the callback to
     * @param string $key The identifier for the callback
     * @param callable $callback The callback to fire when the filter fires
     * @param string|null $queue The queue to bind a callback into. If no queue 
     *        is supplied, the currently scoped queue will be used.
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException if the manager
     *         reports an error binding the callback. This can happen if the
     *         callback does not validate, or if the key is already registered.
     */
    public function bindFilterCallback(string $filter, string $key, callable $callback, string $queue = null): void
    {
        if (is_null($queue)) {
            $queue = $this->getCurrentFilterQueue();
        }
        if (!$this->hasFilter($filter, $queue)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. No filter with '
                        . 'identifier [%2$s] exists in filter scope [%3$s].'
                        , get_class($this), $filter, $queue)
            );
        }
        if ($this->hasFilterCallback($filter, $key, $queue)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Callback with '
                        . 'identifier [%2$s] already has a callback bound to '
                        . 'filter [%3$s] of scope [%4$s].'
                        , get_class($this), $key, $filter, $queue)
            );
        }
        $this->getFilterManager()->bindActionCallback($queue, $filter, $key, $callback);
    }

    /**
     * Removes a given filter callback from the filter and queue specified.
     * If no queue is specified, the currently scoped queue will be used.
     * 
     * @param string $filter The name of the filter to bind the callback to
     * @param string $key The identifier for the callback
     * @param string|null $queue The queue to bind a callback into. If no queue 
     *        is supplied, the currently scoped queue will be used.
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException
     */
    public function unbindFilterCallback(string $filter, string $key, string $queue = null): void
    {
        if (is_null($queue)) {
            $queue = $this->getCurrentFilterQueue();
        }
        if (!$this->hasFilter($filter, $queue)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. No filter with '
                        . 'identifier [%2$s] exists in filter scope [%3$s].'
                        , get_class($this), $filter, $queue)
            );
        }
        if (!$this->hasFilterCallback($filter, $key, $queue)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Callback with '
                        . 'identifier [%2$s] does not have a callback bound to '
                        . 'filter [%3$s] of scope [%4$s].'
                        , get_class($this), $key, $filter, $queue)
            );
        }
        $this->getFilterManager()->unbindActionCallback($queue, $filter, $key);
    }

    /**
     * Lists the filter callbacks in the specified filter of the given queue, or
     * the currently scoped queue if no queue identifier is passed.
     * 
     * @param string $filter
     * @param string $queue
     * @return array
     * @throws \Oroboros\core\exception\InvalidArgumentException
     *         If a given filter or queue do not exist.
     */
    public function listFilterCallbacks(string $filter, string $queue = null): array
    {
        if (is_null($queue)) {
            $queue = $this->getCurrentFilterQueue();
        }
        if (!$this->hasFilter($filter, $queue)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. No filter with '
                        . 'identifier [%2$s] exists in filter scope [%3$s].'
                        , get_class($this), $filter, $queue)
            );
        }
        return $this->getFilterManager()->listActionCallbacks($queue, $filter);
    }

    /**
     * Returns the currently scoped filter id during a filter run, registration, or unregistration
     * If a filter is not running, will return null.
     * 
     * @return string|null
     */
    public function getCurrentFilterId(): ?string
    {
        return $this->getFilterManager()->getCurrentActionId();
    }

    /**
     * Returns the currently scoped filter during a filter run, registration, or unregistration
     * If a filter is not running, will return null.
     * 
     * @return \Oroboros\core\interfaces\library\filter\FilterInterface|null
     */
    public function getCurrentFilter(): ?\Oroboros\core\interfaces\library\filter\FilterInterface
    {
        return $this->getFilterManager()->getCurrentAction();
    }

    /**
     * Returns the currently scoped filter error a filter error status
     * If a filter is not running, will return null.
     * 
     * @return \Exception|null
     */
    public function getCurrentFilterError(): ?\Exception
    {
        return $this->getFilterManager()->getCurrentError();
    }

    /**
     * Returns the currently scoped callback id during a filter run, callback run,
     * filter callback registration, or filter callback unregistration
     * If a filter is not running, will return null.
     * 
     * @return string|null
     */
    public function getCurrentFilterCallbackId(): ?string
    {
        return $this->getFilterManager()->getCurrentCallbackId();
    }

    /**
     * Returns the currently scoped callback during a filter run, filter callback run,
     * filter callback registration, or filter callback unregistration
     * If a filter is not running, will return null.
     * 
     * @return callable|null
     */
    public function getCurrentFilterCallback(): ?callable
    {
        return $this->getFilterManager()->getCurrentCallback();
    }

    /**
     * Returns the scope of the current filter queue.
     * 
     * @return string
     * @throws \Oroboros\core\exception\ErrorException If no queue has been 
     *         scoped at all. Any class utilizing the filter system should
     *         scope it's initial queue within it's constructor, and update
     *         the given queue as appropriate throughout runtime.
     */
    public function getCurrentFilterQueue(): string
    {
        if (is_null($this->current_filter_queue)) {
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. '
                        . 'No filter queue is currently scoped.', get_class($this))
            );
        }
        return $this->current_filter_queue;
    }

    /**
     * runs a single filter action by name.
     * 
     * if the supplied filter, queue, or action key do not match, an invalid argument exception will be raised.
     * 
     * @param string $filter The name of the filter containng the action
     * @param string $key The name of the action
     * @param array $arguments Any arguments to hand to the filter action.
     * @param string|null $queue The name of the queue containing the action.
     *        If none is supplied, the currently scoped queue will be used.
     * @return mixed
     * @throws \Oroboros\core\exception\InvalidArgumentException If the queue
     *         does not exist, the filter does not exist in the queue, or the
     *         action does not exist in the filter.
     */
    protected function runFilterCallback(string $filter, string $key, array $arguments = [], string $queue = null)
    {
        if (is_null($queue)) {
            $queue = $this->getCurrentFilterQueue();
        }
        if (!$this->hasFilter($filter, $queue)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. No filter with '
                        . 'identifier [%2$s] exists in filter scope [%3$s].'
                        , get_class($this), $filter, $queue)
            );
        }
        if (!$this->hasFilterCallback($filter, $key, $queue)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Callback with '
                        . 'identifier [%2$s] does not have a callback bound to '
                        . 'filter [%3$s] of scope [%4$s].'
                        , get_class($this), $key, $filter, $queue)
            );
        }
        $this->getFilterManager()->runActionCallback($queue, $filter, $key, $arguments);
    }

    /**
     * Returns a boolean designation as to whether the current filter
     * has been marked as recoverable or not.
     * 
     * This value is meaningless if a filter is not currently running.
     * 
     * @return bool
     */
    protected function isFilterErrorRecoverable(): bool
    {
        return $this->getFilterManager()->isErrorRecoverable();
    }

    /**
     * Marks an error during a filter as recoverable, which will allow the filter
     * to continue running and prevent the exception from bubbling up in the
     * main thread.
     * 
     * @return void
     */
    protected function markFilterErrorRecoverable(): void
    {
        $this->getFilterManager()->markErrorRecoverable();
    }

    /**
     * Returns a boolean designation as to whether the current filter
     * has been marked as resolved.
     * 
     * This value is meaningless if a filter is not currently running.
     * 
     * @return bool
     */
    protected function isFilterResolved(): bool
    {
        return $this->getFilterManager()->isActionResolved();
    }

    /**
     * Marks a filter as resolved. If called during a running filter,
     * this will tell the filter queue to halt further callback execution
     * and return the existing results as is.
     * 
     * This has no effect if no filter is currently running.
     * 
     * @return void
     */
    protected function markFilterResolved(): void
    {
        $this->getFilterManager()->markActionResolved();
    }

    /**
     * Sets the filter queue scope to the given filter queue.
     * This will insure that only currently scoped filters fire.
     * 
     * @param string $queue
     * @return void
     * @throws \Oroboros\core\exception\ErrorException If an attempt is made to
     *         scope to an filter queue that does not exist.
     */
    protected function scopeFilterQueue(string $queue): void
    {
        if (!$this->hasFilterQueue($queue)) {
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. Provided filter queue '
                        . '[%2$s] does not exist.', get_class($this), $queue)
            );
        }
        $this->current_filter_queue = $queue;
    }

    /**
     * Registers an filter queue with the filter manager.
     * 
     * @param string $queue
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException if an attempt
     *         is made to register a queue that already exists.
     */
    protected function registerFilterQueue(string $queue): void
    {
        try {
            $this->getFilterManager()->registerQueue($queue);
            $this->valid_filter_queues = $this->getFilterManager()->listQueues();
        } catch (\Oroboros\core\exception\InvalidArgumentException $e) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. '
                        . 'Provided filter queue [%2$s] is already '
                        . 'registered.', get_class($this), $queue)
                    , $e->getCode(), $e);
        }
    }

    /**
     * Unregisters an filter queue, which unbinds all filters within it and
     * releases all callbacks registered to them.
     * 
     * @param string $queue
     * @return void
     * @throws \Oroboros\core\exception\ErrorException if an attempt is made
     *         to unregister the currently scoped queue.
     * @throws \Oroboros\core\exception\InvalidArgumentException if an attempt
     *         is made to unregister a non-existent queue.
     */
    protected function unregisterFilterQueue(string $queue): void
    {
        if ($queue === $this->current_filter_queue) {
            // Disallow unregistering the current queue
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. Cannot unregister the '
                        . 'current filter queue [%2$s].', get_class($this), $queue)
            );
        }
        try {
            // Filter manager will handle missing queue issues
            $this->getFilterManager()->unregisterQueue($queue);
            $this->valid_filter_queues = $this->getFilterManager()->listQueues();
        } catch (\Oroboros\core\exception\InvalidArgumentException $e) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. '
                        . 'Provided filter queue [%2$s] is already '
                        . 'registered.', get_class($this), $queue)
                    , $e->getCode(), $e);
        }
    }

    /**
     * Returns the filter manager object
     * 
     * @return \Oroboros\core\interfaces\library\filter\FilterManagerInterface
     * @throws \Oroboros\core\exception\core\InvalidClassException If the implementing
     *         class has not called the initialization method, which makes it
     *         impossible to return the filter manager.
     */
    protected function getFilterManager(): \Oroboros\core\interfaces\library\filter\FilterManagerInterface
    {
        if (is_null($this->filter_manager)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. Referenced the filter '
                        . 'manager before calling method [%2$s]. '
                        . 'This class is not useable in it\'s current state.', get_class($this), 'initializeFilters')
            );
        }
        return $this->filter_manager;
    }
    /**
     * -------------------------------------------------------------------------
     * Observer Internal Bindings
     * 
     * These methods will be called when various filter manager observer events
     * fire. They have been structured to provide the relevant data to each
     * particular case, and can be overridden to handle specifics on a case by
     * case basis. The default behavior is to do nothing.
     * -------------------------------------------------------------------------
     */

    /**
     * Observer event binding for filter registration
     * 
     * This can be overridden if you want to track filter registration and
     * perform additional steps, such as importing callbacks into it.
     * 
     * Default behavior is to do nothing.
     * 
     * @param string $queue The name of the queue an action was entered into
     * @param string $id The name of the filter entered into the queue
     * @param \Oroboros\core\interfaces\library\filter\FilterInterface $filter
     *        The filter object registered
     * @param \Oroboros\core\interfaces\library\filter\FilterManagerInterface $manager
     *        The filter manager that triggered the event
     * @return void
     */
    protected function handleFilterRegister(string $queue, string $id, \Oroboros\core\interfaces\library\filter\FilterInterface $filter, \Oroboros\core\interfaces\library\filter\FilterManagerInterface $manager): void
    {
        // no-op
    }

    /**
     * Observer event binding for filter unregistration
     * 
     * This can be overridden if you want to track when entire filters are
     * removed and perform additional steps.
     * 
     * Default behavior is to do nothing.
     * 
     * @param string $queue The name of the queue an action was removed from
     * @param string $id The name of the filter removed from the queue
     * @param \Oroboros\core\interfaces\library\filter\FilterInterface $filter
     *        The filter object unregistered
     * @param \Oroboros\core\interfaces\library\filter\FilterManagerInterface $manager
     *        The filter manager that triggered the event
     * @return void
     */
    protected function handleFilterUnregister(string $queue, string $id, \Oroboros\core\interfaces\library\filter\FilterInterface $filter, \Oroboros\core\interfaces\library\filter\FilterManagerInterface $manager): void
    {
        // no-op
    }

    /**
     * Observer event binding for filter execution
     * 
     * This can be overridden if you want to track when specific filters fire
     * and perform additional steps prior to any callbacks running.
     * 
     * Default behavior is to do nothing.
     * 
     * @param string $queue The queue containing the currently firing filter
     * @param string $filter_id The identifier for the currently firing filter
     * @param array $data The data passed to the filter when it was called
     * @param \Oroboros\core\interfaces\library\filter\FilterInterface $filter
     *        The currently firing filter object
     * @param \Oroboros\core\interfaces\library\filter\FilterManagerInterface $manager
     *        The filter manager that triggered the observer event
     * @return void
     */
    protected function handleFilterRun(string $queue, string $filter_id, array $data, \Oroboros\core\interfaces\library\filter\FilterInterface $filter, \Oroboros\core\interfaces\library\filter\FilterManagerInterface $manager): void
    {
        // no-op
    }

    /**
     * Observer event binding for filter error status
     * 
     * This can be overridden if you want to mitigate the failure of an
     * filter run, and log errors, perform rollbacks, or any other
     * relevant cleanup.
     * 
     * Default behavior is to do nothing.
     * 
     * @param string $queue The queue containing the currently firing filter
     * @param string $filter_id The identifier for the currently firing filter
     * @param string $callback_id The identifier for the currently firing callback
     * @param \Exception $error The exception raised during the filter
     * @param array $data The data passed to the filter when it was called
     * @param callable $callback The literal callback that is currently
     *        in scope at the time of the exception
     * @param \Oroboros\core\interfaces\library\filter\FilterInterface $filter
     *        The currently firing filter object
     * @param \Oroboros\core\interfaces\library\filter\FilterManagerInterface $manager
     *        The filter manager that triggered the observer event
     * @return void
     */
    protected function handleFilterError(string $queue, string $filter_id, string $callback_id, \Exception $error, array $data, callable $callback, \Oroboros\core\interfaces\library\filter\FilterInterface $filter, \Oroboros\core\interfaces\library\filter\FilterManagerInterface $manager): void
    {
        // no-op
    }

    /**
     * Fires when the filter manager completes a clean execution.
     * 
     * This can be overridden if you want to analyze the final results of
     * a completed filter.
     * 
     * Default behavior is to do nothing.
     * 
     * @param array $details An array of all relevant filter completion details.
     *        Values will vary based on the specific filter that fired but keys are consistent.
     * @param \Oroboros\core\interfaces\library\filter\FilterManagerInterface $manager
     *        The filter manager that triggered the observer event
     * @return void
     */
    protected function handleFilterComplete(array $details, \Oroboros\core\interfaces\library\filter\FilterManagerInterface $manager): void
    {
        // no-op
    }

    /**
     * Fires when the filter manager returns to ready state.
     * 
     * This can be overridden if you want to run additional filters or take
     * other actions only on ready state of the filter manager.
     * 
     * Default behavior is to do nothing.
     * 
     * @param \Oroboros\core\interfaces\library\filter\FilterManagerInterface $manager
     *        The filter manager that triggered the observer event
     * @return void
     */
    protected function handleFilterReady(\Oroboros\core\interfaces\library\filter\FilterManagerInterface $manager): void
    {
        // no-op
    }

    /**
     * Observer event binding for filter callback execution
     * 
     * This can be overridden if you want to call some other process when
     * specific callback filters fire.
     * 
     * Default behavior is to do nothing.
     * 
     * @param string $queue The queue containing the currently firing filter
     * @param string $filter_id The identifier for the currently firing filter
     * @param string $callback_id The identifier for the currently firing callback
     * @param array $data The data passed to the filter when it was called
     * @param callable $callback The literal callback that is currently firing
     * @param \Oroboros\core\interfaces\library\filter\FilterInterface $filter
     *        The currently firing filter object
     * @param \Oroboros\core\interfaces\library\filter\FilterManagerInterface $manager
     *        The filter manager that triggered the observer event
     * @return void
     */
    protected function handleFilterCallbackRun(string $queue, string $filter_id, string $callback_id, array $data, callable $callback, \Oroboros\core\interfaces\library\filter\FilterInterface $filter, \Oroboros\core\interfaces\library\filter\FilterManagerInterface $manager): void
    {
        // no-op
    }

    /**
     * Observer event binding for filter callback error status
     * 
     * This can be overridden if you want to log errors, break operation,
     * or perform rollback steps to mitigate problems.
     * 
     * Default behavior is to do nothing.
     * 
     * @param string $queue The queue containing the currently firing filter
     * @param string $filter_id The identifier for the currently firing filter
     * @param string $callback_id The identifier for the currently firing callback
     * @param \Exception $error The exception raised during the callback
     * @param array $data The data passed to the filter when it was called
     * @param callable $callback The literal callback that is currently firing
     * @param \Oroboros\core\interfaces\library\filter\FilterInterface $filter
     *        The currently firing filter object
     * @param \Oroboros\core\interfaces\library\filter\FilterManagerInterface $manager
     *        The filter manager that triggered the observer event
     * @return void
     */
    protected function handleFilterCallbackError(string $queue, string $filter_id, string $callback_id, \Exception $error, array $data, callable $callback, \Oroboros\core\interfaces\library\filter\FilterInterface $filter, \Oroboros\core\interfaces\library\filter\FilterManagerInterface $manager): void
    {
        // no-op
    }

    /**
     * Observer event binding for filter callback error status
     * 
     * This can be overridden if you want to obtain a specific callback
     * operation result, or if you want to tell the filter manager to
     * break operation during a run because the objective has already
     * been handled for the specific filter.
     * 
     * Default behavior is to do nothing.
     * 
     * @param string $queue The queue that the last callback ran in
     * @param string $filter_id The filter id of the filter the last callback ran in
     * @param string $callback_id The identifier for the filter that just completed
     * @param array $data The data passed to the filter callback
     * @param type $result The return result of the callback
     * @param callable $callback The literal callable callback
     * @param \Oroboros\core\interfaces\library\filter\FilterInterface $filter
     *        The filter object containing the callback that just ran
     * @param \Oroboros\core\interfaces\library\filter\FilterManagerInterface $manager
     *        The filter manager that triggered the observer event
     * @return void
     */
    protected function handleFilterCallbackComplete(string $queue, string $filter_id, string $callback_id, array $data, $result, callable $callback, \Oroboros\core\interfaces\library\filter\FilterInterface $filter, \Oroboros\core\interfaces\library\filter\FilterManagerInterface $manager): void
    {
        // no-op
    }

    /**
     * Observer event binding for filter callback binding
     * 
     * This can be overridden if you want to track callback filter binds,
     * which can be useful for debugging how pluggables and exterior objects
     * hook into the filters of the class
     * 
     * Default behavior is to do nothing.
     * 
     * @param string $queue The name of the queue that had a callback bound to one of it's filters
     * @param string $filter_id The name of the filter the callback was bound to
     * @param string $callback_id The name of the callback
     * @param callable $callback The callable callback reference
     * @param \Oroboros\core\interfaces\library\filter\FilterInterface $filter The filter object
     * @param \Oroboros\core\interfaces\library\filter\FilterManagerInterface $manager
     *        The filter manager object that triggered the observer event
     * @return void
     */
    protected function handleFilterCallbackBind(string $queue, string $filter_id, string $callback_id, callable $callback, \Oroboros\core\interfaces\library\filter\FilterInterface $filter, \Oroboros\core\interfaces\library\filter\FilterManagerInterface $manager): void
    {
        // no-op
    }

    /**
     * Observer event binding for filter callback unbinding
     * 
     * This can be overridden if you want to track callback filter unbinds,
     * which can be useful for debugging how pluggables and exterior objects
     * hook into the filters of the class
     * 
     * Default behavior is to do nothing.
     * 
     * @param string $queue The name of the queue that had a callback unbound from one of its filters
     * @param string $filter_id The name of the filter the callback was unbound from
     * @param string $callback_id The name of the callback
     * @param callable $callback The callable callback reference
     * @param \Oroboros\core\interfaces\library\filter\FilterInterface $filter The filter object
     * @param \Oroboros\core\interfaces\library\filter\FilterManagerInterface $manager
     *        The filter manager object that triggered the observer event
     * @return void
     */
    protected function handleFilterCallbackUnbind(string $queue, string $filter_id, string $callback_id, callable $callback, \Oroboros\core\interfaces\library\filter\FilterInterface $filter, \Oroboros\core\interfaces\library\filter\FilterManagerInterface $manager): void
    {
        // no-op
    }

    /**
     * Observer event binding for filter queue registration
     * 
     * This can be overridden if you want a to run post-processes or
     * register filters when a new queue is registered.
     * 
     * Default behavior is to do nothing.
     * 
     * @param string $queue The name of the newly registered queue
     * @param \Oroboros\core\interfaces\library\filter\FilterManagerInterface $manager
     *        The filter manager that triggered the observer event
     * @return void
     */
    protected function handleFilterQueueRegister(string $queue, \Oroboros\core\interfaces\library\filter\FilterManagerInterface $manager): void
    {
        // no-op
    }

    /**
     * Observer event binding for filter queue unregistration
     * 
     * This can be overridden if you want to run post-processes
     * when an filter queue is removed.
     * 
     * Default behavior is to do nothing.
     * 
     * @param string $queue The name of the unregistered queue
     * @param \Oroboros\core\interfaces\library\filter\FilterManagerInterface $manager
     *        The filter manager that triggered the observer event
     * @return void
     */
    protected function handleFilterQueueUnregister(string $queue, \Oroboros\core\interfaces\library\filter\FilterManagerInterface $manager): void
    {
        // no-op
    }

    /**
     * You may override this method to add additional filter observer bindings,
     * however you MUST also include the results of
     * `parent::declareFilterManagerObserverFilterBindings()` or you will break
     * your class filter system.
     * 
     * @return array
     */
    protected function declareFilterManagerObserverFilterBindings(): array
    {
        return self::$default_filter_observer_bindings;
    }

    /**
     * Override this method to declare the default filter queues
     * for the current class.
     * 
     * These will be registered with the filter manager automatically.
     * 
     * @return array
     */
    protected function declareDefaultFilterQueues(): array
    {
        return [];
    }

    /**
     * Creates a new filter within a given queue (or the currently scoped queue
     * if not supplied). The filter queue will be created if it does not exist.
     * 
     * @param string $filter
     * @param string $queue
     * @return void
     */
    protected function registerFilter(string $filter, string $queue = null): void
    {
        if (is_null($queue)) {
            $queue = $this->getCurrentFilterQueue();
        }
        try {
            // Create the queue if it does not already exist
            if (!$this->getFilterManager()->hasQueue($queue)) {
                $this->registerFilterQueue($queue);
            }
            $this->getFilterManager()->registerAction($queue, $filter);
        } catch (\Oroboros\core\exception\InvalidArgumentException $e) {
            // Only occurs with a pre-existing filter name
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Could not create filter '
                        . '[%2$s] in queue [%3$] because it '
                        . 'already exists.', get_class($this), $filter, $queue)
            );
        }
    }

    /**
     * Runs a specific filter. All of the actions within the filter will run with the given arguments.
     * 
     * If the supplied queue or filter do not exist, an invalid argument exception will be raised.
     * 
     * @param string $filter The name of the filter
     * @param array $arguments Any arguments to hand to the filter
     * @param string|null $queue The name of the queue containing the filter.
     *        If none is supplied, the currently scoped queue will be used.
     * @return mixed
     * @throws \Oroboros\core\exception\InvalidArgumentException If the queue
     *         does not exist or the filter does not exist in the queue.
     */
    protected function runFilter(string $filter, array $arguments = [], string $queue = null)
    {
        if (is_null($queue)) {
            $queue = $this->getCurrentFilterQueue();
        }
        if (!$this->hasFilter($filter, $queue)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. No filter with '
                        . 'identifier [%2$s] exists in filter scope [%3$s].'
                        , get_class($this), $filter, $queue)
            );
        }
        return $this->getFilterManager()->runAction($queue, $filter, $arguments);
    }

    /**
     * Unregisters an filter from the specified queue, releasing all observer 
     * bindings to it and deleting all registered callbacks for it.
     * 
     * If no queue is supplied, the currently scoped queue will be used.
     * 
     * @param string $filter
     * @param string $queue
     * @return void
     * @throws \Oroboros\core\exception\InvalidArgumentException If the given
     *         filter does not exist.
     */
    protected function unregisterFilter(string $filter, string $queue = null): void
    {
        if (is_null($queue)) {
            $queue = $this->getCurrentFilterQueue();
        }
        try {
            // Create the queue if it does not already exist
            if (!$this->getFilterManager()->hasQueue($queue)) {
                $this->registerFilterQueue($queue);
            }
            $this->getFilterManager()->unregisterAction($queue, $filter);
        } catch (\Oroboros\core\exception\InvalidArgumentException $e) {
            // Only occurs with a non-existent filter name
            throw new \Oroboros\core\exception\InvalidArgumentException(
                    sprintf('Error encountered in [%1$s]. Could not remove filter '
                        . '[%2$s] in queue [%3$s] because it '
                        . 'does not exist.', get_class($this), $filter, $queue)
            );
        }
    }

    /**
     * Adds the spl observer bindings for the filter manager
     * 
     * @return void
     */
    private function bindFilterObserverFilters(): void
    {
        foreach ($this->declareFilterManagerObserverFilterBindings() as $key => $callback) {
            $key = trim(sprintf('%1$s:%2$s.%3$s', 'filter-manager', static::CLASS_TYPE, trim($key, '.')), '.');
            $callback = [$this, $callback];
            $this->bindObserverEvent($key, $callback);
        }
    }

    /**
     * Removes the spl observer bindings for the filter manager
     * 
     * @return void
     */
    private function unbindFilterObserverEvents(): void
    {
        foreach ($this->declareFilterManagerObserverFilterBindings() as $key => $callback) {
            $key = trim(sprintf('%1$s.%2$s.%3$s', 'filter-manager', static::CLASS_TYPE, trim($key, '.')), '.');
            $this->unbindObserverEvent($key);
        }
    }

    /**
     * Notifies filter watchers of a relevant filter action.
     * 
     * @param string $type The type of action performed (eg: register, unregister, etc)
     * @param string $queue The queue that the action applies to (eg: init, ready, error, shutdown, etc)
     * @param string $event The name of the filter affected
     * @return void
     */
    private function updateFilterWatchers(string $type, string $queue, string $filter): void
    {
        $key = trim(sprintf('%1$s.%2$s.%3$s', $type, $queue, $filter), '.');
        $this->setObserverEvent($key);
    }

    /**
     * Call this method in the constructor of the filter aware class to initialize
     * @return void
     */
    private function initializeFilters(): void
    {
        if (is_null($this->filter_manager)) {
            $this->initializeFilterWatchers();
            $this->filter_manager = $this->load('library', 'filter\\FilterStrategy', $this->getCommand(), $this->getArguments(), $this->getFlags())->bind($this);
            $this->bindFilterObserverFilters();
            $this->initializeDefaultFilterQueues();
            // Use the default filter queue if none is specified and the default exists
            if ($this->hasFilterQueue('init')) {
                $this->scopeFilterQueue('init');
            }
            if ($this->hasArgument('filter-broker') && ($this->getArgument('filter-broker') instanceof \Oroboros\core\interfaces\library\filter\FilterBrokerInterface)) {
                $this->importFilterBroker($this->getArgument('filter-broker'));
            }
        }
    }

    /**
     * Performs the binding of the current object to registered watchers,
     * so that they may inject filters as needed into new filter queues.
     * 
     * @return void
     */
    private function initializeFilterWatchers(): void
    {
        foreach (self::$filter_watchers as $watcher) {
            $this->attach($watcher);
        }
    }

    /**
     * Removes bindings of existing watchers from the object.
     * This should be run in the destructor.
     * 
     * @return void
     */
    private function unbindFiltertWatchers(): void
    {
        foreach (self::$filter_watchers as $watcher) {
            $this->detach($watcher);
        }
    }

    /**
     * Performs initialization of any declared default filter queues
     * 
     * @return void
     */
    private function initializeDefaultFilterQueues(): void
    {
        $defaults = $this->declareDefaultFilterQueues();
        foreach ($defaults as $key => $value) {
            if (is_int($key)) {
                // assume the value is the name and no default actions exist
                if ($this->hasFilterQueue($value)) {
                    // Do not double register queues
                    continue;
                }
                $this->registerFilterQueue($value);
            } else {
                // assume the key is the name and the value is a set of one or more filters to bind into them
                if (!$this->hasFilterQueue($key)) {
                    // Do not double register queues
                    $this->registerFilterQueue($key);
                }
                if (is_string($value)) {
                    // Create an filter from the value
                    if (!$this->hasFilter($value, $key)) {
                        // Do not double-register filters
                        $this->registerFilter($value, $key);
                    }
                } elseif (is_iterable($value)) {
                    foreach ($value as $name) {
                        // Create filters from all values
                        if (!$this->hasFilter($value, $name)) {
                            // Do not double-register filters
                            $this->registerFilter($key, $name);
                        }
                    }
                }
            }
        }
    }
}
