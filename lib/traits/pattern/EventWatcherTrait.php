<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\pattern;

/**
 * Event Watcher Trait
 * Provides functionality to monitor when classes register events so that
 * event callbacks can be automatically imported.
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
trait EventWatcherTrait
{

    /**
     * Designates the observer events to watch registration for to determine
     * when events are registered or unregistered.
     * 
     * @var array
     */
    private static $event_watchlist_observer_bindings = [
        'event.register' => 'onWatchItemEventRegister',
        'event.unregister' => 'onWatchItemEventUnregister',
    ];

    /**
     * Designates that the event watchlist is initialized
     * 
     * @var bool
     */
    private $event_watchlist_initialized = false;

    /**
     * Designates the subject that will be used to watch event registration events.
     * 
     * @var \Oroboros\core\interfaces\library\event\EventWatcherInterface
     */
    private $event_watchlist_subject = null;

    /**
     * Checks for any event callbacks matching the designated event
     * and applies them if they exist.
     * 
     * @param \Oroboros\core\interfaces\library\event\EventAwareInterface $subject
     * @return void
     */
    final public function onWatchItemEventRegister(\Oroboros\core\interfaces\library\event\EventAwareInterface $subject): void
    {
        $type = $subject::CLASS_TYPE;
        $scope = $subject::CLASS_SCOPE;
        $queue = $subject->getCurrentEventQueue();
        $event = $subject->getCurrentEvent();
        $id = $subject->getCurrentEventId();
        $this->addEventCallbacks($event, $type, $scope, $queue, $id);
    }

    /**
     * Checks for any event callbacks matching the designated event
     * and removes them if they exist.
     * 
     * @param \Oroboros\core\interfaces\library\event\EventAwareInterface $subject
     * @return void
     */
    final public function onWatchItemEventUnregister(\Oroboros\core\interfaces\library\event\EventAwareInterface $subject): void
    {
        $type = $subject::CLASS_TYPE;
        $scope = $subject::CLASS_SCOPE;
        $queue = $subject->getCurrentEventQueue();
        $event = $subject->getCurrentEvent();
        $id = $subject->getCurrentEventId();
        $this->removeEventCallbacks($event, $type, $scope, $queue, $id);
    }

    /**
     * Adds any relevant event callbacks to a newly registered event.
     * 
     * Override this method to add callbacks unique to this watcher.
     * 
     * @param \Oroboros\core\interfaces\library\event\EventInterface $event
     */
    protected function addEventCallbacks(\Oroboros\core\interfaces\library\event\EventInterface $event, string $type = null, string $scope = null, string $queue = null, string $id = null): void
    {
        // no-op
    }

    /**
     * Removes any relevant callbacks from an unregistered event.
     * 
     * Override this method to remove callbacks unique to this watcher.
     * 
     * @param \Oroboros\core\interfaces\library\event\EventInterface $event
     */
    protected function removeEventCallbacks(\Oroboros\core\interfaces\library\event\EventInterface $event, string $type = null, string $scope = null, string $queue = null, string $id = null): void
    {
        // no-op
    }

    /**
     * Override this method to declare a set of classes to
     * register automatically to as event watchers.
     * 
     * This should return an indexed array of class names
     * that implement `\Oroboros\core\interfaces\library\event\EventAwareInterface`
     * 
     * @return array
     */
    protected function declareEventWatchList(): array
    {
        return [];
    }

    /**
     * Initializes a set of subjects against all declared classes in the
     * event watchlist.
     * 
     * @param iterable $subjects must contain only objects that implement
     *        `Oroboros\core\interfaces\library\event\EventWatcherInterface`
     */
    protected function initializeEventWatchListSubjects(iterable $subjects)
    {
        $existing_subject = $this->event_watchlist_subject;
        foreach ($subjects as $subject) {
            $this->initializeEventWatchListSubject($subject);
        }
        $this->event_watchlist_subject = $existing_subject;
    }

    /**
     * Initializes a given subject against all declared classes
     * in the event watchlist.
     * 
     * @param object $subject must be an instance of
     *        `Oroboros\core\interfaces\library\event\EventWatcherInterface`
     *        to avoid raising an exception. This method will allow mistyped
     *        objects to be handled by the implementing class.
     * @return void
     */
    protected function initializeEventWatchListSubject($subject): void
    {
        $existing_subject = $this->event_watchlist_subject;
        $this->verifyEventWatchlistSubject($subject);
        foreach ($this->declareEventWatchList() as $classname) {
            $this->verifyEventWatchListClass($classname);
            $this->initializeEventWatchItem($classname, $subject);
        }
        $this->event_watchlist_subject = $existing_subject;
    }

    /**
     * Registers the object to watch for event registration events on any
     * instantiated instance of the given class name and inject event callbacks
     * when relevant events are registered.
     * 
     * @param string $class the name of a class
     *        implementing `Oroboros\core\interfaces\library\event\EventAwareInterface`
     * 
     * @param \Oroboros\core\interfaces\library\event\EventWatcherInterface $subject
     * @return void
     */
    protected function initializeEventWatchItem(string $class, \Oroboros\core\interfaces\library\event\EventWatcherInterface $subject): void
    {
        if (!$class::isWatchingEvents($subject, true)) {
            $class::watchEvents($subject);
        }
    }

    /**
     * Internal getter for the event watchlist subject
     * Will return null if no event watchlist subject is scoped
     * 
     * @return \Oroboros\core\interfaces\library\event\EventWatcherInterface|null
     */
    protected function getEventWatchListSubject(): ?\Oroboros\core\interfaces\library\event\EventWatcherInterface
    {
        return $this->event_watchlist_subject;
    }

    /**
     * Sets the subject that will automatically pick up event watchlist
     * results and aggregate callbacks to them.
     * 
     * @param \Oroboros\core\interfaces\library\event\EventWatcherInterface $subject
     * @return void
     */
    protected function setEventWatchListSubject(\Oroboros\core\interfaces\library\event\EventWatcherInterface $subject): void
    {
        $this->event_watch_subject = $subject;
    }

    /**
     * Call this method in the constructor of the implementing class to
     * initialize event watcher behavior.
     * 
     * @return void
     */
    private function initializeEventWatchList(): void
    {
        if ($this->event_watchlist_initialized !== true) {
            if ($this instanceof \Oroboros\core\interfaces\library\event\EventWatcherInterface) {
                $this->setEventWatchListSubject($this);
                foreach (self::$event_watchlist_observer_bindings as $key => $binding) {
                    $this->bindObserverEvent($key, [$this, $binding]);
                }
            }
            $subject = $this->getEventWatchListSubject();
            foreach ($this->declareEventWatchList() as $classname) {
                $this->verifyEventWatchListClass($classname);
                if (!is_null($subject)) {
                    $this->bindEventWatchObserverEvents($subject);
                    $this->verifyEventWatchlistSubject($subject);
                    $this->initializeEventWatchItem($classname, $subject);
                }
            }
            $this->event_watchlist_initialized = true;
        }
    }

    /**
     * Verifies that a subject is an event watcher.
     * 
     * @param mixed $subject
     * @return void
     * @throws \Oroboros\core\exception\ErrorException
     */
    private function verifyEventWatchlistSubject($subject): void
    {
        $expected = 'Oroboros\\core\\interfaces\\library\\event\\EventWatcherInterface';
        if (!is_object($subject)) {
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. Provided event watchlist '
                        . 'subject is not a valid object.', get_class($this))
            );
        }
        if (!($subject instanceof $expected)) {
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. Provided event watchlist '
                        . 'subject [%2$s] does not implement expected interface '
                        . '[%3$s].', get_class($this), get_class($subject), $expected)
            );
        }
    }

    /**
     * Verifies that a provided class is event aware.
     * 
     * @param string $classname
     * @return void
     * @throws \Oroboros\core\exception\core\InvalidClassException
     */
    private function verifyEventWatchListClass(string $classname): void
    {
        $expected = 'Oroboros\\core\\interfaces\\library\\event\\EventAwareInterface';
        if (!class_exists($classname)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. Provided event watchlist '
                        . 'entry [%2$s] is not a valid class. This class is not '
                        . 'useable in it\'s current state.', get_class($this), $classname)
            );
        }
        $interfaces = class_implements($classname);
        if (!in_array($expected, $interfaces)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. Provided event watchlist '
                        . 'entry [%2$s] does not implement expected interface '
                        . '[%3$s]. This class is not useable in it\'s current '
                        . 'state.', get_class($this), $classname, $expected)
            );
        }
    }

    /**
     * Registers the observer events to watch for event registration,
     * if the current object is a valid observer
     * 
     * @return void
     */
    private function bindEventWatchObserverEvents($subject): void
    {
        $this->verifyEventWatchlistSubject($subject);
        if ($subject !== $this) {
            return;
        }
    }
}
