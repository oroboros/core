<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\pattern;

/**
 * Director Trait
 * Mixin to make classes into directors.
 * Director/Worker pattern interaction is commonly used in C++, Smalltalk,
 * and other statically typed object oriented languages, as originally
 * outlined in the Gang of Four book Design Patterns.
 * It is most commonly used to create scalable Builder patterns,
 * but can be used for most any other application that needs scalability.
 * 
 * The class must also implement the director interface to be considered a valid director.
 * 
 * Directors are tasked with overseeing the aggregation of tasks to a pool of Worker objects,
 * so the referencing class does not need to be concerned with instantiation
 * of the correct worker to accomplish the desired task.
 * 
 * If pre-existing inheritance is not an issue,
 * you may extend \Oroboros\core\abstracts\patterns\director\AbstractDirector
 * to circumvent the need to write custom logic.
 *
 * @author Brian Dayhoff
 * @see Oroboros\core\interfaces\pattern\director\DirectorInterface
 */
trait DirectorTrait
{

    use \Oroboros\core\traits\ContainerPackagerUtility;

    private $workers = null;

    /**
     * Adds a worker to the director pool.
     * @param \Oroboros\core\interfaces\pattern\director\WorkerInterface $worker
     * @return $this (method chainable)
     * @throws \ErrorException If the provided worker does not implement the expected interface defined by the director
     * @throws \ErrorException If the director defines a worker scope, and the provided worker is out of scope
     */
    public function addWorker(\Oroboros\core\interfaces\pattern\director\WorkerInterface $worker): \Oroboros\core\interfaces\pattern\director\DirectorInterface
    {
        $this->verifyWorker($worker);
        $worker->setDirector($this);
        $this->workers[$worker::WORKER_TASK] = $worker;
        return $this;
    }

    /**
     * Returns a container of registered worker names
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    public function listWorkers(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        return $this->containerize(array_keys($this->workers->toArray()));
    }

    /**
     * Returns whether or not a worker is associated with a given task
     * @param string $task The key name for the worker
     * @return bool
     */
    public function hasWorker(string $task): bool
    {
        return $this->workers->has($task);
    }

    /**
     * Returns the worker object associated with the specified task
     * @param string $task The key name for the worker
     * @return \Oroboros\core\interfaces\pattern\director\WorkerInterface
     * @throws \InvalidArgumentException If the given task type does not have a worker
     */
    public function getWorker(string $task): \Oroboros\core\interfaces\pattern\director\WorkerInterface
    {
        if (!$this->workers->has($task)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'No worker exists corresponding to task type [%2$s].', get_class($this), $task));
        }
        return $this->workers->get($task);
    }

    /**
     * Removes a specified worker from the directors worker pool if it exists.
     * @param string $key Corresponds to the value of the class constant WORKER_TASK defined for the given worker
     * @return $this (method chainable)
     */
    public function removeWorker(string $key): \Oroboros\core\interfaces\pattern\director\DirectorInterface
    {
        if ($this->workers->has($key)) {
            unset($this->workers[$key]);
        }
        return $this;
    }

    /**
     * Executes a given task against a worker object designated to handle execution.
     * 
     * @param string $task The keyword corresponding to the task to execute
     * @param array $arguments (optional) Any arguments that should be passed to the worker to resolve the task
     * @param array $flags (optyional) Any flags that should be passed to the worker to resolve the task
     * @return mixed
     * @throws \InvalidArgumentException If no worker is registered to handle the specified task
     * @throws \Oroboros\core\exception\core\InvalidClassException Invalid class exceptions within work tasks are not caught, and propogate if encountered.
     * @throws \ErrorException If a valid task was requested, but the task failed to execute with the given parameters.
     */
    public function execute(string $task, array $arguments = [], array $flags = [])
    {
        if (!$this->workers->has($task)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'No worker exists corresponding to task type [%2$s].', get_class($this), $task));
        }
        try {
            $result = $this->workers->get($task)->executeTask($arguments, $flags);
            return $result;
        } catch (\Oroboros\core\exception\core\InvalidClassException $e) {
            // Re-throw this, as these exceptions are expected not to be handled.
            throw $e;
        } catch (\Exception $e) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. '
                        . 'Task [%2$s] failed to resolve against worker [%3$s] with supplied arguments. '
                        . 'Reason: [%4$s].', get_class($this), $task, get_class($this->workers->get($task)), $e->getMessage()), $e->getCode());
        }
    }

    /**
     * Returns the key names associated with tasks that can be passed to workers
     * @return array
     */
    protected function getWorkerKeys(): array
    {
        return array_keys($this->workers->toArray());
    }

    private function initializeDirector(): void
    {
        $this->verifyClassScope();
        $this->verifyWorkerInterface();
        $this->initializeWorkerContainer();
    }

    private function initializeWorkerContainer(): void
    {
        if (is_null($this->workers)) {
            $this->workers = $this::containerize([]);
        }
    }

    private function verifyWorker(\Oroboros\core\interfaces\pattern\director\WorkerInterface $worker): void
    {
        $expected_interface = static::EXPECTED_WORKER_INTERFACE;
        $expected_scope = static::WORKER_SCOPE;
        if (!in_array($expected_interface, class_implements($worker))) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided worker [%2$s] does not honor expected interface [%3$s].', get_class($this), get_class($worker), $expected_interface));
        }
        // Only verify scope if it is defined
        if (!is_null($expected_scope) && $worker::WORKER_SCOPE !== $expected_scope) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided worker [%2$s] is out of expected scope [%3$s]. '
                        . 'Worker scope is [%4$s].'), get_class($this), get_class($worker), $expected_scope, $worker::WORKER_SCOPE);
        }
    }

    private function verifyClassScope(): void
    {
        if (is_null(static::CLASS_SCOPE)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(sprintf('Class [%1$s] is invalid.'
                        . 'Class constant [%2$s] must be defined.', get_class($this), 'CLASS_SCOPE'));
        }
    }

    private function verifyWorkerInterface(): void
    {
        $expected = self::EXPECTED_WORKER_INTERFACE;
        $existing = static::EXPECTED_WORKER_INTERFACE;
        if (!($expected === $existing || in_array($expected, class_implements($existing)))) {
            throw new \Oroboros\core\exception\core\InvalidClassException(sprintf('Director class [%1$s] is invalid.'
                        . 'Class constant [%2$s] must define interface [%3$s] as an interface that is a valid '
                        . 'extension of [%4$s].', get_class($this), 'EXPECTED_WORKER_INTERFACE', $existing, $expected));
        }
    }
}
