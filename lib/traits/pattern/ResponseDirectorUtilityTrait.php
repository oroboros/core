<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\pattern;

/**
 * Response Director Utility Trait
 * Provides neccessary utilities for handling a response director instance
 * for objects packaging data to send to a view.
 * 
 * This allows objects to correctly handle data through abstraction
 * without ever being directly exposed to the view itself.
 * 
 * @requiresClassConstant RESPONSE_DIRECTOR_CLASS
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
trait ResponseDirectorUtilityTrait
{

    /**
     * The response director tasked with building the response payload.
     * @var \Oroboros\core\interfaces\pattern\director\DirectorInterface
     */
    private $response_director = null;

    /**
     * Returns the response director to child classes.
     * @return \Oroboros\core\interfaces\pattern\director\DirectorInterface
     */
    protected function getResponseDirector(): \Oroboros\core\interfaces\pattern\director\DirectorInterface
    {
        return $this->response_director;
    }

    /**
     * Initializes the response director. This should be called in
     * the constructor of the implementing class if a response director
     * was not passed through dependency injection.
     * 
     * @return void
     * @throws \Oroboros\core\exception\core\InvalidClassException
     */
    protected function initializeResponseBuilder(): void
    {
        $expected = 'Oroboros\\core\\interfaces\\pattern\\director\\DirectorInterface';
        if ($this->hasArgument('response-director')) {
            $director = $this->getArgument('response-director');
        } else {
            $director = $this->load('library', static::RESPONSE_DIRECTOR_CLASS);
        }
        if (!is_object($director) || !in_array($expected, class_implements($director))) {
            throw new \Oroboros\core\exception\core\InvalidClassException(sprintf('Class [%1$s] is invalid. '
                        . 'Response director must be a valid implentation of [%2$s].', get_class($this), $expected));
        }
        $builders = $this->getResponseBuilders();
        foreach ($builders as $builder) {
            $director->addWorker($builder);
        }
        $this->setResponseDirector($director);
    }

    /**
     * Setter function for the response director. If passed through dependency injection, use this.
     * @param \Oroboros\core\interfaces\pattern\director\DirectorInterface $director
     * @return void
     */
    protected function setResponseDirector(\Oroboros\core\interfaces\pattern\director\DirectorInterface $director): void
    {
        $this->response_director = $director;
    }

    /**
     * Override this method in a child class to supply builders to construct the response.
     * These will be fed into the response director.
     * 
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    public function getResponseBuilders(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $container = $this::containerize();
        return $container;
    }

    /**
     * Verifies that the class or abstraction is correctly set up to utilize a response director.
     * This should be called in the constructor of the class directly implementing this trait.
     * @throws \OutOfBoundsException
     */
    private function verifyResponseDirector()
    {
        $class = null;
        $expected = 'Oroboros\\core\\interfaces\\pattern\\director\\DirectorInterface';
        if (!is_null(static::RESPONSE_DIRECTOR_CLASS)) {
            $class = $this->getFullClassName('library', static::RESPONSE_DIRECTOR_CLASS);
        }
        if (is_null(static::RESPONSE_DIRECTOR_CLASS) || !class_exists($class) || !in_array($expected, class_implements($class))) {
            throw new \OutOfBoundsException(sprintf('Class [%1$s] is invalid. '
                        . 'Controller must declare class constant [%2$s] with a valid extension of [%3$s].', get_class($this), 'RESPONSE_DIRECTOR_CLASS', $expected));
        }
    }
}
