<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\pattern;

/**
 * Observer Trait
 * Provides drop in methods to honor Oroboros\core\interfaces\pattern\observer\ObserverInterface
 *
 * @author Brian Dayhoff
 */
trait ObserverTrait
{

    /**
     * The callbacks associated with event bindings.
     * format key: event name, value: callable
     * @var Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $observer_event_bindings = null;

    /**
     * Receive update from subject
     * @param Oroboros\core\interfaces\pattern\observer\ObservableInterface $subject
     * @return void 
     */
    public function update(\SplSubject $subject): void
    {
        if (!($subject instanceof \Oroboros\core\interfaces\pattern\observer\ObservableInterface)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided object must be an instance of [%2$s].', get_class($this), 'Oroboros\\core\\interfaces\\pattern\\observer\\ObservableInterface'));
        }
        $this->checkClassObserverUpdates($subject);
        $observer_event = $subject->getObserverEvent();
        $prefix = trim(sprintf('%1$s.%2$s', $subject::CLASS_TYPE, $subject::CLASS_SCOPE), ':.') . '.';
        if (strpos($observer_event, $prefix) === 0) {
            $key = substr($observer_event, strlen($prefix));
            if ($this->observer_event_bindings->has($key)) {
                call_user_func($this->observer_event_bindings->get($key), $subject);
                return;
            }
        }
        $prefix = trim(sprintf('%1$s:%2$s', $subject::CLASS_TYPE, $subject::CLASS_SCOPE), ':.') . '.';
        if (strpos($observer_event, $prefix) === 0 && strpos($prefix, ':') !== false) {
            $key = substr($observer_event, strlen($prefix));
            if ($this->observer_event_bindings->has($key)) {
                call_user_func($this->observer_event_bindings->get($key), $subject);
                return;
            }
        }
        if ($this->observer_event_bindings->has($subject->getObserverEvent())) {
            call_user_func($this->observer_event_bindings->get($subject->getObserverEvent()), $subject);
        }
    }

    /**
     * Get observer priority
     * 
     * @return int
     */
    public function getObserverPriority(): int
    {
        return $this->declareObserverPriority();
    }

    /**
     * Call this method in your constructor to initialize the observer functionality.
     * @return void
     */
    protected function initializeObserver(): void
    {
        $this->initializeObserverEventBindings();
    }

    /**
     * Override this method to declare event bindings for observable events.
     * This must return an associative array, where the key is the name of
     * the event to listen for, and the value is the callable to pass
     * the observable object to.
     * 
     * Callables must take zero or one parameter. an object instance of 
     * Oroboros\\core\\interfaces\\pattern\\observer\\ObservableInterface
     * must be a valid parameter if any parameters are required.
     * 
     * @return array
     */
    protected function declareObserverEventBindings(): array
    {
        return [];
    }

    /**
     * Override this method to declare a priority for the observer.
     * Events will fire in order of priority when the observed object
     * updates the observer, so higher priorities will be sent notification
     * first.
     * 
     * Observers that present heavier overhead or functionality that should
     * occur after other updates should have a low priority.
     * 
     * Observers that present critical tasks or fire quickly
     * should have a high priority.
     * @return int
     */
    protected function declareObserverPriority(): int
    {
        return 0;
    }

    /**
     * Binds a callback to a specific observable event.
     * 
     * Callbacks must take zero or one parameter. an object instance of 
     * Oroboros\\core\\interfaces\\pattern\\observer\\ObservableInterface
     * must be a valid parameter if any parameters are required.
     * 
     * @param string $event
     * @param callable $callback
     * @return void
     */
    protected function bindObserverEvent(string $event, callable $callback): void
    {
        $this->observer_event_bindings[$event] = $callback;
    }

    /**
     * Releases an event binding for observable events,
     * if any such binding exists.
     * @param string $event
     * @return void
     */
    protected function unbindObserverEvent(string $event): void
    {
        if ($this->observer_event_bindings->has($event)) {
            unset($this->observer_event_bindings[$event]);
        }
    }

    /**
     * Additional update criteria for adapters.
     * 
     * No operation by default.
     * 
     * @param \Oroboros\core\interfaces\adapter\AdapterInterface $adapter
     * @return void
     */
    protected function handleAdapterObserverUpdate(\Oroboros\core\interfaces\adapter\AdapterInterface $adapter): void
    {
        // no-op
    }

    /**
     * Additional update criteria for application classes.
     * 
     * No operation by default.
     * 
     * @param \Oroboros\core\interfaces\adapter\AdapterInterface $application
     * @return void
     */
    protected function handleApplicationObserverUpdate(\Oroboros\core\interfaces\application\ApplicationInterface $application): void
    {
        // no-op
    }

    /**
     * Additional update criteria for bootstrap classes.
     * 
     * No operation by default.
     * 
     * @param \Oroboros\core\interfaces\bootstrap\BootstrapInterface $bootstrap
     * @return void
     */
    protected function handleBootstrapObserverUpdate(\Oroboros\core\interfaces\bootstrap\BootstrapInterface $bootstrap): void
    {
        // no-op
    }

    /**
     * Additional update criteria for controllers.
     * 
     * No operation by default.
     * 
     * @param \Oroboros\core\interfaces\controller\ControllerInterface $controller
     * @return void
     */
    protected function handleControllerObserverUpdate(\Oroboros\core\interfaces\controller\ControllerInterface $controller): void
    {
        // no-op
    }

    /**
     * Additional update criteria for exceptions.
     * 
     * No operation by default.
     * 
     * @param \Oroboros\core\interfaces\exception\ExceptionInterface $exception
     * @return void
     */
    protected function handleExceptionObserverUpdate(\Oroboros\core\interfaces\exception\ExceptionInterface $exception): void
    {
        // no-op
    }

    /**
     * Additional update criteria for extensions.
     * 
     * No operation by default.
     * 
     * @param \Oroboros\core\interfaces\extension\ExtensionInterface $extension
     * @return void
     */
    protected function handleExtensionObserverUpdate(\Oroboros\core\interfaces\extension\ExtensionInterface $extension): void
    {
        // no-op
    }

    /**
     * Additional update criteria for libraries.
     * 
     * No operation by default.
     * 
     * @param \Oroboros\core\interfaces\library\LibraryInterface $library
     * @return void
     */
    protected function handleLibraryObserverUpdate(\Oroboros\core\interfaces\library\LibraryInterface $library): void
    {
        // no-op
    }

    /**
     * Additional update criteria for models.
     * 
     * No operation by default.
     * 
     * @param \Oroboros\core\interfaces\model\ModelInterface $model
     * @return void
     */
    protected function handleModelObserverUpdate(\Oroboros\core\interfaces\model\ModelInterface $model): void
    {
        // no-op
    }

    /**
     * Additional update criteria for modules.
     * 
     * No operation by default.
     * 
     * @param \Oroboros\core\interfaces\module\ModuleInterface $module
     * @return void
     */
    protected function handleModuleObserverUpdate(\Oroboros\core\interfaces\module\ModuleInterface $module): void
    {
        // no-op
    }

    /**
     * Additional update criteria for services.
     * 
     * No operation by default.
     * 
     * @param \Oroboros\core\interfaces\service\ServiceInterface $service
     * @return void
     */
    protected function handleServiceObserverUpdate(\Oroboros\core\interfaces\service\ServiceInterface $service): void
    {
        // no-op
    }

    /**
     * Additional update criteria for views.
     * 
     * No operation by default.
     * 
     * @param \Oroboros\core\interfaces\view\ViewInterface $view
     * @return void
     */
    protected function handleViewObserverUpdate(\Oroboros\core\interfaces\view\ViewInterface $view): void
    {
        // no-op
    }

    /**
     * Removes the class type and scope declarations from an event and returns
     * the relative event with the class identity portion removed.
     * 
     * @param \Oroboros\core\interfaces\pattern\observer\ObservableInterface $object
     * @return string
     */
    protected function filterObserverClassEventName(\Oroboros\core\interfaces\pattern\observer\ObservableInterface $object): string
    {
        $object_scope = trim(sprintf('%1$s:%2$s', $object::CLASS_TYPE, $object::CLASS_SCOPE), ':.') . '.';
        $event = $object->getObserverEvent();
        if (strpos($event, $object_scope) === 0) {
            $event = substr($event, strlen($object_scope));
        }
        return $event;
    }

    /**
     * Allows interface-based event declarations.
     * This method is non-blocking to the normal observer event bindings.
     * @param \SplSubject $subject
     * @return void
     */
    private function checkClassObserverUpdates(\SplSubject $subject): void
    {
        if (!($subject instanceof \Oroboros\core\interfaces\BaseInterface)) {
            // Not a class based observer event
            return;
        }
        if ($subject instanceof \Oroboros\core\interfaces\adapter\AdapterInterface) {
            $this->handleAdapterObserverUpdate($subject);
        }
        if ($subject instanceof \Oroboros\core\interfaces\application\ApplicationInterface) {
            $this->handleApplicationObserverUpdate($subject);
        }
        if ($subject instanceof \Oroboros\core\interfaces\bootstrap\BootstrapInterface) {
            $this->handleBootstrapObserverUpdate($subject);
        }
        if ($subject instanceof \Oroboros\core\interfaces\controller\ControllerInterface) {
            $this->handleControllerObserverUpdate($subject);
        }
        if ($subject instanceof \Oroboros\core\interfaces\exception\ExceptionInterface) {
            $this->handleExceptionObserverUpdate($subject);
        }
        if ($subject instanceof \Oroboros\core\interfaces\extension\ExtensionInterface) {
            $this->handleExtensionObserverUpdate($subject);
        }
        if ($subject instanceof \Oroboros\core\interfaces\library\LibraryInterface) {
            $this->handleLibraryObserverUpdate($subject);
        }
        if ($subject instanceof \Oroboros\core\interfaces\model\ModelInterface) {
            $this->handleModelObserverUpdate($subject);
        }
        if ($subject instanceof \Oroboros\core\interfaces\module\ModuleInterface) {
            $this->handleModuleObserverUpdate($subject);
        }
        if ($subject instanceof \Oroboros\core\interfaces\service\ServiceInterface) {
            $this->handleServiceObserverUpdate($subject);
        }
    }

    private function initializeObserverEventBindings(): void
    {
        $bindings = $this->declareObserverEventBindings();
        $this->verifyDeclaredObserverEventBindings($bindings);
        $this->observer_event_bindings = \Oroboros\core\library\container\Container::init(null, $bindings);
    }

    private function verifyDeclaredObserverEventBindings(array $bindings): void
    {
        $invalid = [];
        foreach ($bindings as $event => $callback) {
            if (!is_callable($callback)) {
                $invalid[] = $event;
            }
        }
        if (!empty($invalid)) {
            throw new \Oroboros\core\exception\ErrorException(sprintf('Error encountered in [%1$s]. '
                        . 'The following observer event bindings are invalid [%2$s]. '
                        . 'Only callable values are allowed.', get_class($this), implode(', ', $invalid)));
        }
    }
}
