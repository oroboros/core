<?php
/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\pattern;

/**
 * Observable Trait
 * Provides drop in methods to honor Oroboros\core\interfaces\pattern\observer\ObservableInterface
 *
 * @author Brian Dayhoff
 */
trait ObservableTrait
{

    /**
     * The objects observing the observable
     * @var Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private $observer_objects = null;

    /**
     * The event notification priority of the observers
     * @var Oroboros\core\interfaces\library\container\ContainerInterface
     */
    protected $observer_priority = null;

    /**
     *
     * @param string
     */
    private $name;

    /**
     *
     * @param string
     */
    private $observer_event;

    /**
     * Attach an observer
     * @param Oroboros\core\interfaces\pattern\observer\ObserverInterface $observer
     * @return void 
     */
    public function attach(\SplObserver $observer)
    {
        if (!($observer instanceof \Oroboros\core\interfaces\pattern\observer\ObserverInterface)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided object must be an instance of [%2$s].', get_class($this), 'Oroboros\\core\\interfaces\\pattern\\observer\\ObserverInterface'));
        }
        $observerKey = spl_object_hash($observer);
        $this->observer_objects[$observerKey] = $observer;
        $this->observer_priority[$observerKey] = $observer->getObserverPriority();
        $priority = $this->observer_priority->toArray();
        arsort($priority);
        $this->observer_priority = $this->observer_priority->fromArray($priority);
    }

    /**
     * Detach an observer
     * @param Oroboros\core\interfaces\pattern\observer\ObserverInterface $observe
     * @return void
     */
    public function detach(\SplObserver $observer)
    {
        if (!($observer instanceof \Oroboros\core\interfaces\pattern\observer\ObserverInterface)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(sprintf('Error encountered in [%1$s]. '
                        . 'Provided object must be an instance of [%2$s].', get_class($this), 'Oroboros\\core\\interfaces\\pattern\\observer\\ObserverInterface'));
        }
        $key = array_search($observer, $this->observer_objects->toArray(), true);
        if ($key !== false) {
            unset($this->observer_objects[$key]);
            unset($this->observer_priority[$key]);
        }
    }

    /**
     * Notify an observer
     * @return void 
     */
    public function notify()
    {
        foreach ($this->observer_priority as $key => $value) {
            try {
                $this->observer_objects[$key]->update($this);
            } catch (\Psr\Container\NotFoundExceptionInterface $e) {
                // Dirty removal, clear this key.
                unset($this->observer_priority[$key]);
            }
        }
    }

    /**
     * Returns the event name for the observable event
     * 
     * The event called will be prefixed by the observable name,
     * so observers can differentiate between the same event firing
     * in separate observables.
     * 
     * @return string
     * @throws \Oroboros\core\exception\ErrorException if no event has been set
     *         before calling this method.
     */
    public function getObserverEvent(): string
    {
        if (is_null($this->observer_event)) {
            throw new \Oroboros\core\exception\ErrorException(
               sprintf('Error encountered in [%1$s]. No observer event has been set.', get_class($this)) 
            );
        }
        return trim(sprintf('%1$s.%2$s', $this->declareObservableName(), $this->observer_event), '.');
    }

    /**
     * Run this method in your constructor one time to initialize observable functionality.
     */
    protected function initializeObservable(): void
    {
        $this->initializeObserverContainer();
    }

    /**
     * Set or update event 
     *
     * @param $event
     * @return void
     */
    protected function setObserverEvent(string $event): void
    {
        $this->observer_event = $event;
        $this->notify();
    }

    /**
     * Returns the container of all observers bound to the observable.
     * 
     * @return \Oroboros\core\library\container\Container
     * @throws \Oroboros\core\exception\core\InvalidClassException If the observer
     *         initialization method was never called prior to use. This will raise
     *         fatal errors if not prevented. This means the referencing class is broken.
     */
    protected function getObservers(): \Oroboros\core\library\container\Container
    {
        if (is_null($this->observer_objects)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
               sprintf('Error encountered in [%1$s]. Observer has not been '
                   . 'initialized by calling method [%2$s] prior to execution. '
                   . 'This class is not useable in it\'s current state.'
                   , get_class($this), 'initializeObservable') 
            );
        }
        return $this->observer_objects;
    }

    abstract protected function declareObservableName(): string;

    private function initializeObserverContainer(): void
    {
        $this->observer_objects = \Oroboros\core\library\container\Container::init();
        $this->observer_priority = \Oroboros\core\library\container\Container::init();
    }
}
