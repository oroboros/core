<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\pattern;

/**
 * Broker Trait
 * Provides the methods required to function as a broker
 * 
 * @see \Oroboros\core\interfaces\pattern\broker\BrokerInterface
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
trait BrokerTrait
{

    private $broker_data = null;

    /**
     * Takes a subject and analyzes whether it can provide the resources required of it
     * 
     * If the class constant `BROKER_SUBJECT` exists and defines a valid interface,
     * This will return `true` if the given object implements that interface.
     * 
     * If the class constant `BROKER_SUBJECT` exists, is not null, and does not 
     * resolve to a valid interface name, an `InvalidClassException` will be raised.
     * 
     * all other cases return false
     * 
     * @param object $subject
     * @return bool
     * @throws \Oroboros\core\exception\core\InvalidClassException
     */
    public function analyze(object $subject): bool
    {
        $def = sprintf('%1$s::%2$s', get_class($this), 'BROKER_SUBJECT');
        if (defined($def)) {
            $const = $this::BROKER_SUBJECT;
            if (is_null($const)) {
                return false;
            }
            if (!is_string($const)) {
                throw new \Oroboros\core\exception\core\InvalidClassException(
                        sprintf('Error encountered in [%1$s]. Class constant [%2$s] '
                            . 'must be a string or null if present. This class is '
                            . 'not useable in it\'s current state.'
                            , get_class($this), 'BROKER_SUBJECT')
                );
            }
            if (!interface_exists($const)) {
                throw new \Oroboros\core\exception\core\InvalidClassException(
                        sprintf('Error encountered in [%1$s]. Class constant [%2$s] '
                            . 'must be define a valid interface if it is not null. '
                            . 'This class is not useable in it\'s current state.'
                            , get_class($this), 'BROKER_SUBJECT')
                );
            }
            $const = '\\' . trim($const, '\\');
            return ($subject instanceof $const);
        }
        return false;
    }

    /**
     * If method `analyze` returns `true`, this method MUST return
     * the relevant dataset for the subject.
     * 
     * If method `analyze` returns `false`, this method MUST return
     * a non-blocking empty container.
     * 
     * If a class defines a class container, that container will be used.
     * Otherwise the default generic container will be used.
     * 
     * @param object $subject
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    public function resolve(object $subject): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $container_class = '\\Oroboros\\core\\library\\container\\Container';
        if (defined(sprintf('%1$s::$2$s', get_class($this), 'CONTAINER_CLASS')) && static::CONTAINER_CLASS !== null) {
            $container_class = static::CONTAINER_CLASS;
        }
        if (!$this->analyze($subject)) {
            return $container_class::init();
        }
        if (is_null($this->broker_data)) {
            return $container_class::init();
        }
        return \Oroboros\core\library\container\Container::init(null, $this->broker_data->toArray());
    }

    /**
     * Returns an array of the default broker data.
     * Override this method to provide a dataset.
     * 
     * @return array
     */
    protected function declareBrokerDefaultData(): array
    {
        return [];
    }

    /**
     * Call this function once in the constructor of your implementing class
     * to initialize broker functionality.
     * 
     * @return void
     */
    private function initializeBroker(): void
    {
        if (is_null($this->broker_data)) {
            $defaults = $this->declareBrokerDefaultData();
            $this->broker_data = \Oroboros\core\library\container\Container::init(null, $defaults);
        }
    }
}
