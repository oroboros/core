<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\pattern;

/**
 * Filter Watcher Trait
 * Provides functionality to monitor when classes register filters so that
 * filter callbacks can be automatically imported.
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
trait FilterWatcherTrait
{

    /**
     * Designates the observer events to watch registration for to determine
     * when filters are registered or unregistered.
     * 
     * @var array
     */
    private static $filter_watchlist_observer_bindings = [
        'filter.register' => 'onWatchItemFilterRegister',
        'filter.unregister' => 'onWatchItemFilterUnregister',
    ];

    /**
     * Designates that the filter watchlist is initialized
     * 
     * @var bool
     */
    private $filter_watchlist_initialized = false;

    /**
     * Designates the subject that will be used to watch filter registration events.
     * 
     * @var \Oroboros\core\interfaces\library\filter\FilterWatcherInterface
     */
    private $filter_watchlist_subject = null;

    /**
     * Checks for any filter callbacks matching the designated filter
     * and applies them if they exist.
     * 
     * @param \Oroboros\core\interfaces\library\event\EventAwareInterface $subject
     * @return void
     */
    final public function onWatchItemFilterRegister(\Oroboros\core\interfaces\library\event\EventAwareInterface $subject): void
    {
        $type = $subject::CLASS_TYPE;
        $scope = $subject::CLASS_SCOPE;
        $queue = $subject->getCurrentFilterQueue();
        $event = $subject->getCurrentFilter();
        $id = $subject->getCurrentFilterId();
        $this->addFilterCallbacks($event, $type, $scope, $queue, $id);
    }

    /**
     * Checks for any filter callbacks matching the designated filter
     * and removes them if they exist.
     * 
     * @param \Oroboros\core\interfaces\library\event\EventAwareInterface $subject
     * @return void
     */
    final public function onWatchItemFilterUnregister(\Oroboros\core\interfaces\library\event\EventAwareInterface $subject): void
    {
        $type = $subject::CLASS_TYPE;
        $scope = $subject::CLASS_SCOPE;
        $queue = $subject->getCurrentFilterQueue();
        $event = $subject->getCurrentFilter();
        $id = $subject->getCurrentFilterId();
        $this->removeFilterCallbacks($event, $type, $scope, $queue, $id);
    }

    /**
     * Adds any relevant filter callbacks to a newly registered filter.
     * 
     * Override this method to add callbacks unique to this watcher.
     * 
     * @param \Oroboros\core\interfaces\library\filter\FilterInterface $filter
     */
    protected function addFilterCallbacks(\Oroboros\core\interfaces\library\filter\FilterInterface $filter, string $type = null, string $scope = null, string $queue = null, string $id = null): void
    {
        // no-op
    }

    /**
     * Removes any relevant callbacks from an unregistered filter.
     * 
     * Override this method to remove callbacks unique to this watcher.
     * 
     * @param \Oroboros\core\interfaces\library\filter\FilterInterface $filter
     */
    protected function removeFilterCallbacks(\Oroboros\core\interfaces\library\filter\FilterInterface $filter, string $type = null, string $scope = null, string $queue = null, string $id = null): void
    {
        // no-op
    }

    /**
     * Override this method to declare a set of classes to
     * register automatically to as filter watchers.
     * 
     * This should return an indexed array of class names
     * that implement `\Oroboros\core\interfaces\library\filter\FilterAwareInterface`
     * 
     * @return array
     */
    protected function declareFilterWatchList(): array
    {
        return [];
    }

    /**
     * Initializes a set of subjects against all declared classes in the
     * filter watchlist.
     * 
     * @param iterable $subjects must contain only objects that implement
     *        `Oroboros\core\interfaces\library\event\EventWatcherInterface`
     */
    protected function initializeFilterWatchListSubjects(iterable $subjects)
    {
        $existing_subject = $this->filter_watchlist_subject;
        foreach ($subjects as $subject) {
            $this->initializeFilterWatchListSubject($subject);
        }
        $this->filter_watchlist_subject = $existing_subject;
    }

    /**
     * Initializes a given subject against all declared classes
     * in the filter watchlist.
     * 
     * @param object $subject must be an instance of
     *        `Oroboros\core\interfaces\library\filter\FilterWatcherInterface`
     *        to avoid raising an exception. This method will allow mistyped
     *        objects to be handled by the implementing class.
     * @return void
     */
    protected function initializeFilterWatchListSubject($subject): void
    {
        $existing_subject = $this->filter_watchlist_subject;
        $this->verifyFilterWatchlistSubject($subject);
        foreach ($this->declareFilterWatchList() as $classname) {
            $this->verifyFilterWatchListClass($classname);
            $this->initializeFilterWatchItem($classname, $subject);
        }
        $this->filter_watchlist_subject = $existing_subject;
    }

    /**
     * Registers the object to watch for filter registration events on any
     * instantiated instance of the given class name and inject filter callbacks
     * when relevant filters are registered.
     * 
     * @param string $class the name of a class
     *        implementing `Oroboros\core\interfaces\library\filter\FilterAwareInterface`
     * 
     * @param \Oroboros\core\interfaces\library\event\EventWatcherInterface $subject
     * @return void
     */
    protected function initializeFilterWatchItem(string $class, \Oroboros\core\interfaces\library\filter\FilterWatcherInterface $subject): void
    {
        if (!$class::isWatchingFilters($subject, true)) {
            $class::watchFilters($subject);
        }
    }

    /**
     * Internal getter for the filter watchlist subject
     * Will return null if no filter watchlist subject is scoped
     * 
     * @return \Oroboros\core\interfaces\library\filter\FilterWatcherInterface|null
     */
    protected function getFilterWatchListSubject(): ?\Oroboros\core\interfaces\library\filter\FilterWatcherInterface
    {
        return $this->filter_watchlist_subject;
    }

    /**
     * Sets the subject that will automatically pick up filter watchlist
     * results and aggregate callbacks to them.
     * 
     * @param \Oroboros\core\interfaces\library\filter\FilterWatcherInterface $subject
     * @return void
     */
    protected function setFilterWatchListSubject(\Oroboros\core\interfaces\library\filter\FilterWatcherInterface $subject): void
    {
        $this->filter_watch_subject = $subject;
    }

    /**
     * Call this method in the constructor of the implementing class to
     * initialize filter watcher behavior.
     * 
     * @return void
     */
    private function initializeFilterWatchList(): void
    {
        if ($this->filter_watchlist_initialized !== true) {
            if ($this instanceof \Oroboros\core\interfaces\library\filter\FilterWatcherInterface) {
                $this->setFilterWatchListSubject($this);
                foreach (self::$filter_watchlist_observer_bindings as $key => $binding) {
                    $this->bindObserverEvent($key, [$this, $binding]);
                }
            }
            $subject = $this->getFilterWatchListSubject();
            foreach ($this->declareFilterWatchList() as $classname) {
                $this->verifyFilterWatchListClass($classname);
                if (!is_null($subject)) {
                    $this->bindFilterWatchObserverEvents($subject);
                    $this->verifyFilterWatchlistSubject($subject);
                    $this->initializeFilterWatchItem($classname, $subject);
                }
            }
            $this->filter_watchlist_initialized = true;
        }
    }

    private function verifyFilterWatchlistSubject($subject): void
    {
        $expected = 'Oroboros\\core\\interfaces\\library\\filter\\FilterWatcherInterface';
        if (!is_object($subject)) {
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. Provided filter watchlist '
                        . 'subject is not a valid object.', get_class($this))
            );
        }
        if (!($subject instanceof $expected)) {
            throw new \Oroboros\core\exception\ErrorException(
                    sprintf('Error encountered in [%1$s]. Provided filter watchlist '
                        . 'subject [%2$s] does not implement expected interface '
                        . '[%3$s].', get_class($this), get_class($subject), $expected)
            );
        }
    }

    /**
     * Verifies that a provided class is filter aware.
     * 
     * @param string $classname
     * @return void
     * @throws \Oroboros\core\exception\core\InvalidClassException
     */
    private function verifyFilterWatchListClass(string $classname): void
    {
        $expected = 'Oroboros\\core\\interfaces\\library\\filter\\FilterAwareInterface';
        if (!class_exists($classname)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. Provided filter watchlist '
                        . 'entry [%2$s] is not a valid class. This class is not '
                        . 'useable in it\'s current state.', get_class($this), $classname)
            );
        }
        $interfaces = class_implements($classname);
        if (!in_array($expected, $interfaces)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. Provided filter watchlist '
                        . 'entry [%2$s] does not implement expected interface '
                        . '[%3$s]. This class is not useable in it\'s current '
                        . 'state.', get_class($this), $classname, $expected)
            );
        }
    }

    /**
     * Registers the observer events to watch for filter registration,
     * if the current object is a valid observer
     * 
     * @return void
     */
    private function bindFilterWatchObserverEvents($subject): void
    {
        $this->verifyFilterWatchlistSubject($subject);
        if ($subject !== $this) {
            return;
        }
    }
}
