<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\pluggable;

/**
 * Module Controller Trait
 * Provides resources for controllers provided by modules to
 * resolve localization of their own assets.
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
trait ModuleControllerTrait
{
    use ModuleLibraryTrait;
    
    /**
     * Returns the help topic for the module cli if it exists, and if the documentation module is installed.
     * 
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $arguments
     * @param \Oroboros\core\interfaces\library\container\ContainerInterface $flags
     * @return \Oroboros\core\interfaces\controller\CliControllerInterface
     */
    public function help(\Oroboros\core\interfaces\library\container\ContainerInterface $arguments = null, \Oroboros\core\interfaces\library\container\ContainerInterface $flags = null): \Oroboros\core\interfaces\controller\CliControllerInterface
    {
        if (!$this->getModules()->has('docs')) {
            return $this->error(404, 'No help page found for this topic because the documentation module is not installed.');
        }
        $controller = $this->load('controller', sprintf('%1$s\\DocumentationController', static::CLASS_SCOPE), $this->getCommand(), $this->getArguments(), $this->getFlags());
        $topic = [
            'category' => 'modules',
            'subcategory' => static::MODULE_NAME,
            'article' => static::CLASS_SCOPE,
        ];
        $arguments = $arguments->toArray();
        $key = array_search('help', $arguments, true);
        if ($key !== false) {
            unset($arguments[$key]);
        }
        $iter = 1;
        foreach ($arguments as $key => $arg) {
            $keyname = sprintf('%1$s%2$s', 'sub', $iter);
            $topic[$keyname] = $arg;
            $iter++;
        }
        return $controller->categoryIndex($this->containerize($topic), $flags);
    }
}
