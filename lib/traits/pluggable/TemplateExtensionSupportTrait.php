<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\pluggable;

/**
 * Template Extension Support Trait
 * Grants a uniform method for checking if a pluggable has
 * additional template support to provide to the template engine.
 * 
 * Usage:
 * If your pluggable supports extending a specific template engine,
 * override the method `packageTemplateExtensions`
 * 
 * This method will need to return a containerized associative array of
 * extensions that are valid for the given template engine. package an array
 * of them and return it with `$this->containerize($your_extensions);`
 * 
 * For any unsupported template engine, return null.
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
trait TemplateExtensionSupportTrait
{
    /**
     * Returns a boolean designation as to whether the current pluggable 
     * provides support for the given template engine identifier.
     * 
     * @param string $engine
     * @return bool
     */
    public function hasTemplateExtensions(string $engine): bool
    {
        return !is_null($this->packageTemplateExtensions($engine));
    }
    
    /**
     * Returns a PSR-11 container of extensions for the given template engine identifier,
     * if the pluggable supplies any. If a request to this method is made for
     * an unsupported type, an InvalidArgumentException will be thrown.
     * 
     * @param string $engine slug name of the template engine (eg "twig", "blade", "smarty", etc)
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface PSR-11 dependency container of the requested extensions for the given template engine identifier
     * @throws \Oroboros\core\exception\InvalidArgumentException if a request for extensions for an unsupported template engine is made
     */
    public function getTemplateExtensions(string $engine): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        if (!$this->hasTemplateExtensions($engine)) {
            throw new \Oroboros\core\exception\InvalidArgumentException(
                sprintf('Error encountered in [%1$s]. Provided template engine [%2$s] does not have any additional supported extensions.', get_class($this), $engine)
            );
        }
        return $this->packageTemplateExtensions($engine);
    }
    
    /**
     * Override this method if your pluggable supports template extensions.
     * You will need to package an array of them with `$this->containerize();`
     * 
     * For unsupported template engines, return `null`.
     * 
     * @param string $engine
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface|null
     */
    protected function packageTemplateExtensions(string $engine): ?\Oroboros\core\interfaces\library\container\ContainerInterface
    {
        return null;
    }
}
