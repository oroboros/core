<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\pluggable;

/**
 * Pluggable Importer Extensible ActionUtility
 * 
 * Allows a pluggable importer to correctly register event and filter bindings
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
trait PluggableImporterExtensibleActionUtility
{

    use \Oroboros\core\traits\pattern\FilterWatcherTrait {
        \Oroboros\core\traits\pattern\FilterWatcherTrait::declareFilterWatchList as private old_declareFilterWatchList;
    }
    use \Oroboros\core\traits\pattern\EventWatcherTrait {
        \Oroboros\core\traits\pattern\EventWatcherTrait::declareEventWatchList as private old_declareEventWatchList;
    }

    /**
     * The underlying base classes that can take filters and events
     * 
     * @var array
     */
    private static $action_aware_base_classes = [
        \Oroboros\core\abstracts\adapter\AbstractAdapter::class,
        \Oroboros\core\abstracts\application\AbstractApplication::class,
        \Oroboros\core\abstracts\bootstrap\AbstractBootstrap::class,
        \Oroboros\core\abstracts\controller\AbstractController::class,
        \Oroboros\core\abstracts\extension\AbstractExtension::class,
        \Oroboros\core\abstracts\model\AbstractModel::class,
        \Oroboros\core\abstracts\module\AbstractModule::class,
        \Oroboros\core\abstracts\service\AbstractService::class,
        \Oroboros\core\abstracts\view\AbstractView::class,
    ];
    
    /**
     * Returns the base classes that are event aware
     * 
     * @return array
     */
    protected function declareEventWatchList(): array
    {
        return self::$action_aware_base_classes;
    }
    
    /**
     * Returns the base classes that are filter aware
     * 
     * @return array
     */
    protected function declareFilterWatchList(): array
    {
        return self::$action_aware_base_classes;
    }

    /**
     * Tells the pluggable to bind to events and filters if no pluggable instance
     * already is bound to them for each base class that allows events and filters.
     * 
     * @param \Oroboros\core\interfaces\pluggable\PluggableInterface $pluggable
     * @return void
     */
    protected function importAssets(\Oroboros\core\interfaces\pluggable\PluggableInterface $pluggable): void
    {
        // no-op
    }

    /**
     * Imports a pluggable. All assets will be lazy loaded on demand,
     * but will load correctly after the pluggable base folder has
     * been passed to this method.
     * @param string $path
     * @return void
     * @throws \InvalidArgumentException If the pluggable is not valid,
     *     or is the wrong type for the current importer.
     */
    protected function importPluggable(string $path): void
    {
        parent::importPluggable($path);
        foreach ($this->fetch() as $key => $pluggable) {
            // Setup event binding listeners
            $this->initializeEventWatchListSubject($pluggable);
            // Setup filter binding listeners
            $this->initializeFilterWatchListSubject($pluggable);
            // Perform any additional import steps as per normal
            $this->importAssets($pluggable);
        }
    }
}
