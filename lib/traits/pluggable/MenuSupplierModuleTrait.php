<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\pluggable;

/**
 * Menu Supplier Module Trait
 * Provides logic for modules that update menus
 * 
 * Usage: Override `declareUpdateMenuIds` with a list of menu id's that
 * the module has edits for.
 * 
 * These menus will be handed to `formatMenu`, which may inject
 * the required sections into the menu and return it.
 *
 * @note This trait should only be implemented on modules that
 *       intend to supply menu sections or links
 * @see \Oroboros\core\interfaces\module\ModuleInterface
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
trait MenuSupplierModuleTrait
{
    
    /**
     * The config data for menu injection. Loads one time the first
     * time the implementing class instantiates.
     * 
     * @var \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    private static $menu_details = null;

    /**
     * Passes the given menu to the internal method referenced
     * by its entry in `self::$valid_menus`
     * 
     * @param string $name
     * @param \Oroboros\core\interfaces\library\menu\MenuContainerInterface $menu
     * @return \Oroboros\core\interfaces\library\menu\MenuContainerInterface
     */
    public function updateMenu(string $name, \Oroboros\core\interfaces\library\menu\MenuContainerInterface $menu): \Oroboros\core\interfaces\library\menu\MenuContainerInterface
    {
        if (array_key_exists($name, self::$valid_menus)) {
            $method = self::$valid_menus[$name];
            return $this->$method($menu);
        }
        return $menu;
    }

    /**
     * Declares the keys of `self::$valid_menus` as valid targets for menu update.
     * 
     * @return array
     */
    protected function declareUpdateMenuIds(): array
    {
        if (!property_exists(get_class($this), 'valid_menus')) {
            return [];
        }
        return array_keys(self::$valid_menus);
    }

    /**
     * Appends demo sections to the primary menu.
     * @param \Oroboros\core\interfaces\library\menu\MenuContainerInterface $menu
     * @return \Oroboros\core\interfaces\library\menu\MenuContainerInterface
     */
    protected function filterPrimaryMenu(\Oroboros\core\interfaces\library\menu\MenuContainerInterface $menu): \Oroboros\core\interfaces\library\menu\MenuContainerInterface
    {
        $this->initializeMenus();
        $class = get_class($menu);
        $links = $menu['links'];
        $links[$this->name()] = $this->generatePrimaryNavLink($class);
        return $menu;
    }

    protected function filterUiMenu(\Oroboros\core\interfaces\library\menu\MenuContainerInterface $menu): \Oroboros\core\interfaces\library\menu\MenuContainerInterface
    {
        $this->initializeMenus();
        $class = get_class($menu);
        $links = $menu['links'];
        $links[$this->name()] = $this->generatePrimaryNavLink($class);
        return $menu;
    }

    private function generatePrimaryNavLink(string $class, array $data = null): \Oroboros\core\interfaces\library\menu\MenuContainerInterface
    {
        $this->initializeMenus();
        $origin = false;
        if (is_null($data)) {
            $origin = true;
            try {
                $data = $class::init(null, self::$menu_details['frontend']['menu']);
            } catch (\Psr\Container\ContainerExceptionInterface $e) {
                // No config defined. This is non blocking.
                return $class::init();
            }
        }
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                $data[$key] = $this->generatePrimaryNavLink($class, $value);
            }
        }
        if ($this->getModules()->has('docs') && $this::app()->debugEnabled() && $origin === true) {
            $help = $this->getModuleHelpLink($class);
            if (!is_null($help)) {
                $data['children'][sprintf('%1$s-help', $this->name())] = $help;
            }
        }
        if (is_array($data)) {
            return $class::init(null, $data);
        }
        return $data;
    }

    /**
     * Returns true if the given id exists in the list of declared menu id's
     * 
     * @param string $menu
     * @return bool
     */
    public function hasMenuData(string $menu): bool
    {
        return in_array($menu, $this->declareUpdateMenuIds(), true);
    }

    private function getModuleHelpLink(string $class): ?\Oroboros\core\interfaces\library\menu\MenuContainerInterface
    {
        return $class::init(null, [
                'id' => sprintf('%1$s-documentation-help-link', $this->name()),
                'title' => sprintf('%1$s Help', $this->title()),
                'icon' => 'fas fa-info-circle',
                'href' => sprintf('/documentation/modules/%1$s/', $this->name()),
        ]);
    }

    private function initializeMenus(): void
    {
        if (is_null(self::$menu_details)) {
            $file = $this->path() . 'config/menus.json';
            if ($this::IS_CLI) {
                $key = 'cli';
            } else {
                $key = 'html';
            }
            if (!is_readable($file)) {
                self::$menu_details = $this->containerize();
                return;
            }
            $data = $this->load('library', 'parser\\JsonParser', $file)->fetch()[$key];
            self::$menu_details = $this->containerize($data);
        }
    }
}
