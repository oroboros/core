<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\pluggable;

/**
 * Module Library Trait
 * Provides common utilities for module libraries
 * to work within their relative scope
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
trait ModuleLibraryTrait
{

    use PluggableUtilityTrait;
    
    /**
     * Returns the module matching the namespace of the current class.
     * 
     * This does not require dependency injection to function correctly.
     * 
     * @return \Oroboros\core\interfaces\module\ModuleInterface
     * @throws \Oroboros\core\exception\ErrorException If no module matches the
     *         current class. This can occur if your are not implementing namespacing correctly.
     */
    protected function getModule(): \Oroboros\core\interfaces\module\ModuleInterface
    {
        $modules = \Oroboros\core\Oroboros::modules();
        $namespace = $this->resolvePluggableNamespace();
        foreach ($modules as $module) {
            if ($module->baseNamespace() === $namespace) {
                return $module;
            }
        }
        throw new \Oroboros\core\exception\ErrorException(
                sprintf('Error encountered in [%1$s]. No module found for namespace [%2$s].', get_class($this), $namespace)
        );
    }
}
