<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\pluggable;

/**
 * Pluggable Utility Trait
 * Provides utility methods for pluggable traits to resolve their parent definer
 * class and reference it locally without dependency injection.
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
trait PluggableUtilityTrait
{
    
    /**
     * Resolves the pluggable base namespace.
     * The pluggable must be using the default PSR-4 configuration
     * for this to work correctly.
     * @return string
     */
    private function resolvePluggableNamespace(): string
    {
        $classname = get_class($this);
        $vendor = substr($classname, 0, strpos($classname, '\\'));
        $classname = substr($classname, strpos($classname, '\\') + 1);
        $package = substr($classname, 0, strpos($classname, '\\'));
        return sprintf('%1$s\\%2$s', $vendor, $package);
    }
    
    /**
     * Resolves the disk location of the pluggable.
     * The pluggable must be using the default directory structure
     * for this to work correctly.
     * 
     * @return string
     */
    private function resolvePluggableLocation(): string
    {
        $autoloader = new \Oroboros\core\library\autoload\Autoloader();
        $source_folder = $autoloader->lookupNamespace($this->resolvePluggableNamespace());
        return substr($source_folder, 0, strrpos($source_folder, 'lib'));
    }
}
