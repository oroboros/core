<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\pluggable;

/**
 * Extension Library Trait
 * Provides common utilities for extension libraries
 * to work within their relative scope
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
trait ExtensionLibraryTrait
{
    use PluggableUtilityTrait;
    
    /**
     * Returns the extension matching the namespace of the current class.
     * 
     * This does not require dependency injection to function correctly.
     * 
     * @return \Oroboros\core\interfaces\extension\ExtensionInterface
     * @throws \Oroboros\core\exception\ErrorException If no extension matches the
     *         current class. This can occur if your are not implementing namespacing correctly.
     */
    protected function getExtension(): \Oroboros\core\interfaces\extension\ExtensionInterface
    {
        $extensions = \Oroboros\core\Oroboros::extensions();
        $namespace = $this->resolvePluggableNamespace();
        foreach ($extensions as $extension) {
            if ($extension->baseNamespace() === $namespace) {
                return $extension;
            }
        }
        throw new \Oroboros\core\exception\ErrorException(
                sprintf('Error encountered in [%1$s]. No extension found for namespace [%2$s].', get_class($this), $namespace)
        );
    }
}
