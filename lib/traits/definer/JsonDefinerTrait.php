<?php
/*
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Oroboros\core\traits\definer;

/**
 * Json Definer Trait
 * Provides a simple mechanism for definers to pull their data from a json file.
 *
 * @author Brian Dayhoff <bdayhoff@gmail.com>
 */
trait JsonDefinerTrait
{

    /**
     * Parses the json file designated by the definer file class constant,
     * and returns the specified data.
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    protected function declareDataSet(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        $file = $this->declareDataRootPath() . static::DEFINER_FILE;
        return $this->load('library', 'parser\\JsonParser', $file)->fetch();
    }

    /**
     * Declares the base path used for relative file path resolution.
     * 
     * By default, this is relative to the application root.
     * 
     * @return string
     */
    protected function declareDataRootPath(): string
    {
        return $this::app()->getApplicationRoot();
    }

    /**
     * Insures that the definer file class constant is declared properly.
     * @return void
     * @throws \Oroboros\core\exception\core\InvalidClassException if the definer file class constant is not correct.
     */
    private function validateDefinerFile(): void
    {
        $class = get_class($this);
        $constant_name = 'DEFINER_FILE';
        $constant = sprintf('%1$s::%2$s', $class, $constant_name);
        if (!defined($constant)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. Expected class constant '
                        . '[%2$s] is not defined. This class is not useable in it\'s'
                        . ' current state.', $class, $constant_name)
            );
        }
        $file = $this->declareDataRootPath() . static::DEFINER_FILE;
        if (!is_readable($file)) {
            throw new \Oroboros\core\exception\core\InvalidClassException(
                    sprintf('Error encountered in [%1$s]. Expected class constant '
                        . '[%2$s] with value [%3$s] does not resolve to a readable file.'
                        . ' This class is not useable in it\'s current state.'
                        , $class, $constant_name, static::DEFINER_FILE)
            );
        }
    }
}
