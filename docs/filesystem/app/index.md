# Oroboros Application File System Documentation {#title}

- [Overview](#overview)
- [Additional Directories](#additional-directories)
    - [The Cache Directory](#app-cache-directory)
    - [The Log Directory](#app-log-directory)
    - [The Node Modules Directory](#app-node-modules-directory)
    - [The Vendor Directory](#app-vendor-directory)
- [Loose Files](#loose-files)
    - [The App Definition](#loose-files-app-definition)
    - [The Front Controller](#loose-files-front-controller)
    - [The Binary Controller](#loose-files-binary-controller)
    - [The `.gitignore` File](#loose-files-gitignore)
    - [The `.env` File](#loose-files-dotenv)
    - [The `.env.defaults` File](#loose-files-dotenv-defaults)
    - [The `.htaccess` File](#loose-files-htaccess)
    - [The `Gruntfile.js` File](#loose-files-gruntfile)
    - [The License File](#loose-files-license)
    - [The ReadMe File](#loose-files-readme)
    - [The `composer.json` File](#loose-files-composer)
    - [The `composer.lock` File](#loose-files-composer-lock)
    - [The `package.json` File](#loose-files-package-json)
    - [The `package-lock.json` File](#loose-files-package-lock)
    - [The Favicon File(s)](#loose-files-favicons)
    - [Extraneous Loose Files](#loose-files-extraneous)

## Overview {#overview}

The application filesystem is very similar to the [standard directory structure][1], however it has a few extra directories to facilitate working as a standalone application.


## Additional Directories {#additional-directories}

The following additional directories are present in an [application][2] in addition to those listed under the [standard directory structure][1]:


### The Cache Directory {#app-cache-directory}

The cache directory is used in the event that the server environment configuration does not declare a [standardized directory][3] for temp files.

By default, Oroboros will look for the standard linux format environment variable [TMPDIR][4] and use that value if it exists, which will typically point to either `/tmp` or `/var/tmp`.

This is used as a template cache, and for any other sort of file caching mechanism in the event that your server does not declare a standard location for temporary files.
If none of these apply, if a standardized temp file location exists, or your app serves only static content that does not require any form of caching, this directory will not be used.


### The Log Directory {#app-log-directory}

The log directory is used for generating server logs in the event that the server environment does not declare a [standardized directory][3] for log files.

By default, Oroboros will look for the standard linux format environment log path `/var/log`, and use that if it exists and is writeable by default. The application can also be optionally configured to utilize your webserver log by configuring your `.env` file or adding the correct definitions to your `$PATH`.

If any of these apply, this directory will not be used.

__note:__ _This may also be configured to override the server settings via an environment variable if you wish to keep logs centralized in your project directory. _


### The Node Modules Directory {#app-node-modules-directory}

The node_modules directory is used for locally compiled node_modules installed via `npm install`. This follows the [standard convention][5] for node modules.


### The Vendor Directory {#app-vendor-directory}

The vendor directory is used locally for compiled composer packages installed via `composer update`. This follows the [standard convention][6] for composer packages.


## Loose Files {#loose-files}

There are a few loose files typically found within an application root directory, which help manage application lifecycle and build operations.


#### The App Definition {#loose-files-app-definition}

The `App.php` file implements the interface `\Oroboros\core\interfaces\application\ApplicationInterface`, and is used to define how your application works in conjunction with the underlying core logic.

The easiest means to create this file is to extend `\Oroboros\core\abstracts\application\AbstractApplication`, and simply override any methods that should not use the default definitions. There is more documentation on this file [here][10].

__This file MUST exist in your root application directory for your application to register correctly with the core.__ You may move it elsewhere and manually include it, however this will disable the automatic build operations provided by the core for your application if you choose to do so.

This file is required.


#### The Front Controller {#loose-files-front-controller}

The `index.php` file acts as the [Front Controller][7] for your application. It will always be the file pointed to for all http requests that do not point to a public resource.

A minimal front controller example is provided below:

```
<?php

/**
 * This example assumes that your vendor/package root Psr-4 namespace is `myapp\app`,
 * and that your oroboros installation exists in a directory adjacent
 * to your root directory in your web root with a directory name of 'core'.
 * 
 * You may need to adjust some of these values to match your existing environment.
 * 
 * In the given example, change the path for OROBOROS_DIRECTORY_LOCATION to reflect
 * the fully qualified path to your oroboros core installation.
 * 
 * Also you must change `new myapp\app\App()` to reflect the correct
 * namespace defined in your `App.php` application definition class.
 * 
 * Any additional custom constants should also be defined here prior to bootstrapping if any are needed.
 */

define('MYAPP_BASEPATH', __DIR__ . DIRECTORY_SEPARATOR);
define('MYAPP_VENDOR', MYAPP_BASEPATH . 'vendor' . DIRECTORY_SEPARATOR);
define('OROBOROS_DIRECTORY_LOCATION', realpath(MYAPP_BASEPATH . '../') . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR);
if (is_readable(MYAPP_VENDOR . 'autoload.php'))
{
    require_once MYAPP_VENDOR . 'autoload.php';
}
require_once OROBOROS_DIRECTORY_LOCATION . 'setup.php';
require_once MYAPP_BASEPATH . 'App.php';
\Oroboros\core\Oroboros::bootstrap(new myapp\app\App());

```

This file is required.


#### The Binary Controller {#loose-files-binary-controller}

The `app` file acts as the [Front Controller][7] for command line operations within the scope of your application. All app-specific command-line functionality can be facilitated through the use of this file. See the [bash api][8] for more information.

The format for this file is as follows:

```
#!/bin/bash
_binary="$(which oroboros 2>/dev/null)";
which oroboros > /dev/null 2>&1;
if [ $? -eq 0 ]; then
  oroboros $@;
else
  printf "\e[91m[::ERROR::] No path to dependency binary [\e[93moroboros\e[91m] detected.\e[0m\n";
  printf "\e[91m            Please use the following command to make it accessible:\e[0m\n";
  printf "\e[91m            [\e[93msudo ln -s /path/to/oroboros/oroboros /usr/local/bin/oroboros\e[91m]\e[0m\n";
  exit 1;
fi
```

There is no need to add additional bash logic, the underlying bash api will localize to the app if it detects that it was called by this file, and sandbox operations only to the current app scope.

If you create this file in your application root, you must also run `sudo chmod +x ./app` to make it executable. After this, you may utilize it by calling `./app` from your command-line, and it's api is identical to the standard oroboros [bash api][8], except that it will only run commands _in the current application scope_ and cannot execute tasks globally.

This file is optional.


#### The `.gitignore` File {#loose-files-gitignore}

The root directory `.gitignore` acts as the master git ignore config for your application. Subdirectories may also contain a `.gitignore` file to maintain their own scope. This tells your version control which files not to push into your repo.


#### The `.env` File {#loose-files-dotenv}

The `.env` file allows localized environment configurations to be shared between http requests and command-line access that is not appropriate to add to the server environment itself. This acts as an override for [`.env.defaults`](#loose-files-dotenv-defaults), and is not automatically present. If it is present, it will _not be added to your git repository by default_, as it may leak sensitive credentials to unauthorized parties.

This file is optional.


#### The `.env.defaults` File {#loose-files-dotenv-defaults}

The `.env.defaults` file allows localized environment configuration to be shared between http and command-line access that is not appropriate to add to the server environment itself. Unlike the [`.env`](#loose-files-dotenv) file, this _is added to version control by default_, and represents _generalized environment configurations that should exist across all instances of the application_. This file will be overridden by identical keys that exist in the `.env` file, if they exist _(and if the `.env` itself exists)_.

__note:__ _You should not store any sensitive configurations that should not be distributed in this file. This includes database passwords, api keys, or any other sensitive credentials that your app may need. These should be either placed in a `.env` file or added to your server configuration._

This file is required.


#### The `.htaccess` File {#loose-files-htaccess}

This file will act as the default webserver configuration _in the absence of a virtual host definition_. It is advised for production settings to create a `vhost.conf` file that is owned by your webserver to use as a replacement for this file. However, this will keep the vast majority of your web requests secure, provided that you set the file permissions to not be editable by the http server.

This file is required if you do not have a `vhost.conf` file to replace it. If your environment has created a `vhost.conf` file, it may be removed _in that environment only_.


#### The `Gruntfile.js` File {#loose-files-gruntfile}

This file handles the standard build operations for your application if you are using the default build configuration. It is used to manage the build, asset optimization, and numerous general setup tasks. It works in conjunction with the bash build script to dynamically generate configurations that cannot be tracked easily through static configurations, although it can also accept static configurations as per the typical [grunt specification][9].

This file is required for builds to work. If you are running a pre-built production deployment package, it may be removed _in that environment only_.


#### The License File {#loose-files-license}

If your application has a general license, it should be placed in this file in markdown format.

This file is optional.


#### The ReadMe File {#loose-files-readme}

If your application has a general readme, it should be placed in this file in markdown format.

This file is optional.


#### The `composer.json` File {#loose-files-composer}

This file provides instructions to composer about the dependency packages your php code relies upon, and your application definition if you are distributing your app through packagist.

This file is required for builds to run correctly. If you are running a pre-built production deployment of your application, you may remove this file _in that environment only_.


#### The `composer.lock` File {#loose-files-composer-lock}

This file locks composer dependencies to specific pre-defined versions. The composer specification suggests that you commit this file.

_If you are adjusting your composer package dependencies frequently during early development, you should not commit this file._

_If you have a stable set of composer package dependencies that are unlikely to change, you should commit this file._

This file is optional.


#### The `package.json` File {#loose-files-package-json}

This file defines your npm package dependencies, and your app definition if it is intended to be distrubuted through npm.

This file is required for builds to run correctly. If you are running a pre-built production deployment of your application, you may remove this file _in that environment only_.


#### The `package-lock.json` File {#loose-files-package-lock}

This file locks npm package dependencies to specific pre-defined versions. The npm specification suggests that you commit this file.

_If you are adjusting your npm package dependencies frequently during early development, you should not commit this file._

_If you have a stable set of npm package dependencies that are unlikely to change, you should commit this file._

This file is optional.


#### The Favicon File(s) {#loose-files-favicons}

This file _(or files)_ determines the icon that shows in the browser tab rendering your page. This is automatically picked up without any additional configuration, if it exists.

This file is optional.


#### Extraneous Loose Files {#loose-files-extraneous}

Additional files required for a custom application may be added to the root directory on a case by case basis without interfering with operations.

It is suggested that whenever possible _configuration files be placed in the [`etc` directory](/documentation/core/filesystem/#universal-etc-directory) for security purposes_.
As this is not always possible due to the various non-standard requirements of various dependencies, they may be placed in your application root as well if required, however you are responsible for effectively managing the security implications of loose files that you add which are not defined within this documentation.

Typically you will want to update your `.htaccess` or `vhost.conf` file to deny web views of any additional configuration files, and you will also want to insure that file permissions and ownership are correct for intended access rights.




[1]: /documentation/core/filesystem/
[2]: /documentation/core/usage/php/app/
[3]: https://en.wikipedia.org/wiki/Filesystem_Hierarchy_Standard
[4]: https://en.wikipedia.org/wiki/TMPDIR
[5]: https://docs.npmjs.com/packages-and-modules
[6]: https://getcomposer.org/doc/01-basic-usage.md
[7]: https://en.wikipedia.org/wiki/Front_controller
[8]: /documentation/core/usage/cli/
[9]: https://gruntjs.com/getting-started
[10]: /documentation/core/usage/php/app/