# Oroboros Pluggable File System Documentation {#title}

- [Overview](#overview)
- [Loose Files](#loose-files)

## Overview {#overview}

Pluggables include [modules][2], [extensions][3], and [services][4], all of which follow the [standard directory structure][1], with two minor but important caveats.



## Loose Files {#loose-files}

All Pluggables require two additional files in their root directory addition to following the [standard directory structure][1]. They must have a _Definer Class_, and a `manifest.json` file.

All pluggable definer classes implement the interface `\Oroboros\core\interfaces\pluggable\PluggableInterface`, which designates that they represent additional functionality that can be added to the system.

Documentation on the definer class for each pluggable type can be found within their corresponding link, as listed above in the [overview](#overview).

[1]: /documentation/core/filesystem/
[2]: /documentation/core/usage/php/module/
[3]: /documentation/core/usage/php/extension/
[4]: /documentation/core/usage/php/service/