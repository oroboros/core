# Oroboros Standard Directory Structure Documentation {#title}

- [Overview](#overview)
- [Standard Directory Structure](#standard-directory-structure)
    - [The Config Directory](#universal-config-directory)
    - [The Documentation Directory](#universal-documentation-directory)
    - [The Etc Directory](#universal-etc-directory)
        - [The Bin Directory](#universal-etc-bin-directory)
        - [The Build Directory](#universal-etc-build-directory)
        - [The Source Directory](#universal-etc-source-directory)
        - [The Test Directory](#universal-etc-test-directory)
    - [The Lib Directory](#universal-lib-directory)
    - [The Template Directory](#universal-tpl-directory)


## Overview {#overview}

Oroboros uses a standardized uniform filesystem format that applies to the core, apps, extensions, modules, services, and most any other package designed to work with this system.

Using a uniform filesystem allows all assets that extend upon or augment the core platform to be treated in a unified way, making it far simpler to handle CI operations, testing, building, compatibility evaluation, and other related concerns.


## Standard Directory Structure {#standard-directory-structure}

The following directories are always recognized when placed in any asset root folder:

- [config](#universal-config-directory) _(optional, but nearly always needed)_
- [docs](#universal-documentation-directory) _(optional)_
- [etc](#universal-etc-directory) _(optional)_
- [lib](#universal-lib-directory) _(required for any php code supplied)_
- [tpl](#universal-tpl-directory) _(optional)_

### The Configuration Directory {#universal-config-directory}

The configuration directory is used for storing default configurations. These are the baseline configuration for the core, an application, an extension, module, or service, which is why the core and most extensions andd modules _do not require a database to run_. If a data layer is used, some of these may be ignored or overridden, depending on how the data layer is implemented. Regardless of override, they will typically be imported into the data layer, and also are automatically cached if you have a cache layer declared, which speeds up the overhead of loading numerous loose files or polling a database on each subsequent operation significantly.

All configs are typically stored in json format.


### The Documentation Directory {#universal-documentation-directory}

Any documentation that is relevant to the current scope is stored here. Documentation can be written in simple markdown format, and it will be automatically picked up by the [Documentation Module][1] if it is enabled.

Documentation folders _must_ contain an index.md file or they will be ignored. Aside from that, they may contain any number of documentation files in canonicalized name format, and may also contain subfolders following the same convention. Documentation may nest up to 12 levels deep without any further configuration.

### The Etc Directory {#universal-etc-directory}

The etc directory contains private programmatic resources. This includes javascript source files, scss, bash scripts, extraneous data assets that do not warrant a configuration, build scripts, error page definitions, media, unit testing assets, etc.

No frontend content is ever served directly from this directory. Rather it is migraged into the [public directory]() when a build is run.

An explanation of typical subdirectories may be found below:

#### The Bin Directory {#universal-etc-bin-directory}

The bin directory contains the bash api. Please refer to the [bash filesystem][2] for more extensive information.


#### The Build Directory {#universal-etc-build-directory}

The build directory contains any scripts used to augment build operations. Typically these will only exist in the [core][3] or an [application][4], however they can also be used in [modules][5], [extensions][6], or [services][7] if they require a complex setup process.


#### The Source Directory {#universal-etc-source-directory}

The source directory contains javascript source files, scss, and media assets that are packaged directly with the source files. All of these are compressed, optimized, linted, and moved to the [public directory][] when a build is run.


#### The Test Directory {#universal-etc-test-directory}

The test directory includes any assets used to facilitate unit testing. _It does not contain actual tests, only assets used in conjunction with them_. Examples include sample configurations, files to test upload/download/extract/compress/etc operations, or any other related assets that are not code but are used to facilitate unit testing.


### The Lib Directory {#universal-lib-directory}

The lib directory contains _all php class-based source code for the current scope_, with the only exceptions being front controllers, App definition files, or module/service/extension definition files, which are placed in their corresponding root folder.


### The Template Directory {#universal-tpl-directory}

The template directory contains all templating files. Immediate subfolders sould correspond to the name of the template engine that needs to load the templates, followed by the scope that the templates serve.

For example, if you were looking for a twig template that served html output, it's path would be in `tpl/twig/html/...`.

Likewise if you had an extension that adds support for Laravel Blade which was serving html, it's path would be `tpl/blade/html/...`.


[1]: /documentation/modules/docs/
[2]: /documentation/core/filesystem/bash/
[3]: /documentation/core/
[4]: /documentation/core/usage/php/app/
[5]: /documentation/core/usage/php/module/
[6]: /documentation/core/usage/php/extension/
[7]: /documentation/core/usage/php/service/