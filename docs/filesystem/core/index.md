# Oroboros Core File System Documentation {#title}

- [Overview](#overview)

## Overview {#overview}

The core filesystem is very similar to the [standard directory structure][1], however it has a few extra directories to facilitate working as a standalone application or being used to facilitate application management.

All of the extra directories and loose files that apply to an [application filesystem][2] also apply to the core filesystem with the exception of there not being an `app` binary or `App.php` application definition.

The `oroboros` global binary _which has global scope_ is used in place of the `app` binary _(which only has local application scope)_. This is the root binary controller that provides the underlying [bash api][3].

Likewise, there is no default `App.php` in the oroboros core root, because the core provides a couple of app definition fallbacks of it's own, which are internally used when no `App.php` is present in the current scope. These are selected from their own lib sub-path, and the logic to select the appropriate one is hard coded as a fallback resource. Providing _any_ valid `App.php` will entirely negate the use of these.

Other than these caveats, the filesystem used by core is _identical to the [application filesystem][2]_.

[1]: /documentation/core/filesystem/
[2]: /documentation/core/filesystem/app/
[3]: /documentation/core/usage/cli/