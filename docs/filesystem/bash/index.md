# Oroboros Bash File System Documentation {#title}

- [Overview](#overview)

## Overview {#overview}

The bash filesystem is very similar to the [standard directory structure][1], however it has a few very slight variations required to work with low level operations that shell code typically handles.

[1]: /documentation/core/filesystem/