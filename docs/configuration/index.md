# Oroboros Core Configuration {#title}

- [Overview](#overview)
- [Configuration via Web Browser](/documentation/core/configuration/browser/)
- [Configuration via Command Line](/documentation/core/configuration/command-line/)
- [Configuring the Environment](/documentation/core/configuration/environment/)

## Overview {#overview}