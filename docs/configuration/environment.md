# Oroboros Environment Configuration {#title}

- [Overview](#overview)
- [Default Environment](#default-environment)
    - [The .env.defaults File](#env-defaults-file)
- [Customizing the Environment](#custom-environment)
- [The .env File](#env-file)
    - [Required Keys](#env-required-keys)
    - [Optional Keys](#env-optional-keys)
    - [Defining Connections](#env-defining-connections)
        - [Example MySQL Connection](#env-example-connection-mysql)
        - [Example Redis Connection](#env-example-connection-redis)
        - [Example Supervisord Connection](#env-example-connection-supervisord)

## Overview {#overview}

The default behavior of Oroboros is to utilize values located in a `.env` file located in the application root.
_This is true both for Oroboros Core as well as applications extending from the Oroboros Core framework._

The core also contains a `.env.defaults` file, which sets the baseline values for the core. This file is distributed with the installation.
Any keys from `.env.defaults` that also exist in a `.env` file will be overridden by the values in the `.env` file.

These keys _are resolved prior to classloading, and compile as class constants directly into the application interface,_
which renders them entirely immutable at runtime. Provided file permissions on the `.env` and `.env.default` file are correct,
this presents a secure way to make environment information accessible throughout the application without worry of it being altered 
by application logic.

_Note: No sensitive information such as passwords are stored in interfaces, only default parameters about the application environment 
that would commonly be required in numerous places to make runtime logic decisions._

## Default Environment {#default-environment}


### The .env.defaults File {#env-defaults-file}

The core ships with an `.env.defaults` file that acts as a fallback resource in the absence of a `.env` file definition.
This allows the core to operate without significant additional configuration. All of these keys may be overridden by placing them
in a `.env` file, which takes precedence if it exists.

## Customizing the Environment {#custom-environment}


### The .env File {#env-file}

When a new application is defined (or when the default core package is installed), there is no `.env` file defined. This may be created
by the user to modify how the core or application operates.

### Required Keys {#env-required-keys}

The following keys are required for the application to operate:

- `OROBOROS_CORE_ENVIRONMENT` - This defines the internal environment state of the application.
  Valid keys are `local`, `staging`, `production`, and `development`.
  The default value is `local`.
- `OROBOROS_CORE_SERVER_USERNAME` - This defines the system user running the application.
  This value typically does not need to be changed.
- `OROBOROS_CORE_SERVER_USERGROUP` - This defines the usergroup of the system user running the
  application. This value typically does not need to be changed.
- `OROBOROS_CORE_APPLICATION_ROOT` - This defines the application root of the application.
  This value should not be changed.
- `OROBOROS_CORE_TERMINAL_COLUMNS` - This value determines the number of display columns for cli
  operations. This value should not be changed.
- `OROBOROS_CORE_TERMINAL_LINES` - This value determines the row count of the display for cli operations.
  This value should not be changed.
- `OROBOROS_CORE_DEBUG` - This value determines whether or not debug operations will be enabled at runtime.
  This should be overridden and set to `false` in a production environment.
- `OROBOROS_CORE_TEMPLATE_ENGINE` - This value determines the default templating engine used by the application.
  This value may be changed if another template engine is made available (eg smarty). In order to do so,
  a compatible adapter for that template engine needs to be supplied that implements
  `Oroboros\core\interfaces\library\template\TemplateInterface`. This would typically be supplied in a [module](/documentation/modules/).

_Note: All of these keys are defined in the `.env.defaults` file. If circumstance does not dictate changing them, it is best to leave them be._


### Optional Keys {#env-optional-keys}

### Defining Connections {#env-defining-connections}

Connection credentials for the application to databases, caching, job queues, etc. are typically defined in the `.env` file.
This allows the credentials to be provided securely without propagating into git repositories and potentially exposing access rights
to those who shouldn't have them.  
The format for connection credentials follows two parts, a set of _definer_ keys, and a _selector_ key that tells the application to 
use a specific connection definition as it's default.

#### Example MySQL Database Connection {#env-example-connection-mysql}

The _definer_ keys to provide the application with secure mysql credentials should look similar to the following:

```
APP_CONNECTIONS_MYSQL_DEFAULT_SCHEMA=example
APP_CONNECTIONS_MYSQL_DEFAULT_USER=example
APP_CONNECTIONS_MYSQL_DEFAULT_PASSWORD=password
APP_CONNECTIONS_MYSQL_DEFAULT_HOST=localhost
APP_CONNECTIONS_MYSQL_DEFAULT_PORT=3306
APP_CONNECTIONS_MYSQL_DEFAULT_TYPE=mysql
```

This also requires a _selector_ key if it is desired to be used as the default database connection, which should follow
the following format:

```
APP_CONNECTIONS_DEFAULTS_MYSQL=default
```

If this were the only definition provided in your `.env` file, the full file would look like this:

```
#./env

APP_CONNECTIONS_DEFAULTS_MYSQL=default
APP_CONNECTIONS_MYSQL_DEFAULT_SCHEMA=example
APP_CONNECTIONS_MYSQL_DEFAULT_USER=example
APP_CONNECTIONS_MYSQL_DEFAULT_PASSWORD=password
APP_CONNECTIONS_MYSQL_DEFAULT_HOST=localhost
APP_CONNECTIONS_MYSQL_DEFAULT_PORT=3306
APP_CONNECTIONS_MYSQL_DEFAULT_TYPE=mysql
```

This will tell the application that if no database is supplied to use the database connection named `default`, and
on connection attempts the database name (schema) is `example`, the database username is `example`, the password is `password`, 
the hostname is `localhost`, the port is `3306`, and the pdo driver used to handle connection is `mysql`.

All mysql-based models within the application would then attempt to use this connection if no other connection is supplied to them.


#### Example Redis Connection {#env-example-connection-redis}

The _definer_ keys to provide the application with secure redis credentials as a caching layer should look similar to the following:

```
APP_CONNECTIONS_REDIS_DEFAULT_HOST=localhost
APP_CONNECTIONS_REDIS_DEFAULT_PORT=6379
```

This also requires a _selector_ key if it is desired to be used as the default cache layer, which should follow
the following format:

```
APP_CONNECTIONS_DEFAULTS_CACHE_REDIS=default
```

This will tell the application to use Redis as it's default cache connection, which may be reached at `localhost` on port `6379`.

If this were the only definition provided in your `.env` file, the full file would look like this:

```
#./env

APP_CONNECTIONS_DEFAULTS_CACHE_REDIS=default
APP_CONNECTIONS_REDIS_DEFAULT_HOST=localhost
APP_CONNECTIONS_REDIS_DEFAULT_PORT=6379
```

_Note: Redis may also be used as a job queue rather than a cache. This example demonstrates how to define it as a caching layer, 
but it may be used for a number of puropses, and numerous other caching strategies may also be used in place of redis._


#### Example Supervisord Connection {#env-example-connection-supervisord}

The _definer_ keys to provide the application with secure supervisord credentials as a job scheduler should look similar to the following:

```
APP_CONNECTIONS_SUPERVISORD_DEFAULT_HOST=localhost
APP_CONNECTIONS_SUPERVISORD_DEFAULT_PORT=9001
```

This also requires a _selector_ key if it is desired to be used as the default job task manager, which should follow
the following format:

```
APP_CONNECTIONS_DEFAULTS_SUPERVISORD=default
```

This will tell the application to use Supervisord as it's default job queue, which may be reached at `localhost` on port `9001`.

If this were the only definition provided in your `.env` file, the full file would look like this:

```
#./env

APP_CONNECTIONS_DEFAULTS_SUPERVISORD=default
APP_CONNECTIONS_SUPERVISORD_DEFAULT_HOST=localhost
APP_CONNECTIONS_SUPERVISORD_DEFAULT_PORT=9001
```

