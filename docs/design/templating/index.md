# Oroboros Core Templating Documentation {#title}

- [Overview](#overview)

## Overview {#overview}

Oroboros's default template engine is [Twig][1], however it will support any templating implementation that implements the interface `\Oroboros\core\interfaces\library\template\TemplateInterface`.

The default layout specification provided with the system is [Bootstrap 5][2], which is implemented via the [Bootstrap Extension][3]



[1]: https://twig.symfony.com/
[2]: https://getbootstrap.com/
[3]: /documentation/extensions/bootstrap/