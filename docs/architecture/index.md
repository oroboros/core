# Oroboros Core Architecture Overview {#title}

- [Overview](#overview)
- [Design Principles](#design-principles)
    - [Require Nothing, Support Everything](#design-principles-requirements)
    - [Opinion Belongs in Business Logic, not in Underlying Utilities](#design-principles-requirements)
    - [Follow Existing Accepted Standards Explicitly](#design-principles-requirements)
    - [Trust the Environment First](#design-principles-environment)
    - [Support Cross-Application](#design-principles-cross-application)
    - [Don't Mince Scopes](#design-principles-scoping)
    - [Build Clean, Ship Clean](#design-principles-build-process)
    - [Don't be Trendy, Be Practical](#design-principles-cross-practicality)

## Overview {#overview}

Oroboros Core is presents a _unified development stack_ designed to facilitate 
quick and efficient development of secure, performant, and scalable web applications.

It is designed to present a unified api across multiple layers of the application 
stack, so that familiarity with _any_ level of the stack will inherently provide 
some intuitive understanding of _any other layer of the stack_.

The core ships with three primary components, the [bash api][1], [php api][2], 
and [javascript api][3]. Each of these can be utilized _independently_ or 
_inter-dependently_, and can freely be used in conjunction with any other 
relevant technologies preferable to your specific needs.


## Design Principles {#design-principles}

At it's heart, Oroboros intends to be a toolbox of unopinionated resources for 
unified application development that can be used either as a standalone 
application platform or as middleware, and supports a traditional single-server 
stack as well as an extensible, scalable cloud-driven approach. In following with 
this core principle, there are a number of intentional design choices made to 
maximize extensibility and scalability without convoluting the development cycle.


### Require Nothing, Support Everything {#design-principles-requirements}

While it is impossible to have absolutely no requirements, Oroboros strives to 
drastically minimize expectations on the underlying server environment, and 
grant at least partial functionality in all cases where it is supported. 
The [bash api][1] does not inherently require php, node.js, or any other 
requirements of the other two api's, and can be freely used standalone on any 
system that has at least bash 2.4 enabled. Likewise, the [php api][2] requires 
php 7.4 or later, but does not require that the bash or javascript dependencies 
exist. The javascript api requires node.js 14.16.1 or better _for server-side 
javascript only_ and ES6 support for _browser side javascript_, but does not 
have explicit dependence on bash or php. Any of these api's can be freely used 
by themselves without the presence of the requirements of the others.

Oroboros _does not require a database, but supports several database types._

Oroboros _does not require a cache layer, but supports redis, memcached, 
and opcache if they are present._

Oroboros _does not require supervisord, but supports it if it is present, 
and provides its own daemonizing implementation if it is absent._

Oroboros _does not require beanstalkd, but supports it if it is present, 
and can easily be extended easily to provide unified compatibility with any 
MQ implementation._

Likewise, all future additions to the core framework will treat _all dependencies 
as optional rather than hard requirements_ in all cases where this is possible, 
and will simply provide additional functionality when they are detected or 
configured.

In keeping with this philosophy, the underlying dependency and package requirements 
of Oroboros are kept as slim as possible to maintain extensibility and minimize 
breakage due to upstream changes.


### Opinion Belongs in Business Logic, not in Underlying Utilities {#design-principles-requirements}

Oroboros does not inherently impose any opinion about runtime logic beyond what 
is required to make the system run. It does not box you into any specific 
development approach. It does not require that you follow it's standards. 
It does not require that you learn new things or jump through hoops just to 
make it run. 
It does provide a number of resources to speed up development time, and does have 
it's own internal standards to support these tools where it is appropriate, 
but none of them are imposed directly as mandatory.

It is the underlying objective of this system to leave all decisions about 
technological need and execution plan to the developer building on the platform, 
and to supply straightforward, pragmatic, and easy to follow and maintain options 
to anyone who desires to use them.

Opinion is absolutely neccessary to perform any coherent objective, but it is 
ideally the realm of the developers coding toward a specific objective to impose 
this, not the tools they use.


### Follow Existing Accepted Standards Explicitly {#design-principles-requirements}

In order to keep Oroboros extensible and compatible with the maximum number of 
other technologies, great effort has been made to always code toward the existing 
accepted standards first. Any new development is preceded by a great deal of 
research into existing accepted [RFC Specifications][4], and language specific 
implementations follow the most universally applied best practices for the 
existing language.

The [php api][2] draws extensively from the [PHP-FIG accepted PSR's][5].

The [javascript api][3] draws heavily from the generally accepted [MDN best 
practices][6], but operates with a distinct nod to browser adoption to maintain 
portability when standards have not yet become widely implemented.

The [bash api][1] does not follow any explicit coding standard as no universal 
one exists, however it makes a point to favor portability, security, readability, 
and extensibility, and to defer opinion to the specific implementation as much 
as is possible. Likewise its [api][1] follows the typical paradigm of both the 
[php api][2] and [javascript api][3] as closely as possible, _language limitations 
notwithstanding_, so that building with it or extending upon it has a distinctly 
familiar feel to the rest of the stack.

_It should be noted that the [bash api][1] does not explicitly follow the 
POSIXLY\_STRICT standard, which is too limiting to allow for a coherent api to 
exist between all three api's. For this reason, it should be assumed that 
the bash api will **not** automatically work with a posix shell other than bash. This 
was an intentional design decision to support readability and extensibility 
without making the bash development process overly painful to developers using it._

__note to Mac users:__ The version of bash that ships with OSX is _severely deprecated_. 
This is due to Apple not liking the license used by newer versions of bash, 
and has nothing to do with underlying functionality. 
You will need to install a more up to date version via homebrew and put it 
into your `$PATH` for the cli api to work on OSX. Your mac will still work fine 
with a newer bash version. Apple has recently migrated to Zsh in newer releases, 
and support for this is currently untested. The typical shell sticking point is 
that older bash versions and several other shells do not support associative 
arrays. Zsh does, so it should work fine. It just has not been formally tested 
as of yet.

### Trust the Environment First {#design-principles-environment}

Oroboros attempts to use the generally accepted [standard environment definitions][7] 
and environment variables wherever applicable in order to maximize portability 
and expected operation. In the event that non-standard settings exist or a system 
is not correctly configured, some auxilliary workarounds are supplied to prevent 
disruption of execution, but these are never favored when a well defined standard 
environment option exists.

Likewise, custom localized environment support is also provided so that 
application-scope overrides can be supplied securely and intuitively.


### Support Cross-Application {#design-principles-cross-application}

Oroboros is not intended to take over your entire stack, or complicate the usage of 
existing tools that you rely upon or favor in your typical workflow. 
It is inherently designed not to make any of it's underlying tools limit the 
functionality provided by other tools or hijack the workflow from them. 
If you favor Angular or React for the frontend, it is not mandatory that you 
use the supplied javascript api. If you favor Laravel as an underlying framework, 
you can use the provided libraries within it without issue and without them colliding. 
If you prefer to do builds with gulp or webpack over grunt, you can substitute 
either of those without issue, or use any combination of the three without 
convoluting your workflow. 

This is inherently the purpose of taking as much of an unopinionated approach to 
framework architecture as is possible; it leaves the option to integrate other 
tools seamlessly with minimal conflict. We all want to get more done in less time. 
Re-learning an entirely new approach to accomplish an already existing result does 
not support that, and is inherently burdensome to those using the tools that do it. 
This tool is committed to not doing that.


### Don't Mince Scopes {#design-principles-scoping}

As Oroboros provides a unified api meant to work at multiple scopes, a great deal 
of effort has gone into insuring that scope collision does not occur. Controllers 
that handle http requests **cannot** be run from the command line, and controllers 
that handle cli requests **cannot** be called directly from the web. 

This restriction helps to insure that security measures cannot be bypassed or 
simply ignored, and that unintended exploits do not bubble up without any 
understanding as to how they happened.

Oroboros accomplishes this by baking environment definitions into interfaces 
dynamically prior to compiling them that are then inherited by another interface 
declaring the expected environment value explicitly with an identical class 
constant key. If they match, the class compiles clean. 
If they do not, it generates a fatal error that cannot be avoided and immediately 
kills execution. As these interfaces are the basis for method parameters, this 
cannot be avoided by a potential attacker without bypassing the whole system entirely, 
leaving it next to impossible to leverage out of scope exploits within the 
existing codebase. This is likewise implemented in tools that should only be 
available in a specific scope to prevent them from rogue inclusion elsewhere. For 
example, a websocket application _needs to boot from the command line_, so it 
cannot be initiated during the runtime of an http request. 

This can be worked around by using an MQ server to relay between a command line 
task and an http task without exposing either to the immediate vulnerabilities 
of the other. 
Likewise, command line tools that scaffold applications cannot be directly run 
from http, as even in an intended use case it would require dangerously lax file 
permissions to accomplish that would be easy to exploit, and allowing this kind 
of functionality would make it far too easy to inject malicious code remotely. 
If you need http to run command-line oriented tasks, relay it to a job queue or 
websocket, and if you need command line tasks to run http, make a curl request 
to your own site, so that both can operate in their expected scope without 
introducing bugs or exploits.

__note:__ _While this prevents out-of-scope code from running, you can 
inadvertently introduce exploits if you do not check authorization between 
separate threads communicating over a commonly understood protocol. 
Scope enforcement prevents exploits only within the same thread, it is up to the 
developer to enforce overall security across their entire stack, and this cannot 
be automatically approached meaningfully without consideration of the specific 
implementation_.


### Build Clean, Ship Clean {#design-principles-cross-build-process}

In contrast to most other systems, Oroboros _does not support pre-built plugins_. 
This is intentional. Pluggable functionality defers it's build operation to the 
application running it, and compiles at the same time as the application. 

The purpose of this is to insure that production bugs arising from conflicting 
plugins do not occur. 
If a module conflicts with another installed module, it will be immediately 
apparent because the build will fail, which leaves very little chance that a 
conflict will be shipped to production and cause widespread havoc and potentially 
dire consequences for a live application when it hits end-user space.

This prevents modules and extensions with underrlying reliance on differing 
versions of a common dependency from breaking each other. If both can't compile, 
they are simply not compatible in their existing version declaration, and one or 
both will need to be removed or downgraded to insure production stability.

Likewise, unit testing is automatic during builds, and any module, extension, 
or service is _explicitly expected to pass the same underlying tests that the 
application uses_. This insures that code that already works does not break 
due to a hasty module downloaded that doesn't run clean. Modules cannot build 
if they cannot honor the unit tests for their scope that the core platform uses. 
If something claims it provides an http controller, it's controller _must pass 
the core unit tests for http controllers or it is not possible to enable it_. 
The same is true of any other class type.

You may also extend upon this to provide your own application specific unit 
tests to determine whether or not any given extension, service, or module is 
_safe to run without issue in your own environment_. All pluggables __must__ 
pass the core unit tests to be deployed at all, and may _optionally be required 
to pass your own too_.


### Don't be Trendy, Be Practical {#design-principles-cross-practicality}

Oroboros is intentionally straightforward, clear cut, unpretentious, and explicitly 
avoids using nonsensical product identifiers for common features or supporting 
every new thing that is all the rage. Official offerings intentionally abhor hype, 
meaningless non-descriptive names, and shortcuts to save a few keystrokes or 
utilize brand new language features just for the sake of using them.

_It is more important to understand how to accomplish the desired work quickly, 
clearly, and effectively than it is to appear cool or hip._

What is timelessly cool and hip is _getting things done reliably_. 
Everyone loves that. Everyone has always loved that, and they always 
will love that.

For this reason, development does not take shortcuts to save keystrokes at the 
expense of significantly greater downtime debugging later on. The underlying 
codebase does not use `use` statements to shorthand namespaces, all functions 
explicitly declare what they take and what they give, so anyone backtracing 
doesn't need to waste time guessing which of five different namespaces any 
given input is meant to come from.

Exception and error messages are clear, descriptive, and impart exactly what 
went wrong and where it went wrong. When underlying dependencies throw cryptic 
errors, they are as often as is possible wrapped in a more descriptive 
error message to make debugging them coherent and straightforward.

While interfaces commonly enforce explicit input and output for public api's of 
classes, Oroboros also applies this standard to protected and private methods, 
so backtracing or investigating source code to determine what is going on is 
clear cut and straightforward, even in methods not intended to be extended upon.

We do not use trendy marketing names for official modules or extensions. 
We use clear cut, descriptive names that give a meaningful and intuitive 
understanding of what they do and how they accomplish it. We avoid acronyms 
wherever possible, and where they do exist they are written to explain clearly 
first, not to look like some other irrelevant word just because it sounds neat. 
Things that work well sell themselves and do not need marketing hype to gain 
a following. That is our intended selling point.

Documentation is provided clearly with real world examples. It is not cut from 
a generic template or arcane. The docs will explain what a thing does, how, 
what options exist, what it is intended to work alongside if anything specific, 
and provide legible and understandable examples for common use cases. 
All documentation that ships with this system was written alongside a 
hand-built test case demonstrating the effectiveness of each example prior to 
entering it into documentation, so documentation examples _exactly reflect 
actual use_.

Public facing api's _do not change without a very, very good reason_. Almost all 
classes share a common constructor, and will always share a common constructor. 
It will take the exact same three arguments pretty much forever, so there will 
never be any need to wrap existing code in an ever increasing number of factories 
to resolve to the same result. Implementations will change over time, however 
they will always favor changing under the hood without disrupting the public 
api in all possible cases, unless for whatever reason the public facing api is 
unclear or ambiguous.

Adoption of additional technologies is based on real-world adoption and the 
practical impact of implementation, not on current buzz or trending hashtags. 
When a new technology proves real world worth, it will be supported officially. 
It will not be added for the sake of jumping on a bandwagon that just happens 
to have a lot of people that recently jumped on it.

The api is intentionally descriptive and modeled after natural language, and is 
nearly identical in all three of the provided api's. This was an intentional 
design decision to support ease of understanding for those unfamiliar who are 
trying to learn how to leverage it, and to allow developers that typically only 
work at one level of the stack to review source from other layers outside their 
typical realm without a huge learning curve. Likewise, key features specific to 
one language are emulated in the others if they do not exist to keep the api's 
consistent. The PHP codebase emulates events, which are heavily relied upon in 
javascript. The javascript codebase emulates interfaces, which are heavily 
relied upon in php to maintain code coherence. the public method api of classes 
in both is pretty much identical, and accomplishes a nearly identical result.

It is our hope that with this approach, this platform will remain easy to use, 
easy to integrate, easy to learn, and will never take longer to learn than it 
takes to use for the purpose it is intended to serve.


[1]: /documentation/core/usage/cli/
[2]: /documentation/core/usage/php/
[3]: /documentation/core/usage/javascript/
[4]: https://en.wikipedia.org/wiki/Request_for_Comments
[5]: https://www.php-fig.org/psr/
[6]: https://developer.mozilla.org/en-US/docs/MDN/Guidelines/Code_guidelines/JavaScript
[7]: https://en.wikipedia.org/wiki/Filesystem_Hierarchy_Standard