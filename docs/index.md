# Oroboros Core Documentation {#title}

- [Overview](#overview)
    - [Design Documentation](#overview-design)
    - [Development Documentation](#overview-development)
- [Installation](#installation)
- [Configuration](#configuration)

## Overview {#overview}

Welcome to the Oroboros Core documentation.

### Design Documentation {#overview-design}

Oroboros has a host of standard industry design utilities to speed up the process of designing a robust responsive layout.

### Development Documentation {#overview-development}

Oroboros provides utilities to create integrated applications that work seamlessly together across a variety of languages. It provides an api for [Bash][3], [PHP][4], and [Javascript][5], and may be integrated directly into other languages via the [bash api][3] or through either a [CLI controller][6] or a [Socket Controller][7] depending on the needs of your application and how you want resources to be accessed.

## Installation {#installation}

Full documentation on system installation is available [here][1].

## Configuration {#configuration}

Full documentation on system configuration is available [here][2].

[1]: /documentation/core/installation/
[2]: /documentation/core/configuration/
[3]: /documentation/core/usage/shell/
[4]: /documentation/core/usage/php/
[5]: /documentation/core/usage/javascript/
[6]: /documentation/core/usage/php/controller/cli/
[7]: /documentation/core/usage/php/controller/socket/