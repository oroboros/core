# Oroboros PHP Model Class Documentation {#title}

- [Overview](#overview)

## Overview {#overview}

Models perform the business logic tasks of the application. They do not make decisions about which operations need to be accomplished, but they do all of the logical operations to handle the selection of data or manipulation of it. Data persistence is handled by models, as well as the relation between one data source and another.

Models are most commonly referenced by controllers, but may also occasionally be utilized by libraries, components, or bootstrap classes on a case by case. There is no enforced restriction preventing direct interaction between a view and a model, however it is suggested not to do this without a controller relaying information between them for security considerations.