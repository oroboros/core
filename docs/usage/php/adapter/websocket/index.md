# Oroboros PHP WebSocket Adapter Api Documentation {#title}

- [Overview](#overview)

## Overview {#overview}

The WebSocket adapter works as an interface to allow applications to serve content over a web socket and manage peers using the standard controller format. Under the hood it runs on [Ratchet][1].

[1]: http://socketo.me/