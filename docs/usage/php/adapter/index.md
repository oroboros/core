# Oroboros PHP Adapter Class Documentation {#title}

- [Overview](#overview)

## Overview {#overview}

The adapter class is used to provide a uniform means to interact with external utilities available to the server. This allows other classes to take a uniform approach to interacting with resources outside the scope of PHP.