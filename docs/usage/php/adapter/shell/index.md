# Oroboros PHP Shell Adapter Api Documentation {#title}

- [Overview](#overview)

## Overview {#overview}

The Shell adapter manages interactions with the bash shell, granting controllers a simplified and secure way to manage calls to the underlying shell. Typically it is only appropriate to utilize this adapter from the scope of a [cli controller][1] or [socket controller][2] for security reasons.

[1]: /documentation/core/usage/php/controller/cli/
[2]: /documentation/core/usage/php/controller/socket/