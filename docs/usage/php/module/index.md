# Oroboros PHP Module Documentation {#title}

- [Overview](#overview)

## Overview {#overview}

Modules represent a portable, unified and encapsulated chunk of business logic that can be added to the core or an application built upon it to directly provide additional functionality. In contrast to [extensions][1], modules do provide business logic and opinion about how the app should function. Much like extensions, they may also provide additional resources for arbitrary use, but these are typically constrained to the realm of logic the module is intended to supply.

Modules are loaded during the [bootstrap][2] process and are distributed to controllers via dependency injection. Additionally, their namespace, template paths, and configurations are automatically loaded during bootstrap. This allows their classes to be autoloaded using the [Psr-4 Specification][3] directly, and their template paths to be automatically registered with the template engine.

[1]: /documentation/core/usage/php/extension/
[2]: /documentation/core/usage/php/bootstrap/
[3]: https://www.php-fig.org/psr/psr-4/