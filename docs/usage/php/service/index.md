# Oroboros PHP Service Documentation {#title}

- [Overview](#overview)

## Overview {#overview}

Services represent a uniform way to handle an external api or resource that is too complex to be handled directly by a single adapter. If all operations with the external resource can be effectively handled by a single class, it should be an adapter. If it requires a complex layer of classes and sub-classes, then a service is an appropriate answer.

Services are pluggable resources, much like [extensions][1] and [modules][2]. In contrast to adapters, they may be encapsulated and shared between apps as distinct packages.

[1]: /documentation/core/usage/php/extension/
[2]: /documentation/core/usage/php/module/ 