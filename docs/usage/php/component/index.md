# Oroboros PHP Component Documentation {#title}

- [Overview](#overview)

## Overview {#overview}

Components represent encapsulated chunks of logic that can be utilized by controllers to simplify complex and repetitive operations of sub-logic. They do not make decisions about direct execution, but they can make decisions about execution within their own immediate scope. Unlike a controller, they do not control the full scope of the request, only some specific portion of it.
