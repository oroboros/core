# Oroboros PHP HTML Component Documentation {#title}

- [Overview](#overview)

## Overview {#overview}

HTML Components provide uniform functionality to html pages encapsulated within a self-contained class that can register all of it's own dependency stylesheets, scripts, image assets, callback routes, and related data, and will generate correct markup for their use case automatically. Likewise, their frontend scripting will automatically hook into them in the browser if you are using the [Oroboros javascript component api][5], which is designed to work directly in conjunction with backend components seamlessly.

[5]: /documentation/core/usage/javascript/component/