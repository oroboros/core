# Oroboros PHP Trait Documentation {#title}

- [Overview](#overview)

## Overview {#overview}

Traits represent shared logic between classes where direct class inheritance is not desirable. If used intelligently, they significantly reduce code duplication and prevent having monolithic classes that chug in non-performant ways or are saddled with tons of irrelevant methods that do not apply to the current scope. If not used intelligently, they can introduce significant problems like the [diamond of death][1] bug.

Oroboros traits are typically designed to map to an interface that is commonly shared, so that the methods required to honor that interface have a single representation that does not require direct inheritance. There are a small number of utility traits provided as well to facilitate streamlining development of simple apps that do not require a large number of dependencies or external resources, or when the scope of the desired effect does not warrant a full blown mvc approach (like a simple one-page app, blog that only reads markdown to generate posts, simple task runner, etc).

[1]: https://en.wikipedia.org/wiki/Multiple_inheritance#The_diamond_problem