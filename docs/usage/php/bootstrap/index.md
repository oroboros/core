# Oroboros PHP Bootstrap Class Documentation {#title}

- [Overview](#overview)

## Overview {#overview}

A bootstrap class makes decisions about how a request to the application is resolved, which routes are relevant, and how to handle calls to controller methods. All controllers are called via a bootstrap class, and are handled by the bootstrap class when errors occur. A bootstrap class is tasked with loading the correct resources for the program to execute, selecting the correct controller for the request, and instantiating it correctly with dependencies that it requires to handle the actual request. Bootstrap classes also catch errors that are not handled by the controller and mitigate them. Command line and http requests use separate bootstrap objects, so the scope of controllers that the request route resolves to will be correctly configured to a uniform standard by the time the controller method is called.