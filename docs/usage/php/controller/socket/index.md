# Oroboros PHP Socket Controller Api Documentation {#title}

- [Overview](#overview)

## Overview {#overview}

Socket controllers are used to create interactive websocket applications. By default, these are served via [Ratchet][1], utilizing the [web-socket adapter][2].

Utilizing these controllers requires some additional configuration to enable.

[1]: http://socketo.me/
[2]: /documentation/core/usage/php/adapter/websocket/