# Oroboros PHP Rest Controller Api Documentation {#title}

- [Overview](#overview)

## Overview {#overview}

Rest Controllers are used to provide external Api's to distribute to remote clients. They function similarly to [HTTP controllers][1] with expanded functionality and more robust tools for authentication. For simple on-site authentication or public api's, it is suggested to utilize a standard [HTTP controller][1].

[1]: /documentation/core/usage/php/controller/http/