# Oroboros PHP HTTP Controller Api Documentation {#title}

- [Overview](#overview)

## Overview {#overview}

HTTP Controllers are used to provide endpoints for Ajax requests and external Api's to distribute to remote clients. Default HTTP controller implementations can authorize a request against a user session or provide open api access for public api's. For more robust access control, it is suggested to use a [Rest controller][1].

[1]: /documentation/core/usage/php/controller/rest/