# Oroboros PHP Page Controller Api Documentation {#title}

- [Overview](#overview)

## Overview {#overview}

Page controllers are tasked with serving typical HTML web pages. They have a host of utilities at their disposal to automatically manage access rights, required scripts, stylesheets, media, html components, and other typical externally linked resources required to serve a coherent web page.