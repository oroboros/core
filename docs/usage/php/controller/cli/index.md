# Oroboros PHP CLI Controller Api Documentation {#title}

- [Overview](#overview)

## Overview {#overview}

CLI Controllers are used to provide command line functionality in the application layer that is _too complex to be handled in [bash][1] easily_.

[1]: /documentation/core/usage/shell/