# Oroboros PHP Extension Documentation {#title}

- [Overview](#overview)

## Overview {#overview}

Extensions provide additional resources to the core system or app built upon it, without any opinion as to how those resources are used. In contrast to [modules][1], extensions do not inherently make business decisions about logic, instead simply presenting a toolbox of resources to freely implement as desired.

Extensions are loaded during the [bootstrap][2] process and are distributed to controllers via dependency injection. Additionally, their namespace, template paths, and configurations are automatically loaded during bootstrap. This allows their classes to be autoloaded using the [Psr-4 Specification][3] directly, and their template paths to be automatically registered with the template engine.

[1]: /documentation/core/usage/php/module/
[2]: /documentation/core/usage/php/bootstrap/
[3]: https://www.php-fig.org/psr/psr-4/