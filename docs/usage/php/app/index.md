# Oroboros PHP App Class Documentation {#title}

- [Overview](#overview)
- [Creating an App Class](#creating-an-app-class)

## Overview {#overview}

The App class represents the primary definition of an existing application. It determines how the app will bootstrap, what modules, extensions, components, and libraries are available to it, and what credentials and services it is allowed to use.

All sites or command line applications must have a valid app definition class. This class will act as the front controller for calls to the application.

## Creating an App Class {#creating-an-app-class}

The app class implements `\Oroboros\core\interfaces\application\ApplicationInterface`.  
This may be easily implemented by extending `\Oroboros\core\abstracts\application\AbstractApplication`.

Below is an example of a minimal App class definition:

```
<?php

namespace myvendor\mypackage;

class App extends \Oroboros\core\abstracts\application\AbstractApplication
{

    /**
     * Returns base namespace of the application.
     * This is used for Psr-4 registration.
     * @return string
     */
    public function getBaseNamespace(): string
    {
        return 'myvendor\\mypackage';
    }

    /**
     * Returns the root directory of the application.
     * @return string
     */
    public function getApplicationRoot(): string
    {
        return __DIR__ . DIRECTORY_SEPARATOR;
    }

    /**
     * Returns the source directory of the application.
     * This is used for Psr-4 autoload registration.
     * @return string
     */
    public function getApplicationSourceDirectory(): string
    {
        // Assuming your classes are stored in a folder named lib:
        return $this->getApplicationRoot() . 'lib' . DIRECTORY_SEPARATOR;
    }

}

```

