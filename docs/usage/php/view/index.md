# Oroboros PHP View Documentation {#title}

- [Overview](#overview)

## Overview {#overview}

Views represent the presentation layer that the user interacts with. Views do not typically make decisions about what is presented, rather they insure that the data given to them is rendered correctly for the given format they represent. Much like a television or computer monitor, they have no opinion about what they display, only how to translate what is given to them into a format that is useful to the user.