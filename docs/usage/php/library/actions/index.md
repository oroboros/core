# Oroboros Action Api Documentation Index {#title}

- [Overview](#overview)
- [Usage](#usage)
- [Architecture](#architecture)
    - [ActionAware Objects](#action-aware-object)
    - [ActionManager Objects](#action-manager-object)
    - [Action Objects](#action-object)
    - [ActionWatcher Objects](#action-watcher-object)
    - [ActionListener Objects](#action-listener-object)
    - [ActionBroker Objects](#action-broker-object)
    - [ActionStrategy Objects](#action-strategy-object)
    - [ActionQueueIndex Objects](#action-queue-index-object)
    - [ActionQueue Objects](#action-queue-object)
    - [ActionPool Objects](#action-pool-object)

## Overview {#overview}

The action api is how Oroboros handles event-driven actions within php. This
allows for action callbacks to be registered or declared similar to how javascript
handles events. The [event api][1] and the [filter api][2] both use this for
synchronous events, and the [job api][3] uses this for asynchronous events.

Unlike javascript and most other event systems implemented in php, the action api
defers control of execution back to the original object that declared the action
as much as is possible rather than assuming everything _must_ be ok. If the original
object does nothing with this, then it is not particularly much different than any
other event system, however the option to intervene at each subsequent step of a
chain of action callbacks exists, and the chain can be broken, errors marked as
recoverable, execution halted, or whatever other mitigation steps are needed.


## Usage {#usage}

The default action system provides only an architectural baseline for implementing
synchronous or asynchronous actions. It is implemented in a number of other constructs,
but it does not automatically run as a standallone construct.

## Extending Actions {#extending}

The action system is inherently designed to be extended, allowing the implementation
of a complex and far-reaching event type to be fairly straightforward to add.

A minimal implementation should have the following classes:
- A custom action type, extending `\Oroboros\core\abstracts\action\AbstractAction`
- A custom action manager type, extending `\Oroboros\core\abstracts\action\AbstractActionManager`

If you would also like objects controlling managers to relay your custom action
type status externally, support [brokers](#action-broker-object), [watchers](#action-watcher-object),
and [listeners](#action-listener-object), then your class controlling the manager must
implement interface `\Oroboros\core\interfaces\library\action\ActionAwareInterface`,
and it will have to manually handle the observer events emitted by the manager.

## Architecture {#architecture}

There are several related constructs involved in making the action system work,
below is a summary of each:

### ActionAware Objects {#action-aware-object}

An ActionAware object is any class that acts as an underlying owner of declared
actions. The action aware object interfaces with an ActionManager, which performs
the orchestration of action queues, actions, and callbacks, and relays information
back to the ActionAware object via [observer events][4]. From there, the ActionAware
object may opt to relay these messages to other objects interested or keep them private.

The ActionAware object may scope queues of actions, and all subsequent action
activity will only happen within the currently scoped queue unless the ActionAware
object explicitly declares that it wants to run an operation from another queue.
Scoped queues are stateful, but arbitrary calls that declare a queue explicitly
are not. An ActionAware object _must declare at least one queue_ to avoid errors.

ActionAware objects implement the interface `\Oroboros\core\interfaces\library\action\ActionAwareInterface`

There is no default implementation available.


### ActionManager Objects {#action-manager-object}

ActionManager objects implement the interface `\Oroboros\core\interfaces\library\action\ActionManagerInterface`

A default implementation may be achieved by extending `\Oroboros\core\abstracts\library\action\AbstractActionManager`.


### Action Objects {#action-object}

An action object is simply a status relay for a queue of zero or more callbacks
with unique identifiers, which can run them sequentially or individually, and relay
status, output, error state, results, and callback name back to the controlling object.

It is suggested to use an [ActionManager](#action-manager-object) rather than
try to manually manage this behavior, and then simply hook into the action manager
for updates.

Action objects implement the interface `\Oroboros\core\interfaces\library\action\ActionInterface`

A default implementation may be achieved by extending `\Oroboros\core\abstracts\library\action\AbstractAction`.


### ActionWatcher Objects {#action-watcher-object}

An action watcher listens for action registration or unregistration, and
automatically populates or removes a set of callbacks if it matches the
internal criteria for it's callback set. These are used by objects that
do not own the action, but want to hook into it to automatically insert
their set of callbacks at the appropriate time. This provides a simple
means for pluggables to interlace and keep any optional dependencies updated,
or to quickly distribute a set of common tasks across specific portions
of the runtime.

ActionWatcher objects implement the interface `\Oroboros\core\interfaces\library\action\ActionWatcherInterface`

A default implementation may be achieved by extending `\Oroboros\core\abstracts\library\action\AbstractActionWatcher`.


### ActionListener Objects {#action-listener-object}

An action listener listens for action and callbacks being run, completed, or in
error state, and relays the status back to the object owning the listener.
These are used to insure that task logic dependent on the completion or error
status of a specific event or callback can run at the correct timing.

ActionListener objects implement the interface `\Oroboros\core\interfaces\library\action\ActionListenerInterface`

A default implementation may be achieved by extending `\Oroboros\core\abstracts\library\action\AbstractActionListener`.


### ActionBroker Objects {#action-broker-object}

An ActionBroker is used by an [ActionAware](#action-aware-object) object to
pre-populate it's list of queues, corresponding actions, and corresponding
callbacks respectively. These can be used when large blocks of objects need
to declare the same queues/actions/callbacks.

ActionBroker objects implement the interface `\Oroboros\core\interfaces\library\action\ActionBrokerInterface`

A default implementation may be achieved by extending `\Oroboros\core\abstracts\library\action\AbstractActionBroker`.


### ActionStrategy Objects {#action-strategy-object}

An ActionStrategy is used by any [ActionAware](#action-aware-object) to determine
the correct manager for the type of action they are intending to run, and also
by [ActionManagers](#action-manager-object) internally to determine the corresponding
[Action](#action-object) object to create when given a registerAction call.

ActionStrategy objects implement the interface `\Oroboros\core\interfaces\library\action\ActionStrategyInterface`

A default implementation may be achieved by extending `\Oroboros\core\abstracts\library\action\AbstractActionStrategy`.


### ActionQueueIndex Objects {#action-queue-index-object}

An ActionQueueIndex is the master container of actions owned by the [ActionManager](#action-manager-object).
This will contain a set of keys corresponding to queues, with their value being an [ActionQueue](#action-queue-object)
that contains the actual [actions](#action-object) that run in any given queue. These objects are
only used internally by managers.

ActionQueueIndex objects implement the interface `\Oroboros\core\interfaces\library\action\ActionQueueIndexInterface`

A default implementation may be achieved by extending `\Oroboros\core\abstracts\library\action\AbstractActionQueueIndex`.


### ActionQueue Objects {#action-queue-object}

Action objects represent a queue of callbacks to run when they are executed.
They hook into an [ActionManager](#action-manager-object) and relay data about
their state back to the [ActionManager](#action-manager-object) as an execution progresses.

[Actions](#action-object) will relay start, complete, callback start, callback complete, error,
callback error, callback registration, and callback unregistration back to their
corresponding manager.

ActionQueue objects implement the interface `\Oroboros\core\interfaces\library\action\ActionQueueInterface`

A default implementation may be achieved by extending `\Oroboros\core\abstracts\library\action\AbstractActionQueue`.


### ActionPool Objects {#action-pool-object}

An action pool is a container of callbacks registered to unique keys, which will
run when an [Action](#action-object) is executed. Action pools are generated by an [Action](#action-object) object upon
instantiation, and all callbacks passed to them will exist within their pool object
as long as they are registered. These objects are only used internally by [Actions](#action-object),

ActionPool objects implement the interface `\Oroboros\core\interfaces\library\action\ActionPoolInterface`.

A default implementation may be achieved by extending `\Oroboros\core\abstracts\library\action\AbstractActionPool`.



[1]: 
[2]: 
[3]: 
[4]: 