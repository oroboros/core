# Oroboros PHP Library Class Documentation {#title}

- [Overview](#overview)

## Overview {#overview}

Libraries are unopinionated classes meant to handle all aspects of a specific task. They are the most commonly used class type. They typically do all of the work and provide none of the opinion about what work needs to be done.