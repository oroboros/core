# Oroboros Null Template Engine Api Documentation {#title}

- [Overview](#overview)

## Overview {#overview}

The null template engine provides nullifies all content passed to it, rendering nothing.
It exists for the purpose of negating output in cases where no output is needed without
the need to refactor other logic that expects to print into a template.

The null template satisfies the [template api][1] without rendering anything
during the [dispatch][2] step.

[1]: /documentation/core/usage/php/library/templates/
[2]: /documentation/core/usage/php/library/dispatch/