# Oroboros Twig Template Engine Api Documentation {#title}

- [Overview](#overview)
- [Usage](#usage)
    - [Additional Tests](#usage-additional-tests)
        - [Boolean](#usage-additional-tests-boolean)
        - [True](#usage-additional-tests-true)
        - [False](#usage-additional-tests-false)
        - [Object](#usage-additional-tests-object)
        - [Array](#usage-additional-tests-array)
        - [String](#usage-additional-tests-string)
        - [Component](#usage-additional-tests-component)
        - [Container](#usage-additional-tests-container)
    - [Additional Filters](#usage-additional-filters)
        - [Camel Case](#usage-additional-filters-camel-case)
        - [Brute Case](#usage-additional-filters-brute-case)
        - [Canonicalize](#usage-additional-filters-canonicalize)
        - [Excerpt](#usage-additional-filters-excerpt)
        - [Extract](#usage-additional-filters-extract)
        - [Indent](#usage-additional-filters-indent)
        - [Interpolate](#usage-additional-filters-interpolate)
        - [Leftpad](#usage-additional-filters-leftpad)
        - [Rightpad](#usage-additional-filters-rightpad)
        - [Split](#usage-additional-filters-split)
        - [Wrap](#usage-additional-wrap)

## Overview {#overview}

The Twig Template Engine is the default templating implementation for most content,
extending upon the commonly used templating library [Twig][1].

All core html content and most generated content use this template engine by default.


## Usage {#usage}

All default usage of the [template api][2] apply to the twig templage engine,
as well as all [usage criteria][3] for [Twig][1] itself. It is currently based
on Twig v3.


### Additional Tests {#usage-additional-tests}

In addition to the typical twig usage, Oroboros provides a number of helpful
tests to use within twig templates. These are provided for convenience to help
with general templating tasks as well as interoperability with the oroboros core
framework.


#### Boolean {#usage-additional-tests-boolean}

This test returns `true` if the given value is _strictly boolean `true` or `false`_.
This is useful in cases where a zero or one might be a valid value, but a strict
`true` or `false` should be handled separately. This can be accomplished with
vanilla twig logic, but it is fairly verbose to do so.

```

{# Example usage #}

{% if value is boolean %}
    ...
{% endif %}

```


#### True {#usage-additional-tests-true}

This test returns `true` if the given value is _exactly boolean: `true`_.
This is useful in cases where an exact match of `true` should be treated
differently than a _non-falsy value_. This can also be accomplished with
vanilla twig logic, but it is verbose and unintuitive to do so.


```

{# Example usage #}

{% if value is true %}
    ...
{% endif %}

```

#### False {#usage-additional-tests-false}

This test returns `true` if the given value is _exactly boolean: `false`_.
This is useful in cases where `false` should be handled differently than a
_falsy value (eg: 0, null, empty array, etc)_. This can also be accomplished
with vanilla twig logic, but it is verbose and unintuitive to do so.


```

{# Example usage #}

{% if value is false %}
    ...
{% endif %}

```


#### Object {#usage-additional-tests-object}

This will return `true` if the given value is _a php object instance_.
This is useful in cases where vanilla twig tests will give false positives
without a verbose set of criteria, and an _object with primitive characteristics
should be treated differently than a primitive value_. An example would be an
_object that has a `__toString` method but is not a string, or an object that is
iterable but is not an array_. This can also be accomplished with vanilla twig
logic in a verbose and unintuitive way, and tends to clutter up templates
significantly without it's own form of test.


```

{# Example usage #}

{% if value is object %}
    ...
{% endif %}

```


#### Array {#usage-additional-tests-array}

This test returns `true` _only if a standard array primitive is passed as it's
value_. It will return `false` on iterable objects, generators, and objects
implementing the `\ArrayAccess` interface _(underlying vanilla twig tests will
return `true` for all of these)_.

This is useful in cases where _a primitive array should be handled differently
than an iterable object_. This can also be achieved with vanilla twig tests,
but it is extremely verbose to do so.

```

{# Example usage #}

{% if value is array %}
    ...
{% endif %}

```


#### String {#usage-additional-tests-string}

This test returns `true` _only if a natural string primitive is passed_.
Unlike the vanilla twig tests, it does not return `true` for an object with
a `__toString` method, or an instance of `\Stringable`. This can also be
accomplished with vanilla twig tests, but it is verbose and unintuitive to do so.

```

{# Example usage #}

{% if value is string %}
    ...
{% endif %}

```


#### Component {#usage-additional-tests-component}

This returns `true` if the given value is an instance of
`\Oroboros\core\interfaces\library\component\ComponentInterface`. These are stringable,
but need to typically have the `raw` filter applied (they perform their own data
sanitization internally already and do not need to be double-escaped). This test
is provided due to the commonality of oroboros components turning up in twig templates.
There is no natural vanilla twig counterpart test.

```

{# Example usage #}

{% if value is component %}
    ...
{% endif %}

```


#### Container {#usage-additional-tests-container}

This returns `true` if the given vallue is an instance of
`\Oroboros\core\interfaces\library\container\ContainerInterface`.
These are iterable objects that are used extensively throughout
the entirety of Oroboros. Typically, these can be treated as any array.
However it is notable that they will raise an exception if a missing key is
requested from them rather than raise a warning as a standard array primitive
would, which can break templating if not anticipated. This method is provided
for cases where it is appropriate to check if the given value is a container.
There is no natural vanilla twig counterpart test.

```

{# Example usage #}

{% if value is container %}
    ...
{% endif %}

```


### Additional Filters {#usage-additional-filters}

In addition to the typical twig usage, Oroboros provides a number of helpful
filters to use within twig templates.


#### Camel Case {#usage-additional-filters-camel-case}

The camel case filter provides a twig template passthrough to the
[string formatter][4] method of the same name.

This filter will cast a given string to camelCase, removing spaces and
capitalizing each word except the first character. It works on space separated
strings as well as canonicalized strings.

To use the camel case filter within a twig template, simply use the following format:

```

<p>{{ "some string of text"|camelcase }}</p>

{# outputs "someStringOfText" #}

```


#### Brute Case {#usage-additional-filters-brute-case}

The brute case filter provides a twig template passthrough to the
[string formatter][4] method of the same name.

This filter will cast a given string to BruteCase, removing spaces and
capitalizing each word. It works on space separated strings as well as
canonicalized strings.

To use the brute case filter within a twig template, simply use the following format:

```

<p>{{ "some string of text"|brutecase }}</p>

{# outputs "SomeStringOfText" #}

```


#### Canonicalize {#usage-additional-filters-canonicalize}

The canonicalize filter provides a twig template passthrough to the
[string formatter][4] method of the same name.

This filter will cast a given string to hyphen-separated-format. It works on
space separated strings.

To use the canonicalize filter within a twig template, simply use the following format:

```

<p>{{ "some string of text"|canonicalize }}</p>

{# outputs "some-string-of-text" #}

```


#### Excerpt {#usage-additional-filters-excerpt}

The excerpt filter provides a twig template passthrough to the
[string formatter][4] method of the same name.

This filter will extract an excerpt of an arbitrarily specified number of characters.
It will not break words mid-word, but will intelligently look backward for the
last word-break in the event the given string would be cut mid-word.

If the given string has a length _less than the given excerpt cutoff point_, the
string will be output in it's entirety. If the string is _longer than the given
excerpt cutoff point_, the excerpt will be of a length _to the last space character
that exists prior to the cutoff point_, with three dots appended to it.

The length of the exerpt and value to append may be specified. The default length
is `300` characters, and the default suffix is `...`.

To use the exerpt filter within a twig template, simply use the following format:

```

<p>{{ "some very long string of text that is way longer than you want to print in the current context, but is not provided in a shorter format because the source doesn't care about the output and only returns the full value of the given string as is, leaving the details of the display value to the view which should be handling it anyways in a well structured system."|excerpt }}</p>

{# outputs "some very long string of text that is way longer than you want to print in the current context, but is not provided in a shorter format because the source doesn't care about the output and only returns the full value of the given string as is, leaving the details of the display value to the view..." #}

```

If you wish to specify a maximum length, you may do so by passing a number into
the first parameter of the filter (`string|filter(10)`). Using the same string
as above with thirty characters, you would get the following output:

```

<p>{{ "some very long string of text that is way longer than you want to print in the current context, but is not provided in a shorter format because the source doesn't care about the output and only returns the full value of the given string as is, leaving the details of the display value to the view which should be handling it anyways in a well structured system."|excerpt(30) }}</p>

{# outputs "some very long string of text..." #}

```

You may also specify a custom suffix for the exerpt by passing a second string
argument to the filter. If you wanted to append after thirty characters a link
to the full content as per the following `<a href="/some/link/">... (read more)</a>`,
then you would do so as follows:

```

<p>{{ "some very long string of text that is way longer than you want to print in the current context, but is not provided in a shorter format because the source doesn't care about the output and only returns the full value of the given string as is, leaving the details of the display value to the view which should be handling it anyways in a well structured system."|excerpt(30, '<a href="/some/link/">... (read more)</a>')|raw }}</p>

{# outputs "some very long string of text...<a href="/some/link/">... (read more)</a>" #}

```

_note that in the above example, the `raw` filter is also used to prevent twig
from automatically escaping the html markup characters._


#### Extract {#usage-additional-filters-extract}

The extract filter provides a twig template passthrough to the
[string formatter][4] method of the same name.

This filter will extract content from a string that exists between the given delimiters.
Delimiters must be specified when the filter is applied. If no delimeters are specified,
then null will be used, which will return the full string.

To use the extract filter within a twig template, simply use the following format:

```

<p>{{ "some [arbitrary string] of text with something valuable between square braces"|extract('[', ']') }}</p>

{# outputs "arbitrary string" #}

```


#### Indent {#usage-additional-filters-indent}

The indent filter provides a twig template passthrough to the
[string formatter][4] method of the same name.

This filter will indent each line in an entire block of content by the
specified number of spaces. If the string contains multiple lines, each
line will be indented an equivalent amount.

The default indent count is `1` character, and the default indent character is `" "`.
These may be arbitrarily specified. The specified character does not need to be
a single character. It can be any valid string.

To use the indent filter within a twig template, simply use the following format:

```

<p>{{ "some string"|indent }}</p>

{# outputs " some string" #}

<p>{{ "some string"|indent(4) }}</p>

{# outputs "    some string" #}

<p>{{ "some string"|indent(4, '-') }}</p>

{# outputs "----some string" #}

{# example with a line break in it #}
<p>{{ "some\nstring"|indent(4, '-') }}</p>

{#
outputs
"
----some
----string
"
#}

```


#### Interpolate {#usage-additional-filters-interpolate}

The interpolate filter provides a twig template passthrough to the
[string formatter][4] method of the same name.

This filter will perform interpolation of a given string.

To use the interpolate filter within a twig template, simply use the following format:

```

<p>{{ "this [inject] [inject2] [inject3]."|interpolate({'inject': 'is an example', 'inject2': 'of an interpolated', 'inject3': 'string'}) }}</p>

{# outputs "this is an example of an interpolated string." #}

```

You may specify custom delimiters as the 2nd and 3rd arguments as per the following:

```

<p>{{ "this [inject] {inject2} {inject3}."|interpolate({'inject': 'is an example', 'inject2': 'of an interpolated', 'inject3': 'string'}, '{', '}') }}</p>

{# outputs "this [inject] of an interpolated string." #}

```

You may also specify custom prefixes and suffixes to apply to interpolated values
as the 4th and 5th arguments, as per the following:

```

<p>{{ "this {inject} {inject2} {inject3}."|interpolate({'inject': 'is an example', 'inject2': 'of an interpolated', 'inject3': 'string'}, '{', '}', '[::', '::]') }}</p>

{# outputs "this [::is an example::] [::of an interpolated::] [::string::]." #}

```


#### Leftpad {#usage-additional-filters-leftpad}

The leftpad filter provides a twig template passthrough to the
[string formatter][4] method of the same name.

This filter will left-pad a given string by the specified number of spaces
(or characters).

This filter functions identically to the [indent](#usage-additional-filters-indent) filter,
except that it _does not pad each line, only the entire string itself_.

To use the left pad filter within a twig template, simply use the following format:

```

<p>{{ "some string"|leftpad }}</p>

{# outputs " some string" #}

```


#### Rightpad {#usage-additional-filters-rightpad}

The rightpad filter provides a twig template passthrough to the
[string formatter][4] method of the same name.

This filter will right-pad a given string by the specified number of spaces
(or characters).

This filter functions identically to the [leftpad](#usage-additional-filters-leftpad) filter,
except that it _pads the end of a string instead of the beginning_.

To use the right pad filter within a twig template, simply use the following format:

```

<p>{{ "some string"|rightpad }}</p>

{# outputs "some string " #}

```


#### Split {#usage-additional-filters-split}

The split filter provides a twig template passthrough to the
[string formatter][4] method of the same name.

This filter will break a string into an iterable array on each instance of
the break character. The default break character is a line break, but this
may be arbitrarily specified.

To use the split filter within a twig template, simply use the following format:

```
{% for value in "foo\nbar\nbaz"|split %}
    <p>{{ value }}</p>
{% endfor %}

{#
outputs
"
<p>foo</p>
<p>bar</p>
<p>baz</p>
"
#}

```

You may also specify a custom split character. In the below example a space is
used instead of a line break:

```
{% for value in "foo bar baz"|split(' ') %}
    <p>{{ value }}</p>
{% endfor %}

{#
outputs
"
<p>foo</p>
<p>bar</p>
<p>baz</p>
"
#}
```


#### Wrap {#usage-additional-wrap}

The wrap filter provides a twig template passthrough to the
[string formatter][4] method of the same name.

This filter will wrap a given string within the supplied opening and closing delimiters.
The default delimiters are square braces.

To use the wrap filter within a twig template, simply use the following format:

```

<p>{{ "some string"|wrap }}</p>

{# outputs "[some string]" #}

```

You may also specify arbitrary delimiters, as per the following:

```

{# Wrap in backticks #}
<p>{{ "some string"|wrap('`', '`') }}</p>

{# outputs "`some string`" #}

{# Wrap in angle braces #}
<p>{{ "some string"|wrap('<', '>') }}</p>

{# outputs "<some string>" #}

{# Wrap in squiggly braces #}
<p>{{ "some string"|wrap('{', '}') }}</p>

{# outputs "{some string}" #}

{# Wrap in parentheses #}
<p>{{ "some string"|wrap('(', ')') }}</p>

{# outputs "(some string)" #}

```


[1]: https://twig.symfony.com/
[2]: /documentation/core/usage/php/library/templates/
[3]: https://twig.symfony.com/doc/3.x/
[4]: /documentation/core/usage/php/library/formatter/string-formatter/