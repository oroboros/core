# Oroboros Raw Template Engine Api Documentation {#title}

- [Overview](#overview)

## Overview {#overview}

The raw template engine provides content on an as-is basis. It is the default
option for all cases where no other template engine is specified.

The raw template exists only to satisfy the [template api][1] without regard to
injected content.

[1]: /documentation/core/usage/php/library/templates/