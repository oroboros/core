# Oroboros Definer Api Documentation {#title}

- [Overview](#overview)
- [Usage](#usage)
    - [Extending Definers](#extending-definers)
        - [Optional Extension Methods](#extending-definers-optional-methods)
## Overview {#overview}

Definers are classes that handle dynamic compilation of a data set from a template.
They allow an incomplete dataset to import known required or optional values into anywhere
within their data-tree based on supplied values. These can be re-compiled with new values,
which allows a static config to work as a template data-set, or numerous individual data
objects with minor variations to be generated from a set of rotating values.

Definers are used extensively throughout the core logic in places where a database
is optional or a data structure is repetitive with a few minor but important variations.
This allows data trees to be recycled for various purposes with minimal business logic
required to reformat values that may be nested at any arbitrary depth level.

They can also be used to make static configurations environment aware, template
credentials and leave only the sensitive username/password details to a more
secure environment definition, or serve any other purpose where information
is split into both static and dynamic sources.


## Usage {#usage}

Definers implement the interface `\Oroboros\core\interfaces\library\definer\DefinerInterface`.

There is abstraction available for a definer class via `\Oroboros\core\abstracts\library\definer\AbstractDefiner`.

### Extending Definers {#extending-definers}

The abstract definer contains _one method_ that **must** be overridden:

```

/**
 * This method must return a container of the data values
 * used to establish how the definer operates.
 * 
 * may contain the following keys:
 * - required: an associative array of keys where the key is the required key
 *             and the format is the variable type that is acceptable
 * - optional: an associative array of keys where the key is the optional key
 *             and the format is the variable type that is acceptable
 * - defaults: an associative array of keys to use as default data. Values
 *             may be any primitive value, null, any iterable object, or a
 *             multidimensional array. Objects resolving to strings will be
 *             string cast prior to searching, and arrays will be
 *             containerized recursively.
 * @return \Oroboros\core\interfaces\library\container\ContainerInterface
 */
abstract protected function declareDataSet(): \Oroboros\core\interfaces\library\container\ContainerInterface;

```

This method must return a container of initial data.

Containers are instances of `\Oroboros\core\interfaces\library\container\ContainerInterface`,
Which may be obtained natively through nearly all Oroboros abstraction via `$this::containerize($some_array);`

A minimal implementation of a valid definer class should look like the following:

```

class MyCustomDefiner extends \Oroboros\core\abstracts\library\definer\AbstractDefiner
{
    /**
     * This method must return a container of the data values
     * used to establish how the definer operates.
     * 
     * may contain the following keys:
     * - required: an associative array of keys where the key is the required key
     *             and the format is the variable type that is acceptable
     * - optional: an associative array of keys where the key is the optional key
     *             and the format is the variable type that is acceptable
     * - provide:  an associative array of keys where the key is the provided identifier,
     *             and the value is the injectable value.
     * - defaults: an associative array of keys to use as default data. Values
     *             may be any primitive value, null, any iterable object, or a
     *             multidimensional array. Objects resolving to strings will be
     *             string cast prior to searching, and arrays will be
     *             containerized recursively.
     * @return \Oroboros\core\interfaces\library\container\ContainerInterface
     */
    protected function declareDataSet(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        return $this->containerize([
            'defaults' => []
        ]);
    }

}

```

note that _only the `defaults` key is required._

This method may _optionally_ provide a list of keys the object will `require`
to compile properly, a set of `optional` keys that _are not required but will
extend the output if present_, and a set of keys the data itself will `provide`,
which will be injected wherever they occur within the `defaults` without external
supply being required.

Additional keys may be present, but are not handled by the default abstraction.

An example utilizing all default definitions is provided below:

```

    protected function declareDataSet(): \Oroboros\core\interfaces\library\container\ContainerInterface
    {
        return $this->containerize([
            'require' => [
                'quux' => 'string',
            ],
            'optional' => [
                'baz' => 'string',
            ],
            'provide' => [
                'bazbar' => 'baz-bar'
            ],
            'defaults' => [
                'foo' => 'bar'
                'baz' => 'quux'
                'foobar' => '{::foobar::}',
                'bazbar' => '{::bazbar::}',
            ]
        ]);
    }

```

The above example will require supplied keys `quux` and `foobar`, indicates that
_if `baz` is provided it will be used, but it must be a string_, and that _`bazbar`
is provided and required, but it does not need to be dealt with by the class interacting
with the definer_.

#### Optional Extension Methods {#extending-definers-optional-methods}

Definers contain _three additional methods_ that can be overridden to declare
default required keys, default optional keys, and default provided keys respectively.

These will be _merged with whatever values the returned dataset provides,
with preference being given to the dataset over declarations_.

To provide additional required keys _not defined in the dataset_, override the following method:

```

/**
 * Overrride this method if you wish to declare a
 * predefined set of default values for required keys.
 * 
 * These may be overridden externally before the object compiles.
 * 
 * @return array
 */
protected function declareDefaultRequiredKeys(): array
{
    return [];
}

```

The format of the return values should be where the `key` is the required key,
and the `value` is a [resolveable][1] type, or array of [resolveable][1] types.


[1]: /documentation/core/usage/php/library/resolvers/