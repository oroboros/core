# Oroboros PHP Filesystem Library Class Documentation {#title}

- [Overview](#overview)

## Overview {#overview}

Filesystem libraries provide resources to manipulate the filesystem.