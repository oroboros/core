# Oroboros PHP FileWriter Library Class Documentation {#title}

- [Overview](#overview)
- [Usage](#usage)
    - [Options](#options)
    - [Checking the Writer Status](#status-checks)
    - [Extending the FileWriter](#extending)

## Overview {#overview}

The FileWriter library provides a straightforward way to generate files quickly and performantly. It uses streams to minimize the overhead of writing large files, and has a variety of options to manipulate how it operates with the filesystem.

## Usage {#usage}

A default file writer object may be obtained using the standard library factory:

```
$writer = $this->load('library', 'file\\FileWriter');
```

You may pass it a path as the third argument, which will be resolved to a destination folder and resulting file. This does not need to exist at the time of generation, _however if the specified folder does not exist yet there are a few caveats (see below)_.

```
$path = '/path/to/my/file.txt';
$writer = $this->load('library', 'file\\FileWriter', $path);
```

You may alternately specify the file and folder path after instantiation. The following example will have the exact same result as the above:


```
$directory = '/path/to/my';
$file = 'file.txt';
$writer = $this->load('library', 'file\\FileWriter');
$writer->setDirectory($directory);
$writer->setFile($file);
```

You may check if the file writer is currently scoped to a resolvable file with the following:

```
if ($writer->isWriteable()) {
   // perform write actions
}
```

The writer has two methods for writing contents to a file. It may either write a given string directly, or may alternately be given a stream, in the case where you want to send a high volume of data without consuming a large volume of memory.

To write a string directly:

```
$content = 'This is some text.';
$writer->write($content);
```

To feed a stream of data to the file:

```
$file_to_stream = '/path/to/example/my-gigantic-file.txt';
$stream = fopen($file_to_stream, 'r');
$writer->stream($stream);
```

There are no restrictions on what manner of stream may be given to the writer, so you may also use `fopen` to open a website url for instance, or any other valid implementation of `fopen`.

### Options {#options}

Options are either passed in as an associative array as the 4th argument in the factory load operation, or are set using the `setOption` method. Both of the below examples have the same effect.

Example using the factory:

```
$file = '/path/to/my/file.txt';
$args = [
    'write-mode' => 'a+',
    'generate-directories' => true
];
$writer = $this->load('library', 'file\\FileWriter', $file, $args);
```

Example using `setOption`:

```
$directory = '/path/to/my';
$file = 'file.txt';
$writer = $this->load('library', 'file\\FileWriter');
$writer->setOption('write-mode', 'a+');
$writer->setOption('generate-directories', true);
$writer->setDirectory($directory);
$writer->setFile($file);
```

**note:** _The current value of any option may be obtained using the method `getOption`_.


There are several options that the `FileWriter` may take:


- `lock-mode`
    - Defaults to the value of the predefined constant `LOCK_EX`, which tells the writer to obtain an exclusive lock during write operations.
    - Valid options are: `LOCK_EX` or `LOCK_EX | LOCK_NB` _(the predefined constants as written, or their numerical equivalents)_
- `write-mode`
    - The write mode to use for fopen. Default value is `wb+`. All valid php methods that write are valid. Read-only variants are not valid.
    - The full list of valid options is: `r+`, `rb+`, `w`, `w+`, `wb`, `wb+`, `a`, `a+`, `ab`, `ab+`, `x`, `x+`, `xb`, `xb+`, `c`, `c+`, `cb`, `cb+`
- `generate-directories`
    - Tells the writer whether to generate missing directories. This is set to `false` by default.
    - This is required if you want to give the writer a path that does not yet exist and have it generate any missing directories on the path to the file.
    - It should be noted that even with this enabled, there _must_ a valid folder in the supplied directory path, and the last _existing_ folder in the supplied path __must be writeable__, or you will still encounter an exception when attempting to write.
- `resolve-backpathing`
    - Whether to resolve paths that contain `..` to navigate up the directory tree.
    - This is set to `false` by default for security considerations, and must be explicitly enabled.
    - The default `false` value is intended to keep user-supplied paths from attempting to navigate backwards in the directory structure. It is easy enough to obtain an absolute filepath using `realpath` from the codebase, it is suggested not to enable this unless absolutely neccessary.
- `file-permission`
    - The permission to set on created files. Default value is `0664`.
    - Any octal value that resolves to a UNIX file permission is valid.
- `directory-permission`
    - The permission to set on created directories. Default value is `0775`.
    - Any octal value that resolves to a UNIX file permission is valid.

### Checking the Writer Status {#status-checks}

If you need the name of the current file, you may use `getFile`:

```
$file = '/path/to/my/file.txt';
$writer = $this->load('library', 'file\\FileWriter', $file);
echo $writer->getFile(); // returns "myfile.txt"
```

You may also get the currently scoped directory using `getDirectory`:

```
$file = '/path/to/my/file.txt';
$writer = $this->load('library', 'file\\FileWriter', $file);
echo $writer->getDirectory(); // returns "/path/to/my/"
```

And you may also get the fully qualified path currently scoped with `getFullPath`:

```
$file = '/path/to/my/file.txt';
$writer = $this->load('library', 'file\\FileWriter', $file);
echo $writer->getFullPath(); // returns "/path/to/my/file.txt"
```

If you need to check whether a write call is safe, you may use `isWriteable`.
If the write operation can proceed without generating an error, this will return `true`. If there are conflicting options, a missing file, a missing directory, or the given path can't be written to, then this will return `false`.

### Extending or Replacing the `FileWriter`. {#extending}

The file writer implements the interface `Oroboros\core\interfaces\library\file\FileWriterInterface`. Anything implementing this interface may be substituted systemwide for any dependency that requires a file writer.

In your application, you may override the default automatically by placing a class named `FileWriter` at the path location `lib/library/file/FileWriter.php` relative to your application root. It may likewise be overridden the same way in a [module][1] or [extension][2].

[1]: /documentation/core/usage/php/module/
[2]: /documentation/core/usage/php/extension/