# Oroboros PHP Api Index {#title}

- [Overview](#overview)
- [Class Types](#class-types)
- [Runtime](#runtime)
    - [CLI Runtime](#runtime-cli)
        - [Shell](#runtime-cli)
        - [Websocket Server](#runtime-cli-websockets)
        - [Job Queues](#runtime-cli-jobs)
    - [HTTP Runtime](#runtime-http)
        - [Webserver](#runtime-http-webserver)
        - [Ajax Server](#runtime-http-ajax)
        - [Rest Server](#runtime-http-rest)

## Overview {#overview}

@todo


## Class Types {#class-types}

@todo


## Runtime {#runtime}

@todo


### Cli Runtime {#runtime-cli}

@todo


#### Shell {#runtime-cli-shell}

@todo


#### Websocket Server {#runtime-cli-websockets}

@todo


#### Job Queues {#runtime-cli-jobs}

@todo


### HTTP Runtime {#runtime-http}

@todo


#### WebServer {#runtime-http-webserver}

@todo


#### Ajax Server {#runtime-http-ajax}

@todo



#### Rest Server {#runtime-http-rest}

@todo
