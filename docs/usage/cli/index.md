# Oroboros Bash Api Documentation {#title}

- [Overview](#overview)
- [Understanding the Command-Line Order of Operations](#cli-order-of-operations)
    - [Bash Api](#bash-api)
        - [Core Module](#bash-api-core-module)

## Overview {#overview}

Oroboros ships with a bash command line api that can be used for a variety of low level tasks. 
This allows the php environment to unify with other programming languages in a streamlined way, 
and allows for a streamlined approach to system administration.

## Understanding the Command-Line Order of Operations {#cli-order-of-operations}

When any command is made to the command line api, Oroboros will first compile any bash modules it contains 
and cross reference the passed parameters to them to see if any routes are registered at the bash level. 
If a match is found, it will execute the bash logic directly that coincides to the command passed.

If no match is found, it will bootstrap the php cli and pass the given arguments to the php cli 
api to be handled by the cli router.

### Bash Api {#bash-api}

#### Core Module {#bash-api-core-module}

The core module handles update and maintenance operations related to the oroboros core platform.
You may view the core help from the command line with the following command: `oroboros -c`