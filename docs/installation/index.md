# Oroboros Core Installation {#title}

- [Overview](#overview)
- [installation via web browser](#install-browser)
- [installation via command line](#install-cli)
- [building from source](#source-build)

## Overview {#overview}

## Installation via Browser {#install-browser}

Installation via web browser may be found [here][1].

This requires that you have built from source already, or downloaded a pre-built package.

## Installing via CLI {#install-cli}

Installation via command line may be found [here][2].

## Building from Source {#source-build}

Full documentation on how to build Oroboros from source may be found [here][3].

[1]: /documentation/modules/setup/web/
[2]: /documentation/modules/setup/cli/
[3]: /documentation/core/installation/build-from-source/