# Building Oroboros from Source {#title}

- [Overview](#overview)
- [Requirements](#requirements)
    - [Optional Requirements](#optional-requirements)
- [Obtaining Source Files](#obtaining-source-files)
    - [Preparing the Source](#preparing-source)
- [Checking your Local Environment for Compatibility](#checking-local-environment)
    - [Troubleshooting Environment Issues](#troubleshooting-environment-issues)

## Overview {#overview}


## Requirements {#requirements}

The following requirements are needed to run Oroboros:
- PHP 7.4+                                       _(currently untested on PHP 8)_
- Bash 4.2+                                      _(required for the command line interface)_
- Node.js ^14.16.1
- NPM ^7.11.2
- Apache 2.4+

### Optional Requirements {#optional-requirements}

Additionally, having any of the following soft requirements available will enable additional functionality:
- Git
- Beanstalk or Beanstalkd
- Redis
- Memcache or Memcached
- Supervisor or Supervisord
- MySQL, PostGres, Oracle, Sqlite, or any other database layer that can be connected to with PDO


## Obtaining Source Files {#obtaining-source-files}

Oroboros Core source files may be installed via Git with the following command:

```
git clone git@gitlab.com:oroboros/core.git
```


### Preparing the Source {#preparing-source}

After cloning the repository from git, you should run the following commands to prepare the build:

```
cd /path/to/oroboros                        # navigate to your source directory
chmod +x ./oroboros                         # make the oroboros shell script executable
ln -s ./oroboros /usr/local/bin/oroboros    # make the oroboros shell script usable systemwide
npm install                                 # install node.js dependencies
grunt init && grunt build                   # grunt will handle composer and setup operations
```

If your local environment follows the compatibility requirements above, this should be all you need to build from source.

You may also wish to create a vhost file for your installation, although this is optional.


## Checking your Local Environment for Compatibility {#checking-local-environment}

Oroboros contains utilities to automatically detect misconfigurations and incompatible local environments that may interfere with a build.
The following section will outline how to evaluate and correct these issues.


### Troubleshooting Environment Issues {#troubleshooting-environment-issues}

Oroboros ships with a built in environment evaulation utility written in bash. If there are issues with the build step, 
you can evaluate your local environment compatibility by running the following command from the project root:

```
./oroboros -c -v
```

_or alternately the longhand version, if you prefer better readability:_

```
./oroboros --core --verify
```

The above will check dependencies in your local environment to insure that there are no blocking errors for a build.
It will also evaluate a number of optional integration features and report whether your system is configured to allow 
them to be used by Oroboros.