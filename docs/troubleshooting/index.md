# Oroboros Core Troubleshooting {#title}

- [Overview](#overview)
- [Common Issues](#common-issues)
    - [Sessions Not Working](#troubleshooting-session-failure)

## Overview {#overview}

Welcome to the troubleshooting page for Oroboros Core.


## Common Issues {#common-issues}

Below is a list of common issues that you may encounter in various environments,
and suggested resolutions for them.


### Sessions Won't Persist Beteen CLI and HTTP {#troubleshooting-session-failure}

If your sessions are not detectable from websockets, it is likely that you have
an internal setting called `PrivateTmp` conflicting with shared temp file access.

The `PrivateTmp` value may be defined by your http server, by PHP-FPM, or both.
Both of these will need to be disabled if they exist for sessions to be detected
mutually between cli and http.

If this is the case, it prevents PHP from accurately determining what the real 
location of the temp directory is via `sys_get_tmp_dir()`, and there is no 
realistic way to determine the accurate temp path at runtime in any affected
environment.

You can determine if you have this issue by running the following command from cli:

```
sudo systemctl show php-fpm | grep PrivateTmp
```

If the above returns `PrivateTmp=yes` or `PrivateTmp=true` then this issue affects you.

To fix this, you may either [disable `PrivateTmp`](#troubleshooting-session-failure-fix-1) 
or alternately you may [specify an alternate session path](#troubleshooting-session-failure-fix-2)
in the event that you require this setting. You may also use a non-file-based
session storage mechanism, like memcache or redis if they are available.


#### Fix #1: Disable PrivateTmp for your Http Service {#troubleshooting-session-failure-fix-1}

You may resolve this by setting `PrivateTmp=no` for your webserver service
and restarting your webserver.

_The following may vary slightly from one distro to another.
This example is based on an apache service named `httpd`.
It may also be named `apache`, `apache2`, etc._

First check the existence of the default normally located at `/usr/lib/systemd/system/httpd.service`
with the following command:

```
sudo cat /usr/lib/systemd/system/httpd.service
```

This should display results similar to this:

```
[Unit]
Description=Apache Web Server
After=network.target remote-fs.target nss-lookup.target

[Service]
Type=simple
ExecStart=/usr/bin/httpd -k start -DFOREGROUND
ExecStop=/usr/bin/httpd -k graceful-stop
ExecReload=/usr/bin/httpd -k graceful
PrivateTmp=true
LimitNOFILE=infinity
KillMode=mixed

[Install]
WantedBy=multi-user.target
```

Next we will run the following to create an override:

```
sudo mkdir /etc/systemd/system/httpd.service.d
sudo echo "[Service]" >  /etc/systemd/system/httpd.service.d/nopt.conf
sudo echo "PrivateTmp=false" >> /etc/systemd/system/httpd.service.d/nopt.conf
```

If you are getting permission denied despite using sudo, use the following instead:

```
sudo vim /etc/systemd/system/httpd.service.d/nopt.conf
```

Press `i` to insert, and paste the following into the file with `ctrl+shift+v`

```
[Service]
PrivateTmp=false
```

Hit `ESC` and type `:wq` to save and exit. This will override _only the `PrivateTmp`
value and leave the rest of your config intact, so it does not collide with updates.

Refresh the daemon:

```
sudo systemctl daemon-reload
```

Enter the following command to insure that `PrivateTmp=false`

```
systemctl cat httpd.service
```

You should see something like this:

```
# /usr/lib/systemd/system/httpd.service
[Unit]
Description=Apache Web Server
After=network.target remote-fs.target nss-lookup.target

[Service]
Type=simple
ExecStart=/usr/bin/httpd -k start -DFOREGROUND
ExecStop=/usr/bin/httpd -k graceful-stop
ExecReload=/usr/bin/httpd -k graceful
PrivateTmp=true
LimitNOFILE=infinity
KillMode=mixed

[Install]
WantedBy=multi-user.target

# /etc/systemd/system/httpd.service.d/nopt.conf
[Service]
PrivateTmp=false
```

Finally restart your webserver:

```
sudo systemctl restart httpd
```

You are now using the standard tmp directory.

You will also need to make sure that oroboros has a tmp dir that is accessible
to both the web and cli. If you had mixed usage prior to this, your webserver
will likely snag on folder permissions trying to write to the twig cache.

So we will begin by flushing out any existing tmp dir
and re-creating it, as cli usage also would have generated one.

```
# delete the existing tmp dir if it exists
sudo rm -rf /tmp/oroboros

# create the base directory
mkdir /tmp/oroboros

# give it group write permission
chmod 770 /tmp/oroboros

# Change yourusername to whoever `whoami` resolves to,
# and httpusername to whatever your webserver uses
sudo chown yourusername:httpusername /tmp/oroboros
```

That should be all you need.