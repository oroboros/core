<?php
/* 
 * Bootstraps the library.
 */

// Require the composer autoloader, if present
if (is_readable(OROBOROS_CORE_BASE_DIRECTORY . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php'))
{
    require_once OROBOROS_CORE_BASE_DIRECTORY . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
}
// Load the constant definitions.
require_once OROBOROS_CORE_BASE_DIRECTORY
    . 'lib' . DIRECTORY_SEPARATOR
    . 'library' . DIRECTORY_SEPARATOR
    . 'environment' . DIRECTORY_SEPARATOR
    . 'EnvironmentFormatter.php';
require_once OROBOROS_CORE_BASE_DIRECTORY . 'setup' . DIRECTORY_SEPARATOR . 'definitions.php';

// Initialize package autoloading
// This is done after composer if it is present, so duplicate autoloading is not required (Composer is more heavily optimized).
// In the event that Composer is not loaded, the provided autoloader will act as a suitable replacement.
require_once OROBOROS_CORE_LIB . 'library' . DIRECTORY_SEPARATOR . 'autoload' . DIRECTORY_SEPARATOR . 'Autoloader.php';
(function(){
    $autoloader = new \Oroboros\core\library\autoload\Autoloader();
    $autoloader->register();
    $autoloader->addNamespace(OROBOROS_CORE_NAMESPACE, OROBOROS_CORE_LIB);
    // Load the baseline control class.
    require_once OROBOROS_CORE_LIB . 'Oroboros.php';
    \Oroboros\core\Oroboros::setAutoloader($autoloader);
    // Perform baseline initialization
    \Oroboros\core\Oroboros::initialize();
})();
