<?php
/*
 * Global constant definitions
 */

define('OROBOROS_CORE_CONFIG', OROBOROS_CORE_BASE_DIRECTORY . 'config' . DIRECTORY_SEPARATOR);
define('OROBOROS_CORE_SETUP', OROBOROS_CORE_BASE_DIRECTORY . 'setup' . DIRECTORY_SEPARATOR);
define('OROBOROS_CORE_LIB', OROBOROS_CORE_BASE_DIRECTORY . 'lib' . DIRECTORY_SEPARATOR);
define('OROBOROS_CORE_TMP', (function () {
        // PHP still returns a bad string if the system returns a private temp directory (eg default setting on CentOS),
        // in accordance with the system settings.
        // return false if this occurs so writes to an invalid directory do not occur.
        $sys_tmp = sys_get_temp_dir();
        if (is_writable($sys_tmp)) {
            if (!file_exists($sys_tmp . DIRECTORY_SEPARATOR . 'oroboros')) {
                $res = mkdir(
                    $sys_tmp . DIRECTORY_SEPARATOR . 'oroboros',
                    0770,
                    true,
                );
                if ($res) {
                    $sys_tmp = $sys_tmp . DIRECTORY_SEPARATOR . 'oroboros';
                }
            } else {
                $sys_tmp = $sys_tmp . DIRECTORY_SEPARATOR . 'oroboros';
            }
        }
        if (!$sys_tmp || $sys_tmp === DIRECTORY_SEPARATOR . 'tmp') {
            return false;
        }
        return rtrim($sys_tmp, '\\/') . DIRECTORY_SEPARATOR;
    })());
define('OROBOROS_CORE_VENDOR', OROBOROS_CORE_BASE_DIRECTORY . 'vendor' . DIRECTORY_SEPARATOR);
define('OROBOROS_CORE_TEMPLATES', OROBOROS_CORE_BASE_DIRECTORY . 'tpl' . DIRECTORY_SEPARATOR);
define('OROBOROS_CORE_NAMESPACE', 'Oroboros\\core');
define('OROBOROS_CORE_CLI', (php_sapi_name() === 'cli'));
// Define the page host (this script has no opinion about what its host is. Routing is out of scope for this operation)
define('OROBOROS_CORE_SSL', OROBOROS_CORE_CLI ? false : (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off')); // Windows just can't seem to grasp how to follow typical web standards :/
define('OROBOROS_CORE_HOST', OROBOROS_CORE_CLI ? 'cli' : 'http' . ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') // Windows just can't seem to grasp how to follow typical web standards :/
            ? 's' : NULL) . '://' . $_SERVER['HTTP_HOST']); // There is no http host if this is a command line call.
// Define the page uri (this script has no opinion about what it's uri is. Routing is out of scope for this operation)
define('OROBOROS_CORE_URI', OROBOROS_CORE_CLI ? false : OROBOROS_CORE_HOST . $_SERVER['REQUEST_URI']); // There is no request uri if this is a command line call.
// Anonymous function that handles setting dynamic constants from environment variables
(function () {
    $env_keys = json_decode(file_get_contents(OROBOROS_CORE_CONFIG . 'env.json'), 1);
    $env_parser = new \Symfony\Component\Dotenv\Dotenv();
    // Always load defaults
    $env_parser->load(OROBOROS_CORE_BASE_DIRECTORY . '.env.defaults');
    if (is_readable(OROBOROS_CORE_BASE_DIRECTORY . '.env')) {
        // If host-specific variables exist, also load those,
        // which will replace any overridden defaults
        $env_parser->overload(OROBOROS_CORE_BASE_DIRECTORY . '.env');
    }
    // Clear the parser, it is not needed again.
    unset($env_parser);
    $env = \Oroboros\core\library\environment\EnvironmentFormatter::format(array_merge(getenv(), $_ENV));
    foreach ($env_keys as $key => $details) {
        $val = getenv($details['key']);
        if (!array_key_exists($details['key'], $env)) {
            // Undefined environment variable, use the configured default, or null if not defined.
            $val = array_key_exists('default', $details) ? $details['default'] : null;
        } else {
            $val = $env[$details['key']];
        }
        if (!is_null($details['valid']) && is_string($details['valid'])) {
            // Value must equal the provided string
            if ($val !== $details['valid']) {
                throw new \OutOfRangeException('Invalid environment configuration. '
                        . 'Environment key [%1$s] evaluated by .env variable [%2$s] is incorrect. '
                        . 'Expected [%3$s], received [%4$s]. '
                        . 'Could not resolve required constant [%5$s].', $key, $details['key'], $details['valid'], ( ( is_string($val) ) ? '"' . $val . '"' : ( is_int($val) ? (string) $val : ( is_null($val) ? 'NULL' : ( ($val === true) ? 'TRUE' : 'FALSE' ) ) )), $details['constant']);
            }
        } elseif (!is_null($details['valid']) && is_array($details['valid'])) {
            // Value must equal the provided string
            if (!in_array($val, $details['valid'])) {
                throw new \OutOfRangeException('Invalid environment configuration. '
                        . 'Environment key [%1$s] evaluated by .env variable [%2$s] is incorrect. '
                        . 'Expected one of [%3$s], received [%4$s]. '
                        . 'Could not resolve required constant [%5$s].', $key, $details['key'], implode(', ', array_map(function ($expected) {
                                return ( (is_string($expected)) ? '"' . $expected . '"' : ( is_int($expected) ? (string) $expected : (is_null($expected) ? 'NULL' : ( ($expected === true) ? 'TRUE' : 'FALSE' ) ) ) );
                            }, $details['valid'])), ( (is_string($val)) ? '"' . $val . '"' : ( is_int($val) ? (string) $val : (is_null($val) ? 'NULL' : ( ($val === true) ? 'TRUE' : 'FALSE' ) ) )), $details['constant']);
            }
        }
        $constant = $details['constant'];
        if (!defined($constant)) {
            // Do not re-define pre-existing constants
            define($constant, $val);
        }
    }
    // Clear the expected keys, they are not needed again.
    unset($env_keys);
})();
// Only dynamically define the log directory if no environment setting exists for it
if (!defined('OROBOROS_CORE_LOGS')) {
    define('OROBOROS_CORE_LOGS', (function () {
            $path = false;
            if (ini_get('error_log') === '') {
                if (is_writable('/var/log')) {
                    $path = '/var/log/';
                    if (!file_exists($path . DIRECTORY_SEPARATOR . 'oroboros')) {
                        mkdir($path . DIRECTORY_SEPARATOR . 'oroboros', 0770);
                        $path = $path . DIRECTORY_SEPARATOR . 'oroboros' . DIRECTORY_SEPARATOR;
                    }
                }
                if ($path === false) {
                    // Log directory not set, use the provided log directory
                    $path = OROBOROS_CORE_BASE_DIRECTORY . 'log' . DIRECTORY_SEPARATOR;
                }
            } else {
                // Log directory is set, use the log location
                $path = rtrim(pathinfo(ini_get('error_log'), PATHINFO_DIRNAME), '\\/') . DIRECTORY_SEPARATOR;
                if (!file_exists($path . 'oroboros') && is_writable(pathinfo($path, PATHINFO_DIRNAME))) {
                    mkdir($path . 'oroboros');
                } else {
                    return false;
                }
                $path = $path . 'oroboros' . DIRECTORY_SEPARATOR;
            }
            if (!is_writable($path)) {
                return false;
            }
            return $path;
        })());
}
// Only dynamically define the cache if no environment setting exists for it
if (!defined('OROBOROS_CORE_CACHE')) {
    define('OROBOROS_CORE_CACHE', (function () {
            $tmpdir = ((OROBOROS_CORE_TMP) ? OROBOROS_CORE_TMP : OROBOROS_CORE_BASE_DIRECTORY) . 'cache' . DIRECTORY_SEPARATOR;
            if (!is_writable($tmpdir)) {
                if (!file_exists($tmpdir)) {
                    mkdir($tmpdir);
                    chmod($tmpdir, 0770);
                } else {
                    return false;
                }
            }
            return $tmpdir;
        })());
}