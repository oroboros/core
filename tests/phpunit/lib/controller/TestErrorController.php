<?php
namespace Oroboros\coreTest\controller;

/**
 * Unit Test Http Error Controller
 * Provides error handling for html requests
 *
 * @author Brian Dayhoff
 */
final class TestErrorController extends \Oroboros\core\abstracts\controller\AbstractCliErrorController
{

    const CLASS_SCOPE = 'cli';

    public function index()
    {
        $this->print_message('this is the test error controller index');
        return $this;
    }
}
