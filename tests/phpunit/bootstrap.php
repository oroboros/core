<?php
/* 
 * Bootstrap file for PHPUnit testing
 */
ini_set('memory_limit', '1G'); // Code coverage is a thirsty girl if you use XDebug
require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'setup.php';
define('OROBOROS_CORE_PHPUNIT_DIRECTORY', __DIR__ . DIRECTORY_SEPARATOR);
define('OROBOROS_CORE_PHPUNIT_NAMESPACE', 'Oroboros\\coreTest');
define('OROBOROS_CORE_PHPUNIT_LIB_DIRECTORY', OROBOROS_CORE_PHPUNIT_DIRECTORY . 'lib' . DIRECTORY_SEPARATOR);
define('OROBOROS_CORE_PHPUNIT_ASSET_DIRECTORY', OROBOROS_CORE_PHPUNIT_DIRECTORY . 'assets' . DIRECTORY_SEPARATOR);
define('OROBOROS_CORE_PHPUNIT_TEMPLATE_DIRECTORY', OROBOROS_CORE_PHPUNIT_ASSET_DIRECTORY . 'tpl' . DIRECTORY_SEPARATOR);
define('OROBOROS_CORE_PHPUNIT_ETC_DIRECTORY', OROBOROS_CORE_PHPUNIT_DIRECTORY . 'etc' . DIRECTORY_SEPARATOR);
define('OROBOROS_CORE_PHPUNIT_CONFIG_DIRECTORY', OROBOROS_CORE_PHPUNIT_DIRECTORY . 'config' . DIRECTORY_SEPARATOR);
require_once OROBOROS_CORE_PHPUNIT_LIB_DIRECTORY . 'application' . DIRECTORY_SEPARATOR . 'TestRunnerApplication.php';
$autoloader = new \Oroboros\core\library\autoload\Autoloader();
$autoloader->register();
$autoloader->addNamespace('Oroboros\\coreTest', __DIR__ . DIRECTORY_SEPARATOR . 'tests' . DIRECTORY_SEPARATOR, true);
\Oroboros\core\Oroboros::bootstrap(new \Oroboros\coreTest\application\TestRunnerApplication());