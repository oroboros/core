#!/bin/bash

# The exit code will be set to a non-zero value on any error, which will trigger the cleanup script to exit with an error code.
declare -i _exit_code;
_exit_code=0;

# The baseline command is set to the general help index, so if no commands are passed, it will disclose what commands are available.
_command="-h";

# Set the parsed argument container
# Arguments passed in such as --host="localhost"
# will be added to this array as $_args['host'] = localhost
# These may then be referenced with `get_arg` or checked with `check_arg`
declare -A _args=();

# Set the parsed application argument container
# Arguments passed in such as --host="localhost"
# will be added to this array as $_args['host'] = localhost
# These may then be referenced with `get_app_arg` or checked with `check_app_arg`
# They may be set with `set_app_arg $key $value`
declare -A _app_args=();

# Set the baseline var container
# These contain global system arguments parsed into the local scope
declare -A _var=();

# Set the baseline var container for application variables
# These contain arguments to be sent to the application.
declare -A _app_var=();

# Set the baseline command container
# Contains the baseline command(s)
declare -A _cmd=();

# Set the baseline command container for application commands.
# Contains commands determined by the application layer.
declare -A _app_cmd=();

# Set the baseline parameter container
declare -A _params=();

# Set the baseline flag container
# Contains flags passed in as subcommands to influence the
# specific operation taken by the mapped command method.
# These must be handled in the mapped command itself.
declare -a _flags=();

# Set the declared flag container
# Contains flags declared as valid. Passed flags will be checked
# against these to determine if they are stored.
declare -a _flagset=();

# Set the declared flag container for application flags
# Contains all valid bash flags and all unknown flags.
# The application must handle its own flags and discard
# any unknown to it.
declare -a _app_flagset=();

# The application path
# constructed into a uri structure for app routing
declare -a _app_path=();

# Set the lockfile array
# Contains a list of active locks, which determine if a command can be
# concurrently executed, or prevent it if it must only be run singularly.
declare -A _locks=();


_flags+=('--debug');
_flags+=('--quiet');
_flags+=('--silent');
_flags+=('--interactive');

#for i in ${_flags}; do
#    echo "Declared flag: [${_c_yellow}${i}${_c_cyan}]";
#done