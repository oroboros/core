#!/bin/bash

# Oroboros Bootstrap File
# This file includes the baseline source files and sets up the current execution for operation.
# Bootstraps the bash library.
# Insures that all expected functions, variables, and declarations are present
# Parses arguments and determines how to route the command passed
# Insures that clean teardown occurs after setup and execution of the requested command in the correct order.

# Loads modules of the bash library
source $_lib"/setup.sh";

# Include the argument parser and route the command
source $_lib"/args.sh";

if exists $_command in _cmd; then
  # Execute the given command.
  Main__main "${_cmd[$_command]}" "$_params" "${_flagset}";
else
  # No known method.
  Main__main "${_command}" "$_params" "${_flagset}";
  php index.php ${_raw_args};
fi
# Run the main program
