# Internal functions for the bash library that provide functionality for declaring errors and error handling.

# Terminates execution
# Will exit with an error code if errors occurred during operation.
# If no errors occurred, will exit clean (with status code 0).
function terminate(){
  if [ $_exit_code == 0 ]; then
    # Exit clean
#    success "Operation successful.";
    exit 0;
  else
    # Exit with error
#    printf $_c_red"Operation completed with errors.\n"$_c_clear;
    exit ${_exit_code};
  fi
}


# Renders a fatal error message to stderr and terminates execution with an error status code
function fatal {
  local _message="[::FATAL::] ${1}";
  if ! has_flag "--silent"; then
    (>&2 printf "${_c_red}${_message}${_c_clear}\n");
  fi
  log_message "$(remove_colors "${_message}")";
  _exit_code=1;
  terminate;
}

# Renders an error message to stderr
function error {
  local _message="[::ERROR::] ${1}";
  if ! has_flag "--silent"; then
    (>&2 printf "${_c_orange}${_message}${_c_clear}\n");
  fi
  log_message "$(remove_colors "${_message}")";
  _exit_code=1;
}

# Renders a warning message to stderr
function warning {
  local _message="[::WARNING::] ${1}";
  if ! has_flag "--silent"; then
    (>&2 printf "${_c_yellow}${_message}${_c_clear}\n");
  fi
  log_message "$(remove_colors "${_message}")";
#  _exit_code=1;
}

# Renders a successful operation message to stdout
function success {
  local _message="${1}";
  if ! has_flag "--quiet" && ! has_flag "--silent"; then
    printf "${_c_green}${_message}${_c_clear}\n";
  fi
  log_message "$(remove_colors "${_message}")";
}

# Renders an info message to stdout
function info {
  local _message="[::INFO::] ${1}";
  if ! has_flag "--quiet" && ! has_flag "--silent"; then
    printf "${_c_cyan}${_message}${_c_clear}\n";
  fi
  log_message "$(remove_colors "${_message}")";
}

# Renders a debug message to stdout
function debug {
  local _message="[::DEBUG::] ${1}";
  if has_flag "--debug" && ! has_flag "--quiet" && ! has_flag "--silent"; then
    printf "${_c_gray}${_message}${_c_clear}\n";
    # Debug messages still only log if the debug flag was passed
    log_message "$(remove_colors "${_message}")";
  fi
}
