#!/bin/bash

# Internal functions for the bash library that provide functionality for logging.

# Logs an entry
log_message() {
  local _message="${1}";
  local _file="${2}";
  if [ -z "${_message}" ]; then
    # Nothing to do
    return 1;
  fi
  if [ -z "${_file}" ] && check_arg 'logfile'; then
    # Use the specified log file
    _file="$(get_arg 'logfile')";
  elif [ -z "${_file}" ]; then
    # Use the default log file
    _file="${_log}/shell.log";
  fi
  echo -e "$(remove_colors "${_message}")" >> "${_file}";
  return 0;
}
