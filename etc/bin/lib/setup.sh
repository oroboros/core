#!/bin/bash

# Oroboros Core Bootstrap File
# This file includes any modules loaded into the bash library, and properly sets them up to recieve commands.
# Include the bash libraries


# Include the baseline declarations
source $_lib"/declarations.sh";

# Import and localize environment parameters
source $_lib"/vars.sh";

# Include the syntax highlighter functionality
source $_lib"/colors.sh";

# Include the internal bash library functions
source $_lib"/functions.sh";

while IFS= read -d $'\0' -r working_directory ; do
  _old_ifs=$IFS;
  while IFS= read -d $'\0' -r file; do
    source "${file}";
  done < <(find ${working_directory} -mindepth 1 -type f -print0)
  IFS=$_old_ifs;
done < <(find ${_lib} -mindepth 1 -type d -print0)

# Include the module libraries, config, and flag declarations
while IFS= read -d $'\0' -r file ; do 
  if [ -f "$file/cfg.sh" ]; then
      source "$file/cfg.sh"
      
      # If there is a localized override config file, also include that.
      # These are not git versioned, and allow users to map to their own environment
      # without disrupting updates via the git repository.
      if [ -f "$file/cfg.override.sh" ]; then
        source "$file/cfg.override.sh";
      fi
  fi
  if [ -f "$file/index.sh" ]; then
      source "$file/index.sh"
      
      # If there is a localized override index file, also include that.
      # These are not git versioned, and allow users to map to their own parameter indexes
      # for customized usage without disrupting updates via the git repository.
      # This may be used to resolve colliding flags in a local environment as needed.
      if [ -f "$file/index.override.sh" ]; then
        source "$file/index.override.sh";
      fi
  fi
  if [ -f "$file/functions.sh" ]; then
    source "$file/functions.sh"
  fi
done < <(find ${_modules} -mindepth 1 -maxdepth 1 -type d -print0)

# Register the exit handlers
trap Main__interruptHandler INT
trap Main__terminationHandler TERM
trap Main__exitHandler EXIT

function Main__main() {
  local _passed_command="$1";
  local _passed_params="$2";
  local _passed_flags="$3";
  if ! [[ "${_passed_command:0:1}" = "-" ]] && [[ "$(type -t "${_passed_command}")" = "function" ]]; then
    # Valid function call.
    $_passed_command $params;
    _exit_code=$?;
    # Run cleanup
    source $_lib"/teardown.sh";
    return $_exit_code;
  elif exists $_passed_command in _cmd; then
    # Execute the given command.
    ${_cmd[$_passed_command]} $_passed_params;
    _exit_code=$?;
    # Run cleanup
    source $_lib"/teardown.sh";
    return $_exit_code;
  else
    # Any other command goes to the application.
    if [ ! -f "${_dir}/index.php" ]; then
      if [ "${_api_scope}" == 'global' ]; then
        # This is a global scope request, forward to the oroboros root installation.
        core_exec;
        _exit_code=$?;
        exit $_exit_code;
      else
      error "Current directory [${_c_yellow}${_dir}${_c_orange}] is not an application directory.";
      _exit_code=126;
      fi
    else
      app_exec;
      _exit_code=$?;
      exit $_exit_code;
    fi
    # Run cleanup
    source $_lib"/teardown.sh";
    return $_exit_code;
  fi
}

# catch signals and exit
trap exit INT TERM EXIT
