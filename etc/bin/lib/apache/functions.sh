# General purpose utilities for handling apache interaction

apache_verify() {
  :
}

apache_version() {
  local _return=0;
  local _apache_detected_binary="$(apache_binary)";
  _return=$?;
  if [ $_return -eq 0 ]; then
    echo "$(${_apache_detected_binary} -v | grep Apache | cut -d'/' -f2 | cut -d' ' -f1)";
  fi
  return $_return;
}

apache_binary() {
  local _apache_detected_binary="$(which httpd)";
  if [ "${_apache_detected_binary}" == "" ]; then
    _apache_detected_binary="$(which apache2)";
  fi
  if [ "${_apache_detected_binary}" == "" ]; then
    return 1;
  fi
  echo "${_apache_detected_binary}";
  return 0;
}

apache_binary_type() {
  local _binary="$(apache_binary)";
  if [ ! $? -eq 0 ]; then
    return 1;
  fi
  echo "$(basename "${_binary}")";
  return 0;
}

apache_config() {
  :
}
