# General purpose utilities for handling osd interaction


os_verify() {
  :
}

os_distro() {
  local _return=0;
  local _os_detected_binary="";
  local _os_type="${_var['os_type']}";
  case "${_os_type}" in
    'linux')
      _os_detected_binary="$(_os_linux_get_distro)";
      ;;
    'osx')
      _os_detected_binary='osx';
      ;;
    'windows')
      _os_detected_binary='windows';
      ;;
    'unknown')
      error "Operating system could not be determined.";
      return 1;
      ;;
  esac;
  echo "${_os_detected_binary}";
  return $_return;
}

os_type() {
  echo "$(_os_get_distro_type)";
}

os_binary() {
  local _os_detected_binary="$(_os_detect_system_control)";
  if [ "${_os_detected_binary}" == "" ]; then
    return 1;
  fi
  echo "${_os_detected_binary}";
  return 0;
}

os_binary_ctrl() {
  local _designation=1;
  local _ctrl="";
  local _os="$(os_distro)";
  case "${_os}" in
    'antergos')
      # Antergos should be using systemctl
      _ctrl="$(_os_systemctl_binary)";
      _designation=$?;
      ;;
    'archlinux')
      # Arch linux should be using systemctl
      _ctrl="$(_os_systemctl_binary)";
      _designation=$?;
      ;;
    '"Manjaro Linux"')
      # Manjaro Linux should be using systemctl
      _ctrl="$(_os_systemctl_binary)";
      _designation=$?;
      ;;
    'linux')
        :
      ;;
    'osx')
      # OSX should be using homebrew
      _ctrl="$(_os_check_brew)";
      _designation=$?;
      ;;
    'windows')
        :
      ;;
    'unknown')
      # Cannot determine binary controller on an unknown operating system.
      _designation=1;
      ;;
  esac;
  if [ ! $_designation -eq 0 ]; then
    error "Could not determine binary controller.";
    return 1;
  fi
  echo "${_ctrl}";
  return $_designation;
}

# Locates the absolute path to a binary on the operating system.
os_locate_binary() {
  local _expected="${1}";
  local _location="";
  if [ -z "${_expected}" ]; then
    error "No binary provided.";
    return 1;
  fi
  _location="$(which "${_expected}")";
  if [ ! $? -eq 0 ]; then
    error "Could not find a valid instance of [${_c_yellow}${_expected}${_c_orange}].";
  fi
  echo "${_location}";
  return 0;
}

os_binary_ctrl_type() {
  local _designation=1;
  local _ctrl="";
  local _os="$(os_distro)";
  case "${_os}" in
    'antergos')
      # Antergos should be using systemctl
      _ctrl="$(_os_systemctl_binary)";
       echo 'systemctl';
      _designation=$?;
      ;;
    'archlinux')
      # Arch linux should be using systemctl
      _ctrl="$(_os_systemctl_binary)";
      if [ $? -eq 0 ]; then
       echo 'systemctl';
       return 0; 
      fi
      ;;
    'linux')
        :
      ;;
    'osx')
      # OSX should be using homebrew
      _ctrl="$(_os_check_brew)";
      if [ $? -eq 0 ]; then
       echo 'brew';
       return 0; 
      fi
      ;;
    'windows')
        :
      ;;
    'unknown')
      # Cannot determine binary controller on an unknown operating system.
      echo "unknown";
      return 1;
      ;;
  esac;
  if [ ! $_designation -eq 0 ]; then
    error "Could not determine binary controller type.";
    return 1;
  fi
  return $_designation;
}

_os_detect_system_control() {
  local _os_type="${_var['os_type']}";
  case "${_os_type}" in
    'linux')
      _os_detected_binary="$(_os_linux_get_distro)";
      ;;
    'osx')
      _os_detected_binary='osx';
      ;;
    'windows')
      _os_detected_binary='windows';
      ;;
    'unknown')
      error "Operating system could not be determined.";
      return 1;
      ;;
  esac;
  echo "${_os_detected_binary}";
  return 0;
}

_os_check_systemctl() {
  local _sysctl="$(which systemctl)";
  if [ ! $? -eq 0 ]; then
    return 1;
  fi
  echo "${_sysctl}";
  return 0;
}

_os_systemctl_binary() {
  local _sysctl="$(which systemctl)";
  if [ ! $? -eq 0 ]; then
    return 1;
  fi
  echo "${_sysctl}";
  return 0;
}

_os_check_brew() {
  local _sysctl="$(which brew)";
  if [ ! $? -eq 0 ]; then
    return 1;
  fi
  echo "${_sysctl}";
  return 0;
}

_os_brew_binary() {
  local _brew="$(which brew)";
  if [ ! $? -eq 0 ]; then
    return 1;
  fi
  echo "${_brew}";
  return 0;
}

_os_is_linux() {
  if 
  [ "$(_os_get_distro_type)" == "linux" ]; then
    return 0;
  fi
  return 1;
}

_os_is_osx() {
  if [ "$(_os_get_distro_type)" == "osx" ]; then
    return 0;
  fi
  return 1;
}

_os_is_windows() {
  if [ "$(_os_get_distro_type)" == "windows" ]; then
    return 0;
  fi
  return 1;
}

_os_linux_get_distro() {
  local _systype="$(_os_linux_pull_distro_name)";
  echo "${_systype}";
  return 0;
}

_os_get_distro_type() {
  echo "${_var['os_type']}";
}

_os_linux_pull_distro_name() {
  lsb_release -ds 2>/dev/null || cat /etc/*release 2>/dev/null | head -n1 | cut -d' ' -f1 | tr '[:upper:]' '[:lower:]' | cut -d'"' -f2 || uname -om | cut -d' ' -f1 | tr '[:upper:]' '[:lower:]' | cut -d'"' -f2;
}