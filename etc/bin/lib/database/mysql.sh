# Internal functions for the bash library that provide functionality for mysql database control.

mysql_verify() {
  :
}

mysql_version() {
  local _return=0;
  local _mysql_detected_binary="$(mysql_binary)";
  _return=$?;
  if [ $_return -eq 0 ]; then
    echo "$(${_mysql_detected_binary} --version | awk '{ print $5 }' | awk -F\, '{ print $1 }' | cut -d'-' -f1)";
  fi
  return $_return;
}

mysql_binary() {
  local _mysql_detected_binary="$(which mysql)";
  if [ "${_mysql_detected_binary}" == "" ]; then
    return 1;
  fi
  echo "${_mysql_detected_binary}";
  return 0;
}

mysql_config() {
  :
}

