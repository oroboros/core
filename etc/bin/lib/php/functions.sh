# General purpose utilities for handling php interaction

php_verify() {
  :
}

php_version() {
  local _return=0;
  local _php_detected_binary="$(php_binary)";
  _return=$?;
  if [ $_return -eq 0 ]; then
    echo "$(php -v | grep ^PHP | cut -d' ' -f2)";
  fi
  return $_return;
}

php_binary() {
  local _php_detected_binary="$(which php)";
  if [ "${_php_detected_binary}" == "" ]; then
    return 1;
  fi
  echo "${_php_detected_binary}";
  return 0;
}

php_config() {
  :
}

php_lint() {
  :
}

php_execute() {
  :
}

php_run() {
  :
}

php_composer_verify() {
  :
}

php_composer_fetch() {
  :
}

php_composer_binary() {
  local _php_composer_detected_binary="$(which composer)";
  if [ "${_php_composer_detected_binary}" == "" ]; then
    return 1;
  fi
  echo "${_php_composer_detected_binary}";
  return 0;
}

php_composer_version() {
  local _return=0;
  local _php_composer_detected_binary="$(php_composer_binary)";
  _return=$?;
  if [ $_return -eq 0 ]; then
    echo "$(${_php_composer_detected_binary} -V | cut -d' ' -f3)";
  fi
  return $_return;
}

php_composer_install() {
  :
}

php_composer_flush() {
  :
}

php_compser_update() {
  :
}

php_composer_require() {
  :
}

php_composer_package() {
  :
}

php_composer_package_version() {
  :
}

