#!/bin/bash

# This file handles existing variables and configurations.
# If the current user has defined custom configuration options,
# they will be parsed here in place of the default settings.
#
# Some additional variables will be localized from the global scope,
# such as the username, home directory, etc also.
# This allows persistent references to them in the event that
# administrative modules alter them, or perform operations that
# would skew them such as sudo su or other system maintenance
# operations that may alter the existing user environment.

# Current shell user
_var['user']="$USER";
# Current shell user home directory
_var['homedir']=~;
# Current shell user path (string format)
_var['path']=$PATH;
# Current shell version
_var['shell']=$SHELL;
# Current language format for shell sessions
_var['lang']=$LANG;
# Current default command line text editor with safe fallback
_var['editor']=${_var['editor']:-${VISUAL:-vi}};
# Current bash environment
_var['env']=$BASH_ENV;
# Current free disk space of the primary mount. Can be used to check if a backup operation will choke due to disk space constraints.
_var['free_space']=$(df -k --output=avail "${_var['homedir']}" | tail -n1);
if [[ "$OSTYPE" == "linux-gnu" ]]; then
        # ...
    _var['os_type']='linux';
elif [[ "$OSTYPE" == "darwin"* ]]; then
        # Mac OSX
    _var['os_type']='osx';
elif [[ "$OSTYPE" == "cygwin" ]]; then
        # POSIX compatibility layer and Linux environment emulation for Windows
    _var['os_type']='windows';
elif [[ "$OSTYPE" == "msys" ]]; then
        # Lightweight shell and GNU utilities compiled for Windows (part of MinGW)
    _var['os_type']='windows';
elif [[ "$OSTYPE" == "win32" ]]; then
        # I'm not sure this can happen.
    _var['os_type']='windows';
elif [[ "$OSTYPE" == "freebsd"* ]]; then
        # ...
    _var['os_type']='linux';
else
        # Unknown.
    _var['os_type']='unknown';
fi