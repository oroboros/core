#!/bin/bash

# This file routes arguments to the correct module

if [ ! $# -eq 0 ]; then
  _command="${1}";
  _app_command="${1}";
  _app_path+=("${_command}");
  declare _last;
  shift 1;
    while [[ $# -gt 0 ]]
    do
    key="${1}";
    if [[ "${key}" = "--"* ]] && [[ "${key}" = *"="* ]]; then
      k="$(extract_arg_key "${key}")";
      v="$(extract_arg_value "${key}")";
      set_arg "${k}" "${v}";
      set_app_arg "${k}" "${v}";
      shift 1;
      continue;
    elif [[ "${key}" = "--"* ]]; then
      if in_array _flags $key; then
          # Verified as a bash flag, may also be an app flag
          _flagset+=("${key}");
          _app_flagset+=("${key}");
      else
          # Not a bash flag, may be an app flag
          _app_flagset+=("${key}");
      fi
      shift 1;
      continue;
    elif [[ "${key}" = "-"* ]]; then
      subj="$(echo "${key}" | cut -d'-' -f2)";
      shift 1;
      while [[ ${#subj} -gt 0 ]]
      do
        nextchar="-${subj:0:1}";
        subj=${subj:1};
        if in_array _flags "${nextchar}"; then
          # Verified as a bash flag, may also be an app flag
          _flagset+=("${nextchar}");
          _app_flagset+=("${nextchar}");
        else
          # Not a bash flag, may be an app flag
          _app_flagset+=("${nextchar}");
        fi
      done;
      continue;
    else
      if [[ $# -lt 2 ]] || [[ "${2}" = "-"* ]]; then
        _app_path+=("${key}");
        shift 1;
      else
        _app_path+=("${1}");
        _app_path+=("${2}");
        _params[$1]="${2}";
        set_arg "${1}" "${2}";
        shift 2; # past value
      fi
    fi
    done
fi
