#!/bin/bash
case $_exit_code in
  0 )
    # Clean exit
    success "Operation completed.";
  ;;
  1 )
    # General Error
    error "Program exited with errors.";
  ;;
  2 )
    # Syntax Error
    error "A syntax error occurred.";
  ;;
  126 )
    # Command cannot execute
    error "The requested command could not resolve.";
  ;;
  127 )
    # Command not found
    error "The requested command is not recognized.";
  ;;
  128 )
    # Invalid exit command
    error "Program exited with an invalid exit command.";
  ;;
  130 )
    # SIGTERM
    error "Program terminated by user.";
  ;;
  137 )
    # kill -9
    error "Program force exited.";
  ;;
  * )
    # Unknown error
    error "Program exited with an unknown error.";
  ;;
esac;
# This file runs cleanup after an operation has completed, insuring that lock files and temporary files are appropriately cleared prior to allowing the script to terminate.
#clean_locks;
terminate;