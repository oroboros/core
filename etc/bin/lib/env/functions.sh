#!/bin/bash

# Provides functionality for evaluation of the command line server environment

env_init() {
    local _pwd="$(pwd)";
    echo "${_pwd}";
    exit 0;
}
