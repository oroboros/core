#!/bin/bash

# This file provides functionality for outputting color text to the command line.
# These must be printed with either `echo -e` or `printf` to display correctly

declare -r _f_bold="\x1B[1m";     # Bold text
declare -r _f_italic="\x1B[3m";   # Italic text
declare -r _f_underline="\x1B[4m";# Underline text
declare -r _f_strike="\x1B[9m";   # Strikethrough text
declare -r _c_clear="\x1B[0m";    # Reset to defaults
declare -r _c_red="\x1B[31m";     # dark red
declare -r _c_yellow="\x1B[93m";  # yellow
declare -r _c_green="\x1B[92m";   # light green (dark green has visibility issues)
declare -r _c_orange="\x1B[91m";  # orange/light red (outputs as lighter red on some consoles)
declare -r _c_blue="\x1B[34m";    # blue
declare -r _c_cyan="\x1B[96m";     # cyan/light blue (depending on console)
declare -r _c_gray="\x1B[37m";    # gray


