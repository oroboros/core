# General purpose utilities for handling memcache interaction

memcache_verify() {
  :
}

memcache_version() {
  local _return=0;
  local _memcache_detected_binary="$(memcache_binary)";
  _return=$?;
  if [ $_return -eq 0 ]; then
    echo "$(${_memcache_detected_binary} --version | cut -d'=' -f2 | cut -d' ' -f2)";
  fi
  return $_return;
}

memcache_binary() {
  local _memcache_detected_binary="$(which memcached)";
  if [ ! $? -eq 0 ]; then
    return 1;
  fi
  echo "${_memcache_detected_binary}";
  return 0;
}

memcache_config() {
  :
}

