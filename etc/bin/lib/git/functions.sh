
git_verify() {
  :
}

git_version() {
  local _return=0;
  local _git_detected_binary="$(git_binary)";
  _return=$?;
  if [ $_return -eq 0 ]; then
    echo "$(git --version | grep ^git | cut -d' ' -f3)";
  fi
  return $_return;
}

git_binary() {
  local _git_detected_binary="$(which git)";
  if [ "${_git_detected_binary}" == "" ]; then
    return 1;
  fi
  echo "${_git_detected_binary}";
  return 0;
}

git_config() {
  :
}

# Internal functions for the bash library that provide functionality for git version control.

# Returns the name of the current git branch for the provided directory
git_get_current_branch() {
  local _dirname="$1";
  local _git_folder="${_dirname}";
  local _git_current_branch="";
  local _str="";
  local _pwd="$(pwd)";
  if [ ! -d "${_dirname}" ]; then
    # Directory does not exist
    warning "Provided directory [${_c_clear}${_dirname}${_c_yellow}] does not exist.";
    return 1;
  fi
  if [ ! -d "${_dirname}/.git" ] || [ ! "true" == "$(cd "${_dirname}" && git rev-parse --is-inside-work-tree 2> /dev/null && cd "${_pwd}")" ]; then
    # Directory is not a git repository
    warning "Provided directory [${_c_clear}${_dirname}${_c_yellow}] is not a git repository.";
    return 1;
  fi
  cd "${_git_folder}";
  _str="$(git symbolic-ref HEAD 2>/dev/null | cut -d"/" -f 3)";
  if [ ! -z "$(git symbolic-ref HEAD 2>/dev/null | cut -d"/" -f 4)" ]; then
    _str="${_str}/$(git symbolic-ref HEAD 2>/dev/null | cut -d"/" -f 4)";
  fi
  _git_current_branch="${_str}";
  echo "${_git_current_branch}";
  cd "${_pwd}";
  return 0;
}

# Returns the name of the current git commit hash for the provided directory
git_get_current_commit_id() {
  local _dirname="$1";
  local _git_folder="${_dirname}";
  local _git_commit_hash="";
  local _pwd="$(pwd)";
  if [ ! -d "${_dirname}" ]; then
    # Directory does not exist
    warning "Provided directory [${_c_clear}${_dirname}${_c_yellow}] does not exist.";
    return 1;
  fi
  if [ ! -d "${_dirname}/.git" ] || [ ! "true" == "$(cd "${_dirname}" && git rev-parse --is-inside-work-tree 2> /dev/null && cd "${_pwd}")" ]; then
    # Directory is not a git repository
    warning "Provided directory [${_c_clear}${_dirname}${_c_yellow}] is not a git repository.";
    return 1;
  fi
  cd "${_git_folder}";
  _git_commit_hash="$(git rev-parse --short HEAD)";
  echo "${_git_commit_hash}";
  cd "${_pwd}";
}

# Checks if the remote repository has changes.
# If unchanged, does nothing.
# If local is ahead of remote, also does nothing.
# If local is behind remote cleanly, performs a git pull.
# If local has diverged from remote, performs a git pull --rebase.
# If any pull needs to be done and uncommitted changes exist, stashes them and restores the stash after the pull, then drops the stash.
# Throws a warning on an invalid remote, non-existent folder, or if the provided path is not a git repository.
# $1 (string) the directory containing the git repo
# $2 (string) the name of the remote. If not supplied, defaults to "origin" (optional)
# $3 (string) the name of the branch to update. If not supplied, defaults to the current local branch (optional)
git_check_remote_changes() {
  local _dirname="$1";
  local _remote="$([[ -n "$2" ]] && echo "$2" || echo "origin" )";
  local _remote_addr="";
  local _git_folder="${_dirname}";
  local _git_commit_hash="";
  local _pwd="$(pwd)";
  local _branch="$(git_get_current_branch "${_dirname}")";
  local _update_branch="$([[ -n "$3" ]] && echo "$3" || echo "${_branch}" )";
  local _local_longhash="";
  local _remote_longhash="";
  local _base_longhash="";
  local _restore_stash=1;
  local _stash_name="";
  local _cmd="";
  if [ ! -d "${_dirname}" ]; then
    # Directory does not exist
    warning "Provided directory [${_c_clear}${_dirname}${_c_yellow}] does not exist.";
    return 1;
  fi
  if [ ! -d "${_dirname}/.git" ] || [ ! "true" == "$(cd "${_dirname}" && git rev-parse --is-inside-work-tree 2> /dev/null && cd "${_pwd}")" ]; then
    # Directory is not a git repository
    warning "Provided directory [${_c_clear}${_dirname}${_c_yellow}] is not a git repository.";
    return 1;
  fi
  cd "${_git_folder}";
  _remote_addr="$(git remote -v | grep "${_remote}" | cut -d'@' -f2 | cut -d' ' -f1)";
  # Update remote references
  _cmd="git remote update ${_remote}";
  if has_flag "--debug"; then
    $_cmd;
  else
    $_cmd >/dev/null 2>&1;
  fi
  git ls-remote --heads "${_remote}" | grep "${_update_branch}" &>/dev/null;
  if [ $? -eq 1 ]; then
    # Branch does not exist on remote. Skip pull.
    debug "Current branch [${_c_yellow}${_branch}${_c_gray}] does not exist on remote [${_c_yellow}${_remote}${_c_gray}]. No action required.";
  else
    # Stash any uncommitted changes temporarily
    if ! git_check_uncommitted_changes "${_dirname}"; then
      debug "Stashing local changed files for [${_c_yellow}${_branch}${_c_cyan}].";
      _restore_stash=0;
      _stash_name="${_branch}_${_local_longhash}";
      _cmd="git stash save ${_stash_name} $(has_flag '--debug' || echo '--quiet')";
      $_cmd;
    fi
    if [ "${_branch}" != "${_update_branch}" ]; then
      debug "Switching from current branch [${_c_yellow}${_branch}${_c_cyan}] to update branch [${_c_yellow}${_update_branch}${_c_cyan}].";
      _cmd="git checkout ${_update_branch} $(has_flag '--debug' || echo '--quiet')";
      $_cmd;
    fi
    _local_longhash="$(git rev-parse @)";
    _remote_longhash="$(git rev-parse "${_remote}/${_update_branch}")";
    _base_longhash="$(git merge-base @ "${_remote}/${_update_branch}")";
    if [ "${_local_longhash}" == "${_remote_longhash}" ]; then
      # Current local branch is up to date with remote. Do nothing
      info "No changes to pull for branch [${_c_yellow}${_update_branch}${_c_cyan}] from remote [${_c_yellow}${_remote}${_c_cyan}].";
      debug "Current branch [${_c_yellow}${_update_branch}${_c_gray}] is up to date with remote [${_c_yellow}${_remote}${_c_gray}].";
    elif [ "${_local_longhash}" == "${_base_longhash}" ]; then
      # Current branch is behind remote. Pull required.
      info "Pulling changes for [${_c_yellow}${_update_branch}${_c_cyan}] from remote [${_c_yellow}${_remote}${_c_cyan}].";
      debug "Current branch [${_c_yellow}${_update_branch}${_c_gray}] is behind remote [${_c_yellow}${_remote}${_c_gray}] with no divergence. Rebase not needed.";
      git pull "${_remote}" "${_update_branch}" $(has_flag '--debug' || echo '--quiet');
    elif [ "${_remote_longhash}" == "${_base_longhash}" ]; then
      # Current branch is ahead of remote. Do nothing.
      info "No changes to pull for branch [${_c_yellow}${_update_branch}${_c_cyan}] from remote [${_c_yellow}${_remote}${_c_cyan}].";
      debug "Current branch [${_c_yellow}${_update_branch}${_c_gray}] is currently ahead of remote [${_c_yellow}${_remote}${_c_gray}].";
    else
      # Diverged. Throw error. @TODO pull-rebase
      info "Local branch [${_c_yellow}${_update_branch}${_c_cyan}] has diverged from remote [${_c_yellow}${_remote}${_c_cyan}]. Rebasing local changes on top of remote commits.";
      _cmd="git pull ${_remote} ${_update_branch} --rebase $(has_flag '--debug' || echo '--quiet')";
      $_cmd;
    fi
    if [ "${_branch}" != "${_update_branch}" ]; then
      debug "Restoring from current branch [${_c_yellow}${_branch}${_c_cyan}] from update branch [${_c_yellow}${_update_branch}${_c_cyan}].";
      _cmd="git checkout ${_branch} $(has_flag '--debug' || echo '--quiet')";
      $_cmd;
    fi
    if [ $_restore_stash -eq 0 ]; then
      debug "Restoring local stashed files for [${_c_yellow}${_branch}${_c_cyan}].";
      _cmd="git stash restore ${_stash_name} $(has_flag '--debug' || echo '--quiet')";
      $_cmd;
      debug "Removing stale stash with id [${_c_yellow}${_stash_name}${_c_gray}].";
      _cmd="git drop stash ${_stash_name} $(has_flag '--debug' || echo '--quiet')";
      $_cmd;
    fi
  fi
  cd "${_pwd}";
  return 0;
}

# Checks if the current git repo has uncommitted changes
git_check_uncommitted_changes() {
  local _dirname="$1";
  local _remote="$([[ -n "$2" ]] && echo "$2" || echo "origin" )";
  local _remote_addr="";
  local _git_folder="${_dirname}";
  local _git_commit_hash="";
  local _pwd="$(pwd)";
  local _branch="$(git_get_current_branch "${_dirname}")";
  local _local_longhash="";
  local _remote_longhash="";
  local _base_longhash="";
  local _err=0;
  if [ ! -d "${_dirname}" ]; then
    # Directory does not exist
    warning "Provided directory [${_c_clear}${_dirname}${_c_yellow}] does not exist.";
    _err=1;
  elif [ ! -d "${_dirname}/.git" ] || [ ! "true" == "$(cd "${_dirname}" && git rev-parse --is-inside-work-tree 2> /dev/null && cd "${_pwd}")" ]; then
    # Directory is not a git repository
    warning "Provided directory [${_c_clear}${_dirname}${_c_yellow}] is not a git repository.";
    _err=1;
  else
    cd "${_git_folder}";
    # Update the index
    git update-index -q --ignore-submodules --refresh;
    # Disallow uncommitted changes in the index
    if ! git diff-index --cached --quiet HEAD --ignore-submodules --; then
      _err=1;
    fi
  fi
  cd "${_pwd}";
  return $_err;
}

# Checks if the current git repo has unstaged files
git_check_unstaged_files() {
  local _dirname="$1";
  local _remote="$([[ -n "$2" ]] && echo "$2" || echo "origin" )";
  local _remote_addr="";
  local _git_folder="${_dirname}";
  local _git_commit_hash="";
  local _pwd="$(pwd)";
  local _branch="$(git_get_current_branch "${_dirname}")";
  local _local_longhash="";
  local _remote_longhash="";
  local _base_longhash="";
  local _err=0;
  if [ ! -d "${_dirname}" ]; then
    # Directory does not exist
    warning "Provided directory [${_c_clear}${_dirname}${_c_yellow}] does not exist.";
    return 1;
  elif [ ! -d "${_dirname}/.git" ] || [ ! "true" == "$(cd "${_dirname}" && git rev-parse --is-inside-work-tree 2> /dev/null && cd "${_pwd}")" ]; then
    # Directory is not a git repository
    warning "Provided directory [${_c_clear}${_dirname}${_c_yellow}] is not a git repository.";
    return 1;
  else
    cd "${_git_folder}";
    # Update the index
    git update-index -q --ignore-submodules --refresh;
    # Disallow unstaged changes in the working tree
    if ! git diff-files --quiet --ignore-submodules --; then
      _err=1;
    fi
  fi
  cd "${_pwd}";
  return $_err;
}

# Lists the commit hashes in chronological order
# $1 (string) The folder location of the git repo (required)
# $2 (int) If passed as 0, lists the long hashes instead of the short hashes (optional)
# Returns exit code 1 with a warning if the provided directory does not exist or is not a git repo
git_list_commits() {
  local _dirname="$1";
  local _pwd="$(pwd)";
  local _err=0;
  local _current_hash="";
  if [ ! -d "${_dirname}" ]; then
    # Directory does not exist
    warning "Provided directory [${_c_clear}${_dirname}${_c_yellow}] does not exist.";
    return 1;
  elif [ ! -d "${_dirname}/.git" ] || [ ! "true" == "$(cd "${_dirname}" && git rev-parse --is-inside-work-tree 2> /dev/null && cd "${_pwd}")" ]; then
    # Directory is not a git repository
    warning "Provided directory [${_c_clear}${_dirname}${_c_yellow}] is not a git repository.";
    return 1;
  else
    cd "${_dirname}";
    IFS=$'\n';
    for i in $(git log --pretty=format:"%ad %h by %an, %s" --date=iso | sort -r | less); do
      _current_hash="$(echo "${i}" | cut -d " " -f 4)";
    if [ ! -z "$2" ] && [ $2 -eq 0 ]; then
      # Echo the long hash
      git rev-parse "${_current_hash}";
    else
      # Echo the short hash
      echo "${_current_hash}";
    fi
    done
    cd "${_pwd}";
    return $_err;
  fi
}
# Lists git tags for the given repository filepath
# $1 (string) The folder location of the git repo (required)
# $2 (int) If passed as 0, lists the short hashes with the tag name (optional)
# $3 (int) If passed as 0, lists the long hashes instead of the short hashes when the 2nd parameter is also true (optional)
# Returns exit code 1 with a warning if the provided directory does not exist or is not a git repo
git_list_tags() {
  local _dirname="$1";
  local _pwd="$(pwd)";
  local _err=0;
  local _current_tag="";
  local _current_hash="";
  if [ ! -d "${_dirname}" ]; then
    # Directory does not exist
    warning "Provided directory [${_c_clear}${_dirname}${_c_yellow}] does not exist.";
    return 1;
  elif [ ! -d "${_dirname}/.git" ] || [ ! "true" == "$(cd "${_dirname}" && git rev-parse --is-inside-work-tree 2> /dev/null && cd "${_pwd}")" ]; then
    # Directory is not a git repository
    warning "Provided directory [${_c_clear}${_dirname}${_c_yellow}] is not a git repository.";
    return 1;
  else
    cd "${_dirname}";
    IFS=$'\n';
    for i in $(git show-ref --tags); do
      _current_tag="$(echo "${i}" | cut -d " " -f 2 | cut -d "/" -f 3)";
      if [ ! -z "$2" ] && [ $2 -eq 0 ]; then
        # Echo the long hash
        if [ ! -z "$3" ] && [ $3 -eq 0 ]; then
          _current_hash="$(git rev-list -n 1 ${i})";
        else
          _current_hash="$(git rev-parse --short $(git rev-list -n 1 ${i}))";
        fi
      fi
      if [ ! -z "${_current_hash}" ]; then
        echo "${_current_hash} ${_current_tag}";
      else
        echo "${_current_tag}";
      fi
    done
    cd "${_pwd}";
    return $_err;
  fi
}