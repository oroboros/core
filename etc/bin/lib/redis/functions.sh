# General purpose utilities for handling redis interaction

redis_verify() {
  :
}

redis_version() {
  local _return=0;
  local _redis_detected_binary="$(redis_binary)";
  _return=$?;
  if [ $_return -eq 0 ]; then
    echo "$(${_redis_detected_binary} -v | cut -d'=' -f2 | cut -d' ' -f1)";
  fi
  return $_return;
}

redis_binary() {
  local _redis_detected_binary="$(which redis-server)";
  if [ -z "${_redis_detected_binary}" ]; then
    return 1;
  fi
  echo "${_redis_detected_binary}";
  return 0;
}

redis_binary_type() {
  local _binary="$(redis_binary)";
  if [ ! $? -eq 0 ] || [ -z "${_binary}" ]; then
    return 1;
  fi
  echo "$(basename ${_binary})";
  return 0;
}

redis_config() {
  :
}

