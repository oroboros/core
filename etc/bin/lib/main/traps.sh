#!/bin/bash

# Designates primary exit handler functions

# @description signal handler for SIGINT
function Main__interruptHandler() {
    echo "SIGINT caught"
    exit
} 
# @description signal handler for SIGTERM
function Main__terminationHandler() { 
    echo "SIGTERM caught"
    exit
} 
# @description signal handler for end of the program (clean or unclean). 
# probably redundant call, we already call the cleanup in main.
function Main__exitHandler() { 
    echo 'Program exited.';
    exit
} 
