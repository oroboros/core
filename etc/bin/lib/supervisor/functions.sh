# General purpose utilities for handling supervisord interaction


supervisor_verify() {
  :
}

supervisor_version() {
  local _return=0;
  local _supervisor_detected_binary="$(supervisor_binary)";
  _return=$?;
  if [ $_return -eq 0 ]; then
    echo "$(supervisord -v)";
  fi
  return $_return;
}

supervisor_binary() {
  local _supervisor_detected_binary="$(which supervisord)";
  if [ "${_supervisor_detected_binary}" == "" ]; then
    return 1;
  fi
  echo "${_supervisor_detected_binary}";
  return 0;
}

supervisor_config() {
  :
}
