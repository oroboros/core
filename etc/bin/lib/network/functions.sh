#!/bin/bash

# This file provides functionality for verifying network and host connectivity.

network_verify() {
  local _hostname="$1";
  local _port="$2";
  if [[ $_hostname = "" ]]; then
    # Use the default
    _hostname="google.com";
  fi
  if ! [[ $_port = "" ]]; then
    # Use the default
    _hostname="${_hostname}:${_port}";
  fi
  if ping -q -c 1 -W 1 "${_hostname}" >/dev/null 2>&1; then
    return 0;
  else
    return 1;
  fi
}