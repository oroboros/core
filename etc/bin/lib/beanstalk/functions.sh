# General purpose utilities for handling beanstalkd interaction


beanstalk_verify() {
  :
}

beanstalk_version() {
  local _return=0;
  local _beanstalk_detected_binary="$(beanstalk_binary)";
  _return=$?;
  if [ $_return -eq 0 ]; then
    echo "$(beanstalkd -v | grep beanstalkd | cut -d' ' -f2)";
  fi
  return $_return;
}

beanstalk_binary() {
  local _beanstalk_detected_binary="$(which beanstalkd)";
  if [ "${_beanstalk_detected_binary}" == "" ]; then
    return 1;
  fi
  echo "${_beanstalk_detected_binary}";
  return 0;
}

beanstalk_config() {
  :
}
