#!/bin/bash

# Gets the path of the cron binary.
# Returns false if cron is not installed.
cron_binary() {
  local _cron="$(which crontab)";
  if [ $? -eq 0 ]; then
    echo "${_cron}";
    return 0;
  fi
  return 1;
}

# Sets a job to run at an interval of minutes
cron_interval_minute() {
  local _cron="$(cron_binary)";
  local _command="${1}";
  local _interval=${2};
  local _rangeset="";
  local _entry="";
  local re='^[0-9]+$';
  if [ ! $? -eq 0 ]; then
    error "No crontab installation was detected.";
    return 1;
  fi
  if [ -z "${_command}" ]; then
    error "No command supplied, cannot set cron interval.";
    return 1;
  fi
  if [ -z "${_interval}" ]; then
    error "No interval supplied, cannot set cron interval.";
    return 1;
  fi
  if ! [[ $_interval =~ $re ]] ; then
    echo "Interval is not a valid number. Cannot set cron interval.";
    return 1;
  fi
  if [[ $_interval -lt 0 ]] || [[ $_interval -gt 60 ]]; then
    echo "Interval is not in the valid minute of hour interval range. Cannot set cron interval.";
    return 1;
  fi
  _rangeset="*/${_interval}";
  _entry="$(_cron_format_command "${_command}" "${_rangeset}")";
  _cron_add_entry "${_entry}";
  return $?;
}

# Sets a job to run at an interval of hours
cron_interval_hour() {
  local _cron="$(cron_binary)";
  local _command="${1}";
  local _interval=${2};
  local _rangeset="";
  local _entry="";
  local re='^[0-9]+$';
  if [ ! $? -eq 0 ]; then
    error "No crontab installation was detected.";
    return 1;
  fi
  if [ -z "${_command}" ]; then
    error "No command supplied, cannot set cron interval.";
    return 1;
  fi
  if [ -z "${_interval}" ]; then
    error "No interval supplied, cannot set cron interval.";
    return 1;
  fi
  if [[ $_interval -lt 0 ]] || [[ $_interval -gt 23 ]]; then
    echo "Interval is not in the valid hour of day interval range. Cannot set cron interval.";
    return 1;
  fi
  _rangeset="*/${_interval}";
  _entry="$(_cron_format_command "${_command}" * "${_rangeset}")";
  _cron_add_entry "${_entry}";
  return $?;
}

# Sets a job to run at an interval of days
cron_interval_day() {
  local _cron="$(cron_binary)";
  local _command="${1}";
  local _interval=${2};
  local _rangeset="";
  local _entry="";
  local re='^[0-9]+$';
  if [ ! $? -eq 0 ]; then
    error "No crontab installation was detected.";
    return 1;
  fi
  if [ -z "${_command}" ]; then
    error "No command supplied, cannot set cron interval.";
    return 1;
  fi
  if [ -z "${_interval}" ]; then
    error "No interval supplied, cannot set cron interval.";
    return 1;
  fi
  if [[ $_interval -lt 0 ]] || [[ $_interval -gt 31 ]]; then
    echo "Interval is not in the valid day of month interval range. Cannot set cron interval.";
    return 1;
  fi
  _rangeset="*/${_interval}";
  _entry="$(_cron_format_command "${_command}" * * "${_rangeset}")";
  _cron_add_entry "${_entry}";
  return $?;
}

# Sets a job to run at an interval of months
cron_interval_month() {
  local _cron="$(cron_binary)";
  local _command="${1}";
  local _interval=${2};
  local _rangeset="";
  local _entry="";
  local re='^[0-9]+$';
  if [ ! $? -eq 0 ]; then
    error "No crontab installation was detected.";
    return 1;
  fi
  if [ -z "${_command}" ]; then
    error "No command supplied, cannot set cron interval.";
    return 1;
  fi
  if [ -z "${_interval}" ]; then
    error "No interval supplied, cannot set cron interval.";
    return 1;
  fi
  if [[ $_interval -lt 0 ]] || [[ $_interval -gt 12 ]]; then
    echo "Interval is not in the valid month of year interval range. Cannot set cron interval.";
    return 1;
  fi
  _rangeset="*/${_interval}";
  _entry="$(_cron_format_command "${_command}" * * * "${_rangeset}")";
  _cron_add_entry "${_entry}";
  return $?;
}

# Sets a job to run at a set day of the week
cron_interval_weekday() {
  local _cron="$(cron_binary)";
  local _command="${1}";
  local _interval=${2};
  local _rangeset="";
  local _entry="";
  local re='^[0-9]+$';
  if [ ! $? -eq 0 ]; then
    error "No crontab installation was detected.";
    return 1;
  fi
  if [ -z "${_command}" ]; then
    error "No command supplied, cannot set cron interval.";
    return 1;
  fi
  if [ -z "${_interval}" ]; then
    error "No interval supplied, cannot set cron interval.";
    return 1;
  fi
  if [[ $_interval -lt 0 ]] || [[ $_interval -gt 7 ]]; then
    echo "Interval is not in the valid day of week interval range. Cannot set cron interval.";
    return 1;
  fi
  _rangeset="*/${_interval}";
  _entry="$(_cron_format_command "${_command}" * * * * "${_rangeset}")";
  _cron_add_entry "${_entry}";
  return $?;
}

# Formats an interval string correctly
_cron_format_interval_string() {
  local _cron="$(cron_binary)";
  local _command="${1}";
  local _interval=${2};
  local re='^[0-9]+$';
  if [ ! $? -eq 0 ]; then
    error "No crontab installation was detected.";
    return 1;
  fi
  if [ -z "${_command}" ]; then
    error "No command supplied, cannot set cron interval.";
    return 1;
  fi
  if [ -z "${_interval}" ]; then
    error "No interval supplied, cannot set cron interval.";
    return 1;
  fi
}

# Formats a semantically correct crontab job entry string
_cron_format_command() {
  local _command="${1}";
  local _minute_of_hour="${2}";
  local _hour_of_day="${3}";
  local _day_of_month="${4}";
  local _month_of_year="${5}";
  local _day_of_week="${6}";
  local _cronstring="";
  if [ -z "${_command}" ]; then
    error "No command supplied. Cannot format cron job.";
    return 1;
  fi
  if [ -z "${_minute_of_hour}" ]; then
    _minute_of_hour="*";
  fi
  if [ -z "${_hour_of_day}" ]; then
    _hour_of_day="*";
  fi
  if [ -z "${_day_of_month}" ]; then
    _day_of_month="*";
  fi
  if [ -z "${_month_of_year}" ]; then
    _month_of_year="*";
  fi
  if [ -z "${_day_of_week}" ]; then
    _day_of_week="*";
  fi
  _cronstring="${_minute_of_hour} ${_hour_of_day} ${_day_of_month} ${_month_of_year} ${_day_of_week} ${_command}";
  echo "${_cronstring}";
  return 0;
}

# Checks if a duplicate cron job entry exists
_cron_check_duplicate() {
  local _cron="$(cron_binary)";
  local _entry="${1}";
  local _exists=1;
  local _crontab="";
  local _old_ifs="${IFS}";
  if [ ! $? -eq 0 ]; then
    error "No crontab installation was detected.";
    return 1;
  fi
  if [ -z "${_entry}" ]; then
    error "No crontab entry provided.";
    return 1;
  fi
  _crontab="$(_cron_get_crontab)";
  if [ ! $? -eq 0 ]; then
    error "Could not obtain crontab. Have you run [${_c_yellow}crontab -e${_c_orange}] at least once?";
    return 1;
  fi
  IFS=$'\n';
  set -f
  for i in $(_cron_get_crontab);
  do
    if [[ "${i}" = "${_entry}" ]]; then
      _exists=0;
      break;
    fi
  done
  IFS=$"${_old_ifs}";
  return $_exists;
}

# Gets the crontab of the current user scope
_cron_get_crontab() {
  local _designation=0;
  local _cron="$(cron_binary)";
  local _crontab="";
  if [ ! $? -eq 0 ]; then
    error "No crontab installation was detected.";
    return 1;
  fi
  _crontab="$(crontab -l 2>/dev/null)";
  _designation=$?;
  if [ $_designation -eq 0 ]; then
    echo $"${_crontab}";
  fi
  return $_designation;
}

# Adds an entry to the crontab of the current user scope,
# if it is not already entered.
_cron_add_entry() {
  local _designation=0;
  local _entry="${1}";
  local _cron="$(cron_binary)";
  local _check=0;
  if [ ! $? -eq 0 ]; then
    error "No crontab installation was detected.";
    return 2;
  fi
  if [ -z "${_entry}" ]; then
    error "No entry argument supplied, cannot enter job into crontab.";
    return 2;
  fi
  _cron_check_duplicate "${_entry}";
  _check=$?;
  if [ $_check -eq 0 ]; then
    warning "Cron entry [${_c_clear}${_entry}${_c_yellow}] is already registered. Skipping duplicate entry.":
    return 1;
  elif [ $_check -eq 2 ]; then
    error "An error occurred while checking the crontab status. Entry cannot be scheduled.";
    return 1;
  fi
  (${_cron} -l ; echo "${_entry}") | ${_cron} -;
  _designation=$?;
  if [ $_designation -eq 0 ]; then
    info "Command [${_c_yellow}${_entry}${_c_cyan}] scheduled in crontab.";
  else
    error "Unable to schedule command [${_c_yellow}${_entry}${_c_orange}] in crontab.";
  fi
  return $_designation;
}