# General purpose utilities for handling app interaction

# Verifies that the core application integrity is sound
core_verify() {
  :
}

# Retrieves the core application version
core_version() {
  local _return=0;
  local _app_detected_binary="$(core_binary)";
  _return=$?;
  if [ $_return -eq 0 ]; then
    echo "$(${_app_detected_binary} -v | grep Apache | cut -d'/' -f2 | cut -d' ' -f1)";
  fi
  return $_return;
}

# Returns the fully qualified path of the core application front controller file
core_binary() {
  if [ -f "${_oroboros_dir}/index.php" ]; then
    echo "${_oroboros_dir}/index.php";
  else
    error "No core application index exists in directory [${_c_yellow}${_oroboros_dir}${_c_orange}]. Your installation is likely broken.";
    return 1;
  fi
  return 0;
}

# Returns the fully qualified path of the core application shell accessor
core_shell() {
  if [ -f "${_oroboros_dir}/oroboros" ]; then
    echo "${_oroboros_dir}/oroboros";
  else
    error "No core application shell exists in directory [${_c_yellow}${_oroboros_dir}${_c_orange}]. Your installation is likely broken.";
    return 1;
  fi
  return 0;
}

# Runs a command against the core application binary
core_exec() {
  local _designation=0;
  local _app_exec_path="$1";
  local _app_exec_args="$2";
  local _app_exec_flags="$3";
  local _pwd="$(pwd)";
  cd "${_dir}";
  local _app="$(core_binary)";
  if ! [[ $? -eq 0 ]]; then
    return 1
  fi
  if [ -z "${_app_exec_command}" ]; then
    _app_exec_command="$(app_get_command)";
    _designation=$_designation+$?;
  fi
  if [ -z "${_app_exec_args}" ]; then
    _app_exec_args="$(app_get_arguments)";
    _designation=$_designation+$?;
  fi
  if [ -z "${_app_exec_flags}" ]; then
    _app_exec_flags="$(app_get_flags)";
    _designation=$_designation+$?;
  fi
  if [ -z "${_app_exec_path}" ]; then
    _app_exec_path="$(app_get_path)";
    _designation=$_designation+$?;
  fi
  if ! [[ $_designation -eq 0 ]]; then
    return 1;
  fi
  php "${_app}" ${_app_exec_path} ${_app_exec_args} ${_app_exec_flags};
  _designation=$?;
  cd "${_pwd}";
  return $_designation;
}

# Runs a command against the core application shell
core_shell_exec() {
  local _designation=0;
  local _app_exec_command="$1";
  local _app_exec_args="$2";
  local _app_exec_flags="$3";
  local _pwd="$(pwd)";
  cd "${_dir}";
  local _app="$(core_shell)";
  if [ -z "${_app_exec_command}" ]; then
    _app_exec_command="$(app_get_command)";
    _designation=$_designation+$?;
  fi
  if [ -z "${_app_exec_args}" ]; then
    _app_exec_args="$(app_get_arguments)";
    _designation=$_designation+$?;
  fi
  if [ -z "${_app_exec_flags}" ]; then
    _app_exec_flags="$(app_get_flags)";
    _designation=$_designation+$?;
  fi
  if ! [[ $_designation -eq 0 ]]; then
    return 1;
  fi
#  info "Command: [${_c_yellow}${_app_command}${_c_cyan}]";
#  info "Arguments: [${_c_yellow}${_app_args}${_c_cyan}]";
#  info "Flags: [${_c_yellow}${_app_flags}${_c_cyan}]";
  "${_app}"${_app_exec_command}${_app_exec_args}${_app_exec_flags};
  _designation=$?;
  cd "${_pwd}";
  return $_designation;
}

# Configures the core application.
core_config() {
  :
}

# Gets the declared name of the core application
core_get_name() {
  local _designation=0;
  local _command="app";
  local _args="name";
  local _value="$(core_exec "${_command}" "${_args}")";
  if [ ! $? -eq 0 ]; then
    error "Core application not reachable in [${_c_yellow}$(pwd)${_c_orange}]. Your installation is likely broken.";
    return 1;
  fi
  echo "$(remove_colors ${_value})" | sed -e 's/\x1b\[[0-9;]*[a-zA-Z]//g';
  return $_designation;
}

# Gets the declared Psr-4 vendor/package namespace of the core application
core_get_namespace() {
  local _designation=0;
  local _command="app";
  local _args="namespace";
  local _value="$(core_exec "${_command}" "${_args}" | sed -r 's/\\/-/g')";
  if [ ! $? -eq 0 ]; then
    error "Core application not reachable in [${_c_yellow}${_oroboros_dir}${_c_orange}]. Your installation is likely broken.";
    return 1;
  fi
  remove_colors "${_value}";
  return $_designation;
}

# Gets the declared runtime environment of the core application
# (eg: local, dev, staging, production, etc)
core_get_environment() {
  local _designation=0;
  local _command="app";
  local _args="environment";
  local _value="$(core_exec "${_command}" "${_args}")";
  if [ ! $? -eq 0 ]; then
    error "Core application not reachable in [${_c_yellow}${_oroboros_dir}${_c_orange}]. Your installation is likely broken.";
    return 1;
  fi
  echo "$(remove_colors ${_value})" | sed -e 's/\x1b\[[0-9;]*[a-zA-Z]//g';
  return $_designation;
}

# Gets the absolute filepath of the core application
core_get_directory() {
  local _designation=0;
  local _command="app";
  local _args="path";
  local _value="$(core_exec "${_command}" "${_args}")";
  if [ ! $? -eq 0 ]; then
    error "Core application not reachable in [${_c_yellow}${_oroboros_dir}${_c_orange}]. Your installation is likely broken.";
    return 1;
  fi
  echo "$(remove_colors ${_value})";
  return $_designation;
}

# Gets the version number of the core application
core_get_version() {
  local _designation=0;
  local _command="app";
  local _args="version";
  local _value="$(core_exec "${_command}" "${_args}")";
  if [ ! $? -eq 0 ]; then
    error "Core application not reachable in [${_c_yellow}${_oroboros_dir}${_c_orange}]. Your installation is likely broken.";
    return 1;
  fi
  echo "$(remove_colors ${_value})";
  return $_designation;
}

# Returns the command designated to be passed to the core application if a core_exec call is made.
core_get_command() {
  local _app_command_cmd='';
  if exists $_command in _cmd; then
    # Recognized command.
    _app_command_cmd=" ${_cmd[${_command}]}";
  else
    # Unrecognized command, let the app handle it.
    _app_command_cmd=" ${_command}";
  fi
  echo "${_app_command_cmd}";
  return 0;
}

# Returns the path designated to be passed to the core application if a core_exec call is made.
core_get_path() {
  local _app_command_path='';
  local _key="";
  for i in "${_app_path[@]}"
  do
    _key="$(echo -e "$(remove_colors ${i})" | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//')";
    _app_command_path="${_app_command_path} ${_key}";
    _key="";
  done
  echo "${_app_command_path}";
  return 0;
}

# Returns the arguments designated to be passed to the core application if a core_exec call is made.
core_get_arguments() {
  local _app_command_args='';
  local _key="";
  local _value="";
  for i in "${!_app_args[@]}"
  do
    _key="$(echo -e "$(remove_colors ${i})" | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//')";
    _value="$(echo -e "${_app_args[$i]}")";
    _app_command_args="${_app_command_args} ${_key}=${_value}";
    _key="";
    _value="";
  done
  echo "${_app_command_args}";
  return 0;
}

# Returns the flags designated to be passed to the application if a core_exec call is made.
core_get_flags() {
  local _app_command_flags='';
  for i in "${_app_flagset[@]}"
  do
    _key="$(echo -e "$(remove_colors ${i})" | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//')";
    _app_command_flags="${_app_command_flags} ${_key}";
  done
  echo "${_app_command_flags}";
  return 0;
}

# Registers the core application path with the base platform
core_register() {
  local _designation=0;
  local _pwd="$(pwd)";
  local _path="${1}";
  if [ -z "${_path}" ]; then
    error "No path parameter supplied.";
    return 1;
  fi
  if [ ! -d "${_path}" ]; then
    error "Provided path parameter [${_c_yellow}${_path}${_c_orange}] is not a directory.";
    return 1;
  fi
  cd "${_path}";
  :
  cd "${_pwd}";
  return $_designation;
}

# Runs all validation functions on the core application. Returns 0 if all pass, 1 or higher if any fail.
core_validate() {
  local _designation=0;
  local _pwd="$(pwd)";
  local _path="${1}";
  if [ -z "${_path}" ]; then
    error "No path parameter supplied.";
    return 1;
  fi
  if [ ! -d "${_path}" ]; then
    error "Provided path parameter [${_c_yellow}${_path}${_c_orange}] is not a directory.";
    return 1;
  fi
  cd "${_path}";
  _app_validate_environment "${_path}";
  if [ ! $? -eq 0 ]; then
    warning "Core application environment is not valid.";
    ((_designation++));
  fi
  _app_validate_registration "${_path}";
  if [ ! $? -eq 0 ]; then
    warning "Core application registration is not valid.";
    ((_designation++));
  fi
  _app_validate_resources "${_path}";
  if [ ! $? -eq 0 ]; then
    warning "Core application resources are not valid.";
    ((_designation++));
  fi
  _app_validate_installation "${_path}";
  if [ ! $? -eq 0 ]; then
    warning "Core application installation is not valid.";
    ((_designation++));
  fi
  _app_validate_dns "${_path}";
  if [ ! $? -eq 0 ]; then
    warning "Core application installation is not valid.";
    ((_designation++));
  fi
  cd "${_pwd}";
  return $_designation;
}
