# Internal functions for the bash library that provide functionality for managing process locks with lockfiles.
# This file contains functionality for clearing, setting, and checking lock files, which are used to prevent
# duplicate operations from running concurrently where they would interfere with each others operations.

# Removes a single lock. This can be called at the end of any blocking process when it has resolved correctly.
_remove_lock(){
  if [ ! $1 ]; then
    error "Incorrect usage. No lock file parameter passed.";
    return 1;
  fi
  if [ -f "${_lock}/${1}.lock" ]; then
    rm "${_lock}/${1}.lock";
  fi
  _update_locks;
  return 0;
}

# Sets a single lock. This should be called on any long running operation that makes system changes to prevent duplicate concurrent actions colliding.
_set_lock(){
  if [ ! $1 ]; then
    error "Incorrect usage. No lock file parameter passed.";
    return 1;
  fi
  echo "${_pid}" > "${_lock}/${1}.lock";
  _locks["${1}"]="${_pid}";
  _update_locks;
  return 0;
}

# Checks if a lock is currently set for an operation. Returns true if the lock is set and false otherwise.
_check_lock(){
  _update_locks;
  local _lockname=$1;
  if [ ! $1 ] || [ -z "$1" ]; then
    error "Incorrect usage. No lock file parameter passed.";
    return 1;
  fi
  for i in "${!_locks[@]}"; do
    if [ "${i}" == "${_lockname}" ]; then
      # Lock exists
      return 0;
    fi
  done
  return 1;
}

# Refreshes the global lock list against the current lockfile directory
_update_locks() {
  _locks=();
  local _keyname;
  for i in "$(ls $_lock)"; do
    if [ -f "${_lock}/${i}" ] && [ ! "${i}" == ".gitignore" ]; then
      # A lockfile exists.
      _keyname=$(echo "${i}" | grep -o '^[^\.]*');
      _locks["$_keyname"]="$(cat "${_lock}/${i}")";
    fi
  done
  return 0;
}

# Lists the existing locks, and the corresponding process id associated with them
_list_locks() {
  _update_locks;
  for i in "${!_locks[@]}"; do
    echo "${_locks[$i]}: ${i}";
  done
  return 0;
}

# Echos the process id associated with a set lock if it exists.
_get_lock_process() {
  _update_locks;
  local _lockname=$1;
  if [ -z "${_lockname}" ]; then
    error "Incorrect usage at function [_get_lock_process]. No lock file parameter passed.";
    return 1;
  fi
  if _check_lock "${_lockname}"; then
    echo "${_locks["${_lockname}"]}";
  fi
  return 0;
}

# Prevents duplicate operations that are already running from proceeding.
# This is used on lengthy operations to prevent concurrent calls that would corrupt expected output.
_validate_lock() {
  _update_locks;
  local _lockname=$1;
  if [ -z "${_lockname}" ]; then
    fatal "Incorrect usage at function [_validate_lock]. No lock file parameter passed. Terminating execution to prevent cascading errors.";
  fi
  if _check_lock "${_lockname}"; then
    fatal "Lock already set for [${_c_yellow}${_lockname}${_c_red}] under process id [${_c_yellow}$(_get_lock_process "${_lockname}")${_c_red}]. Terminating execution.";
  fi
}
