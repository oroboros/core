# General purpose utilities for handling app interaction

# Verifies that the application integrity is sound
app_verify() {
  :
}

# Retrieves the application version
app_version() {
  local _return=0;
  local _app_detected_binary="$(app_binary)";
  _return=$?;
  if [ $_return -eq 0 ]; then
    echo "$(${_app_detected_binary} -v | grep Apache | cut -d'/' -f2 | cut -d' ' -f1)";
  fi
  return $_return;
}

# Returns the fully qualified path of the application front controller file
app_binary() {
  local _pwd="$(pwd)";
  if [ -f "${_pwd}/index.php" ]; then
    echo "${_pwd}/index.php";
  else
    error "No application index exists in directory [${_c_yellow}${_dir}${_c_orange}].";
    return 1;
  fi
  return 0;
}

# Returns the fully qualified path of the application shell accessor
app_shell() {
  local _pwd="$(pwd)";
  if [ -f "${_pwd}/app" ]; then
    echo "${_pwd}/app";
  elif [ -f "${_pwd}/oroboros" ] && [[ "${_dir}" = "${_oroboros_dir}" ]]; then
    echo "${_pwd}/oroboros";
  else
    error "No application shell exists in directory [${_c_yellow}${_dir}${_c_orange}].";
    return 1;
  fi
  return 0;
}

# Runs a command against the application binary
app_exec() {
  local _designation=0;
  local _app_exec_path="$1";
  local _app_exec_args="$2";
  local _app_exec_flags="$3";
  local _pwd="$(pwd)";
  cd "${_dir}";
  local _app="$(app_binary)";
  if ! [[ $? -eq 0 ]]; then
    return 1
  fi
  if [ -z "${_app_exec_command}" ]; then
    _app_exec_command="$(app_get_command)";
    _designation=$_designation+$?;
  fi
  if [ -z "${_app_exec_args}" ]; then
    _app_exec_args="$(app_get_arguments)";
    _designation=$_designation+$?;
  fi
  if [ -z "${_app_exec_flags}" ]; then
    _app_exec_flags="$(app_get_flags)";
    _designation=$_designation+$?;
  fi
  if [ -z "${_app_exec_path}" ]; then
    _app_exec_path="$(app_get_path)";
    _designation=$_designation+$?;
  fi
  if ! [[ $_designation -eq 0 ]]; then
    return 1;
  fi
  php "${_app}" ${_app_exec_path} ${_app_exec_args} ${_app_exec_flags};
  _designation=$?;
  cd "${_pwd}";
  return $_designation;
}

# Runs a command against the application shell
app_shell_exec() {
  local _designation=0;
  local _app_exec_command="$1";
  local _app_exec_args="$2";
  local _app_exec_flags="$3";
  local _pwd="$(pwd)";
  cd "${_dir}";
  local _app="$(app_shell)";
  if [ -z "${_app_exec_command}" ]; then
    _app_exec_command="$(app_get_command)";
    _designation=$_designation+$?;
  fi
  if [ -z "${_app_exec_args}" ]; then
    _app_exec_args="$(app_get_arguments)";
    _designation=$_designation+$?;
  fi
  if [ -z "${_app_exec_flags}" ]; then
    _app_exec_flags="$(app_get_flags)";
    _designation=$_designation+$?;
  fi
  if ! [[ $_designation -eq 0 ]]; then
    return 1;
  fi
#  info "Command: [${_c_yellow}${_app_command}${_c_cyan}]";
#  info "Arguments: [${_c_yellow}${_app_args}${_c_cyan}]";
#  info "Flags: [${_c_yellow}${_app_flags}${_c_cyan}]";
  "${_app}"${_app_exec_command}${_app_exec_args}${_app_exec_flags};
  _designation=$?;
  cd "${_pwd}";
  return $_designation;
}

# Configures the application.
app_config() {
  :
}

# Gets the declared name of the application
app_get_name() {
  local _designation=0;
  local _command="app";
  local _args="name";
  local _value="$(app_exec "${_command}" "${_args}")";
  if [ ! $? -eq 0 ]; then
    error "Application not reachable in [${_c_yellow}$(pwd)${_c_orange}].";
    return 1;
  fi
  echo "$(remove_colors ${_value})" | sed -e 's/\x1b\[[0-9;]*[a-zA-Z]//g';
  return $_designation;
}

# Gets the declared Psr-4 vendor/package namespace of the application
app_get_namespace() {
  local _designation=0;
  local _command="app";
  local _args="namespace";
  local _value="$(app_exec "${_command}" "${_args}" | sed -r 's/\\/-/g')";
  if [ ! $? -eq 0 ]; then
    error "Application not reachable in [${_c_yellow}$(pwd)${_c_orange}].";
    return 1;
  fi
  remove_colors "${_value}";
  return $_designation;
}

# Gets the declared runtime environment of the application
# (eg: local, dev, staging, production, etc)
app_get_environment() {
  local _designation=0;
  local _command="app";
  local _args="environment";
  local _value="$(app_exec "${_command}" "${_args}")";
  if [ ! $? -eq 0 ]; then
    error "Application not reachable in [${_c_yellow}$(pwd)${_c_orange}].";
    return 1;
  fi
  echo "$(remove_colors ${_value})" | sed -e 's/\x1b\[[0-9;]*[a-zA-Z]//g';
  return $_designation;
}

# Gets the absolute filepath of the application
app_get_directory() {
  local _designation=0;
  local _command="app";
  local _args="path";
  local _value="$(app_exec "${_command}" "${_args}")";
  if [ ! $? -eq 0 ]; then
    error "Application not reachable in [${_c_yellow}$(pwd)${_c_orange}].";
    return 1;
  fi
  echo "$(remove_colors ${_value})";
  return $_designation;
}

# Gets the version number of the application
app_get_version() {
  local _designation=0;
  local _command="app";
  local _args="version";
  local _value="$(app_exec "${_command}" "${_args}")";
  if [ ! $? -eq 0 ]; then
    error "Application not reachable in [${_c_yellow}$(pwd)${_c_orange}].";
    return 1;
  fi
  echo "$(remove_colors ${_value})";
  return $_designation;
}

# Returns the command designated to be passed to the application if a app_exec call is made.
app_get_command() {
  local _app_command_cmd='';
  if exists $_command in _cmd; then
    # Recognized command.
    _app_command_cmd=" ${_cmd[${_command}]}";
  else
    # Unrecognized command, let the app handle it.
    _app_command_cmd=" ${_command}";
  fi
  echo "${_app_command_cmd}";
  return 0;
}

# Returns the path designated to be passed to the application if a app_exec call is made.
app_get_path() {
  local _app_command_path='';
  local _key="";
  for i in "${_app_path[@]}"
  do
    _key="$(echo -e "$(remove_colors ${i})" | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//')";
    _app_command_path="${_app_command_path} ${_key}";
    _key="";
  done
  echo "${_app_command_path}";
  return 0;
}

# Returns the arguments designated to be passed to the application if a app_exec call is made.
app_get_arguments() {
  local _app_command_args='';
  local _key="";
  local _value="";
  for i in "${!_app_args[@]}"
  do
    _key="$(echo -e "$(remove_colors ${i})" | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//')";
    _value="$(echo -e "${_app_args[$i]}")";
    _app_command_args="${_app_command_args} ${_key}=${_value}";
    _key="";
    _value="";
  done
  echo "${_app_command_args}";
  return 0;
}

# Returns the flags designated to be passed to the application if a app_exec call is made.
app_get_flags() {
  local _app_command_flags='';
  for i in "${_app_flagset[@]}"
  do
    _key="$(echo -e "$(remove_colors ${i})" | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//')";
    _app_command_flags="${_app_command_flags} ${_key}";
  done
  echo "${_app_command_flags}";
  return 0;
}

# Registers the application path with the base platform
app_register() {
  local _designation=0;
  local _pwd="$(pwd)";
  local _path="${1}";
  if [ -z "${_path}" ]; then
    error "No path parameter supplied.";
    return 1;
  fi
  if [ ! -d "${_path}" ]; then
    error "Provided path parameter [${_c_yellow}${_path}${_c_orange}] is not a directory.";
    return 1;
  fi
  cd "${_path}";
  :
  cd "${_pwd}";
  return $_designation;
}

# Runs all validation functions on the application. Returns 0 if all pass, 1 or higher if any fail.
app_validate() {
  local _designation=0;
  local _pwd="$(pwd)";
  local _path="${1}";
  if [ -z "${_path}" ]; then
    error "No path parameter supplied.";
    return 1;
  fi
  if [ ! -d "${_path}" ]; then
    error "Provided path parameter [${_c_yellow}${_path}${_c_orange}] is not a directory.";
    return 1;
  fi
  cd "${_path}";
  _app_validate_environment "${_path}";
  if [ ! $? -eq 0 ]; then
    warning "Application environment is not valid.";
    ((_designation++));
  fi
  _app_validate_registration "${_path}";
  if [ ! $? -eq 0 ]; then
    warning "Application registration is not valid.";
    ((_designation++));
  fi
  _app_validate_resources "${_path}";
  if [ ! $? -eq 0 ]; then
    warning "Application resources are not valid.";
    ((_designation++));
  fi
  _app_validate_installation "${_path}";
  if [ ! $? -eq 0 ]; then
    warning "Application installation is not valid.";
    ((_designation++));
  fi
  _app_validate_dns "${_path}";
  if [ ! $? -eq 0 ]; then
    warning "Application installation is not valid.";
    ((_designation++));
  fi
  cd "${_pwd}";
  return $_designation;
}

# Validates that the .env file exists, and all expected keys are present
_app_validate_environment() {
  local _designation=0;
  local _pwd="$(pwd)";
  local _path="${1}";
  local _env="${_path}/.env";
  local _expected="";
  if [ -z "${_path}" ]; then
    error "No path parameter supplied.";
    return 1;
  fi
  if [ ! -d "${_path}" ]; then
    error "Provided path parameter [${_c_yellow}${_path}${_c_orange}] is not a directory.";
    return 1;
  fi
  cd "${_path}";
  if [ ! -f "${_env}" ]; then
    # No environment file exists in the application
    error "No environment file exists in [${_c_yellow}${_path}${_c_orange}].";
    return 1;
  fi
  _expected="APP_DOMAIN";
  grep -Fxq "${_expected}=" "${_env}" >/dev/null 2>&1;
  if [ ! $? -eq 0 ]; then
    debug "Expected environment parameter [${_c_yellow}${_expected}${_c_gray}] does not exist in [${_c_yellow}${_env}${_c_gray}].";
    ((_designation++));
  fi
  _expected="OROBOROS_CORE_ENVIRONMENT";
  grep -Fxq "${_expected}=" "${_env}" >/dev/null 2>&1;
  if [ ! $? -eq 0 ]; then
    debug "Expected environment parameter [${_c_yellow}${_expected}${_c_gray}] does not exist in [${_c_yellow}${_env}${_c_gray}].";
    ((_designation++));
  fi
  _expected="OROBOROS_CORE_USER_WEBSERVER";
  grep -Fxq "${_expected}=" "${_env}" >/dev/null 2>&1;
  if [ ! $? -eq 0 ]; then
    debug "Expected environment parameter [${_c_yellow}${_expected}${_c_gray}] does not exist in [${_c_yellow}${_env}${_c_gray}].";
    ((_designation++));
  fi
  _expected="OROBOROS_CORE_USER_SHELL";
  grep -Fxq "${_expected}=" "${_env}" >/dev/null 2>&1;
  if [ ! $? -eq 0 ]; then
    debug "Expected environment parameter [${_c_yellow}${_expected}${_c_gray}] does not exist in [${_c_yellow}${_env}${_c_gray}].";
    ((_designation++));
  fi
  cd "${_pwd}";
  return $_designation;
}

# Validates that the application path has been correctly registered with the base platform
_app_validate_registration() {
  local _designation=0;
  if [ -z "${_current_app}" ]; then
    error "The current directory [${_c_yellow}$(pwd)${_c_orange}] is not a registered application. Please register it with [${_c_yellow}oroboros -a -r${_c_orange}].";
    debug "Registration is required for automatic updates and daemonization to occur correctly.";
    debug "Registration will occur automatically for scaffolded applications.";
    debug "Applications that have been imported, cloned, or moved from their original location need to be manually registered.";
    return 1;
  else
    info "Currently operating on registered application [${_c_yellow}${_current_app}${_c_cyan}].";
  fi
  return $_designation;
}

# Validates that the application expected resources are present
_app_validate_resources() {
  local _designation=0;
  local _pwd="$(pwd)";
  local _path="${1}";
  if [ -z "${_path}" ]; then
    error "No path parameter supplied.";
    return 1;
  fi
  if [ ! -d "${_path}" ]; then
    error "Provided path parameter [${_c_yellow}${_path}${_c_orange}] is not a directory.";
    return 1;
  fi
  cd "${_path}";
  :
  cd "${_pwd}";
  return $_designation;
}

# Validates that the application is correctly installed
_app_validate_installation() {
  local _designation=0;
  local _pwd="$(pwd)";
  local _path="${1}";
  if [ -z "${_path}" ]; then
    error "No path parameter supplied.";
    return 1;
  fi
  if [ ! -d "${_path}" ]; then
    error "Provided path parameter [${_c_yellow}${_path}${_c_orange}] is not a directory.";
    return 1;
  fi
  cd "${_path}";
  :
  cd "${_pwd}";
  return $_designation;
}

# Checks if the front page of the host can be reached via http or https
_app_validate_dns() {
  local _designation=0;
  local _insecure=0;
  local _secure=0;
  local _hostname="$(_app_get_hostname "${_path}")";
  if [ ! $? -eq 0 ]; then
    error "Could not determine hostname.";
    return 1;
  fi
  if curl -s --head  --request GET "http://${_hostname}/" | grep "200 OK" > /dev/null; then 
    # Valid, non-ssl
    debug "Non-secure verification of [${_c_yellow}http://${_hostname}/${_c_gray}] was successful (non-ssl).";
    _insecure=0;
  else
    # Not valid, non-ssl
    debug "Non-secure verification of [${_c_yellow}http://${_hostname}/${_c_gray}] failed (non-ssl).";
    _insecure=1;
  fi
  if curl -s --head  --request GET "https://${_hostname}/" | grep "200 OK" > /dev/null; then 
    # Valid, with ssl
    debug "Secure verification of [${_c_yellow}https://${_hostname}/${_c_gray}] was successful (ssl).";
    _secure=0;
  else
    # Not valid, with ssl
    debug "Secure verification of [${_c_yellow}https://${_hostname}/${_c_gray}] failed (ssl).";
    _secure=1;
  fi
  if [[ ! $_secure -eq 0 ]] && [[ ! $_insecure -eq 0 ]]; then
    # Site is not reachable
    debug "Application at [${_c_yellow}https://${_hostname}/${_c_gray}] is not reachable via http.";
    return 1;
  fi
  return $_designation;
}

# Returns the hostname of the application declared in the .env file
_app_get_hostname() {
  local _designation=0;
  local _path="${1}";
  local _env="${_path}/.env";
  local _hostname="";
  if [ -z "${_path}" ]; then
    error "No path parameter supplied.";
    return 1;
  fi
  if [ ! -d "${_path}" ]; then
    debug "Provided path parameter [${_c_yellow}${_path}${_c_gray}] is not a directory. Cannot obtain hostname.";
    return 1;
  fi
  cd "${_path}";
  if [ ! -f "${_env}" ]; then
    # No environment file exists in the application
    debug "No environment file exists in [${_c_yellow}${_path}${_c_gray}]. Cannot obtain hostname.";
    return 1;
  fi
  _hostname="$(cat .env | grep "APP_DOMAIN=" | cut -d'=' -f2)";
  if [ -z "${_hostname}" ]; then
    warning "No hostname is defined in the environment file at [${_c_yellow}${_env}${_c_gray}]. Cannot obtain hostname.";
    return 1;
  fi
  echo "${_hostname}";
  return 0;
}