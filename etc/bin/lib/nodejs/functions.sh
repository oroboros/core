# General purpose utilities for handling nodejs interaction

nodejs_verify() {
  :
}

nodejs_version() {
  local _return=0;
  local _nodejs_detected_binary="$(nodejs_binary)";
  local _nodejs_detected_version="";
  _return=$?;
  if [ $_return -eq 0 ]; then
    _nodejs_detected_version="$(node -v)";
    echo "${_nodejs_detected_version:1}";
  fi
  return $_return;
}

nodejs_binary() {
  local _nodejs_detected_binary="$(which node)";
  if [ "${_nodejs_detected_binary}" == "" ]; then
    return 1;
  fi
  echo "${_nodejs_detected_binary}";
  return 0;
}

nodejs_config() {
  :
}

nodejs_lint() {
  :
}

nodejs_execute() {
  :
}

nodejs_run() {
  :
}

