#!/bin/bash

# Internal functions for the bash library.

# Example: if [ exists foo in bar ]; then ... fi # Evaluates on true
# Example: if [ ! exists foo in bar ]; then ... fi # Evaluates on false
exists(){
  if [ "$2" != in ]; then
    error "Missing parameter. Correct usage: exists {key} in {array}";
    return
  fi   
  eval '[ ${'$3'[$1]+muahaha} ]'; # Maniacal laughter evaluates to true.
}

# Use for boolean evaluation of whether an array value exists
# Example: if in_array array "foo"; then ... fi # Evaluates on true
# Example: if ! in_array array "foo"; then ... fi # Evaluates on false
in_array() {
    local haystack=${1}[@]
    local needle=${2}
    for i in ${!haystack}; do
        if [[ ${i} == ${needle} ]]; then
            return 0
        fi
    done
    return 1
}

### Param Handling ###
# Params designate which top level module to call.
# They are passed as shorthand flags: -h
# Or verbosely: --help
# Not all params have shorthand, particularly if they would
# collide with other params and create ambiguous commands.
######################

# Checks if a param exists, returns boolean (true if it is set, false if not)
has_param(){
  exists $1 in _params;
  return $?;
}

# Echos the value of the given given param key, if it was defined.
get_param() {
  if has_param $1; then
    echo ${_params[$1]};
  fi
}

## Flag Handling ###
# Flags designate sub-commands within the realm of a top level parameter.
# They have identical form and structure to params, but occur
# after the top level parameter in the call.
# They are likewise called as shorthand flags: -h
# Or verbosely: --help
# Flags are dependent on which module was called. Each module should have
# at least a help flag that lists its other available flags.
####################

# Checks if a flag exists, returns boolean (true if it is set, false if not)
has_flag(){
  local key=$1;
  if in_array _flagset $key; then
    return 0;
  else
    return 1;
  fi;
}

### Argument Handling ###
# Arguments are values passed in from the command prompt to be
# directly evaluated as values during an operation.
# They are passed in the following format:
# -key="value"
#########################

# Extracts the key from an equal operator argument passed, removing dashes and the equal separator.
extract_arg_key() {
  while IFS='=' read -ra ADDR; do
    for i in "${ADDR[@]}"; do
      echo "${i}";
      break;
      done
    break
  done <<< "$1"
  return 0;
}

# Extracts the value from an equal operator argument passed, removing the containing quotes if present.
extract_arg_value() {
  while IFS='=' read -r ADDR; do
    local temp="";
    temp="$(echo ${ADDR%\"} | cut -d'=' -f2)";
  done <<< "$1"
  echo "${temp}";
  return 0;
}

# Checks if an argument has been passed.
# Returns true if so, false otherwise
check_arg() {
  local has_arg=1;
  for i in "${!_args[@]}"
  do
    if [ "$i" == "$1" ]; then
      return 0;
    fi
  done
  return 1;
}

# Sets an application argument key and value.
# If the provided argument key has not been passed, will throw an error.
set_arg() {
  local _key="$1";
  local _value="$2";
  if [[ -z $_key ]]; then
    error "Argument key is not defined.";
    return 1;
  fi
  if [[ -z $_value ]]; then
    error "Argument value is not defined for key [${_c_yellow}${_key}${_c_orange}].";
    return 1;
  fi
  _args[${_key}]="${_value}";
  return 0;
}

# Gets the value for the provided argument key.
# If the provided argument key has not been passed, will throw an error.
get_arg() {
  check_arg $1;
  local check=$?;
  if [ $check -eq 1 ]; then
    error "Requested argument key [$1] is not defined.";
    return 1;
  fi
  echo "${_args[$1]}";
}

# Checks if an argument has been passed for the application.
# Returns true if so, false otherwise
check_app_arg() {
  local has_arg=1;
  for i in "${!_app_args[@]}"
  do
    if [ "$i" == "$1" ]; then
      return 0;
    fi
  done
  return 1;
}

# Sets an application argument key and value.
# If the provided argument key has not been passed, will throw an error.
set_app_arg() {
  local _key="$1";
  local _value="$2";
  if [[ -z $_key ]]; then
    error "Application argument key is not defined.";
    return 1;
  fi
  if [[ -z $_value ]]; then
    error "Application argument value is not defined for key [${_c_yellow}${_key}${_c_orange}].";
    return 1;
  fi
  # App arguments must be url encoded to maintain compatibility with the Psr-7 spec without being mutated
  _value="$(urlencode "${_value}")";
  _app_args[${_key}]="${_value}";
  return 0;
}

# Gets the value for the provided application argument key.
# If the provided argument key has not been passed, will throw an error.
get_app_arg() {
  check_app_arg $1;
  local check=$?;
  if [ $check -eq 1 ]; then
    error "Requested application argument key [$1] is not defined.";
    return 1;
  fi
  echo "${_args[$1]}";
}


get_command() {
  local _command_cmd='';
  if exists $_command in _cmd; then
    # Recognized command.
    _command_cmd=" ${_cmd[${_command}]}";
  else
    # Unrecognized command, let the app handle it.
    _command_cmd=" ${_command}";
  fi
  echo "${_command_cmd}";
  return 0;
}

get_arguments() {
  local _command_args='';
  local _key="";
  local _value="";
  for i in "${!_args[@]}"
  do
    _key="$(echo -e "$(remove_colors ${i})" | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//')";
    _value="$(echo -e "${_args[$i]}")";
    _command_args="${_command_args} ${_key}=${_value}";
    _key="";
    _value="";
  done
  echo "${_command_args}";
  return 0;
}

get_flags() {
  local _command_flags='';
  for i in "${_flagset[@]}"
  do
    echo "${i}";
    _key="$(echo -e "$(remove_colors ${i})" | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//')";
    _command_flags="${_command_flags} ${_key}";
  done
  echo "${_command_flags}";
  return 0;
}


# Returns a boolean determination as to whether the help menu for the current command was requested
is_help(){
  if $(has_flag "-h") || $(has_flag "--help"); then
    return 0;
  fi
  return 1;
}

# Prints a timestamp (YYY-MM-DD)
get_timestamp() {
  date +%F;
}

### Utility Methods ###
# Provides functionality for common operations
# required in most/all modules
#######################

# Returns true if a directory is empty, false otherwise.
# Throws a warning if the directory does not exist, but returns true (non-existent directories do not contain files).
directory_empty() {
  local _dirname=$1;
  local _is_empty=0;
  if [ ! -d "${_dirname}" ]; then
    # Directory does not exist
    warning "Provided directory [${_c_clear}${_dirname}${_c_yellow}] does not exist.";
    return 0;
  fi
  if [ "$(ls -A "${_dirname}")" ]; then
    _is_empty=1;
  fi
  return $_is_empty;
}

### Custom Configuration ###
# Provides functionality to load custom environment-specific
# configurations in place of the default values.
# These override defaults, and are in turn
# overridden by explicitly passed arguments.
############################

# Retrieves the location of the custom configuration directory, or where it should be if it does not exist.
get_custom_config_directory() {
  local _config_directory="${_var['homedir']}/.oroboros";
  echo "${_config_directory}";
  return 0;
}

# Creates the custom config directory if it does not already exist.
create_custom_config_directory() {
  local _config_directory=$(get_custom_config_directory);
  if [ ! -d "${_config_directory}" ]; then
    debug "Generating base level custom config directory at [${_config_directory}].";
    mkdir "${_config_directory}";
  fi
  return 0;
}

# Creates the custom config subdirectory if it does not already exist.
create_custom_config_subdirectory() {
  local _config_directory="$(get_custom_config_directory)";
  local _config_subdirectory="${_config_directory}/${1}";
  if [ -z "${1}" ]; then
    error "No config subdirectory argument provided.";
    return 1;
  fi
  if [ ! -d "${_config_directory}" ]; then
    create_custom_config_directory;
  fi
  if [ ! -d "${_config_subdirectory}" ]; then
    debug "Generating custom config subdirectory at [${_config_subdirectory}].";
    mkdir "${_config_subdirectory}";
  fi
  return 0;
}

# Loads custom config variables if present
load_custom_config() {
  local _file=$1;
  local _cfg="${_var["homedir"]}/.oroboros";
  if [ -f "${_cfg}/${_file}" ]; then
    debug "Loading custom configuration for [${_c_yellow}${_file}${_c_gray}].";
    source "${_cfg}/${_file}";
  else
    debug "No custom config overrides available for [${_c_yellow}${_file}${_c_gray}].";
  fi
  return 0;
}

# Loads custom config variables from a subdirectory config if present
load_custom_subdirectory_config() {
  if [ -z "${1}" ]; then
    error "No subdirectory config argument provided.";
    return 1;
  fi
  local _path="$(get_custom_config_directory)/${1}";
  local _directory="$(dirname ${_path})";
  local _file="$(basename ${_path})";
  if [ -f "${_path}" ]; then
    debug "Loading custom subdirectory configuration for [${_c_yellow}${_file}${_c_gray}] from location at [${_c_yellow}${_path}${_c_gray}].";
    source "${_path}";
  else
    debug "No custom config overrides available for [${_c_yellow}${_file}${_c_gray}] at [${_c_yellow}${_path}${_c_gray}].";
  fi
  return 0;
}

# Creates a custom config file if it does not already exist
create_custom_config() {
  create_custom_config_directory;
  local _file="$(get_custom_config_directory)/$1";
  if [ ! -f "${_file}" ]; then
    debug "Creating override config file for [${_c_yellow}${1}${_c_gray}] at [${_c_yellow}${_file}${_c_gray}].";
    touch "${_file}";
  fi
  return 0;
}

# Creates a custom subdirectory config file if it does not already exist
create_custom_subdirectory_config() {
  if [ -z "${1}" ]; then
    error "No config subdirectory file argument provided.";
    return 1;
  fi
  local _file="$(get_custom_config_directory)/$1";
  local _path="$(dirname ${_file})";
  create_custom_config_directory;
  create_custom_config_subdirectory "$(dirname ${1})";
  if [ ! $? -eq 0 ]; then
    error "failed to create custom config subdirectory at [${_c_yellow}${_path}${_c_orange}].";
    return 1;
  fi
  if [ ! -f "${_file}" ]; then
    debug "Creating subdirectory config file for [${_c_yellow}${1}${_c_gray}] at [${_c_yellow}${_file}${_c_gray}].";
    touch "${_file}";
  fi
  return 0;
}

# Updates a key in a custom config if it already exists, or writes it if it does not.
update_custom_config() {
  create_custom_config "$1";
  local _file="$(get_custom_config_directory)/$1";
  local _base_key="_$1";
  local _key=$2;
  local _value=$3;
  local _sed_string='s,'"${_base_key}"'["'"$(remove_colors ${_key})"'"]},'"${_value}"',g';
#  echo 'sed -i '"$(remove_colors ${_sed_string})"' '"${_file}"; exit;
  if [ ! $(cat "${_file}" | grep -Fc "${_base_key}"'["'"${_key}"'"]') -eq 0 ]; then
    # Value exists. Update with sed
    debug "Updating existing config option [${_c_yellow}${_key}${_c_gray}] with value [${_c_yellow}${_value}${_c_gray}]";
#    sed -i "$(remove_colors ${_sed_string})" "${_file}" > "${_file}.bak" && mv "${_file}.bak" "${_file}";
    sed -i "$(remove_colors ${_sed_string})" "${_file}" 2>/dev/null;
  else
    # Value does not exist. Append.
    debug "Appending new config option [${_c_yellow}${_key}${_c_gray}] with value [${_c_yellow}${_value}${_c_gray}]";
    echo "${_base_key}"'["'"$(remove_colors ${_key})"'"]="'"$(remove_colors ${_value})"'";' >> "${_file}";
  fi
  return 0;
}

# Updates a key in a custom config if it already exists, or writes it if it does not.
update_custom_subdirectory_config() {
  create_custom_subdirectory_config "$1";
  local _file="$(get_custom_config_directory)/$1";
  local _path="$(dirname ${_file})";
  local _filename="$(basename ${1})";
  local _base_key="_$(dirname ${1})_${_filename}";
  local _key=$2;
  local _value=$3;
  local _sed_string='s,'"${_base_key}"'["'"${_key}"'"]},'"${_value}"',g';
#  echo 'sed -i '"$(remove_colors ${_sed_string})"' '"${_file}"; exit;
  if [ ! $(cat "${_file}" | grep -Fc "${_base_key}"'["'"$(remove_colors ${_key})"'"]') -eq 0 ]; then
    # Value exists. Update with sed
    debug "Updating existing config option [${_c_yellow}${_key}${_c_gray}] with value [${_c_yellow}${_value}${_c_gray}]";
#    sed -i "$(remove_colors ${_sed_string})" "${_file}" > "${_file}.bak" && mv "${_file}.bak" "${_file}";
    sed -i "$(remove_colors ${_sed_string})" "${_file}" 2>/dev/null;
  else
    # Value does not exist. Append.
    debug "Appending new config option [${_c_yellow}${_key}${_c_gray}] with value [${_c_yellow}${_value}${_c_gray}]";
    echo "${_base_key}"'["'"$(remove_colors ${_key})"'"]="'"$(remove_colors ${_value})"'";' >> "${_file}";
  fi
  return 0;
}

# Escapes sed characters
escape_sed_chars() {
  local _string=$1;
  local _escaped_string="";
  _escaped_string = $(sed -e 's/[\/&]/\\&/g');
  echo "${_escaped_string}";
  return 0;
}

# Url encodes a string
urlencode() {
  local length="${#1}"
  for (( i = 0; i < length; i++ )); do
    local c="${1:i:1}"
    case $c in
      [a-zA-Z0-9.~_-]) printf "$c" ;;
      *) printf '%s' "$c" | xxd -p -c1 |
      while read c; do printf '%%%s' "$c"; done ;;
    esac
  done
  return 0;
}

# Url decodes a string
urldecode() {
  local url_encoded="${1//+/ }";
  printf '%b' "${url_encoded//%/\\x}";
  return 0;
}

# Removes all shell color codes and formatting
remove_colors() {
  local _string="${1}";
  if [ -z "${_string}" ]; then
    return 1;
  fi
  echo "${_string}" | sed -r 's/\[([0-9]{1,2}([0-9]{1,2})?)?[m]{1}//g' | sed -r "s/[\033|\x1b]//g" | sed -r "s/x1B([0-9]{0,2}m){0,1}//g";
  return 0;
}

function strip_escape_codes() {
    local output="";
    local char="";
    local within_code=1;
    local input="${1//\"/\\\"}" output="" i char within_code=0;
    for ((i=0; i < ${#input}; ++i)); do
        char="${input:i:1}";                    # get current character
        if (( ${within_code} == 1 )); then      # if we're currently within an escape code, check if end of
            case "${char}" in                   # code is reached, i.e. if current character is a letter
                [a-zA-Z]) within_code=0 ;;      # we're no longer within an escape code
            esac
            continue;
        fi
        if [[ "${char}" == $'\e' ]]; then       # if current character is '\e', we've reached an escape code
            within_code=1;                      # now we're within an escape code
            continue;
        fi
        output+="${char}";                      # if none of the above applies, add current character to output
    done
    echo "${output}";
}

# Returns a designation of whether two directories match,
# regardless of whether they have the same listed absolute path.
# Always returns false and raises an error if less than two parameters are passed.
# Always returns false and raises an error if either parameter supplied is not a directory.
directories_match() {
  local _from="$(get_absolute_path ${1})";
  local _to="$(get_absolute_path ${2})";
  if [ -z "${_from}" ]; then
    error "No base directory provided.";
    return 1;
  fi
  if [ ! -d "${_from}" ]; then
    error "Provided base directory is not a directory.";
    return 1;
  fi
  if [ -z "${_to}" ]; then
    error "No comparison directory provided.";
    return 1;
  fi
  if [ ! -d "${_to}" ]; then
    error "Provided comparison directory is not a directory.";
    return 1;
  fi
  diff -r "${_from}" "${_to}" >/dev/null 2>&1;
  return $?;
}

# Gets the absolute filepath of a given directory,
# removing symlink references and returning the
# fully qualified absolute path
get_absolute_path() {
  local _source="${1}";
  local _target="";
  local _path="";
  if [ -z "${_source}" ]; then
    error "No directory path provided.";
    return 1;
  fi
  while [ -h "$_source" ]; do # resolve $_source until the file is no longer a symlink
    _target="$(readlink "$_source")"
    if [[ $_target == /* ]]; then
      _source="$_target"
    else
      _path="$( dirname "$_source" )"
      _source="$_path/$_target" # if $_source was a relative symlink, we need to resolve it relative to the path where the symlink file was located
    fi
  done
  echo "${_source}";
  return 0;
}