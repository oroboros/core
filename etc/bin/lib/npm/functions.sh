# General purpose utilities for handling npm interaction


npm_verify() {
  :
}

npm_version() {
  local _return=0;
  local _npm_detected_binary="$(npm_binary)";
  _return=$?;
  if [ $_return -eq 0 ]; then
    echo "$(npm -v)";
  fi
  return $_return;
}

npm_binary() {
  local _npm_detected_binary="$(which npm)";
  if [ "${_npm_detected_binary}" == "" ]; then
    return 1;
  fi
  echo "${_npm_detected_binary}";
  return 0;
}

npm_config() {
  :
}

npm_install() {
  local _designation=0;
  local _pwd="$(pwd)";
  local _install_directory="$1";
  local _install_node_modules="${_install_directory}/node_modules";
  local _install_package_lock="${_install_directory}/package-lock.json";
  local _npm="$(npm_binary)";
  local _cmd="";
  if [ ! -d "${_install_directory}" ]; then
    error "Provided NPM install path [${_c_yellow}${_install_directory}${_c_orange}] is not a directory.";
    return 1;
  fi
  cd "${_install_directory}";
  if [ -f "${_install_package_lock}" ]; then
    debug "Flushing old package lock file at [${_c_yellow}${_install_package_lock}${_c_gray}].";
    rm "${_install_package_lock}";
  fi
  if [ -d "${_install_node_modules}" ]; then
    debug "Flushing old node modules folder at [${_c_yellow}${_install_node_modules}${_c_gray}].";
    rm -rf "${_install_node_modules}";
  fi
  debug "Running npm install command.";
  if has_flag '--debug'; then
    _cmd="${_npm} install";
    $_cmd;
    _cmd="${_npm} install --only=dev";
    $_cmd;
  else
    _cmd="${_npm} install$(has_flag '--debug' || echo " --silent --no-progress")";
    $_cmd >/dev/null 2>&1;
    _cmd="${_npm} install --only=dev$(has_flag '--debug' || echo " --silent --no-progress")";
    $_cmd >/dev/null 2>&1;
  fi
  cd "${_pwd}";
  return $_designation;
}