## This file defines the parameter mapping for the config module.
#
## Commands
#_cmd["-d"]="_config_index";
#_cmd["--config"]="_config_index";
#_cmd["--config"]="_config_config";
#
## Flags
#_flags+=("-b");
#_flags+=("--backup");
#_flags+=("-c");
#_flags+=("--commit");
#_flags+=("-r");
#_flags+=("--rollback");
#_flags+=("--list-archives");
#_flags+=("--list-versions");
