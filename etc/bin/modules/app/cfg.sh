## This file controls persistent configurations
declare -A _register_apps=();
_key="${_dir}/.env";
if [ ! -f "${_key}" ]; then
  # No application environment defined
  debug "No application environment file exists.";
  declare -r _app_domain="";
  declare -r _app_runtime_environment="";
  declare -r _app_user_webserver="";
  declare -r _app_user_group_webserver="";
  declare -r _app_user_shell="";
  declare -r _app_user_group_shell="";
else
  # Import application environment keys
  source "${_key}";
  debug "Importing local application environment from [${_c_yellow}${_key}${_c_gray}].";
  # Check expected keys
  _key="APP_DOMAIN";
  if [[ "${!_key}" = "" ]]; then
    debug "No application domain is defined.";
    declare -r _app_domain="";
  else
    declare -r _app_domain="${!_key}";
  fi
  _key="OROBOROS_CORE_ENVIRONMENT";
  if [[ "${!_key}" = "" ]]; then
    debug "No application runtime environment is defined.";
    declare -r _app_runtime_environment="";
  else
    declare -r _app_runtime_environment="${!_key}";
  fi
  _key="OROBOROS_CORE_USER_WEBSERVER";
  if [[ "${!_key}" = "" ]]; then
    debug "No application webserver username is defined.";
    declare -r _app_user_webserver="";
  else
    declare -r _app_user_webserver="${!_key}";
  fi
  _key="OROBOROS_CORE_USER_GROUP_WEBSERVER";
  if [[ "${!_key}" = "" ]]; then
    debug "No application webserver user group is defined.";
    declare -r _app_user_group_webserver="";
  else
    declare -r _app_user_group_webserver="${!_key}";
  fi
  _key="OROBOROS_CORE_USER_SHELL";
  if [[ "${!_key}" = "" ]]; then
    debug "No application shell username is defined.";
    declare -r _app_user_shell="";
  else
    declare -r _app_user_shell="${!_key}";
  fi
  _key="OROBOROS_CORE_USER_GROUP_SHELL";
  if [[ "${!_key}" = "" ]]; then
    debug "No application shell user group is defined.";
    declare -r _app_user_group_shell="";
  else
    declare -r _app_user_group_shell="${!_key}";
  fi
fi
create_custom_config 'app';
load_custom_config 'app';
create_custom_subdirectory_config 'register/apps';
load_custom_subdirectory_config 'register/apps';
for i in "${!_register_apps[@]}";
do
  if directories_match "${_register_apps[${i}]}" "${_dir}"; then
    declare -r _current_app="${i}";
    break;
  fi
done;
if [ -z "${_current_app+x}" ]; then
  declare -r _current_app="";
fi
