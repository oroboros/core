## This file defines the parameter mapping for the config module.
#
## Commands
_cmd["-a"]="_app_index";
_cmd["--application"]="_app_index";

# Flags
_flags+=("-v");
_flags+=("--verify");
_flags+=("-e");
_flags+=("--environment");
_flags+=("-r");
_flags+=("--register");
_flags+=("-s");
_flags+=("--status");
_flags+=("--config");
_flags+=("--check-debug");
_flags+=("--enable-debug");
_flags+=("--disable-debug");
_flags+=("--check-maintenance");
_flags+=("--enable-maintenance");
_flags+=("--disable-maintenance");
_flags+=("--test-registration");
_flags+=("--test-environment");
_flags+=("--test-updates");
_flags+=("--test-build");
_flags+=("--test-files");
_flags+=("--test-http");
_flags+=("--test-ssl");
_flags+=("--test-cli");
_flags+=("--test-database");
_flags+=("--test-redis");
_flags+=("--test-memcache");
_flags+=("--test-beanstalk");
_flags+=("--test-supervisor");
_flags+=("--test-logs");
_flags+=("--test-cache");
_flags+=("--test-uptime");
_flags+=("--test-load");
_flags+=("--test-disk");
_flags+=("--test-ports");
_flags+=("--test-memory");
_flags+=("--test-sitemaps");
_flags+=("--test-public");
_flags+=("--test-secure");