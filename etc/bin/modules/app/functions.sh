# Function declarations for the app module

# Control method for application management operations. Routes commands to the
# appropriate handler method.
# All methods passed to the application module call this method first, which aggregates sub-commands
# to various other functions listed here based on the sub-commands passed.

# ------------------------------------------------------------------------------
#                         Application Index Method
# ------------------------------------------------------------------------------
_app_index() {
  load_custom_config "app";
  if has_flag "-e" || has_flag "--environment"; then
    # Configures the application environment
    _app_environment;
  elif has_flag "-v" || has_flag "--verify"; then
    # Verifies the application
    _app_verify;
  elif has_flag "-s" || has_flag "--status"; then
    # Checks the application status
    _app_status;
  elif has_flag "-r" || has_flag "--register"; then
    # Checks registers an application
    _app_register;
  elif has_flag "--config"; then
    # Runs the application configuration
    _app_config;
  elif has_flag "--enable-debug"; then
    # Put the application into debug mode
    _app_debug_enable;
  elif has_flag "--disable-debug"; then
    # Take the application out of debug mode
    _app_debug_disable;
  elif has_flag "--enable-maintenance"; then
    # Put the application into maintenance mode
    _app_maintenance_enable;
  elif has_flag "--disable-maintenance"; then
    # Take the application out of maintenance mode
    _app_maintenance_disable;
  else
    # Display the application help menu index
    _app_help_index;
  fi
  return $?;
}

# ------------------------------------------------------------------------------
#                            Application Controllers
# ------------------------------------------------------------------------------

# Checks the status of the last application operation ran
_app_status() {
  if is_help; then
    _app_help_status;
  else
    error "This functionality is not yet implemented.";
    return 1;
  fi
}

# Registers an application with the base platform
_app_register() {
  if is_help; then
    _app_help_register;
  else
    local _config_name="register/apps";
    local _app_name="$(app_get_namespace | sed -e 's/\\/-/g')";
    if [ ! $? -eq 0 ]; then
      error "Could not determine application identifier.";
      return 1;
    fi
    local _app_path="$(app_get_directory)";
    if [ ! $? -eq 0 ]; then
      error "Could not determine application directory.";
      return 1;
    fi
    if [[ "${_app_name}" = "Oroboros_core" ]]; then
      error "Cannot register the core framework [${_c_yellow}${_app_name}${_c_orange}] with path [${_c_yellow}${_app_path}${_c_orange}].";
      return 1;
    fi
    debug "Registering application [${_c_yellow}"${_app_name}"${_c_gray}] with path [${c_yellow}${_app_path}${_c_gray}].";
    update_custom_subdirectory_config "${_config_name}" "${_app_name}" "${_app_path}";
    if [ ! $? -eq 0 ]; then
      error "Could not register application [${_c_yellow}"${_app_name}"${_c_orange}] with path [${_c_yellow}${_app_path}${_c_orange}].";
      return 1;
    fi
    info "Registered application [${_c_yellow}"${_app_name}"${_c_cyan}].";
  fi
}

# Sets the application runtime environment.
# (eg local, dev, production, etc)
_app_environment() {
  if is_help; then
    _app_help_environment;
  else
    error "This functionality is not yet implemented.";
    return 1;
  fi
}

# Verifies the environment and dependencies needed to run a clean application.
_app_verify() {
  if is_help; then
    _app_help_verify;
  else
    error "This functionality is not yet implemented.";
    return 1;
  fi
}

# Puts the application into maintenance mode if it is not currently in maintenance mode.
_app_maintenance_enable() {
  if is_help; then
    _app_help_maintenance;
  else
    error "This functionality is not yet implemented.";
    return 1;
  fi
}

# Removes the application from maintenance mode if it is currently in maintenance mode.
_app_maintenance_disable() {
  if is_help; then
    _app_help_maintenance;
  else
    error "This functionality is not yet implemented.";
    return 1;
  fi
}

# Puts the application into debug mode if it is not currently in debug mode.
# If the application runtime environment is local, dev, or testing,
# then this has no effect, as these environments always use debug mode explicitly.
_app_debug_enable() {
  if is_help; then
    _app_help_debug;
  else
    error "This functionality is not yet implemented.";
    return 1;
  fi
}

# Removes the application from debug mode if it is currently in debug mode.
# If the application runtime environment is local, dev, or testing,
# then this has no effect, as these environments always use debug mode explicitly.
_app_debug_disable() {
  if is_help; then
    _app_help_debug;
  else
    error "This functionality is not yet implemented.";
    return 1;
  fi
}

# Manually up the localized configuration for the application module.
# Use this where environments differ from the default.
# This only needs to be done one time, or when system settings change.
_app_config() {
  if is_help; then
    _app_help_config;
  else
    error "This functionality is not yet implemented.";
    return 1;
    local -A _cfg_path="$(get_custom_config_directory)/app";
    local -A _cfg=();
    local _prompt="";
    local _default="";
    create_custom_config "app";
    if has_flag "--interactive"; then
      clear;
      printf "${_f_bold}${_c_cyan}Application Configuration${_c_clear}\n";
    fi
    if check_arg "user"; then
      _cfg["database_root_user"]=$(get_arg "user");
    elif has_flag "--interactive"; then
      _default=$([ ! -z ${_app['database_root_user']} ] && echo ${_app['database_root_user']} || echo ${_app_defaults['database_root_user']});
      printf "Enter the database root user for running database migrations ( default ["$_c_yellow"${_default}"$_c_clear"] ).\n? ";
      read "_prompt";
      printf "\n";
      if [ -z "${_prompt}" ]; then
        _cfg["database_root_user"]="${_default}";
      else
        _cfg['database_root_user']="${_prompt}";
      fi
    fi
    if check_arg "password"; then
      _cfg["database_root_password"]=$(get_arg "password");
    elif has_flag "--interactive"; then
      _default=$([ ! -z ${_app['database_root_password']} ] && echo ${_app['database_root_password']} || echo ${_app_defaults['database_root_password']});
      printf "Enter the database root password for running database migrations ( default ["$_c_yellow"${_default}"$_c_clear"] )\n? ";
      read -s "_prompt";
      printf "\n";
      if [ -z "${_prompt}" ]; then
        _cfg['database_root_password']="${_default}";
      else
        _cfg['database_root_password']="${_prompt}";
      fi
    fi
    if check_arg "host"; then
      _cfg['database_root_host']=$(get_arg "host");
    elif has_flag "--interactive"; then
      _default=$([ ! -z ${_app['database_root_host']} ] && echo ${_app['database_root_host']} || echo ${_app_defaults['database_root_host']});
      printf "Enter the database hostname for running database migrations ( default ["$_c_yellow"${_default}"$_c_clear"] ).\n? ";
      read "_prompt";
      printf "\n";
      if [ -z "${_prompt}" ]; then
        _cfg['database_root_host']="${_default}";
      else
        _cfg['database_root_host']="${_prompt}";
      fi
    fi
    if check_arg "port"; then
      _cfg['database_root_port']=$(get_arg "port");
    elif has_flag "--interactive"; then
      _default=$([ ! -z ${_app['database_root_port']} ] && echo ${_app['database_root_port']} || echo ${_app_defaults['database_root_port']});
      printf "Enter the root database port for running database migrations ( default ["$_c_yellow"${_default}"$_c_clear"] ).\n? ";
      read "_prompt";
      printf "\n";
      if [ -z "${_prompt}" ]; then
        _cfg['database_root_port']="${_default}";
      else
        _cfg['database_root_port']="${_prompt}";
      fi
    fi
    if check_arg "database"; then
      _cfg['database_root_schema']=$(get_arg "database");
    elif has_flag "--interactive"; then
      _default=$([ ! -z ${_app['database_root_schema']} ] && echo ${_app['database_root_schema']} || echo ${_app_defaults['database_root_schema']});
      printf "Enter the default database schema name for running database migrations ( default ["$_c_yellow"${_default}"$_c_clear"] ).\n? ";
      read "_prompt";
      printf "\n";
      if [ -z "${_prompt}" ]; then
        _cfg['database_root_schema']="${_default}";
      else
        _cfg['database_root_schema']="${_prompt}";
      fi
    fi
    
    if check_arg "test-user"; then
      _cfg["test-user"]=$(get_arg "test-user");
    elif has_flag "--interactive"; then
      _default=$([ ! -z ${_app['database_test_user']} ] && echo ${_app['database_test_user']} || echo ${_app_defaults['database_test_user']});
      printf "Enter the unit testing database user for running tests on deployment ( default ["$_c_yellow"${_default}"$_c_clear"] ).\n? ";
      read "_prompt";
      printf "\n";
      if [ -z "${_prompt}" ]; then
        _cfg["database_test_user"]="${_default}";
      else
        _cfg['database_test_user']="${_prompt}";
      fi
    fi
    if check_arg "test-password"; then
      _cfg["database_test_password"]=$(get_arg "test-password");
    elif has_flag "--interactive"; then
      _default=$([ ! -z ${_app['database_test_password']} ] && echo ${_app['database_test_password']} || echo ${_app_defaults['database_test_password']});
      printf "Enter the unit testing database password for running tests on deployment ( default ["$_c_yellow"${_default}"$_c_clear"] )\n? ";
      read -s "_prompt";
      printf "\n";
      if [ -z "${_prompt}" ]; then
        _cfg['database_test_password']="${_default}";
      else
        _cfg['database_test_password']="${_prompt}";
      fi
    fi
    if check_arg "test-host"; then
      _cfg['database_test_host']=$(get_arg "test-host");
    elif has_flag "--interactive"; then
      _default=$([ ! -z ${_app['database_test_host']} ] && echo ${_app['database_test_host']} || echo ${_app_defaults['database_test_host']});
      printf "Enter the unit testing hostname for running tests on deployment ( default ["$_c_yellow"${_default}"$_c_clear"] ).\n? ";
      read "_prompt";
      printf "\n";
      if [ -z "${_prompt}" ]; then
        _cfg['database_test_host']="${_default}";
      else
        _cfg['database_test_host']="${_prompt}";
      fi
    fi
    if check_arg "test-password"; then
      _cfg['database_test_password']=$(get_arg "test-password");
    elif has_flag "--interactive"; then
      _default=$([ ! -z ${_app['database_test_port']} ] && echo ${_app['database_test_port']} || echo ${_app_defaults['database_test_port']});
      printf "Enter the unit testing database port ( default ["$_c_yellow"${_default}"$_c_clear"] ).\n? ";
      read "_prompt";
      printf "\n";
      if [ -z "${_prompt}" ]; then
        _cfg['database_test_password']="${_default}";
      else
        _cfg['database_test_password']="${_prompt}";
      fi
    fi
    if check_arg "test-database"; then
      _cfg['database_test_scheam']=$(get_arg "test-database");
    elif has_flag "--interactive"; then
      _default=$([ ! -z ${_app['database_test_schema']} ] && echo ${_app['database_test_schema']} || echo ${_app_defaults['database_test_schema']});
      printf "Enter the unit testing database name for running tests on deployment ( default ["$_c_yellow"${_default}"$_c_clear"] ).\n? ";
      read "_prompt";
      printf "\n";
      if [ -z "${_prompt}" ]; then
        _cfg['database_test_schema']="${_default}";
      else
        _cfg['database_test_schema']="${_prompt}";
      fi
    fi
    
    if check_arg "save-path"; then
      _cfg['database_save_path']=$(get_arg "save-path");
    elif has_flag "--interactive"; then
      _default=$([ ! -z ${_app['database_save_path']} ] && echo ${_app['database_save_path']} || echo ${_app_defaults['database_save_path']});
      printf "Enter the data backup path ( default ["$_c_yellow"${_default}"$_c_clear"] ).\n? ";
      read "_prompt";
      printf "\n";
      if [ -z "${_prompt}" ]; then
        _cfg['database_save_path']="${_default}";
      else
        _cfg['database_save_path']="${_prompt}";
      fi
    fi
    if check_arg "backup-space"; then
      _cfg['database_backup_minimum_disk_space']=$(get_arg "backup-space");
    elif has_flag "--interactive"; then
      _default=$([ ! -z ${_app['database_backup_minimum_disk_space']} ] && echo ${_app['database_backup_minimum_disk_space']} || echo ${_app_defaults['database_backup_minimum_disk_space']});
      printf "Enter the minimum required free bytes for data backups ( default ["$_c_yellow"${_default}"$_c_clear"] ).\n? ";
      read "_prompt";
      printf "\n";
      if [ -z "${_prompt}" ]; then
        _cfg['database_backup_minimum_disk_space']="${_default}";
      else
        _cfg['database_backup_minimum_disk_space']="${_prompt}";
      fi
    fi
    info "Updating application configuration.";
    for i in "${!_cfg[@]}"
    do
      update_custom_config "app" "$i" "${_cfg[$i]}";
    done
    debug "Custom database configuration saved to [${_c_yellow}$(get_custom_config_directory)/database${_c_gray}].";
    debug "Values present in this file will override existing defaults on subsequent operations.";
    debug "Additionally, explicitly passing arguments will override custom configuration options also.";
    debug "New configuration file values:\n\n$(cat "$(get_custom_config_directory)/app")\n";
    success "Database configuration updated.";
  fi
}

# ------------------------------------------------------------------------------
#                                Build Internals
# ------------------------------------------------------------------------------

_app_verify_environment() {
#  local _con="$(_app_get_connection_string)";
  local _method="";
  local verifier_key="";
  local _designation=0;
  local -A _required=();
  local -a _required_valid=();
  local -a _required_invalid=();
  local -A _optional=();
  local -a _optional_valid=();
  local -a _optional_invalid=();
  local _app_verified=0;
  local _app_optional_verified=0;
  local _os="$(os_distro)";
  _required['network']=0;
  _required['git']=0;
  _required['mysql']=0;
  _required['php']=0;
  _required['composer']=0;
  _required['nodejs']=0;
  _required['npm']=0;
  _required['webserver']=0;
  _optional['supervisor']=0;
  _optional['beanstalk']=0;
  _optional['redis']=0;
  info "Verifying the local environment and dependencies.";
  debug "Detected operating system [${_c_yellow}${_os}${_c_gray}]";
  debug "Verifying required dependencies...";
  for verifier_key in "${!_required[@]}"; do
    _method="_app_verify_${verifier_key}";
    $_method;
    if [ $? -eq 0 ]; then
      _required_valid+=("$verifier_key");
    else
      _required_invalid+=("$verifier_key");
    fi
  done;
  debug "Verifying optional dependencies...";
  for verifier_key in "${!_optional[@]}"; do
    _method="_app_verify_${verifier_key}";
    if ! [[ $(type -t $_method &2>/dev/null) = 'function' ]]; then
      error "Expected function [${_c_yellow}${_method}${_c_orange}] does not exist.";
      return 1;
    fi
    $_method;
    if [ $? -eq 0 ]; then
      _optional_valid+=("$verifier_key");
    else
      _optional_invalid+=("$verifier_key");
    fi
  done;

  debug "Generating verifier designation...";
  for verifier_key in "${_required_invalid[@]}"; do
    _app_verified=1;
  done;
  for verifier_key in "${_optional_invalid[@]}"; do
    _app_optional_verified=1;
  done;
  if [ ! $_app_verified -eq 0 ]; then
    error "There are blocking errors, application cannot proceed.";
    if [ ! $_app_optional_verified -eq 0 ]; then
      warning "Some optional dependencies are also invalid.";
    fi
  elif [ ! $_app_optional_verified -eq 0 ]; then
    warning "Some optional dependencies are invalid. The application can proceed,\n              but optional invalid functionality will be omitted in this environment.";
  fi
  for verifier_key in "${_required_invalid[@]}"; do
    error "Required dependency [${_c_yellow}${verifier_key}${_c_orange}] is not valid.";
  done;
  for verifier_key in "${_optional_invalid[@]}"; do
    warning "Optional dependency [${_c_clear}${verifier_key}${_c_yellow}] is not valid.";
  done;
  if [ $_app_verified -eq 0 ] && [ $_app_optional_verified -eq 0 ]; then
    success "All environment settings and dependencies are valid.";
  fi
  return $_app_verified;
}

_app_verify_application_environment() {
  local _pwd="$(pwd)";
  local _env_file="${_pwd}/.env";
  if [ ! -f "${_pwd}/.env" ]; then
    error "No application environment file detected at [${_c_yellow}${_env_file}${_c_orange}].";
    return 1;
  fi
  return 0;
}

# ------------------------------------------------------------------------------
#                               Help Documentation
# ------------------------------------------------------------------------------

# General application command set help menu
_app_help_index() {
  clear;
  printf "\n${_c_cyan}${_f_bold}Application Management Help\n${_c_clear}\n";
  echo '----------------------------------------------------------------------';
  printf "\n";
  printf "The application module configures an application.\n";
  printf "The following commands provide means to edit the application environment, validate configuration, and verify operation.\n\n";
  printf "A local environment preflight will always occur prior to calling any application commands.\n";
  printf "In the event baseline configuration is missing or invalid, you will be prompted to run initial or corrective configuration.\n";
  printf "Non-empty directories that are not Oroboros applications will simply return an error rather than forward to configuration..\n\n";
  printf "${_f_bold}Checks the runtime status of the application:\n${_c_clear}";
  printf "${_c_red}-a --status${_c_clear}\n\n";
  printf "${_f_bold}Sets up the environment parameters for the application:\n${_c_clear}";
  printf "${_c_red}-a --environment${_c_clear}\n\n";
  printf "${_f_bold}Sets up the local default configuration for the application setup:\n${_c_clear}";
  printf "${_c_red}-a --config${_c_clear}\n\n";
  printf "${_f_bold}Verifies the setup of the application:\n${_c_clear}";
  printf "${_c_red}-a --verify${_c_clear}\n\n";
  printf "${_f_bold}Puts the application into maintenance mode:\n${_c_clear}";
  printf "${_f_italic}Has no effect if the application is already in maintenance mode.\n${_c_clear}";
  printf "${_c_red}-a --enable-maintenance${_c_clear}\n\n";
  printf "${_f_bold}Takes the application out of maintenance mode:\n${_c_clear}";
  printf "${_f_italic}Has no effect if the application is not in maintenance mode.\n${_c_clear}";
  printf "${_c_red}-a --disable-maintenance${_c_clear}\n\n";
  printf "${_f_bold}Puts the application into debug mode, or takes it out of debug mode:\n${_c_clear}";
  printf "${_f_italic}Has no effect if the application is already in debug mode.\n${_c_clear}";
  printf "${_c_red}-a --enable-debug${_c_clear}\n\n";
  printf "${_f_bold}Takes the application out of debug mode:\n${_c_clear}";
  printf "${_f_italic}Has no effect if the application is not in debug mode.\n${_c_clear}";
  printf "${_f_italic}The application will always run in debug mode in development and test environments.\n${_c_clear}";
  printf "${_c_red}-a --disable-debug${_c_clear}\n\n";
  printf "\n";
  printf "${_c_yellow}${_f_bold}Flags${_c_clear}${_f_bold}(format ${_c_clear}${_c_red}--key${_c_clear}${_f_bold})${_c_clear}${_f_bold}${_c_yellow}:\n\n${_c_clear}";
  printf "${_c_red}--debug${_c_clear}                 Displays additional verbose information about underlying logic for debugging.\n";
  printf "${_c_red}--quiet${_c_clear}                 Hides all output except error messages.\n";
  printf "${_c_red}--silent${_c_clear}                Hides all output including error messages.\n";
  printf "\n";
  exit 0;
}

# Help menu for the application default configuration command
_app_help_register() {
  clear;
  printf "\n${_c_cyan}${_f_bold}Application Registration Help\n${_c_clear}\n";
  echo '----------------------------------------------------------------------';
  printf "\n";
  printf "The application registration operation binds an application path to the set of known applications in Oroboros.\n";
  printf "Registered applications can be automatically updated, monitored, and have extraneous jobs and server instances\n";
  printf "automatically started and persisted using the primary application health monitor.\n\n";
  printf "Additionally, registered applications can be be referenced from anywhere on disk\nusing oroboros commands, whereas unregistered applications can only be referenced from their own root directory.\n\n";
  printf "This command has no additional arguments or flags.\n\n";
  printf "\n";
  exit 0;
}

# Help menu for the application default configuration command
_app_help_config() {
  clear;
  printf "\n${_c_cyan}${_f_bold}Application Configuration Help\n${_c_clear}\n";
  echo '----------------------------------------------------------------------';
  printf "\n";
  printf "The application configuration operation defines the default parameters for running an application.\n";
  printf "This command will mirrors the settings that are used to generate the ${_c_yellow}.env${_c_clear} file, but instead sets\nthe defaults used for setup of further applications.\n";
  printf "Any additional applications created will use these defaults initially,\nuntil their individual configuration is set.\n";
  printf "Applications use the default config from ${_f_italic}when they are initially set up${_c_clear}.\nThis does not automatically update if defaults change later.\n\n";
  printf "You may use the interactive setup by passing the [${_c_orange}--interactive${_c_clear}] flag,\nor alternately may use the defined keys below for programmatic setup.\n\n";
  printf "${_f_bold}Defines the application runtime environment:\n${_c_clear}";
  printf "${_f_italic}Valid options [${_c_clear}${_c_yellow}local|dev|staging|demo|testing|production${_c_clear}${_f_italic}]\n${_c_clear}";
  printf "${_c_red}-b -c --env=${_c_yellow}\"<environment>\"${_c_red}${_c_clear}\n\n";
  printf "${_f_bold}Defines the dns hostname the application should accept:\n${_c_clear}";
  printf "${_f_italic}The application will only serve content when referenced by the defined dns from this setting.\n${_c_clear}";
  printf "${_c_red}-b -c --domain=${_c_yellow}\"<dns>\"${_c_red}${_c_clear}\n\n";
  printf "${_f_bold}Defines the username of the webserver (should correspond to the apache/nginx username):\n${_c_clear}";
  printf "${_f_italic}Files referenced only via http will have their ownership set to this user.\n${_c_clear}";
  printf "${_c_red}-b -c --webserver-username=${_c_yellow}\"<username>\"${_c_red}${_c_clear}\n\n";
  printf "${_f_bold}Defines the user group of the webserver (should correspond to the apache/nginx usergroup):\n${_c_clear}";
  printf "${_f_italic}Files accessed only over http or shared between http and cli will have their\ngroup ownership set to this usergroup.\n${_c_clear}";
  printf "${_c_red}-b -c --webserver-usergroup=${_c_yellow}\"<usergroup>\"${_c_red}${_c_clear}\n\n";
  printf "${_f_bold}Defines the username of the shell user (should correspond to command line username):\n${_c_clear}";
  printf "${_f_italic}Files referenced only via the command line will have their ownership set to this user.\n${_c_clear}";
  printf "${_c_red}-b -c --shell-username=${_c_yellow}\"<username>\"${_c_red}${_c_clear}\n\n";
  printf "${_f_bold}Defines the user group of the shell user (should correspond to the command line usergroup):\n${_c_clear}";
  printf "${_f_italic}Files accessed only over the command line that should not be accessible via http\nwill have their group ownership set to this usergroup.\n${_c_clear}";
  printf "${_c_red}-b -c --shell-usergroup=${_c_yellow}\"<usergroup>\"${_c_red}${_c_clear}\n\n";
  
  printf "\n";
  printf "${_c_yellow}${_f_bold}Flags${_c_clear}${_f_bold}(format ${_c_clear}${_c_red}--key${_c_clear}${_f_bold})${_c_clear}${_f_bold}${_c_yellow}:\n\n${_c_clear}";
  printf "${_c_red}--interactive${_c_clear}           Runs the configuration in interactive mode.\n";
  printf "${_f_italic}                        This is for manual setup, and cannot be automated. For automated setup, pass the arguments verbosely.\n";
  printf "${_c_red}--debug${_c_clear}                 Displays additional verbose information about underlying logic for debugging.\n";
  printf "${_c_red}--quiet${_c_clear}                 Hides all output except error messages.\n";
  printf "${_f_italic}                        No effect when the [${_c_clear}${_c_red}--interactive${_c_clear}${_f_italic}] flag is passed.${_c_clear}\n";
  printf "${_c_red}--silent${_c_clear}                Hides all output including error messages.\n";
  printf "${_f_italic}                        No effect when the [${_c_clear}${_c_red}--interactive${_c_clear}${_f_italic}] flag is passed.${_c_clear}\n";
  printf "\n";
  exit 0;
}

# Help menu for the application verifier command
_app_help_verify() {
  clear;
  printf "\n${_c_cyan}${_f_bold}Application Verifier Help\n${_c_clear}\n";
  echo '----------------------------------------------------------------------';
  printf "\n";
  printf "The application verifier command determines the health of the current application.\n";
  printf "This will return information about the current runtime health of the application, and how well it is performing under load.\n";
  printf "If you want to check that the application is currently accessible at all, use the status command [${_c_red}-a --status${_c_clear}].\n\n";
  printf "By default, all health checks will be run. A number of flags are provided to verify the health of specific aspects of the application.\n";
  printf "The default behavior is to run all tests. This may be altered by passing\nvarious flags in to test only the status of specific tests.\nThis command does not take arguments, but does have a number of flags\nto declare that specific tests should run.\n";
  printf "If any test flags are present, only tests corresponding to the\ntest flags passed will be run.\n\n";
  printf "\n";
  printf "${_c_yellow}${_f_bold}Flags${_c_clear}${_f_bold}(format ${_c_clear}${_c_red}--key${_c_clear}${_f_bold})${_c_clear}${_f_bold}${_c_yellow}:\n\n${_c_clear}";
  printf "${_c_red}--test-uptime${_c_clear}           Tests the total application uptime, and displays the duration since the last time the application was down or in maintenance mode.\n";
  printf "${_c_red}--test-load${_c_clear}             Tests the current request load on the application, and benchmarks how much additional request load can be handled by this instance.\n";
  printf "${_c_red}--test-disk${_c_clear}             Tests the remaining disk space on the server.\n";
  printf "${_c_red}--test-ports${_c_clear}            Tests the open ports on the local server with ${_c_yellow}nmap${_c_clear}, and displays a list of ports that can be externally accessed.\n";
  printf "${_c_red}--test-memory${_c_clear}           Tests active memory consumption on the server, and displays the raw memory available as well as the percentage of available memory in use.\n";
  printf "${_c_red}--test-sitemaps${_c_clear}         Tests the sitemap configuration.\n";
  printf "${_c_red}--test-public${_c_clear}           Tests the accessibility of resources in the public directory.\n";
  printf "${_c_red}--test-secure${_c_clear}           Tests the accessibility of secure resources, and reports any files visible on the web that should not be accessible.\n";
  printf "${_c_red}--debug${_c_clear}                 Displays additional verbose information about underlying logic for debugging.\n";
  printf "${_c_red}--quiet${_c_clear}                 Hides all output except error messages.\n";
  printf "${_c_red}--silent${_c_clear}                Hides all output including error messages.\n";
  printf "\n";
  exit 0;
}

# Help menu for the application status command
_app_help_status() {
  clear;
  printf "\n${_c_cyan}${_f_bold}Application Status Help\n${_c_clear}\n";
  echo '----------------------------------------------------------------------';
  printf "\n";
  printf "The application status operation checks that the application is currently operational.\n";
  printf "This command will run a series of tests to insure that the application is accessible\nover http and the command line, whether ssl is valid, whether database and other\nservice connections are accessible, and any other configured criteria.\n\n";
  printf "The default behavior is to run all tests. This may be altered by passing\nvarious flags in to test only the status of specific tests.\nThis command does not take arguments, but does have a number of flags\nto declare that specific tests should run.\n";
  printf "If any test flags are present, only tests corresponding to the\ntest flags passed will be run.\n\n";
  printf "${_c_yellow}${_f_bold}Flags${_c_clear}${_f_bold}(format ${_c_clear}${_c_red}--key${_c_clear}${_f_bold})${_c_clear}${_f_bold}${_c_yellow}:\n\n${_c_clear}";
  printf "${_c_red}--check-maintnance${_c_clear}      Reports whether maintenance mode is currently enabled for the application.\n";
  printf "${_c_red}--check-debug${_c_clear}           Reports whether debug mode is currently enabled for the application.\n";
  printf "${_c_red}--test-registration${_c_clear}     Tests whether the application is correctly registered with the platform.\n";
  printf "${_c_red}--test-environment${_c_clear}      Tests whether the application environment is configured correctly.\n";
  printf "${_c_red}--test-updates${_c_clear}          Tests whether the application is running on the most recent release.\n";
  printf "${_c_red}--test-build${_c_clear}            Tests whether the application build can run correctly.\n";
  printf "${_c_red}--test-files${_c_clear}            Tests whether the filebase and directory permissions are correct.\n";
  printf "${_c_red}--test-http${_c_clear}             Runs the http uptime test.\n";
  printf "${_c_red}--test-cli${_c_clear}              Runs the cli uptime test.\n";
  printf "${_c_red}--test-ssl${_c_clear}              Tests whether ssl is valid.\n";
  printf "${_c_red}--test-database${_c_clear}         Tests whether the application can reach the database.\n";
  printf "${_c_red}--test-redis${_c_clear}            Tests whether the application can reach redis.\n";
  printf "${_c_red}--test-memcache${_c_clear}         Tests whether the application can reach memcache.\n";
  printf "${_c_red}--test-beanstalk${_c_clear}        Tests whether the application can reach beanstalk.\n";
  printf "${_c_red}--test-supervisor${_c_clear}       Tests whether the application can reach supervisor.\n";
  printf "${_c_red}--test-logs${_c_clear}             Tests whether the application logging setup is valid.\n";
  printf "${_c_red}--test-cache${_c_clear}            Tests whether the application caching setup is valid.\n";
  printf "${_c_red}--debug${_c_clear}                 Displays additional verbose information about underlying logic for debugging.\n";
  printf "${_c_red}--quiet${_c_clear}                 Hides all output except error messages.\n";
  printf "${_c_red}--silent${_c_clear}                Hides all output including error messages.\n";
  printf "\n";
  exit 0;
}

# Help menu for the application environment configuration command
_app_help_environment() {
  clear;
  printf "\n${_c_cyan}${_f_bold}Application Environment Configuration Help\n${_c_clear}\n";
  echo '----------------------------------------------------------------------';
  printf "\n";
  printf "The application environment configuration operation defines the baseline parameters for running an application.\n";
  printf "This command will create the ${_c_yellow}.env${_c_clear} file used to define the baseline operation of the application.\n";
  printf "You may use the interactive setup by passing the [${_c_orange}--interactive${_c_clear}] flag,\nor alternately may use the defined keys below for programmatic setup.\n\n";
  printf "${_f_bold}Defines the application runtime environment:\n${_c_clear}";
  printf "${_f_italic}Valid options [${_c_clear}${_c_yellow}local|dev|staging|demo|testing|production${_c_clear}${_f_italic}]\n${_c_clear}";
  printf "${_c_red}-b -c --env=${_c_yellow}\"<environment>\"${_c_red}${_c_clear}\n\n";
  printf "${_f_bold}Defines the dns hostname the application should accept:\n${_c_clear}";
  printf "${_f_italic}The application will only serve content when referenced by the defined dns from this setting.\n${_c_clear}";
  printf "${_c_red}-b -c --domain=${_c_yellow}\"<dns>\"${_c_red}${_c_clear}\n\n";
  printf "${_f_bold}Defines the username of the webserver (should correspond to the apache/nginx username):\n${_c_clear}";
  printf "${_f_italic}Files referenced only via http will have their ownership set to this user.\n${_c_clear}";
  printf "${_c_red}-b -c --webserver-username=${_c_yellow}\"<username>\"${_c_red}${_c_clear}\n\n";
  printf "${_f_bold}Defines the user group of the webserver (should correspond to the apache/nginx usergroup):\n${_c_clear}";
  printf "${_f_italic}Files accessed only over http or shared between http and cli will have their\ngroup ownership set to this usergroup.\n${_c_clear}";
  printf "${_c_red}-b -c --webserver-usergroup=${_c_yellow}\"<usergroup>\"${_c_red}${_c_clear}\n\n";
  printf "${_f_bold}Defines the username of the shell user (should correspond to command line username):\n${_c_clear}";
  printf "${_f_italic}Files referenced only via the command line will have their ownership set to this user.\n${_c_clear}";
  printf "${_c_red}-b -c --shell-username=${_c_yellow}\"<username>\"${_c_red}${_c_clear}\n\n";
  printf "${_f_bold}Defines the user group of the shell user (should correspond to the command line usergroup):\n${_c_clear}";
  printf "${_f_italic}Files accessed only over the command line that should not be accessible via http\nwill have their group ownership set to this usergroup.\n${_c_clear}";
  printf "${_c_red}-b -c --shell-usergroup=${_c_yellow}\"<usergroup>\"${_c_red}${_c_clear}\n\n";
  printf "\n";
  printf "${_c_yellow}${_f_bold}Flags${_c_clear}${_f_bold}(format ${_c_clear}${_c_red}--key${_c_clear}${_f_bold})${_c_clear}${_f_bold}${_c_yellow}:\n\n${_c_clear}";
  printf "${_c_red}--interactive${_c_clear}           Runs the configuration in interactive mode.\n";
  printf "${_f_italic}                        This is for manual setup, and cannot be automated. For automated setup, pass the arguments verbosely.\n";
  printf "${_c_red}--debug${_c_clear}                 Displays additional verbose information about underlying logic for debugging.\n";
  printf "${_c_red}--quiet${_c_clear}                 Hides all output except error messages.\n";
  printf "${_f_italic}                        No effect when the [${_c_clear}${_c_red}--interactive${_c_clear}${_f_italic}] flag is passed.${_c_clear}\n";
  printf "${_c_red}--silent${_c_clear}                Hides all output including error messages.\n";
  printf "${_f_italic}                        No effect when the [${_c_clear}${_c_red}--interactive${_c_clear}${_f_italic}] flag is passed.${_c_clear}\n";
  printf "\n";
  exit 0;
}

# Help menu for application debug commands
_app_help_debug() {
  clear;
  printf "\n${_c_cyan}${_f_bold}Application Debug Mode Help\n${_c_clear}\n";
  echo '----------------------------------------------------------------------';
  printf "\n";
  printf "Debug mode alerts the application to display debugging information on all page loads and command line requests.\n";
  printf "If the application runtime environment is any of [${_c_yellow}local|dev|testing${_c_clear}],\nthen debug mode is always explicitly enabled and this setting has no effect.\n\n";
  printf "There are two relevant commands for this functionality:\n\n";
  printf "${_f_bold}Puts the application into debug mode, or takes it out of debug mode:\n${_c_clear}";
  printf "${_f_italic}Has no effect if the application is already in debug mode.\n${_c_clear}";
  printf "${_c_red}-a --enable-debug${_c_clear}\n\n";
  printf "${_f_bold}Takes the application out of debug mode:\n${_c_clear}";
  printf "${_f_italic}Has no effect if the application is not in debug mode.\n${_c_clear}";
  printf "${_f_italic}The application will always run in debug mode in development and test environments.\n${_c_clear}";
  printf "${_c_red}-a --disable-debug${_c_clear}\n\n";
  printf "\n";
  printf "${_c_yellow}${_f_bold}Flags${_c_clear}${_f_bold}(format ${_c_clear}${_c_red}--key${_c_clear}${_f_bold})${_c_clear}${_f_bold}${_c_yellow}:\n\n${_c_clear}";
  printf "${_c_red}--debug${_c_clear}                 Displays additional verbose information about underlying logic for debugging.\n";
  printf "${_c_red}--quiet${_c_clear}                 Hides all output except error messages.\n";
  printf "${_c_red}--silent${_c_clear}                Hides all output including error messages.\n";
  printf "\n";
  exit 0;
}

# Help menu for application maintenance commands
_app_help_maintenance() {
  clear;
  printf "\n${_c_cyan}${_f_bold}Application Maintenance Mode Help\n${_c_clear}\n";
  echo '----------------------------------------------------------------------';
  printf "\n";
  printf "Maintenance mode presents a static maintenance page on all http page loads,\nand prevents command line interaction with aspects of application functionality that would cause errors during updates.\n\n";
  printf "This is used during builds, updates, migrations, and any other process where running commands would throw errors\nor corrupt the underlying update/build/migration process.\n\n";
  printf "All http requests to the application will display a static maintenance page while maintenance mode is enabled.\n\n";
  printf "Shell commands that do not interact with the application codebase directly are still accessible, but application driven functionality is not.\n";
  printf "Any shell commands that would forward to the app codebase will throw a warning indicating maintenance mode and exit.\n\n";
  printf "There are two relevant commands for this functionality:\n\n";
  printf "${_f_bold}Puts the application into maintenance mode:\n${_c_clear}";
  printf "${_f_italic}Has no effect if the application is already in maintenance mode.\n${_c_clear}";
  printf "${_c_red}-a --enable-maintenance${_c_clear}\n\n";
  printf "${_f_bold}Takes the application out of maintenance mode:\n${_c_clear}";
  printf "${_f_italic}Has no effect if the application is not in maintenance mode.\n${_c_clear}";
  printf "${_c_red}-a --disable-maintenance${_c_clear}\n\n";
  printf "\n";
  printf "${_c_yellow}${_f_bold}Flags${_c_clear}${_f_bold}(format ${_c_clear}${_c_red}--key${_c_clear}${_f_bold})${_c_clear}${_f_bold}${_c_yellow}:\n\n${_c_clear}";
  printf "${_c_red}--debug${_c_clear}                 Displays additional verbose information about underlying logic for debugging.\n";
  printf "${_c_red}--quiet${_c_clear}                 Hides all output except error messages.\n";
  printf "${_c_red}--silent${_c_clear}                Hides all output including error messages.\n";
  printf "\n";
  exit 0;
}