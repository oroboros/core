# Function declarations for the build module

# Control method for build operations. Routes commands to the appropriate handler method.
# All methods passed to the build module call this method first, which aggregates sub-commands
# to various other functions listed here based on the sub-commands passed.

# ------------------------------------------------------------------------------
#                             Build Index Method
# ------------------------------------------------------------------------------
_build_index() {
  load_custom_config "build";
  if has_flag "-v" || has_flag "--verify"; then
    _build_verify_environment;
    _build_verify_application_environment;
  elif has_flag "-s" || has_flag "--status"; then
    _build_status;
  elif has_flag "-l" || has_flag "--latest"; then
    _build_verify_environment;
    if [ ! $? -eq 0 ]; then
      error "Cannot run the latest build because there are environment issues.";
      return 1;
    fi
    _build_verify_application_environment;
    if [ ! $? -eq 0 ]; then
      error "Cannot run the latest build because the application environment is not configured.";
      return 1;
    fi
    info "Proceeding with the latest build.";
    _build_run_latest;
  elif has_flag "-d" || has_flag "--dev"; then
    _build_verify_environment;
    if [ ! $? -eq 0 ]; then
      error "Cannot run the latest development build because there are environment issues.";
      return 1;
    fi
    _build_verify_application_environment;
    if [ ! $? -eq 0 ]; then
      error "Cannot run the latest development build because the application environment is not configured.";
      return 1;
    fi
    info "Proceeding with the latest development build.";
    
  elif has_flag "-r" || has_flag "--reset"; then
    :
  elif has_flag "-c" || has_flag "--config"; then
    _build_config;
  else
    # Display the build help menu index
    _build_help_index;
  fi
  terminate;
}

# ------------------------------------------------------------------------------
#                                Build Controllers
# ------------------------------------------------------------------------------

# Checks the status of the last build operation ran
_build_status() {
  :
}

# Builds the most recent production release
_build_release() {
  :
}

# Builds the most recent development release
_build_dev() {
  :
}

# Verifies the environment and dependencies needed to run a clean build.
_build_verify() {
  :
}

# Performs a full reset back to the last clean build state.
_build_reset() {
  :
}

# Manually up the localized configuration for the build module.
# Use this where environments differ from the default.
# This only needs to be done one time, or when system settings change.
_build_config() {
  if is_help; then
    _build_help_config;
  else
    local -A _cfg_path="$(get_custom_config_directory)/build";
    local -A _cfg=();
    local _prompt="";
    local _default="";
    if has_flag "--interactive"; then
      clear;
    fi
    create_custom_config "build";
    if has_flag "--interactive"; then
      printf "${_f_bold}${_c_cyan}Database Configuration${_c_clear}\n";
    fi
    if check_arg "user"; then
      _cfg["database_root_user"]=$(get_arg "user");
    elif has_flag "--interactive"; then
      _default=$([ ! -z ${_build['database_root_user']} ] && echo ${_build['database_root_user']} || echo ${_build_defaults['database_root_user']});
      printf "Enter the database root user for running database migrations ( default ["$_c_yellow"${_default}"$_c_clear"] ).\n? ";
      read "_prompt";
      printf "\n";
      if [ -z "${_prompt}" ]; then
        _cfg["database_root_user"]="${_default}";
      else
        _cfg['database_root_user']="${_prompt}";
      fi
    fi
    if check_arg "password"; then
      _cfg["database_root_password"]=$(get_arg "password");
    elif has_flag "--interactive"; then
      _default=$([ ! -z ${_build['database_root_password']} ] && echo ${_build['database_root_password']} || echo ${_build_defaults['database_root_password']});
      printf "Enter the database root password for running database migrations ( default ["$_c_yellow"${_default}"$_c_clear"] )\n? ";
      read -s "_prompt";
      printf "\n";
      if [ -z "${_prompt}" ]; then
        _cfg['database_root_password']="${_default}";
      else
        _cfg['database_root_password']="${_prompt}";
      fi
    fi
    if check_arg "host"; then
      _cfg['database_root_host']=$(get_arg "host");
    elif has_flag "--interactive"; then
      _default=$([ ! -z ${_build['database_root_host']} ] && echo ${_build['database_root_host']} || echo ${_build_defaults['database_root_host']});
      printf "Enter the database hostname for running database migrations ( default ["$_c_yellow"${_default}"$_c_clear"] ).\n? ";
      read "_prompt";
      printf "\n";
      if [ -z "${_prompt}" ]; then
        _cfg['database_root_host']="${_default}";
      else
        _cfg['database_root_host']="${_prompt}";
      fi
    fi
    if check_arg "port"; then
      _cfg['database_root_port']=$(get_arg "port");
    elif has_flag "--interactive"; then
      _default=$([ ! -z ${_build['database_root_port']} ] && echo ${_build['database_root_port']} || echo ${_build_defaults['database_root_port']});
      printf "Enter the root database port for running database migrations ( default ["$_c_yellow"${_default}"$_c_clear"] ).\n? ";
      read "_prompt";
      printf "\n";
      if [ -z "${_prompt}" ]; then
        _cfg['database_root_port']="${_default}";
      else
        _cfg['database_root_port']="${_prompt}";
      fi
    fi
    if check_arg "database"; then
      _cfg['database_root_schema']=$(get_arg "database");
    elif has_flag "--interactive"; then
      _default=$([ ! -z ${_build['database_root_schema']} ] && echo ${_build['database_root_schema']} || echo ${_build_defaults['database_root_schema']});
      printf "Enter the default database schema name for running database migrations ( default ["$_c_yellow"${_default}"$_c_clear"] ).\n? ";
      read "_prompt";
      printf "\n";
      if [ -z "${_prompt}" ]; then
        _cfg['database_root_schema']="${_default}";
      else
        _cfg['database_root_schema']="${_prompt}";
      fi
    fi
    
    if check_arg "test-user"; then
      _cfg["test-user"]=$(get_arg "test-user");
    elif has_flag "--interactive"; then
      _default=$([ ! -z ${_build['database_test_user']} ] && echo ${_build['database_test_user']} || echo ${_build_defaults['database_test_user']});
      printf "Enter the unit testing database user for running tests on deployment ( default ["$_c_yellow"${_default}"$_c_clear"] ).\n? ";
      read "_prompt";
      printf "\n";
      if [ -z "${_prompt}" ]; then
        _cfg["database_test_user"]="${_default}";
      else
        _cfg['database_test_user']="${_prompt}";
      fi
    fi
    if check_arg "test-password"; then
      _cfg["database_test_password"]=$(get_arg "test-password");
    elif has_flag "--interactive"; then
      _default=$([ ! -z ${_build['database_test_password']} ] && echo ${_build['database_test_password']} || echo ${_build_defaults['database_test_password']});
      printf "Enter the unit testing database password for running tests on deployment ( default ["$_c_yellow"${_default}"$_c_clear"] )\n? ";
      read -s "_prompt";
      printf "\n";
      if [ -z "${_prompt}" ]; then
        _cfg['database_test_password']="${_default}";
      else
        _cfg['database_test_password']="${_prompt}";
      fi
    fi
    if check_arg "test-host"; then
      _cfg['database_test_host']=$(get_arg "test-host");
    elif has_flag "--interactive"; then
      _default=$([ ! -z ${_build['database_test_host']} ] && echo ${_build['database_test_host']} || echo ${_build_defaults['database_test_host']});
      printf "Enter the unit testing hostname for running tests on deployment ( default ["$_c_yellow"${_default}"$_c_clear"] ).\n? ";
      read "_prompt";
      printf "\n";
      if [ -z "${_prompt}" ]; then
        _cfg['database_test_host']="${_default}";
      else
        _cfg['database_test_host']="${_prompt}";
      fi
    fi
    if check_arg "test-password"; then
      _cfg['database_test_password']=$(get_arg "test-password");
    elif has_flag "--interactive"; then
      _default=$([ ! -z ${_build['database_test_port']} ] && echo ${_build['database_test_port']} || echo ${_build_defaults['database_test_port']});
      printf "Enter the unit testing database port ( default ["$_c_yellow"${_default}"$_c_clear"] ).\n? ";
      read "_prompt";
      printf "\n";
      if [ -z "${_prompt}" ]; then
        _cfg['database_test_password']="${_default}";
      else
        _cfg['database_test_password']="${_prompt}";
      fi
    fi
    if check_arg "test-database"; then
      _cfg['database_test_scheam']=$(get_arg "test-database");
    elif has_flag "--interactive"; then
      _default=$([ ! -z ${_build['database_test_schema']} ] && echo ${_build['database_test_schema']} || echo ${_build_defaults['database_test_schema']});
      printf "Enter the unit testing database name for running tests on deployment ( default ["$_c_yellow"${_default}"$_c_clear"] ).\n? ";
      read "_prompt";
      printf "\n";
      if [ -z "${_prompt}" ]; then
        _cfg['database_test_schema']="${_default}";
      else
        _cfg['database_test_schema']="${_prompt}";
      fi
    fi
    
    if check_arg "save-path"; then
      _cfg['database_save_path']=$(get_arg "save-path");
    elif has_flag "--interactive"; then
      _default=$([ ! -z ${_build['database_save_path']} ] && echo ${_build['database_save_path']} || echo ${_build_defaults['database_save_path']});
      printf "Enter the data backup path ( default ["$_c_yellow"${_default}"$_c_clear"] ).\n? ";
      read "_prompt";
      printf "\n";
      if [ -z "${_prompt}" ]; then
        _cfg['database_save_path']="${_default}";
      else
        _cfg['database_save_path']="${_prompt}";
      fi
    fi
    if check_arg "backup-space"; then
      _cfg['database_backup_minimum_disk_space']=$(get_arg "backup-space");
    elif has_flag "--interactive"; then
      _default=$([ ! -z ${_build['database_backup_minimum_disk_space']} ] && echo ${_build['database_backup_minimum_disk_space']} || echo ${_build_defaults['database_backup_minimum_disk_space']});
      printf "Enter the minimum required free bytes for data backups ( default ["$_c_yellow"${_default}"$_c_clear"] ).\n? ";
      read "_prompt";
      printf "\n";
      if [ -z "${_prompt}" ]; then
        _cfg['database_backup_minimum_disk_space']="${_default}";
      else
        _cfg['database_backup_minimum_disk_space']="${_prompt}";
      fi
    fi
    info "Updating build configuration.";
    for i in "${!_cfg[@]}"
    do
      update_custom_config "build" "$i" "${_cfg[$i]}";
    done
    debug "Custom database configuration saved to [${_c_yellow}$(get_custom_config_directory)/database${_c_gray}].";
    debug "Values present in this file will override existing defaults on subsequent operations.";
    debug "Additionally, explicitly passing arguments will override custom configuration options also.";
    debug "New configuration file values:\n\n$(cat "$(get_custom_config_directory)/build")\n";
    success "Database configuration updated.";
  fi
}

# ------------------------------------------------------------------------------
#                                Build Internals
# ------------------------------------------------------------------------------

_build_verify_environment() {
#  local _con="$(_build_get_connection_string)";
  local _method="";
  local verifier_key="";
  local _designation=0;
  local -A _required=();
  local -a _required_valid=();
  local -a _required_invalid=();
  local -A _optional=();
  local -a _optional_valid=();
  local -a _optional_invalid=();
  local _build_verified=0;
  local _build_optional_verified=0;
  local _os="$(os_distro)";
  _required['network']=0;
  _required['git']=0;
  _required['mysql']=0;
  _required['php']=0;
  _required['composer']=0;
  _required['nodejs']=0;
  _required['npm']=0;
  _required['webserver']=0;
  _optional['supervisor']=0;
  _optional['beanstalk']=0;
  _optional['redis']=0;
  info "Verifying the local environment and dependencies.";
  debug "Detected operating system [${_c_yellow}${_os}${_c_gray}]";
  debug "Verifying required dependencies...";
  for verifier_key in "${!_required[@]}"; do
    _method="_build_verify_${verifier_key}";
    $_method;
    if [ $? -eq 0 ]; then
      _required_valid+=("$verifier_key");
    else
      _required_invalid+=("$verifier_key");
    fi
  done;
  debug "Verifying optional dependencies...";
  for verifier_key in "${!_optional[@]}"; do
    _method="_build_verify_${verifier_key}";
    if ! [[ $(type -t $_method &2>/dev/null) = 'function' ]]; then
      error "Expected function [${_c_yellow}${_method}${_c_orange}] does not exist.";
      return 1;
    fi
    $_method;
    if [ $? -eq 0 ]; then
      _optional_valid+=("$verifier_key");
    else
      _optional_invalid+=("$verifier_key");
    fi
  done;

  debug "Generating verifier designation...";
  for verifier_key in "${_required_invalid[@]}"; do
    _build_verified=1;
  done;
  for verifier_key in "${_optional_invalid[@]}"; do
    _build_optional_verified=1;
  done;
  if [ ! $_build_verified -eq 0 ]; then
    error "There are blocking errors, build cannot proceed.";
    if [ ! $_build_optional_verified -eq 0 ]; then
      warning "Some optional dependencies are also invalid.";
    fi
  elif [ ! $_build_optional_verified -eq 0 ]; then
    warning "Some optional dependencies are invalid. The build can proceed,\n              but optional invalid functionality will be omitted in this environment.";
  fi
  for verifier_key in "${_required_invalid[@]}"; do
    error "Required dependency [${_c_yellow}${verifier_key}${_c_orange}] is not valid.";
  done;
  for verifier_key in "${_optional_invalid[@]}"; do
    warning "Optional dependency [${_c_clear}${verifier_key}${_c_yellow}] is not valid.";
  done;
  if [ $_build_verified -eq 0 ] && [ $_build_optional_verified -eq 0 ]; then
    success "All environment settings and dependencies are valid.";
  fi
  return $_build_verified;
}
_build_status() {
  local _pwd="$(pwd)";
  local verifier_key="";
  local _designation=0;
  local -a _required=();
  local -a _required_valid=();
  local -a _required_invalid=();
  local -a _optional=();
  local -a _optional_valid=();
  local -a _optional_invalid=();
  local _build_verified=0;
  local _build_optional_verified=0;
  local _os="$(os_distro)";
  _required+=('core_environment');
  _required+=('core');
  _required+=('registration');
  _required+=('environment');
  _required+=('app');
  info "Checking the build status in [${_c_yellow}${_dir}${_c_cyan}].";
  debug "Verifying status...";
  for verifier_key in "${_required[@]}"; do
    _method="_build_status_${verifier_key}";
    if ! [[ $(type -t $_method &2>/dev/null) = 'function' ]]; then
      error "Expected function [${_c_yellow}${_method}${_c_orange}] does not exist.";
      return 1;
    fi
    $_method;
    if [ $? -eq 0 ]; then
      _required_valid+=("$verifier_key");
    else
      _required_invalid+=("$verifier_key");
    fi
  done;
  debug "Verifying optional updates...";
  for verifier_key in "${_optional[@]}"; do
    _method="_build_verify_${verifier_key}";
    $_method;
    if [ $? -eq 0 ]; then
      _optional_valid+=("$verifier_key");
    else
      _optional_invalid+=("$verifier_key");
    fi
  done;

  debug "Generating status designation...";
  for verifier_key in "${_required_invalid[@]}"; do
    _build_verified=1;
  done;
  for verifier_key in "${_optional_invalid[@]}"; do
    _build_optional_verified=1;
  done;
  if [ ! $_build_verified -eq 0 ]; then
    error "There are blocking errors, build cannot proceed.";
    if [ ! $_build_optional_verified -eq 0 ]; then
      warning "Some optional dependencies are also invalid.";
    fi
  elif [ ! $_build_optional_verified -eq 0 ]; then
    warning "Some optional dependencies are invalid. The build can proceed,\n              but optional invalid functionality will be omitted in this environment.";
  fi
  for verifier_key in "${_required_invalid[@]}"; do
    error "Required dependency [${_c_yellow}${verifier_key}${_c_orange}] is not valid.";
  done;
  for verifier_key in "${_optional_invalid[@]}"; do
    warning "Optional dependency [${_c_clear}${verifier_key}${_c_yellow}] is not valid.";
  done;
  if [ $_build_verified -eq 0 ] && [ $_build_optional_verified -eq 0 ]; then
    success "All environment settings and dependencies are valid.";
  fi
  return $_build_verified;
}

_build_verify_application_environment() {
  local _pwd="$(pwd)";
  local _env_file="${_pwd}/.env";
  if [ ! -f "${_pwd}/.env" ]; then
    error "No application environment file detected at [${_c_yellow}${_env_file}${_c_orange}].";
    return 1;
  fi
#  info "Importing application environment detected at [${_c_yellow}${_env_file}${_c_cyan}].";
#  source "${_env_file}";
  return 0;
}

_build_verify_registration() {
  local _designation=0;
  debug "Verifying application registration...";
  
  return $_designation;
}

_build_verify_network() {
  local _designation=0;
  network_verify;
  _designation=$?;
  debug "Verifying network connectivity...";
  if [ ! $? -eq 0 ]; then
    error "No internet connectivity detected.";
    return 1;
  fi
  debug "Network connectivity is [${_c_yellow}active${_c_gray}].";
  return $_designation;
}

_build_verify_git() {
  local _designation=0;
  local _build_git_binary="";
  local _build_git_version="";
  debug "Verifying git installation...";
  _build_git_binary="$(git_binary)";
  if [ ! $? -eq 0 ]; then
    error "No git installation detected.";
    return 1;
  fi
  _build_git_version="$(git_version)";
  debug "Git detected at [${_c_yellow}${_build_git_binary}${_c_gray}] with version [${_c_yellow}${_build_git_version}${_c_gray}].";
  if ! git_check_uncommitted_changes "${_dir}"; then
    error "Unstaged file changes exist in [${_c_yellow}${_dir}${_c_orange}].";
    _designation=1;
  fi
  return $_designation;
}

_build_verify_php() {
  local _designation=0;
  local _build_php_binary="";
  local _build_php_version="";
  debug "Verifying php installation...";
  _build_php_binary="$(php_binary)";
  if [ ! $? -eq 0 ]; then
    error "No php installation detected.";
    return 1;
  fi
  _build_php_version="$(php_version)";
  debug "PHP detected at [${_c_yellow}${_build_php_binary}${_c_gray}] with version [${_c_yellow}${_build_php_version}${_c_gray}].";
  return $_designation;
}

_build_verify_nodejs() {
  local _designation=0;
  local _build_nodejs_binary="";
  local _build_nodejs_version="";
  debug "Verifying node.js installation...";
  _build_nodejs_binary="$(nodejs_binary)";
  if [ ! $? -eq 0 ]; then
    error "No nodejs installation detected.";
    return 1;
  fi
  _build_nodejs_version="$(nodejs_version)";
  debug "Node.js detected at [${_c_yellow}${_build_nodejs_binary}${_c_gray}] with version [${_c_yellow}${_build_nodejs_version}${_c_gray}].";
  return $_designation;
}

_build_verify_npm() {
  local _designation=0;
  local _build_npm_binary="";
  local _build_npm_version="";
  debug "Verifying npm installation...";
  _build_npm_binary="$(npm_binary)";
  if [ ! $? -eq 0 ]; then
    error "No npm installation detected.";
    return 1;
  fi
  _build_npm_version="$(npm_version)";
  debug "NPM detected at [${_c_yellow}${_build_npm_binary}${_c_gray}] with version [${_c_yellow}${_build_npm_version}${_c_gray}].";
  return $_designation;
}

_build_verify_webserver() {
  local _designation=0;
  local _build_apache_binary="";
  local _build_apache_version="";
  debug "Verifying apache installation...";
  _build_apache_binary="$(apache_binary)";
  if [ ! $? -eq 0 ]; then
    error "No apache installation detected.";
    return 1;
  fi
  _build_apache_version="$(apache_version)";
  debug "Apache detected at [${_c_yellow}${_build_apache_binary}${_c_gray}] with version [${_c_yellow}${_build_apache_version}${_c_gray}].";
  return $_designation;
}

_build_verify_redis() {
  local _designation=0;
  local _build_redis_binary="";
  local _build_redis_version="";
  debug "Verifying redis installation...";
  _build_redis_binary="$(redis_binary)";
  if [ ! $? -eq 0 ]; then
    error "No redis server installation detected.";
    return 1;
  fi
  _build_redis_version="$(redis_version)";
  debug "Redis server detected at [${_c_yellow}${_build_redis_binary}${_c_gray}] with version [${_c_yellow}${_build_redis_version}${_c_gray}].";
  return $_designation;
}

_build_verify_mysql() {
  local _designation=0;
  local _install_user="${_build['database_root_user']}";
  local _install_password="${_build['database_root_password']}";
  local _build_mysql_binary="";
  local _build_mysql_version="";
  debug "Verifying mysql installation...";
  _build_mysql_binary="$(mysql_binary)";
  if [ ! $? -eq 0 ]; then
    error "No mysql installation detected.";
    return 1;
  fi
  if [ -z "${_install_user}" ]; then
    error "No migration user defined. Please run the build config with [${_c_yellow}oroboros -b --config --interactive${_c_orange}] to define one.";
    return 1;
  fi
  _build_mysql_version="$(mysql_version)";
  debug "MySQL client detected at [${_c_yellow}${_build_mysql_binary}${_c_gray}] with version [${_c_yellow}${_build_mysql_version}${_c_gray}].";
  return $_designation;
}

_build_verify_composer() {
  local _designation=0;
  local _build_php_binary="";
  local _build_php_version="";
  debug "Verifying composer installation...";
  _build_composer_binary="$(php_composer_binary)";
  if [ ! $? -eq 0 ]; then
    error "No composer installation detected.";
    return 1;
  fi
  _build_composer_version="$(php_composer_version)";
  debug "Composer detected at [${_c_yellow}${_build_composer_binary}${_c_gray}] with version [${_c_yellow}${_build_composer_version}${_c_gray}].";
  return $_designation;
}

_build_verify_supervisor() {
  local _designation=0;
  local _build_supervisor_binary="";
  local _build_supervisor_version="";
  debug "Verifying supervisor installation...";
  _build_supervisor_binary="$(supervisor_binary)";
  if [ ! $? -eq 0 ]; then
    error "No supervisor installation detected.";
    return 1;
  fi
  _build_supervisor_version="$(supervisor_version)";
  debug "Supervisor detected at [${_c_yellow}${_build_supervisor_binary}${_c_gray}] with version [${_c_yellow}${_build_supervisor_version}${_c_gray}].";
  return $_designation;
}

_build_verify_beanstalk() {
  local _designation=0;
  local _build_beanstalk_binary="";
  local _build_beanstalk_version="";
  debug "Verifying beanstalk installation...";
  _build_beanstalk_binary="$(beanstalk_binary)";
  if [ ! $? -eq 0 ]; then
    error "No beanstalk installation detected.";
    return 1;
  fi
  _build_beanstalk_version="$(beanstalk_version)";
  debug "Beanstalk detected at [${_c_yellow}${_build_beanstalk_binary}${_c_gray}] with version [${_c_yellow}${_build_beanstalk_version}${_c_gray}].";
  return $_designation;
}

_build_status_registration() {
  local _designation=0;
  debug "Verifying application registration status...";
  return $_designation;
}

_build_status_core() {
  local _designation=0;
  debug "Verifying core platform build status...";
  return $_designation;
}

_build_status_app() {
  local _designation=0;
  debug "Verifying application build status...";
  return $_designation;
}

_build_status_core_environment() {
  local _designation=0;
  debug "Verifying core platform environment status...";
  return $_designation;
}

_build_status_environment() {
  local _designation=0;
  debug "Verifying application environment status...";
  return $_designation;
}

_build_run_latest() {
  local _designation=0;
  local _git_branch="$(_build_get_git_branch)";
  local _git_update_branch="master";
  if ! [[ $_git_branch = $_git_update_branch ]]; then
    info "Checking out the git update branch [${_c_yellow}${_git_update_branch}${_c_cyan}].";
    git checkout "${_git_update_branch}" >/dev/null 2>&1;
    if [ ! $? -eq 0 ]; then
      error "Failed to checkout the update branch [${_c_yellow}${_git_update_branch}${_c_cyan}]. Build aborted.";
      return 1;
    fi
  fi
  local _git_original_commit_id="$(git_get_current_commit_id ${_dir})";
  git_check_remote_changes "${_oroboros_dir}";
  git_check_remote_changes "${_dir}";
  if [[ "${_git_original_commit_id}" = "$(git_get_current_commit_id ${_dir})" ]]; then
    info "Already on the most recent git update for branch [${_c_yellow}${_git_update_branch}${_c_cyan}].";
    if ! [[ $_git_branch = $_git_update_branch ]]; then
      info "Returning to the previous git branch [${_c_yellow}${_git_branch}${_c_cyan}].";
      git checkout "${_git_branch}";
    fi
  fi
  _build_run_npm;
  _designation=$?;
  if ! [[ $_designation -eq 0 ]]; then
    error "Error running NPM update tasks.";
    return 1;
  fi
  _build_run_grunt_init;
  _designation=$?;
  if ! [[ $_designation -eq 0 ]]; then
    error "Error running Grunt init tasks.";
    return 1;
  fi
  _build_run_grunt_build;
  _designation=$?;
  if ! [[ $_designation -eq 0 ]]; then
    error "Error running Grunt build tasks.";
    return 1;
  fi
  _build_run_database_migrations;
  _designation=$?;
  if ! [[ $_designation -eq 0 ]]; then
    error "Error running database migrations.";
    return 1;
  fi
  success "Build resolved successfully.";
  return $_designation;
}

_build_run_npm() {
  local _npm="$(npm_binary)";
  local _pwd="$(pwd)";
  info "Running NPM tasks for the core library.";
  npm_install "${_oroboros_dir}";
  info "Running NPM tasks for the application.";
  npm_install "${_dir}";
  return 0;
}

_build_run_grunt_init() {
  local _designation=0;
  local _core_updated=0;
  local _app_updated=0;
  local _npm="$(npm_binary)";
  local _pwd="$(pwd)";
  info "Running Grunt initialization tasks for the core library.";
  cd "${_oroboros_dir}";
  if has_flag '--debug'; then
    grunt init;
  else
    grunt init >/dev/null;
  fi
  _core_updated=$?;
  if ! [[ $_core_updated -eq 0 ]]; then
    error "Error running grunt initialization in [${_c_yellow}${_oroboros_dir}${_c_orange}].";
  fi
  info "Running Grunt initialization tasks for the application.";
  cd "${_dir}";
  if has_flag '--debug'; then
    grunt init;
  else
    grunt init >/dev/null;
  fi
  _app_updated=$?;
  if ! [[ $_app_updated -eq 0 ]]; then
    error "Error running grunt initialization in [${_c_yellow}${_dir}${_c_orange}].";
  fi
  _designation=$_core_updated+$_app_updated;
  cd "${_pwd}";
  return 0;
}
_build_run_grunt_build() {
  local _designation=0;
  local _core_updated=0;
  local _app_updated=0;
  local _npm="$(npm_binary)";
  local _pwd="$(pwd)";
  info "Running Grunt build tasks for the core library.";
  cd "${_oroboros_dir}";
  if has_flag '--debug'; then
    grunt build;
  else
    grunt build >/dev/null;
  fi
  _core_updated=$?;
  if ! [[ $_core_updated -eq 0 ]]; then
    error "Error running grunt build in [${_c_yellow}${_oroboros_dir}${_c_orange}].";
  fi
  info "Running Grunt build tasks for the application.";
  cd "${_dir}";
  if has_flag '--debug'; then
    grunt build;
  else
    grunt build >/dev/null;
  fi
  _app_updated=$?;
  if ! [[ $_app_updated -eq 0 ]]; then
    error "Error running grunt build in [${_c_yellow}${_dir}${_c_orange}].";
  fi
  _designation=$_core_updated+$_app_updated;
  cd "${_pwd}";
  return 0;
}

_build_run_database_migrations() {
  local _designation=0;
  local _install_user="${_build['database_root_user']}";
  local _install_password="${_build['database_root_password']}";
  local _install_host="${_build['database_root_host']}";
  local _install_port="${_build['database_root_port']}";
  local _migration_path="${_dir}/etc/src/sql";
  local _db="$(mysql_binary)";
  local _pwd="$(pwd)";
  local _con="--user=${_install_user} --password=${_install_password} --host=${_install_host} --port=${_install_port}";
  cd "${_migration_path}";
  info "Running database migrations.";
  for filename in "${_migration_path}/"*".sql";
  do
    info "Importing [${_c_yellow}$(basename ${filename[@]})${_c_cyan}]".
    debug "Import command [${_c_yellow}${_db} ${_con} < ${filename[@]}${_c_gray}]";
    ${_db} ${_con} < ${filename[@]};
    if [ ! $? -eq 0 ]; then
      error "Failed to import [${_c_yellow}$(basename ${filename[@]})${_c_orange}]";
      $_designation++;
    else
      info "Successfully ran migration for [${_c_yellow}$(basename ${filename[@]})${_c_cyan}]";
    fi
  done;
  cd "${_pwd}";
  return $_designation;
}

_build_get_git_branch() {
  local _designation=0;
  local _branch="$(git_get_current_branch ${_dir})";
  _designation=$?;
  if [[ $_designation -eq 0 ]]; then
    echo "${_branch}";
  fi
  return $_designation;
}

_build_mark_progress() {
  local _build_step="$1";
}

_build_check_progress() {
  :
}

_build_mark_complete() {
  :
}

_build_mark_failed() {
  :
}

# ------------------------------------------------------------------------------
#                               Help Documentation
# ------------------------------------------------------------------------------

# Help menu for database archive list command
_build_help_index() {
  clear;
  printf "\n${_c_cyan}${_f_bold}Build Help\n${_c_clear}\n";
  echo '----------------------------------------------------------------------';
  printf "\n";
  printf "Running a build will destroy all local file and database changes to the application.\n";
  printf "If you have localized file or data changes, make sure you back them up prior to running a build or dev build.\n\n";
  printf "A local environment preflight will always occur prior to a build running.\n";
  printf "The build will not proceed if any dependencies or environment settings\n";
  printf "are not compatible with the application requirements.\n\n";
  printf "${_f_bold}Checks the status of the last build:\n${_c_clear}";
  printf "${_c_red}-b --status${_c_clear}\n\n";
  printf "${_f_bold}Sets up the local configuration for the build:\n${_c_clear}";
  printf "${_c_red}-b --config${_c_clear}\n\n";
  printf "${_f_bold}Builds the application from the latest release:\n${_c_clear}";
  printf "${_c_red}-b --latest${_c_clear}\n\n";
  printf "${_f_bold}Builds the application from the latest dev release:\n${_c_clear}";
  printf "${_c_red}-b --dev${_c_clear}\n\n";
  printf "${_f_bold}Checks the local environment for build compatibility:\n${_c_clear}";
  printf "${_c_red}-b --verify${_c_clear}\n\n";
  printf "${_f_bold}Resets to the last clean build state and rebuilds:\n${_c_clear}";
  printf "${_c_red}-b --reset${_c_clear}\n\n";
  printf "\n";
  printf "${_c_yellow}${_f_bold}Flags${_c_clear}${_f_bold}(format ${_c_clear}${_c_red}--key${_c_clear}${_f_bold})${_c_clear}${_f_bold}${_c_yellow}:\n\n${_c_clear}";
  printf "${_c_red}--force${_c_clear}                 Forces the build to run, even if the environment is incomplete or uncommitted file changes exist.\n";
  printf "${_c_red}--debug${_c_clear}                 Displays additional verbose information about underlying logic for debugging.\n";
  printf "${_c_red}--quiet${_c_clear}                 Hides all output except error messages.\n";
  printf "${_c_red}--silent${_c_clear}                Hides all output including error messages.\n";
  printf "${_c_red}--daemonize${_c_clear}             Runs the build in the background.\n";
  printf "\n";
#  printf "${_f_bold}Related:${_c_clear}${_f_italic}${_c_gray} Config Help:${_c_clear}       ${_c_red}oroboros -d --config -h${_c_clear}\n";
#  printf "${_f_bold}Related:${_c_clear}${_f_italic}${_c_gray} List Commit Help:${_c_clear}  ${_c_red}oroboros -d --list-versions -h${_c_clear}\n";
#  printf "${_f_bold}Related:${_c_clear}${_f_italic}${_c_gray} Commit Help:${_c_clear}       ${_c_red}oroboros -d -c -h${_c_clear}\n";
#  printf "${_f_bold}Related:${_c_clear}${_f_italic}${_c_gray} Backup Help:${_c_clear}       ${_c_red}oroboros -d -b -h${_c_clear}\n";
#  printf "${_f_bold}Related:${_c_clear}${_f_italic}${_c_gray} Build Help:${_c_clear}        ${_c_red}oroboros -d --build -h${_c_clear}\n";
#  printf "${_f_bold}Related:${_c_clear}${_f_italic}${_c_gray} Rollback Help:${_c_clear}     ${_c_red}oroboros -d --rollback -h${_c_clear}\n";
  printf "\n";
  exit 0;
}

# Help menu for database archive list command
_build_help_config() {
  clear;
  printf "\n${_c_cyan}${_f_bold}Build Configuration Help\n${_c_clear}\n";
  echo '----------------------------------------------------------------------';
  printf "\n";
  printf "The build configuration operation defines the baseline parameters for running build operations.\n";
  printf "You may use the interactive setup by passing the [${_c_orange}--interactive${_c_clear}] flag,\nor alternately may use the defined keys below for programmatic setup.\n\n";
  printf "${_f_bold}Defines the default root database user to use for database migrations:\n${_c_clear}";
  printf "${_c_red}-b -c --user=${_c_yellow}\"<username>\"${_c_red}${_c_clear}\n\n";
  printf "${_f_bold}Defines the default root database password to use for database migrations:\n${_c_clear}";
  printf "${_c_red}-b -c --password=${_c_yellow}\"<password>\"${_c_red}${_c_clear}\n\n";
  printf "${_f_bold}Defines the default root database host to use for database migrations:\n${_c_clear}";
  printf "${_c_red}-b -c --host=${_c_yellow}\"<host>\"${_c_red}${_c_clear}\n\n";
  printf "${_f_bold}Defines the default root database port to use for database migrations:\n${_c_clear}";
  printf "${_c_red}-b -c --port=${_c_yellow}\"<port>\"${_c_red}${_c_clear}\n\n";
  printf "${_f_bold}Defines the default root database schema to use for database migrations:\n${_c_clear}";
  printf "${_c_red}-b -c --schema=${_c_yellow}\"<schema>\"${_c_red}${_c_clear}\n\n";
  printf "${_f_bold}Defines the default testing database user to use for database unit testing on deploy:\n${_c_clear}";
  printf "${_c_red}-b -c --test-user=${_c_yellow}\"<username>\"${_c_red}${_c_clear}\n\n";
  printf "${_f_bold}Defines the default test database password to use for database unit testing on deploy:\n${_c_clear}";
  printf "${_c_red}-b -c --test-password=${_c_yellow}\"<password>\"${_c_red}${_c_clear}\n\n";
  printf "${_f_bold}Defines the default test database host to use for database unit testing on deploy:\n${_c_clear}";
  printf "${_c_red}-b -c --test-host=${_c_yellow}\"<host>\"${_c_red}${_c_clear}\n\n";
  printf "${_f_bold}Defines the default test database port to use for database unit testing on deploy:\n${_c_clear}";
  printf "${_c_red}-b -c --test-port=${_c_yellow}\"<port>\"${_c_red}${_c_clear}\n\n";
  printf "${_f_bold}Defines the default test database schema to use for database unit testing on deploy:\n${_c_clear}";
  printf "${_c_red}-b -c --test-schema=${_c_yellow}\"<schema>\"${_c_red}${_c_clear}\n\n";
  printf "${_f_bold}Defines the disk location to store backup snapshots of database data:\n${_c_clear}";
  printf "${_c_red}-b -c --backup-path=${_c_yellow}\"/path/to/database/backups\"${_c_red}${_c_clear}\n\n";
  printf "${_f_bold}Defines the minimum available disk space required to allow a backup to occur:\n${_c_clear}";
  printf "${_c_red}-b -c --backup-space=${_c_yellow}\"<number-of-bytes>\"${_c_red}${_c_clear}\n\n";
  printf "\n";
  printf "${_c_yellow}${_f_bold}Flags${_c_clear}${_f_bold}(format ${_c_clear}${_c_red}--key${_c_clear}${_f_bold})${_c_clear}${_f_bold}${_c_yellow}:\n\n${_c_clear}";
  printf "${_c_red}--interactive${_c_clear}           Runs the configuration in interactive mode.\n";
  printf "${_c_red}--debug${_c_clear}                 Displays additional verbose information about underlying logic for debugging.\n";
  printf "${_c_red}--quiet${_c_clear}                 Hides all output except error messages.\n";
  printf "${_c_red}--silent${_c_clear}                Hides all output including error messages.\n";
  printf "\n";
#  printf "${_f_bold}Related:${_c_clear}${_f_italic}${_c_gray} Config Help:${_c_clear}       ${_c_red}oroboros -d --config -h${_c_clear}\n";
#  printf "${_f_bold}Related:${_c_clear}${_f_italic}${_c_gray} List Commit Help:${_c_clear}  ${_c_red}oroboros -d --list-versions -h${_c_clear}\n";
#  printf "${_f_bold}Related:${_c_clear}${_f_italic}${_c_gray} Commit Help:${_c_clear}       ${_c_red}oroboros -d -c -h${_c_clear}\n";
#  printf "${_f_bold}Related:${_c_clear}${_f_italic}${_c_gray} Backup Help:${_c_clear}       ${_c_red}oroboros -d -b -h${_c_clear}\n";
#  printf "${_f_bold}Related:${_c_clear}${_f_italic}${_c_gray} Build Help:${_c_clear}        ${_c_red}oroboros -d --build -h${_c_clear}\n";
#  printf "${_f_bold}Related:${_c_clear}${_f_italic}${_c_gray} Rollback Help:${_c_clear}     ${_c_red}oroboros -d --rollback -h${_c_clear}\n";
  printf "\n";
  exit 0;
}