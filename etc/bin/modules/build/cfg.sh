## This file controls persistent configurations
if [[ "${APP_CONNECTIONS_DEFAULTS_DATABASE}" = "" ]]; then
  debug "No application default database type defined.";
else
  declare -r _build_database_type="${APP_CONNECTIONS_DEFAULTS_DATABASE}";
    _key='APP_CONNECTIONS_DEFAULTS_'"$(printf '%s' "${_build_database_type}" | awk '{ print toupper($0) }')";
  if [[ "${!_key}" = "" ]]; then
    debug "No default database connection key defined."
  else
    declare -r _build_database_connection_key="${!_key}";
    _key='APP_CONNECTIONS_'"$(printf '%s' "${_build_database_type}" | awk '{ print toupper($0) }')"'_'"$(printf '%s' "${_build_database_connection_key}" | awk '{ print toupper($0) }')"'_TYPE';
    if [[ "${!_key}" = "" ]]; then
      debug "No default database connection type defined.";
      declare -r _build_database_connection_type="{_build_database_type}";
    else
      declare -r _build_database_connection_type="${!_key}";
    fi
    _key='APP_CONNECTIONS_'"$(printf '%s' "${_build_database_type}" | awk '{ print toupper($0) }')"'_'"$(printf '%s' "${_build_database_connection_key}" | awk '{ print toupper($0) }')"'_HOST';
    if [[ "${!_key}" = "" ]]; then
      debug "No default database connection host defined.";
      declare -r _build_database_connection_host="127.0.0.1";
    else
      declare -r _build_database_connection_host="${!_key}";
    fi
    _key='APP_CONNECTIONS_'"$(printf '%s' "${_build_database_type}" | awk '{ print toupper($0) }')"'_'"$(printf '%s' "${_build_database_connection_key}" | awk '{ print toupper($0) }')"'_PORT';
    if [[ "${!_key}" = "" ]]; then
      debug "No default database connection port defined.";
      declare -r _build_database_connection_port="3306";
    else
      declare -r _build_database_connection_port="${!_key}";
    fi
    _key='APP_CONNECTIONS_'"$(printf '%s' "${_build_database_type}" | awk '{ print toupper($0) }')"'_'"$(printf '%s' "${_build_database_connection_key}" | awk '{ print toupper($0) }')"'_USER';
    if [[ "${!_key}" = "" ]]; then
      debug "No default database connection user defined.";
      declare -r _build_database_connection_user="root";
    else
      declare -r _build_database_connection_user="${!_key}";
    fi
    _key='APP_CONNECTIONS_'"$(printf '%s' "${_build_database_type}" | awk '{ print toupper($0) }')"'_'"$(printf '%s' "${_build_database_connection_key}" | awk '{ print toupper($0) }')"'_PASSWORD';
    if [[ "${!_key}" = "" ]]; then
      debug "No default database connection password defined.";
      declare -r _build_database_connection_password="";
    else
      declare -r _build_database_connection_password="${!_key}";
    fi
    _key='APP_CONNECTIONS_'"$(printf '%s' "${_build_database_type}" | awk '{ print toupper($0) }')"'_'"$(printf '%s' "${_build_database_connection_key}" | awk '{ print toupper($0) }')"'_SCHEMA';
    if [[ "${!_key}" = "" ]]; then
      debug "No default database connection schema defined.";
      declare -r _build_database_connection_schema="";
    else
      declare -r _build_database_connection_schema="${!_key}";
    fi
  fi
fi

#info "Database type [${_c_yellow}${_build_database_type}${_c_cyan}]";
#info "Database connection key [${_c_yellow}${_build_database_connection_key}${_c_cyan}]";
#info "Database connection host [${_c_yellow}${_build_database_connection_host}${_c_cyan}]";
#info "Database connection port [${_c_yellow}${_build_database_connection_port}${_c_cyan}]";
#info "Database connection schema [${_c_yellow}${_build_database_connection_schema}${_c_cyan}]";
#info "Database connection user [${_c_yellow}${_build_database_connection_user}${_c_cyan}]";
#info "Database connection password [${_c_yellow}${_build_database_connection_password}${_c_cyan}]";

## Set a readonly reference to the current working directory
#declare -r _home="$HOME";
#
## Define the module path
#declare -r _module_config="${_modules}/config";
#
## Load the default config
#source "${_module_config}/config/defaults.sh";
#
#if [ -f "${_module_config}/config/config.sh" ]; then
#  source "${_module_config}/config/config.sh";
#fi
create_custom_config 'build';
load_custom_config 'build';