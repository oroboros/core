# This file defines the parameter mapping for the config module.

# Commands
_cmd["-b"]="_build_index";
_cmd["--build"]="_build_index";
#
## Flags
_flags+=("-c");
_flags+=("--config");
_flags+=("-v");
_flags+=("--verify");
_flags+=("-s");
_flags+=("--status");
_flags+=("-r");
_flags+=("--reset");
_flags+=("-l");
_flags+=("--latest");
_flags+=("-d");
_flags+=("--dev");
_flags+=("--interactive");

