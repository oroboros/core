## This file controls persistent configurations

## Set a readonly reference to the current working directory
#declare -r _home="$HOME";
#
## Define the module path
#declare -r _module_config="${_modules}/config";
#
## Load the default config
#source "${_module_config}/config/defaults.sh";
#
#if [ -f "${_module_config}/config/config.sh" ]; then
#  source "${_module_config}/config/config.sh";
#fi
create_custom_config 'config';
load_custom_config 'config';