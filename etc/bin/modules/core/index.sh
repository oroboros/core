# This file defines the parameter mapping for the config module.

# Commands
_cmd["-c"]="_core_index";
_cmd["--core"]="_core_index";
#
## Flags
_flags+=("-c");
_flags+=("--config");
_flags+=("-v");
_flags+=("--verify");
_flags+=("-s");
_flags+=("--status");
_flags+=("-r");
_flags+=("--reset");
_flags+=("-l");
_flags+=("--latest");
_flags+=("-d");
_flags+=("--dev");
_flags+=("-h");
_flags+=("--help");
_flags+=("--debug");
_flags+=("--quiet");
_flags+=("--silent");
_flags+=("--interactive");
_flags+=("--daemonize");

