## This file controls persistent configurations
#if [[ "${APP_CONNECTIONS_DEFAULTS_DATABASE}" = "" ]]; then
#  debug "No application default database type defined.";
#else
#  declare -r _build_database_type="${APP_CONNECTIONS_DEFAULTS_DATABASE}";
#    _key='APP_CONNECTIONS_DEFAULTS_'"$(printf '%s' "${_build_database_type}" | awk '{ print toupper($0) }')";
#    _value="";
#  if [[ "${!_key}" = "" ]]; then
#    debug "No default database connection key defined."
#  else
#    declare -r _build_database_connection_key="${!_key}";
#    _key='APP_CONNECTIONS_'"$(printf '%s' "${_build_database_type}" | awk '{ print toupper($0) }')"'_'"$(printf '%s' "${_build_database_connection_key}" | awk '{ print toupper($0) }')"'_TYPE';
#    _value="";
#    if [[ "${!_key}" = "" ]]; then
#      debug "No default database connection type defined.";
#    else
#      _value="${!_key}";
#    fi
#    declare -r _build_database_connection_type="${_value}";
#    _key='APP_CONNECTIONS_'"$(printf '%s' "${_build_database_type}" | awk '{ print toupper($0) }')"'_'"$(printf '%s' "${_build_database_connection_key}" | awk '{ print toupper($0) }')"'_HOST';
#    _value="";
#    if [[ "${!_key}" = "" ]]; then
#      debug "No default database connection host defined.";
#      _value="127.0.0.1";
#    else
#      _value="${!_key}";
#    fi
#    declare -r _build_database_connection_host="${_value}";
#    _key='APP_CONNECTIONS_'"$(printf '%s' "${_build_database_type}" | awk '{ print toupper($0) }')"'_'"$(printf '%s' "${_build_database_connection_key}" | awk '{ print toupper($0) }')"'_PORT';
#    _value="";
#    if [[ "${!_key}" = "" ]]; then
#      debug "No default database connection port defined.";
#      _value="3306";
#    else
#      _value="${!_key}";
#    fi
#    declare -r _build_database_connection_port="${_value}";
#    _key='APP_CONNECTIONS_'"$(printf '%s' "${_build_database_type}" | awk '{ print toupper($0) }')"'_'"$(printf '%s' "${_build_database_connection_key}" | awk '{ print toupper($0) }')"'_USER';
#    _value="";
#    if [[ "${!_key}" = "" ]]; then
#      debug "No default database connection user defined.";
#      _value="root";
#    else
#      _value="${!_key}";
#    fi
#    declare -r _build_database_connection_user="${_value}";
#    _key='APP_CONNECTIONS_'"$(printf '%s' "${_build_database_type}" | awk '{ print toupper($0) }')"'_'"$(printf '%s' "${_build_database_connection_key}" | awk '{ print toupper($0) }')"'_PASSWORD';
#    _value="";
#    if [[ "${!_key}" = "" ]]; then
#      debug "No default database connection password defined.";
#    else
#      _value="${!_key}";
#    fi
#    declare -r _build_database_connection_password="${_value}";
#    _key='APP_CONNECTIONS_'"$(printf '%s' "${_build_database_type}" | awk '{ print toupper($0) }')"'_'"$(printf '%s' "${_build_database_connection_key}" | awk '{ print toupper($0) }')"'_SCHEMA';
#    _value="${!_key}";
#    if [[ "${!_key}" = "" ]]; then
#      debug "No default database connection schema defined.";
#    else
#      _value="${!_key}";
#    fi
#    declare -r _build_database_connection_schema="${_value}";
#  fi
#fi

#info "Database type [${_c_yellow}${_build_database_type}${_c_cyan}]";
#info "Database connection key [${_c_yellow}${_build_database_connection_key}${_c_cyan}]";
#info "Database connection host [${_c_yellow}${_build_database_connection_host}${_c_cyan}]";
#info "Database connection port [${_c_yellow}${_build_database_connection_port}${_c_cyan}]";
#info "Database connection schema [${_c_yellow}${_build_database_connection_schema}${_c_cyan}]";
#info "Database connection user [${_c_yellow}${_build_database_connection_user}${_c_cyan}]";
#info "Database connection password [${_c_yellow}${_build_database_connection_password}${_c_cyan}]";

## Set a readonly reference to the current working directory
#declare -r _home="$HOME";
#
## Define the module path
#declare -r _module_config="${_modules}/config";
#
## Load the default config
#source "${_module_config}/config/defaults.sh";
#
#if [ -f "${_module_config}/config/config.sh" ]; then
#  source "${_module_config}/config/config.sh";
#fi
declare -a _build=();
declare -A _build_defaults=();
_build_defaults['database_root_user']='root';
_build_defaults['database_root_password']='';
_build_defaults['database_root_host']='localhost';
_build_defaults['database_root_port']='3306';
_build_defaults['database_root_schema']='';
_build_defaults['database_test_user']='test';
_build_defaults['database_test_password']='';
_build_defaults['database_test_host']='localhost';
_build_defaults['database_test_port']='3306';
_build_defaults['database_test_schema']='test';
_build_defaults['database_save_path']="${HOME}/.oroboros/backups/database";
_build_defaults['database_backup_minimum_disk_space']='2147483648';
create_custom_config 'core';
load_custom_config 'core';