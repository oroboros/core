# Function declarations for the core module

# Control method for core operations. Routes commands to the appropriate handler method.
# All methods passed to the core module call this method first, which aggregates sub-commands
# to various other functions listed here based on the sub-commands passed.

# ------------------------------------------------------------------------------
#                             Core Index Method
# ------------------------------------------------------------------------------
_core_index() {
  load_custom_config "core";
  if has_flag "-v" || has_flag "--verify"; then
    _core_verify_environment;
    _core_verify_application_environment;
  elif has_flag "-s" || has_flag "--status"; then
    _core_status;
  elif has_flag "-l" || has_flag "--latest"; then
    _core_verify_environment;
    if [ ! $? -eq 0 ]; then
      error "Cannot run the latest core because there are environment issues.";
      return 1;
    fi
    _core_verify_application_environment;
    if [ ! $? -eq 0 ]; then
      error "Cannot run core functionality until the core environment is configured.";
      return 1;
    fi
    info "Proceeding with the latest core.";
    
  elif has_flag "-d" || has_flag "--dev"; then
    _core_verify_environment;
    if [ ! $? -eq 0 ]; then
      error "Cannot run the latest development core because there are environment issues.";
      return 1;
    fi
    _core_verify_application_environment;
    if [ ! $? -eq 0 ]; then
      error "Cannot run the latest development core because the application environment is not configured.";
      return 1;
    fi
    info "Proceeding with the latest development core.";
    
  elif has_flag "-r" || has_flag "--reset"; then
    :
  elif has_flag "-c" || has_flag "--config"; then
    _core_config;
  else
    # Display the core help menu index
    _core_help_index;
  fi
  terminate;
}

# ------------------------------------------------------------------------------
#                                Build Controllers
# ------------------------------------------------------------------------------

# Checks the status of the last core operation ran
_core_status() {
  :
}

# Builds the most recent production release
_core_release() {
  :
}

# Builds the most recent development release
_core_dev() {
  :
}

# Verifies the environment and dependencies needed to run a clean core.
_core_verify() {
  :
}

# Performs a full reset back to the last clean core state.
_core_reset() {
  :
}

# Handles the configuration of the core platform.
_core_config() {
  local _designation=0;
  info "This is the core configuration.";
  return $_designation;
}

# ------------------------------------------------------------------------------
#                                Build Internals
# ------------------------------------------------------------------------------

_core_verify_environment() {
#  local _con="$(_core_get_connection_string)";
  local _method="";
  local verifier_key="";
  local _designation=0;
  local -A _required=();
  local -a _required_valid=();
  local -a _required_invalid=();
  local -A _optional=();
  local -a _optional_valid=();
  local -a _optional_invalid=();
  local _core_verified=0;
  local _core_optional_verified=0;
  local _os="$(os_distro)";
  _required['network']=0;
  _required['ctrl']=0;
  _required['git']=0;
  _required['mysql']=0;
  _required['php']=0;
  _required['nodejs']=0;
  _required['npm']=0;
  _required['webserver']=0;
  _optional['supervisor']=0;
  _optional['beanstalk']=0;
  _optional['redis']=0;
  info "Verifying the local environment and dependencies.";
  info "Detected operating system [${_c_yellow}${_os}${_c_cyan}]";
  debug "Verifying required dependencies...";
  for verifier_key in "${!_required[@]}"; do
    _method="_core_verify_${verifier_key}";
    $_method;
    if [ $? -eq 0 ]; then
      _required_valid+=("$verifier_key");
    else
      _required_invalid+=("$verifier_key");
    fi
  done;
  debug "Verifying optional dependencies...";
  for verifier_key in "${!_optional[@]}"; do
    _method="_core_verify_${verifier_key}";
    if ! [[ $(type -t $_method &2>/dev/null) = 'function' ]]; then
      error "Expected function [${_c_yellow}${_method}${_c_orange}] does not exist.";
      return 1;
    fi
    $_method;
    if [ $? -eq 0 ]; then
      _optional_valid+=("$verifier_key");
    else
      _optional_invalid+=("$verifier_key");
    fi
  done;

  debug "Generating verifier designation...";
  for verifier_key in "${_required_invalid[@]}"; do
    _core_verified=1;
  done;
  for verifier_key in "${_optional_invalid[@]}"; do
    _core_optional_verified=1;
  done;
  if [ ! $_core_verified -eq 0 ]; then
    error "There are blocking errors, core cannot proceed.";
    if [ ! $_core_optional_verified -eq 0 ]; then
      warning "Some optional dependencies are also invalid.";
    fi
  elif [ ! $_core_optional_verified -eq 0 ]; then
    warning "Some optional dependencies are invalid. The core can proceed,\n              but optional invalid functionality will be omitted in this environment.";
  fi
  for verifier_key in "${_required_invalid[@]}"; do
    error "Required dependency [${_c_yellow}${verifier_key}${_c_orange}] is not valid.";
  done;
  for verifier_key in "${_optional_invalid[@]}"; do
    warning "Optional dependency [${_c_clear}${verifier_key}${_c_yellow}] is not valid.";
  done;
  if [ $_core_verified -eq 0 ] && [ $_core_optional_verified -eq 0 ]; then
    success "All environment settings and dependencies are valid.";
  fi
  return $_core_verified;
}
_core_status() {
  local _pwd="$(pwd)";
  local verifier_key="";
  local _designation=0;
  local -a _required=();
  _required+=('os');
  _required+=('ctrl');
  _required+=('php');
  _required+=('git');
  _required+=('webserver');
  _required+=('nodejs');
  _required+=('npm');
  _required+=('environment');
  local -a _required_valid=();
  local -a _required_invalid=();
  local -a _optional=();
  _optional+=('composer');
  _optional+=('supervisor');
  _optional+=('beanstalk');
  _optional+=('memcache');
  _optional+=('redis');
  _optional+=('mysql');
  local -a _optional_valid=();
  local -a _optional_invalid=();
  local _core_verified=0;
  local _core_optional_verified=0;
  local _os="$(os_distro)";
  _required+=('core_environment');
  _required+=('core');
  _required+=('registration');
  _required+=('environment');
  _required+=('app');
  info "Checking the core status in [${_c_yellow}${_dir}${_c_cyan}].";
  debug "Verifying status...";
  for verifier_key in "${_required[@]}"; do
    _method="_core_status_${verifier_key}";
    if ! [[ $(type -t $_method &2>/dev/null) = 'function' ]]; then
      error "Expected function [${_c_yellow}${_method}${_c_orange}] does not exist.";
      return 1;
    fi
    debug "Checking required functionality for [${_c_yellow}${verifier_key}${_c_gray}]";
    $_method;
    if [ $? -eq 0 ]; then
      _required_valid+=("$verifier_key");
    else
      _required_invalid+=("$verifier_key");
    fi
  done;
  debug "Verifying optional updates...";
  for verifier_key in "${_optional[@]}"; do
    _method="_core_status_${verifier_key}";
    debug "Checking optional functionality for [${_c_yellow}${verifier_key}${_c_gray}]";
    $_method;
    if [ $? -eq 0 ]; then
      _optional_valid+=("$verifier_key");
    else
      _optional_invalid+=("$verifier_key");
    fi
  done;

  debug "Generating status designation...";
  for verifier_key in "${_required_invalid[@]}"; do
    _core_verified=1;
  done;
  for verifier_key in "${_optional_invalid[@]}"; do
    _core_optional_verified=1;
  done;
  if [ ! $_core_verified -eq 0 ]; then
    error "There are blocking errors, core cannot proceed.";
    if [ ! $_core_optional_verified -eq 0 ]; then
      warning "Some optional dependencies are also invalid.";
    fi
  elif [ ! $_core_optional_verified -eq 0 ]; then
    warning "Some optional dependencies are invalid. The core can proceed,\n              but optional invalid functionality will be omitted in this environment.";
  fi
  for verifier_key in "${_required_invalid[@]}"; do
    error "Required dependency [${_c_yellow}${verifier_key}${_c_orange}] is not valid.";
  done;
  for verifier_key in "${_optional_invalid[@]}"; do
    warning "Optional dependency [${_c_clear}${verifier_key}${_c_yellow}] is not valid.";
  done;
  if [ $_core_verified -eq 0 ] && [ $_core_optional_verified -eq 0 ]; then
    success "All environment settings and dependencies are valid.";
  fi
  return $_core_verified;
}

_core_verify_os() {
  local _designation=0;
  debug "Verifying operating system...";
  local _os="$(os_type)";
  if [ ! $? -eq 0 ]; then
    error "Operating system could not be determined.";
    return 1;
  fi
  info "Detected operating system [${_c_yellow}${_os}${_c_cyan}].";
  return $_designation;
}

_core_status_os() {
  return 0;
}

_core_verify_ctrl() {
  local _designation=0;
  debug "Verifying binary controller...";
  local _os="$(os_distro)";
  local _ctrl="$(os_binary_ctrl)";
  if [ ! $? -eq 0 ]; then
    error "Binary controller could not be determined.";
    return 1;
  fi
  info "Binary controller for [${_c_yellow}${_os}${_c_cyan}] located at [${_c_yellow}${_ctrl}${_c_cyan}].";
  return $_designation;
}

_core_status_ctrl() {
  return 0;
}

_core_verify_application_environment() {
  local _pwd="$(pwd)";
  local _env_file="${_pwd}/.env";
  if [ ! -f "${_pwd}/.env" ]; then
    error "No application environment file detected at [${_c_yellow}${_env_file}${_c_orange}].";
    return 1;
  fi
  info "Importing application environment detected at [${_c_yellow}${_env_file}${_c_cyan}].";
  source "${_env_file}";
  return 0;
}

_core_verify_registration() {
  local _designation=0;
  debug "Verifying application registration...";
  
  return $_designation;
}

_core_status_registration() {
  return 0;
}

_core_verify_network() {
  local _designation=0;
  network_verify;
  _designation=$?;
  debug "Verifying network connectivity...";
  if [ ! $? -eq 0 ]; then
    error "No internet connectivity detected.";
    return 1;
  fi
  info "Network connectivity is active.";
  return $_designation;
}

_core_status_network() {
  _core_verify_network;
  return $?;
}

_core_verify_git() {
  local _designation=0;
  local _core_git_binary="";
  local _core_git_version="";
  debug "Verifying git installation...";
  _core_git_binary="$(git_binary)";
  if [ ! $? -eq 0 ]; then
    error "No git installation detected.";
    return 1;
  fi
  _core_git_version="$(git_version)";
  info "Git detected at [${_c_yellow}${_core_git_binary}${_c_cyan}] with version [${_c_yellow}${_core_git_version}${_c_cyan}].";
  return $_designation;
}

_core_status_git() {
  _core_verify_git;
  return $?;
}

_core_verify_php() {
  local _designation=0;
  local _core_php_binary="";
  local _core_php_version="";
  debug "Verifying php installation...";
  _core_php_binary="$(php_binary)";
  if [ ! $? -eq 0 ]; then
    error "No php installation detected.";
    return 1;
  fi
  _core_php_version="$(php_version)";
  info "PHP detected at [${_c_yellow}${_core_php_binary}${_c_cyan}] with version [${_c_yellow}${_core_php_version}${_c_cyan}].";
  return $_designation;
}

_core_status_php() {
  _core_verify_php;
  return $?;
}

_core_verify_nodejs() {
  local _designation=0;
  local _core_nodejs_binary="";
  local _core_nodejs_version="";
  debug "Verifying node.js installation...";
  _core_nodejs_binary="$(nodejs_binary)";
  if [ ! $? -eq 0 ]; then
    error "No nodejs installation detected.";
    return 1;
  fi
  _core_nodejs_version="$(nodejs_version)";
  info "Node.js detected at [${_c_yellow}${_core_nodejs_binary}${_c_cyan}] with version [${_c_yellow}${_core_nodejs_version}${_c_cyan}].";
  return $_designation;
}

_core_status_nodejs() {
  _core_verify_nodejs;
  return $?;
}

_core_verify_npm() {
  local _designation=0;
  local _core_npm_binary="";
  local _core_npm_version="";
  debug "Verifying npm installation...";
  _core_npm_binary="$(npm_binary)";
  if [ ! $? -eq 0 ]; then
    error "No npm installation detected.";
    return 1;
  fi
  _core_npm_version="$(npm_version)";
  info "NPM detected at [${_c_yellow}${_core_npm_binary}${_c_cyan}] with version [${_c_yellow}${_core_npm_version}${_c_cyan}].";
  return $_designation;
}

_core_status_npm() {
  _core_verify_npm;
  return $?;
}

_core_verify_webserver() {
  local _designation=0;
  local _core_apache_binary="";
  local _core_apache_version="";
  debug "Verifying apache installation...";
  _core_apache_binary="$(apache_binary)";
  if [ ! $? -eq 0 ]; then
    error "No apache installation detected.";
    return 1;
  fi
  _core_apache_version="$(apache_version)";
  info "Apache detected at [${_c_yellow}${_core_apache_binary}${_c_cyan}] with version [${_c_yellow}${_core_apache_version}${_c_cyan}].";
  return $_designation;
}

_core_status_webserver() {
  local _apache="$(apache_binary_type)";
  if [ ! $? -eq 0 ] || [ -z "${_apache}" ]; then
    error "Cannot check webserver status because the apache binary could not be found.";
    return 1;
  fi
  local _stat="$(ps -eo comm,etime,user | grep "${_apache}" | grep -v root | cut -d$'\n' -f 1 | awk '$1=$1')";
  if [ ! $? -eq 0 ]; then
    error "Web server [${_c_yellow}${_apache}${_c_orange}] is down.";
    return 1;
  fi
  local _user="$(echo "${_stat}" | cut -d' ' -f3)";
  local _uptime="$(echo "${_stat}" | cut -d' ' -f2)";
  info "Web server [${_c_yellow}${_apache}${_c_cyan}] running since [${_c_yellow}${_uptime}${_c_cyan}] under user [${_c_yellow}${_user}${_c_cyan}].";
  return 0;
}

_core_verify_redis() {
  local _designation=0;
  local _core_redis_binary="";
  local _core_redis_version="";
  debug "Verifying redis installation...";
  _core_redis_binary="$(redis_binary)";
  if [ ! $? -eq 0 ]; then
    error "No redis server installation detected.";
    return 1;
  fi
  _core_redis_version="$(redis_version)";
  info "Redis server detected at [${_c_yellow}${_core_redis_binary}${_c_cyan}] with version [${_c_yellow}${_core_redis_version}${_c_cyan}].";
  return $_designation;
}

_core_status_redis() {
  local _redis="$(redis_binary_type)";
  if [ ! $? -eq 0 ] || [ -z "${_redis}" ]; then
    error "Cannot check redis status because the redis-server binary could not be found.";
    return 1;
  fi
  local _stat="$(ps -eo comm,etime,user | grep "${_redis}" | grep -v root | cut -d$'\n' -f 1 | awk '$1=$1')";
  if [ ! $? -eq 0 ]; then
    error "Redis server [${_c_yellow}${_redis}${_c_orange}] is down.";
    return 1;
  fi
  local _user="$(echo "${_stat}" | cut -d' ' -f3)";
  local _uptime="$(echo "${_stat}" | cut -d' ' -f2)";
  info "Redis server [${_c_yellow}${_redis}${_c_cyan}] running since [${_c_yellow}${_uptime}${_c_cyan}] under user [${_c_yellow}${_user}${_c_cyan}].";
  return 0;
}

_core_verify_memcache() {
  local _designation=0;
  local _core_memcache_binary="";
  local _core_memcache_version="";
  debug "Verifying memcache installation...";
  _core_memcache_binary="$(memcache_binary)";
  if [ ! $? -eq 0 ]; then
    error "No memcache server installation detected.";
    return 1;
  fi
  _core_memcache_version="$(memcache_version)";
  info "Memcache server detected at [${_c_yellow}${_core_memcache_binary}${_c_cyan}] with version [${_c_yellow}${_core_memcache_version}${_c_cyan}].";
  return $_designation;
}

_core_status_memcache() {
  return 0;
}

_core_verify_mysql() {
  local _designation=0;
  local _core_mysql_binary="";
  local _core_mysql_version="";
  debug "Verifying mysql installation...";
  _core_mysql_binary="$(mysql_binary)";
  if [ ! $? -eq 0 ]; then
    error "No mysql installation detected.";
    return 1;
  fi
  _core_mysql_version="$(mysql_version)";
  info "MySQL client detected at [${_c_yellow}${_core_mysql_binary}${_c_cyan}] with version [${_c_yellow}${_core_mysql_version}${_c_cyan}].";
  return $_designation;
}

_core_status_mysql() {
  return 0;
}

_core_verify_composer() {
  local _designation=0;
  debug "Verifying composer installation...";
  
  return $_designation;
}

_core_status_composer() {
  return 0;
}

_core_verify_supervisor() {
  local _designation=0;
  local _core_supervisor_binary="";
  local _core_supervisor_version="";
  debug "Verifying supervisor installation...";
  _core_supervisor_binary="$(supervisor_binary)";
  if [ ! $? -eq 0 ]; then
    error "No supervisor installation detected.";
    return 1;
  fi
  _core_supervisor_version="$(supervisor_version)";
  info "Supervisor detected at [${_c_yellow}${_core_supervisor_binary}${_c_cyan}] with version [${_c_yellow}${_core_supervisor_version}${_c_cyan}].";
  return $_designation;
}

_core_status_supervisor() {
  return 0;
}

_core_verify_beanstalk() {
  local _designation=0;
  local _core_beanstalk_binary="";
  local _core_beanstalk_version="";
  debug "Verifying beanstalk installation...";
  _core_beanstalk_binary="$(beanstalk_binary)";
  if [ ! $? -eq 0 ]; then
    error "No beanstalk installation detected.";
    return 1;
  fi
  _core_beanstalk_version="$(beanstalk_version)";
  info "Beanstalk detected at [${_c_yellow}${_core_beanstalk_binary}${_c_cyan}] with version [${_c_yellow}${_core_beanstalk_version}${_c_cyan}].";
  return $_designation;
}

_core_status_beanstalk() {
  return 0;
}

_core_status_registration() {
  local _designation=0;
  debug "Verifying application registration status...";
  return $_designation;
}

_core_status_core() {
  local _designation=0;
  debug "Verifying core platform core status...";
  return $_designation;
}

_core_status_app() {
  local _designation=0;
  debug "Verifying application core status...";
  return $_designation;
}

_core_status_core_environment() {
  local _designation=0;
  debug "Verifying core platform environment status...";
  return $_designation;
}

_core_status_environment() {
  local _designation=0;
  debug "Verifying application environment status...";
  return $_designation;
}

_core_mark_progress() {
  local _core_step="$1";
}

_core_check_progress() {
  :
}

_core_mark_complete() {
  :
}

_core_mark_failed() {
  :
}

_core_detect_systemctl() {
  :
}

_core_detect_brew() {
  :
}

# ------------------------------------------------------------------------------
#                               Help Documentation
# ------------------------------------------------------------------------------

# Help menu for database archive list command
_core_help_index() {
  clear;
  printf "\n${_c_cyan}${_f_bold}Core Help\n${_c_clear}\n";
  echo '----------------------------------------------------------------------';
  printf "\n";
  printf "This is the core help menu for the Oroboros command line api.\n\n";
  printf "${_f_bold}Checks the status of the core platform:\n${_c_clear}";
  printf "${_c_red}-c --status${_c_clear}\n\n";
  printf "${_f_bold}Sets up the local configuration for the core platform:\n${_c_clear}";
  printf "${_f_italic}This ${_f_bold}MUST${_c_clear}${_f_italic} be run at least one time!\n${_c_clear}";
  printf "${_c_red}-c --config${_c_clear}\n\n";
  printf "${_f_bold}Builds the core application platform from the latest release:\n${_c_clear}";
  printf "${_c_red}-c --latest${_c_clear}\n\n";
  printf "${_f_bold}Builds the core application platform from the latest dev release:\n${_c_clear}";
  printf "${_c_red}-c --dev${_c_clear}\n\n";
  printf "${_f_bold}Checks the local environment for compatibility:\n${_c_clear}";
  printf "${_c_red}-c --verify${_c_clear}\n\n";
  printf "${_f_bold}Resets to the last clean core application platform state and rebuilds the core:\n${_c_clear}";
  printf "${_c_red}-c --reset${_c_clear}\n\n";
  printf "\n";
  printf "${_c_yellow}${_f_bold}Flags${_c_clear}${_f_bold}(format ${_c_clear}${_c_red}--key${_c_clear}${_f_bold})${_c_clear}${_f_bold}${_c_yellow}:\n\n${_c_clear}";
  printf "${_c_red}--force${_c_clear}                 Forces the core command to run regardless of errors.\n";
  printf "${_c_red}--debug${_c_clear}                 Displays additional verbose information about underlying logic for debugging.\n";
  printf "${_c_red}--quiet${_c_clear}                 Hides all output except error messages.\n";
  printf "${_c_red}--silent${_c_clear}                Hides all output including error messages.\n";
  printf "${_c_red}--daemonize${_c_clear}             Runs the requested core process in the background.\n";
  printf "\n";
#  printf "${_f_bold}Related:${_c_clear}${_f_italic}${_c_gray} Config Help:${_c_clear}       ${_c_red}oroboros -d --config -h${_c_clear}\n";
#  printf "${_f_bold}Related:${_c_clear}${_f_italic}${_c_gray} List Commit Help:${_c_clear}  ${_c_red}oroboros -d --list-versions -h${_c_clear}\n";
#  printf "${_f_bold}Related:${_c_clear}${_f_italic}${_c_gray} Commit Help:${_c_clear}       ${_c_red}oroboros -d -c -h${_c_clear}\n";
#  printf "${_f_bold}Related:${_c_clear}${_f_italic}${_c_gray} Backup Help:${_c_clear}       ${_c_red}oroboros -d -b -h${_c_clear}\n";
#  printf "${_f_bold}Related:${_c_clear}${_f_italic}${_c_gray} Build Help:${_c_clear}        ${_c_red}oroboros -d --core -h${_c_clear}\n";
#  printf "${_f_bold}Related:${_c_clear}${_f_italic}${_c_gray} Rollback Help:${_c_clear}     ${_c_red}oroboros -d --rollback -h${_c_clear}\n";
  printf "\n";
  exit 0;
}