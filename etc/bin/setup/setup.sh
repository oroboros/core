#!/bin/bash

# Set a readonly reference to the current working directory
declare -r _library="${_script_directory}/lib";

# Set a readonly reference to the current user
declare -r _usr="$(whoami)";

# Declare the current command line library version
declare -r _version="1.0.0";

# Set the process id as a constant, for creating lock files and process associations.
declare -r _pid=$$;

# Set a readonly reference to the current working directory
declare -r _cwd="$(pwd)";

# Set readonly references to the directory structure
declare -r _lib=$_script_directory"/lib";
declare -r _modules=$_script_directory"/modules";
declare -r _lock=$_script_directory"/lock";
declare -r _log=$_script_directory"/log";
declare -r _tmp=$_script_directory"/tmp";
declare -r _app_script_directory="${_dir}/etc/bin)";
declare -r _app_lib=$_app_script_directory"/lib";
declare -r _app_modules=$_app_script_directory"/modules";
declare -r _app_log=$_app_script_directory"/log";
declare -r _app_tmp=$_app_script_directory"/tmp";
declare -r _oroboros_dir="${SOURCE%/*}";
declare -r _oroboros_binary="${_oroboros_dir}/$(basename "$(test -L "$0" && readlink "$0" || echo "$0")")";
declare -r _app_binary="${_dir}/app";
declare -r _oroboros_env_defaults="${_oroboros_dir}/.env.defaults";
declare -r _oroboros_env="${_oroboros_dir}/.env";
declare -r _app_env_defaults="${_dir}/.env.defaults";
declare -r _app_env="${_dir}/.env";

# Include the base default environment
if [ -f "${_oroboros_env_defaults}" ]; then
  source "${_oroboros_env_defaults}";
fi

# Include the base environment overrides
if [ -f "${_oroboros_env}" ]; then
  source "${_oroboros_env}";
fi

# Include the app default environment
if [ -f "${_app_env_defaults}" ]; then
  source "${_app_env_defaults}";
fi

# Include the app environment overrides
if [ -f "${_app_env}" ]; then
  source "${_app_env}";
fi

# Run the bootstrap
source $_lib"/errors/error_handling.sh";
source $_lib"/bootstrap.sh";

