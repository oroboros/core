-- Adminer 4.8.1 MySQL 5.5.5-10.6.4-MariaDB dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

CREATE DATABASE `oroboros` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;
USE `oroboros`;

CREATE TABLE `blacklist` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `dns` varbinary(16) NOT NULL COMMENT 'the dns record associated with the blacklist entry',
  `provider` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT 'app' COMMENT 'the identifier for the provider',
  `created_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'the date the blacklist record was created',
  `starts_at` datetime DEFAULT NULL COMMENT 'the date the blacklist record becomes active',
  `expires_at` datetime DEFAULT NULL COMMENT 'the date the blacklist record expires',
  `code` int(64) DEFAULT NULL COMMENT 'an optional status code for the blacklist record',
  `reason` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'a human readable reason for the blacklist entry',
  PRIMARY KEY (`id`),
  UNIQUE KEY `blacklist_index_dns` (`dns`),
  KEY `blacklist_index_details` (`created_at`,`starts_at`,`expires_at`,`code`),
  KEY `blacklist_index_provider` (`provider`),
  CONSTRAINT `blacklist_relation_provider` FOREIGN KEY (`provider`) REFERENCES `providers` (`key`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Contains a list of blacklisted dns records';


CREATE TABLE `components` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `key` varchar(64) COLLATE utf8mb4_bin NOT NULL COMMENT 'component identifier',
  `provider` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT 'app' COMMENT 'component provider',
  `type` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT 'html' COMMENT 'the type of component',
  `name` varchar(128) COLLATE utf8mb4_bin NOT NULL DEFAULT 'Untitled Component' COMMENT 'the human readable name for the component',
  `description` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'a brief description of the component',
  `class` varchar(1024) COLLATE utf8mb4_bin NOT NULL COMMENT 'the class representing a component instance',
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp() COMMENT 'the date the component record was added to the system',
  `modified_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'the date the component record was last modified',
  PRIMARY KEY (`id`),
  UNIQUE KEY `components_index_key` (`key`,`type`),
  KEY `components_index_provider` (`provider`),
  KEY `components_relation_type` (`type`),
  CONSTRAINT `components_relation_provider` FOREIGN KEY (`provider`) REFERENCES `providers` (`key`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `components_relation_type` FOREIGN KEY (`type`) REFERENCES `components_types` (`key`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Defines components known to the system';


CREATE TABLE `components_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `key` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT 'unique identifier for the component type',
  `provider` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT 'app' COMMENT 'provider of the component type',
  `name` varchar(128) COLLATE utf8mb4_bin NOT NULL DEFAULT 'Untitled Component Type' COMMENT 'human readable name of the component type',
  `description` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'brief description of the component type',
  PRIMARY KEY (`id`),
  UNIQUE KEY `components_types_index_key` (`key`),
  KEY `components_types_index_provider` (`provider`),
  CONSTRAINT `components_types_relation_provider` FOREIGN KEY (`provider`) REFERENCES `providers` (`key`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Defines valid types of components';


CREATE TABLE `content` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `key` varchar(128) COLLATE utf8mb4_bin NOT NULL COMMENT 'unique identifier for the content',
  `provider` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT 'app' COMMENT 'identifier for the content provider',
  `type` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT 'the content type identifier',
  `owner` int(11) unsigned NOT NULL COMMENT 'the owner identity of the content',
  `parent` int(11) unsigned DEFAULT NULL COMMENT 'the parent content that the content is encapsulated by, if any',
  `component` int(11) unsigned NOT NULL COMMENT 'the component used to serve the content',
  `value` text COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'the content payload',
  `permission` int(11) unsigned DEFAULT NULL COMMENT 'the permission required to access the content',
  `enabled` tinyint(3) unsigned NOT NULL DEFAULT 1 COMMENT 'whether or not the content is enabled',
  `locked` tinyint(3) unsigned NOT NULL DEFAULT 0 COMMENT 'whether or not the content is locked to prevent edits',
  `hidden` tinyint(3) unsigned NOT NULL DEFAULT 0 COMMENT 'whether or not the content is hidden from management gui''s',
  `public` tinyint(3) unsigned NOT NULL DEFAULT 0 COMMENT 'whether the content is publicly indexed for searches and display',
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp() COMMENT 'the date the content record was created',
  `modified_at` datetime NOT NULL COMMENT 'the date the content record was last modified',
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_index_key` (`key`,`type`,`owner`),
  UNIQUE KEY `content_index_permission` (`permission`),
  KEY `content_index_provider` (`provider`),
  KEY `content_index_component` (`component`),
  KEY `content_index_parent` (`parent`),
  KEY `content_relation_type` (`type`),
  KEY `content_relation_owner` (`owner`),
  CONSTRAINT `content_relation_component` FOREIGN KEY (`component`) REFERENCES `components` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `content_relation_owner` FOREIGN KEY (`owner`) REFERENCES `identity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `content_relation_parent` FOREIGN KEY (`parent`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `content_relation_provider` FOREIGN KEY (`provider`) REFERENCES `providers` (`key`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `content_relation_type` FOREIGN KEY (`type`) REFERENCES `content_types` (`key`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Defines individual content records';


CREATE TABLE `content_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `key` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT 'unique identifier for the content type',
  `provider` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT 'app' COMMENT 'identifier for the content type provider',
  `name` varchar(128) COLLATE utf8mb4_bin NOT NULL DEFAULT 'Untitled Content Type' COMMENT 'human readable name of the content type',
  `description` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'a brief description of the content type',
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_types_index_key` (`key`),
  KEY `content_types_index_provider` (`provider`),
  CONSTRAINT `content_types_relation_provider` FOREIGN KEY (`provider`) REFERENCES `providers` (`key`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Defines valid content types';


CREATE TABLE `dns` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `key` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT 'the unique key of the dns entry',
  `provider` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT 'app' COMMENT 'the identifier for the provider',
  `name` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'an optional human readable name for the dns entry',
  `description` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'an optional brief description of the dns entry''s purpose',
  `ip` varbinary(16) DEFAULT NULL COMMENT 'the ip address of the dns entry, if applicable',
  `ipv6` varchar(46) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'the text representation of an ipv6 address, if applicable',
  `octet_a` varchar(3) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'the first octet of an IPv4 address, if applicable',
  `octet_b` varchar(3) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'the second octet of an IPv4 address, if applicable. May also represent a wildcard.',
  `octet_c` varchar(3) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'the third octet of an IPv4 address, if applicable. May also represent a wildcard.',
  `octet_d` varchar(3) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'the fourth octet of an IPv4 address, if applicable. May also represent a wildcard.',
  `cidr` int(2) unsigned DEFAULT NULL COMMENT 'the cidr notation block, if applicable.',
  `wildcard` tinyint(1) unsigned NOT NULL DEFAULT 0 COMMENT 'whether or not the dns is a wildcard representation',
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT 1 COMMENT 'whether or not the dns entry is enabled',
  `system` tinyint(1) unsigned NOT NULL DEFAULT 0 COMMENT 'whether or not the dns entry is a system dns definition',
  `locked` tinyint(1) unsigned NOT NULL DEFAULT 0 COMMENT 'whether or not the dns entry is locked to prevent edits',
  `hidden` tinyint(1) unsigned NOT NULL DEFAULT 0 COMMENT 'whether or not the dns entry is hidden from management pages',
  `expires_at` datetime DEFAULT NULL COMMENT 'the date the dns entry expires on, if applicable',
  `created_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'the date the dns entry was created',
  `modified_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'the date the dns entry was last modified',
  PRIMARY KEY (`id`),
  UNIQUE KEY `dns_index_key` (`key`),
  KEY `dns_index_provider` (`provider`),
  KEY `dns_index_ipv4` (`octet_a`,`octet_b`,`octet_c`,`octet_d`,`cidr`),
  KEY `dns_index_inet` (`ip`),
  KEY `dns_index_ipv6` (`ipv6`),
  KEY `dns_index_bool` (`wildcard`,`enabled`,`system`,`locked`,`hidden`),
  KEY `dns_index_dates` (`expires_at`,`created_at`,`modified_at`),
  CONSTRAINT `dns_relation_provider` FOREIGN KEY (`provider`) REFERENCES `providers` (`key`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Defines dns records of visitors and known endpoints';


CREATE TABLE `ghostlist` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `dns` varbinary(16) NOT NULL COMMENT 'the dns record associated with the ghostlist entry',
  `provider` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT 'app' COMMENT 'the identifier for the provider',
  `created_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'the date the ghostlist record was created',
  `starts_at` datetime DEFAULT NULL COMMENT 'the date the ghostlist record becomes active',
  `expires_at` datetime DEFAULT NULL COMMENT 'the date the ghostlist record expires',
  `code` int(64) DEFAULT NULL COMMENT 'an optional status code for the ghostlist record',
  `reason` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'a human readable reason for the ghostlist entry',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ghostlist_index_dns` (`dns`),
  KEY `ghostlist_index_details` (`created_at`,`starts_at`,`expires_at`,`code`),
  KEY `ghostlist_index_provider` (`provider`),
  CONSTRAINT `ghostlist_relation_provider` FOREIGN KEY (`provider`) REFERENCES `providers` (`key`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Contains a list of ghostlisted dns records';


CREATE TABLE `identity` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `identity` varchar(64) COLLATE utf8mb4_bin NOT NULL COMMENT 'the uid for the identity',
  `network` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '*' COMMENT 'The primary network associated with the identity',
  `organization` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '*' COMMENT 'The primary organization associated with the identity',
  `type` varchar(16) COLLATE utf8mb4_bin NOT NULL COMMENT 'The type of identity provided',
  `provider` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT 'app' COMMENT 'the identifier for the provider',
  `enabled` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'Designates whether the identity is enabled',
  `verified` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Designates whether the identity is verified',
  `locked` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Designates whether the identity is locked in a read-only state. This is generally the case for system and recovery identities.',
  `hidden` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Designates whether the identity is hidden from identity management pages. This is generally the case for locked identities.',
  `public` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Designates whether the identity is publicly searchable. This is always false by default. Admins can configure specific identities to be publicly indexed if their application functionality warrants this behavior.',
  `created_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'The date the record was entered',
  `modified_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'The date the record was last modified',
  PRIMARY KEY (`id`),
  UNIQUE KEY `identity_index_identity` (`identity`,`organization`,`network`,`type`),
  KEY `identity_relation_type` (`type`),
  KEY `identity_relation_organization` (`organization`,`network`),
  KEY `identity_index_provider` (`provider`),
  CONSTRAINT `identity_relation_organization` FOREIGN KEY (`organization`, `network`) REFERENCES `organizations` (`key`, `host`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `identity_relation_provider` FOREIGN KEY (`provider`) REFERENCES `providers` (`key`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `identity_relation_type` FOREIGN KEY (`type`) REFERENCES `identity_types` (`key`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Defines identities known to the system';


CREATE TABLE `identity_credentials` (
  `id` int(11) unsigned NOT NULL COMMENT 'primary key',
  `identity` int(11) unsigned NOT NULL COMMENT 'the associated identity record',
  `type` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'password' COMMENT 'the type of credential represented by the record',
  `provider` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT 'app' COMMENT 'the identifier for the provider',
  `password` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT 'NO_PASSWORD_DEFINED' COMMENT 'the password hash, if applicable. Plaintext entries are handled in the application.',
  `force_reset` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'whether or not the credentials require a force reset on the next login or page visit. If this is 1, the user must update the credential before taking any other login action',
  `compromised` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'whether or not the credentials are flagged as compromised',
  `created_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'the date the credentials record was created',
  `modified_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'the date the credentials record was last modified',
  PRIMARY KEY (`id`),
  UNIQUE KEY `identity_credentials_index_identity` (`identity`,`type`),
  KEY `identity_credentials_index_provider` (`provider`),
  CONSTRAINT `identity_credentials_relation_identity` FOREIGN KEY (`identity`) REFERENCES `identity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `identity_credentials_relation_provider` FOREIGN KEY (`provider`) REFERENCES `providers` (`key`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Defines credentials used to authenticate identities';


CREATE TABLE `identity_credentials_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `key` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT 'type identifier',
  `provider` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT 'app' COMMENT 'the identifier for the provider',
  `name` varchar(128) COLLATE utf8mb4_bin NOT NULL DEFAULT 'Untitled Credential Type' COMMENT 'readable name for the type',
  `description` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'description of the credential type',
  PRIMARY KEY (`id`),
  UNIQUE KEY `identity_credentials_types_index_key` (`key`),
  KEY `identity_credentials_types_index_provider` (`provider`),
  CONSTRAINT `identity_credentials_types_relation_provider` FOREIGN KEY (`provider`) REFERENCES `providers` (`key`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Defines types of credentials that are valid for authentication of identities';


CREATE TABLE `identity_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `key` varchar(64) COLLATE utf8mb4_bin NOT NULL COMMENT 'the identifying key of the identity group',
  `provider` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT 'app' COMMENT 'the identifier for the provider',
  `type` varchar(16) COLLATE utf8mb4_bin NOT NULL DEFAULT 'user' COMMENT 'the identity group type identifier',
  `name` varchar(128) COLLATE utf8mb4_bin NOT NULL DEFAULT 'Untitled Identity Group' COMMENT 'the human readable name of the identity group',
  `description` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'a brief description of the identity group''s purpose',
  `enabled` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'whether or not the identity group is enabled',
  `locked` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'whether or not the identity group is locked to prevent edits',
  `hidden` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'whether or not the identity group is hidden from management pages',
  `public` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'whether or not the identity group is publicly visible to views, indexing, and searches',
  `parent_key` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'the parent key of the parent group, if any',
  `parent_type` varchar(16) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'the parent type of the parent group, if any',
  `created_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'the date the identity group was created',
  `modified_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'the date the identity group was last modified',
  PRIMARY KEY (`id`),
  UNIQUE KEY `identity_groups_index_key` (`key`,`type`),
  KEY `identity_groups_index_parent` (`parent_key`,`parent_type`),
  KEY `identity_groups_index_provider` (`provider`),
  CONSTRAINT `identity_groups_relation_parent` FOREIGN KEY (`parent_key`, `parent_type`) REFERENCES `identity_groups` (`key`, `type`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `identity_groups_relation_provider` FOREIGN KEY (`provider`) REFERENCES `providers` (`key`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Defines identity groups';


CREATE TABLE `identity_groups_assignments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `provider` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT 'app' COMMENT 'the identifier for the provider',
  `identity` int(11) unsigned NOT NULL COMMENT 'the associated identity record',
  `group` int(11) unsigned NOT NULL COMMENT 'the associated identity group record',
  PRIMARY KEY (`id`),
  UNIQUE KEY `identity_groups_assignments_index_assignment` (`identity`,`group`),
  KEY `identity_groups_assignments_index_provider` (`provider`),
  KEY `identity_groups_assignments_relation_group` (`group`),
  CONSTRAINT `identity_groups_assignments_relation_group` FOREIGN KEY (`group`) REFERENCES `identity_groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `identity_groups_assignments_relation_identity` FOREIGN KEY (`identity`) REFERENCES `identity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `identity_groups_assignments_relation_provider` FOREIGN KEY (`provider`) REFERENCES `providers` (`key`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Defines association between an identity and an identity group';


CREATE TABLE `identity_groups_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `key` varchar(16) COLLATE utf8mb4_bin NOT NULL COMMENT 'the unique identifier for the identity group type',
  `provider` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT 'app' COMMENT 'the identifier for the provider',
  `name` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT 'Untitled Identity Group Type' COMMENT 'the human readable name of the identity group type',
  `description` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'a brief description of the identity group type''s purpose',
  PRIMARY KEY (`id`),
  UNIQUE KEY `identity_groups_types_index_key` (`key`),
  KEY `identity_groups_types_index_provider` (`provider`),
  CONSTRAINT `identity_groups_types_relation_provider` FOREIGN KEY (`provider`) REFERENCES `providers` (`key`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Defines types of identity group that are valid to the system';


CREATE TABLE `identity_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `key` varchar(16) COLLATE utf8mb4_bin NOT NULL COMMENT 'the unique identifier of the identity type',
  `provider` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT 'app' COMMENT 'the identifier for the provider',
  `name` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT 'Untitled Identity Type' COMMENT 'the human readable name of the identity type',
  `description` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'a brief description of the identity type''s purpose',
  PRIMARY KEY (`id`),
  UNIQUE KEY `identity_types_index_key` (`key`),
  KEY `identity_types_index_provider` (`provider`),
  CONSTRAINT `identity_types_relation_provider` FOREIGN KEY (`provider`) REFERENCES `providers` (`key`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Defines types of identity that are valid to the system';


CREATE TABLE `jobs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `key` varchar(64) COLLATE utf8mb4_bin NOT NULL COMMENT 'the unique identifier for the job',
  `type` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT 'the type identifier for the job',
  `provider` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT 'app' COMMENT 'the identifier for the provider',
  `name` varchar(128) COLLATE utf8mb4_bin NOT NULL DEFAULT 'Untitled Job' COMMENT 'human readable name for the job',
  `description` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'a brief description of the job''s purpose',
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT 1 COMMENT 'whether or not the job is enabled',
  `locked` tinyint(1) unsigned NOT NULL DEFAULT 0 COMMENT 'whether or not the job is locked to prevent edits',
  `hidden` tinyint(1) unsigned NOT NULL DEFAULT 0 COMMENT 'whether or not the job is hidden from management pages',
  `last_run` datetime DEFAULT NULL COMMENT 'the date the job was last run',
  `created_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'the date the job record was created',
  `modified_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'the date the job record was last modified',
  PRIMARY KEY (`id`),
  UNIQUE KEY `jobs_index_key` (`key`,`type`),
  KEY `jobs_index_provider` (`provider`),
  KEY `jobs_index_bool` (`enabled`,`locked`,`hidden`),
  KEY `jobs_index_dates` (`last_run`,`created_at`,`modified_at`),
  KEY `jobs_relation_type` (`type`),
  CONSTRAINT `jobs_relation_provider` FOREIGN KEY (`provider`) REFERENCES `providers` (`key`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `jobs_relation_type` FOREIGN KEY (`type`) REFERENCES `jobs_types` (`key`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Defines valid jobs for the job queue system to run';


CREATE TABLE `jobs_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `key` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT 'unique identifier for the job type',
  `provider` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT 'app' COMMENT 'provider of the job type',
  `name` varchar(128) COLLATE utf8mb4_bin NOT NULL DEFAULT 'Untitled Job Type' COMMENT 'human readable name of the job type',
  `description` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'a brief description of the job type',
  PRIMARY KEY (`id`),
  UNIQUE KEY `jobs_types_index_key` (`key`),
  KEY `jobs_types_index_provider` (`provider`),
  CONSTRAINT `jobs_types_relation_provider` FOREIGN KEY (`provider`) REFERENCES `providers` (`key`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Defines types of jobs that the system can run with the job system.';


CREATE TABLE `layouts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `key` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT 'unique identifier for the layout',
  `provider` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT 'app' COMMENT 'identifier for the provider',
  `section` varchar(16) COLLATE utf8mb4_bin NOT NULL COMMENT 'identifier for the site section the layout applies to',
  `name` varchar(128) COLLATE utf8mb4_bin NOT NULL DEFAULT 'Untitled Layout' COMMENT 'human readable name for the layout',
  `description` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'a brief description of the layout',
  `class` varchar(1024) COLLATE utf8mb4_bin NOT NULL COMMENT 'the class associated with the layout in the codebase',
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT 1 COMMENT 'whether or not the layout is enabled',
  `created_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'the date the layout record was created',
  `modified_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'the date the layout record was last modified',
  PRIMARY KEY (`id`),
  UNIQUE KEY `layouts_index_key` (`key`,`section`),
  KEY `layouts_index_provider` (`provider`),
  KEY `layouts_index_bool` (`enabled`),
  KEY `layouts_index_dates` (`created_at`,`modified_at`),
  KEY `layouts_relation_section` (`section`),
  CONSTRAINT `layouts_relation_provider` FOREIGN KEY (`provider`) REFERENCES `providers` (`key`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `layouts_relation_section` FOREIGN KEY (`section`) REFERENCES `sections` (`key`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Defines valid content layouts';


CREATE TABLE `media` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Defines media known to the system';


CREATE TABLE `media_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Defines valid media types recognized by the system';


CREATE TABLE `menus` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `key` varchar(64) COLLATE utf8mb4_bin NOT NULL COMMENT 'unique identifier for the menu',
  `section` varchar(16) COLLATE utf8mb4_bin NOT NULL DEFAULT 'frontend' COMMENT 'the identifier for the site section the menu applies to',
  `provider` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT 'app' COMMENT 'the identifier for the provider',
  `component` varchar(64) COLLATE utf8mb4_bin NOT NULL COMMENT 'the component that represents the menu',
  `name` varchar(128) COLLATE utf8mb4_bin NOT NULL DEFAULT 'Untitled Menu' COMMENT 'human readable name for the menu',
  `description` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'brief description of the menu''s purpose',
  `permission` int(11) unsigned DEFAULT NULL COMMENT 'the associated permission node for the menu, if applicable',
  `class` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'css classes to apply to the menu',
  `toggle` tinyint(1) unsigned NOT NULL DEFAULT 1 COMMENT 'whether the menu has a toggle',
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_index_key` (`key`,`section`),
  KEY `menus_index_provider` (`provider`),
  KEY `menus_index_component` (`component`),
  KEY `menus_index_permission` (`permission`),
  KEY `menus_relation_section` (`section`),
  CONSTRAINT `menus_relation_component` FOREIGN KEY (`component`) REFERENCES `components` (`key`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `menus_relation_permission` FOREIGN KEY (`permission`) REFERENCES `permissions` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `menus_relation_provider` FOREIGN KEY (`provider`) REFERENCES `providers` (`key`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `menus_relation_section` FOREIGN KEY (`section`) REFERENCES `sections` (`key`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Defines navigation menus registered with the system';


CREATE TABLE `menus_links` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `menu` int(11) unsigned NOT NULL COMMENT 'the primary key of the associated menu',
  `key` varchar(64) COLLATE utf8mb4_bin NOT NULL COMMENT 'the unique identifier for the menu link',
  `provider` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT 'app' COMMENT 'the identifier for the provider',
  `parent` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'the parent link identifier, if applicable',
  `title` varchar(64) COLLATE utf8mb4_bin NOT NULL COMMENT 'the display title of the link',
  `permission` int(11) unsigned DEFAULT NULL COMMENT 'the associated permission node, if applicable',
  `icon` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'the link icon, if applicable',
  `class` varchar(1024) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'css classes for the link, if applicable',
  `href` varchar(1024) COLLATE utf8mb4_bin NOT NULL DEFAULT '#' COMMENT 'the link target',
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_links_index_key` (`menu`,`key`,`parent`),
  KEY `menus_links_index_permission` (`permission`),
  KEY `menus_links_index_provider` (`provider`),
  KEY `menus_links_index_parent` (`menu`,`parent`),
  CONSTRAINT `menus_links_index_parent` FOREIGN KEY (`menu`, `parent`) REFERENCES `menus_links` (`menu`, `key`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `menus_links_relation_menu` FOREIGN KEY (`menu`) REFERENCES `menus` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `menus_links_relation_permission` FOREIGN KEY (`permission`) REFERENCES `permissions` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `menus_links_relation_provider` FOREIGN KEY (`provider`) REFERENCES `providers` (`key`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;


CREATE TABLE `networks` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `key` varchar(64) COLLATE utf8mb4_bin NOT NULL COMMENT 'the network identifying key',
  `provider` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT 'app' COMMENT 'the identifier for the provider',
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT 'Untitled Network' COMMENT 'the human readable name of the network',
  `description` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'a brief description of the network',
  `dns` varchar(32) COLLATE utf8mb4_bin DEFAULT '*' COMMENT 'the dns identifier associated with the network',
  `local` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'a brief description of the network',
  `internal` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'whether or not the network is internal',
  `trusted` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'whether or not the network is trusted',
  `system` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'whether or not the network is an internal system network for automation',
  `locked` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'whether or not the network is locked to prevent edits',
  `hidden` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'whether or not the network is hidden from management pages',
  `enabled` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'whether or not the network is enabled',
  PRIMARY KEY (`id`),
  UNIQUE KEY `networks_index_key` (`key`),
  KEY `networks_index_provider` (`provider`),
  KEY `networks_index_dns` (`dns`),
  KEY `networks_index_bool` (`local`,`internal`,`trusted`,`system`,`locked`,`hidden`,`enabled`),
  CONSTRAINT `networks_relation_dns` FOREIGN KEY (`dns`) REFERENCES `dns` (`key`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `networks_relation_provider` FOREIGN KEY (`provider`) REFERENCES `providers` (`key`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Defines network definitions known to the system';


CREATE TABLE `networks_assignments_identity` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `provider` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT 'app' COMMENT 'identifier for the provider',
  `network` int(11) unsigned NOT NULL COMMENT 'associated network record',
  `identity` int(11) unsigned NOT NULL COMMENT 'associated identity record',
  PRIMARY KEY (`id`),
  UNIQUE KEY `networks_assignments_identity_index_assignment` (`network`,`identity`),
  KEY `networks_assignments_identity_index_provider` (`provider`),
  KEY `networks_assignments_identity_relation_identity` (`identity`),
  CONSTRAINT `networks_assignments_identity_relation_identity` FOREIGN KEY (`identity`) REFERENCES `identity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `networks_assignments_identity_relation_network` FOREIGN KEY (`network`) REFERENCES `networks` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `networks_assignments_identity_relation_provider` FOREIGN KEY (`provider`) REFERENCES `providers` (`key`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Maps networks to identities';


CREATE TABLE `notifications` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Defines notifications sent to users';


CREATE TABLE `organizations` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `key` varchar(64) COLLATE utf8mb4_bin NOT NULL COMMENT 'the unique identifier for the organization',
  `provider` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT 'app' COMMENT 'the identifier for the provider',
  `type` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT 'the type of organization',
  `name` varchar(128) COLLATE utf8mb4_bin NOT NULL DEFAULT 'Untitled Organization' COMMENT 'the human readable name of the organization',
  `description` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'a brief description of the organization',
  `host` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT 'public' COMMENT 'the default host associated with the organization',
  `class` varchar(512) COLLATE utf8mb4_bin NOT NULL DEFAULT 'organization\\Organization' COMMENT 'the class used to represent the organization in the codebase',
  `parent_key` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'the parent identifier, if any',
  `parent_host` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'the parent host, if any',
  `enabled` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'whether or not the organization is enabled',
  `locked` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'whether or not the organization is locked to prevent edits',
  `hidden` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'whether or not the organization is hidden from management pages',
  `public` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'whether or not the organization is publicly visible for page views, indexing and searches',
  `system` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'whether or not the organization is an system organization',
  `internal` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'whether or not the organization is internal, under the control of the ownership of the system',
  `created_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'the date the organization record was created',
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp() COMMENT 'the date the organization record was last modified',
  PRIMARY KEY (`id`),
  UNIQUE KEY `organizations_index_key` (`key`),
  UNIQUE KEY `organizations_index_relationship_binding` (`key`,`host`),
  KEY `organizations_index_host` (`host`),
  KEY `organizations_index_parent` (`parent_key`,`parent_host`),
  KEY `organizations_index_type` (`type`),
  KEY `organizations_index_provider` (`provider`),
  CONSTRAINT `organizations_relation_host` FOREIGN KEY (`host`) REFERENCES `networks` (`key`) ON UPDATE CASCADE,
  CONSTRAINT `organizations_relation_parent` FOREIGN KEY (`parent_key`, `parent_host`) REFERENCES `organizations` (`key`, `host`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `organizations_relation_provider` FOREIGN KEY (`provider`) REFERENCES `providers` (`key`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `organizations_relation_type` FOREIGN KEY (`type`) REFERENCES `organizations_types` (`key`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Defines organizations known to the system';


CREATE TABLE `organizations_assignments_identity` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `provider` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT 'app' COMMENT 'the identifier for the provider',
  `organization` int(11) unsigned NOT NULL COMMENT 'the associated organization',
  `identity` int(11) unsigned NOT NULL COMMENT 'the associated identity',
  PRIMARY KEY (`id`),
  UNIQUE KEY `organizations_assignments_identity_index_3` (`organization`,`identity`),
  KEY `organizations_assignments_identity_index_provider` (`provider`),
  KEY `organizations_assignments_identity_relation_identity` (`identity`),
  CONSTRAINT `organizations_assignments_identity_relation_identity` FOREIGN KEY (`identity`) REFERENCES `identity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `organizations_assignments_identity_relation_organization` FOREIGN KEY (`organization`) REFERENCES `organizations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `organizations_assignments_identity_relation_provider` FOREIGN KEY (`provider`) REFERENCES `providers` (`key`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Maps organizations to identities';


CREATE TABLE `organizations_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `key` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT 'the unique identifier for the organization type',
  `provider` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT 'app' COMMENT 'the identifier for the provider',
  `name` varchar(128) COLLATE utf8mb4_bin NOT NULL DEFAULT 'Untitlted Organization Type' COMMENT 'the human readable name of the organization type',
  `description` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'a brief description of the organization type',
  PRIMARY KEY (`id`),
  UNIQUE KEY `organizations_types_index_key` (`key`),
  KEY `organizations_types_index_provider` (`provider`),
  CONSTRAINT `organizations_types_relation_provider` FOREIGN KEY (`provider`) REFERENCES `providers` (`key`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Defines valid organization types';


CREATE TABLE `pages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `provider` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT 'app' COMMENT 'the identifier for the provider',
  PRIMARY KEY (`id`),
  KEY `pages_index_provider` (`provider`),
  CONSTRAINT `pages_relation_provider` FOREIGN KEY (`provider`) REFERENCES `providers` (`key`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Defines standard pages served by the application';


CREATE TABLE `permissions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `key` varchar(64) COLLATE utf8mb4_bin NOT NULL COMMENT 'the unique identifier for the permission',
  `provider` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT 'app' COMMENT 'the identifier for the provider',
  `name` varchar(128) COLLATE utf8mb4_bin NOT NULL DEFAULT 'Untitled Permission' COMMENT 'the human readable name of the permission',
  `description` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'a brief description of the permission''s purpose',
  `parent` int(11) unsigned DEFAULT NULL COMMENT 'the parent permission, if any',
  `class` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT 'auth\\permissions\\Permission' COMMENT 'the class used in the codebase to represent the permission',
  `baseline` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'whether or not the permission is baseline. Baseline permissions cannot have a parent record.',
  `final` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'whether or not the permission is final. Final permissions cannot have child records.',
  `enabled` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'whether or not the permission is enabled',
  `locked` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'whether or not the permission is locked to prevent edits',
  `system` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'whether or not the permission is a system permission',
  `hidden` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'whether or not the permission is hidden from management pages',
  `created_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'the date the permission record was created',
  `modified_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'the date the permission record was last modified',
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_index_key` (`key`,`parent`),
  KEY `permissions_relation_parent` (`parent`),
  KEY `permissions_index_provider` (`provider`),
  CONSTRAINT `permissions_relation_parent` FOREIGN KEY (`parent`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `permissions_relation_provider` FOREIGN KEY (`provider`) REFERENCES `providers` (`key`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Defines permissions used by the system to authorize access rights';


CREATE TABLE `permissions_assignments_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `provider` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT 'app' COMMENT 'the identifier for the provider',
  `group` int(11) unsigned NOT NULL COMMENT 'the primary key of the associated permission group',
  `permission` int(11) unsigned NOT NULL COMMENT 'the primary key of the associated permission node',
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_assignments_groups_index_assignment` (`group`,`permission`),
  KEY `permissions_assignments_groups_relation_permission` (`permission`),
  KEY `permissions_assignments_groups_index_provider` (`provider`),
  CONSTRAINT `permissions_assignments_groups_relation_1` FOREIGN KEY (`group`) REFERENCES `permissions_groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `permissions_assignments_groups_relation_permission` FOREIGN KEY (`permission`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `permissions_assignments_groups_relation_provider` FOREIGN KEY (`provider`) REFERENCES `providers` (`key`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Maps permissions to permission groups';


CREATE TABLE `permissions_assignments_identity` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `provider` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT 'app' COMMENT 'the identifier for the provider',
  `identity` int(11) unsigned NOT NULL COMMENT 'the primary key of the associated identity',
  `permission` int(11) unsigned NOT NULL COMMENT 'the primary key of the associated permission node',
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_assignments_identity_index_assignment` (`identity`,`permission`),
  KEY `permissions_assignments_identity_relation_permission` (`permission`),
  KEY `permissions_assignments_identity_index_provider` (`provider`),
  CONSTRAINT `permissions_assignments_identity_relation_1` FOREIGN KEY (`identity`) REFERENCES `identity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `permissions_assignments_identity_relation_permission` FOREIGN KEY (`permission`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `permissions_assignments_identity_relation_provider` FOREIGN KEY (`provider`) REFERENCES `providers` (`key`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Maps permissions to identities';


CREATE TABLE `permissions_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `key` varchar(64) COLLATE utf8mb4_bin NOT NULL COMMENT 'the unique identifier for the permission group',
  `provider` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT 'app' COMMENT 'the identifier for the provider',
  `name` varchar(128) COLLATE utf8mb4_bin NOT NULL DEFAULT 'Untitled Permission Group' COMMENT 'the human readable name of the permission group',
  `description` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'a brief description of the permission group''s purpose',
  `host` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '*' COMMENT 'the host associated with the permission group',
  `parent_key` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'the parent group key, if any',
  `parent_host` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'the parent group host, if any',
  `enabled` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'whether or not the permission group is enabled',
  `locked` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'whether or not the permission group is locked to prevent edits',
  `final` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'whether or not the permission group is final. Final permission groups cannot have child records.',
  `created_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'the date the permission group record was created',
  `modified_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'the date the permission group record was last modified',
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_groups_index_key` (`key`,`host`),
  KEY `permissions_groups_index_parent` (`parent_key`,`parent_host`),
  KEY `permissions_groups_relation_host` (`host`),
  KEY `permissions_groups_index_provider` (`provider`),
  CONSTRAINT `permissions_groups_relation_host` FOREIGN KEY (`host`) REFERENCES `networks` (`key`) ON UPDATE CASCADE,
  CONSTRAINT `permissions_groups_relation_parent` FOREIGN KEY (`parent_key`, `parent_host`) REFERENCES `permissions_groups` (`key`, `host`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `permissions_groups_relation_provider` FOREIGN KEY (`provider`) REFERENCES `providers` (`key`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Defines permission groups';


CREATE TABLE `permission_groups_assignments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `provider` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT 'app' COMMENT 'the identifier for the provider',
  `permission_group` int(11) unsigned NOT NULL COMMENT 'the primary key of the associated permission group',
  `identity_group` int(11) unsigned NOT NULL COMMENT 'the primary key of the associated identity group',
  PRIMARY KEY (`id`),
  UNIQUE KEY `permission_groups_assignments_index_assignment` (`permission_group`,`identity_group`),
  KEY `permission_groups_assignments_relation_identity_group` (`identity_group`),
  KEY `permission_groups_assignments_index_provider` (`provider`),
  CONSTRAINT `permission_groups_assignments_relation_1` FOREIGN KEY (`permission_group`) REFERENCES `permissions_groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `permission_groups_assignments_relation_identity_group` FOREIGN KEY (`identity_group`) REFERENCES `identity_groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `permission_groups_assignments_relation_provider` FOREIGN KEY (`provider`) REFERENCES `providers` (`key`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Maps permission groups to identity groups';


CREATE TABLE `pluggables` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `key` varchar(64) COLLATE utf8mb4_bin NOT NULL COMMENT 'unique identifier for the pluggable',
  `type` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT 'type identifier for the pluggable',
  `provider` varchar(32) COLLATE utf8mb4_bin DEFAULT 'app' COMMENT 'identifier for the provider, if applicable',
  `name` varchar(128) COLLATE utf8mb4_bin NOT NULL DEFAULT 'Untitled Pluggable' COMMENT 'human readable name for the pluggable',
  `description` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'a brief description of the pluggable''s purpose',
  `path` varchar(1024) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'path to the pluggable resource, if applicable',
  `manifest` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'name of the manifest file, if applicable',
  `definer` varchar(1024) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'name of the definer class in the codebase of the pluggable, if applicable',
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT 1 COMMENT 'whether the pluggable is enabled',
  `locked` tinyint(1) unsigned NOT NULL DEFAULT 0 COMMENT 'whether the pluggable is locked to prevent edits',
  `hidden` tinyint(1) unsigned NOT NULL DEFAULT 0 COMMENT 'whether the pluggable is hidden from management pages',
  `created_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'the date the pluggable record was created',
  `modified_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'the date the pluggable record was last modified',
  PRIMARY KEY (`id`),
  UNIQUE KEY `pluggables_index_key` (`key`,`type`),
  UNIQUE KEY `pluggables_index_details` (`id`,`key`,`type`,`provider`,`enabled`,`locked`,`hidden`),
  KEY `pluggables_index_provider` (`provider`),
  KEY `pluggables_index_bool` (`enabled`,`locked`,`hidden`),
  KEY `pluggables_index_dates` (`created_at`,`modified_at`),
  KEY `pluggables_index_path` (`path`(768)),
  KEY `pluggables_index_manifest` (`manifest`),
  KEY `pluggables_index_definer` (`definer`(768)),
  KEY `pluggables_relation_type` (`type`),
  CONSTRAINT `pluggables_relation_provider` FOREIGN KEY (`provider`) REFERENCES `providers` (`key`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pluggables_relation_type` FOREIGN KEY (`type`) REFERENCES `pluggables_types` (`key`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Defines pluggables known to the system that extend functionality';


CREATE TABLE `pluggables_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `key` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT 'unique identifier for the pluggable type',
  `provider` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT 'app' COMMENT 'identifier for the provider',
  `name` varchar(128) COLLATE utf8mb4_bin NOT NULL DEFAULT 'Untitled Pluggable Type' COMMENT 'human readable name of the pluggable type',
  `description` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'a brief description of the pluggable type''s purpose',
  `is_provider` tinyint(1) unsigned NOT NULL DEFAULT 0 COMMENT 'whether the pluggable is a resource provider',
  PRIMARY KEY (`id`),
  UNIQUE KEY `pluggables_types_index_key` (`key`),
  KEY `pluggables_types_index_provider` (`provider`),
  KEY `pluggables_types_index_provides` (`is_provider`),
  CONSTRAINT `pluggables_types_relation_provider` FOREIGN KEY (`provider`) REFERENCES `providers` (`key`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Defines pluggable resource types known to the system';


CREATE TABLE `providers` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `type` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT 'provider type',
  `key` varchar(64) COLLATE utf8mb4_bin NOT NULL COMMENT 'unique identifier for the provider',
  `name` varchar(128) COLLATE utf8mb4_bin NOT NULL DEFAULT 'Untitled Provider' COMMENT 'human readable name of the provider',
  `description` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'brief description of the provider',
  PRIMARY KEY (`id`),
  UNIQUE KEY `providers_index_key` (`key`),
  KEY `providers_index_type` (`type`),
  CONSTRAINT `providers_relation_type` FOREIGN KEY (`type`) REFERENCES `providers_types` (`key`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Designates asset providers.';


CREATE TABLE `providers_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `key` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT 'unique identifier for the provider type',
  `name` varchar(128) COLLATE utf8mb4_bin NOT NULL DEFAULT '"Untitled Provider"' COMMENT 'human readable name of the provider',
  `description` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'description of the provider type',
  PRIMARY KEY (`id`),
  UNIQUE KEY `providers_types_index_key` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Designates asset provider types.';


CREATE TABLE `routes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `type` varchar(8) COLLATE utf8mb4_bin NOT NULL DEFAULT 'get' COMMENT 'the route type of the associated record',
  `network` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '*' COMMENT 'the network filter of the associated route',
  `organization` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '*' COMMENT 'the organization associated with the route',
  `key` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT 'the key identifier of the route',
  `provider` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT 'app' COMMENT 'the identifier of the provider',
  `route` varchar(2048) COLLATE utf8mb4_bin NOT NULL COMMENT 'the literal route definition',
  `controller` varchar(1024) COLLATE utf8mb4_bin NOT NULL COMMENT 'the controller class in the codebase mapped to the route',
  `permission` int(11) unsigned DEFAULT NULL COMMENT 'the baseline permission associated with visiting the route',
  `enabled` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'whether or not the route is enabled',
  `locked` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'whether or not the route is locked to prevent edits',
  `hidden` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'whether or not the route is hidden from management pages',
  `system` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'whether or not the route is a system route for automation',
  `public` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'whether or not the route is publicly available for viewing, indexing, and searches',
  `created_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'the date the route record was created',
  `modified_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'the date the route record was last modified',
  PRIMARY KEY (`id`),
  KEY `routes_index_provider` (`provider`),
  CONSTRAINT `routes_relation_provider` FOREIGN KEY (`provider`) REFERENCES `providers` (`key`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Designates valid application routes';


CREATE TABLE `routes_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `key` varchar(8) COLLATE utf8mb4_bin NOT NULL COMMENT 'the unique identifier for the route type',
  `provider` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT 'app' COMMENT 'the identifier for the provider',
  `name` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT 'the human readable name of the route type',
  `http` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'whether or not the route type is http ',
  `enabled` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'whether or not the route type is enabled ',
  PRIMARY KEY (`id`),
  UNIQUE KEY `routes_types_index_key` (`key`),
  KEY `routes_types_index_provider` (`provider`),
  CONSTRAINT `routes_types_relation_provider` FOREIGN KEY (`provider`) REFERENCES `providers` (`key`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Defines valid route types recognized by the system';


CREATE TABLE `scripts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `key` varchar(64) COLLATE utf8mb4_bin NOT NULL COMMENT 'the unique identifier for the script',
  `provider` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT 'app' COMMENT 'the identifier for the provider',
  `version` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'the version number of the script',
  `name` varchar(128) COLLATE utf8mb4_bin NOT NULL DEFAULT 'Untitled Script' COMMENT 'the human readable name of the script',
  `description` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'a brief description of the script''s purpose',
  `bin` varchar(1024) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'the uncompiled script source, if applicable',
  `source` varchar(1024) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'the compiled source file, if applicable',
  `sourcemap` varchar(1024) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'the source map, if applicable',
  `cdn` varchar(1024) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'the cdn link, if applicable',
  `integrity` varchar(1024) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'the subresource integrity hash, if applicable',
  `crossorigin` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'the crossorigin value, if applicable',
  PRIMARY KEY (`id`),
  UNIQUE KEY `scripts_index_key` (`key`,`version`),
  UNIQUE KEY `scripts_index_cdn` (`cdn`,`integrity`,`crossorigin`) USING HASH,
  KEY `scripts_index_provider` (`provider`),
  KEY `scripts_index_source` (`source`(768)),
  KEY `scripts_index_bin` (`bin`(768)),
  KEY `scripts_index_sourcemap` (`sourcemap`(768)),
  CONSTRAINT `scripts_relation_provider` FOREIGN KEY (`provider`) REFERENCES `providers` (`key`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Defines script resources known to the system';


CREATE TABLE `scripts_dependencies` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `script` int(11) unsigned NOT NULL COMMENT 'the primary key of the associated script',
  `dependency` int(11) unsigned NOT NULL COMMENT 'the primary key of the associated dependency',
  PRIMARY KEY (`id`),
  UNIQUE KEY `scripts_dependencies_index_assignment` (`script`,`dependency`),
  KEY `scripts_dependencies_relation_dependency` (`dependency`),
  CONSTRAINT `scripts_dependencies_relation_dependency` FOREIGN KEY (`dependency`) REFERENCES `scripts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `scripts_dependencies_relation_script` FOREIGN KEY (`script`) REFERENCES `scripts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Maps script dependencies';


CREATE TABLE `sections` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `key` varchar(16) COLLATE utf8mb4_bin NOT NULL COMMENT 'The identifier for the section.',
  `provider` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT 'app' COMMENT 'the identifier for the provider',
  `name` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT 'Untitled Site Section' COMMENT 'This is the human readable name for the site section.',
  `description` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'A brief description of the site section.',
  `network` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '*' COMMENT 'the network associated with the site section',
  PRIMARY KEY (`id`),
  UNIQUE KEY `sections_index_key` (`key`),
  KEY `sections_index_network` (`network`),
  KEY `sections_index_provider` (`provider`),
  CONSTRAINT `sections_relation_network` FOREIGN KEY (`network`) REFERENCES `networks` (`key`) ON UPDATE CASCADE,
  CONSTRAINT `sections_relation_provider` FOREIGN KEY (`provider`) REFERENCES `providers` (`key`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Defines site sections provided by the system';


CREATE TABLE `settings` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Defines settings for managing system functionality';


CREATE TABLE `styles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `key` varchar(64) COLLATE utf8mb4_bin NOT NULL COMMENT 'the unique identifier for the stylesheet',
  `provider` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT 'app' COMMENT 'the identifier for the provider',
  `version` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'the version number of the stylesheet',
  `name` varchar(128) COLLATE utf8mb4_bin NOT NULL DEFAULT 'Untitled Stylesheet' COMMENT 'the human readable name of the stylesheet',
  `description` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'a brief description of the stylesheet''s purpose',
  `bin` varchar(1024) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'the uncompiled stylesheet source, if applicable',
  `source` varchar(1024) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'the compiled source file, if applicable',
  `sourcemap` varchar(1024) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'the source map, if applicable',
  `cdn` varchar(1024) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'the cdn link, if applicable',
  `integrity` varchar(1024) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'the subresource integrity hash, if applicable',
  `crossorigin` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'the crossorigin value, if applicable',
  PRIMARY KEY (`id`),
  UNIQUE KEY `styles_index_key` (`key`,`version`),
  UNIQUE KEY `styles_index_cdn` (`cdn`,`integrity`,`crossorigin`) USING HASH,
  KEY `styles_index_provider` (`provider`),
  KEY `styles_index_source` (`source`(768)),
  KEY `styles_index_bin` (`bin`(768)),
  KEY `styles_index_sourcemap` (`sourcemap`(768)),
  CONSTRAINT `styles_relation_provider` FOREIGN KEY (`provider`) REFERENCES `providers` (`key`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Defines stylesheet resources known to the system';


CREATE TABLE `styles_dependencies` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `style` int(11) unsigned NOT NULL COMMENT 'the primary key of the associated stylesheet',
  `dependency` int(11) unsigned NOT NULL COMMENT 'the primary key of the associated stylesheet dependency',
  PRIMARY KEY (`id`),
  UNIQUE KEY `styles_dependencies_index_assignment` (`style`,`dependency`),
  KEY `styles_dependencies_relation_dependency` (`dependency`),
  CONSTRAINT `styles_dependencies_relation_dependency` FOREIGN KEY (`dependency`) REFERENCES `styles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `styles_dependencies_relation_style` FOREIGN KEY (`style`) REFERENCES `styles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Maps stylesheet dependencies';


CREATE TABLE `themes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `key` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT 'the unique identifier for the theme',
  `provider` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT 'app' COMMENT 'the identifier for the provider',
  `name` varchar(128) COLLATE utf8mb4_bin NOT NULL DEFAULT 'Untitled Theme' COMMENT 'human readable name for the theme',
  `description` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'a brief description of the theme',
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT 1 COMMENT 'whether the theme is enabled',
  `default` tinyint(1) unsigned NOT NULL DEFAULT 0 COMMENT 'whether the theme is default',
  `locked` tinyint(1) unsigned NOT NULL DEFAULT 0 COMMENT 'whether the theme is locked to prevent edits',
  `hidden` tinyint(1) unsigned NOT NULL DEFAULT 0 COMMENT 'whether the theme is hidden from management pages',
  `created_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'the date the theme record was created',
  `modified_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'the date the theme record was last modified',
  PRIMARY KEY (`id`),
  UNIQUE KEY `themes_index_key` (`key`),
  KEY `themes_index_provider` (`provider`),
  KEY `themes_index_bool` (`enabled`,`default`,`locked`,`hidden`),
  KEY `themes_index_dates` (`created_at`,`modified_at`),
  CONSTRAINT `themes_relation_provider` FOREIGN KEY (`provider`) REFERENCES `providers` (`key`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Defines layout themes known to the system';


CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `username` varchar(64) COLLATE utf8mb4_bin NOT NULL COMMENT 'The username for the user.',
  `identity` varchar(64) COLLATE utf8mb4_bin NOT NULL COMMENT 'The identity associated with the user.',
  `organization` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT 'public' COMMENT 'The organization associated with the user.',
  `host` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT 'public' COMMENT 'The network associated with the user.',
  `organization_type` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT 'public' COMMENT 'The bound organization type',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_index_identity` (`identity`,`organization`,`host`,`organization_type`),
  UNIQUE KEY `users_index_username` (`username`),
  CONSTRAINT `users_index_identity` FOREIGN KEY (`identity`, `organization`, `host`, `organization_type`) REFERENCES `identity` (`identity`, `organization`, `network`, `type`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Defines the site userbase';


CREATE TABLE `users_details` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `user` int(11) unsigned NOT NULL COMMENT 'the user associated with the detail',
  `key` varchar(64) COLLATE utf8mb4_bin NOT NULL COMMENT 'the unique identifier for the detail within it''s type and category',
  `type` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT 'the type of the detail',
  `category` int(11) unsigned NOT NULL COMMENT 'the category of the detail',
  `provider` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT 'app' COMMENT 'the provider for the user detail field',
  `value` text COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'the detail field',
  `enabled` tinyint(3) unsigned NOT NULL DEFAULT 1 COMMENT 'whether or not the detail field is enabled',
  `locked` tinyint(3) unsigned NOT NULL DEFAULT 0 COMMENT 'whether or not the detail field is open to edits',
  `hidden` tinyint(3) unsigned NOT NULL DEFAULT 0 COMMENT 'whether or not the detail is hidden from display',
  `public` tinyint(3) unsigned NOT NULL DEFAULT 0 COMMENT 'whether or not the detail is available for public display, search, and indexing',
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp() COMMENT 'the date the detail was created',
  `modified_at` datetime NOT NULL COMMENT 'the date the user detail was last modified',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_details_index_key` (`user`,`key`,`type`,`category`),
  KEY `users_details_relation_type` (`type`,`category`),
  KEY `users_details_relation_provider` (`provider`),
  CONSTRAINT `users_details_relation_provider` FOREIGN KEY (`provider`) REFERENCES `providers` (`key`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `users_details_relation_type` FOREIGN KEY (`type`, `category`) REFERENCES `users_details_types` (`key`, `category`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `users_details_relation_user` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Defines details about application users';


CREATE TABLE `users_details_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `key` varchar(64) COLLATE utf8mb4_bin NOT NULL COMMENT 'unique identifier for the detail category',
  `provider` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT 'app' COMMENT 'identifier of the category provider',
  `name` varchar(128) COLLATE utf8mb4_bin NOT NULL DEFAULT 'Untitled User Detail Category' COMMENT 'human readable name of the detail category',
  `description` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'brief description of the detail category',
  `parent` int(11) unsigned DEFAULT NULL COMMENT 'the parent category, if any',
  `enabled` tinyint(3) unsigned NOT NULL DEFAULT 1 COMMENT 'whether or not the category is enabled',
  `locked` tinyint(3) unsigned NOT NULL DEFAULT 0 COMMENT 'whether or not the category is locked to prevent edits',
  `hidden` tinyint(3) unsigned NOT NULL DEFAULT 0 COMMENT 'whether or not the category is hidden from management pages',
  `public` tinyint(3) unsigned NOT NULL DEFAULT 0 COMMENT 'whether or not the category is publicly visible to page views, indexing, and searches',
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp() COMMENT 'the date the category record was created',
  `modified_at` datetime NOT NULL COMMENT 'the date the category record was last modified',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_details_categories_index_key` (`key`,`parent`),
  KEY `users_details_categories_index_provider` (`provider`),
  KEY `users_details_categories_relation_parent` (`parent`),
  CONSTRAINT `users_details_categories_relation_parent` FOREIGN KEY (`parent`) REFERENCES `users_details_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `users_details_categories_relation_provider` FOREIGN KEY (`provider`) REFERENCES `providers` (`key`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Defines valid detail categories about users';


CREATE TABLE `users_details_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `key` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT 'unique identifier for the detail type',
  `provider` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT 'identifier for the provider of the detail type',
  `category` int(11) unsigned NOT NULL COMMENT 'identifier for the category of details the type is associated with',
  `name` varchar(128) COLLATE utf8mb4_bin NOT NULL DEFAULT '"Untitled User Detail Type"' COMMENT 'human readable name of the user detail type',
  `description` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'a brief description of the user detail type',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_details_types_index_key` (`key`,`category`),
  UNIQUE KEY `users_details_types_index_provider` (`provider`),
  KEY `users_details_types_relation_category` (`category`),
  CONSTRAINT `users_details_types_relation_category` FOREIGN KEY (`category`) REFERENCES `users_details_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `users_details_types_relation_provider` FOREIGN KEY (`provider`) REFERENCES `providers` (`key`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Defines valid detail types for user details';


CREATE TABLE `users_identity` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `provider` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT 'app' COMMENT 'unique identifier for the provider',
  `user` int(11) unsigned NOT NULL COMMENT 'designates the user record association',
  `identity` int(11) unsigned NOT NULL COMMENT 'designates the identity record assignment',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_identity_index_assignment` (`user`,`identity`),
  KEY `users_identity_index_provider` (`provider`),
  KEY `users_identity_relation_identity` (`identity`),
  CONSTRAINT `users_identity_relation_identity` FOREIGN KEY (`identity`) REFERENCES `identity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `users_identity_relation_provider` FOREIGN KEY (`provider`) REFERENCES `providers` (`key`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `users_identity_relation_user` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Maps users to identities';


CREATE TABLE `users_session` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `type` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT 'http' COMMENT 'the session type identifier',
  `provider` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT 'app' COMMENT 'the identifier for the provider',
  `user` int(11) unsigned NOT NULL COMMENT 'the user associated with the session',
  `sid` varchar(128) COLLATE utf8mb4_bin NOT NULL COMMENT 'the session identifier',
  `dns` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '*' COMMENT 'the remote ip address of the user session',
  `created_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'the date the session was created',
  `modified_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'the date the session was last modified',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_session_index_key` (`type`,`user`,`sid`),
  UNIQUE KEY `users_session_index_sid` (`sid`),
  KEY `users_session_index_dns` (`dns`),
  KEY `users_session_index_provider` (`provider`),
  KEY `users_session_relation_user` (`user`),
  CONSTRAINT `users_session_relation_dns` FOREIGN KEY (`dns`) REFERENCES `dns` (`key`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `users_session_relation_provider` FOREIGN KEY (`provider`) REFERENCES `providers` (`key`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `users_session_relation_type` FOREIGN KEY (`type`) REFERENCES `users_session_types` (`key`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `users_session_relation_user` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Defines active user sessions';


CREATE TABLE `users_session_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `key` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT 'the unique identifier for the session type',
  `provider` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT 'app' COMMENT 'the identifier for the provider',
  `name` varchar(128) COLLATE utf8mb4_bin NOT NULL COMMENT 'human readable name of the session type',
  `description` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'a brief description of the session type''s purpose',
  `enabled` tinyint(3) unsigned NOT NULL DEFAULT 1 COMMENT 'whether or not the session type is enabled',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_session_types_index_key` (`key`),
  KEY `users_session_types_index_provider` (`provider`),
  KEY `users_session_types_index_enabled` (`enabled`),
  CONSTRAINT `users_session_types_relation_provider` FOREIGN KEY (`provider`) REFERENCES `providers` (`key`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Defines valid session types';


CREATE TABLE `whitelist` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `dns` varbinary(16) NOT NULL COMMENT 'the associated dns record for the whitelist entry',
  `provider` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT 'app' COMMENT 'the identifier for the provider',
  `created_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'the date the whitelist record was created',
  `starts_at` datetime DEFAULT NULL COMMENT 'the date the whitelist record becomes active',
  `expires_at` datetime DEFAULT NULL COMMENT 'the date the whitelist record expires',
  `code` int(64) DEFAULT NULL COMMENT 'an optional status code for the whitelist record',
  `reason` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'a human readable reason for the whitelist entry',
  PRIMARY KEY (`id`),
  UNIQUE KEY `whitelist_index_dns` (`dns`),
  KEY `whitelist_index_details` (`created_at`,`starts_at`,`expires_at`,`code`),
  KEY `whitelist_index_provider` (`provider`),
  CONSTRAINT `whitelist_relation_provider` FOREIGN KEY (`provider`) REFERENCES `providers` (`key`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Contains a list of whitelisted dns records';


-- 2021-10-04 08:24:48
