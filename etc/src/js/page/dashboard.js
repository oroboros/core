/* 
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

'use strict';
(function (core) {
    const page = 'dashboard-page';
    const oroboros = core();
    const abstract = oroboros.abstract('page');
    const component_bindings = {
//        'accounts-index': 'table',
    };
    const lib = {
        init: function () {
            (function ($) {

                // Toggle the side navigation
                $("#sidebarToggle, #sidebarToggleTop").on('click', function (e) {
                    $("body").toggleClass("sidebar-toggled");
                    $(".sidebar").toggleClass("toggled");
                    if ($(".sidebar").hasClass("toggled")) {
                        $('.sidebar .collapse').collapse('hide');
                    }
                    ;
                });

                // Close any open menu accordions when window is resized below 768px
                $(window).resize(function () {
                    if ($(window).width() < 768) {
                        $('.sidebar .collapse').collapse('hide');
                    }
                    ;

                    // Toggle the side navigation when window is resized below 480px
                    if ($(window).width() < 480 && !$(".sidebar").hasClass("toggled")) {
                        $("body").addClass("sidebar-toggled");
                        $(".sidebar").addClass("toggled");
                        $('.sidebar .collapse').collapse('hide');
                    }
                    ;
                });

                // Prevent the content wrapper from scrolling when the fixed side navigation hovered over
                $('body.fixed-nav .sidebar').on('mousewheel DOMMouseScroll wheel', function (e) {
                    if ($(window).width() > 768) {
                        var e0 = e.originalEvent,
                                delta = e0.wheelDelta || -e0.detail;
                        this.scrollTop += (delta < 0 ? 1 : -1) * 30;
                        e.preventDefault();
                    }
                });

                // Scroll to top button appear
                $(document).on('scroll', function () {
                    var scrollDistance = $(this).scrollTop();
                    if (scrollDistance > 100) {
                        $('.scroll-to-top').fadeIn();
                    } else {
                        $('.scroll-to-top').fadeOut();
                    }
                });

                // Smooth scrolling using jQuery easing
                $(document).on('click', 'a.scroll-to-top', function (e) {
                    var $anchor = $(this);
                    console.log($anchor.attr('href'));
                    $('html, body').stop().animate({
                        scrollTop: ($($anchor.attr('href')).offset().top)
                    }, 1000, 'easeInOutExpo');
                    e.preventDefault();
                });

            })(jQuery);
        }
    };
    const DashboardPage = class DashboardPage extends abstract {

        static classKey()
        {
            return page;
        }

        constructor(id, element) {
            super(id, element);
        }

        onReady(subject) {
            let element = subject.element;
            for (let key in component_bindings)
            {
                try {
                    subject.loadComponent(component_bindings[key], key);
                } catch (e) {
                    console.warn(e.message);
                }
            }

        }
    }
    lib.init();
    oroboros.register('page', page, DashboardPage);

    let element = document.getElementsByTagName('body')[0];
    if (element.id === page)
    {
        oroboros.create('page', page, element);
    }
})(Oroboros);
