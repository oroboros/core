(function (core) {
    const oroboros = core();
    const component = 'login-form';
    const abstract = oroboros.extend('component', 'form');
    const lib = {
        init: function () {

        }
    };
    const LoginForm = class LoginForm extends abstract {

        static classKey()
        {
            return component;
        }

        constructor(id, args, flags) {
            super(id, args, flags);
        }

        initialize()
        {
            super.initialize();
            return this;
        }

        onReady(subject) {
            super.onReady(subject);
        }

        setElement(element, preserve) {
            return super.setElement(element, preserve);
        }

        validateForm()
        {
            return super.validateForm();
        }

        onSubmit(event) {
            return super.onSubmit(event);
        }
        
        onReset(event) {
            return super.onReset(event);
        }
    }
    lib.init();
    oroboros.register('component', component, LoginForm);
})(Oroboros);
