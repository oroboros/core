/* 
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
'use strict';
(function (core) {
    'use strict';;
    const oroboros = core();
    const component = 'terminal';
    const abstract = oroboros.abstract('component');
    const lib = {
        is_initialized: false,
        init: function () {
            if (!this.is_initialized)
            {
                this.initializeAddons();
                this.is_initialized = true;
            }
        },
        initializeAddons: function () {
            Terminal.applyAddon(attach);
            Terminal.applyAddon(fit);
            Terminal.applyAddon(fullscreen);
            Terminal.applyAddon(search);
            Terminal.applyAddon(terminado);
            Terminal.applyAddon(webLinks);
            Terminal.applyAddon(winptyCompat);
//            Terminal.applyAddon(zmodem);
        }
    };
    const TerminalComponent = class TerminalComponent extends abstract {

        static classKey()
        {
            return component;
        }

        constructor(id, args, flags) {
            super(id, args, flags);
            this.terminal = null;
            this.content = null;
        }

        onReady(subject) {
            super.onReady(subject);
            subject.initializeTerminal();
            subject.initializeTerminalContent();
        }

        initializeTerminal() {
            let terminal = this.terminal = new Terminal();
//            this.terminal.on('data', function (data) {
//                terminal.write(data.replace(/\r/g, '\n\r'));
//            });
            this.content = this.element.innerText.split(/\r?\n/g) || [];
            this.element.innerText = null;
            this.terminal.open(this.element);
            this.terminal.fit();
//            this.terminal.search();
            console.dir(this.terminal);
        }

        initializeTerminalContent() {
            console.dir(this.content.length);
            console.dir(this.content);
            for (let line of this.content)
            {
                this.terminal.writeln(line.trim());
            }
        }
    }
    lib.init();
    oroboros.register('component', component, TerminalComponent);
})(Oroboros);
