(function (core) {
    const oroboros = core();
    const component = 'form';
    const abstract = oroboros.abstract('component');
    const lib = {
        init: function () {

        }
    };
    const Form = class Form extends abstract {

        static classKey()
        {
            return component;
        }

        constructor(id, args, flags) {
            super(id, args, flags);
            this.nonce = null;
            this.fields = {};
            this.method = null;
            this.action = null;
            this.validation = {};
            this.reset = null;
            this.submit = null;
            this.before_submit = null;
            this.submit_error = null;
            this.submit_success = null;
            this.initialize();
        }

        initialize()
        {
            super.initialize();
            if (this.element != null)
            {
                this.setElement(this.element, true);
            }
            return this;
        }

        onReady(subject) {
            super.onReady(subject);
        }

        setElement(element, preserve) {
            const subject = this;
            const handleSubmit = function (event) {
                let result;
                try {
                    result = subject.onSubmit(event);
                    if (!result)
                    {
                        event.preventDefault();
                        event.stopPropagation();
                        return false;
                    }
                    return true;
                } catch (e)
                {
                    event.preventDefault();
                    event.stopPropagation();
                    throw new Error('Uncaught form submit error: [' + e.message + ']');
                }
            };
            const handleReset = function (event) {
                let result;
                try {
                    result = subject.onReset(event);
                    if (!result) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                } catch (e) {
                    event.preventDefault();
                    event.stopPropagation();
                    throw new Error('Uncaught form reset error: [' + e.message + ']');
                }
            }
            if (!(element instanceof HTMLFormElement))
            {
                throw new TypeError('Provided argument [element] must be an instance of [HTMLFormElement]');
            }
            if (this.element != null)
            {
                this.element.removeEventListener('submit', handleSubmit);
                this.element.removeEventListener('reset', handleReset);
            }
            super.setElement(element, preserve);
            this.element.addEventListener('submit', handleSubmit);
            this.element.addEventListener('reset', handleReset);
            return this;
        }

        validateForm()
        {
            return true;
        }

        onSubmit(event) {
            let designation = this.validateForm();
            if (designation == false)
            {
                event.stopPropagation();
                event.preventDefault();
                return false;
            }
            return true;
        }
        
        onReset(event) {
            return true;
        }
    }
    lib.init();
    oroboros.register('component', component, Form);
})(Oroboros);
