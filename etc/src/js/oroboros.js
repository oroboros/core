/* 
 * The MIT License
 *
 * Copyright 2019 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

'use strict';
(function (documentObject, windowObject) {
    /**
     * Persistent reference to the document object
     */
    const doc = documentObject || document;

    /**
     * Persistent reference to the window object
     */
    const win = windowObject || window;

    /**
     * Any data passed from the backend to the client.
     * This does not need to be present for the frontend to work,
     * however in its absence logic will have to be explicitly dictated,
     * which makes components less reusable and more opinionated.
     */
    const data = (function (window) {
        let data;
        if (typeof window.page_data !== 'undefined')
        {
            data = window.page_data;
            delete window.page_data;
        } else {
            data = {};
        }
        return data;
    })(win);

    /**
     * Unique, immutable string that persists for the entire
     * lifecycle of the page view.
     * This value may be inherited from the backend if supplied,
     * otherwise it will be arbitrarily generated on page load.
     * 
     * This is used as a reference to identify a specific page view session.
     * @type String
     */
    const fingerprint = data.fingerprint || (function () {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < 64; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
    })();

    /**
     * All of the context keys declared in the dom
     */
    const context = (function () {
        let elements = document.querySelectorAll('[data-context]');
        let values = {};
        var context_key, context_id;
        for (let node of elements)
        {
            context_key = node.attributes['data-context'].nodeValue;
            if (!values.hasOwnProperty(context_key))
            {
                values[context_key] = node;
            }
        }
        return values;
    })();

    /**
     * All of the component keys declared in the dom
     */
    const components = (function () {
        let elements = document.querySelectorAll('[data-component]');
        let values = {};
        var context_key, context_id;
        for (let node of elements)
        {
            context_key = node.attributes['data-component'].nodeValue;
            if (!values.hasOwnProperty(context_key))
            {
                values[context_key] = node;
            }
        }
        return values;
    })();

    /**
     * The internal library
     */
    const lib = {
        is_initialized: false,
        /**
         * Kicks off initialization of the internal library
         */
        init: function () {
            if (!this.is_initialized)
            {
                this.core.init();
                this.window.init(win);
                this.document.init(doc);
                this.data.init(data, this);
                this.dns.init();
                this.dom.init();
                this.scripts.init();
                this.stylesheets.init();
                this.cookie.init(doc);
                this.headers.init();
                this.debug.init();
                this.library.init();
                this.extensions.init();
                this.modules.init();
                this.endpoints.init();
                this.ajax.init();
                this.events.init();
                this.components.init(components, context);
                this.user.init();
                this.page.init();
                this.layout.init();
                this.skin.init();
                this.bootstrap.init();
                this.controllers.init();
                this.models.init();
                this.views.init();
                this.core.bootstrap();
                this.is_initialized = true;
            }
        },
        /**
         * Provides compatibility patches
         */
        polyfill: {
            /**
             * Local Storage support for legacy browsers.
             */
            localstorage: (function () {
                /* jshint ignore:start */
                if (!window.localStorage) {
                    Object.defineProperty(window, "localStorage", new (function () {
                        var aKeys = [], oStorage = {};
                        Object.defineProperty(oStorage, "getItem", {
                            value: function (sKey) {
                                return sKey ? this[sKey] : null;
                            },
                            writable: false,
                            configurable: false,
                            enumerable: false
                        });
                        Object.defineProperty(oStorage, "key", {
                            value: function (nKeyId) {
                                return aKeys[nKeyId];
                            },
                            writable: false,
                            configurable: false,
                            enumerable: false
                        });
                        Object.defineProperty(oStorage, "setItem", {
                            value: function (sKey, sValue) {
                                if (!sKey) {
                                    return;
                                }
                                document.cookie = escape(sKey) + "=" + escape(sValue) + "; expires=Tue, 19 Jan 2038 03:14:07 GMT; path=/";
                            },
                            writable: false,
                            configurable: false,
                            enumerable: false
                        });
                        Object.defineProperty(oStorage, "length", {
                            get: function () {
                                return aKeys.length;
                            },
                            configurable: false,
                            enumerable: false
                        });
                        Object.defineProperty(oStorage, "removeItem", {
                            value: function (sKey) {
                                if (!sKey) {
                                    return;
                                }
                                document.cookie = escape(sKey) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/";
                            },
                            writable: false,
                            configurable: false,
                            enumerable: false
                        });
                        Object.defineProperty(oStorage, "clear", {
                            value: function () {
                                if (!aKeys.length) {
                                    return;
                                }
                                for (var sKey in aKeys) {
                                    document.cookie = escape(sKey) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/";
                                }
                            },
                            writable: false,
                            configurable: false,
                            enumerable: false
                        });
                        this.get = function () {
                            var iThisIndx;
                            for (var sKey in oStorage) {
                                iThisIndx = aKeys.indexOf(sKey);
                                if (iThisIndx === -1) {
                                    oStorage.setItem(sKey, oStorage[sKey]);
                                } else {
                                    aKeys.splice(iThisIndx, 1);
                                }
                                delete oStorage[sKey];
                            }
                            for (aKeys; aKeys.length > 0; aKeys.splice(0, 1)) {
                                oStorage.removeItem(aKeys[0]);
                            }
                            for (var aCouple, iKey, nIdx = 0, aCouples = document.cookie.split(/\s*;\s*/); nIdx < aCouples.length; nIdx++) {
                                aCouple = aCouples[nIdx].split(/\s*=\s*/);
                                if (aCouple.length > 1) {
                                    oStorage[iKey = unescape(aCouple[0])] = unescape(aCouple[1]);
                                    aKeys.push(iKey);
                                }
                            }
                            return oStorage;
                        };
                        this.configurable = false;
                        this.enumerable = true;
                    })());
                }
                /* jshint ignore:end */
            })(),
            /**
             * Implementation of the Promise api for legacy browsers.
             * @see https://github.com/taylorhakes/promise-polyfill
             */
            promise: (function (global, factory) {
                typeof exports === 'object' && typeof module !== 'undefined' ? factory() :
                        typeof define === 'function' && define.amd ? define(factory) :
                        (factory());
            }(this, (function () {
                'use strict';

                /**
                 * @this {Promise}
                 */
                function finallyConstructor(callback) {
                    var constructor = this.constructor;
                    return this.then(
                            function (value) {
                                return constructor.resolve(callback()).then(function () {
                                    return value;
                                });
                            },
                            function (reason) {
                                return constructor.resolve(callback()).then(function () {
                                    return constructor.reject(reason);
                                });
                            }
                    );
                }

                // Store setTimeout reference so promise-polyfill will be unaffected by
                // other code modifying setTimeout (like sinon.useFakeTimers())
                var setTimeoutFunc = setTimeout;

                function noop() {}

                // Polyfill for Function.prototype.bind
                function bind(fn, thisArg) {
                    return function () {
                        fn.apply(thisArg, arguments);
                    };
                }

                /**
                 * @constructor
                 * @param {Function} fn
                 */
                function Promise(fn) {
                    if (!(this instanceof Promise))
                        throw new TypeError('Promises must be constructed via new');
                    if (typeof fn !== 'function')
                        throw new TypeError('not a function');
                    /** @type {!number} */
                    this._state = 0;
                    /** @type {!boolean} */
                    this._handled = false;
                    /** @type {Promise|undefined} */
                    this._value = undefined;
                    /** @type {!Array<!Function>} */
                    this._deferreds = [];

                    doResolve(fn, this);
                }

                function handle(self, deferred) {
                    while (self._state === 3) {
                        self = self._value;
                    }
                    if (self._state === 0) {
                        self._deferreds.push(deferred);
                        return;
                    }
                    self._handled = true;
                    Promise._immediateFn(function () {
                        var cb = self._state === 1 ? deferred.onFulfilled : deferred.onRejected;
                        if (cb === null) {
                            (self._state === 1 ? resolve : reject)(deferred.promise, self._value);
                            return;
                        }
                        var ret;
                        try {
                            ret = cb(self._value);
                        } catch (e) {
                            reject(deferred.promise, e);
                            return;
                        }
                        resolve(deferred.promise, ret);
                    });
                }

                function resolve(self, newValue) {
                    try {
                        // Promise Resolution Procedure: https://github.com/promises-aplus/promises-spec#the-promise-resolution-procedure
                        if (newValue === self)
                            throw new TypeError('A promise cannot be resolved with itself.');
                        if (
                                newValue &&
                                (typeof newValue === 'object' || typeof newValue === 'function')
                                ) {
                            var then = newValue.then;
                            if (newValue instanceof Promise) {
                                self._state = 3;
                                self._value = newValue;
                                finale(self);
                                return;
                            } else if (typeof then === 'function') {
                                doResolve(bind(then, newValue), self);
                                return;
                            }
                        }
                        self._state = 1;
                        self._value = newValue;
                        finale(self);
                    } catch (e) {
                        reject(self, e);
                    }
                }

                function reject(self, newValue) {
                    self._state = 2;
                    self._value = newValue;
                    finale(self);
                }

                function finale(self) {
                    if (self._state === 2 && self._deferreds.length === 0) {
                        Promise._immediateFn(function () {
                            if (!self._handled) {
                                Promise._unhandledRejectionFn(self._value);
                            }
                        });
                    }

                    for (var i = 0, len = self._deferreds.length; i < len; i++) {
                        handle(self, self._deferreds[i]);
                    }
                    self._deferreds = null;
                }

                /**
                 * @constructor
                 */
                function Handler(onFulfilled, onRejected, promise) {
                    this.onFulfilled = typeof onFulfilled === 'function' ? onFulfilled : null;
                    this.onRejected = typeof onRejected === 'function' ? onRejected : null;
                    this.promise = promise;
                }

                /**
                 * Take a potentially misbehaving resolver function and make sure
                 * onFulfilled and onRejected are only called once.
                 *
                 * Makes no guarantees about asynchrony.
                 */
                function doResolve(fn, self) {
                    var done = false;
                    try {
                        fn(
                                function (value) {
                                    if (done)
                                        return;
                                    done = true;
                                    resolve(self, value);
                                },
                                function (reason) {
                                    if (done)
                                        return;
                                    done = true;
                                    reject(self, reason);
                                }
                        );
                    } catch (ex) {
                        if (done)
                            return;
                        done = true;
                        reject(self, ex);
                    }
                }

                Promise.prototype['catch'] = function (onRejected) {
                    return this.then(null, onRejected);
                };

                Promise.prototype.then = function (onFulfilled, onRejected) {
                    // @ts-ignore
                    var prom = new this.constructor(noop);

                    handle(this, new Handler(onFulfilled, onRejected, prom));
                    return prom;
                };

                Promise.prototype['finally'] = finallyConstructor;

                Promise.all = function (arr) {
                    return new Promise(function (resolve, reject) {
                        if (!arr || typeof arr.length === 'undefined')
                            throw new TypeError('Promise.all accepts an array');
                        var args = Array.prototype.slice.call(arr);
                        if (args.length === 0)
                            return resolve([]);
                        var remaining = args.length;

                        function res(i, val) {
                            try {
                                if (val && (typeof val === 'object' || typeof val === 'function')) {
                                    var then = val.then;
                                    if (typeof then === 'function') {
                                        then.call(
                                                val,
                                                function (val) {
                                                    res(i, val);
                                                },
                                                reject
                                                );
                                        return;
                                    }
                                }
                                args[i] = val;
                                if (--remaining === 0) {
                                    resolve(args);
                                }
                            } catch (ex) {
                                reject(ex);
                            }
                        }

                        for (var i = 0; i < args.length; i++) {
                            res(i, args[i]);
                        }
                    });
                };

                Promise.resolve = function (value) {
                    if (value && typeof value === 'object' && value.constructor === Promise) {
                        return value;
                    }

                    return new Promise(function (resolve) {
                        resolve(value);
                    });
                };

                Promise.reject = function (value) {
                    return new Promise(function (resolve, reject) {
                        reject(value);
                    });
                };

                Promise.race = function (values) {
                    return new Promise(function (resolve, reject) {
                        for (var i = 0, len = values.length; i < len; i++) {
                            values[i].then(resolve, reject);
                        }
                    });
                };

                // Use polyfill for setImmediate for performance gains
                Promise._immediateFn =
                        (typeof setImmediate === 'function' &&
                                function (fn) {
                                    setImmediate(fn);
                                }) ||
                        function (fn) {
                            setTimeoutFunc(fn, 0);
                        };

                Promise._unhandledRejectionFn = function _unhandledRejectionFn(err) {
                    if (typeof console !== 'undefined' && console) {
                        console.warn('Possible Unhandled Promise Rejection:', err); // eslint-disable-line no-console
                    }
                };

                /** @suppress {undefinedVars} */
                var globalNS = (function () {
                    // the only reliable means to get the global object is
                    // `Function('return this')()`
                    // However, this causes CSP violations in Chrome apps.
                    if (typeof self !== 'undefined') {
                        return self;
                    }
                    if (typeof window !== 'undefined') {
                        return window;
                    }
                    if (typeof global !== 'undefined') {
                        return global;
                    }
                    throw new Error('unable to locate global object');
                })();

                if (!('Promise' in globalNS)) {
                    globalNS['Promise'] = Promise;
                } else if (!globalNS.Promise.prototype['finally']) {
                    globalNS.Promise.prototype['finally'] = finallyConstructor;
                }

            })))
        },
        /**
         * Handles common functions used for utility purposes.
         */
        library: {
            elements: {},
            is_initialized: false,
            init: function (object)
            {
                // Do not duplicate initialization
                if (!this.is_initialized)
                {
                    this.is_initialized = true;
                }
            },
            /**
             * Saves CPU on the client by preventing excessive nested event firing
             * @param {type} func
             * @param {type} wait
             * @param {type} immediate
             * @return {Function}
             */
            debounce: function (func, wait, immediate) {
                var timeout;
                return function () {
                    var context = this, args = arguments;
                    var later = function () {
                        timeout = null;
                        if (!immediate)
                            func.apply(context, args);
                    };
                    var callNow = immediate && !timeout;
                    clearTimeout(timeout);
                    timeout = setTimeout(later, wait);
                    if (callNow)
                        func.apply(context, args);
                };
            },
            /**
             * Returns the type of a variable as a string.
             * 
             * @param mixed obj
             * @returns string
             */
            toType: function (obj)
            {
                return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase();
            },
            /**
             * Checks if a given variable is a class instance
             * 
             * @param mixed obj
             * @returns boolean
             */
            isClass: function (obj)
            {
                return typeof obj === 'function' && /^\s*class\s+/.test(obj.toString());
            },
            device: {
                pixelRatio: function () {
                    var ratio = 1;
                    if (win.screen.systemXDPI !== undefined && win.screen.logicalXDPI !== undefined && win.screen.systemXDPI > win.screen.logicalXDPI)
                    {
                        ratio = win.screen.systemXDPI / win.screen.logicalXDPI;
                    } else if (win.devicePixelRatio !== undefined) {
                        ratio = win.devicePixelRatio;
                    }
                    return ratio;
                }
            },
            validate: {
                type: function (supplied, expected)
                {
                    if (typeof supplied !== expected)
                    {
                        throw new TypeError('Invalid argument supplied. Expected [' + expected + '] but recieved [' + lib.library.toType(supplied) + '].');
                    }
                },
                instance: function (supplied, expected)
                {
                    if (!(typeof supplied === "object" && (object instanceof expected)))
                    {
                        throw new TypeError('Invalid argument supplied. Expected instance of [' + expected.name + '] but recieved [' + (typeof supplied === "object" ? supplied.prototype.name : lib.library.toType(supplied)) + '].');
                    }
                }
            },
            url: {
                host: function (uri)
                {
                    let url = new URL(uri);
                    return url.hostname;
                },
                path: function (uri)
                {
                    let url = new URL(uri);
                    return url.pathname;
                },
                protocol: function (uri)
                {
                    let url = new URL(uri);
                    return url.protocol.replace(':', '');
                },
                port: function (uri)
                {
                    let url = new URL(uri);
                    return url.port;
                },
                hash: function (uri)
                {
                    let url = new URL(uri);
                    return url.hash;
                },
                href: function (uri)
                {
                    let url = new URL(uri);
                    return url.href;
                }
            }
        },
        /**
         * Represents the core internal logic.
         * Base level abstraction, as well as nulled objects, defaults,
         * and internal classes are declared and instantiated here.
         */
        core: {
            abstracts: null,
            lib: null,
            nulls: null,
            defaults: null,
            internals: null,
            is_initialized: false,
            init: function () {
                if (!this.is_initialized)
                {
                    this.abstracts = {};
                    this.lib = {};
                    this.nulls = {};
                    this.defaults = {};
                    this.internals = {};
                    this.build_abstraction();
                    this.build_core();
                    this.build_nulls();
                    this.build_defaults();
                    this.build_internals();
                    this.is_initialized = true;
                }
            },
            build_abstraction: function () {
                class AbstractBase {

                    static classType()
                    {
                        return null;
                    }

                    static classKey()
                    {
                        return null;
                    }

                    static verifyElement(element, valid)
                    {
                        if (typeof valid === 'undefined')
                        {
                            return;
                        }
                        if (!(typeof element !== 'undefined' && typeof element.constructor !== 'undefined' && (element.constructor === valid || element.constructor.prototype instanceof valid)))
                        {
                            throw new TypeError('Provided element is not valid in [' + this.name + ']. Expected [' + valid.name + '], received [' + ((element === null) ? 'null' : (element === undefined ? 'undefined' : element.constructor.name)) + '].');
                        }
                    }

                    constructor(id, args, flags) {
                        this.id = id;
                        this.args = args || {};
                        this.flags = flags || {};
                        this.type = this.constructor.classType();
                        this.key = this.constructor.classKey();
                        this.library = lib.library;
                        this.is_loaded = false;
                        this.is_ready = false;
                        this.window = win;
                        this.document = doc;
                        this.data = {};
                        if (this.type === null)
                        {
                            throw new Error('Error in [' + this.constructor.name + ']. No class type is defined by the object class.');
                        }
                        if (this.key === null)
                        {
                            throw new Error('Error in [' + this.constructor.name + ']. No class key is defined by the object class.');
                        }
                        if (this.hasData())
                        {
                            this.getData();
                        }
                        lib.events.ready(this.onReady, this);
                        lib.events.load(this.onLoad, this);
                        return this;
                    }

                    initialize() {

                    }

                    onReady(subject) {
                        subject.is_ready = true;
                    }

                    onLoad(subject) {
                        subject.is_loaded = true;
                    }

                    getData()
                    {
                        const valid = lib.core.map;
                        let type = this.constructor.classType();
                        let key = this.constructor.classKey();
                        let target;
                        if (!this.hasData())
                        {
                            this.data = {};
                            return;
                        }
                        target = lib[valid[type]];
                        this.data = target.data[key][this.id];
                    }

                    hasData()
                    {
                        const valid = lib.core.map;
                        let type = this.constructor.classType();
                        let key = this.constructor.classKey();
                        let target;
                        if (!valid.hasOwnProperty(type))
                        {
                            return false;
                        }
                        target = lib[valid[type]];
                        if (this.id != null && target.data.hasOwnProperty(key) && target.data[key].hasOwnProperty(this.id))
                        {
                            return true;
                        }
                        return false;
                    }
                }
                class AbstractAjax extends AbstractBase {

                    static classType()
                    {
                        return 'ajax';
                    }

                    constructor(id, args, flags) {
                        super(id, args || {}, flags || {});
                        this.envelope = lib.ajax.envelope;
                    }

                    ajax(type, args) {
                        return new this.envelope(type, args || this.args);
                    }

                    get(args) {
                        return new this.envelope('get', args || this.args);
                    }

                    post(args) {
                        return new this.envelope('post', args || this.args);

                    }

                    put(args) {
                        return new this.envelope('put', args || this.args);

                    }

                    del(args) {
                        return new this.envelope('delete', args || this.args);

                    }

                    options(args) {
                        return new this.envelope('options', args || this.args);
                    }

                    trace(args) {
                        return new this.envelope('trace', args || this.args);
                    }

                    head(args) {
                        return new this.envelope('head', args || this.args);
                    }

                    patch(args) {
                        return new this.envelope('patch', args || this.args);
                    }
                }
                class AbstractBootstrap extends AbstractBase {

                    static classType()
                    {
                        return 'bootstrap';
                    }

                    constructor(id, args, flags) {
                        super(id, args, flags);
                    }
                }
                class AbstractModule extends AbstractBase {

                    static classType()
                    {
                        return 'module';
                    }

                    constructor(id, args, flags) {
                        super(id, args, flags);
                    }
                }
                class AbstractComponent extends AbstractBase {

                    static classType()
                    {
                        return 'component';
                    }

                    static getValidElement()
                    {
                        return Element;
                    }

                    constructor(id, args, flags) {
                        super(id, args, flags);
                        try {
                            this.children = this.args.hasOwnProperty('children') ? this.args.children : {};
                            this.context = this.args.hasOwnProperty('context') ? this.args.context : null;
                            this.element = this.args.hasOwnProperty('element') ? this.args.element : null;
                            this.parent = this.args.hasOwnProperty('parent') ? this.args.parent : null;
                            this.category = this.args.hasOwnProperty('category') ? this.args.category : null;
                            this.attributes = this.args.hasOwnProperty('attributes') ? this.args.attributes : {};
                            this.class = this.args.hasOwnProperty('class') ? this.args.class : [];
                            this.constructor.verifyElement(this.args.element, this.constructor.getValidElement());
                            this.element = this.args.element;
                        } catch (e) {
                            this.element = null;
                        }
                        this.initialize();
                    }

                    initialize() {
                        super.initialize();
                        if (this.element !== null)
                        {
                            this.setElement(this.element, true);
                        }
                        this.intersector = new lib.common.observer(function () {
                            subject.onIntersect(subject);
                        });
                        return this;
                    }

                    onReady(subject) {
                        super.onReady(subject);
                    }

                    onLoad(subject) {
                        super.onLoad(subject);
                    }

                    onIntersect(subject) {

                    }

                    getElement() {
                        return this.element;
                    }

                    setElement(element, preserve = false) {
                        if (!(element instanceof Element))
                        {
                            throw new TypeError('Provided argument [element] must be an instance of [Element]');
                        }
                        if (preserve)
                        {
                            // Inject any missing existing attributes into the provided element prior to updating
                            for (let attr in this.attributes)
                            {
                                if (!(element.attributes.hasOwnProperty(attr) && element.attributes[attr] === this.attributes[attr]))
                                {
                                    if (['id', 'class', 'data-context', 'data-component', 'data-parent', 'data-category'].includes(attr))
                                    {
                                        // Gets handled later
                                        continue;
                                    }
                                    element.attributes[attr] = this.attributes[attr];
                                }
                            }
                            if (this.context !== null && !(element.attributes.hasOwnProperty('data-context') && element.attributes['data-context'] === this.context))
                            {
                                element.attributes['data-context'] = this.context;
                            }
                            if (this.category !== null && !(element.attributes.hasOwnProperty('data-category') && element.attributes['data-category'] === this.category))
                            {
                                element.attributes['data-category'] = this.category;
                            }
                            if (this.parent !== null && !(element.attributes.hasOwnProperty('data-parent') && element.attributes['data-parent'] === this.parent))
                            {
                                element.attributes['data-parent'] = this.parent;
                            }
                            for (let classname of this.class)
                            {
                                if (!element.classList.contains(classname))
                                {
                                    element.classList.push(classname);
                                }
                            }
                        }
                        if (!(element.attributes.hasOwnProperty('data-component') && element.attributes['data-component'] === this.key))
                        {
                            element.attributes['data-component'] = this.key;
                            console.log(this.key);
                            console.log(element);
                        }
                        this.element = element;
                        this.context = (this.element.attributes !== undefined && this.element.attributes['data-context'] !== undefined) ? this.element.attributes['data-context'].nodeValue : null;
                        this.parent = (this.element.attributes !== undefined && this.element.attributes['data-parent'] !== undefined) ? this.element.attributes['data-parent'].nodeValue : null;
                        this.category = (this.element.attributes !== undefined && this.element.attributes['data-category'] !== undefined) ? this.element.attributes['data-category'].nodeValue : null;
                        this.class = this.element.classList || [];
                        return this;
                    }

                    releaseElement() {
                        this.element = null;
                        this.context = null;
                        this.parent = null;
                        this.category = null;
                        this.class = [];
                        return this;
                    }

                    hasParent(id) {
                        return this.parent !== null && this.parent.id === id;
                    }

                    getParent() {
                        return this.parent;
                    }

                    setParent(parent) {
                        lib.components.verify(child);
                        this.parent = parent;
                        if (!this.attributes === null)
                        {

                        }
                        return this;
                    }

                    deleteParent() {
                        this.parent = null;
                        return this;
                    }

                    hasChild(id) {
                        return this.children.hasOwnProperty(id);
                    }

                    getChild(id) {
                        if (!this.hasChild())
                        {
                            throw new Error('Specified child component [' + id + '] does not exist in this scope.');
                        }
                        return this.children[id];
                    }

                    setChild(child) {
                        lib.components.verify(child);
                        this.children[child.id] = child;
                        return this;
                    }

                    createChild(key, id, args, flags)
                    {
                        return this;
                    }

                    deleteChild(id)
                    {
                        delete this.children[id];
                        return this;
                    }

                    ajax(type, args)
                    {
                        let request_type = type || 'get';
                        let request_args = args || {};
                        return new lib.core.defaults.ajax(request_type, request_args).ajax(request_type, request_args);
                    }
                }
                class AbstractRouter extends AbstractBase {

                    static classType()
                    {
                        return 'router';
                    }

                    constructor(id, args, flags) {
                        super(id, args, flags);
                        this.routes = {};
                        this.initialize();
                    }

                    initialize()
                    {
                        super.initialize();
                        this.setRoutes(this.args);
                        return this;
                    }

                    setRoutes(routes)
                    {
                        this.routes = routes;
                        return this;
                    }

                    hasRoute(key)
                    {
                        return this.routes.hasOwnProperty(key);
                    }

                    getRoute(key)
                    {
                        if (!this.hasRoute(key))
                        {
                            throw new Error('No route exists for key [' + key + '].');
                        }
                        return this.routes[key];
                    }

                    setRoute(key, subject)
                    {
                        this.routes[key] = subject;
                        return this;
                    }

                    onReady(subject) {

                    }

                }

                class AbstractPage extends AbstractBase {

                    static classType()
                    {
                        return 'page';
                    }

                    static getValidElement()
                    {
                        return HTMLBodyElement;
                    }

                    constructor(id, args, flags) {
                        super(id, args, flags);
                        let element;
                        try {
                            // Check if the body element was passed,
                            // which means it is attached on initialization.
                            this.constructor.verifyElement(this.args.element, this.constructor.getValidElement());
                            element = this.args.element;
                            this.is_attached = true;
                        } catch (e) {
                            // Use the default and mark the object as unattached.
                            element = doc.body;
                            this.is_attached = false;
                        }
                        this.cookie = lib.cookie.proxy();
                        this.element = element || doc.body;
                        this.layout = this.args.layout || front_controller.layout;
                        this.user = lib.user.current_user;
                        this.skin = this.args.skin || front_controller.skin;
                        this.is_registered = false;
                        this.is_active = false;
                        this.components = this.args.components || {};
                        this.endpoints = this.args.endpoints || {};
                        this.modules = this.args.modules || {};
                        this.extensions = this.args.extensions || {};
                        this.initialize();
                        return this;
                    }

                    initialize() {
                        super.initialize();
                        const subject = this;
                        let monitor_class = this.window.MutationObserver
                                || win.WebKitMutationObserver
                                || win.MozMutationObserver;

                        this.pagemonitor = new monitor_class(this.library.debounce(function (mutation) {
                            if (mutation.type == 'childList') {
                                subject.onDomChange();
                            } else if (mutation.type == 'attributes') {
                                subject.onAttributeChange();
                            } else if (mutation.type == 'subtree') {
                                subject.onSubtreeChange();
                            }
                        }, 250));
                        this.register();
                    }

                    onLoad(subject) {
                        super.onLoad(subject);
                        let element = subject.element;
                        let classList = element.classList;
                        let layout;
                        subject.pagemonitor.observe(subject.element, {
                            attributes: true,
                            childList: true,
                            subtree: true
                        });
                        for (let classname of classList)
                        {
                            if (lib.layout.has(classname))
                            {
                                layout = lib.layout.get(classname);
                                break;
                            }
                        }
                        if (typeof layout === 'undefined') {
                            layout = lib.layout.get('default');
                        }
                        subject.setLayout(layout);
                    }

                    onDomChange() {

                    }

                    onAttributeChange() {

                    }

                    onSubtreeChange() {

                    }

                    /**
                     * Registers the page in the page queue as
                     * a target for current page replacement
                     */
                    register() {
                        if (!this.is_registered)
                        {
                            this.is_registered = true;
                        } else {
                            throw new Error('Page object [' + this.id + '] is already registered.');
                        }
                        return this;
                    }

                    /**
                     * Unregisters the page in the page queue as
                     * a target for page replacement
                     */
                    unregister() {
                        if (this.is_registered)
                        {
                            this.is_registered = false;
                        } else {
                            throw new Error('Page object [' + this.id + '] cannot be unregistered because it is not currently registered.');
                        }
                        return this;
                    }

                    /**
                     * Attaches the object as the currently active page.
                     * 
                     * This does nothing if the current instance is already attached.
                     */
                    attach() {
                        if (!this.is_registered)
                        {
                            throw new Error('Cannot attach page [' + this.id + '] because it is not currently registered.');
                        }
                        if (!this.is_active)
                        {
                            this.is_active = true;
                        } else {
                            throw new Error('Page object [' + this.id + '] cannot be attached because it is already attached.');
                        }
                        if (this.element.id !== this.key)
                        {
                            this.element.id = this.key;
                        }
                        if (!this.layout.is_active)
                        {
                            this.layout.activate();
                        }
//                        console.groupCollapsed('Page Controller [' + this.key + '] Activated');
//                        console.dir(this);
//                        console.groupEnd();
                        return this;
                    }

                    /**
                     * Detaches the object as the currently active page.
                     * 
                     * This does nothing if the current instance is not attached.
                     */
                    detach() {
                        if (this.is_active)
                        {
                            if (!this.is_registered)
                            {
                                throw new Error('Page object [' + this.id + '] cannot be detached because it is not currently registered.');
                            }
                            this.is_active = false;
                        } else {
                            throw new Error('Page object [' + this.id + '] cannot be detached because it is not currently attached.');
                        }
                        if (this.element.id === this.key)
                        {
                            this.element.id = '';
                        }
                        return this;
                    }

                    /**
                     * Replaces the currently active page with the newly supplied page instance.
                     * The given instance must also extend this class.
                     * 
                     * This does nothing if the current instance is not attached.
                     */
                    replace(new_instance) {
                        if (this.is_active) {
                            if (new_instance.is_registered)
                            {
                                throw new Error('Page object [' + this.id + '] cannot be replaced with unregistered page object [' + new_instance.id + '].');
                            }
                            this.detach();
                            new_instance.attach();
                        } else {
                            throw new Error('Page object [' + this.id + '] cannot be replaced with [' + new_instance.id + '] because it is not currently attached.');
                        }
                        return new_instance;
                    }

                    setUser(user) {
                        let current = this.user;
                        try {
                            lib.user.verifyExtendingClass(user);
                            this.user = user;
                        } catch (e) {
                            this.user = current;
                            throw e;
                        }
                        return this;
                    }

                    setSkin(skin)
                    {
                        let current = this.skin;
                        try {
                            lib.skin.verifyExtendingClass(skin);
                            this.skin = skin;
                        } catch (e) {
                            this.skin = current;
                            throw e;
                        }
                        return this;
                    }

                    setLayout(layout)
                    {
                        let current = this.layout;
                        try {
                            lib.layout.verifyExtendingClass(layout);
                            this.layout = new layout(this.constructor.classKey());
                        } catch (e) {
                            this.layout = current;
                            throw e;
                        }
                        return this;
                    }

                    loadComponent(key, component_context) {
                        let element, component;
                        if (!context.hasOwnProperty(component_context))
                        {
                            throw new TypeError('No dom element with context [' + component_context + '] found.');
                        }
                        element = context[component_context];
                        component = oroboros.create('component', key, component_context, {element: element});
                        this.components[key] = component;
                        return this;
                    }
                }
                class AbstractUser extends AbstractBase {

                    static classType()
                    {
                        return 'user';
                    }

//                    constructor(id, args, flags) {
                    constructor(id, username, organization, details, role, enabled, logged_in, verified) {
//                        super(id, args, flags);
                        super(id);
                        this.username = username || 'anonymous';
                        this.organization = organization || 'public';
                        this.details = details || {};
                        this.role = role || 'default';
                        this.enabled = enabled || false;
                        this.logged_in = logged_in || false;
                        this.verified = verified || false;
                        this.initialize();
                        return this;
                    }

                    initialize() {
                        super.initialize();
                    }

                    username()
                    {
                        return this.username;
                    }

                    organization()
                    {
                        return this.organization;
                    }

                    details(key)
                    {
                        if (typeof key === 'undefined')
                        {
                            return this.details;
                        }
                        if (this.details.hasOwnProperty(key))
                        {
                            return this.details[key];
                        }
                        throw new TypeError('User [' + this.id + '] does not have detail key [' + key + '].');
                    }
                }
                class AbstractEndpoint extends AbstractBase {

                    static classType()
                    {
                        return 'endpoint';
                    }

                    constructor(id, args, flags) {
                        super(id, args, flags);
                    }
                }
                class AbstractExtension extends AbstractBase {

                    static classType()
                    {
                        return 'extension';
                    }

                    constructor(id, args, flags) {
                        super(id, args, flags);
                        this.modules = {};
                        this.components = {};
                        this.skins = {};
                        this.layouts = {};
                        this.endpoints = {};
                        this.initialize();
                    }

                    initialize()
                    {

                    }

                    getModules()
                    {

                    }

                    getComponents()
                    {

                    }

                    getSkins()
                    {

                    }

                    getLayouts()
                    {

                    }

                    getEndpoints()
                    {

                    }
                }
                class AbstractLayout extends AbstractBase {

                    static classType()
                    {
                        return 'layout';
                    }

                    constructor(id, args, flags) {
                        super(id, args, flags);
                        this.element = this.args.element || null;
                        this.skin = this.args.skin || null;
                        this.is_active = false;
                        this.sections = {};
                        this.components = {};
                        this.bindings = {};
                        this.initialize();
                        return this;
                    }

                    initialize() {
                        super.initialize();
//                        console.dir(this);
                    }

                    activate() {
                        this.is_active = true;
                    }

                    deactivate() {
                        this.is_active = false;
                    }

                    loadLayoutSection(key, section_id, section_context, section_flags) {
                        const section_class = lib.core.defaults.layout_section;
                        let id = section_id;
                        let context = section_context;
                        let instance = new section_class(id, section_context, section_flags);
                        this.sections[key] = instance;
                    }

                    setComponent(section, key, component)
                    {
                        return this;
                    }

                    getComponent(section, key)
                    {

                    }

                    hasComponent(section, key)
                    {

                    }

                    setSkin(skin)
                    {
                        return this;
                    }

                    getSkin()
                    {

                    }

                    hasSkin(key)
                    {

                    }
                }
                class AbstractLayoutSection extends AbstractBase {

                    static classType()
                    {
                        return 'layout_section';
                    }

                    static layoutKey() {
                        return null;
                    }

                    constructor(id, args, flags) {
                        super(id, args, flags);
                        let element;
                        this.bound = false;
                        this.element = this.args.element || null;
                        this.context = this.args.context || null;
                        this.selector_id = this.args.id || null;
                        this.initialize();
                    }

                    initialize()
                    {
                        super.initialize();
                        if (this.selector_id === null && this.args.id)
                        {
                            this.selector_id = this.args.id;
                        }
                        if (this.selector_id)
                        {
                            try {
//                                console.dir(doc.getElementById(this.selector_id));
                                this.bind(doc.getElementById(this.selector_id));
                            } catch (e) {
                                this.unbind();
                            }
                        }
                        if (!(this.element instanceof HTMLElement))
                        {
                            this.unbind();
                        }
                        return this;
                    }

                    bind(selector)
                    {
                        if (!(selector instanceof HTMLElement))
                        {
                            throw new TypeError('Provided selector must be an instance of HTMLElement. Received [' + ((selector === null) ? 'null' : (selector === undefined ? 'undefined' : selector.name)) + '].');
                        }
                        this.element = selector;
                        if (this.element.id)
                        {
                            this.selector_id = this.element.id;
                        } else {
                            this.selector_id = false;
                        }
                        this.updateComponents();
                        this.bound = true;
                        return this;
                    }

                    unbind()
                    {
                        this.element = null;
                        this.bound = false;
                        this.releaseComponents();
                        return this;
                    }

                    updateComponents()
                    {
                        let components;
                        if (this.element.childNodes !== undefined && this.element.childNodes.length > 0)
                        {
                            // Iterate over the children
                            components = lib.components.extract(this.element.childNodes);
                        } else {
                            components = {};
                        }
                        this.components = components;
                        return this;
                    }

                    releaseComponents() {
                        this.components = {};
                        return this;
                    }
                }
                class AbstractSkin extends AbstractBase {

                    static classType()
                    {
                        return 'skin';
                    }

                    constructor(id, args, flags) {
                        super(id, args, flags);
                    }

                    initialize()
                    {
                        super.initialize();
                    }
                }
                class AbstractEvent extends AbstractBase {

                    static classType()
                    {
                        return 'event';
                    }

                    constructor(id, args, flags) {
                        super(id, args, flags);
                    }

                    initialize()
                    {
                        super.initialize();
                    }
                }
                class AbstractModel extends AbstractBase {

                    static classType()
                    {
                        return 'model';
                    }

                    constructor(id, args, flags) {
                        super(id, args, flags);
                    }

                    initialize()
                    {
                        super.initialize();
                    }
                }
                class AbstractController extends AbstractBase {

                    static classType()
                    {
                        return 'controller';
                    }

                    constructor(id, args, flags) {
                        super(id, args, flags);
                    }

                    initialize()
                    {
                        super.initialize();
                    }
                }
                class AbstractView extends AbstractBase {

                    static classType()
                    {
                        return 'view';
                    }

                    constructor(id, args, flags) {
                        super(id, args, flags);
                    }

                    initialize()
                    {
                        super.initialize();
                    }
                }
                this.abstracts.ajax = AbstractAjax;
                this.abstracts.bootstrap = AbstractBootstrap;
                this.abstracts.module = AbstractModule;
                this.abstracts.component = AbstractComponent;
                this.abstracts.router = AbstractRouter;
                this.abstracts.page = AbstractPage;
                this.abstracts.user = AbstractUser;
                this.abstracts.endpoint = AbstractEndpoint;
                this.abstracts.layout = AbstractLayout;
                this.abstracts.layout_section = AbstractLayoutSection;
                this.abstracts.skin = AbstractSkin;
                this.abstracts.event = AbstractEvent;
                this.abstracts.base = AbstractBase;
                this.abstracts.model = AbstractModel;
                this.abstracts.controller = AbstractController;
                this.abstracts.view = AbstractView;
            },
            build_core: function () {
                const library = {
                    index: function () {
                        return this.registered.keys();
                    },
                    isRegistered: function (key) {
                        return this.registered.hasOwnProperty(key);
                    },
                    register: function (key, object) {
                        this.verify(object);
                        if (this.registered.hasOwnProperty(key))
                        {
                            throw new TypeError('Unexpected ' + this.type + ' registration. Key [' + key + '] is already registered.');
                        }
                        this.registered[key] = object;
                    },
                    hasInstance: function (key) {
                        return this.instances.hasOwnProperty(key);
                    },
                    getInstance: function (key) {
                        if (!this.hasInstance(key))
                        {
                            throw new TypeError('No known instance in ' + this.type + ' exists for key [' + key + '].');
                        }
                        return this.instances[key];
                    },
                    unregister: function (key) {
                        let object;
                        if (this.objects.hasOwnProperty(key))
                        {
                            object = this.objects[key];
                            delete this.registered[key];
                            return true;
                        }
                        return false;
                    },
                    create: function (key, id, args, flags) {
                        let target, instance;
                        if (!this.registered.hasOwnProperty(key))
                        {
                            throw new TypeError('Provided key [' + key + '] has no registered instance of type [' + this.getType() + '].');
                        }
                        target = this.registered[key];
                        instance = new target(id, args, flags);
                        this.instances[instance.id] = instance;
                        return instance;
                    },
                    get: function (key) {
                        if (!this.has(key))
                        {
                            throw new TypeError('Key [' + key + '] does not exist in ' + this.type + '.');
                        }
                        return this.registered[key];
                    },
                    has: function (key) {
                        return this.registered.hasOwnProperty(key);
                    },
                    verify: function (object) {
                        let abstract = this.abstract;
                        if (!(object.prototype instanceof abstract))
                        {
                            throw new TypeError('Provided object [' + object.prototype.name + '] does not extend expected abstract [' + abstract.name + '].');
                        }
                    },
                    verifyExtendingClass: function (subject) {
                        let abstract = this.abstract;
                        if (!(typeof subject !== 'undefined' && typeof subject.constructor !== 'undefined' && (subject === abstract || subject.prototype === abstract || subject.prototype instanceof abstract || subject.prototype.constructor.prototype === abstract || subject.prototype.constructor.prototype instanceof abstract)))
                        {
                            throw new TypeError('Provided object [' + subject.prototype.constructor.name + '] does not extend expected abstract [' + abstract.name + '].');
                        }
                    },
                    verifyObject: function (object) {
                        let abstract = this.abstract;
                        if (!(typeof object !== 'undefined' && typeof object.constructor !== 'undefined' && (object.prototype === abstract || object.prototype instanceof abstract || object.prototype.constructor.prototype === abstract || object.prototype.constructor.prototype instanceof abstract)))
                        {
                            throw new TypeError('Provided object [' + object.prototype.constructor.name + '] does not extend expected abstract [' + abstract.name + '].');
                        }
                    },
                    getType: function () {
                        return this.type;
                    },
                    getPath: function () {
                        const valid = lib.core.map;
                        return lib[valid[this.getType()]];
                    }
                };
                const sections = {
                    'endpoint': 'endpoints',
                    'user': 'user',
                    'page': 'page',
                    'component': 'components',
                    'layout': 'layout',
                    'skin': 'skin',
                    'model': 'models',
                    'controller': 'controllers',
                    'view': 'views',
                    'extension': 'extensions',
                    'module': 'modules'
                };
                for (let section in sections)
                {
                    lib[sections[section]].abstract = this.abstracts[section];
                    lib[sections[section]].type = section;
                    lib[sections[section]].data = null;
                    lib[sections[section]].keys = {};
                    lib[sections[section]].instances = {};
                    lib[sections[section]].registered = {};
                    for (let func in library)
                    {
                        lib[sections[section]][func] = library[func];
                    }
                }
                this.map = sections;
            },
            build_nulls: function () {
                class NullAjax extends this.abstracts.ajax {
                    static classKey()
                    {
                        return 'null';
                    }
                }
                class NullBootstrap extends this.abstracts.bootstrap {
                    static classKey()
                    {
                        return 'null';
                    }
                }
                class NullModule extends this.abstracts.module {
                    static classKey()
                    {
                        return 'null';
                    }
                }
                class NullComponent extends this.abstracts.component {
                    static classKey()
                    {
                        return 'null';
                    }
                }
                class NullRouter extends this.abstracts.router {
                    static classKey()
                    {
                        return 'null';
                    }
                }
                class NullPage extends this.abstracts.page {
                    static classKey()
                    {
                        return 'null';
                    }
                }
                class NullUser extends this.abstracts.user {
                    static classKey()
                    {
                        return 'null';
                    }
                }
                class NullEndpoint extends this.abstracts.endpoint {
                    static classKey()
                    {
                        return 'null';
                    }
                }
                class NullLayout extends this.abstracts.layout {
                    static classKey()
                    {
                        return 'null';
                    }
                }
                class NullLayoutSection extends this.abstracts.layout_section {
                    static classKey()
                    {
                        return 'null';
                    }
                }
                class NullSkin extends this.abstracts.skin {
                    static classKey()
                    {
                        return 'null';
                    }
                }
                class NullEvent extends this.abstracts.event {
                    static classKey()
                    {
                        return 'null';
                    }
                }
                class NullModel extends this.abstracts.model {
                    static classKey()
                    {
                        return 'null';
                    }
                }
                class NullController extends this.abstracts.controller {
                    static classKey()
                    {
                        return 'null';
                    }
                }
                class NullView extends this.abstracts.view {
                    static classKey()
                    {
                        return 'null';
                    }
                }
                this.nulls.ajax = NullAjax;
                this.nulls.bootstrap = NullBootstrap;
                this.nulls.module = NullModule;
                this.nulls.component = NullComponent;
                this.nulls.page = NullPage;
                this.nulls.user = NullUser;
                this.nulls.endpoint = NullEndpoint;
                this.nulls.layout = NullLayout;
                this.nulls.layout_section = NullLayoutSection;
                this.nulls.skin = NullSkin;
                this.nulls.event = NullEvent;
                this.nulls.model = NullModel;
                this.nulls.controller = NullController;
                this.nulls.view = NullView;
            },
            build_defaults: function () {
                class Ajax extends this.abstracts.ajax {
                    static classKey()
                    {
                        return 'default';
                    }
                }
                class Bootstrap extends this.abstracts.bootstrap {
                    static classKey()
                    {
                        return 'default';
                    }
                }
                class Module extends this.abstracts.module {
                    static classKey()
                    {
                        return 'default';
                    }
                }
                class Component extends this.abstracts.component {
                    static classKey()
                    {
                        return 'default';
                    }
                }
                class Modal extends this.abstracts.component {
                    static classKey()
                    {
                        return 'modal';
                    }
                }
                class Banner extends this.abstracts.component {
                    static classKey()
                    {
                        return 'banner';
                    }
                }
                class Alert extends this.abstracts.component {
                    static classKey()
                    {
                        return 'alert';
                    }
                }
                class Page extends this.abstracts.page {
                    static classKey()
                    {
                        return 'default';
                    }
                }
                class User extends this.abstracts.user {
                    static classKey()
                    {
                        return 'default';
                    }
                }
                class Endpoint extends this.abstracts.endpoint {
                    static classKey()
                    {
                        return 'default';
                    }
                }
                class Layout extends this.abstracts.layout {
                    static classKey()
                    {
                        return 'default';
                    }
                }
                class LayoutSection extends this.abstracts.layout_section {
                    static classKey()
                    {
                        return 'default';
                    }

                    constructor(id, args, flags) {
                        super(id, args, flags);
                    }

                    initialize()
                    {
                        super.initialize();
                    }
                }
                class Skin extends this.abstracts.skin {
                    static classKey()
                    {
                        return 'default';
                    }
                }
                class Event extends this.abstracts.event {
                    static classKey()
                    {
                        return 'default';
                    }
                }
                class Model extends this.abstracts.model {
                    static classKey()
                    {
                        return 'default';
                    }
                }
                class Controller extends this.abstracts.controller {
                    static classKey()
                    {
                        return 'default';
                    }
                }
                class ErrorController extends this.abstracts.controller {
                    static classKey()
                    {
                        return 'error';
                    }
                }
                class View extends this.abstracts.view {
                    static classKey()
                    {
                        return 'default';
                    }
                }
                this.defaults.ajax = Ajax;
                this.defaults.bootstrap = Bootstrap;
                this.defaults.module = Module;
                this.defaults.component = Component;
                this.defaults.page = Page;
                this.defaults.user = User;
                this.defaults.endpoint = Endpoint;
                this.defaults.layout = Layout;
                this.defaults.layout_section = LayoutSection;
                this.defaults.skin = Skin;
                this.defaults.event = Event;
                this.defaults.model = Model;
                this.defaults.controller = Controller;
                this.defaults.error_controller = ErrorController;
                this.defaults.view = View;
            },
            build_internals: function () {
                class CurrentUser extends this.abstracts.user {
                    static classKey()
                    {
                        return 'current-user';
                    }

//                    constructor(id, args, flags) {
                    constructor(id, username, organization, details, role, enabled, logged_in, verified) {
//                        super(id, args, flags);
                        super(id, username, organization, details, role, enabled, logged_in, verified);
                    }

                    initialize() {
                        super.initialize();
                    }
                }
                class Router extends this.abstracts.router {
                    static classKey()
                    {
                        return 'router';
                    }

                    constructor(id, args, flags) {
                        super(id, args, flags);
                    }

                    initialize()
                    {
                        super.initialize();
                    }
                }
                class FrontController extends this.abstracts.controller {
                    static classKey()
                    {
                        return 'front-controller';
                    }

                    constructor(id, args, flags) {
                        super(id, args, flags);
                        this.page = null;
                        this.document = doc;
                        this.window = win;
                        this.current_route = null;
                        this.layout = null;
                        this.skin = null;
                        this.pagebody = this.document.body;
                        this.router = new Router('primary', lib.page.registered);
                        this.initialize();
                    }

                    initialize()
                    {
                        super.initialize();
                        const subject = this;
                        this.updatePageBody();
                        let monitor_class = this.window.MutationObserver
                                || win.WebKitMutationObserver
                                || win.MozMutationObserver;

                        this.pagemonitor = new monitor_class(this.library.debounce(function (mutation) {
                            if (((subject.pagebody.id === '' || subject.pagebody.id === 'default') && subject.page_id !== 'default') || (subject.pagebody.id !== '' && subject.pagebody.id !== 'default' && subject.pagebody.id !== subject.page_id))
                            {
                                subject.route();
                            }
                        }, 250));
                    }

                    route()
                    {
                        this.updatePageBody();
                        let route_class, route, route_args = {
                            layout: this.layout
                        }, route_flags = {

                        };
                        if (!this.page_id === 'default')
                        {
                            console.warn('No known route exists for [' + this.page_id + ']. The default route definition will be used.');
                        }
                        route_class = this.router.getRoute(this.page_id);
                        route = oroboros.create('page', route_class.classKey(), route_class.classKey(), {element: this.pagebody});
                        if (this.current_route !== null)
                        {
                            this.current_route.detach();
                        }
                        this.current_route = route;
                        this.current_route.attach();
                        console.groupCollapsed('Page route updated');
                        console.info('The front controller');
                        console.dir(this);
                        console.info('the current route');
                        console.dir(this.current_route);
                        console.groupEnd();
                    }

                    onReady(subject) {
                        lib.components.materialize();
                        subject.router.setRoutes(lib.page.registered);
                        subject.pagemonitor.observe(subject.pagebody, {
                            attributes: true
                        });
                        subject.updatePageBody();
                        subject.updateLayout();
                        subject.updateSkin();
                        subject.route();
                    }

                    updateLayout()
                    {
                        let layout_key, updated = false
                                , layout_args = {

                                }, layout_flags = {

                        };
                        for (layout_key of this.page_class)
                        {
                            if (lib.layout.registered.hasOwnProperty(layout_key))
                            {
                                this.layout = oroboros.create('layout', layout_key, layout_key, layout_args, layout_flags);
                                updated = true;
                                break;
                            }
                        }
                        if (!updated)
                        {
                            // Use the default layout if no other layout is found.
                            console.warn('Could not find any specified layout, using the default layout.');
                            this.layout = oroboros.create('layout', 'default', 'default', layout_args, layout_flags);
                        }
                    }

                    updateSkin()
                    {
                        let skin_key, updated = false
                                , skin_args = {

                                }, skin_flags = {

                        };
                        for (skin_key of this.page_class)
                        {
                            if (lib.skin.registered.hasOwnProperty(skin_key))
                            {
                                this.skin = oroboros.create('skin', skin_key, skin_key, skin_args, skin_flags);
                                updated = true;
                                break;
                            }
                        }
                        if (!updated)
                        {
                            // Use the default skin if no other skin is found.
                            console.warn('Could not find any specified skin, using the default skin.');
                            this.skin = oroboros.create('skin', 'default', 'default', skin_args, skin_flags);
                        }

                    }

                    updatePageBody()
                    {
                        console.groupCollapsed('Evaluating page class');
                        console.info('this');
                        console.dir(this);
                        console.info('Page body id');
                        console.dir(this.pagebody.id);
                        console.info('Router assignment');
                        console.dir(this.router.hasRoute(this.pagebody.id));
                        console.info('Router');
                        console.dir(this.router);
                        console.groupEnd();
                        this.page_id = (this.pagebody.id !== '' ? this.pagebody.id : 'default');
                        if (!this.router.hasRoute(this.page_id))
                        {
                            // Do not set the page id to a non-existent page registration
//                            console.warn('No known route exists for [' + this.page_id + ']. The default route definition will be used.');
                            this.page_id = 'default';
                        }
                        this.page_class = this.pagebody.classList;
                        this.page_attributes = this.pagebody.attributes;
                    }
                }
                this.internals.current_user = CurrentUser;
                this.internals.front_controller = FrontController;
            },
            bootstrap: function () {
                this.register_defaults();
            },
            register_defaults: function () {
                let default_class;
                for (let key in this.defaults)
                {
                    default_class = this.defaults[key];
                    try {
                        oroboros.register(key, default_class.classKey(), default_class);
                    } catch (e) {
//                        console.warn(e)
                        continue;
                    }
                }
            }
        },
        /**
         * Handles the dissemination of data passed from the backend,
         * and insures it is only provided to correctly registered
         * corresponding elements.
         */
        data: {
            data: null,
            is_initialized: false,
            storage: null,
            init: function (data, lib) {
                const valid = lib.core.map;
                let target, instance;
                if (!this.is_initialized)
                {
                    this.data = data;
                    this.storage = win.localStorage;
                    // Populate the internal library with the data supplied by the page
                    for (let key in this.data)
                    {
                        if (this.data.hasOwnProperty(key) && valid.hasOwnProperty(key))
                        {
                            target = lib[valid[key]];
                            target.data = data[key];
                        }
                    }
                    this.is_initialized = true;
                }
            }
        },
        /**
         * Handles functionality related to dns resolution
         */
        dns: {
            is_initialized: false,
            preload: {},
            prefetch: {},
            preconnect: {},
            init: function () {
                if (!this.is_initialized)
                {
                    this.collectPreconnect();
                    this.collectPreload();
                    this.collectPrefetch();
//                    console.groupCollapsed('Dns settings detected');
//                    console.dir(this);
//                    console.groupEnd();
                    this.is_initialized = true;
                }
            },
            collectPreconnect: function () {
                let links = doc.getElementsByTagName('link');
                let preconnect = {};
//                console.dir(links);
                for (let i of links) {
                    if (!(i.hasAttribute('rel') && i.getAttribute('rel') === 'preconnect'))
                    {
                        continue;
                    }
                    preconnect[i.getAttribute('href')] = i;
                }
                this.preconnect = preconnect;
            },
            collectPreload: function () {
                let links = doc.getElementsByTagName('link');
                let preload = {};
                for (let i of links) {
                    if (!(i.hasAttribute('rel') && i.getAttribute('rel') === 'preload'))
                    {
                        continue;
                    }
                    preload[i.getAttribute('href')] = i;
                }
                this.preload = preload;
            },
            collectPrefetch: function () {
                let links = doc.getElementsByTagName('link');
                let prefetch = {};
                for (let i of links) {
                    if (!(i.hasAttribute('rel') && i.getAttribute('rel') === 'prefetch'))
                    {
                        continue;
                    }
                    prefetch[i.getAttribute('href')] = i;
                }
                this.prefetch = prefetch;
            },
        },
        /**
         * Handles functionality related to dom manipulation
         */
        dom: {
            is_initialized: false,
            init: function () {
                if (!this.is_initialized)
                {
                    this.is_initialized = true;
                }
            }
        },
        /**
         * Tracks stylesheets added to the dom
         */
        stylesheets: {
            is_initialized: false,
            stylesheets: {},
            inline: [],
            init: function () {
                if (!this.is_initialized)
                {
                    this.collectStylesheets();
                    this.is_initialized = true;
                }
            },
            collectStylesheets: function () {
                let links = doc.getElementsByTagName('link');
                let styles = doc.getElementsByTagName('style');
                for (let i of links)
                {
                    if (!(i.hasAttribute('type') && i.getAttribute('type') === 'text/css'))
                    {
                        continue;
                    }
                    this.stylesheets[i.getAttribute('href')] = i;
                }
                for (let i of styles)
                {
                    this.inline.push(i);
                }
            }
        },
        /**
         * Tracks scripts added to the dom
         */
        scripts: {
            is_initialized: false,
            scripts: {},
            inline: [],
            init: function () {
                if (!this.is_initialized)
                {
                    this.collectScripts();
                    this.is_initialized = true;
                }
            },
            collectScripts: function () {
                let scripts = doc.getElementsByTagName('script');
                for (let i of scripts)
                {
                    if (!(i.hasAttribute('type') && ['application/javascript', 'text/javascript'].includes(i.getAttribute('type'))))
                    {
                        // Not a javascript tag
                        continue;
                    }
                    if (i.hasAttribute('src'))
                    {
                        // Remote
                        this.scripts[i.getAttribute('src')] = i;
                    } else {
                        // Inline
                        this.inline.push(i);
                    }
                }
            }
        },
        /**
         * Bootstraps the page mechanics
         */
        bootstrap: {
            data: null,
            is_initialized: false,
            init: function () {
                if (!this.is_initialized)
                {
                    if (this.data === null)
                    {
                        this.data = {};
                    }
                    this.is_initialized = true;
                }
            }
        },
        /**
         * Handles generation and distribution of event bindings,
         * stores registered callbacks, and provides internals for
         * triggering events as well as registering and unregistering them.
         */
        events: {
            initialized: false,
            data: null,
            viewport: null,
            dom: null,
            /**
             * Event binding keys. These are used with `on`, `this.bind`, and `this.release` methods.
             * 
             * Sancity event objects are structured in such a way that the official event handles and
             * the shorthand notation presented by these keys are interchangeable.
             * 
             * The categorys below reflect the documentation available on MDN.
             * Only standard, non-deprecated events are included.
             * Additional events can be added with a module if desired.
             * 
             * @link https://developer.mozilla.org/en-US/docs/Web/Events
             */
            keys: {
                "resource": {
                    "cached": "cached",
                    "error": "error",
                    "abort": "abort",
                    "beforeunload": "beforeunload",
                    "unload": "unload"
                },
                "network": {
                    "online": "online",
                    "offline": "offline"
                },
                "focus": {
                    "focus": "focus",
                    "blur": "blur"
                },
                "websocket": {
                    "open": "open",
                    "message": "message",
                    "error": "error",
                    "close": "close"
                },
                "session": {
                    "show": "pageshow",
                    "hide": "pagehide",
                    "pop": "popstate"
                },
                "animation": {
                    "start": "animationstart",
                    "end": "animationend",
                    "each": "animationiteration"
                },
                "transition": {
                    "start": "transitionstart",
                    "end": "transitionend",
                    "run": "transitionrun",
                    "cancel": "transitioncancel"
                },
                "form": {
                    "reset": "reset",
                    "submit": "submit"
                },
                "print": {
                    "before": "beforeprint",
                    "after": "afterprint"
                },
                "text": {
                    "start": "compositionstart",
                    "update": "compositionupdate",
                    "end": "compositionend"
                },
                "viewport": {
                    "fullscreen": "fullscreenchange",
                    "error": "fullscreenerror",
                    "resize": "resize",
                    "scroll": "scroll"
                },
                "clipboard": {
                    "cut": "cut",
                    "copy": "copy",
                    "paste": "paste"
                },
                "keyboard": {
                    "keyup": "keyup",
                    "keydown": "keydown",
                    "keypress": "keypress"
                },
                "mouse": {
                    "enter": "mouseenter",
                    "over": "mouseover",
                    "move": "mousemove",
                    "down": "mousedown",
                    "up": "mouseup",
                    "aux": "auxclick",
                    "click": "click",
                    "doubleclick": "dblclick",
                    "menu": "contextmenu",
                    "wheel": "wheel",
                    "leave": "mouseleave",
                    "out": "mouseout",
                    "select": "select",
                    "error": "pointerlockerror"
                },
                "drag": {
                    "start": "dragstart",
                    "drag": "drag",
                    "end": "dragend",
                    "enter": "dragenter",
                    "over": "dragover",
                    "leave": "dragleave",
                    "drop": "drop"
                },
                "media": {
                    "duration": "durationchange",
                    "metadata": "loadedmetadata",
                    "data": "loadeddata",
                    "canplay": "canplay",
                    "playthrough": "canplaythrough",
                    "end": "ended",
                    "empty": "emptied",
                    "stall": "stalled",
                    "suspend": "suspend",
                    "play": "play",
                    "playing": "playing",
                    "pause": "pause",
                    "wait": "waiting",
                    "seek": "seeking",
                    "seeked": "seeked",
                    "rate": "ratechange",
                    "time": "timeupdate",
                    "complete": "complete",
                    "audioprocess": "audioprocess"
                },
                "progress": {
                    "begin": "loadstart",
                    "progress": "progress",
                    "error": "error",
                    "timeout": "timeout",
                    "abort": "abort",
                    "load": "load",
                    "end": "loadend"
                }
            },
            viewport: win,
            dom: doc,
            is_initialized: false,
            elements: {},
            list: {
                ready: [],
                load: []
            },
            fired: {
                ready: false,
                load: false
            },
            installed: {
                ready: false,
                load: false
            },
            init: function () {
                let readyFunc = this.onReady;
                let loadFunc = this.onLoad;
                let abstract;
                let ieLoadFunc = function () {
                    if (document.readyState === "complete") {
                        readyFunc();
                    }
                };
                if (this.data === null)
                {
                    this.data = {};
                }
                if (!this.initialized)
                {
                    this.dom = doc;
                    this.viewport = win;
                    if (doc.addEventListener)
                    {
                        doc.addEventListener('DOMContentLoaded', readyFunc, false);
                        win.addEventListener('load', loadFunc, false);
                    } else {
                        doc.attachEvent('onreadystatechange', ieLoadFunc);
                        win.attachEvent('onload', loadFunc);
                    }
                    for (let key in this.events)
                    {
                        if (this.events.hasOwnProperty(key))
                        {
                            this.elements[key] = this.events[key](this.abstract);
                        }
                    }
                    this.is_initialized = true;
                    this.installed.ready = true;
                    this.installed.load = true;
                    this.initialized = true;
                }
                if (doc.readyState === "complete") {
                    setTimeout(readyFunc, 1);
                }

            },
            ready: function (callback, ctx) {
                let fired = lib.events.fired.ready;
                let list = lib.events.list.ready;
                if (typeof callback !== "function") {
                    throw new TypeError("callback for ready(fn) must be a function");
                }
                if (fired) {
                    setTimeout(function () {
                        callback(ctx);
                    }, 1);
                    return;
                } else {
                    list.push({fn: callback, context: ctx});
                }

            },
            load: function (callback, ctx) {
                let fired = lib.events.fired.load;
                let list = lib.events.list.load;
                if (typeof callback !== "function") {
                    throw new TypeError("callback for load(fn) must be a function");
                }
                if (fired) {
                    setTimeout(function () {
                        callback(ctx);
                    }, 1);
                    return;
                } else {
                    list.push({fn: callback, context: ctx});
                }

            },
            onReady: function ()
            {
                lib.events.fired.ready = true;
                for (var i = 0; i < lib.events.list.ready.length; i++) {
                    lib.events.list.ready[i].fn.call(win, lib.events.list.ready[i].context);
                    delete lib.events.list.ready[i];
                }
            },
            onLoad: function ()
            {
                lib.events.fired.load = true;
                for (var i = 0; i < lib.events.list.load.length; i++) {
                    lib.events.list.load[i].fn.call(win, lib.events.list.load[i].context);
                    delete lib.events.list.load[i];
                }
            },
            /**
             * Event Handler Abstract
             * 
             * This provides the abstract basis for all other event handler objects.
             * 
             * Event handlers allow multiple bindings to be tracked
             * for a single selector, and allow the entire group to be put
             * to sleep and woken up without releasing them. This allows
             * for a great deal of flexibility in terms of memory overhead
             * management throughout runtime.
             * 
             * @return AbstractEvent
             */
            abstract: (function () {
                let cat = false;
                let bindings = {};
                let selectors = {};
                let options = {
                    sleep: false
                };
                let library = {
                    initialize: function ()
                    {
                        if (this.category === false)
                        {
                            throw new Error('Attempted to instantiate an unextended abstract in [' + this.constructor.name + ']. Concrete extensions must supply a category of valid event types.');
                        }
                        if (typeof this.category !== "string" || !lib.events.keys.hasOwnProperty(this.category))
                        {
                            throw new Error('Declared event category [' + this.category + '] is not valid.');
                        }
                        this.valid = lib.events.keys[this.category];
                        for (let key of Object.keys(this.valid))
                        {
                            this.bindings[key] = {};
                            this.selectors[key] = {};
                        }
                    },
                    /**
                     * Binds a selector event for intersection observation.
                     * 
                     * @param string handle An identifier for the callback
                     * @param Function callback A function to fire on a click event
                     * @param Element selector A dom selector to observe for click events
                     * @returns undefined
                     */
                    bind: function (handle, type, callback, selector)
                    {
                        let self = this;
                        library.verifyHandle(handle);
                        library.verifyCallback(callback);
                        library.verifySelector(selector, this.validSelector);
                        type = library.verifyType(type, this.valid);
                        this.bindings[type][handle] = function (event) {
                            if (!self.options.sleep)
                            {
                                callback(event);
                            }
                        };
                        this.selectors[type][handle] = selector;
                        this.selectors[type][handle].addEventListener(this.valid[type], this.bindings[type][handle]);
                    },
                    /**
                     * Releases a click event binding. This will retain any other bindings that are currently in place.
                     */
                    release: function (handle, type)
                    {
                        library.verifyHandle(handle);
                        type = library.verifyType(type, this.valid);
                        if (typeof this.bindings[type][handle] !== "undefined" && this.bindings[type][handle] !== null)
                        {
                            this.selectors[type][handle].removeEventListener(this.valid[type], this.bindings[type][handle]);
                            delete this.selectors[type][handle];
                            delete this.bindings[type][handle];
                        }
                    },
                    /**
                     * Temporarily disables all click bindings.
                     * 
                     * They are retained for re-enabling without the overhead of re-linking them.
                     */
                    sleep: function ()
                    {
                        this.options.sleep = true;
                    },
                    /**
                     * Re-enables existing click bindings when in a sleep state.
                     */
                    wakeup: function ()
                    {
                        this.options.sleep = false;
                    },
                    /**
                     * Remove all existing bindings so the event handler can be garbage collected.
                     * 
                     * This method should be called prior to using `delete` on a event handler,
                     * so it does not have extraneous references pointing to it by event bindings
                     * that will be exceptionally difficult to track down and remove otherwise.
                     * 
                     * Event Handlers work by encapsulating an instance of themselves
                     * in an event binding, and then deferring to the registered method
                     * if the object is not asleep. This means that destruction requires
                     * unregistering all event bindings, which is preferable behavior
                     * for the destructor anyhow.
                     */
                    destroy: function ()
                    {
                        for (let type in this.bindings)
                        {
                            if (this.bindings.hasOwnProperty(type))
                            {
                                for (let handle in this.bindings[type])
                                {
                                    if (this.bindings[type].hasOwnProperty(handle))
                                    {
                                        // Release all existing bindings so there are no 
                                        // closure encapsulated references to this object.
                                        this.release(handle, type);
                                    }
                                }
                            }
                        }
                    },
                    /**
                     * Verifies the type, and standardizes it to the expected shorthand key.
                     * 
                     * If the type is out of scope or not known, it will throw a ReferenceError instead.
                     */
                    verifyType: function (type, valid)
                    {
                        let keys = Object.keys(valid);
                        for (let key of keys)
                        {
                            if (type === key || (valid.hasOwnProperty(key) && type === valid[key]))
                            {
                                return key;
                            }
                        }
                        throw new ReferenceError('Provided type [' + type + '] is not valid.');
                    },
                    /**
                     * Verifies that a callback can be used for an event binding correctly.
                     */
                    verifyCallback: function (callback)
                    {
                        if (typeof callback !== 'function')
                        {
                            throw new TypeError('Invalid callback. Expected [function], but received [' + lib.library.toType(callback) + ']');
                        }
                    },
                    /**
                     * Verifies that the provided selector is the expected valid type.
                     * 
                     * Generally this is an instance of Element, but individual overrides
                     * can provide a different selector instance in their prototype if need be,
                     * which makes this flexible enough to account for numerous binding types.
                     */
                    verifySelector: function (selector, valid)
                    {
                        if (typeof selector !== "object" || !(selector instanceof valid))
                        {
                            throw new TypeError('Invalid selector passed. Expected [' + valid.name + '].');
                        }
                    },
                    /**
                     * Verifies that the handle for a binding is a string.
                     * These are used as object keys, so they have to be strings.
                     */
                    verifyHandle: function (handle)
                    {
                        if (typeof handle !== 'string')
                        {
                            throw new TypeError('Invalid handle. Expected [string], but received [' + lib.library.toType(handle) + ']');
                        }
                    }
                };
                function AbstractEvent()
                {
                    this.bindings = this.bindings || bindings;
                    this.selectors = this.selectors || selectors;
                    this.options = this.options || options;
                    return this;
                }
                AbstractEvent.prototype.validSelector = Element;
                AbstractEvent.prototype.category = cat;
                AbstractEvent.prototype.bind = library.bind;
                AbstractEvent.prototype.release = library.release;
                AbstractEvent.prototype.sleep = library.sleep;
                AbstractEvent.prototype.wakeup = library.wakeup;
                AbstractEvent.prototype.destroy = library.destroy;
                AbstractEvent.prototype.initialize = library.initialize;
                AbstractEvent.prototype.constructor = AbstractEvent;
                return AbstractEvent;
            })(),
            /**
             * Provides individual concrete event binding handlers.
             * These each handle a disinct subset of clientside event
             * interactions.
             */
            events: {
                /**
                 * Produces a function that handles resource event bindings.
                 * 
                 * @param AbstractEvent abstract
                 * @returns ResourceEvent
                 */
                resource: function (abstract)
                {
                    let cat = 'resource';
                    let library = {};
                    let proto = abstract;
                    function ResourceEvent()
                    {
                        if (!(this instanceof ResourceEvent))
                        {
                            return new ResourceEvent();
                        }
                        proto.call(this);
                        this.category = cat;
                        this.initialize();
                        return this;
                    }
                    ResourceEvent.prototype = proto.prototype;
                    ResourceEvent.prototype.constructor = ResourceEvent;
                    return ResourceEvent;
                },
                /**
                 * Produces a function that handles network event bindings.
                 * 
                 * @param AbstractEvent abstract
                 * @returns NetworkEvent
                 */
                network: function (abstract)
                {
                    let cat = 'network';
                    let library = {};
                    let proto = abstract;
                    function NetworkEvent()
                    {
                        if (!(this instanceof NetworkEvent))
                        {
                            return new NetworkEvent();
                        }
                        proto.call(this);
                        this.category = cat;
                        this.initialize();
                        return this;
                    }
                    NetworkEvent.prototype = proto.prototype;
                    NetworkEvent.prototype.constructor = NetworkEvent;
                    return NetworkEvent;
                },
                /**
                 * Produces a function that handles focus event bindings.
                 * 
                 * @param AbstractEvent abstract
                 * @returns FocusEvent
                 */
                focus: function (abstract)
                {
                    let cat = 'focus';
                    let library = {};
                    let proto = abstract;
                    function FocusEvent()
                    {
                        if (!(this instanceof FocusEvent))
                        {
                            return new FocusEvent();
                        }
                        proto.call(this);
                        this.category = cat;
                        this.initialize();
                        return this;
                    }
                    FocusEvent.prototype = proto.prototype;
                    FocusEvent.prototype.constructor = FocusEvent;
                    return FocusEvent;
                },
                /**
                 * Produces a function that handles websocket event bindings.
                 * 
                 * @param AbstractEvent abstract
                 * @returns WebSocketEvent
                 */
                websocket: function (abstract)
                {
                    let cat = 'websocket';
                    let library = {};
                    let proto = abstract;
                    function WebSocketEvent()
                    {
                        if (!(this instanceof WebSocketEvent))
                        {
                            return new WebSocketEvent();
                        }
                        proto.call(this);
                        this.category = cat;
                        this.initialize();
                        return this;
                    }
                    WebSocketEvent.prototype = proto.prototype;
                    WebSocketEvent.prototype.constructor = WebSocketEvent;
                    return WebSocketEvent;
                },
                /**
                 * Produces a function that handles session event bindings.
                 * 
                 * @param AbstractEvent abstract
                 * @returns SessionEvent
                 */
                session: function (abstract)
                {
                    let cat = 'session';
                    let library = {};
                    let proto = abstract;
                    function SessionEvent()
                    {
                        if (!(this instanceof SessionEvent))
                        {
                            return new SessionEvent();
                        }
                        proto.call(this);
                        this.category = cat;
                        this.initialize();
                        return this;
                    }
                    SessionEvent.prototype = proto.prototype;
                    SessionEvent.prototype.constructor = SessionEvent;
                    return SessionEvent;
                },
                /**
                 * Produces a function that handles css animation event bindings.
                 * 
                 * @param AbstractEvent abstract
                 * @returns AnimationEvent
                 */
                animation: function (abstract)
                {
                    let cat = 'animation';
                    let library = {};
                    let proto = abstract;
                    function AnimationEvent()
                    {
                        if (!(this instanceof AnimationEvent))
                        {
                            return new AnimationEvent();
                        }
                        proto.call(this);
                        this.category = cat;
                        this.initialize();
                        return this;
                    }
                    AnimationEvent.prototype = proto.prototype;
                    AnimationEvent.prototype.constructor = AnimationEvent;
                    return AnimationEvent;
                },
                /**
                 * Produces a function that handles css transition event bindings.
                 * 
                 * @param AbstractEvent abstract
                 * @returns TransitionEvent
                 */
                transition: function (abstract)
                {
                    let cat = 'transition';
                    let library = {};
                    let proto = abstract;
                    function TransitionEvent()
                    {
                        if (!(this instanceof TransitionEvent))
                        {
                            return new TransitionEvent();
                        }
                        proto.call(this);
                        this.category = cat;
                        this.initialize();
                        return this;
                    }
                    TransitionEvent.prototype = proto.prototype;
                    TransitionEvent.prototype.constructor = TransitionEvent;
                    return TransitionEvent;
                },
                /**
                 * Produces a function that handles form event bindings.
                 * 
                 * @param AbstractEvent abstract
                 * @returns FormEvent
                 */
                form: function (abstract)
                {
                    let cat = 'form';
                    let library = {};
                    let proto = abstract;
                    function FormEvent()
                    {
                        if (!(this instanceof FormEvent))
                        {
                            return new FormEvent();
                        }
                        proto.call(this);
                        this.category = cat;
                        this.initialize();
                        return this;
                    }
                    FormEvent.prototype = proto.prototype;
                    FormEvent.prototype.constructor = FormEvent;
                    return FormEvent;
                },
                /**
                 * Produces a function that handles print event bindings.
                 * 
                 * @param AbstractEvent abstract
                 * @returns PrintEvent
                 */
                print: function (abstract)
                {
                    let cat = 'print';
                    let library = {};
                    let proto = abstract;
                    function PrintEvent()
                    {
                        if (!(this instanceof PrintEvent))
                        {
                            return new PrintEvent();
                        }
                        proto.call(this);
                        this.category = cat;
                        this.initialize();
                        return this;
                    }
                    PrintEvent.prototype = proto.prototype;
                    PrintEvent.prototype.constructor = PrintEvent;
                    return PrintEvent;
                },
                /**
                 * Produces a function that handles text composition event bindings.
                 * 
                 * @param AbstractEvent abstract
                 * @returns TextEvent
                 */
                text: function (abstract)
                {
                    let cat = 'text';
                    let library = {};
                    let proto = abstract;
                    function TextEvent()
                    {
                        if (!(this instanceof TextEvent))
                        {
                            return new TextEvent();
                        }
                        proto.call(this);
                        this.category = cat;
                        this.initialize();
                        return this;
                    }
                    TextEvent.prototype = proto.prototype;
                    TextEvent.prototype.constructor = TextEvent;
                    return TextEvent;
                },
                /**
                 * Produces a function that handles viewport resize event bindings.
                 * 
                 * @param AbstractEvent abstract
                 * @returns ViewportEvent
                 */
                view: function (abstract)
                {
                    let cat = 'viewport';
                    let library = {};
                    let proto = abstract;
                    function ViewportEvent()
                    {
                        if (!(this instanceof ViewportEvent))
                        {
                            return new ViewportEvent();
                        }
                        proto.call(this);
                        this.category = cat;
                        this.initialize();
                        return this;
                    }
                    ViewportEvent.prototype = proto.prototype;
                    ViewportEvent.prototype.constructor = ViewportEvent;
                    return ViewportEvent;
                },
                /**
                 * Produces a function that handles clipboard event bindings.
                 * 
                 * @param AbstractEvent abstract
                 * @returns ClipboardEvent
                 */
                clipboard: function (abstract)
                {
                    let cat = 'clipboard';
                    let library = {};
                    let proto = abstract;
                    function ClipboardEvent()
                    {
                        if (!(this instanceof ClipboardEvent))
                        {
                            return new ClipboardEvent();
                        }
                        proto.call(this);
                        this.category = cat;
                        this.initialize();
                        return this;
                    }
                    ClipboardEvent.prototype = proto.prototype;
                    ClipboardEvent.prototype.constructor = ClipboardEvent;
                    return ClipboardEvent;
                },
                /**
                 * Produces a function that handles keyboard event bindings.
                 * 
                 * @param AbstractEvent abstract
                 * @returns KeyboardEvent
                 */
                keyboard: function (abstract)
                {
                    let cat = 'keyboard';
                    let library = {};
                    let proto = abstract;
                    function KeyboardEvent()
                    {
                        if (!(this instanceof KeyboardEvent))
                        {
                            return new KeyboardEvent();
                        }
                        proto.call(this);
                        this.category = cat;
                        this.initialize();
                        return this;
                    }
                    KeyboardEvent.prototype = proto.prototype;
                    KeyboardEvent.prototype.constructor = KeyboardEvent;
                    return KeyboardEvent;
                },
                /**
                 * Produces a function that handles mouse event bindings.
                 * 
                 * @param AbstractEvent abstract
                 * @returns MouseEvent
                 */
                mouse: function (abstract)
                {
                    let cat = 'mouse';
                    let library = {};
                    let proto = abstract;
                    function MouseEvent()
                    {
                        if (!(this instanceof MouseEvent))
                        {
                            return new MouseEvent();
                        }
                        proto.call(this);
                        this.category = cat;
                        this.initialize();
                        return this;
                    }
                    MouseEvent.prototype = proto.prototype;
                    MouseEvent.prototype.constructor = MouseEvent;
                    return MouseEvent;
                },
                /**
                 * Produces a function that handles drag and drop event bindings.
                 * 
                 * @param AbstractEvent abstract
                 * @returns DragEvent
                 */
                drag: function (abstract)
                {
                    let cat = 'drag';
                    let library = {};
                    let proto = abstract;
                    function DragEvent()
                    {
                        if (!(this instanceof DragEvent))
                        {
                            return new DragEvent();
                        }
                        proto.call(this);
                        this.category = cat;
                        this.initialize();
                        return this;
                    }
                    DragEvent.prototype = proto.prototype;
                    DragEvent.prototype.constructor = DragEvent;
                    return DragEvent;
                },
                /**
                 * Produces a function that handles media event bindings.
                 * 
                 * @param AbstractEvent abstract
                 * @returns MediaEvent
                 */
                media: function (abstract)
                {
                    let cat = 'media';
                    let library = {};
                    let proto = abstract;
                    function MediaEvent()
                    {
                        if (!(this instanceof MediaEvent))
                        {
                            return new MediaEvent();
                        }
                        proto.call(this);
                        this.category = cat;
                        this.initialize();
                        return this;
                    }
                    MediaEvent.prototype = proto.prototype;
                    MediaEvent.prototype.constructor = MediaEvent;
                    return MediaEvent;
                },
                /**
                 * Produces a function that handles progress and loading event bindings.
                 * 
                 * @param AbstractEvent abstract
                 * @returns ProgressEvent
                 */
                progress: function (abstract)
                {
                    let cat = 'progress';
                    let library = {};
                    let proto = abstract;
                    function ProgressEvent()
                    {
                        if (!(this instanceof ProgressEvent))
                        {
                            return new ProgressEvent();
                        }
                        proto.call(this);
                        this.category = cat;
                        this.initialize();
                        return this;
                    }
                    ProgressEvent.prototype = proto.prototype;
                    ProgressEvent.prototype.constructor = ProgressEvent;
                    return ProgressEvent;
                }
            }
        },
        /**
         * Handles the master debug protocol, as determined by the backend,
         * which is in turn determined by the underlying webserver settings
         * on the origin server.
         */
        debug: {
            elements: {},
            enabled: false,
            data: null,
            is_initialized: false,
            init: function ()
            {
                // Do not duplicate initialization
                if (!this.is_initialized)
                {
                    if (this.data === null)
                    {
                        this.data = {};
                    }
                    if (this.data.enabled !== undefined && this.data.enabled)
                    {
                        this.enabled = true;
                    }
                    this.is_initialized = true;
                }
            }
        },
        /**
         * Represents information about the page document, its dimensions, and its properties
         */
        document: {
            document: null,
            html: null,
            head: null,
            body: null,
            height: 0,
            width: 0,
            type: null,
            init: function (document)
            {
                const ref = this;
                const changeFunc = function () {
                    ref.height = Math.max(ref.body.scrollHeight, ref.body.offsetHeight, ref.html.clientHeight, ref.html.scrollHeight, ref.html.offsetHeight);
                    ref.width = Math.max(ref.body.scrollWidth, ref.body.offsetWidth, ref.html.clientWidth, ref.html.scrollWidth, ref.html.offsetWidth);
//                    console.info('Document dimensions updated:' + ref.height + ' x ' + ref.width);
                };
                if (!document instanceof HTMLDocument)
                {
                    throw new TypeError('Provided document object must be an instance of [HTMLDocument]');
                }
                this.document = document;
                this.html = this.document.documentElement;
                this.head = this.document.head;
                this.body = this.document.body;
                this.type = this.document.doctype;
                this.document.addEventListener('resize', lib.library.debounce(changeFunc, 25));
                changeFunc();
            },
            proxy: function () {
                const handler = {
                    get: function (obj, prop) {
                        if (!['html', 'head', 'body', 'height', 'width', 'type'].includes(prop))
                        {
                            return undefined;
                        }
                        return lib.document[prop];
                    },
                    set: function (obj, prop, value) {
                        throw new Error('Document properties are read only.');
                    },
                    has: function (obj, prop) {
                        if (!['html', 'head', 'body', 'height', 'width', 'type'].includes(prop))
                        {
                            return false;
                        }
                        return true;
                    },
                    ownKeys: function (prop) {
                        return ['html', 'head', 'body', 'height', 'width', 'type'];
                    },
                    deleteProperty: function (obj, prop) {
                        throw new Error('Document properties cannot be deleted.');
                    }
                };
                return new Proxy({}, handler);
            }
        },
        /**
         * Represents information about the viewport, its dimensions, and its properties
         */
        window: {
            window: null,
            height: 0,
            width: 0,
            type: null,
            init: function (window)
            {
                const ref = this;
                const changeFunc = function () {
                    ref.height = ref.window.innerHeight;
                    ref.width = ref.window.innerWidth;
//                    console.info('Window dimensions updated:' + ref.height + ' x ' + ref.width);
                };
                if (!window instanceof Window)
                {
                    throw new TypeError('Provided document object must be an instance of [Window]');
                }
                this.window = window;
                this.window.addEventListener('resize', lib.library.debounce(changeFunc, 25));
                changeFunc();
            },
            proxy: function () {
                const handler = {
                    get: function (obj, prop) {
                        if (!['height', 'width', 'type'].includes(prop))
                        {
                            return undefined;
                        }
                        return lib.window[prop];
                    },
                    set: function (obj, prop, value) {
                        throw new Error('Window properties are read only.');
                    },
                    has: function (obj, prop) {
                        if (!['height', 'width', 'type'].includes(prop))
                        {
                            return false;
                        }
                        return true;
                    },
                    ownKeys: function (prop) {
                        return ['height', 'width', 'type'];
                    },
                    deleteProperty: function (obj, prop) {
                        throw new Error('Window properties cannot be deleted.');
                    }
                };
                return new Proxy({}, handler);
            }
        },
        /**
         * Represents a controlled access point to cookies, with simplified get/set operations
         */
        cookie: {
            cookies: null,
            init: function (document) {
                this.cookies = document.cookie;
//                console.info('cookies');
//                console.dir(this.cookies);
            },
            proxy: function () {
                const handler = {

                };
                return new Proxy({}, handler);
            }
        },
        /**
         * Represents a controlled access point to headers, with simplified get/set operations
         */
        headers: {
            headers: null,
            forbidden: [
                'accept-charset',
                'accept-encoding',
                'access-control-request-headers',
                'access-control-request-method',
                'connection',
                'content-length',
                'cookie',
                'cookie2',
                'date',
                'dnt',
                'expect',
                'host',
                'keep-alive',
                'origin',
                'proxy-',
                'sec-',
                'referer',
                'te',
                'trailer',
                'transfer-encoding',
                'vpgrade',
                'via',
                'set-cookie',
                'set-cookie2'
            ],
            init: function () {
                const headers = {};
                this.headers = new Headers(headers);
//                console.info('headers');
//                console.dir(this.headers);
            },
            proxy: function () {
                const handler = {
                    get: function (obj, prop) {
                        return lib.headers.headers.get(prop);
                    },
                    set: function (obj, prop, value) {
                        if (lib.headers.forbidden.includes(prop.toLowerCase()))
                        {
                            throw new Error('Header [' + prop + '] cannot be modified by javascript.');
                        }
                        lib.headers.headers.set(prop, value);
                        return obj;
                    },
                    has: function (obj, prop) {
                        return lib.headers.headers.has(prop);
                    },
                    ownKeys: function (prop) {
                        return lib.headers.headers.entries().getOwnPropertyNames();
                    },
                    deleteProperty: function (obj, prop) {
                        if (lib.headers.forbidden.includes(prop.toLowerCase()))
                        {
                            throw new Error('Header [' + prop + '] cannot be modified by javascript.');
                        }
                        lib.headers.headers.delete(prop);
                        return obj;
                    }
                };
                return new Proxy({}, handler);
            },
            create: function (headers) {
                const handler = {
                    get: function (obj, prop) {
                        return instance.get(prop);
                    },
                    set: function (obj, prop, value) {
                        if (lib.headers.forbidden.includes(prop.toLowerCase()))
                        {
                            throw new Error('Header [' + prop + '] cannot be modified by javascript.');
                        }
                        instance.set(prop, value);
                        return obj;
                    },
                    has: function (obj, prop) {
                        return instance.has(prop);
                    },
                    ownKeys: function (prop) {
                        return instance.entries().getOwnPropertyNames();
                    },
                    deleteProperty: function (obj, prop) {
                        if (lib.headers.forbidden.includes(prop.toLowerCase()))
                        {
                            throw new Error('Header [' + prop + '] cannot be modified by javascript.');
                        }
                        instance.delete(prop);
                        return obj;
                    }
                };
                let instance, proxy;
                if (!(headers instanceof Object) && !(headers === undefined))
                {
                    throw new TypeError('Headers must be an object if supplied.');
                }
                instance = new Headers(headers);
                return new Proxy({}, instance);
            }
        },
        /**
         * Factorizes Proxy objects for objects generated, which are generally
         * returned in place of internals directly to retain abstraction
         * and visibility.
         */
        proxy: {
            elements: {},
            /**
             * Proxy Factory
             * 
             * @param type object
             * @returns undefined
             */
            initialize: function (object)
            {
                var callbacks = this.callbacks;
                var subject = object;
                return (function ()
                {
                    var ProxyObject = callbacks;
                    return ProxyObject;
                })();
            },
            register: function (key, object, callbacks)
            {
                if ((key in this.elements))
                {
                    // Do not double proxy objects.
                    return this.elements[key];
                }
                var methods = callbacks || this.initialize(object);
                this.elements[key] = new Proxy(object, methods);
                return this.elements[key];
            },
            callbacks: {
                construct: function (selector, args)
                {
                    this.selector = selector;
                    this.args = args;
                    return this;
                },
                getPrototypeOf: function (obj)
                {
                    return obj.prototype;
                },
                setPrototypeOf: function (obj, proto)
                {
                    obj.prototype = proto;
                    return true;
                },
                apply: function (obj, thisArg, args)
                {
                    return true;
                },
                get: function (obj, prop)
                {
                    return prop in obj ?
                            obj[prop] : false;
                },
                set: function (obj, key, value)
                {
                    obj[key] = value;
                    return true;
                },
                has: function (obj, key)
                {
                    return (key in obj) ?
                            true : false;
                },
                defineProperty: function (obj, key, value)
                {
                    Object.defineProperty(obj, key, value);
                    return obj;
                },
                getOwnPropertyDescriptor: function (obj, key)
                {
                    return Object.getOwnPropertyDescriptor(obj, key);
                },
                isExtensible: function (obj)
                {
                    return Reflect.isExtensible(obj);
                },
                preventExtensions: function (obj)
                {
                    obj.canEvolve = false;
                    Object.preventExtensions(obj);
                    return true;
                },
                ownKeys: function (obj)
                {
                    return Reflect.ownKeys(target);
                },
                deleteProperty: function (obj, key)
                {
                    if (key in obj)
                    {
                        delete obj[key];
                    }
                }
            },
        },
        /**
         * Handles generation and distribution of ajax callback handler objects,
         * promises, and related functionality.
         */
        ajax: {
            endpoints: {},
            request_methods: [
                "get",
                "post",
                "put",
                "delete",
                "options",
                "trace",
                "head",
                "patch",
                "connect",
            ],
            is_initialized: false,
            init: function (data)
            {
                if (!this.is_initialized)
                {
                    lib.common.ajax = this.envelope;
                    this.is_initialized = true;
                }
            },
            parseEndpoints: function (endpoints)
            {

            },
            getRequest: function (data)
            {
                return new this.envelope('get', data);
            },
            postRequest: function (data)
            {
                return new this.envelope('post', data);
            },
            putRequest: function (data)
            {
                return new this.envelope('put', data);
            },
            deleteRequest: function (data)
            {
                return new this.envelope('delete', data);
            },
            optionsRequest: function (data)
            {
                return new this.envelope('options', data);
            },
            traceRequest: function (data)
            {
                return new this.envelope('trace', data);
            },
            headRequest: function (data)
            {
                return new this.envelope('head', data);
            },
            patchRequest: function (data)
            {
                return new this.envelope('patch', data);
            },
            connectRequest: function (data)
            {
                return new this.envelope('connect', data);
            },
            /**
             * Ajax Envelope Factory
             */
            envelope: (function ()
            {
                let library = {
                    validateType: function (type)
                    {
                        lib.library.validate.type(type, "string");
                        for (let req of lib.ajax.request_methods)
                        {
                            if (type === req)
                            {
                                return true;
                            }
                        }
                        throw new ReferenceError('Invalid Ajax request method [' + type + '] supplied. Valid types are [' + lib.ajax.request_methods.toString() + '].');
                    },
                    validateOptions: function (options)
                    {
                        if (typeof options !== "object" && !(typeof options === "object" && (options instanceof Proxy)))
                        {
                            throw new TypeError('Invalid request options supplied. Expected [object|Proxy] but received [' + lib.library.toType(options) + '].');
                        }
                    },
                    getHeaders: function (type, options, endpoint)
                    {
                        let headers = {
                            'X-Requested-With': 'XMLHttpRequest',
                            'Client-Runtime-Fingerprint': fingerprint, // On same origin only
                            'DPR': lib.library.device.pixelRatio(), // On same origin only
                            'Viewport-Width': win.innerWidth         // On same origin only
                        };
                        let existing = {};
                        for (let i in Object.keys(headers))
                        {
                            existing[i.toLowerCase()] = i;
                        }
                        switch (type)
                        {
                            case "get":
                                break;
                            case "post":
                                break;
                            case "put":
                                break;
                            case "delete":
                                break;
                            case "options":
                                break;
                            case "trace":
                                break;
                            case "head":
                                break;
                            case "patch":
                                break;
                            case "connect":
                                break;
                        }
                        if ((typeof options !== 'undefined') && (typeof options.headers === 'object'))
                        {
                            for (let key in options.headers)
                            {
                                if (existing.hasOwnProperty(key.toLowerCase()))
                                {
                                    headers[existing[key.toLowerCase()]] = options.headers[key];
                                } else {
                                    headers[key] = options.headers[key];
                                }
                                existing[key.toLowerCase()] = key;
                            }

                        }
                        if (!existing.hasOwnProperty('accept'))
                        {
                            headers['Accept'] = '*';
                        }
                        return headers;
                    },
                    // Placeholder callback. Does nothing.
                    nullCallback: function () {},
                    // Placeholder XHR filter. Just returns whatever it got.
                    nullXhrFilter: function (instance, xhr) {
                        return xhr;
                    },
                    // Placeholder response content filtering method.
                    // Just returns whatever it got.
                    nullResponseFilter: function (data, instance) {
                        return data;
                    },

                };
                function AjaxEnvelope(type, options)
                {
                    if (!(this instanceof AjaxEnvelope))
                    {
                        return new AjaxEnvelope(type, options);
                    }
                    let req = (typeof type === "function") ? type() : type;
                    let opt = (typeof options === "function") ? options() : (options || {});
                    let valid_opt = false;
                    let self = this;
                    library.validateType(req);
                    library.validateOptions(opt);
                    this.type = req;
                    this.options = opt;
                    this.endpoint = null;
                    this.headers = this.options.headers || {};
                    if (typeof this.options.data !== 'undefined')
                    {
                        lib.library.validate.type(this.options.data, 'string');
                    }
                    this.data = this.options.data || null;
                    this.transformFilter = this.options.filter || library.nullResponseFilter;
                    this.xhr = new XMLHttpRequest();
                    this.beforeCallback = this.options.before || library.nullXhrFilter;
                    this.pendingCallback = this.options.pending || library.nullCallback;
                    this.errorCallback = this.options.error || library.nullCallback;
                    this.startCallback = this.options.start || library.nullCallback;
                    this.doneCallback = this.options.done || library.nullCallback;
                    this.abortCallback = this.options.abort || library.nullCallback;
                    this.timeoutCallback = this.options.timeout || library.nullCallback;
                    this.alwaysCallback = this.options.always || library.nullCallback;
                    this.parseOptions();
                    return this;
                }
                AjaxEnvelope.prototype = {
                    before: function (callback)
                    {
                        lib.library.validate.type(callback, "function");
                        this.beforeCallback = callback;
                        return this;
                    },
                    onBefore: function (instance, event, xhr)
                    {
                        return instance.beforeCallback(instance, event, xhr);
                    },
                    pending: function (callback)
                    {
                        lib.library.validate.type(callback, "function");
                        this.pendingCallback = callback;
                        return this;
                    },
                    onPending: function (instance, event, xhr)
                    {
                        let completion = ((event.lengthComputable) ? event.loaded / event.total * 100 : NaN);
                        return instance.pendingCallback(instance, event, xhr, completion);
                    },
                    error: function (callback)
                    {
                        lib.library.validate.type(callback, "function");
                        this.errorCallback = callback;
                        return this;
                    },
                    onError: function (instance, event, xhr)
                    {
                        return instance.errorCallback(instance, event, xhr);
                    },
                    done: function (callback)
                    {
                        lib.library.validate.type(callback, "function");
                        this.doneCallback = callback;
                        return this;
                    },
                    onDone: function (instance, event, xhr)
                    {
                        return instance.doneCallback(instance, event, xhr);
                    },
                    start: function (callback)
                    {
                        lib.library.validate.type(callback, "function");
                        this.startCallback = callback;
                        return this;
                    },
                    onStart: function (instance, event, xhr)
                    {
                        return instance.startCallback(instance, event, xhr);
                    },
                    timeout: function (callback)
                    {
                        lib.library.validate.type(callback, "function");
                        this.timeoutCallback = callback;
                        return this;
                    },
                    onTimeout: function (instance, event, xhr)
                    {
                        return instance.timeoutCallback(instance, event, xhr);
                    },
                    always: function (callback)
                    {
                        lib.library.validate.type(callback, "function");
                        this.alwaysCallback = callback;
                        return this;
                    },
                    onAlways: function (instance, event, xhr)
                    {
                        return instance.alwaysCallback(instance, event, instance.xhr);
                    },
                    abort: function (callback)
                    {
                        lib.library.validate.type(callback, "function");
                        this.abortCallback = callback;
                        return this;
                    },
                    onAbort(instance, event, xhr)
                    {
                        return instance.abortCallback(instance, event, xhr);
                    },
                    send: function ()
                    {
                        let headers = library.getHeaders(this.type, this.options, this.endpoint);
                        let self = this;
                        this.xhr.open(this.type, this.endpoint);
                        for (let header in headers)
                        {
                            if (headers.hasOwnProperty(header))
                            {
                                this.xhr.setRequestHeader(header, headers[header]);
                            }
                        }
                        this.xhr.onloadstart = function (event) {
                            return self.onStart(self, event, self.xhr);
                        };
                        this.xhr.onabort = function (event) {
                            return self.onAbort(self, event, self.xhr);
                        };
                        this.xhr.onerror = function (event) {
                            return self.onError(self, event, self.xhr);
                        };
                        this.xhr.onload = function (event) {
                            return self.onDone(self, event, self.xhr);
                        };
                        this.xhr.onloadend = function (event) {
                            return self.onAlways(self, event, self.xhr);
                        };
                        this.xhr.onprogress = function (event) {
                            return self.onPending(self, event, self.xhr);
                        };
                        this.xhr.onabort = function (event) {
                            return self.onAbort(self, event, self.xhr);
                        };
                        this.xhr.ontimeout = function (event) {
                            return self.onTimeout(self, event, self.xhr);
                        };
                        this.xhr.send(this.data);
                        return this;
                    },
                    setEndpoint: function (endpoint)
                    {
                        let uri;
                        lib.library.validate.type(endpoint, "string");
                        try {
                            // Attempt to construct full uri
                            uri = new URL(endpoint);
                        } catch (error)
                        {
                            // Attempt to construct relative uri
                            endpoint = endpoint.replace(/^\//, "");
                            uri = new URL(lib.data.data.host + '/' + endpoint);
                        }
                        console.info('Endpoint');
                        this.endpoint = uri;
                        return this;
                    },
                    setHeaders: function (headers)
                    {
                        lib.library.validate.type(headers, "object");
                        this.headers = headers;
                        return this;
                    },
                    setData: function (data)
                    {
                        lib.library.validate.type(data, "string");
                        this.data = data;
                        return this;
                    },
                    filterResponseData: function (instance, data)
                    {
                        let filterType = typeof instance.transformFilter;
                        if (filterType === "array" || filterType === "object")
                        {
                            for (let filter of instance.transformFilter)
                            {
                                data = filter(data, instance);
                            }
                        } else if (typeof instance.transformFilter === "function")
                        {
                            data = instance.transformFilter(data, instance);
                        }
                        return data;
                    },
                    parseOptions: function ()
                    {
                        for (let key in this.options)
                        {
                            if (this.options.hasOwnProperty(key))
                            {
                                switch (key)
                                {
                                    case 'url':
                                        this.setEndpoint(this.options[key]);
                                        break;
                                    case 'data':
                                        this.setData(this.options[key]);
                                        break;
//                                    case 'dataType':
//                                        this.setDataType(this.options[key]);
//                                        break;
                                    case 'headers':
                                        this.setHeaders(this.options[key]);
                                        break;
                                    case 'before':
                                        this.before(this.options[key]);
                                        break;
                                    case 'pending':
                                        this.pending(this.options[key]);
                                        break;
                                    case 'error':
                                        this.error(this.options[key]);
                                        break;
                                    case 'start':
                                        this.start(this.options[key]);
                                        break;
                                    case 'done':
                                        this.done(this.options[key]);
                                        break;
                                    case 'timeout':
                                        this.timeout(this.options[key]);
                                        break;
                                    case 'always':
                                        this.always(this.options[key]);
                                        break;
                                    case 'abort':
                                        this.abort(this.options[key]);
                                        break;
//                                    case 'filter':
//                                        this.setFilter(this.options[key]);
//                                        break;
                                }
                            }
                        }
                        return this;
                    }
                };
                AjaxEnvelope.prototype.constructor = AjaxEnvelope;
                return AjaxEnvelope;
            })()
        },
        /**
         * Handles the unpackaging of modules, and injection of their
         * registered assets into the appropriate locations.
         */
        modules: {
            is_initialized: false,
            init: function () {
                if (!this.is_initialized)
                {
                    if (this.data === null)
                    {
                        this.data = {};
                    }
                    this.is_initialized = true;
                }
            }
        },
        /**
         * Handles component registration, provision,
         * and generation of null and default variants as applicable.
         */
        components: {
            is_initialized: false,
            components: null,
            component_nodes: null,
            context: null,
            init: function (components, context) {
                if (!this.is_initialized)
                {
                    this.components = {};
                    this.component_nodes = (components !== null && components !== undefined ? components : {});
                    this.context = (context !== null && context !== undefined ? context : {});
                    if (this.data === null)
                    {
                        this.data = {};
                    }
                    if (this.keys === null)
                    {
                        this.keys = {};
                        for (let key of context)
                        {
                            this.keys[key] = null;
                        }
                    }
                    if (this.registered === null)
                    {
                        this.registered = {};
                    }
                    this.is_initialized = true;
                }
            },
            materialize: function () {
//                console.groupCollapsed('Materializing known components');
//                for (let key in this.component_nodes)
//                {
//                    console.info('materializing component [' + key + '].');
//                    console.dir(this.component_nodes[key]);
//                    this.components[key] = this.generate(this.component_nodes[key]);
//                }
//                console.dir(this);
//                console.groupEnd();
            },
            extract: function (nodeList) {
                let component_elements = {}, subject;
                if (!(nodeList instanceof NodeList))
                {
                    throw new TypeError('Provided argument [nodeList] must be an instance of [NodeList].');
                }
//                console.groupCollapsed('Evaluating component extraction...');
                for (let key in nodeList)
                {
                    if (nodeList.hasOwnProperty(key) && (nodeList[key] instanceof Element))
                    {
//                        console.groupCollapsed('Results of subject [' + key + ']');
                        subject = this.generate(nodeList[key]);
                        component_elements[key] = subject;
//                        console.dir(subject);
//                        console.groupEnd();
                    }
                }
//                console.groupEnd();
                return component_elements;
            },
            generate: function (element)
            {
                let component, component_type = 'default', details, attributes, id;
                if (!(element instanceof Element))
                {
                    throw new TypeError('Provided argument [element] must be an instance of [Element]');
                }
                // Element attributes, if any
                attributes = element.attributes;
                // Element id, if any
                id = element.id || (attributes.getNamedItem('data-context') !== null ? attributes.getNamedItem('data-context').value : null);
                // The keyname of the component class to fetch to instantiate the usable object
                component_type = (attributes.getNamedItem('data-component') !== null ? attributes.getNamedItem('data-component').value : component_type);
                details = {
                    'id': id,
                    'element': element
                };
                if (this.components.hasOwnProperty(id))
                {
                    return this.components[id];
                }
                try {
                    component = oroboros.create('component', component_type, id, details);
                } catch (e) {
                    console.warn(e);
                    console.dir(component_type);
                    console.dir(id);
                    console.dir(details);
                    console.trace();
                    component = oroboros.create('component', 'default', id, details);
                }
                return component;
            }
        },
        /**
         * Handles extension registration, unpackaging, and injection of their
         * registered assets and declarations into the appropriate locations.
         */
        extensions: {
            is_initialized: false,
            init: function () {
                if (!this.is_initialized)
                {
                    if (this.data === null)
                    {
                        this.data = {};
                    }
                    this.is_initialized = true;
                }
            }
        },
        /**
         * Handles typical page functionality,
         * and provides lookup and update functionality for the current page.
         */
        page: {
            is_initialized: false,
            scoped: null,
            init: function () {
                if (!this.is_initialized)
                {
                    if (this.data === null)
                    {
                        this.data = {};
                    }
                    this.is_initialized = true;
                }
            },
            scope: function (layout)
            {
                this.scoped = layout;
            },
            unscope: function () {
                this.scoped = null;
            }
        },
        /**
         * Handles definition of the current user and dissemination
         * of the object representation of the user to relevant objects.
         */
        user: {
            is_initialized: false,
            current_user: null,
            users: {},
            init: function () {
                let class_obj, user, username, organization, details, role, enabled, logged_in, verified;
                if (!this.is_initialized)
                {
                    if (this.data === null)
                    {
                        this.data = {};
                    }
                    for (let key in this.data)
                    {
                        if (this.data.hasOwnProperty(key))
                        {
                            username = this.data[key].username || 'anonymous';
                            organization = this.data[key].organization || 'public';
                            details = this.data[key].details || {};
                            role = this.data[key].role || 'default';
                            enabled = this.data[key].enabled || false;
                            logged_in = this.data[key].logged_in || false;
                            verified = this.data[key].verified || false;
                            if (key === 'current')
                            {
                                class_obj = lib.core.internals.current_user;
                            } else {
                                class_obj = lib.core.defaults.user;
                            }
                            this.users[key] = new class_obj(key, username, organization, details, role, enabled, logged_in, verified);
                        }
                    }
                    if (!this.users.hasOwnProperty('current'))
                    {
                        class_obj = lib.core.internals.current_user;
                        this.users['current'] = new class_obj('current', 'anonymous', 'public', {}, 'default', false, false, false);
                    }
                    this.current_user = this.users['current'];
                    this.is_initialized = true;
                }
            },

        },
        /**
         * Provides a registry of known endpoints, which registered objects
         * are authorized to access them, and distributes them on request
         * to authorized objects.
         */
        endpoints: {
            is_initialized: false,
            init: function () {
                if (!this.is_initialized)
                {
                    if (this.data === null)
                    {
                        this.data = {};
                    }
                    this.is_initialized = true;
                }
            }
        },
        /**
         * Handles internal operations on the layout
         */
        layout: {
            is_initialized: false,
            init: function () {
                if (!this.is_initialized)
                {
                    if (this.data === null)
                    {
                        this.data = {};
                    }
                    this.is_initialized = true;
                }
            }
        },
        /**
         * Handles internal operations on the skin
         */
        skin: {
            is_initialized: false,
            init: function () {
                if (!this.is_initialized)
                {
                    if (this.data === null)
                    {
                        this.data = {};
                    }
                    this.is_initialized = true;
                }
            }
        },
        /**
         * Page controllers are registered here
         */
        controllers: {
            is_initialized: false,
            init: function () {
                if (!this.is_initialized)
                {
                    if (this.data === null)
                    {
                        this.data = {};
                    }
                    this.is_initialized = true;
                }
            }
        },
        /**
         * Page models are registered here
         */
        models: {
            is_initialized: false,
            init: function () {
                if (!this.is_initialized)
                {
                    if (this.data === null)
                    {
                        this.data = {};
                    }
                    this.is_initialized = true;
                }
            }
        },
        /**
         * View objects are registered here
         */
        views: {
            is_initialized: false,
            init: function () {
                if (!this.is_initialized)
                {
                    if (this.data === null)
                    {
                        this.data = {};
                    }
                    this.is_initialized = true;
                }
            }
        },
        /**
         * Common library for classes
         */
        common: {
            observer: (function ()
            {
                /**
                 * @type IntersectionObserver|Function This will be a null function
                 *     if the current browser does not support IntersectionObservers
                 */
                var observer = null;
                var lib = {
                    options: {
                        root: null,
                        rootMargin: '0px',
                        threshold: 0,
                    },
                    /**
                     * Creates an IntersectionObserver, or nulls it if there is
                     * no supported functionality in the current browser scope.
                     * 
                     * @param Function callback A callback to fire when an intersection event occurs
                     * @param string margin Margin around the root. Can have values similar to the CSS margin property, e.g. "10px 20px 30px 40px" (top, right, bottom, left)
                     * @param int threshold The threshold to fire the intersection event on
                     * @returns Boolean
                     */
                    initialize: function (callback, margin, threshold)
                    {
                        if (typeof IntersectionObserver == 'undefined')
                        {
                            // Webkit and super old IE browsers do not support
                            // this functionality currently.
                            this.observer = this.fallback;
                            return false;
                        }
                        this.options.root = this.options.root;
                        this.options.rootMargin = margin || this.options.rootMargin;
                        this.options.threshold = threshold || this.options.threshold;
                        try
                        {
                            this.observer = new IntersectionObserver(callback, this.options);
                        } catch (error)
                        {
                            lib.errors.errors.push(error);
                            console.error(error);
                            return false;
                        }
                        return true;
                    },
                    /**
                     * Binds a selector event for intersection observation.
                     * 
                     * @param Element element
                     * @param Function callback
                     * @returns undefined
                     */
                    bind: function (element, callback)
                    {
                        this.callback = callback;
                        this.observer.observe(element, this.callback);
                    },
                    /**
                     * Releases an event from intersection observation.
                     */
                    release: function (element)
                    {
                        this.observer.unobserve(element);
                    },
                    /**
                     * Disables the observer entirely
                     */
                    disable: function ()
                    {
                        this.observer.disconnect();
                    },
                    /**
                     * Fallback observer emulated for legacy support that
                     * can still run ES6 for whatever reason.
                     * 
                     * @param {type} event
                     * @param {type} options
                     * @returns undefined
                     */
                    fallback: function (event, options)
                    {
                        //no-op
                    }
                };
                /**
                 * Intersection Observer Constructor
                 * 
                 * Observes intersection events, and fires bound callbacks when they occur.
                 * 
                 * @param type root Dom element used for checking the visibility of the target.
                 * @param string margin Margin around the root. Can have values similar to the CSS margin property, e.g. "10px 20px 30px 40px" (top, right, bottom, left)
                 * @param int|array threshold
                 * @returns IntersectObserver
                 */
                var IntersectObserver = function (callback, margin, threshold)
                {
                    this.observer = observer;
                    this.options = lib.options;
                    this.bindings = {};
                    this.initialize(callback, margin, threshold);
                    return this;
                };
                IntersectObserver.prototype.initialize = lib.initialize;
                IntersectObserver.prototype.fallback = lib.fallback;
                IntersectObserver.prototype.bind = lib.bind;
                IntersectObserver.prototype.constructor = IntersectObserver;
                return IntersectObserver;
            })(),

        }

    };
    /**
     * Represents the object prototype for the global accessor
     */
    const proto = {
        /**
         * Returns all of the registered class keys for the given type
         * @param {type} type
         * @return {unresolved}
         */
        index: function (type) {
            const valid = lib.core.map;
            let target;
            if (!valid.hasOwnProperty(type))
            {
                throw new TypeError('Type [' + type + '] is not a valid class registration key.');
            }
            target = lib[valid[type]];
            return target.index();
        },
        /**
         * Registers a new class instance by keyword,
         * so it can be instantiated using the `create` method,
         * or extended further with the `extend` method.
         * @param {type} type
         * @param {type} key
         * @param {type} object
         * @return {unresolved}
         */
        register: function (type, key, object) {
            const valid = lib.core.map;
            let target;
            if (!valid.hasOwnProperty(type))
            {
                throw new TypeError('Type [' + type + '] is not a valid class registration key.');
            }
            target = lib[valid[type]];
            target.register(key, object);
            return this;
        },
        /**
         * Removes registration of an already registered class instance
         * @param {type} type
         * @param {type} key
         * @return {unresolved}
         */
        unregister: function (type, key) {
            const valid = lib.core.map;
            let target;
            if (!valid.hasOwnProperty(type))
            {
                throw new TypeError('Type [' + type + '] is not a valid class registration key.');
            }
            target = lib[valid[type]];
            target.unregister(key);
            return this;
        },
        /**
         * Checks if a given key has a class registration associated with it of the given type.
         * If this returns true, the `extend` method must not throw an error given the same parameters.
         * @param {type} type
         * @param {type} key
         * @return {unresolved}
         */
        isRegistered: function (type, key) {
            const valid = lib.core.map;
            let target;
            if (!valid.hasOwnProperty(type))
            {
                throw new TypeError('Type [' + type + '] is not a valid class registration key.');
            }
            target = lib[valid[type]];
            return target.isRegistered(key);
        },
        /**
         * Creates a new instance of a registered class
         * @param {type} type
         * @param {type} key
         * @param {type} id
         * @param {type} args
         * @param {type} flags
         * @return {unresolved}
         */
        create: function (type, key, id, args, flags)
        {
            const valid = lib.core.map;
            let target, instance;
            if (!valid.hasOwnProperty(type))
            {
                throw new TypeError('Type [' + type + '] is not a valid class registration key.');
            }
            target = lib[valid[type]];
            instance = target.create(key, id, args, flags);
            return instance;
        },
        /**
         * Returns an abstract class for extension
         * @param {type} type
         * @return {unresolved}
         */
        abstract: function (type) {
            const valid = lib.core.map;
            let target;
            if (!valid.hasOwnProperty(type))
            {
                throw new TypeError('Type [' + type + '] is not a valid class registration key.');
            }
            target = lib[valid[type]];
            return target.abstract;
        },
        /**
         * Returns an already registered concrete implementation
         * of an abstract for further extension
         * @param {type} type
         * @param {type} key
         * @return {unresolved}
         */
        extend: function (type, key) {
            const valid = lib.core.map;
            let target;
            if (!valid.hasOwnProperty(type))
            {
                throw new TypeError('Type [' + type + '] is not a valid class registration key.');
            }
            target = lib[valid[type]];
            if (!target.registered.hasOwnProperty(key))
            {
                throw new TypeError('Instance [' + key + '] of type [' + type + '] is not registered as any known class.');
            }
            return target.registered[key];
        }
    };
    /**
     * The root instance of the global accessor
     */
    let oroboros;
    let front_controller;
    /**
     * The global accessor function
     */
    function Oroboros(id, selector, proxy) {
        let token;
        if (!(this instanceof Oroboros))
        {
            return new Oroboros(id, selector, proxy);
        }
        this.id = id || 'root';
        this.selector = selector || win; // Use the root window object if no selector is passed
//        this.proxy = proxy || lib.proxy.initialize(this.selector);
        return this;
    }
    ;
    Oroboros.prototype = Object.create(proto);
    Oroboros.prototype.constructor = Oroboros;
    oroboros = new Oroboros();

    // Kick off the internal library initialization
    lib.init();
    front_controller = new lib.core.internals.front_controller('root');

    console.groupCollapsed('Core Lib Bootstrap');
    console.info('Document');
    console.dir(doc);
    console.info('Window');
    console.dir(win);
    console.info('lib');
    console.dir(lib);
    console.info('detected components in dom');
    console.dir(components);
    console.info('detected context in dom');
    console.dir(context);
    console.info('data');
    console.dir(data);
    console.info('oroboros class');
    console.dir(Oroboros);
    console.info('oroboros object');
    console.dir(oroboros);
    console.info('root front controller');
    console.dir(front_controller);
    console.info('fingerprint');
    console.dir(fingerprint);
//    console.info('Promise native');
//    console.dir(Promise);
    console.groupEnd();

    // Register the global accessor
    if (typeof win.Oroboros === "undefined")
    {
        win.Oroboros = Oroboros;
    }

    // fin

})(document, window);
