/* 
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

'use strict';
(function (core) {
    const layout = 'frontend';
    const oroboros = core();
    const abstract = oroboros.abstract('layout');
    const layout_sections = {
        'header': 'page-header',
        'primary': 'primary-content',
        'left-sidebar': 'sidebar-left',
        'right-sidebar': 'sidebar-right',
        'footer': 'page-footer'
    };
    const component_bindings = {
        'accounts-index': 'table',
    };
    const lib = {
        is_initialized: false,
        is_ready: false,
        init: function () {
            if (!this.is_initialized)
            {
                this.container.init();
                this.sidebar.init();
                this.content.init();
                this.tooltips.init();
                this.popovers.init();
                this.is_initialized = true;
            }
        },
        ready: function () {
            if (!this.is_ready)
            {
                this.container.ready();
                this.sidebar.ready();
                this.content.ready();
                this.tooltips.ready();
                this.popovers.ready();
                this.is_ready = true;
            }
        },
        container: {
            is_initialized: false,
            is_ready: false,
            selector: null,
            height: 0,
            width: 0,
            init: function () {
                if (!this.is_initialized)
                {
                    const dimensions = function () {
                        target.height = target.selector.innerHeight();
                        target.width = target.selector.innerWidth();
                    };
                    let target = this;
                    this.selector = jQuery('[data-component="dashboard-content-wrapper]:first-child');
                    this.selector.on('change', dimensions);
                    dimensions();
                    this.is_initialized = true;
                }
            },
            ready: function () {
                let target = this;
                let content = lib.content;
                let sidebar = lib.sidebar;
                if (!this.is_ready)
                {

                    this.is_ready = true;
                }
            }
        },
        sidebar: {
            is_initialized: false,
            is_ready: false,
            selector: null,
            init: function () {
                const dimensions = function () {
                    target.selector.innerHeight(container.selector.innerHeight());
                };
                let target = this;
                let content = lib.content;
                let container = lib.container;
                if (!this.is_initialized)
                {
                    this.selector = jQuery('#left-sidebar');
                    container.selector.on('change', dimensions);
                    dimensions();
                    this.is_initialized = true;
                }
            },
            ready: function () {
                let target = this;
                let container = lib.container;
                let content = lib.content;
                if (!this.is_ready)
                {
                    this.selector.on('change', function () {

                    });
                    this.is_ready = true;
                }
            },
            setHeight: function () {
                let selector = this.selector;
            }
        },
        content: {
            is_initialized: false,
            is_ready: false,
            selector: null,
            init: function () {
                const dimensions = function () {
                    target.selector.innerHeight(container.selector.innerHeight());
                };
                let target = this;
                let sidebar = lib.sidebar;
                let container = lib.container;
                if (!this.is_initialized)
                {
                    this.selector = jQuery('#primary-content');
                    this.is_initialized = true;
                }
            },
            ready: function () {
                let target = this;
                let container = lib.container;
                let sidebar = lib.sidebar;
                if (!this.is_ready)
                {

                    this.is_ready = true;
                }
            }
        },
        tooltips: {
            is_initialized: false,
            is_ready: false,
            selector: null,
            init: function () {
                if (!this.is_initialized)
                {
                    this.selector = jQuery('[title]');
                    this.is_initialized = true;
                }
            },
            ready: function () {
                if (!this.is_ready)
                {
                    this.selector.tooltip();
                    this.is_ready = true;
                }
            }
        },
        popovers: {
            is_initialized: false,
            is_ready: false,
            selector: null,
            init: function () {
                if (!this.is_initialized)
                {
                    this.selector = jQuery('[data-toggle="popover"]');
                    this.is_initialized = true;
                }
            },
            ready: function () {
                if (!this.is_ready)
                {
                    this.selector.popover();
                    this.is_ready = true;
                }
            }
        },
    };
    const DefaultLayout = class DefaultLayout extends abstract {

        static classKey()
        {
            return layout;
        }

        constructor(id, args, flags) {
            super(id, args, flags);
            this.initialize();
        }

        initialize() {
            super.initialize();
        }

        onReady(subject) {
            let element;
            lib.ready();
            jQuery('[title]').tooltip();
            jQuery('[data-toggle="popover"]').popover();
            for (let key in layout_sections)
            {
                if (layout_sections.hasOwnProperty(key))
                {
                    try {
                        element = document.getElementById(layout_sections[key]);
                        subject.loadLayoutSection(key, layout_sections[key], {
                            id: layout_sections[key],
                            element: element
                        });
                    } catch (e) {
                        console.warn(e.message);
                    }
                }
            }
        }
    }
    lib.init();
    oroboros.register('layout', layout, DefaultLayout);
})(Oroboros);
