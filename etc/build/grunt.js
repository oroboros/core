/* 
 * The MIT License
 *
 * Copyright 2021 Brian Dayhoff <bdayhoff@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

module.exports = function (grunt) {
    const directives = grunt.file.readJSON('config/grunt.json');
    const lib = {
        is_initialized: false,
        init: function () {
            if (!this.is_initialized) {
                directives.init.pkg = grunt.file.readJSON('package.json');
                this.core.init();
                this.app.init();
                this.is_initialized = true;
            }
        },
        core: {
            is_initialized: false,
            init: function () {
                this.shell.init();
                this.php.init();
                this.twig.init();
                this.js.init();
                this.scss.init();
                this.html.init();
                if (!this.is_initialized) {
                    this.is_initialized = true;
                }
            },
            shell: {
                is_initialized: false,
                init: function () {
                    if (!this.is_initialized) {
                        this.is_initialized = true;
                    }
                }
            },
            php: {
                is_initialized: false,
                init: function () {
                    if (!this.is_initialized) {
                        this.environment.init();
                        this.extensions.init();
                        this.modules.init();
                        this.services.init();
                        this.is_initialized = true;
                    }
                },
                environment: {
                    is_initialized: false,
                    init: function () {
                        if (!this.is_initialized) {
                            this.is_initialized = true;
                        }
                    },
                },
                extensions: {
                    is_initialized: false,
                    init: function () {
                        if (!this.is_initialized) {
                            this.is_initialized = true;
                        }
                    },
                },
                modules: {
                    is_initialized: false,
                    init: function () {
                        if (!this.is_initialized) {
                            this.is_initialized = true;
                        }
                    },
                },
                services: {
                    is_initialized: false,
                    init: function () {
                        if (!this.is_initialized) {
                            this.is_initialized = true;
                        }
                    },
                },
            },
            twig: {
                is_initialized: false,
                init: function () {
                    if (!this.is_initialized) {
                        this.is_initialized = true;
                    }
                }
            },
            js: {
                is_initialized: false,
                init: function () {
                    if (!this.is_initialized) {
                        this.terser.init();
                        this.is_initialized = true;
                    }
                },
                terser: {
                    is_initialized: false,
                    init: function () {
                        if (!this.is_initialized) {
                            this.sourcePaths.init();
                            this.is_initialized = true;
                        }
                    },
                    sourcePaths: {
                        is_initialized: false,
                        init: function () {
                            if (!this.is_initialized) {
                                this.core();
                                this.extensions();
                                this.modules();
                                this.is_initialized = true;
                            }
                        },
                        core: function () {
                            let sourcePath = 'etc/src/js/';
                            let destPath = 'public/js/';
                            let keys = {};
                            let files = grunt.file.expand({filter: "isFile", cwd: sourcePath}, ["**"]);
                            files.map(function (file) {
                                keys[destPath + file.replace('.js', '.min.js')] = sourcePath + file;
                            });
                            directives.init.terser.build.files = keys;
                        },
                        extensions: function () {
                            let basePath = 'lib/extensions/';
                            let destPath = 'public/js/extensions/';
                            let keys = {};
                            for (let extension of grunt.file.expand({cwd: basePath}, ["*"])) {
                                let extensionSourcePath = basePath + extension + '/etc/src/js/';
                                let extensionDestPath = destPath + extension + '/';
                                let files = grunt.file.expand({filter: "isFile", cwd: extensionSourcePath}, ["**/*.js"]);
                                files.map(function (file) {
                                    keys[extensionDestPath + file.replace('.js', '.min.js')] = extensionSourcePath + file;
                                });
                            }
                            for (let k in keys) {
                                directives.init.terser.build.files[k] = keys[k];
                            }
                        },
                        modules: function () {
                            let basePath = 'lib/modules/';
                            let destPath = 'public/js/modules/';
                            let keys = {};
                            for (let module of grunt.file.expand({cwd: basePath}, ["*"])) {
                                let moduleSourcePath = basePath + module + '/etc/src/js/';
                                let moduleDestPath = destPath + module + '/';
                                let files = grunt.file.expand({filter: "isFile", cwd: moduleSourcePath}, ["**/*.js"]);
                                files.map(function (file) {
                                    keys[moduleDestPath + file.replace('.js', '.min.js')] = moduleSourcePath + file;
                                });
                            }
                            for (let k in keys) {
                                directives.init.terser.build.files[k] = keys[k];
                            }
                        }
                    }
                }
            },
            scss: {
                is_initialized: false,
                init: function () {
                    if (!this.is_initialized) {
                        this.definition();
                        this.options();
                        this.postcss();
                        this.is_initialized = true;
                    }
                },
                definition: function () {
                    directives.init.sass.options.implementation = require('node-sass');
                },
                options: function () {
                    directives.init.sass.options.fiber = require('fibers');
                },
                postcss: function () {
                    let processors = [];
                    try {
                        require.resolve('pixrem');
                        processors.push(require('pixrem')()); // add fallbacks for rem units
                    } catch (e) {
                        console.warn('pixrem not found, scss rem unit fallbacks will not be enabled for task postcss');
                    }
                    try {
                        require.resolve('autoprefixer');
                        processors.push(require('autoprefixer')); // add vendor prefixes
                    } catch (e) {
                        console.warn('autoprefixer not found, scss vendor prefixing will not be enabled for task postcss');
                    }
                    try {
                        require.resolve('cssnano');
                        processors.push(require('cssnano')()); // minify the result
                    } catch (e) {
                        console.warn('cssnano not found, scss minification will not be enabled for task postcss');
                    }
                    directives.init.postcss.options.processors = processors;
                }
            },
            html: {
                is_initialized: false,
                init: function () {
                    if (!this.is_initialized) {
                        this.is_initialized = true;
                    }
                }
            },
        },
        app: {
            is_initialized: false,
            init: function () {
                if (!this.is_initialized) {
                    this.shell.init();
                    this.php.init();
                    this.twig.init();
                    this.js.init();
                    this.scss.init();
                    this.html.init();
                    this.is_initialized = true;
                }
            },
            shell: {
                is_initialized: false,
                init: function () {
                    if (!this.is_initialized) {
                        this.is_initialized = true;
                    }
                }
            },
            php: {
                is_initialized: false,
                init: function () {
                    if (!this.is_initialized) {
                        this.environment.init();
                        this.extensions.init();
                        this.modules.init();
                        this.services.init();
                        this.is_initialized = true;
                    }
                },
                environment: {
                    is_initialized: false,
                    init: function () {
                        if (!this.is_initialized) {
                            this.is_initialized = true;
                        }
                    },
                },
                extensions: {
                    is_initialized: false,
                    init: function () {
                        if (!this.is_initialized) {
                            this.is_initialized = true;
                        }
                    },
                },
                modules: {
                    is_initialized: false,
                    init: function () {
                        if (!this.is_initialized) {
                            this.is_initialized = true;
                        }
                    },
                },
                services: {
                    is_initialized: false,
                    init: function () {
                        if (!this.is_initialized) {
                            this.is_initialized = true;
                        }
                    },
                },
            },
            twig: {
                is_initialized: false,
                init: function () {
                    if (!this.is_initialized) {
                        this.is_initialized = true;
                    }
                }
            },
            js: {
                is_initialized: false,
                init: function () {
                    if (!this.is_initialized) {
                        this.is_initialized = true;
                    }
                }
            },
            scss: {
                is_initialized: false,
                init: function () {
                    if (!this.is_initialized) {
                        this.is_initialized = true;
                    }
                }
            },
            html: {
                is_initialized: false,
                init: function () {
                    if (!this.is_initialized) {
                        this.is_initialized = true;
                    }
                }
            }
        }
    };
    const output = {
        init: directives.init,
        tasks: directives.tasks
    };
    lib.init();
    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);
    require('matchdep').filterDev('gruntify-*').forEach(grunt.loadNpmTasks);
    grunt.initConfig(directives.init);
    // Register Grunt tasks
    for (const [key, value] of Object.entries(directives.tasks)) {
        grunt.registerTask(key, value);
    }
    return output;
}
